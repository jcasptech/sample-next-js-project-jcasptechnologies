import * as Yup from "yup";
export interface ShowsObject {
  date: any[];
  startTime: string;
}

export interface AddShowsFormValidationProps {
  venueSelect: string;
  showDetails: ShowsObject[];
}

export const AddShowsFormValidationSchema = Yup.object({
  venueSelect: Yup.string().required("Please select a venue"),
  showDetails: Yup.array()
    .of(
      Yup.object().shape({
        date: Yup.array().required("Please select a show date."),
        startTime: Yup.string().required("Please select a show time."),
      })
    )
    .min(1, "shows minimum"),
}).required();
