import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface CreatePostingsOtherDetailsInputs {
  playSettings: string;
  expectedAttandance: string;
  note: string;
}

export const CreatePostingsOtherDetailsSchema = Yup.object({
  playSettings: Yup.string().required("Play sitting is a required field"),
  expectedAttandance: Yup.string().required(
    "Expected attandance is required field"
  ),
  note: Yup.string().nullable(),
  //   .required("Note is a required field")
  //   .min(5, "Note should be 5 characters minimum.")
  //   .max(1000, "Note must be at most 1000 characters"),
});
