import * as Yup from "yup";

export interface UploadCSVFormInputs {
  csvUrl: string;
}

export const UploadCSVFormSchema = Yup.object({
  csvUrl: Yup.string().required("CSV file is a required field"),
}).required();
