import * as Yup from "yup";
export interface ShowsObject {
  date: string[];
  startTime: string;
  artistId: string;
}

export interface AdminAddShowsFormValidationProps {
  venueSelect: string;
  showDetails: ShowsObject[];
}

export const AdminAddShowsFormValidationSchema = Yup.object({
  venueSelect: Yup.string().required("Venue is required field"),
  showDetails: Yup.array()
    .of(
      Yup.object().shape({
        date: Yup.array().required("Date is Required field"),
        startTime: Yup.string().required("Please select time"),
        artistId: Yup.string().required("Artist is Required field"),
      })
    )
    .min(1, "shows minimum"),
}).required();
