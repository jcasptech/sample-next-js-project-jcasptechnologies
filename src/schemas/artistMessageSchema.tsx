import * as Yup from "yup";

export interface ArtistMessageInputs {
  message: string;
}
export const ArtistMessageSchema = Yup.object({
  message: Yup.string()
    .required("Please write a message")
    .min(10, "Message should be 10 characters minimum.")
    .max(1000, "Message must be at most 1000 characters"),
}).required();

export interface ArtistRatingMessageInputs {
  message: string;
  rating: number;
}
export const ArtistRatingMessageSchema = Yup.object({
  message: Yup.string()
    .required("Please rate this artist.")
    .min(10, "Message should be 10 characters minimum.")
    .max(1000, "Message must be at most 1000 characters"),
  rating: Yup.string()
    .required("Please select a rating")
    .min(0.5, "The minimum rating is 0.5"),
}).required();
