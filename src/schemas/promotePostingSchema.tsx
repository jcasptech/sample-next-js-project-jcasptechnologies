import * as Yup from "yup";

export interface PromotePostingInputs {
  type: "email" | "push" | "text";
  message: string;
  radius: number;
}

export const PromotePostingSchema = Yup.object({
  type: Yup.string().required("Type is a required field"),
  message: Yup.string().when("type", (type, schema) => {
    if (type !== "email") {
      return schema.required("Message is a required field");
    }
    return schema;
  }),
  radius: Yup.number()
    .required("Radius is a required field")
    .typeError("Radius is a required field")
    .min(0, "Radius should be 0 minimum.")
    .max(500, "Radius should be 500 maximum."),
}).required();
