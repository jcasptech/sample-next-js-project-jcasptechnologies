import * as Yup from "yup";

export interface ResetPasswordFormInputs {
  password: string;
  confirmPassword: string;
}
export const ResetPasswordFormValidateSchema = Yup.object({
  password: Yup.string()
    .required("New password is a required field")
    .min(8, "Password is too short - should be 8 characters minimum.")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&~#^()_<>])[A-Za-z\d@$!%*?&~#^()_<>]{8,}$/,
      "Password must contain at least 1 uppercase letter 1 lowercase letter 1 digit and 1 special character"
    ),
  confirmPassword: Yup.string()
    .required("Confirm password is a required field")
    .oneOf(
      [Yup.ref("password")],
      "Password and confirm password does not match"
    ),
}).required();
