import * as Yup from "yup";

export interface AdminAddEventsFormValidationInputs {
  title: string;
  venueId: string;
  artistId: string;
  variety: string;
  formats: string;
  totalCost: number;
  artistProceeds: number;
  bookingDate: string;
  startTime: string;
  endTime: string;
}

export const AdminAddEventsFormValidationSchema = Yup.object({
  title: Yup.string().required("Title is a required field"),
  venueId: Yup.string().required("Venue is a required field"),
  artistId: Yup.string().required("Artist is a Required field"),
  variety: Yup.string().required("Variety is a Required field"),
  formats: Yup.string().required("Preferred Formats is a Required field"),
  totalCost: Yup.number()
    .typeError("Total Cost is a Required field")
    .required("Total Cost is a Required field"),
  artistProceeds: Yup.number()
    .typeError("Artist Proceeds is a Required field")
    .required("Artist Proceeds is a Required field"),
  bookingDate: Yup.string().required("Date is a Required field"),
  startTime: Yup.string().required("Please select time"),
  endTime: Yup.string().required("Please select time"),
}).required();
