import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface JCaspLightFormInputs {
  name: string;
  venueAddress: string;
  googlePlaceId: string;
  userName: string;
  email: string;
}

export const JCaspLightFormValidateSchema = Yup.object({
  name: Yup.string()
    .required("Name is a required field")
    .matches(/^\S/, "Name cannot start with a blank space")
    .min(4, "Contact name must be at least 4 characters")
    .max(30, "Contact name cannot exceed 30 characters"),
  venueAddress: Yup.string()
    .required("Venue Address is a required field")
    .matches(/^\S/, "Venue address cannot start with a blank space"),
  city: Yup.string()
    .required("City is a required field")
    .matches(/^\S/, "City name cannot start with a blank space")
    .min(4, "City name must be at least 4 characters")
    .max(30, "City name cannot exceed 50 characters"),
  email: Yup.string()
    .required("Email is a required field")
    .matches(/^\S/, "Email cannot start with a blank space")
    .email("Email must be a valid")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
}).required();
