import * as Yup from "yup";

export const ArtistProfileFormLinksSchema = Yup.object({
  instagramUsername: Yup.string().trim(),
  youtubeChannel: Yup.string().url("Invalid URL format"),
  vemeoLink: Yup.string().url("Invalid URL format"),
  patreonLink: Yup.string().url("Invalid URL format"),
  spotifyLink: Yup.string().url("Invalid URL format"),
  facebookPage: Yup.string().url("Invalid URL format"),
  website: Yup.string().url("Invalid URL format"),
  liveStreamUrl: Yup.string().url("Invalid URL format"),
}).required();
