import * as Yup from "yup";

export interface PostingInputs {
  note: string;
}
export const PostingSchema = Yup.object({
  note: Yup.string()
    .required("Please write a about you.")
    .min(5, "Note should be 5 characters minimum.")
    .max(1000, "Note must be at most 1000 characters"),
}).required();

export interface AdminPostingInputs {
  // title: string;
  // totalCost: number;
  // startTime: string;
  // endTime: string;
  // artistProceeds: number;
  // bookingDate: string;
  note: string;
}

// export const AdminPostingSchema = Yup.object({
//   title: Yup.string().nullable(),
//   totalCost: Yup.number().nullable(),
//   startTime: Yup.string().required("Start time is required field").nullable(),
//   endTime: Yup.string().required("End time is required field").nullable(),
//   artistProceeds: Yup.number().nullable(),
//   bookingDate: Yup.string().nullable(),
//   note: Yup.string().nullable(),
// }).required();
