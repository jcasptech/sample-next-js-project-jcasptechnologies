import * as Yup from "yup";

export interface ChangePasswordFormInputs {
  newPassword: string;
  confirmPassword: string;
}
export const ChangePasswordFormValidateSchema = Yup.object({
  newPassword: Yup.string()
    .required("required")
    .matches(/^(?=.*[A-Z])/, "uppercase")
    .matches(/^(?=.*[!@#$%^&*])/, "special")
    .min(8, "long"),
  confirmPassword: Yup.string()
    .required("Confirm Password is a required field")
    .oneOf(
      [Yup.ref("newPassword")],
      "Password and confirm password does not match"
    ),
}).required();
