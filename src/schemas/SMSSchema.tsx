import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface SMSInputs {
  phone: string;
}

export const SMSFormValidateSchema = Yup.object({
  phone: Yup.string().required("Phone Number is a required field."),
  // .matches(REGEX.phone, REGEX_VALIDATION_MESSAGE.phone),
}).required();
