import * as Yup from "yup";
export interface VenueEditFormInputs {
  venueName: string;
  venueDetails: string;
  venuePhotos: string;
  contactNumber: string;
  venueAddress: string;
  websiteAddress: string;
  facebookLink: string;
  instagramLink: string;
  youtubeLink: string;
  assignedVenueTo: string;
}

export const VenueEditFormSchema = Yup.object({
  venueName: Yup.string().required("Venue name is a required field"),
  venueDetails: Yup.string().required("Venue details is a required field"),
  venuePhotos: Yup.array().nullable(),
  contactNumber: Yup.string().required("Contact number is a required field"),
  venueAddress: Yup.string().required("Venue Address is a required field"),
  websiteAddress: Yup.string().url("Invalid URL format"),
  facebookLink: Yup.string().url("Invalid URL format"),
  instagramLink: Yup.string().url("Invalid URL format"),
  assignedVenueTo: Yup.string().required("Assign To is a required field"),
  youtubeLink: Yup.string()
    .matches(
      /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
      "Please provide valid Youtube video url"
    )
    .url("Invalid URL format"),
}).required();
