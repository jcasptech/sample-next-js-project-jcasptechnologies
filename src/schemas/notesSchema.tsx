import * as Yup from "yup";

export interface NotesInputs {
  note: string;
}
export const NotesSchema = Yup.object({
  note: Yup.string()
    .required("Please write a note.")
    .min(5, "Note should be 5 characters minimum.")
    .max(1000, "Note must be at most 1000 characters"),
}).required();
