import * as Yup from "yup";

export interface CreatePostingsMusicFormatInputs {
  musicStyles: any[];
  dates: string;
  start: string;
  end: string;
}

export const CreatePostingsMusicFormatSchema = Yup.object({
  dates: Yup.string().required("Date is a required field"),
  start: Yup.string().required("Time is a required field"),
  end: Yup.string().required("Time is a required field"),
  musicStyles: Yup.array()
    .max(10, "Please select at most ten option")
    .min(1, "Please select at least one option")
    .required("Music styles is a required field"),
}).required();
