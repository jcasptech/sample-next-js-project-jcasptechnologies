import * as Yup from "yup";

export const CreateArtistProfileFormSchema = Yup.object({
  photos: Yup.string().nullable().required("Image link is a required field"),
}).required();
