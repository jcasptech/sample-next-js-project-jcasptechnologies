import * as Yup from "yup";

export const CreateArtistProfileFormSchema = Yup.object({
  audioFiles: Yup.string().nullable().required("Audio file is a required "),
}).required();
