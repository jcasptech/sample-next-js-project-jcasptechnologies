import * as Yup from "yup";

export interface artistScoreInputs {
  isAdminScore: boolean;
  adminScore: number;
  score: number;
}

export const artistScoreSchema = Yup.object({
  isAdminScore: Yup.boolean(),
  score: Yup.number()
    .required("Score is a required field")
    .typeError("Score is a required field")
    .min(0, "Score should be 0 minimum")
    .max(200, "Score must be at most 200 maximum"),
  adminScore: Yup.number()
    .nullable()
    .when("isAdminManage", (isAdminScore, schema) => {
      if (isAdminScore) {
        return schema
          .required("Admin Score is a required field")
          .typeError("Admin Score is a required field")
          .min(0, "Admin Score should be 0 minimum")
          .max(200, "Admin Score must be at most 200 maximum");
      }
      return schema;
    }),
}).required();
