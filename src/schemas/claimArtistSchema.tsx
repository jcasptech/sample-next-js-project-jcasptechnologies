import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface ClaimArtistInputs {
  name: string;
  phone: string;
  email: string;
  message: string;
}
export const ClaimArtistSchema = Yup.object({
  name: Yup.string().required("Name is a required field"),
  phone: Yup.string().required("Phone is a required field"),
  email: Yup.string()
    .required("Email is a required field.")
    .email("Email must be a valid email")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  message: Yup.string()
    .required("Message is a required field")
    .min(10, "Message should be 10 characters minimum.")
    .max(1000, "Message must be at most 1000 characters"),
}).required();
