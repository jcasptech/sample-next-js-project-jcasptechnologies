import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface RequestFeatureInputs {
  email: string;
  name: string;
  userType: string;
  message: string;
}

export const RequestFeatureFormValidateSchema = Yup.object({
  name: Yup.string().required("Please enter your name."),
  userType: Yup.string().required("Please select one option."),
  message: Yup.string().required("Please add feature description."),
  email: Yup.string()
    .required("Please enter your email address.")
    .email("Email must be a valid email.")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
}).required();
