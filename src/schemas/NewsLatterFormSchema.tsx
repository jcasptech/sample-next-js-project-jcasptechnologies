import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface NewsLatterFormInputs {
  userQuestion: string;
}

export const NewsLatterFormValidateSchema = Yup.object({
  userQuestion: Yup.string()
    .required("Please give the answer")
    .max(50, "answer must be at most 50 characters"),
}).required();
