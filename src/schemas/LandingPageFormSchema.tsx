import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface LandingPagePhoneInputs {
  phone: string;
}

export interface LandingPageFormInputs {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  restaurantName: string;
  website: string;
  reason: string;
  enquiryType: string;
}

export const LandingPageFormValidateSchema = Yup.object({
  firstName: Yup.string()
    .nullable()
    .max(60, "First Name must be at most 60 characters")
    .test(
      "blankSpance",
      "First Name cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  lastName: Yup.string()
    .nullable()
    .max(60, "Last Name must be at most 60 characters")
    .test(
      "blankSpance",
      "Last Name cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  email: Yup.string()
    .required("Email Address is a required field")
    .email("Email Address must be a valid email")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  phoneNumber: Yup.string()
    .required("Phone Number is a required field")
    .matches(REGEX.phone, REGEX_VALIDATION_MESSAGE.phone),
  restaurantName: Yup.string()
    .nullable()
    .max(60, "Restaurant Name must be at most 60 characters")
    .test(
      "blankSpance",
      "Restaurant Name cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  website: Yup.string()
    .nullable()
    .max(60, "Website must be at most 60 characters")
    .test("isURL", "Invalid URL", (val: any) => {
      return val
        ? /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([/\w .-]*)*\/?$/.test(val)
        : true;
    })
    .test(
      "blankSpance",
      "Website cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  reason: Yup.string()
    .nullable()
    .max(60, "About Your Needs must be at most 60 characters")
    .test(
      "blankSpance",
      "About Your Needs cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  enquiryType: Yup.string().required("Enquiry Type is a required field"),
}).required();

export const LandingPagePhoneValidateSchema = Yup.object({
  phone: Yup.string()
    .required("Phone Number is a required field")
    .matches(REGEX.phone, REGEX_VALIDATION_MESSAGE.phone)
    .matches(/^\s*\S[\s\S]*$/, "Phone Number cannot contain only blankspaces"),
}).required();
