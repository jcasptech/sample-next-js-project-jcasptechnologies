import * as Yup from "yup";

export interface ShowUpdateInputs {
  promoteOnJCasp: boolean;
  note: string;
}
export const ShowUpdateSchema = Yup.object({
  note: Yup.string().nullable(),
}).required();
