import * as Yup from "yup";

export interface EventInputs {
  note: string;
  logTips: number;
}
export const EventSchema = Yup.object({
  note: Yup.string().nullable(),
  logTips: Yup.number().nullable(),
}).required();

export interface AdminEventInputs {
  // title: string;
  // totalCost: number;
  // startTime: string;
  // endTime: string;
  // artistProceeds: number;
  // bookingDate: string;
  // logTips: number;
  note: string;
}

export const AdminEventSchema = Yup.object({
  // title: Yup.string().nullable(),
  // totalCost: Yup.number()
  //   .typeError("Total Cost is a Required field")
  //   .required("Total Cost is a Required field"),
  // startTime: Yup.string().nullable(),
  // endTime: Yup.string().nullable(),
  // artistProceeds: Yup.number()
  //   .typeError("Artist Proceeds is a Required field")
  //   .required("Artist Proceeds is a Required field"),
  // logTips: Yup.number().nullable(),
  // bookingDate: Yup.string().nullable(),
  note: Yup.string()
    .required("Please write a about you.")
    .min(5, "Note should be 5 characters minimum.")
    .max(1000, "Note must be at most 1000 characters"),
}).required();
