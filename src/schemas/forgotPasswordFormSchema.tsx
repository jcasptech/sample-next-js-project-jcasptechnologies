import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface ForgotPasswordFormInputs {
  email: string;
}

export const ForgotPasswordFormValidateSchema = Yup.object({
  email: Yup.string()
    .required("Email is a required field")
    .email("Email must be a valid email")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
}).required();
