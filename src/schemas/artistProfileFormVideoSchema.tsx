import * as Yup from "yup";

interface videoInputs {
  youtubeVideoLinks: string;
}

export interface ArtistProfileVideoInputs {
  videos: videoInputs[];
}
export const ArtistProfileFormVideoSchema = Yup.object({
  videos: Yup.array()
    .of(
      Yup.object().shape({
        youtubeVideoLinks: Yup.string()
          .test(
            "isValidURL",
            "Please provide valid Youtube or Vimeo video url.",
            (val: any) => {
              let isValid = false;
              if (val) {
                if (
                  /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube(-nocookie)?\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|live\/|v\/)?)([\w\-]+)(\S+)?$/.test(
                    val
                  )
                ) {
                  isValid = true;
                } else if (
                  /(?:http|https)?:?\/?\/?(?:www\.)?(?:player\.)?vimeo\.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|video\/|)(\d+)(?:|\/\?)/.test(
                    val
                  )
                ) {
                  isValid = true;
                }
              }
              return isValid;
            }
          )
          .url("Invalid URL format"),
      })
    )
    .min(1, "video minimum"),
}).required();
