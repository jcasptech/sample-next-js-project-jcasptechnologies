import * as Yup from "yup";
import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";

export interface ContactUsFormInputs {
  email: string;
  name: string;
  message: string;
}

export const ContactUsFormValidateSchema = Yup.object({
  name: Yup.string().required("Name is a required field"),
  message: Yup.string().required("Message is a required field"),
  email: Yup.string()
    .required("Email Address is a required field")
    .email("Email must be a valid email")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
}).required();
