import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface CreatePostingsFormInputs {
  title: string;
  variety: string;
  offerAmount: string;
  googlePlaceId: string;
  venueId: string;
}

export const CreatePostingsFormFirstSchema = Yup.object({
  title: Yup.string().required("Title is a required field"),
  variety: Yup.string().required("Variety  is a required field"),
  venueId: Yup.string().required("Location is a required field"),
  offerAmount: Yup.number()
    .typeError("Offer Amount is a required field")
    .required("Offer Amount is a required field"),
}).required();
