import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface CreateArtistProfileFormInputs {
  artistElements: any;
  artistName: string;
  artistPhonenumber: string;
  artistEmailId: string;
  artistLocaleCity: string;
  artistLocaleState: string;
  artistLocaleCountry: string;
  paypalId: string;
  vemeoId: string;
  artistBio: string;
  JCaspVanityTag: string;
  songs: any;
  songName: any;
  SongSingerName: string;
  vemeoLink: string;
  facebookPage: string;
  facebookUsername: string;
  instagramUsername: string;
  websiteAddress: string;
  liveStreamUrl: string;
  patreonLink: string;
  spotifyLink: string;
  about: string;
  youtubeVideoLinks: any;
  photos: any;
  audioFiles: any;
}

export const CreateArtistProfileFormSchema = Yup.object({
  artistName: Yup.string().trim().required("Please Enter your Name"),
  about: Yup.string().trim().required("Artist bio is required field"),
  paypalId: Yup.string().trim().required("Paypal id is a required field"),
  vemeoId: Yup.string().trim().required("vemeo id  is a required field"),
  SongSingerName: Yup.string()
    .trim()
    .required("Singer Name is a required field"),
  songName: Yup.string().trim().required("Song Name is a required field"),
  facebookUsername: Yup.string()
    .trim()
    .required("Facebook User Name is a required field"),
  JCaspVanityTag: Yup.string()
    .trim()
    .required("JCasp Vanity Tag is a required field"),
  artistElements: Yup.string().trim().required("Please Select Artist Element"),
  instagramUsername: Yup.string()
    .trim()
    .required("Instagram Username is a required field"),
  artistEmailId: Yup.string()
    .required("Email is a required field")
    .email("Email is must be valid.")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  artistPhonenumber: Yup.string()
    .required("Phone is a required field")
    .matches(/^[0-9]+$/, "Phone must be only digits")
    .min(2, "Phone is too short - should be 2 characters minimum.")
    .max(16, "Phone is too long - should be 16 characters maximum.")
    .matches(/^\s*\S[\s\S]*$/, "Phone cannot contain only blankspaces"),
  vemeoLink: Yup.string().nullable().required("Vemeo link is a required field"),
  patreonLink: Yup.string()
    .nullable()
    .required("Patreon link is a required field"),
  spotifyLink: Yup.string()
    .nullable()
    .required("Spotify link is a required field"),
  // featuredYoutubeVideoUrl: Yup.string()
  //   .nullable()
  //   .required("Please enter your Youtube video url"),
  audioFiles: Yup.string().nullable().required("Audio file is a required "),
  youtubeVideoLinks: Yup.string()
    .nullable()
    .required("Please enter your Youtube video link "),
  facebookPage: Yup.string()
    .nullable()
    .required("Facebook Page link is a required field"),
  websiteAddress: Yup.string()
    .nullable()
    .required("Please provide your Website Address"),
  liveStreamUrl: Yup.string()
    .nullable()
    .required("LiveStream Url is a required field"),
  photos: Yup.string().nullable().required("Image link is a required field"),
  artistBio: Yup.string().required("Bio is a required field"),
  artistLocaleCity: Yup.string()
    .required("City is a required field")
    .max(60, "City must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "City cannot contain only blankspaces"),
  artistLocaleState: Yup.string()
    .required("State is a required field")
    .max(60, "State must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "State cannot contain only blankspaces"),
  artistLocaleCountry: Yup.string()
    .required("Country is a required field")
    .max(60, "Country must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "Country cannot contain only blankspaces"),
}).required();
