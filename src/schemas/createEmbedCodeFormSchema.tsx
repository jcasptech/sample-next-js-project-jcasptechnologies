import * as Yup from "yup";

export interface CreateEmbedCodeFormValidationProps {
  category: string;
  Id: string;
}

export const CreateEmbedCodeFormValidationSchema = Yup.object({
  category: Yup.string().required("Category is a required field"),
  Id: Yup.string().when("category", {
    is: "artist",
    then: Yup.string().required("Artist is a required field"),
    otherwise: Yup.string().required("Venue is a required field"),
  }),
}).required();
