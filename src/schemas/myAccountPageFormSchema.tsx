import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface MyAccountPageFormInputs {
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  phone: string;
  iAmTheArtist: boolean;
}
export const MyAccountPageFormSchema = Yup.object({
  firstName: Yup.string().required("Firstname is a required field"),
  lastName: Yup.string().required("Lastname is a required field"),
  userName: Yup.string().required("Username is a required field"),
  iAmTheArtist: Yup.string().nullable(),
  email: Yup.string()
    .required("Email is a required field")
    .email("Email must be a valid email")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  phone: Yup.string().required("Phone Number is a required field"),
}).required();
