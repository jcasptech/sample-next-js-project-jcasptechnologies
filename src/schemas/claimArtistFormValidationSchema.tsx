import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface ClaimArtistInputs {
  artistName: string;
  youtubeUrl: string;
  city: string;
  artistBio: string;
  country: string;
  state: string;
}

export const ClaimArtistFormValidateSchema = Yup.object({
  artistName: Yup.string().required("Artist name is required field"),
  youtubeUrl: Yup.string()
    .required("Youtube URL is required field")
    .url("Invalid URL format"),
  city: Yup.string()
    .required("City is required field")
    .max(60, "City must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "City cannot contain only blankspaces"),
  artistBio: Yup.string().required("Artist Bio is required field"),
  country: Yup.string().required("Country is required field"),
  state: Yup.string().required("State is required field"),
}).required();
