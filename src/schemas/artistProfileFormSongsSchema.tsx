import * as Yup from "yup";

interface songsInputs {
  songName: string;
  songSingerName: string;
  url: string;
}
export interface ArtistProfileFormSecondInputs {
  songs: songsInputs[];
  csvFiles: string;
}
export const ArtistProfileFormSecondSchema = Yup.object({
  songs: Yup.array()
    .of(
      Yup.object().shape({
        songName: Yup.string().required("Song name is required"),
        songSingerName: Yup.string().required("Singer name is required"),
        url: Yup.string().required("Singer File is required"),
      })
    )
    .min(1, "Song minimum"),
}).required();
