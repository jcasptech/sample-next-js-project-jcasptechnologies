import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface ArtistSignupFormInputs {
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  password: string;
  referral: string;
}

export const ArtistSignupFormValidateSchema = Yup.object({
  firstname: Yup.string().required("First name is a required field"),
  lastname: Yup.string().required("Last name is a required field"),
  username: Yup.string().required("Username is a required field"),
  email: Yup.string()
    .required("Please enter email id this is mandatory field.")
    .email("Email must be a valid email")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  password: Yup.string()
    .required("Password is a required field")
    .min(8, "short")
    .matches(/[A-Z]/, "uppercase")
    .matches(/\W+/, "special"),
  referral: Yup.string().required("Referral is required field"),
}).required();
