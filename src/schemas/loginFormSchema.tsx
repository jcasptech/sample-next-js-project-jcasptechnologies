import * as Yup from "yup";

export interface LoginFormInputs {
  username: string;
  password: string;
}

export const LoginFormValidateSchema = Yup.object({
  username: Yup.string().required("Username is a required field"),
  password: Yup.string().required("Password is a required field"),
}).required();
