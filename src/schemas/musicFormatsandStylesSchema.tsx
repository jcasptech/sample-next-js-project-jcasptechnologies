import * as Yup from "yup";

interface formatInputs {
  preferredFormats: boolean;
}

export interface ArtistFormatInputs {
  formats: formatInputs[];
}
export const ArtistSchema = Yup.object({
  formats: Yup.array()
    .of(
      Yup.object().shape({
        preferredFormats: Yup.boolean().required(
          "Please enter your Youtube video link "
        ),
      })
    )
    .min(1, "video minimum"),
}).required();
