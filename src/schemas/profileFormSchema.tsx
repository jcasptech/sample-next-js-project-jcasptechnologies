import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface ProfileFormInputs {
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  dateOfBirth: string;
  gender: string;
  height: string;
  weight: string;
  kinRelation: string;
  kinFirstName: string;
  kinLastName: string;
  kinPhone: string;
  kinEmail: string;
  docName: string;
  addressLine1: string;
  addressLine2: string;
  addressLandmark: string;
  addressCity: string;
  addressState: string;
  code: string;
  addressCode: string;
  kinCode: string;
  bio: any;
  services: any;
  number: any;
  docLink: any;
  expireDate: any;
  imageName: any;
  imageLink: any;
}

export const ProviderFormValidateSchema = Yup.object({
  firstName: Yup.string().trim().required("First name is a required field"),
  lastName: Yup.string().trim().required("Last name is a required field"),
  email: Yup.string()
    .email("Email is must be valid.")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email)
    .required("Email is a required field"),
  phone: Yup.string()
    .required("Phone is a required field")
    .matches(/^[0-9]+$/, "Phone must be only digits")
    .min(2, "Phone is too short - should be 2 characters minimum.")
    .max(16, "Phone is too long - should be 16 characters maximum.")
    .matches(/^\s*\S[\s\S]*$/, "Phone cannot contain only blankspaces"),
  dateOfBirth: Yup.string().required("date is required"),
  docName: Yup.string().nullable().required("Please upload document."),
  docLink: Yup.string()
    .nullable()
    .required("Document link is a required field"),
  imageName: Yup.string().nullable().required("Please upload image."),
  imageLink: Yup.string().nullable().required("Image link is a required field"),
  bio: Yup.string().required("Bio is a required field"),
  number: Yup.string().required("Number is a required field"),
  addressLine1: Yup.string()
    .required("Address  line 1 is a required field")
    .max(60, "Address line 1 must be at most 60 characters")
    .matches(
      /^\s*\S[\s\S]*$/,
      "Address Line 1 cannot contain only blankspaces"
    ),
  addressLine2: Yup.string()
    .nullable()
    .max(60, "Address Line 2 must be at most 60 characters")
    .test(
      "blankSpance",
      "Address Line 2 cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  addressLandmark: Yup.string()
    .max(60, "Land Mark must be at most 60 characters")
    .test(
      "blankSpance",
      "Landmark cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  // .required("Landmark is a required field")
  // .matches(/^\s*\S[\s\S]*$/, "Land Mark cannot contain only blankspaces"),
  addressCity: Yup.string()
    .required("City is a required field")
    .max(60, "City must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "City cannot contain only blankspaces"),
  addressState: Yup.string()
    .required("State is a required field")
    .max(60, "State must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "State cannot contain only blankspaces"),
  addressCode: Yup.string()
    .nullable()
    .max(10, "Zip Code must be at most 10 characters")
    .test(
      "blankSpance",
      "Zip Code cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
}).required();

export const PatientFormValidateSchema = Yup.object({
  firstName: Yup.string().trim().required("First name is a required field"),
  lastName: Yup.string().trim().required("Last name is a required field"),
  email: Yup.string()
    .required("Email is a required field")
    .email("Email is must be valid.")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  phone: Yup.string()
    .required("Phone is a required field")
    .matches(/^[0-9]+$/, "Phone must be only digits")
    .min(2, "Phone is too short - should be 2 characters minimum.")
    .max(16, "Phone is too long - should be 16 characters maximum.")
    .matches(/^\s*\S[\s\S]*$/, "Phone cannot contain only blankspaces"),
  dateOfBirth: Yup.string().required("Date of Birth is required field"),
  height: Yup.string()
    .test("len", "Height should be 3 characters maximum", (val: any) =>
      val ? val.length <= 3 : true
    )
    .test("onlyDigit", "Height should be only digit.", (val: any) => {
      return val ? /^[0-9]+$/.test(val) : true;
    }),
  weight: Yup.string()
    .test("len", "Weight should be 3 characters maximum", (val: any) =>
      val ? val.length <= 3 : true
    )
    .test("onlyDigit", "Weight should be only digit.", (val: any) => {
      return val ? /^[0-9]+$/.test(val) : true;
    }),
  kinRelation: Yup.string().required("Relation is a required field"),
  kinFirstName: Yup.string()
    .trim()
    .required("First name is a required field")
    .test(
      "NotEqualTo",
      "Next Of Kin First Name should not be same as user First Name.",
      (val: any, testContext: any) => {
        return val !== testContext.parent.firstName;
      }
    ),
  kinLastName: Yup.string().trim().required("Last name is a required field"),
  kinPhone: Yup.string()
    .required("Phone is a required field")
    .matches(/^[0-9]+$/, "Phone must be only digits")
    .matches(/^\s*\S[\s\S]*$/, "Phone cannot contain only blankspaces")
    .min(2, "Phone is too short - should be 2 characters minimum.")
    .max(16, "Phone is too long - should be 16 characters maximum.")
    .test(
      "NotEqualTo",
      "Next Of Kin Phone should not be same as user Phone.",
      (val: any, testContext: any) => {
        return val !== testContext.parent.phone;
      }
    ),
  kinEmail: Yup.string()
    .required("Email is a required field")
    .email("Email is must be valid.")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email)
    .test(
      "NotEqualTo",
      "Next Of Kin Email should not be same as user Email.",
      (val: any, testContext: any) => {
        return val !== testContext.parent.email;
      }
    ),
  kinCode: Yup.string().required("Code is a required field"),
  addressLine1: Yup.string()
    .required("Address  line 1 is a required field")
    .max(60, "Address Line 1 must be at most 60 characters")
    .matches(
      /^\s*\S[\s\S]*$/,
      "Address Line 1 cannot contain only blankspaces"
    ),
  addressLine2: Yup.string()
    .nullable()
    .max(60, "Address Line 2 must be at most 60 characters")
    .test(
      "blankSpance",
      "Address Line 2 cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  addressLandmark: Yup.string()
    // .required("Landmark is a required field")
    .max(60, "Land Mark must be at most 60 characters")
    .test(
      "blankSpance",
      "Landmark cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
  addressCity: Yup.string()
    .required("City is a required field")
    .max(60, "City must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "City cannot contain only blankspaces"),
  addressState: Yup.string()
    .required("State is a required field")
    .max(60, "State must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "State cannot contain only blankspaces"),
  addressCode: Yup.string()
    .nullable()
    .max(10, "Zip Code must be at most 10 characters")
    .test(
      "blankSpance",
      "Zip Code cannot contain only blankspaces",
      (val: any) => {
        return val ? /^\s*\S[\s\S]*$/.test(val) : true;
      }
    ),
});
