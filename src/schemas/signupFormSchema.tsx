import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface SignupFormInputs {
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
}

export const SignupFormValidateSchema = Yup.object({
  firstName: Yup.string().required("First name is a required field"),
  lastName: Yup.string().required("Last name is a required field"),
  userName: Yup.string().required("Username is a required field"),
  email: Yup.string()
    .required("Please enter email id this is mandatory field.")
    .email("Email must be a valid email")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  password: Yup.string()
    .required("Password is a required field")
    .min(8, "short")
    .matches(/[A-Z]/, "uppercase")
    .matches(/\W+/, "special"),
  confirmPassword: Yup.string()
    .required("Confirm Password is a required field")
    .oneOf(
      [Yup.ref("password")],
      "Password and confirm password does not match"
    ),
}).required();
