import { REGEX, REGEX_VALIDATION_MESSAGE } from "src/libs/constants";
import * as Yup from "yup";

export interface ArtistProfileFormInputs {
  artistElements: string[];
  artistName: string;
  artistPhonenumber: string;
  artistEmailId: string;
  profileImage: string;
  artistLocaleCity: string;
  artistLocaleState: string;
  artistLocaleCountry: string;
  youtubeId: string;
  paymentPreference: "paypal" | "venmo";
  paypalId: string;
  vemeoId: string;
  artistBio: string;
  JCaspVanityTag: string;
  songName: any;
  SongSingerName: string;
  vemeoLink: string;
  facebookPage: string;
  facebookUsername: string;
  instagramUsername: string;
  website: string;
  liveStreamUrl: string;
  patreonLink: string;
  spotifyLink: string;
  about: string;
  youtubeVideoLinks: any;
  photos: any;
  audioFiles: any;
  youtubeChannel: string;
}

export const ArtistProfileFormFirstSchema = Yup.object({
  profileImage: Yup.string().required(
    "Artist profile image is a required field"
  ),
  artistElements: Yup.array()
    .max(10, "Please select at most ten option")
    .min(1, "Please select at least one option")
    .required("Location is required field"),
  artistName: Yup.string().trim().required("Artist Name is required field"),

  paymentPreference: Yup.string().oneOf(["paypal", "venmo"]),
  paypalId: Yup.string()
    .trim()
    .when("paymentPreference", {
      is: "paypal",
      then: Yup.string().required("Paypal id is a required field"),
    }),
  vemeoId: Yup.string()
    .trim()
    .when("paymentPreference", {
      is: "venmo",
      then: Yup.string().required("Venmo id is a required field"),
    }),
  JCaspVanityTag: Yup.string()
    .trim()
    .required("JCasp Vanity Tag is a required field"),
  artistEmailId: Yup.string()
    .required("Email is a required field")
    .email("Email is must be valid.")
    .matches(REGEX.email, REGEX_VALIDATION_MESSAGE.email),
  artistPhonenumber: Yup.string()
    .required("Phone Number is a required field")
    .matches(/^[0-9]+$/, "Phone must be only digits")
    .min(10, "Phone is too short - should be 10 characters minimum.")
    .max(12, "Phone is too long - should be 12 characters maximum.")
    .matches(/^\s*\S[\s\S]*$/, "Phone cannot contain blankspaces"),
  youtubeId: Yup.string()
    .nullable()
    .required("Youtube video url is required field")
    .matches(
      /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
      "Please provide valid Youtube video url"
    )
    .url("Invalid URL format"),
  artistBio: Yup.string().required("Bio is a required field"),
  artistLocaleState: Yup.string()
    .required("State is a required field")
    .max(60, "State must be at most 60 characters")
    .matches(/^\s*\S[\s\S]*$/, "State cannot contain only blankspaces"),
  artistLocaleCity: Yup.string()
    .required("City is a required field")
    .matches(/^\s*\S[\s\S]*$/, "City cannot contain only blankspaces"),
  artistLocaleCountry: Yup.string()
    .required("Country is a required field")
    .max(60, "Country must be at most 60 characters"),
}).required();
