export interface TableFilterObject {
  text: string;
  value: string;
}

export interface TableColumnsProps {
  title: string;
  dataIndex: string;
  sorter?: boolean;
  render?: any; // TODO:
  width?: string;
  filters?: TableFilterObject[];
  className?: string;
}
