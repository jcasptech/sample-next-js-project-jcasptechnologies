import { RootState } from "@redux/reducers";
import { fetchLoginUser, LoginState } from "@redux/slices/auth";
import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useDispatch, useSelector } from "react-redux";
import { deepClone } from "./helpers";

const useLoginStatus = (loginState: LoginState) => {
  const [isLogin, setIsLogin] = useState(false);
  const loginUser = useSelector((state: RootState) => state.loginUser);
  const [, setCookie]: any = useCookies(["authApp"]);
  const dispatch = useDispatch();

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (
      loginUser?.isLogin === true &&
      loginUser?.data &&
      loginUser?.data?.user
    ) {
      const userData = deepClone(loginUser);

      const tmpUser = {
        objectId: userData?.data?.user?.objectid,
      };
      const payload: any = {
        user: tmpUser,
        accessToken: loginUser?.data?.accessToken,
        userType: loginUser?.data?.userType,
      };

      setCookie("authApp", JSON.stringify(payload), { path: "/" });
      setIsLogin(true);
    }
  }, [loginUser]);

  useEffect(() => {
    if (loginState?.data?.accessToken) {
      dispatch(
        fetchLoginUser({
          Authorization: `Bearer ${loginState?.data?.accessToken}`,
        })
      );
    }
    //return () => {};
  }, [loginState]);

  return isLogin;
};

export default useLoginStatus;
