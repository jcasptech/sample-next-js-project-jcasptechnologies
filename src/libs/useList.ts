import { useState } from "react";
import { DEFAULT_TABLE_LIMIT, SHOW_TYPES } from "./constants";
import { deepClone } from "./helpers";

type useListProps = {
  queryParams?: {
    take?: number;
    skip?: number;
    orderBy?: string;
    id?: string;
    range?: string;
    venueId?: string;
    artistId?: string;
    eventId?: string;
    userEmail?: string;
    include?: any;
    isClaimed?: string;
    search_column?: string[];
    search?: string;
    email?: string;
    type?: string;
    content?: string;
    orderByColumn?: string;
    shows?: "TODAY" | "UPCOMING";
    location?: {
      latitude: any;
      longitude: any;
    };
    latitude?: number;
    longitude?: number;
    calendarQuery?: {
      startDate: string;
      endDate: string;
      artistId?: string;
      orderBy?: string;
      shows?: boolean;
      notes?: boolean;
      upcoming?: boolean;
      completed?: boolean;
      canceledByHost?: boolean;
      removed?: boolean;
      declined?: boolean;
      paid?: boolean;
      unpaid?: boolean;
      open?: boolean;
      submitted?: boolean;
      nearMe?: boolean;
      all?: boolean;
      latitude?: number;
      longitude?: number;
      venueId?: string;
      isGodMode?: boolean;
    };
    proceeds?: number[];
    dateRange?: [string, string];
    tags?: string;
  };
};

export const useList = ({
  queryParams = {
    take: DEFAULT_TABLE_LIMIT,
    skip: 0,
  },
}: useListProps) => {
  const [apiParam, setApiParam] = useState({ ...queryParams });

  const handleOnTableChange = ({ pagination, sorter }: any) => {
    setApiParam((d) => {
      const temp = deepClone(d);
      if (pagination) {
        temp["skip"] = pagination?.skip;
      }
      if (sorter?.orderBy) {
        temp["orderBy"] = sorter?.orderBy;
      }
      const newValue = deepClone(temp);
      return newValue;
    });
  };

  return {
    apiParam,
    setApiParam,
    handleOnTableChange,
  };
};

export default useList;
