import { useEffect, useRef } from "react";

const useFirstEffect = (fn: () => void, data: any) => {
  const isFirstRun = useRef(true);
  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false;
      return;
    }
    fn();
    // TODO: How to return effect?
  }, data);
};

export default useFirstEffect;
