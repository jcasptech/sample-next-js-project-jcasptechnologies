import { SITE_URL } from "./constants";

export const SEO_DETAILS = {
  image: `${SITE_URL}/images/general/og-image.jpg`,
  twitter: "@JCaspmusic",
  phone: "932936250",
  googleplay: "com.JCasp.fender",
  locale: "en_US",
  siteName: "JCasp",
};

export const SEO_DETAILS_PAGES: any = {
  "/": {
    title: "Local Shows on JCasp",
    description:
      "Find local, live music happening tonight on our shows calendar",
    url: `${SITE_URL}`,
  },
  "/browse": {
    title: "Artist and Venues | JCasp",
    description: "Find artists and venues on JCasp",
    url: `${SITE_URL}/browse`,
  },
  "/services": {
    title: "Services | JCasp",
    description:
      "Welcome to our live music booking form! Whether you're planning a special event or looking to host live music at your venue, we're here to help.",
    url: `${SITE_URL}/services`,
  },
  "/terms-of-use": {
    title: "Terms Of Use | JCasp",
    description: "Find term of uses JCasp.",
    url: `${SITE_URL}/terms-of-use`,
  },
  "/privacy-policy": {
    title: "Privacy Policy | JCasp",
    description: "Find Privacy Policy of JCasp.",
    url: `${SITE_URL}/privacy-policy`,
  },
  "/contact-us": {
    title: "Contact Us | JCasp",
    description: "Contact to  JCasp.",
    url: `${SITE_URL}/contact-us`,
  },
  "/faq": {
    title: "Frequently asked questions | JCasp",
    description: "Find Frequently asked questions on JCasp.",
    url: `${SITE_URL}/faq`,
  },
  "/chat-gpt-plugin-terms-of-use": {
    title: "Chat GPT Terms Of Use | JCasp",
    description: "Find Chat GPT term of uses JCasp.",
    url: `${SITE_URL}/chat-gpt-plugin-terms-of-use`,
  },
  "/login": {
    title: "Sign in | JCasp",
    description: "Sign in on JCasp.",
    url: `${SITE_URL}/login`,
  },
  "/signup": {
    title: "Sign up | JCasp",
    description: "Sign up on JCasp.",
    url: `${SITE_URL}/signup`,
  },
  "/forgot-password": {
    title: "Forgot Password | JCasp",
    description: "Forgot Password on JCasp.",
    url: `${SITE_URL}/forgot-password`,
  },
  "/JCasp-light": {
    title: "Join as a venue admin | JCasp Lite | JCasp",
    description: "Join as venue admin on JCasp.",
    url: `${SITE_URL}/JCasp-light`,
  },
  "/pricing": {
    title: "JCasp Lite Pricing | JCasp",
    description: "Join as venue admin pricing on JCasp.",
    url: `${SITE_URL}/pricing`,
  },
};
