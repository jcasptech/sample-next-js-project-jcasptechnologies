import { showsObject } from "@redux/slices/artistShow";
import { ShowsObject } from "@redux/slices/shows";
import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { DateTime } from "luxon";
import moment from "moment";
import { NextPageContext } from "next";
import Router from "next/router";
import UAParser from "ua-parser-js";
import {
  acceptedCertificateImage,
  IS_ENABLED_ENCRYPTION,
  PAYLOAD_SECRET_KEY,
} from "./constants";
import CryptoJS from "crypto-js";

let axiosInstance: AxiosInstance | null = null;

const deepClone = (obj: any) => {
  return JSON.parse(JSON.stringify(obj));
};

const API = (force = false): AxiosInstance => {
  if (axiosInstance && !force) {
    return axiosInstance;
  }

  axiosInstance = axios.create({
    baseURL: `${process.env.NEXT_API_ENDPOINT}`,
  });

  return axiosInstance;
};

const fetch: (config: AxiosRequestConfig) => Promise<any> = async (config) => {
  try {
    const response = await API().request(config);

    let res: any = response;
    if (res?.data) {
      res = {
        data: IS_ENABLED_ENCRYPTION
          ? CryptoJSAesJson.decrypt(res.data)
          : res.data,
      };
    }

    if (res?.data?.responseData) {
      res = {
        data: res?.data?.responseData,
      };
    }

    return res?.data;
  } catch (e: any) {
    if (
      e?.error?.response &&
      e?.error?.response?.data &&
      e?.error?.response?.data?.message
    ) {
      throw new Error(
        e.error.response.data.message || "Bad response from server"
      );
    } else {
      throw new Error(e?.error?.message || "Bad response from server");
    }
  }
};

const removeEmpty = (obj: any) => {
  for (const propName in obj) {
    if (obj[propName] === null || obj[propName] === undefined) {
      delete obj[propName];
    }
  }
  return obj;
};

const getDescendantProp = (obj: any, desc: any) => {
  const arr = desc.split(".");
  while (arr.length && (obj = obj[arr.shift()]));
  return obj;
};

const nextRedirect = ({
  ctx = {},
  location,
}: {
  ctx?: NextPageContext | any;
  location: string;
}) => {
  if (typeof window === "undefined" && ctx?.res) {
    ctx.res.writeHead(301, {
      Location: location,
    });
    ctx.res.end();
  } else {
    Router.replace(location);
  }
};

const shortStr = (string: string, limit = 60) => {
  return string && string.length > limit
    ? `${string.substring(0, limit - 1)}...`
    : string;
};

const getUserAgent = (ua: any) => {
  if (ua) {
    let userAgentStr = "";
    const UA: any = new UAParser(ua);
    const browser = UA.getBrowser();
    const os = UA.getOS();

    if (browser && browser.name) {
      userAgentStr = browser.name;
    }
    if (os && os.name) {
      userAgentStr = `${userAgentStr} - ${os.name}`;
    }
    return userAgentStr;
  }
  return "-";
};

const hideEmail = (email: string, count = 4, replaceWith = "*") => {
  if (!email) {
    return null;
  }
  const hiddenEmail = email.replace(/(.{0})(.*)(?=@)/, (gp1, gp2, gp3) => {
    for (let i = 0; i < count; i++) {
      gp2 += replaceWith;
    }
    return `${gp2}${gp3.substring(count)}`;
  });
  return hiddenEmail;
};

const hidePhone = (phone: string, count = 4, replaceWith = "*") => {
  if (!phone) {
    return null;
  }
  const hiddenPhone = phone.replace(/\d(?=\d{4})/g, replaceWith);
  return hiddenPhone;
};

const getDateWithTimeAgo = (date: any) => {
  const startDate = DateTime.local();
  const diffInDays = date.diff(startDate, "days");
  const days = diffInDays.toObject().days;
  if (Math.round(days) > -3) {
    return DateTime.fromISO(date).toRelative();
  }
  return DateTime.fromISO(date).toFormat("dd LLLL");
};

const loadScript = (url: string, callback: any, withoutCheck?: boolean) => {
  if (!document.getElementById("googleMap") || withoutCheck) {
    const script: any = document.createElement("script"); // create script tag
    script.type = "text/javascript";
    script.id = "googleMap";

    // when script state is ready and loaded or complete we will call callback
    if (script.readyState) {
      script.onreadystatechange = function () {
        if (
          script.readyState === "loaded" ||
          script.readyState === "complete"
        ) {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      script.onload = () => callback();
    }

    script.src = url; // load by url
    document.getElementsByTagName("head")[0].appendChild(script); // append to head
  } else {
    callback();
  }
};

const loadBranchScript = (url: string, callback: any) => {
  if (!document.getElementById("BranchIO")) {
    const script: any = document.createElement("script"); // create script tag
    script.type = "text/javascript";
    script.id = "BranchIO";

    // when script state is ready and loaded or complete we will call callback
    if (script.readyState) {
      script.onreadystatechange = function () {
        if (
          script.readyState === "loaded" ||
          script.readyState === "complete"
        ) {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      script.onload = () => callback();
    }

    script.src = url; // load by url
    document.getElementsByTagName("head")[0].appendChild(script); // append to head
  } else {
    callback();
  }
};

const getCapitalize = (string: string) => {
  const words = string.split(" ");

  for (let i = 0; i < words.length; i++) {
    words[i] = words[i][0].toUpperCase() + words[i]?.substring(1);
  }

  return words.join(" ");
};

const imageExists = (image_url: string) => {
  if (acceptedCertificateImage.includes(image_url?.split(".").pop())) {
    return true;
  } else {
    return false;
  }
};

const getFormatReview = (count: number) => {
  if (count) {
    const suffixes = ["", "k", "M", "B", "T"]; // Add more suffixes if needed
    const suffixNum = Math.floor(("" + count).length / 3);
    let shortNumber: any = parseFloat(
      (suffixNum != 0 ? count / Math.pow(1000, suffixNum) : count).toPrecision(
        2
      )
    );
    if (shortNumber % 1 !== 0) {
      shortNumber = shortNumber.toFixed(1);
    }

    return shortNumber + suffixes[suffixNum];
  } else {
    return 0;
  }
};

const capitalizeString = (string: string) => {
  var words = string ? string.split(" ") : [];
  for (var i = 0; i < words.length; i++) {
    var word = words[i];
    words[i] = word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  }
  return words?.join(" ");
};

const uppercaseFirst = (string: string) => {
  return string
    ? string?.charAt(0)?.toUpperCase() + string?.slice(1)?.toLowerCase()
    : "-";
};

const universal_link = (data: any, type: any) => {
  const url =
    process.env.NODE_ENV === "production"
      ? `https://jcaspdev.jcasptechnologies.com/${type}=${
          data?.vanity ? data.vnity : data?.id
        }`
      : `http://localhost:3001?${type}=${
          data?.vanity ? data.vanity : data?.objectId
        }`;
  return url;
};

const google_calendar_url = (show: ShowsObject) => {
  const loc = show?.venue?.address || "TBD";
  const tz = show?.venue?.timeZone || "America/Los_Angeles";
  const start = show?.startDate || new Date();

  const startTime = moment.utc(start).format("YYYYMMDDTHHmmss\\Z");
  const endTime = moment
    .utc(start)
    .add(2, "hours")
    .format("YYYYMMDDTHHmmss\\Z");

  return `https://calendar.google.com/calendar/r/eventedit?text=${
    show?.name
  }&location=${loc}&ctz=${tz}&sf=true&details=${universal_link(
    show,
    "show"
  )}&dates=${startTime}/${endTime}`;
};

const downloadFile = (url: string, fileName: string) => {
  // Replace 'fileUrl' with the actual URL of the file you want to download
  const fileUrl = url;

  // Create a temporary link element
  const link = document.createElement("a");
  link.href = fileUrl;

  // Set the 'download' attribute to specify the filename
  if (fileUrl.includes("pdf")) {
    link.target = "_blank";
  }
  link.setAttribute("download", fileName);

  // Append the link to the document body
  document.body.appendChild(link);

  // Programmatically trigger the download
  link.click();

  // Cleanup: remove the link from the document body
  document.body.removeChild(link);
};

const getYoutubeId = (url: string) => {
  let video_id = url?.split("v=")[1];
  const ampersandPosition = video_id?.indexOf("&");
  if (ampersandPosition != -1) {
    video_id = video_id?.substring(0, ampersandPosition);
  }

  if (video_id) {
    const ampersandPosition = video_id.indexOf("&");
    if (ampersandPosition != -1) {
      video_id = video_id.substring(0, ampersandPosition);
    }

    if (video_id) {
      return video_id;
    } else {
      return null;
    }
  } else {
    return null;
  }
};

const setYoutubeId = (id: string) => {
  if (id) {
    return `https://www.youtube.com/embed/${id}?v=${id}`;
  } else {
    return null;
  }
};

const getVimeoId = (url: string) => {
  const match = /vimeo.*\/(\d+)/i.exec(url);
  // If the match isn't null (i.e. it matched)
  if (match) {
    return match[1];
  } else {
    return null;
  }
};

const getVideoUrl = (url: string) => {
  let videoLink = "";

  if (url.includes("youtube")) {
    videoLink = `https://www.youtube.com/embed/${getYoutubeId(url)}`;
  } else if (url.includes("vimeo")) {
    videoLink = `https://player.vimeo.com/video/${getVimeoId(url)}`;
  }
  return videoLink;
};

const CryptoJSAesJson = {
  /**
     * Encrypt any value
      @param {} value
     * @param {string} password
     * @return {string}
     */
  encrypt: function (value: any) {
    return CryptoJS.AES.encrypt(JSON.stringify(value), PAYLOAD_SECRET_KEY, {
      format: CryptoJSAesJson,
    }).toString();
  },
  /**
       * Decrypt a previously encrypted value
       * @param {string} jsonStr
       * @param {string} password
        @return {}
      */
  decrypt: function (jsonStr: any) {
    let string = jsonStr;
    if (jsonStr instanceof Object) {
      string = JSON.stringify(jsonStr);
    }

    return JSON.parse(
      CryptoJS.AES.decrypt(string, PAYLOAD_SECRET_KEY, {
        format: CryptoJSAesJson,
      }).toString(CryptoJS.enc.Utf8)
    );
  },
  /**
   * Stringify cryptojs data
   * @param {Object} cipherParams
   * @return {string}
   */
  stringify: function (cipherParams: any) {
    var j: any = { ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64) };
    if (cipherParams.iv) j.iv = cipherParams.iv.toString();
    if (cipherParams.salt) j.s = cipherParams.salt.toString();
    return JSON.stringify(j).replace(/\s/g, "");
  },
  /**
       * Parse cryptojs data
       * @param {string} jsonStr
        @return {}
      */
  parse: function (jsonStr: any) {
    var j = JSON.parse(jsonStr);
    var cipherParams = CryptoJS.lib.CipherParams.create({
      ciphertext: CryptoJS.enc.Base64.parse(j.ct),
    });
    if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
    if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
    return cipherParams;
  },
};

export {
  fetch,
  API,
  deepClone,
  removeEmpty,
  getDescendantProp,
  nextRedirect,
  shortStr,
  getUserAgent,
  getDateWithTimeAgo,
  hideEmail,
  hidePhone,
  loadScript,
  getCapitalize,
  imageExists,
  getFormatReview,
  capitalizeString,
  google_calendar_url,
  universal_link,
  downloadFile,
  getVideoUrl,
  getYoutubeId,
  setYoutubeId,
  loadBranchScript,
  uppercaseFirst,
  CryptoJSAesJson,
};
