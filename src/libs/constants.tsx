export const TOAST_MESSAGE_DELAY = 5;

export const STATUS: any = {
  DISABLED: false,
  ENABLED: true,
};

export const DATE_TIME_FORMAT = "EEEE, MMMM d, yyyy h:mm a ZZZZ";
export const DATE_FORMAT = "MM-DD-YYYY";
export const DEFAULT_TIME_ZONE = "America/Los_Angeles";
export const SITE_URL = "https://jcaspdev.jcasptechnologies.com";
export const BASE_URL_NAME: any = {
  url: "http://localhost:3001",
};

export const SHOW_TYPES: any = {
  TODAY: "TODAY",
  UPCOMING: "UPCOMING",
};

export const TYPES_OF_ACTION_USERS: any = {
  artistSubscribe: "You must be logged in to subscribe an artist.",
  artistMessage: "You must be logged in to write a artist message.",
  artistShoutouts: "You must be logged in to shoutout this artist",
  venueSubscribe: "You must be logged in to subscribe venue.",
};

export const SUCCESS_MESSAGES: any = {
  changePassword: "Password changed successfully.",
  signUp: "Signup successfully",
  resendOtp: "OTP sent successfully",
  resetPassword: "Reset password successfully.",
  reScheduleAppointment: "Appointment re-schedule successfully.",
  cancelAppointment: "Appointment cancelled successfully.",
  noShowAppointment: "Appointment no show successfully.",
  deletedPaymentMethod: "Payment method deleted successfully.",
  reviewSucess: "Review added successfully.",
  medicalDocument: "Medical document added successfully.",
  prescriptionExtensionSucess: "Prescription extension requested successfully.",
  prescriptionExtensionSubmitSucess: "Prescription extended successfully.",
  subscribeWebsite: "Subscribed successfully.",
  resetPasswordRequest:
    "A reset link has been sent to your email! You should receive an email from us shortly.",
  requiredLoginSubscribe: "You must be logged in to subscribe an artist.",
  unsubscribeWebsite: "Unsubscribe successfully.",
  requiredLoginMessage: "You must be logged in to write a artist message.",
  copyToClipboard: "Copied To ClipBoard",
  sendArtistMessage: "Artist message sent successfully.",
  requiredLoginRatingMessage: "Please log in to shoutout this artist",
  sendArtistRatingMessage: "Thank you for the shoutout this artist!",
  requiredLoginSubscribeVenue: "You must be logged in to subscribe venue.",
  subscribeArtist: "Artist subscribe successfully.",
  unsubscribeArtist: "Artist unsubscribe successfully.",
  subscribeVenue: "Venue subscribe successfully.",
  unsubscribeVenue: "Venue unsubscribe successfully.",
  artistReviewMessage: "No Artist Review",
  somethingWentWorng: "something went wrong!",
  answerSubmit: "Answer submitted successfully",
  inputFileType:
    "Invalid file type and file size must bi Minimum Dimensions : 750px X 586px",
  expeditedPayment: "The expedited payment has been requested.",
  createNotes: "Note has been added.",
  deleteNotes: "Note has been deleted.",
  submitGig:
    "Success! You will be notified via email if the host contacts you for this event.",
  updateShow: "Show has been updated.",
  profileImage:
    "Invalid file type and file size must be Minimum Dimensions : 750px X 586px",
  artistElements: "Select maximum 10 elements.",
  alreadyArtistElements: "Already exist element.",
  requestFeature: "Feature request sent successfully",
  landingPageSuccessMessage:
    "Thank you for contacting us, we will reach out to you as soon as possible.",
  createArtistSucsess: "Artist Created successfully",
  editArtistSuccess: "Artist updated successfully",
  userUpdateSuccess: "User updated successfully",
  deleteShow: "Show has been deleted",
  updateVenueDetails: "Venue Details updated successfully",
  deleteShows: "Show deleted successfully",
  ClaimArtistSuccess: "Successfully claimed artist",
  withdrawSubmissionSuccess: "Successfully withdraw submission.",
  fileDeletedSuccess: "File Deleted Successfully",
  fileValidError: "Please select valid file",
  ArtistDeactivatedSuccess: "Artist Deactivated Successfully",
  ArtistActivatedSuccess: "Artist Activated Successfully",
  ArtistRemovedSuccess: "Artist Removed Successfully",
  downloadAPPSuccess: "Successfully sent downloaded link SMS.",
  postingConfirmSuccess: "Posting Confirm Successfully",
  postingPromotSuccess: "Posting Promote Successfully",
  showAddedSuccess: "Show Added Successfully",
  eventUpdateSuccess: "Event Update Successfully",
  eventTitleUpdateSuccess: "Title Updated Successfully",
  exportInvoice: "Export Invoice Data Sent Successfully",
  exportEvent: "Export Event Details sent Successfully",
  exportSchedule: "Export Schedule Successfully",
  eventCancelledSuccess: "Event Cancelled Successfully",
  postingUpdateSuccess: "Posting Updated Successfully",
  postingNoteUpdateSuccess: "Posting Note Updated Successfully",
  postingCreatedSuccess: "Posting Created Successfully",
  eventCreatedSuccess: "Event Created Successfully",
  messageApprovedSuccess: "Message Approved Successfully",
  messageDeclinedSuccess: "Message Declined Successfully",
  claimArtistCreatedSuccess: "Claim Artist Created Successfully",
  embedCodeCreateSuccess: "Embed code Created Successfully",
  embedCodeDeletedSuccess: "Embed Code Deleted Successfully",
  postingConfirmedSuccess: "Posting Confirmed Successfully",
  previousMonthInvoiceExportSuccess:
    "Previous mounth Invoice data sent Successfully",
  currentMonthInvoiceExportSuccess:
    "Current mounth Invoice data sent Successfully",
  approvedArtistsInvoiceExportSuccess:
    "Approved Artist Invoice Data sent Successfully",
  allUsersInvoiceExportSuccess: "All Users Invoice Data sent Successfully",
  approvedVenuesExportSuccess: "Approved venues data sent successfully",
  validEventsExportSuccess: "Valid Events data sent successfully",
  artistSignUpSuccess: "Artist Sign Up Successfully",
  contactMessageSuccess: "Contact message sent Successfully",
  exportedDataSuccess: "Exported data sent Successfully",
  tagApprovedSuccess: "Tag Approved Successfully",
  artistUpdateError: "Artist does not updated, please try again",
  postingCreateError: "Failed to create posting, please try again",
  profileUpdateError: "Failed to update profile, please try again",
  recommendedArtistSuccess: "Successfully changed recommended artist.",
  hideBidSuccess: "Successfully hide application.",
  scoreUpdateSuccess: "Score Updated Successfully",
  generatesScoreSuccess: "Successfully started score calculations.",
};

export const DEFAULT_TABLE_LIMIT = 8;

export const DEFAULT_TABLE_PAGINATION_OPTIONS = ["10", "20", "30"];

export const countries: any = {
  GH: "+233",
};

export const LOGIN_USER_TYPE = {
  ADMIN: "ADMIN",
  MANAGER: "MANAGER",
  USER: "USER",
  VENUE_ADMIN: "VENUE_ADMIN",
};
export const REGEX = {
  phone: /^(\(\d{3}\)|\d{3})[-. ]?\d{3}[-. ]?\d{4}$/, //eslint-disable-next-line
  email: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/, //eslint-disable-next-line
};

export const REGEX_VALIDATION_MESSAGE = {
  phone: "Invalid phone number.",
  email: "Invalid email address.",
};

export const AVAILABILITY_DAY_ENUM: any = {
  MONDAY: "MONDAY",
  TUESDAY: "TUESDAY",
  WEDNESDAY: "WEDNESDAY",
  THURSDAY: "THURSDAY",
  FRIDAY: "FRIDAY",
  SATURDAY: "SATURDAY",
  SUNDAY: "SUNDAY",
};

export const dateFormat = "YYYY-MM-DD";

export const USER_GENDER: any = {
  MALE: "Male",
  FEMALE: "Female",
  OTHER: "Other",
};

export const acceptedCertificateTypes: any =
  ".pdf,.jpg,.jpeg,.png,.PNG,.JPEG,.JPG,.PDF";
export const acceptedCertificateDocumentTypes: any = ".pdf,.PDF";
export const acceptedCertificateImageTypes: any =
  ".jpg,.jpeg,.png,.PNG,.JPG,.JPEG";

export const acceptedCertificateFile: any = ["pdf", "PDF"];

export const acceptedCertificateImage: any = [
  "jpg",
  "jpeg",
  "png",
  "JPG",
  "JPEG",
  "PNG",
];

export const acceptedAudioFiles: any = [
  "mp3",
  "wav",
  "aac",
  "ogg",
  "aiff",
  "m4a",
  "amr",
];
export const acceptedCSVFiles: any = ["csv", "pdf"];

export const GOOGLE_API_KEY = "AIzaSyD8HI9QlW1DTnU5pD0J0i8WeWqDT_PVcJ8";
export const FACEBOOK_APP_ID = "1677047019378643";
export const GOOGLE_RECAPTCHA_SITE_KEY =
  "6Lez4H0lAAAAAHrS9m_tlGr19FVB5YoUTabyd4qy";

export const PERMISSIONS: any = {
  USER: [
    "create_posting",
    "active_posting",
    "artists_subscriptions",
    "venues_subscriptions",
  ],
  MANAGER: [
    "edit_artist",
    "create_artist",
    "artist_messages",
    "users_shows",
    "artist_calendar",
    "open_posting",
    "artist_posting",
    "artist_submissions",
  ],
  ADMIN: [
    "create_posting",
    "active_posting",
    "confirm_posting",
    "artist_messages",
    "claimed_artists",
    "users_shows",
    "admin_calendar",
    "user_questions",
    "embed_code",
    "artist_elements",
    "venue_listing",
    "featured_artists",
    "featured_venues",
  ],
  VENUE_ADMIN: [
    "create_posting",
    "active_posting",
    "confirm_posting",
    "venue_calendar",
    "venue_listing",
    "users_shows",
  ],
  COMMON: [
    "dashboard",
    "request_new_feature",
    "account_settings",
    "change_password",
  ],
};

export const PAYLOAD_SECRET_KEY = "TEST_SECRET_KEY";
export const IS_ENABLED_ENCRYPTION = false;
