import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import FAQScene from "./faqScene";

const FAQContainer = () => {
  return (
    <>
      <SEO
        {...{
          pageName: "/faq",
        }}
      />
      <FAQScene />
    </>
  );
};

FAQContainer.Layout = MainLayoutComponent;

export default FAQContainer;
