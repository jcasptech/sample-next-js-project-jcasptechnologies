import ApplicationDetalis from "@components/applicationDetails/applicationDetalis";
import NewsLetterSection from "@components/newsLetter";
import QuestionsAnswer from "./faqQuestionAnswer";
import faqStyles from "./faqStyles.module.scss";

const FAQScene = () => {
  return (
    <>
      <section className={`section-pad ${faqStyles.pageHeader}`}>
        <div className="container-fluid">
          <div className="row m-0">
            <div className="col-12 pHead">
              <h1>Top Questions About JCasp</h1>
            </div>
          </div>
        </div>
      </section>
      <QuestionsAnswer />
      <NewsLetterSection />
    </>
  );
};
export default FAQScene;
