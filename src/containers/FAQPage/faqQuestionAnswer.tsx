import faqStyles from "./faqStyles.module.scss";
import Link from "next/link";
import { Collapse } from "antd";
const QuestionsAnswer = () => {
  return (
    <>
      <section className="section-pad">
        <div className="container-fluid Faqs">
          <div className={`row m0 ${faqStyles.faq_box2}`}>
            <div className="col-12 col-sm-12">
              <h2>General</h2>
              <Collapse bordered={false} accordion expandIconPosition="end">
                <Collapse.Panel header="What is JCasp?" key="1">
                  <p>
                    JCasp is a network and platform that connects working
                    musicians, venues, and live music enthusiasts.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel
                  header="Do artists pay a fee to join JCasp?"
                  key="2"
                >
                  <p>
                    Absolutely not! Artists can join JCasp for free. We would
                    shut down the platform before charging artists.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="How does JCasp make money?" key="3">
                  <p>
                    JCasp offers services to busy restaurants and businesses
                    that need help managing live music, for a fee. This is how
                    the company began. Currently, our focus is on building the
                    network the right way for the right reasons, trusting that
                    the rest will follow.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Does JCasp use AI?" key="4">
                  <p>
                    Yes! JCasp proudly features the first Chat GPT plugin for
                    live music. You can inquire about live music in your area or
                    get recommendations for local artists. We are working on
                    several more applications to enhance the experience through
                    our custom AI tools.
                  </p>
                </Collapse.Panel>
              </Collapse>
            </div>
          </div>

          <div className={`row m0 ${faqStyles.faq_box2}`}>
            <div className="col-12 col-sm-12">
              <h2>Artists</h2>
              <Collapse accordion bordered={false} expandIconPosition="end">
                <Collapse.Panel header="Creating a Profile" key="1">
                  <p>
                    To create a profile, visit the Artist signup page. A
                    complete profile includes:
                  </p>
                  <ul>
                    <li>Artist photos</li>
                    <li>Video links (YouTube or Vimeo)</li>
                    <li>Short Bio</li>
                    <li>Song List</li>
                    <li>Links to your website and social pages</li>
                    <li>Managing Multiple Artist Profiles</li>
                    <li>
                      Whether you have a solo act, full band, or DJ, you can
                      manage multiple profiles from the same account or add
                      elements like “DJ” or “Full Band” to your primary profile.
                    </li>
                  </ul>
                </Collapse.Panel>
                <Collapse.Panel header="Using the JCasp App" key="2">
                  <p>
                    The JCasp App offers artist tools for submitting open gigs
                    and adding upcoming shows. It also helps users discover
                    nearby restaurants and venues with live music.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Submitting for Gigs" key="3">
                  <p>
                    Any approved artist can submit to open gig postings without
                    fees. Some gigs are managed by JCasp (JCasp PRO), while
                    others are from restaurants, event planners, and private
                    hosts.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="JCasp PRO Bookings" key="4">
                  <p>
                    Confirmed JCasp PRO bookings come with an email and text
                    alert containing relevant information and an advance sheet
                    if applicable. PRO bookings also include a payment
                    guarantee, detailed gig information, and direct contact with
                    the client or host.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel
                  header="Why are there no gig postings in my area?"
                  key="5"
                >
                  <p>
                    Gig postings may be limited outside of California, but we
                    hope to expand soon!
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Profile Visibility" key="6">
                  <p>
                    Profiles are sorted based on completeness and upcoming show
                    activity. Keep your profile updated and add your upcoming
                    shows for better visibility.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Adding Upcoming Shows" key="7">
                  <p>
                    Add your shows through your Artist calendar or the mobile
                    app under “Artist Tools.” It&apos;s simple to search for the
                    venue and add dates and times.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Song Lists" key="8">
                  <p>
                    You can add your song list as a CSV or PDF, and it will
                    appear on your artist profile. Clients appreciate seeing
                    what songs you can play.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Need More Help?" key="9">
                  <p>
                    Please send us your questions, and we&apos;ll answer them
                    and add them to this page. Your feedback helps us improve!
                  </p>
                </Collapse.Panel>
              </Collapse>
            </div>
          </div>

          <div className={`row m0 ${faqStyles.faq_box2}`}>
            <div className="col-12 col-sm-12">
              <h2>Restaurants and Venues</h2>
              <Collapse bordered={false} accordion expandIconPosition="end">
                <Collapse.Panel header="Manage Your Venue Profile Page" key="1">
                  <p>
                    People are searching for live music in your area, so keep
                    your venue page updated with upcoming shows and accurate
                    details. To create or claim your venue page, create a venue
                    manager account, search for your venue, and “claim” it. Once
                    approved, you can update photos, add events, message
                    artists, post open gigs, and more.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel
                  header="Promote Your Schedule on JCasp"
                  key="2"
                >
                  <p>
                    Contact us to start or enhance your live music program, get
                    featured placement, or find quality artists in your area.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel
                  header="Start a Live Music Program from Scratch"
                  key="3"
                >
                  <p>
                    Interested in starting a live music program at your
                    restaurant? Contact us for guidance.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel
                  header="Enhance Your Existing Live Music Program"
                  key="4"
                >
                  <p>
                    Looking to promote your live music program? JCasp offers
                    featured placement for limited clients in a given area.
                    Contact us to learn more.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel
                  header="Find Quality Artists in Your Area"
                  key="5"
                >
                  <p>
                    Need assistance in finding the right artists? Contact us,
                    and we can help!
                  </p>
                </Collapse.Panel>
                <Collapse.Panel
                  header="Do's and Don'ts of Having a Live Music Program"
                  key="6"
                >
                  <p>
                    Here&apos;s a guide to the best practices for hosting live
                    music at your venue, ensuring a successful and enjoyable
                    experience for all.
                  </p>
                </Collapse.Panel>
              </Collapse>
            </div>
          </div>

          <div className={`row m0 ${faqStyles.faq_box2}`}>
            <div className="col-12 col-sm-12">
              <h2>Corporate Event Planners & Private Hosts</h2>
              <Collapse bordered={false} accordion expandIconPosition="end">
                <Collapse.Panel
                  header="Find the Perfect Live Music for Your Event"
                  key="1"
                >
                  <p>
                    JCasp is a network of talented musicians and DJs to suit
                    any event, whether it&apos;s a corporate gathering or a
                    private celebration. Browse our platform to discover the
                    perfect match for your musical needs.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="JCasp PRO Service" key="2">
                  <p>
                    Looking for extra assistance in selecting and booking the
                    right artist? Our JCasp PRO Service provides personalized
                    support to ensure your event&apos;s success. We&apos;ll
                    understand your specific requirements, provide you with an
                    options list, and manage the booking from beginning to end.
                    A service fee applies for this premium offering.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="How to Book an Artist" key="3">
                  <p>
                    <strong>Search for Artists:</strong> Use our platform to
                    search for artists by genre, location, or specific needs.
                  </p>

                  <p>
                    <strong>Review Profiles:</strong> Explore artist profiles,
                    including photos, videos, bios, and song lists, to find the
                    right fit.
                  </p>

                  <p>
                    <strong>Contact Directly or Use JCasp PRO:</strong> You
                    can reach out to artists directly through their profiles,
                    post open gigs, or opt for our JCasp PRO Service for
                    end-to-end assistance.
                  </p>

                  <p>
                    <strong>Post Open Gigs:</strong> If you have specific
                    requirements, you can post open gigs, allowing artists to
                    submit for your event.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Benefits of Using JCasp" key="4">
                  <p>
                    <strong>Diverse Selection:</strong> Access to a wide variety
                    of musicians and bands, catering to all musical tastes and
                    event themes.
                  </p>

                  <p>
                    <strong>Quality Assurance:</strong> Our platform features
                    approved and vetted artists to ensure a professional and
                    memorable performance.
                  </p>

                  <p>
                    <strong>Ease of Use:</strong> Intuitive search and booking
                    process, with the option for full-service support through
                    JCasp PRO.
                  </p>

                  <p>
                    <strong>No Hidden Fees:</strong> Transparent pricing with no
                    hidden fees for searches or messaging on the JCasp
                    platform. JCasp PRO offers additional services for a fee.
                  </p>
                </Collapse.Panel>
                <Collapse.Panel header="Need More Assistance?" key="5">
                  <p>
                    Whether you&apos;re new to booking live music or have
                    specific requests, we&apos;re here to help. Contact us
                    directly for personalized support, guidance, and our
                    exclusive JCasp PRO Service.
                  </p>
                </Collapse.Panel>
              </Collapse>
            </div>
          </div>
        </div>

        <div className={`container-fluid  ${faqStyles.faq_box2}`}>
          <div className={`row m0 text-center faq-bottom`}>
            <div className="col-12 col-sm-12  faq-bottom-text ">
              <h3 data-aos="fade-down">Still have questions?</h3>
              <p data-aos="fade-up">
                Can’t find the answer you’re looking for? Please mail to our
                support team.
              </p>
              <div className="col-12 load_more mb-3 faq-btn">
                <Link href="/contact-us" className="hvr-float-shadow ">
                  <svg
                    width="27"
                    height="22"
                    viewBox="0 0 27 22"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M21.9715 1H5.02848C2.80361 1 1 2.54038 1 4.44054V17.5595C1 19.4596 2.80361 21 5.02848 21H21.9715C24.1964 21 26 19.4596 26 17.5595V4.44054C26 2.54038 24.1964 1 21.9715 1Z"
                      stroke="black"
                      stroke-width="1.4"
                      stroke-miterlimit="10"
                    />
                    <path
                      d="M6.76953 7.08131L11.7648 11.8041C12.5357 12.533 13.7415 12.5331 14.5126 11.8044L19.7163 6.88672"
                      stroke="black"
                      stroke-width="1.4"
                      stroke-miterlimit="10"
                      stroke-linecap="round"
                    />
                  </svg>{" "}
                  &nbsp; Send Questions
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default QuestionsAnswer;
