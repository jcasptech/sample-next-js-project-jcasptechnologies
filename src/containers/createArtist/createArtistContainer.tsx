import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { showToast } from "@components/ToastContainer";
import { createArtistAPI } from "@redux/services/artist.api";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { useEffect, useState } from "react";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { deepClone, getYoutubeId } from "src/libs/helpers";
import CreateArtistScene from "./createArtistScene";

export interface CreateArtistContainerProps {}

const CreateArtistContainer = (props: CreateArtistContainerProps) => {
  const [currentStep, setCurrentStep] = useState(1);
  const [isUpdateLoading, setIsUpdateLoading] = useState(false);
  const [profileData, setProfileData] = useState<UpdateArtistPayload>({
    artistName: "",
    profileImage: "",
    artistElements: [],
    artistPhonenumber: "",
    artistEmailId: "",
    artistLocaleCity: "",
    artistLocaleState: "",
    artistLocaleCountry: "",
    youtubeId: "",
    paymentPreference: "paypal",
    paypalId: "",
    vemeoId: "",
    artistBio: "",
    JCaspVanityTag: "",
    songs: [],
    vemeoLink: "",
    facebookPage: "",
    instagramUsername: "",
    website: "",
    liveStreamUrl: "",
    patreonLink: "",
    spotifyLink: "",
    youtubeVideoLinks: [],
    photos: [],
    audioFiles: [],
    profileTheme: "dark",
    csvFile: "",
    isProfileImgUpdated: "false",
  });

  const [isProfileImgUpdated, setIsProfileImgUpdated] = useState("false");

  useEffect(() => {}, []);

  const handlePrev = (index: any) => {
    if (currentStep > 1) {
      setCurrentStep(currentStep - 1);
    }
  };

  const handleNext = (data: any) => {
    setProfileData((prevState: any) => {
      return {
        ...prevState,
        ...data,
      };
    });
    if (currentStep < 5) {
      setCurrentStep(currentStep + 1);
    }
  };

  const handlePostData = async (data: any) => {
    if (profileData) {
      profileData["isProfileImgUpdated"] = isProfileImgUpdated;
      profileData["profileTheme"] = data;
      profileData["youtubeId"] = getYoutubeId(profileData.youtubeId) || "";

      setIsUpdateLoading(true);
      try {
        await createArtistAPI(profileData);
        setIsUpdateLoading(false);
        showToast(SUCCESS_MESSAGES.createArtistSucsess, "success");

        setTimeout(() => {
          location.replace("/dashboard");
        }, 1000);
      } catch (error) {
        setIsUpdateLoading(false);
        showToast(SUCCESS_MESSAGES.artistUpdateError, "warning");
      }
    }
  };

  const handleAddSongs = () => {
    const tmpData = deepClone(profileData);
    tmpData.songs.push({
      songName: "",
      songSingerName: "",
      url: "",
    });
    setProfileData(tmpData);
  };

  useEffect(() => {
    const tmpData = deepClone(profileData);
    if (tmpData.songs.length <= 0) {
      tmpData.songs.push({
        songName: "",
        songSingerName: "",
        url: "",
      });
      setProfileData(tmpData);
    }
  }, [profileData]);

  const handleRemoveSongs = (formId: any) => {
    const tmpData = deepClone(profileData);
    tmpData.songs = tmpData.songs.filter(
      (song: any, index: number) => index !== formId
    );
    setProfileData(tmpData);
  };

  useEffect(() => {
    const tmpData = deepClone(profileData);
    if (tmpData.youtubeVideoLinks.length <= 0) {
      tmpData.youtubeVideoLinks.push({
        link: "",
      });
      setProfileData(tmpData);
    }
  }, [profileData]);

  const handleAddVideolinks = () => {
    const tmpData = deepClone(profileData);
    if (tmpData.youtubeVideoLinks.length < 5) {
      tmpData.youtubeVideoLinks.push({
        link: "",
      });
      setProfileData(tmpData);
    }
  };

  const handleInputChange = (index: number, event: any) => {
    const tmpData = deepClone(profileData);
    tmpData.youtubeVideoLinks[index].link = event.target.value; // Update the value of the specific input field at the given index
    setProfileData(tmpData);
  };

  const handleRemoveVideoForm = (formId: number) => {
    const tmpData = deepClone(profileData);
    tmpData.youtubeVideoLinks = tmpData.youtubeVideoLinks.filter(
      (links: any, index: number) => index !== formId
    );
    setProfileData(tmpData);
  };

  return (
    <>
      <Show when={["create_artist"]} fallback={<NoPermissionsComponent />}>
        <CreateArtistScene
          {...{
            profileData,
            currentStep,
            handlePrev,
            handleNext,
            handlePostData,
            isUpdateLoading,
            handleAddSongs,
            handleRemoveSongs,
            handleAddVideolinks,
            handleRemoveVideoForm,
            setIsProfileImgUpdated,
            setCurrentStep,
            handleInputChange,
          }}
        />
      </Show>
    </>
  );
};

CreateArtistContainer.Layout = AdminLayoutComponent;
export default CreateArtistContainer;
