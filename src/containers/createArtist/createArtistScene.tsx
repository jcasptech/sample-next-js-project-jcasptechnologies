import Profile from "@components/artistProfileSteps/profile";
import { ProgressIcon } from "@components/theme/icons/progressIcon";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { useRouter } from "next/router";
import SongList from "@components/artistProfileSteps/songsList";
import ArtistLinks from "@components/artistProfileSteps/links";
import ArtistVideoLinks from "@components/artistProfileSteps/videoLinks";
import Photos from "@components/artistProfileSteps/photos";
import ArtistAudios from "@components/artistProfileSteps/audios";
import ArtistTheme from "@components/artistProfileSteps/theme";
import Image from "next/image";

export interface CreateArtistScaneProps {
  isUpdateLoading: boolean;
  profileData: UpdateArtistPayload;
  currentStep: number;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
  handlePostData: (d: any) => void;
  handleAddSongs: (d: any) => void;
  handleRemoveSongs: (d: any) => void;
  handleRemoveVideoForm: (d: any) => void;
  handleAddVideolinks: (d: any) => void;
  setIsProfileImgUpdated: (d: any) => void;
  setCurrentStep: (d: any) => void;
  handleInputChange: (index: number, event: any) => void;
}

const CreateArtistScene = (props: CreateArtistScaneProps) => {
  const {
    profileData,
    handlePrev,
    handleNext,
    currentStep,
    handlePostData,
    isUpdateLoading,
    handleAddSongs,
    handleRemoveSongs,
    handleRemoveVideoForm,
    handleAddVideolinks,
    setIsProfileImgUpdated,
    setCurrentStep,
    handleInputChange,
  } = props;
  const router = useRouter();

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="row m0">
            <div className="col-12 ptitle p-0"></div>

            <div className="multi_step_form float-left w-100 p-0">
              <div className="row m0" id="msform">
                <div className="col-12 p-0">
                  <div className="stepper-card float-left w-100">
                    <div className="row m0">
                      <div className="col-12 col-sm-4 col-lg-2 d-none d-sm-block">
                        <div className="navigation float-left w-100">
                          <a
                            className={`active step-nav d-flex justify-content-end`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(1);
                            }}
                          >
                            <div className="text-right">
                              <div>Artist Profile</div>
                            </div>
                            <div>
                              <Image
                                src="/images/artist-steps/active-user.png"
                                alt="active user"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 2 ? "active" : ""
                            }`}
                            onClick={() => {
                              setCurrentStep(2);
                            }}
                          >
                            <div className="text-right">
                              <div>Links</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 2 ? "active-" : ""
                                }link.png`}
                                alt="External Links"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 3 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(3);
                            }}
                          >
                            <div className="text-right">
                              <div>Video</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 3 ? "active-" : ""
                                }youtube.png`}
                                alt="Additional YouTube Video Links"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 4 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(4);
                            }}
                          >
                            <div className="text-right">
                              <div>Photos</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 4 ? "active-" : ""
                                }photos.png`}
                                alt="Upload Photos"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          {/* <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 5 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(5);
                            }}
                          >
                            <div className="text-right">
                              <div>Audio</div>
                            </div>
                            <div>
                              <img
                                src={`/images/artist-steps/${
                                  currentStep >= 5 ? "active-" : ""
                                }audio.png`}
                                alt="Audio Files"
                              />
                            </div>
                          </a> */}

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 5 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(5);
                            }}
                          >
                            <div className="text-right">
                              <div>Theme</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 5 ? "active-" : ""
                                }theme.png`}
                                alt="Select Profile Theme"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>
                        </div>
                      </div>
                      <div className="col-12 col-sm-8 col-lg-10 stepper-right p-0">
                        <div className="w-100 float-left d-block d-sm-none">
                          <div className="steps-progress d-flex">
                            <div className="current-progress">
                              <ProgressIcon
                                strokeWidth={6}
                                sqSize={56}
                                percentage={(currentStep * 100) / 5}
                              ></ProgressIcon>
                              <span>{currentStep} of 5</span>
                            </div>
                            {currentStep === 1 && (
                              <div className="step-info">
                                <div> Artist Profile</div>
                                <small>
                                  Please fill below artist personal details to
                                  process next step.
                                </small>
                              </div>
                            )}

                            {currentStep === 2 && (
                              <div className="step-info">
                                <div>External Links</div>
                                <small>
                                  Enter your social media links , website link
                                  and Zoo links.
                                </small>
                              </div>
                            )}

                            {currentStep === 3 && (
                              <div className="step-info">
                                <div>Additional Video Links</div>
                                <small>
                                  Please add additional YouTube or Vimeo links.
                                  You can add up to 5 videos.
                                </small>
                              </div>
                            )}

                            {currentStep === 4 && (
                              <div className="step-info">
                                <div>Upload Photos</div>
                                <small>
                                  Upload additional Artist photos. We recommend
                                  photos that showcase your live performances
                                </small>
                              </div>
                            )}

                            {/* {currentStep === 5 && (
                              <div className="step-info">
                                <div>Audio Files</div>
                                <small>
                                  Upload your audio files here. Note: Files must
                                  be original works and legally distributable.
                                </small>
                              </div>
                            )} */}

                            {currentStep === 5 && (
                              <div className="step-info">
                                <div>Select Profile Theme</div>
                                <small>
                                  Select the profile mode for your profile.
                                </small>
                              </div>
                            )}
                          </div>
                          <div className="w-100 border-bottom mt-3"></div>
                        </div>

                        <div className="w-100 float-left ps-5 stepcalPd d-none d-sm-block">
                          <p className="mb-0">
                            <span className="stepcal">
                              Step {currentStep}/5
                            </span>
                          </p>
                          <div className="stepInfoH float-left w-100">
                            <div
                              className={`step_info  ${
                                currentStep === 1 ? "impBlock" : "display-none"
                              }`}
                              data-set="1"
                            >
                              <h3 className="m-0"> Artist Profile</h3>
                              <p className="m-0">
                                Please fill below artist personal details to
                                process next step.
                              </p>
                            </div>

                            <div
                              className={`step_info ${
                                currentStep === 2 ? "impBlock" : "display-none"
                              }`}
                            >
                              <h3 className="m-0">External Links</h3>
                              <p className="m-0">
                                Enter your social media links , website link and
                                Zoom links.
                              </p>
                            </div>
                            <div
                              className={`step_info  ${
                                currentStep === 3 ? "impBlock" : "display-none"
                              }`}
                            >
                              <h3 className="m-0">Additional Video Links</h3>
                              <p className="m-0">
                                Please add additional YouTube or Vimeo links.
                                You can add up to 5 videos.
                              </p>
                            </div>
                            <div
                              className={`step_info ${
                                currentStep === 4 ? "impBlock" : "display-none"
                              }`}
                            >
                              <h3 className="m-0">Upload Photos</h3>
                              <p className="m-0">
                                Upload additional Artist photos. We recommend
                                photos that showcase your live performances
                              </p>
                            </div>
                            {/* <div
                              className={`step_info ${
                                currentStep === 5 ? "impBlock" : "display-none"
                              }`}
                            >
                              <h3 className="m-0">Audio Files</h3>
                              <p className="m-0">
                                Upload your audio files here. Note: Files must
                                be original works and legally distributable.
                              </p>
                            </div> */}
                            <div
                              className={`step_info ${
                                currentStep === 5 ? "impBlock" : "display-none"
                              }`}
                            >
                              <h3 className="m-0">Select Profile Theme</h3>
                              <p className="m-0">
                                Select the profile mode for your profile.
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className="ps-5 allSteps">
                          {currentStep === 1 && (
                            <Profile
                              {...{
                                profileData,
                                handleNext,
                                setIsProfileImgUpdated,
                              }}
                            />
                          )}
                          {currentStep === 2 && (
                            <ArtistLinks
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                              }}
                            />
                          )}
                          {currentStep === 3 && (
                            <ArtistVideoLinks
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                                handleAddVideolinks,
                                handleRemoveVideoForm,
                                handleInputChange,
                              }}
                            />
                          )}
                          {currentStep === 4 && (
                            <Photos
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                              }}
                            />
                          )}
                          {/* {currentStep === 5 && (
                            <ArtistAudios
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                              }}
                            />
                          )} */}
                          {currentStep === 5 && (
                            <ArtistTheme
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                                handlePostData,
                                isUpdateLoading,
                              }}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default CreateArtistScene;
