import { ArtistEditIcon } from "@components/theme/icons/artistEditIcon";
import { BellIcon } from "@components/theme/icons/bellIcon";
import { EditIcon } from "@components/theme/icons/editIcon";
import { ArtistObject } from "@redux/slices/artists";
import { Col, Row } from "antd";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import Link from "next/link";
import router, { useRouter } from "next/router";
import { useEffect } from "react";
import { capitalizeString, getFormatReview } from "src/libs/helpers";
import Artiststyles from "./featuredArtists.module.scss";

export interface SingleArtistProps {
  artist: ArtistObject;
  handleArtist: (d: any) => void;
  setIsEdit: (d: ArtistObject) => void;
}

const SingleArtist = (props: SingleArtistProps) => {
  const { artist, handleArtist, setIsEdit } = props;

  return (
    <Col lg={6} md={8} sm={12} xs={24} className={Artiststyles.artist_card}>
      <div className={Artiststyles.thumb}>
        <Image
          className="cursor-pointer"
          src={artist.posterImage?.url || "/images/artist-placeholder.png"}
          alt={artist.name}
          onClick={(e) => handleArtist(artist.vanity)}
          width={425}
          height={260}
        />
        <div className={`${Artiststyles.thumb__favrt}`}>
          <span
            onClick={() => {
              setIsEdit(artist);
            }}
          >
            <svg
              width="36"
              height="36"
              viewBox="0 0 36 36"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="18" cy="18" r="17.5" fill="white" stroke="#626262" />
              <g clip-path="url(#clip0_845_750)">
                <path
                  d="M26 13.6558C25.9138 14.042 25.6913 14.3414 25.4091 14.6082C25.1635 14.8404 24.9276 15.0832 24.6933 15.3269C24.6086 15.415 24.5508 15.4128 24.4649 15.3269C23.205 14.0629 21.9431 12.801 20.6791 11.5414C20.5906 11.453 20.5781 11.3939 20.6744 11.3011C20.9772 11.008 21.2731 10.7074 21.5702 10.409C21.778 10.1997 22.0317 10.0775 22.3129 10H22.8754C23.2344 10.0919 23.5162 10.299 23.7724 10.5624C24.3159 11.1205 24.8669 11.6713 25.4254 12.2147C25.7007 12.4825 25.9156 12.7772 26 13.1562V13.6558Z"
                  fill="#626262"
                />
                <path
                  d="M10.002 25.3432C10.1669 24.866 10.2882 24.3745 10.4285 23.8918C10.6919 22.9825 10.9494 22.0714 11.2096 21.1615C11.2653 20.9677 11.2753 20.9656 11.4177 21.108C12.5816 22.271 13.7449 23.4342 14.9076 24.5976C15.0451 24.7351 15.0436 24.7382 14.8558 24.7917C13.5001 25.1773 12.1443 25.5626 10.7884 25.9478C10.7438 25.9603 10.7013 25.9809 10.6578 25.9978C10.46 25.9978 10.2619 25.9953 10.0641 25.9978C10.0098 25.9978 9.99884 25.9881 10.0001 25.9353C10.0045 25.7391 10.002 25.541 10.002 25.3432Z"
                  fill="#626262"
                />
                <path
                  d="M23.9334 16.0636C23.9106 16.0917 23.8872 16.1261 23.8587 16.1548C23.7456 16.2701 23.6313 16.3839 23.5172 16.4985C20.9925 19.0237 18.4676 21.549 15.9425 24.0745C15.7813 24.2361 15.7809 24.2358 15.6191 24.0745L11.908 20.3631C11.7592 20.2147 11.7592 20.214 11.9117 20.0616L19.7858 12.1875C19.942 12.0313 19.942 12.0313 20.0983 12.1875C21.3391 13.428 22.5795 14.6687 23.8194 15.9096C23.8631 15.9527 23.92 15.9867 23.9334 16.0636Z"
                  fill="#626262"
                />
              </g>
              <defs>
                <clipPath id="clip0_845_750">
                  <rect
                    width="16"
                    height="16"
                    fill="white"
                    transform="translate(10 10)"
                  />
                </clipPath>
              </defs>
            </svg>
          </span>
        </div>
        <div className={`${Artiststyles.thumb__catgrs}`}>
          {artist.tags &&
            artist.tags.map((tag, index) => (
              <span
                key={index}
                className="cursor-pointer"
                onClick={(e) => {
                  e.preventDefault();
                  router.push(`/browse?tag=${tag}`);
                }}
              >
                {capitalizeString(tag)}
              </span>
            ))}
        </div>
      </div>

      <Link
        href={`/artist/${artist.vanity}`}
        target="_blank"
        className={`${Artiststyles.title}`}
      >
        {capitalizeString(artist.name)}
      </Link>

      <div className={`w-100 ${Artiststyles.abstract}`}>
        <div>
          <span className="">Current Score:</span> {artist.sortPriority || 0}
        </div>
        <div>
          <span className="">Admin Score:</span> {artist.adminScore || "-"}
        </div>
      </div>
    </Col>
  );
};

export default SingleArtist;
