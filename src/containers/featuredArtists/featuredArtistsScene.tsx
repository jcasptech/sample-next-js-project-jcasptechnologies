import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { ArtistObject } from "@redux/slices/artists";
import { FeaturedArtistDataResponse } from "@redux/slices/featuredArtist/featuredArtist";
import { Col, Row } from "antd";
import SingleArtist from "./single-artist";

export interface FeaturedArtistsSceneProps {
  featuredArtists: FeaturedArtistDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  isActionLoading: boolean;
  handleArtist: (d: any) => void;
  handleFilter: (d: any) => void;
  setIsEdit: (d: ArtistObject) => void;
  generateScore: (d: any) => void;
}

const FeaturedArtistsScene = (props: FeaturedArtistsSceneProps) => {
  const {
    featuredArtists,
    handleLoadMore,
    isLoading,
    isActionLoading,
    handleArtist,
    handleFilter,
    setIsEdit,
    generateScore,
  } = props;

  return (
    <>
      <div className={`main-section admin-panel-section`}>
        <section className="section">
          <Row gutter={[30, 30]}>
            <Col sm={12} xs={24}>
              <div className="search_form w-100">
                <input
                  type="text"
                  placeholder="Search Artists"
                  className="search_art aos-init aos-animate"
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onChange={(e: any) => {
                    handleFilter({ search: e.target?.value || "" });
                  }}
                />
              </div>
            </Col>
            <Col sm={12} xs={24}>
              <Button
                type="primary"
                htmlType="button"
                className="float-right"
                onClick={generateScore}
                loading={isActionLoading}
                disabled={isActionLoading}
              >
                Generate Score
              </Button>
            </Col>
          </Row>
        </section>
        <section className="section h-75vs">
          <Row gutter={[30, 30]}>
            {featuredArtists &&
              featuredArtists?.list &&
              featuredArtists?.list.length > 0 &&
              featuredArtists.list.map(
                (artist: ArtistObject, index: number) => (
                  <SingleArtist
                    key={`artist-${index}`}
                    {...{
                      artist,
                      handleArtist,
                      setIsEdit,
                    }}
                  />
                )
              )}
          </Row>

          {featuredArtists &&
            featuredArtists?.list &&
            featuredArtists?.list.length <= 0 && (
              <Row gutter={[30, 30]} justify="center">
                <Col>
                  <EmptyMessage description="No artists found." />
                </Col>
              </Row>
            )}

          {featuredArtists?.hasMany && featuredArtists?.hasMany === true && (
            <Row justify={"center"} className="mt-4">
              <Col>
                <Button
                  htmlType="button"
                  type="ghost"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={(e: any) => {
                    e.preventDefault();
                    handleLoadMore(true);
                  }}
                >
                  Load more
                </Button>
              </Col>
            </Row>
          )}
        </section>
      </div>
    </>
  );
};

export default FeaturedArtistsScene;
