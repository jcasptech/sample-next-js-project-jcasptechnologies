import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import ArtistScoreModal from "@components/modals/artistScoreModal";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { generatesScoreFeaturedArtistAPI } from "@redux/services/artist.api";
import { ArtistObject } from "@redux/slices/artists";
import {
  BrowseArtistState,
  fetchArtistFollows,
  loadMoreArtistFollows,
} from "@redux/slices/browseArtist/browseArtist";
import {
  FeaturedArtistState,
  fetchFeaturedArtist,
  loadMoreFeaturedArtist,
} from "@redux/slices/featuredArtist/featuredArtist";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import FeaturedArtistsScene from "./featuredArtistsScene";

export interface FeaturedArtistsContainerProps {}

const FeaturedArtistsContainer = (props: FeaturedArtistsContainerProps) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const [isEdit, setIsEdit] = useState<ArtistObject | null>(null);
  const [isActionLoading, setIsActionLoading] = useState(false);

  const {
    featuredArtist: { isLoading, data: featuredArtists },
  }: {
    featuredArtist: FeaturedArtistState;
  } = useSelector((state: RootState) => ({
    featuredArtist: state.featuredArtist,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
      include: ["metadata"],
    },
  });

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 8;
    dispatch(loadMoreFeaturedArtist(apiParam));
  };

  const handleArtist = (artistSlug: string) => {
    if (artistSlug) {
      router.push(`/artist/${artistSlug}`);
    }
  };

  const handleFilter = (data: any) => {
    apiParam.skip = 0;
    apiParam.search = data?.search;
    dispatch(fetchFeaturedArtist(apiParam));
  };

  const generateScore = async () => {
    try {
      setIsActionLoading(true);
      await generatesScoreFeaturedArtistAPI();
      showToast(SUCCESS_MESSAGES.generatesScoreSuccess, "success");
      setIsActionLoading(false);
      apiParam.skip = 0;
      apiParam.search = "";
      dispatch(fetchFeaturedArtist(apiParam));
    } catch (error) {
      console.log(error);
      setIsActionLoading(false);
    }
  };

  useEffect(() => {
    dispatch(fetchFeaturedArtist(apiParam));
  }, []);

  return (
    <>
      {(isLoading || isActionLoading) && <DefaultSkeleton />}
      <Show when={["featured_artists"]} fallback={<NoPermissionsComponent />}>
        <FeaturedArtistsScene
          {...{
            featuredArtists,
            handleLoadMore,
            isLoading,
            isActionLoading,
            handleArtist,
            handleFilter,
            setIsEdit,
            generateScore,
          }}
        />
      </Show>

      {isEdit && (
        <ArtistScoreModal
          {...{
            artistId: isEdit.objectId,
            handleClose: (e) => {
              setIsEdit(null);
              if (e) {
                apiParam.skip = 0;
                dispatch(fetchFeaturedArtist(apiParam));
              }
            },
          }}
        />
      )}
    </>
  );
};

FeaturedArtistsContainer.Layout = AdminLayoutComponent;
export default FeaturedArtistsContainer;
