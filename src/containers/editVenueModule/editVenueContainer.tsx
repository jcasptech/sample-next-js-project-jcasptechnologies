import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { deleteFileAPI, postFileAPI } from "@redux/services/artist.api";
import {
  getVenueFullDetailsAPI,
  updateVenueDetailsAPI,
} from "@redux/services/venue.api";

import { LoginUserState } from "@redux/slices/auth";
import {
  fetchvenueUserList,
  VenueUserState,
} from "@redux/slices/venueUserList";
import { useReCaptcha } from "next-recaptcha-v3";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import {
  acceptedCertificateImage,
  LOGIN_USER_TYPE,
  SUCCESS_MESSAGES,
} from "src/libs/constants";
import { nextRedirect } from "src/libs/helpers";
import {
  VenueEditFormInputs,
  VenueEditFormSchema,
} from "src/schemas/VenueEditFormSchema";
import EditVenueScene from "./editVenueScane";

export interface EditVenueContainerProps {
  useVenueId: any;
}
const EditVenueContainer = (props: EditVenueContainerProps) => {
  const { useVenueId } = props;
  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();
  const [isExportLoading, setIsExportLoading] = useState(false);

  const {
    register,
    handleSubmit,
    formState,
    setFocus,
    setValue,
    reset,
    control,
  } = useForm<VenueEditFormInputs>({
    resolver: yupResolver(VenueEditFormSchema),
  });

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const [selectedImage, setSelectedImage] = useState<any>([]);
  const [placeData, setPlaceData] = useState({
    place: "",
    place_id: "",
    city: "",
    state: "",
  });
  const [venueData, setVenueData] = useState<any>();
  const [artistData, setArtistData] = useState<any>([]);
  const [assignedArtist, setAssignedArtist] = useState<any>([]);

  const {
    venueUserList: { isLoading, data: venueUserListData },
  }: {
    venueUserList: VenueUserState;
  } = useSelector((state: RootState) => ({
    venueUserList: state.venueUserList,
  }));

  useEffect(() => {
    dispatch(fetchvenueUserList());
  }, []);

  useEffect(() => {
    if (venueData) {
      setValue("venueName", venueData?.venue?.name || "");
      setValue("venueDetails", venueData?.venue?.about || "");
      setValue("contactNumber", venueData?.venue?.phone || "");
      setValue("venueAddress", venueData?.venue?.address || "");
      setValue("websiteAddress", venueData?.venue?.website || "");
      setValue("facebookLink", venueData?.venue?.facebookPageId || "");
      setValue("instagramLink", venueData?.venue?.instagramUsername || "");
      setValue("youtubeLink", venueData?.venue?.youtubeLink || "");
      if (loginUserState?.data?.user?.role === LOGIN_USER_TYPE.VENUE_ADMIN) {
        setValue("assignedVenueTo", loginUserState?.data?.user?.objectId || "");
      }

      let tempPhotos: any = [];
      venueData.photos.map((item: any) => {
        tempPhotos.push({
          url: item?.thumbImage?.url,
          name: item?.thumbImage?.name,
        });
      });
      setSelectedImage(tempPhotos);

      if (loginUserState?.data?.userType === LOGIN_USER_TYPE.ADMIN) {
        if (venueUserListData.length && venueData?.venue) {
          venueUserListData.map((item: any) => {
            if (item.objectId === venueData?.venue?.venueAdmin?.objectId) {
              const tempArtist: any = [
                {
                  value: item.objectId,
                  label: item.name,
                },
              ];
              setValue("assignedVenueTo", item.objectId || "");
              setAssignedArtist(tempArtist);
            }
          });
        }
      }
    }
  }, [venueData]);

  const handleSelect = (place: any) => {
    setPlaceData({
      place: place?.formatted_address,
      place_id: place?.place_id,
      city: place?.address_components[4]?.long_name,
      state: place?.address_components[5]?.short_name,
    });
    setValue("venueAddress", place?.formatted_address || "");
  };

  const onFileSelected = async (files: any[]) => {
    let image = files[0];
    const fileExtension = image?.name?.split(".")?.pop()?.toLowerCase();
    if (image && acceptedCertificateImage.includes(fileExtension)) {
      setIsExportLoading(true);
      const formData = new FormData();
      formData.append("file", image);
      try {
        let response = await postFileAPI(formData);
        if (response) {
          setIsExportLoading(false);
          setSelectedImage((prevState: any) => {
            return [
              ...prevState,
              {
                ["url"]: response?.url,
                ["name"]: response?.file?.originalname,
              },
            ];
          });
        }
      } catch (error) {
        setIsExportLoading(false);
        console.log("not found");
      }
    } else {
      showToast(SUCCESS_MESSAGES.inputFileType, "warning");
    }
  };

  const onSubmit = async (data: any) => {
    let tempData = data;
    if (placeData?.place_id) {
      tempData["googlePlaceId"] = placeData?.place_id;
      tempData["city"] = placeData?.city;
      tempData["state"] = placeData?.state;
    } else {
      tempData["googlePlaceId"] = venueData?.venue?.googlePlaceId;
      tempData["city"] = venueData?.venue?.city;
      tempData["state"] = venueData?.venue?.state;
    }
    if (useVenueId) {
      setIsExportLoading(true);
      try {
        await updateVenueDetailsAPI(useVenueId, tempData);
        showToast(SUCCESS_MESSAGES.updateVenueDetails, "success");
        getVenue(useVenueId);
        setIsExportLoading(false);
      } catch (error) {
        console.log(error, "error");
        setIsExportLoading(false);
      }
    }
  };

  const getVenue = async (id: string) => {
    if (id) {
      setIsExportLoading(true);
      try {
        const recaptchaResponse = await executeRecaptcha("venue_edit");
        let res = await getVenueFullDetailsAPI(id, recaptchaResponse);
        setVenueData(res);
        setIsExportLoading(false);
      } catch (error) {
        console.log(error);
        setIsExportLoading(false);
      }
    }
  };

  useEffect(() => {
    if (useVenueId) {
      if (loaded) {
        getVenue(useVenueId);
      }
    }
  }, [useVenueId, loaded]);

  useEffect(() => {
    let temp: any = [];
    if (selectedImage) {
      selectedImage.map((url: any) => {
        temp.push(url.url);
      });
      setValue("venuePhotos", temp);
    }
  }, [selectedImage]);

  useEffect(() => {
    if (venueUserListData && venueUserListData?.length > 0) {
      venueUserListData?.map((v: any, index: number) => {
        const newData = {
          value: v.objectId,
          label: v.name,
        };
        setArtistData((prevData: any) => [...prevData, newData]);
        return null;
      });
    }
  }, [venueUserListData]);

  const handleArtistChange = (data: any) => {
    if (data) {
      setValue("assignedVenueTo", data);
      setAssignedArtist([data]);
    }
  };

  const handleDeletefile = async (link: any, objectId: any, formId: any) => {
    if (link) {
      const updatedPhotos = selectedImage.filter(
        (image: any, index: any) => index !== formId
      );
      const data: any = { link: link };

      if (!link.includes("public/temp/uploads")) {
        data["type"] = "photoes";
        data["objectID"] = objectId;
      }

      try {
        await deleteFileAPI(data);
        setSelectedImage(updatedPhotos);
        showToast(SUCCESS_MESSAGES.fileDeletedSuccess, "success");
      } catch (error) {
        console.log(error, "error");
      }
    }
  };

  useEffect(() => {
    if (
      loginUserState?.data?.user?.objectId &&
      loginUserState?.data?.userType === LOGIN_USER_TYPE.VENUE_ADMIN
    ) {
      setValue("assignedVenueTo", loginUserState?.data?.user?.objectId);
    }
  }, [loginUserState]);

  return (
    <>
      {isExportLoading && <DefaultSkeleton />}
      <Show when={["venue_listing"]} fallback={<NoPermissionsComponent />}>
        <EditVenueScene
          {...{
            register,
            handleSubmit,
            formState,
            onSubmit,
            onFileSelected,
            selectedImage,
            handleSelect,
            setValue,
            venueData,
            artistData,
            handleArtistChange,
            assignedArtist,
            handleDeletefile,
            loginUserState,
            control,
          }}
        />
      </Show>
    </>
  );
};

EditVenueContainer.getInitialProps = async (ctx: any) => {
  const venueId = ctx?.query?.id;
  let useVenueId = "";

  if (venueId) {
    useVenueId = venueId;
  } else {
    nextRedirect({ ctx, location: "/venue-listing" });
  }
  return {
    useVenueId,
  };
};

EditVenueContainer.Layout = AdminLayoutComponent;
export default EditVenueContainer;
