import { Popconfirm } from "antd";
import editVenueStyle from "./editVenue.module.scss";
import Select from "react-select";
import {
  InputField,
  TextAreaField,
  FileDropField,
  SelectField,
} from "@components/theme/form/formFieldsComponent";
import { acceptedCertificateImage } from "src/libs/constants";
import { useRouter } from "next/router";
import Autocomplete, { usePlacesWidget } from "react-google-autocomplete";
import { GOOGLE_API_KEY } from "src/libs/constants";
import { useState, useEffect } from "react";
import { loadScript } from "src/libs/helpers";
import { DeleteIcon } from "@components/theme/icons/deleteIcon";
import { LOGIN_USER_TYPE } from "src/libs/constants";
import { getVideoUrl } from "src/libs/helpers";
import { Button } from "@components/theme";
import Image from "next/image";

export interface QuestionSceneProps {
  handleSubmit: (d: any) => () => void;
  register: any;
  formState: any;
  onFileSelected: (d: any) => void;
  onSubmit: (d: any) => void;
  selectedImage: any;
  handleSelect: (d: any) => void;
  setValue: any;
  venueData: any;
  artistData: any;
  handleArtistChange: (d: any) => void;
  assignedArtist: any;
  handleDeletefile: (link: any, objectId: any, formId: any) => void;
  loginUserState: any;
  control: any;
}

const EditVenueScene = (props: QuestionSceneProps) => {
  const {
    register,
    formState,
    handleSubmit,
    onSubmit,
    onFileSelected,
    selectedImage,
    handleSelect,
    setValue,
    venueData,
    artistData,
    handleArtistChange,
    assignedArtist,
    handleDeletefile,
    loginUserState,
    control,
  } = props;

  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);
  const [videoLink, setVideoLink] = useState("");

  useEffect(() => {
    setTimeout(function () {
      jQuery(".pac-container").prependTo("#mapMoveHere");
    }, 300);
  }, []);

  return (
    <>
      <div className={`message-page  h-100vh ${editVenueStyle.page}`}>
        <div className="panel-inner-content">
          <div className="col-12 col-sm-12 col-lg-12 shows_holder ola mt-2">
            <div className="card card2 border-0">
              <div className="tab-content" id="tabcontent1">
                <div
                  className="tab-pane fade show active"
                  id="tabs-text-1"
                  role="tabpanel"
                >
                  <form
                    className={`${editVenueStyle.my_account_form} panel-form edit-venue`}
                    onSubmit={handleSubmit(onSubmit)}
                    method="POST"
                  >
                    <div className="new-fm-box">
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Venue Name
                          </label>
                        </div>
                        <div className="col-12 col-lg-8 ">
                          <div className="">
                            <InputField
                              {...{
                                register,
                                formState,
                                id: "venueName",
                                className: `inputField`,
                                placeholder: "Enter Venue Name",
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Venue Details
                          </label>
                        </div>
                        <div className="col-12 col-lg-8">
                          <div className="">
                            <TextAreaField
                              {...{
                                register,
                                formState,
                                id: "venueDetails",
                                className: `input`,
                                placeholder: "Enter Venue Details",
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Venue Photo
                          </label>
                        </div>
                        <div className="col-12 col-lg-8">
                          <div className="">
                            <FileDropField
                              {...{
                                register,
                                formState,
                                name: `venuePhotos`,
                                id: `venuePhotos`,
                                type: "hidden",
                                description: (
                                  <>
                                    <Image
                                      src="/images/artistPhotosIcon.png"
                                      className="mb-3 h-auto"
                                      alt="artistPhotos"
                                      height={60}
                                      width={60}
                                    />
                                    <div>
                                      Drop your photo(s) here or{" "}
                                      <span> Browse </span>
                                    </div>
                                    <div className="mute">
                                      Supports :{" "}
                                      {acceptedCertificateImage
                                        .join(", ")
                                        .toUpperCase()}
                                    </div>
                                  </>
                                ),
                                onChange: (e: any) => {
                                  onFileSelected(e.target.files);
                                },
                              }}
                            />
                          </div>
                          <div className={editVenueStyle.listContainer}>
                            {selectedImage &&
                              selectedImage.length > 0 &&
                              selectedImage.map((image: any, index: any) => (
                                <div className="photos_List" key={index}>
                                  <Image
                                    src={image.url || image || ""}
                                    className="m-0 h-auto"
                                    alt="photo"
                                    height={145}
                                    width={150}
                                  />
                                  <Popconfirm
                                    title="Are you sure you want to delete?"
                                    okText="Yes"
                                    cancelText="No"
                                    onConfirm={() =>
                                      handleDeletefile(
                                        image?.url,
                                        image?.objectId,
                                        index
                                      )
                                    }
                                  >
                                    <div
                                      className={`${editVenueStyle.deleteIcon} cursor-pointer`}
                                    >
                                      <DeleteIcon />
                                    </div>
                                  </Popconfirm>
                                </div>
                              ))}
                          </div>
                        </div>
                      </div>
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Contact Number
                          </label>
                        </div>
                        <div className="col-12 col-lg-8">
                          <div className="">
                            <InputField
                              {...{
                                register,
                                formState,
                                id: "contactNumber",
                                className: `inputField`,
                                placeholder: "Enter Contact Number",
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Venue Address
                          </label>
                        </div>
                        <div className="col-12 col-lg-8">
                          <div className="">
                            <Autocomplete
                              // ref={inputEl}
                              placeholder="Search for venue name or address"
                              apiKey={"AIzaSyBt76hsi8D_EmmZkQE4pIztnAGotzte-1I"}
                              onPlaceSelected={(place) => handleSelect(place)}
                              id="venueAddress"
                              defaultValue={venueData?.venue?.address || ""}
                              className="venue-search"
                              options={{
                                types: [
                                  "bar",
                                  "restaurant",
                                  "shopping_mall",
                                  "stadium",
                                  "tourist_attraction",
                                ],
                              }}
                            />
                            {formState &&
                              formState?.errors &&
                              formState?.errors["venueAddress"] &&
                              formState?.errors["venueAddress"].message && (
                                <span className="text-danger">
                                  {formState?.errors["venueAddress"].message}
                                </span>
                              )}
                            <div id="mapMoveHere"></div>
                          </div>
                        </div>
                      </div>
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Website Address
                          </label>
                        </div>
                        <div className="col-12 col-lg-8">
                          <div className="">
                            <InputField
                              {...{
                                register,
                                formState,
                                id: "websiteAddress",
                                className: `inputField`,
                                placeholder: "Enter Website Address",
                              }}
                            />
                          </div>
                        </div>
                      </div>

                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Facebook Link
                          </label>
                        </div>
                        <div className="col-12 col-lg-8">
                          <div className="">
                            <InputField
                              {...{
                                register,
                                formState,
                                id: "facebookLink",
                                className: `inputField`,
                                placeholder: "Enter Facebook Link",
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Instagram Link
                          </label>
                        </div>
                        <div className="col-12 col-lg-8">
                          <div className="">
                            <InputField
                              {...{
                                register,
                                formState,
                                id: "instagramLink",
                                className: `inputField`,
                                placeholder: "Enter Instagram Link",
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row align-center field-row">
                        <div className="col-lg-4 col-12">
                          <label className={editVenueStyle.label}>
                            Youtube Link
                          </label>
                        </div>
                        <div className="col-12 col-lg-8 ">
                          <div className="">
                            <InputField
                              {...{
                                register,
                                formState,
                                id: "youtubeLink",
                                className: `inputField`,
                                placeholder: "Enter Youtube Link",
                              }}
                              onChange={(e) => setVideoLink(e.target.value)}
                            />
                          </div>
                          {videoLink !== "" && (
                            <div className={`videoPreview`}>
                              <iframe
                                width="auto"
                                height="auto"
                                className="frameVideo"
                                src={`${getVideoUrl(videoLink || "")}`}
                                title="YouTube video player"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                allowFullScreen
                              ></iframe>
                            </div>
                          )}
                        </div>
                      </div>
                      {loginUserState?.data?.userType ===
                        LOGIN_USER_TYPE.ADMIN && (
                        <div className="row align-center field-row">
                          <div className="col-lg-4 col-12">
                            <label className={editVenueStyle.label}>
                              Assign Venue To
                            </label>
                          </div>
                          <div className="col-12 col-lg-8 ">
                            <div className="">
                              <SelectField
                                {...{
                                  register,
                                  formState,
                                  name: "assignedVenueTo",
                                  className: "inputField",
                                  id: "assignedVenueTo",
                                  placeholder: "Assign To",
                                  onSelectChange: handleArtistChange,
                                  control,
                                }}
                                options={artistData}
                              />
                              {/* <Select
                                value={assignedArtist || ""}
                                className={`basic_multi_select selectField`}
                                classNamePrefix="Assign To"
                                placeholder="Assign To"
                                onChange={(d) => handleArtistChange(d)}
                                options={artistData}
                                instanceId="assignedArtist"
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["assignedVenueTo"] &&
                                formState?.errors["assignedVenueTo"]
                                  ?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["assignedVenueTo"]
                                        ?.message
                                    }
                                  </span>
                                )} */}
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                    <div className="row align-center field-row">
                      <div className="col-lg-4 col-12"></div>
                      <div className="col-12 col-lg-8">
                        <Button
                          onClick={(d) => router.push("/venue-listing")}
                          className="mt-4"
                          htmlType="button"
                          type="ghost"
                          disabled={formState.isSubmitting}
                        >
                          <svg
                            width="13"
                            height="9"
                            viewBox="0 0 13 9"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M12 5.1C12.3314 5.1 12.6 4.83137 12.6 4.5C12.6 4.16863 12.3314 3.9 12 3.9V5.1ZM0.575736 4.07574C0.341421 4.31005 0.341421 4.68995 0.575736 4.92426L4.39411 8.74264C4.62843 8.97696 5.00833 8.97696 5.24264 8.74264C5.47696 8.50833 5.47696 8.12843 5.24264 7.89411L1.84853 4.5L5.24264 1.10589C5.47696 0.871573 5.47696 0.491674 5.24264 0.257359C5.00833 0.0230446 4.62843 0.0230446 4.39411 0.257359L0.575736 4.07574ZM12 3.9L1 3.9V5.1L12 5.1V3.9Z"
                              fill="black"
                            />
                          </svg>
                          &nbsp; Back
                        </Button>

                        <Button
                          htmlType="submit"
                          type="primary"
                          loading={formState.isSubmitting}
                          disabled={formState.isSubmitting}
                          className="float-right mt-4"
                        >
                          Save Venue Details
                        </Button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditVenueScene;
