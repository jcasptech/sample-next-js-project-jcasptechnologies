import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { useEffect, useState } from "react";

import { DefaultSkeleton } from "@components/theme/defaultSkeleton";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import {
  addArtistSubscribeAPI,
  artistUnsubscribeAPI,
} from "@redux/services/artist.api";
import { whoAmI } from "@redux/services/auth.api";
import {
  venueSubscribeAPI,
  venueUnsubscribeAPI,
} from "@redux/services/venue.api";
import { LoginUserState, loginUserSuccess } from "@redux/slices/auth";
import {
  BrowseArtistState,
  fetchBrowseArtist,
  loadMoreBrowseArtist,
} from "@redux/slices/browseArtist/browseArtist";
import {
  fetchMusicScene,
  MusicSceneState,
} from "@redux/slices/music-scene/musicScene";
import { VenueState } from "@redux/slices/venueDetails";
import { fetchVenues, loadMoreVenues } from "@redux/slices/venues";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import BrowseArtistScene from "./browseArtistScene";
import SEO from "@components/SEO";
import { useReCaptcha } from "next-recaptcha-v3";
import { LoginModal } from "@components/modals/loginModal";

export interface BrowseArtistProps {
  isTagSearch: any;
}
const BrowseArtistContainer = (props: BrowseArtistProps) => {
  const { isTagSearch } = props;

  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();
  const router = useRouter();
  const [isActionLoading, setIsActionLoading] = useState(false);
  const [currentTab, setCurrentTab] = useState<"artist" | "venue" | null>(
    "artist"
  );
  const [loadApi, setIsLoadApi] = useState(false);
  const [follows, setFollows] = useState<any[]>([]);
  const [venueFollows, setVenueFollows] = useState<any[]>([]);

  const [isLocation, setIsLocation] = useState(false);
  const [defaultSearch, setDefaultSearch] = useState("");
  const [currentLocation, setCurrentLocation] = useState<any>();

  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [userAction, setUserAction] = useState<
    "artistSubscribe" | "venueSubscribe" | null
  >(null);
  const [tmpArtistId, setTmpArtistId] = useState<any>(null);
  const [tmpVenueId, setTmpVenueId] = useState<any>(null);
  const [tmpVenueType, setTmpVenueType] = useState<any>(null);

  const {
    browseArtist: { isLoading, data: browseArtistData },
  }: {
    browseArtist: BrowseArtistState;
  } = useSelector((state: RootState) => ({
    browseArtist: state.browseArtist,
  }));

  const {
    venues: { isLoading: venueIsLoading, data: venues },
  }: {
    venues: VenueState;
  } = useSelector((state: RootState) => ({
    venues: state.venues,
  }));

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const [locationOption, setLocationOption] = useState<any[]>([]);
  const [defaultOption, setDefaultOption] = useState<any>();

  const {
    musicScene: { isLoading: musicIsLoading, data: musicSceneData },
  }: {
    musicScene: MusicSceneState;
  } = useSelector((state: RootState) => ({
    musicScene: state.musicScene,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 16,
      skip: 0,
      include: ["metadata"],
    },
  });

  const { apiParam: venueApiParam } = useList({
    queryParams: {
      take: 16,
      skip: 0,
    },
  });

  let timer: any;
  const handlInputChange = (data: any) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      handleFilter(data);
    }, 500);
  };

  const handleFilter = (data: any) => {
    if (data.search) {
      apiParam.search = data.search;
      venueApiParam.search = data.search;
    } else {
      delete apiParam.search;
      delete venueApiParam.search;
    }

    apiParam.skip = 0;
    apiParam.take = 16;

    venueApiParam.skip = 0;
    venueApiParam.take = 16;

    if (currentTab === "artist") {
      executeRecaptcha("artist_list").then((recaptchaResponse) => {
        dispatch(fetchBrowseArtist(apiParam, recaptchaResponse));
      });
    } else {
      executeRecaptcha("venue_list").then((recaptchaResponse) => {
        dispatch(fetchVenues(venueApiParam, recaptchaResponse));
      });
    }
  };

  const handleLocation = (data: any) => {
    if (data.location) {
      apiParam.latitude = data.location?.latitude;
      apiParam.longitude = data.location?.longitude;

      venueApiParam.latitude = data.location?.latitude;
      venueApiParam.longitude = data.location?.longitude;
    } else {
      delete apiParam.latitude;
      delete apiParam.longitude;

      delete venueApiParam.latitude;
      delete venueApiParam.longitude;
    }

    apiParam.skip = 0;
    apiParam.take = 16;
    venueApiParam.skip = 0;
    venueApiParam.take = 16;

    if (currentTab === "artist") {
      executeRecaptcha("artist_list").then((recaptchaResponse) => {
        dispatch(fetchBrowseArtist(apiParam, recaptchaResponse));
      });
    } else {
      executeRecaptcha("venue_list").then((recaptchaResponse) => {
        dispatch(fetchVenues(venueApiParam, recaptchaResponse));
      });
    }
  };

  const loadMore = (type: "artist" | "venue") => {
    if (type === "artist") {
      apiParam.skip = (apiParam.skip || 0) + 16;
      executeRecaptcha("artist_list").then((recaptchaResponse) => {
        dispatch(loadMoreBrowseArtist(apiParam, recaptchaResponse));
      });
    } else {
      venueApiParam.skip = (venueApiParam.skip || 0) + 16;
      executeRecaptcha("venue_list").then((recaptchaResponse) => {
        dispatch(loadMoreVenues(venueApiParam, recaptchaResponse));
      });
    }
  };

  const handleArtistSubscribe = async (artistId: string) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          await addArtistSubscribeAPI(artistId);
          showToast(SUCCESS_MESSAGES.subscribeArtist, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      setIsLoginModalOpen(true);
      setUserAction("artistSubscribe");
      setTmpArtistId(artistId);
      // showToast(SUCCESS_MESSAGES.requiredLoginSubscribe, "warning");
    }
  };

  const handleArtistUnsubscribe = async (artistId: string) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          await artistUnsubscribeAPI(artistId);
          showToast(SUCCESS_MESSAGES.unsubscribeArtist, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      showToast(SUCCESS_MESSAGES.requiredLoginSubscribe, "warning");
    }
  };

  const handleArtist = (artistSlug: string) => {
    if (artistSlug) {
      router.push(`/artist/${artistSlug}`);
    }
  };

  // Venue functions
  const handleVenueSubscribeActions = async (
    id: string,
    type: "subscribe" | "unsubscribe"
  ) => {
    const restrictMessage = SUCCESS_MESSAGES.requiredLoginSubscribeVenue;
    const successMessage =
      type === "subscribe"
        ? SUCCESS_MESSAGES.subscribeVenue
        : SUCCESS_MESSAGES.unsubscribeVenue;

    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          if (type === "subscribe") {
            await venueSubscribeAPI(id);
          } else {
            await venueUnsubscribeAPI(id);
          }
          showToast(successMessage, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      setIsLoginModalOpen(true);
      setUserAction("venueSubscribe");
      setTmpVenueId(id);
      setTmpVenueType(type);
      // showToast(restrictMessage, "warning");
    }
  };

  const handleVenue = (slug: string) => {
    if (slug) {
      router.push(`/venue/${slug}`);
    }
  };

  useEffect(() => {
    if (loginUserState?.data?.user?.follows) {
      setFollows(loginUserState?.data?.user?.follows);
    }

    if (loginUserState?.data?.user?.venueFollows) {
      setVenueFollows(loginUserState?.data?.user?.venueFollows);
    }
  }, [loginUserState?.data?.user]);

  useEffect(() => {
    if (!musicIsLoading && musicSceneData && musicSceneData?.length > 0) {
      const options: any = musicSceneData.map((city) => {
        return {
          value: city.location,
          label: `${city.city} (${(city.state, city.country)})`,
        };
      });

      if (
        isLocation &&
        currentLocation?.latitude &&
        currentLocation?.longitude
      ) {
        options.unshift({
          value: currentLocation,
          label: "Current location",
        });

        if (!isTagSearch) {
          setDefaultOption(options[0]);

          apiParam.latitude = currentLocation.latitude;
          apiParam.longitude = currentLocation.longitude;

          venueApiParam.latitude = currentLocation.latitude;
          venueApiParam.longitude = currentLocation.longitude;
        }
      }

      if (isTagSearch) {
        setDefaultSearch(isTagSearch);
        handleFilter({
          search: isTagSearch,
        });
      }
      setLocationOption(options);

      executeRecaptcha("artist_list").then((recaptchaResponse) => {
        dispatch(fetchBrowseArtist(apiParam, recaptchaResponse));
      });

      setIsLoadApi(true);
    }
  }, [musicIsLoading, musicSceneData, currentLocation]);

  useEffect(() => {
    if (currentTab && loadApi) {
      if (currentTab === "artist") {
        apiParam.skip = 0;
        apiParam.take = 16;
        executeRecaptcha("artist_list").then((recaptchaResponse) => {
          dispatch(fetchBrowseArtist(apiParam, recaptchaResponse));
        });
      } else {
        venueApiParam.skip = 0;
        venueApiParam.take = 16;
        executeRecaptcha("venues_list").then((recaptchaResponse) => {
          dispatch(fetchVenues(venueApiParam, recaptchaResponse));
        });
      }
    }
  }, [currentTab]);

  useEffect(() => {
    if (loaded) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          function (position) {
            const { latitude, longitude } = position.coords;
            setCurrentLocation({
              latitude,
              longitude,
            });
            setIsLocation(true);
          },
          function (error) {
            setIsLocation(false);
          }
        );
      } else {
        setIsLocation(false);
      }

      executeRecaptcha("music_scense").then((recaptchaResponse) => {
        dispatch(fetchMusicScene(recaptchaResponse));
      });
    }
  }, [loaded]);

  return (
    <>
      {(isLoading || venueIsLoading || isActionLoading) && <DefaultSkeleton />}
      <SEO
        {...{
          pageName: "/browse",
        }}
      />
      <BrowseArtistScene
        {...{
          currentTab,
          setCurrentTab,
          browseArtistData: browseArtistData,
          loadMore,
          isLoading,
          handleFilter,
          follows,
          handleArtistUnsubscribe,
          handleArtistSubscribe,
          locationOption,
          isActionLoading,
          handleLocation,
          handleArtist,
          defaultOption,
          setDefaultOption,
          venues,
          venueFollows,
          handleVenue,
          handleVenueSubscribeActions,
          defaultSearch,
          handlInputChange,
        }}
      />

      {isLoginModalOpen && userAction && (
        <LoginModal
          {...{
            actionFrom: userAction,
            handleOk: () => {
              setIsLoginModalOpen(false);
              setUserAction(null);
              if (userAction === "artistSubscribe") {
                handleArtistSubscribe(tmpArtistId);
              }

              if (userAction === "venueSubscribe") {
                handleVenueSubscribeActions(tmpVenueId, tmpVenueType);
              }
            },
            isOpen: isLoginModalOpen,
            handleCancel: () => {
              setIsLoginModalOpen(false);
            },
          }}
        />
      )}
    </>
  );
};
BrowseArtistContainer.Layout = MainLayoutComponent;

BrowseArtistContainer.getInitialProps = async (ctx: any) => {
  const tags = ctx?.query?.tag;

  return {
    isTagSearch: tags ? tags : null,
  };
};

export default BrowseArtistContainer;
