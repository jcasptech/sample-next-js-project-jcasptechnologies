import { BellIcon } from "@components/theme/icons/bellIcon";
import { ArtistObject } from "@redux/slices/artists";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { capitalizeString, getFormatReview } from "src/libs/helpers";
import BrowseArtiststyles from "./browseArtist.module.scss";

export interface ArtistProps {
  artist: ArtistObject;
  follows: any[];
  handleArtist: (d: any) => void;
  handleArtistUnsubscribe: (id: string) => void;
  handleArtistSubscribe: (id: string) => void;
}

const Artist = (props: ArtistProps) => {
  const {
    artist,
    follows,
    handleArtist,
    handleArtistUnsubscribe,
    handleArtistSubscribe,
  } = props;

  const router = useRouter();

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <div
      className="col-12 col-sm-4 col-md-3 acHold"
      // data-aos="fade-down"
      // data-aos-delay="100"
      // data-aos-duration="1200"
    >
      <div className="ArtCard float-left w-100">
        <div className={BrowseArtiststyles.thumb}>
          <Image
            className="cursor-pointer"
            src={artist.posterImage?.url || "/images/artist-img.png"}
            alt={artist.name}
            onClick={(e) => handleArtist(artist.vanity)}
            width={425}
            height={260}
          />

          <div className={`${BrowseArtiststyles.thumb__catgrs}`}>
            {artist.tags &&
              artist.tags.map((tag, index) => (
                <span
                  key={index}
                  className="cursor-pointer"
                  onClick={(e) => {
                    e.preventDefault();
                    router.push(`/browse?tag=${tag}`);
                  }}
                >
                  {capitalizeString(tag)}
                </span>
              ))}
          </div>
          <div className={`${BrowseArtiststyles.thumb__favrt}`}>
            {artist?.objectId && follows.indexOf(artist.objectId) > -1 ? (
              <span
                onClick={(e) => {
                  e.preventDefault();
                  handleArtistUnsubscribe(artist?.objectId);
                }}
                className="unfollow"
              >
                <BellIcon />
              </span>
            ) : (
              <span
                onClick={(e) => {
                  e.preventDefault();
                  handleArtistSubscribe(artist?.objectId);
                }}
              >
                <BellIcon />
              </span>
            )}
          </div>
        </div>
        <div
          className={`title w-100 float-left cursor-pointer ${BrowseArtiststyles.title}`}
          onClick={(e) => handleArtist(artist.vanity)}
        >
          {capitalizeString(artist.name)}
        </div>
        {/* <!-- note: provision is provided upto 2 lines in the title --> */}
        {/* <div className="reviews float-left w-100 vcenter">
          <img src="/images/rate.png" alt="rate" />
          &nbsp;{artist.rating} ( {getFormatReview(artist.ratingCount)} Reviews
          )
        </div> */}
        <div className="abstract w-100 float-left two-line-and-text-ellipsis ">
          {artist.metadata?.about
            ? `${
                artist.metadata.about.length >= 190
                  ? artist.metadata.about.substring(0, 190) + "..."
                  : artist.metadata.about
              }`
            : ""}
        </div>
      </div>
    </div>
  );
};

export default Artist;
