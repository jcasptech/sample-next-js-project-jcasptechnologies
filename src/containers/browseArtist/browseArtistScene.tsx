import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { BellIcon } from "@components/theme/icons/bellIcon";
import { ArtistObject } from "@redux/slices/artists";
import { BrowseArtistDataResponse } from "@redux/slices/browseArtist/browseArtist";
import Link from "next/link";
import Select from "react-select";
import BrowseArtiststyles from "./browseArtist.module.scss";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import { WebsiteColorIcon } from "@components/theme/icons/websiteColorIcon";
import { PhoneColorIcon } from "@components/theme/icons/phoneColorIcon";
import { LocationColorIcon } from "@components/theme/icons/locationColorIcon";
import { VenueObject, VenuesDataResponse } from "@redux/slices/venues";
import Venue from "@components/venue/venue";
import Artist from "./artist";

interface BrowseArtistSceneProps {
  currentTab: "artist" | "venue" | null;
  setCurrentTab: (d: "artist" | "venue") => void;
  browseArtistData: BrowseArtistDataResponse;
  loadMore: any;
  isLoading: any;
  follows: any[];
  handleFilter: (d: any) => void;
  handleArtistUnsubscribe: (d: any) => void;
  handleArtistSubscribe: (d: any) => void;
  locationOption: any[];
  isActionLoading: boolean;
  handleLocation: (d: any) => void;
  handleArtist: (d: any) => void;
  defaultOption: any;
  setDefaultOption: (d: any) => void;
  venues: VenuesDataResponse;
  venueFollows: any[];
  handleVenue: (d: any) => void;
  handleVenueSubscribeActions: (
    id: string,
    type: "subscribe" | "unsubscribe"
  ) => void;
  defaultSearch: any;
  handlInputChange: (d: any) => void;
}

const BrowseArtistScene = (Props: BrowseArtistSceneProps) => {
  const {
    browseArtistData,
    loadMore,
    follows,
    isLoading,
    handleFilter,
    handleArtistUnsubscribe,
    handleArtistSubscribe,
    locationOption,
    isActionLoading,
    handleLocation,
    handleArtist,
    defaultOption,
    setDefaultOption,
    venues,
    venueFollows,
    handleVenue,
    handleVenueSubscribeActions,
    defaultSearch,
    currentTab,
    setCurrentTab,
    handlInputChange,
  } = Props;

  // useEffect(() => {
  //   AOS.init();
  // }, []);

  return (
    <>
      {/* <section className="section-pad pageHeader">
        <div className="container-fluid">
          <div className="row ">
            <div className={`col-12 ${BrowseArtiststyles.pHead}`}>
              <p>Live Music Near You</p>
            </div>
          </div>
        </div>
      </section> */}
      <section className={`section-pad ${BrowseArtiststyles.head_b}`}>
        <div className="">
          <div className={`row m0 pt-3 pb-3 ${BrowseArtiststyles.s__row}`}>
            <div
              className={`col-12  col-lg-6  ${BrowseArtiststyles.s__row__column}`}
            >
              <div className={`${BrowseArtiststyles.head_b__search_form}`}>
                <input
                  type="search"
                  defaultValue={defaultSearch}
                  placeholder={`Search for ${
                    currentTab === "artist" ? "Artist" : "Venue"
                  }`}
                  className={`search_art aos-init aos-animate ${BrowseArtiststyles.border_b}`}
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onInput={(e: any) => {
                    handlInputChange({
                      search: e.target?.value || "",
                    });
                  }}
                />
                <div className="input-wrap second">
                  {!isActionLoading && (
                    <Select
                      value={defaultOption}
                      options={locationOption}
                      placeholder="Location..."
                      onChange={(e) => {
                        if (e?.value) {
                          setDefaultOption(e);
                          handleLocation({
                            location: e.value,
                          });
                        } else {
                          handleLocation({});
                          setDefaultOption(null);
                        }
                      }}
                      styles={{
                        control: (baseStyles, state) => ({
                          ...baseStyles,
                          border: 0,
                          boxShadow: "none",
                        }),
                      }}
                      className={BrowseArtiststyles.select}
                      classNamePrefix="react-select"
                      isClearable
                      instanceId="location"
                    />
                  )}
                </div>
              </div>
            </div>
            <div
              className={`col-12 col-sm-12 col-lg-3 vcenter ${BrowseArtiststyles.browse_tabs}`}
            >
              <div className="nav-wrapper">
                <ul
                  className="nav nav-pills nav-fill flex-row flex-md-row"
                  id="tabs-text"
                  role="tablist"
                >
                  <li className="nav-item">
                    <a
                      className={`nav-link ${
                        currentTab === "artist" ? "active disabled" : ""
                      }`}
                      onClick={(e) => {
                        e.preventDefault();
                        setCurrentTab("artist");
                      }}
                    >
                      Artists
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={`nav-link ${
                        currentTab === "venue" ? "active disabled" : ""
                      }`}
                      onClick={(e) => {
                        e.preventDefault();
                        setCurrentTab("venue");
                      }}
                    >
                      Venues
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>

      {currentTab === "artist" && (
        <div className={`row m0 load_artists ${BrowseArtiststyles.artist_b}`}>
          {browseArtistData &&
            browseArtistData?.list &&
            browseArtistData?.list.length > 0 &&
            browseArtistData.list.map((artist: ArtistObject, index: number) => (
              <Artist
                key={`artist-${index}`}
                {...{
                  artist,
                  follows,
                  handleArtist,
                  handleArtistSubscribe,
                  handleArtistUnsubscribe,
                }}
              />
            ))}

          {browseArtistData &&
            browseArtistData?.list &&
            browseArtistData?.list.length <= 0 && (
              <EmptyMessage description="No artists found." />
            )}

          {browseArtistData?.hasMany && browseArtistData?.hasMany === true && (
            <div className={`col-12 load_more ${BrowseArtiststyles.btn_b}`}>
              <Button
                htmlType="button"
                type="ghost"
                disabled={isLoading}
                loading={isLoading}
                onClick={(e: any) => {
                  e.preventDefault();
                  loadMore("artist");
                }}
              >
                Load more Artists
              </Button>
            </div>
          )}
        </div>
      )}

      {currentTab === "venue" && (
        <div className={`row m0 load_artists ${BrowseArtiststyles.artist_b}`}>
          {venues &&
            venues?.list &&
            venues?.list.length > 0 &&
            venues.list.map((venue: VenueObject, index: number) => (
              <Venue
                {...{
                  venue,
                  venueFollows,
                  handleVenue,
                  handleVenueSubscribeActions,
                }}
                key={`venue-${index}`}
              />
            ))}

          {venues && venues?.list && venues?.list.length <= 0 && (
            <EmptyMessage description="No venues found." />
          )}

          {/* {isLoading && <DefaultLoader />} */}

          {venues?.hasMany && venues?.hasMany === true && (
            <div className={`col-12 load_more ${BrowseArtiststyles.btn_b}`}>
              <Button
                htmlType="button"
                type="ghost"
                disabled={isLoading}
                loading={isLoading}
                onClick={(e: any) => {
                  e.preventDefault();
                  loadMore("venue");
                }}
              >
                Load more Venues
              </Button>
            </div>
          )}
        </div>
      )}

      {/* <section className={`section-pad ${BrowseArtiststyles.head_b}`}>
        <div className="">
          <div className={`row m0 pt-3 pb-3 ${BrowseArtiststyles.s__row}`}>
            <div className="col-12 col-sm-3 col-lg-3 vcenter">
              <h2
                className={`text-white ${BrowseArtiststyles.browse_section_title}`}
              >
                Active Venues
              </h2>
            </div>
            <div
              className={`col-12 col-sm-6 col-lg-6  ${BrowseArtiststyles.s__row__column}`}
            >
              <div className={`${BrowseArtiststyles.head_b__search_form}`}>
                <input
                  type="search"
                  placeholder="Search for Venue"
                  className={`search_art aos-init aos-animate ${BrowseArtiststyles.border_b}`}
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onChange={(e: any) => {
                    handleVenueFilter({
                      search: e.target?.value || "",
                    });
                  }}
                />
                <div className="input-wrap second col-12 col-sm-6  col-lg-6">
                  <Select
                    value={defaultVenueOption}
                    options={locationOption}
                    placeholder="Location..."
                    onChange={(e) => {
                      if (e?.value) {
                        setDefaultVenueOption(e);
                        handleVenueLocation({
                          location: e.value,
                        });
                      } else {
                        handleVenueLocation({});
                        setDefaultVenueOption(null);
                      }
                    }}
                    styles={{
                      control: (baseStyles, state) => ({
                        ...baseStyles,
                        border: 0,
                        boxShadow: "none",
                      }),
                    }}
                    className={BrowseArtiststyles.select}
                    classNamePrefix="react-select"
                    isClearable
                  />
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-3 col-lg-3 "></div>
          </div>
        </div>
      </section> */}
    </>
  );
};
export default BrowseArtistScene;
