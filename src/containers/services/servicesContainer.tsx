import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import ServicesScene from "./servicesScene";

const ServicesContainer = () => {
  return (
    <>
      <SEO
        {...{
          pageName: "/services",
        }}
      />
      <ServicesScene />
    </>
  );
};

ServicesContainer.Layout = MainLayoutComponent;
export default ServicesContainer;
