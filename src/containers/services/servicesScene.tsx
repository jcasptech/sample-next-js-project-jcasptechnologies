import Head from "next/head";
import Script from "next/script";
import { useEffect, useState } from "react";

export interface ServicesSceneProps {}

const ServicesScene = (props: ServicesSceneProps) => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // Initialize embedded code here
    // Initialize and load external analytics script here
    const script = document.createElement("script");
    script.src = "//embed.typeform.com/next/embed.js";
    script.async = true;
    document.head.appendChild(script);
  }, []);

  return (
    <>
      <Head>
        <title>JCasp Services</title>
      </Head>

      <div
        data-tf-widget="C4owmVsr"
        data-tf-opacity="100"
        data-tf-iframe-props="title=JCasp Services"
        data-tf-transitive-search-params
        data-tf-medium="snippet"
        style={{ width: "100%", height: "600px" }}
      ></div>
    </>
  );
};
export default ServicesScene;
