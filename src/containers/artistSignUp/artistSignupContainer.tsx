import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { ArtistSignUpAPI } from "@redux/services/artist.api";
import router from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ArtistSignupFormInputs,
  ArtistSignupFormValidateSchema,
} from "src/schemas/artistSignupFormSchema";
import ArtistSignUpPage from "./artistSignUpScene";

const ArtistSignupContainer = () => {
  const dispatch = useDispatch();

  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<ArtistSignupFormInputs>({
      resolver: yupResolver(ArtistSignupFormValidateSchema),
    });

  const [isLoading, setIsLoading] = useState(false);
  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      await ArtistSignUpAPI(data);
      showToast(SUCCESS_MESSAGES.artistSignUpSuccess, "success");
      reset();
      setIsLoading(false);
      router.push(`/login`);
    } catch (error: any) {
      setIsLoading(false);
    }
  };
  return (
    <>
      <ArtistSignUpPage
        {...{
          handleSubmit,
          onSubmit,
          register,
          formState,
          isLoading,
        }}
      />
    </>
  );
};

ArtistSignupContainer.Layout = MainLayoutComponent;

export default ArtistSignupContainer;
