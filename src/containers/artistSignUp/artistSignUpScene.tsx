import artistSignupStyles from "./artistSignup.module.scss";
import Link from "next/link";
import ArtistRating from "@components/artistRatingComponent/artistRating";
import Image from "next/image";
import { toast } from "react-toastify";
import { Button } from "@components/theme/button";
import Spinner from "@components/theme/spinner";
import {
  InputField,
  InputPasswordField,
  InputPasswordFieldWithMessage,
} from "@components/theme/form/formFieldsComponent";
import { useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

export interface ArtistSignUpProps {
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  register: any;
  formState: any;
  isLoading: boolean;
}

const ArtistSignUpPage = (props: ArtistSignUpProps) => {
  const { register, formState, handleSubmit, onSubmit, isLoading } = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <section className="section-pad">
        <div className="container-fluid">
          <div className={`row m0 ${artistSignupStyles.main_box}`}>
            <div
              className={`col-12 col-sm-6 ${artistSignupStyles.account_box}`}
            >
              <div className={`row m0 ${artistSignupStyles.border_box}`}>
                <div className={`col-12 ${artistSignupStyles.abLbl}`}>
                  <h3
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Create an Artist Profile
                  </h3>
                  <p
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Fill out form to get started as an Artist
                  </p>
                </div>

                <div
                  className="col-12 form_holder"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <form
                    className={`${artistSignupStyles.formB}`}
                    onSubmit={handleSubmit(onSubmit)}
                  >
                    <div className={`${artistSignupStyles.formB_fieldH}`}>
                      <div className={artistSignupStyles.formFl}>
                        <div
                          className={`${artistSignupStyles.formFl__firstInput}`}
                        >
                          <InputField
                            {...{
                              register,
                              formState,
                              id: "firstname",
                              label: "First Name",
                              className: `${artistSignupStyles.input}`,
                              placeholder: "Enter first name",
                            }}
                          />
                        </div>
                        <div
                          className={`${artistSignupStyles.formFl__lastInput}`}
                        >
                          <InputField
                            {...{
                              register,
                              formState,
                              id: "lastname",
                              label: "Last Name",
                              className: `${artistSignupStyles.input}`,
                              placeholder: "Enter last name",
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className={`${artistSignupStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "username",
                          label: "Create Username",
                          className: `${artistSignupStyles.input}`,
                          placeholder: "Enter username",
                        }}
                      />
                    </div>
                    <div className={`${artistSignupStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "email",
                          label: "Email ID",
                          className: `${artistSignupStyles.input}`,
                          placeholder: "Enter your Email ID",
                        }}
                      />
                    </div>
                    <div className={`${artistSignupStyles.formB_fieldH}`}>
                      <InputPasswordFieldWithMessage
                        {...{
                          register,
                          formState,
                          id: "password",
                          label: "Create Password",
                          className: `${artistSignupStyles.input}`,
                          placeholder: "Enter your Password",
                        }}
                      />
                    </div>
                    <div className={`${artistSignupStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "referral",
                          label: "Referral/How Did You Hear About Us",
                          className: `${artistSignupStyles.input}`,
                          placeholder: "Instagram, Venue, Friend, etc.",
                        }}
                      />
                    </div>
                    <div
                      className={`${artistSignupStyles.formB_fieldH} ${artistSignupStyles.centerDiv} mob-ctr`}
                    >
                      <Button
                        htmlType="submit"
                        disabled={formState?.isSubmitting || isLoading}
                        loading={formState?.isSubmitting || isLoading}
                      >
                        Sign Up
                      </Button>
                    </div>
                    <div
                      className={`${artistSignupStyles.formB_fieldH} ${artistSignupStyles.centerDiv} reglnk mob-ctr`}
                    >
                      <p>
                        Already have an account?{" "}
                        <Link href="/login" className="">
                          Log In
                        </Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            {/* <--Artist Rating--> */}
            {/* <ArtistRating {...{ redirectPage: "/login" }} /> */}
            {/* <--Artist Rating--> */}
          </div>
        </div>
      </section>
    </>
  );
};
export default ArtistSignUpPage;
