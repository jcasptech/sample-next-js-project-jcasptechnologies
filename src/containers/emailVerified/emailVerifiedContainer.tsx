import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { nextRedirect } from "src/libs/helpers";
import EmailVerifiedScene from "./emailVerifiedScene";

export interface EmailVerifiedProps {
  username: string;
}

const EmailVerifiedContainer = (Props: EmailVerifiedProps) => {
  const { username } = Props;
  return (
    <EmailVerifiedScene
      {...{
        username,
      }}
    />
  );
};

EmailVerifiedContainer.getInitialProps = async (ctx: any) => {
  const username = ctx.query?.username;
  if (username) {
    return { username };
  } else {
    nextRedirect({
      ctx,
      location: "/login",
    });
  }
};

EmailVerifiedContainer.Layout = MainLayoutComponent;
export default EmailVerifiedContainer;
