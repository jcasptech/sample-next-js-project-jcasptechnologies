import ArtistRating from "@components/artistRatingComponent/artistRating";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";
import EmailVerifiedStyles from "./emailVerified.module.scss";

export interface EmailVerifiedProps {
  username: string;
}

const EmailVerifiedScene = (props: EmailVerifiedProps) => {
  const { username } = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <section className="section-pad">
        <div className={`container-fluid ${EmailVerifiedStyles.main_box}`}>
          <div className="row m0">
            <div
              className={`col-12 col-sm-6 ${EmailVerifiedStyles.account_box}`}
            >
              <div className={`row m0 ${EmailVerifiedStyles.borderDiv}`}>
                <div className={`col-12 ${EmailVerifiedStyles.abLbl}`}>
                  <h3
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Successfully verified your email!
                  </h3>
                </div>

                <div
                  className="col-12 form_holder"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <div
                    className={`${EmailVerifiedStyles.formB_fieldH} reglnk mob-ctr`}
                  >
                    <Image
                      src="/images/success.png"
                      alt="Success"
                      width={193}
                      height={193}
                    />

                    <p className={`${EmailVerifiedStyles.submitBtn}`}>
                      Log into an account?{" "}
                      <Link href="/login" className="ps-1">
                        Sign in
                      </Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default EmailVerifiedScene;
