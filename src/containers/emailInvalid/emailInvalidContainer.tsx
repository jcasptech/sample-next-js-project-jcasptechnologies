import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { nextRedirect } from "src/libs/helpers";
import EmailInvalidScene from "./emailInvalidScene";

export interface EmailInvalidProps {
  username: string;
}

const EmailInvalidContainer = (Props: EmailInvalidProps) => {
  const { username } = Props;
  return (
    <EmailInvalidScene
      {...{
        username,
      }}
    />
  );
};

EmailInvalidContainer.getInitialProps = async (ctx: any) => {
  const username = ctx.query?.username;
  if (username) {
    return { username };
  } else {
    nextRedirect({
      ctx,
      location: "/login",
    });
  }
};

EmailInvalidContainer.Layout = MainLayoutComponent;
export default EmailInvalidContainer;
