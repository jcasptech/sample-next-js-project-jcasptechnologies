import { Button } from "@components/theme/button";
import { Col, Row } from "antd";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import router from "next/router";
import { useEffect } from "react";
import EmailInvalidStyles from "./emailInvalid.module.scss";

export interface EmailInvalidProps {
  username: string;
}

const EmailInvalidScene = (props: EmailInvalidProps) => {
  const { username } = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <div className={`main-section ${EmailInvalidStyles.notfound_background}`}>
        <section className="section">
          <Row justify={"center"} align="middle" className="h-100">
            <Col span={24} className="text-center">
              <Image
                src={"/images/general/logo.png"}
                width={100}
                height={100}
                alt="JCasp"
              />

              <h2>Invalid email verification link!</h2>
              <p>Something went wrong</p>
              <Button
                htmlType="button"
                type="primary"
                onClick={() => router.push("/login")}
              >
                Sign in
              </Button>
            </Col>
          </Row>
        </section>
        <Image
          src={"/images/general/aux.png"}
          width={40}
          height={512}
          alt="JCasp"
          className="backImage"
        />
      </div>
    </>
  );
};

export default EmailInvalidScene;
