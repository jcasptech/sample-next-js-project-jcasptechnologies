import ConfirmPostingCard from "@components/confirmPostingCard/confirmPostingCard";
import ConfirmPostingSelectEvent from "@components/confirmPostingCard/confirmPostingSelectEvent";
import ActivePostingCard from "@components/postingCard/activePostingCard";
import ActivePostingSelectEvent from "@components/postingCard/activePostingSelectEvent";
import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { ConfirmPostingDataResponse } from "@redux/slices/confirmPosting";
import Image from "next/image";
import Link from "next/link";
import React from "preact/compat";
import confirmPostingStyles from "./confirmPosting.module.scss";

export interface ConfirmPostingSceneProps {
  confirmPostingData: ConfirmPostingDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  selectEventId: string;
  setSelectEventId: (d: any) => void;
}
const ConfirmPostingScene = (props: ConfirmPostingSceneProps) => {
  const {
    confirmPostingData,
    handleLoadMore,
    isLoading,
    selectEventId,
    setSelectEventId,
  } = props;

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="row m0 final-ap">
            <div className="col-12 ">
              <div className="row p-0">
                <div className="col-12 col-sm-12 col-lg-7 posting-list">
                  <div className="row">
                    {confirmPostingData &&
                      confirmPostingData?.list?.map((postData: any, index) => (
                        <ConfirmPostingCard
                          key={index}
                          {...{
                            postData,
                            setSelectEventId,
                          }}
                        />
                      ))}
                    {confirmPostingData?.list &&
                      confirmPostingData?.list?.length <= 0 && (
                        <EmptyMessage description="No postings found." />
                      )}

                    {confirmPostingData.hasMany && (
                      <div className="col-12 load_more">
                        <Button
                          type="ghost"
                          htmlType="button"
                          loading={isLoading}
                          disabled={isLoading}
                          onClick={(e) => {
                            e.preventDefault();
                            handleLoadMore({
                              loadMore: true,
                            });
                          }}
                        >
                          Load more Post
                        </Button>
                      </div>
                    )}
                  </div>
                  <div id="postingDetails"></div>
                </div>

                <div className="col-12 col-sm-12 col-lg-4 viewPost">
                  {selectEventId !== "" ? (
                    <>
                      <div className="col-12 ptitle p-0">
                        <h3>Your Selected Event</h3>
                      </div>
                      <ConfirmPostingSelectEvent
                        {...{
                          selectEventId,
                        }}
                      />
                    </>
                  ) : (
                    <div className={confirmPostingStyles.unSelectedData}>
                      <Image
                        src="/images/general/selectPost.svg"
                        alt=""
                        width={70}
                        height={70}
                      />
                      <p>
                        Please select any event to see further details of that
                        event
                      </p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ConfirmPostingScene;
