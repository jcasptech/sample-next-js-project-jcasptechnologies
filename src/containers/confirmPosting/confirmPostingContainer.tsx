import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import ConfirmPostingScene from "./confirmPostingScene";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@redux/reducers";
import {
  fetchConfirmPosting,
  ConfirmPostingState,
  loadMoreConfirmPosting,
} from "@redux/slices/confirmPosting";
import React, { useEffect, useState } from "react";
import useList from "src/libs/useList";
import { DefaultSkeleton } from "@components/theme";
import { Show } from "react-redux-permission";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";

const ConfirmPostingContainer = () => {
  const dispatch = useDispatch();
  const [selectEventId, setSelectEventId] = useState("");

  const {
    confirmPosting: { isLoading, data: confirmPostingData },
  }: {
    confirmPosting: ConfirmPostingState;
  } = useSelector((state: RootState) => ({
    confirmPosting: state.confirmPosting,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
      include: ["selectedArtist"],
      orderByColumn: "startDate",
      orderBy: "ASCENDING",
    },
  });

  useEffect(() => {
    dispatch(fetchConfirmPosting(apiParam));
  }, [apiParam]);

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 8;
    dispatch(loadMoreConfirmPosting(apiParam));
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["confirm_posting"]} fallback={<NoPermissionsComponent />}>
        <ConfirmPostingScene
          {...{
            confirmPostingData,
            handleLoadMore,
            isLoading,
            selectEventId,
            setSelectEventId,
          }}
        />
      </Show>
    </>
  );
};

ConfirmPostingContainer.Layout = AdminLayoutComponent;
export default ConfirmPostingContainer;
