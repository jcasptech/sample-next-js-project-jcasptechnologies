import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { resetPasswordAPI } from "@redux/services/auth.api";
import { ResetPasswordPayload } from "@redux/slices/auth";
import { useReCaptcha } from "next-recaptcha-v3";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { nextRedirect } from "src/libs/helpers";
import {
  ResetPasswordFormInputs,
  ResetPasswordFormValidateSchema,
} from "src/schemas/resetPasswordFormSchema";
import ResetPassword from "./resetPasswordScene";

export interface ResetPasswordProps {
  token: string;
  password: string;
  confirmPassword: string;
}

const ResetPasswordContainer = (Props: ResetPasswordProps) => {
  const { token } = Props;
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const { executeRecaptcha, loaded } = useReCaptcha();
  const router = useRouter();

  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<ResetPasswordFormInputs>({
      resolver: yupResolver(ResetPasswordFormValidateSchema),
    });

  const onSubmit = async (data: ResetPasswordPayload) => {
    setIsLoading(true);
    try {
      const recaptchaResponse = await executeRecaptcha("reset_password");
      await resetPasswordAPI(
        {
          password: data.password,
          confirmPassword: data.confirmPassword,
          token: token,
        },
        recaptchaResponse
      );
      showToast(SUCCESS_MESSAGES.resetPassword, "success");
      reset();
      router.push("/login");
      setIsLoading(false);
    } catch (error: any) {
      setIsLoading(false);
    }
  };
  return (
    <ResetPassword
      {...{
        handleSubmit,
        onSubmit,
        register,
        formState,
        isLoading,
        isSuccess,
      }}
    />
  );
};

ResetPasswordContainer.getInitialProps = async (ctx: any) => {
  const resetToken = ctx.query.token;
  if (resetToken) {
    return { token: resetToken };
  } else {
    nextRedirect({
      ctx,
      location: "/login",
    });
  }
};

ResetPasswordContainer.Layout = MainLayoutComponent;
export default ResetPasswordContainer;
