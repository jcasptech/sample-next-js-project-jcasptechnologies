import ArtistRating from "@components/artistRatingComponent/artistRating";
import Link from "next/link";
import { useEffect, useState } from "react";
import ResetPasswordStyles from "./resetPassword.module.scss";
import Image from "next/image";
import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { Button } from "@components/theme/button";
import Spinner from "@components/theme/spinner";
import {
  InputPasswordField,
  InputPasswordFieldWithMessage,
} from "@components/theme/form/formFieldsComponent";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import AOS from "aos";
import "aos/dist/aos.css";

export interface ResetPasswordProps {
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  register: any;
  formState: any;
  isLoading: boolean;
  isSuccess: boolean;
}

const ResetPasswordScense = (props: ResetPasswordProps) => {
  const { register, formState, handleSubmit, onSubmit, isLoading, isSuccess } =
    props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <section className="section-pad">
        <div className="container-fluid">
          <div className="row m0">
            {isSuccess ? (
              <div
                className={`col-12 col-sm-6 ${ResetPasswordStyles.account_image}`}
              >
                <div className={`col-12 ${ResetPasswordStyles.abLbl}`}>
                  <h3
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  ></h3>
                  <p
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    {SUCCESS_MESSAGES.resetPassword}
                  </p>
                </div>
                {/* <img src="/images/success.png" alt="artist name" /> */}
                <Image
                  src="/images/success.png"
                  alt="artist name"
                  height={193}
                  width={193}
                  className="auto"
                />
              </div>
            ) : (
              <div
                className={`col-12 col-sm-6 ${ResetPasswordStyles.account_box}`}
              >
                <div className="row m0">
                  <div className={`col-12 ${ResetPasswordStyles.abLbl}`}>
                    <h3
                      data-aos="fade-down"
                      data-aos-delay="100"
                      data-aos-duration="1200"
                    >
                      Reset Your Password
                    </h3>
                    <p
                      data-aos="fade-up"
                      data-aos-delay="100"
                      data-aos-duration="1200"
                    >
                      Enter your new password.
                    </p>
                  </div>
                  <div
                    className="col-12 form_holder"
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    <form
                      className={`${ResetPasswordStyles.formB}`}
                      onSubmit={handleSubmit(onSubmit)}
                    >
                      <div className={`${ResetPasswordStyles.formB_fieldH}`}>
                        <InputPasswordFieldWithMessage
                          {...{
                            register,
                            formState,
                            id: "password",
                            label: "New Password",
                            className: `${ResetPasswordStyles.input}`,
                            placeholder: "Enter a new Password",
                          }}
                        />
                      </div>
                      <div className={`${ResetPasswordStyles.formB_fieldH}`}>
                        <InputPasswordField
                          {...{
                            register,
                            formState,
                            id: "confirmPassword",
                            label: "Confirm Password",
                            className: `${ResetPasswordStyles.input}`,
                            placeholder: "Enter confirm your Password",
                          }}
                        />
                      </div>

                      <div
                        className={`${ResetPasswordStyles.formB_fieldH} mob-ctr`}
                      >
                        <Button
                          htmlType="submit"
                          type="primary"
                          disabled={formState?.isSubmitting || isLoading}
                          loading={formState?.isSubmitting || isLoading}
                        >
                          Change Password
                        </Button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            )}

            {/* <--Artist Rating--> */}
            <ArtistRating {...{ redirectPage: "/login" }} />
            {/* <--Artist Rating--> */}
          </div>
        </div>
      </section>
    </>
  );
};

export default ResetPasswordScense;
