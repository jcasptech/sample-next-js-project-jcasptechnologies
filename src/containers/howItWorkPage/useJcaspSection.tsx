import Image from "next/image";
import howIsWorkStyle from "./howItWork.module.scss";

const UseJCaspSection = () => {
  return (
    <>
      <section className={`section-pad ${howIsWorkStyle.useGig}`}>
        <div className="container-fluid">
          <div className="row m0">
            <div className="col-md-3"></div>
            <div className="col-md-6 h-head">
              <h2>
                How to Use JCasp: A Guide <br />
                for Artists, Venues, and Fans
              </h2>
            </div>
            <div className="col-md-3"></div>
          </div>
          <div className="row m0">
            <div className="col-md-4 col-lg-4"></div>

            <div className="show_tabes col-md-4 col-lg-4 nav-wrapper position-relative mb-2">
              <ul
                className="nav nav-pilles nav-fill flex-md-row"
                id="tabs-text"
                role="tablist"
              >
                <li className="nav-item tab-h">
                  <a
                    className="nav-link mb-sm-3 mb-md-0 active"
                    id="tabs-text-1-tab"
                    data-bs-toggle="tab"
                    href="#tabs-text-1"
                    role="tab"
                    aria-controls="tabs-text-1"
                    aria-selected="true"
                  >
                    For General Users
                  </a>
                </li>
                <li className="nav-item tab-h">
                  <a
                    className="nav-link mb-sm-3 mb-md-0 "
                    id="tabs-text-2-tab"
                    data-bs-toggle="tab"
                    href="#tabs-text-2"
                    role="tab"
                    aria-controls="tabs-text-2"
                    aria-selected="false"
                  >
                    For Artists
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-4 col-lg-4"></div>
          </div>
        </div>

        <div className="container-fluid ">
          <div className="tab-content" id="tabcontent1">
            <div
              className="tab-pane fade active show"
              id="tabs-text-1"
              role="tabpanel"
              aria-labelledby="tabs-text-1-tab"
            >
              <div className="row m0 guide">
                <div className="path">
                  <Image
                    src="/images/about-us/Path.png"
                    alt="Path"
                    className="h-auto"
                    width={1338}
                    height={131}
                  />
                </div>
                <div className="col-md-3 col-sm-12 col-xs-12 sec-1">
                  <Image
                    src="/images/about-us/create-profile.png"
                    alt="Create a Profile"
                    className="h-auto"
                    width={156}
                    height={144}
                  />
                  <h3>Create a Profile</h3>
                  <p>
                    When you sign up for JCasp, you&lsquo;ll create a free
                    artist profile. We&apos;ll ask you to upload a hi-res photo,
                    at least one YouTube link, and a bio. And don&lsquo;t forget
                    to provide your payment address so you can receive payment
                    for gigs.
                  </p>
                </div>

                <div className="col-md-3 sec-1">
                  <Image
                    src="/images/about-us/get-active.png"
                    alt="Active"
                    className="h-auto"
                    width={156}
                    height={144}
                  />
                  <h3>Get Active</h3>
                  <p>
                    Once your profile is approved, you&lsquo;ll be visible and
                    active on the website. Visitors can send you messages and
                    inquiries, and you can submit for open gigs by browsing
                    opportunities and sending the host a message.
                  </p>
                </div>

                <div className="col-md-3 sec-1">
                  <Image
                    src="/images/about-us/boost-visible.png"
                    alt="Boost Your Visibility"
                    className="h-auto"
                    width={156}
                    height={144}
                  />
                  <h3>Boost Your Visibility</h3>
                  <p>
                    Want to increase your visibility? You can add multiple
                    photos, YouTube videos, and upcoming shows to your profile.
                    The more complete your profile, the better! And don&lsquo;t
                    forget, factors that contribute to higher visibility include
                    total upcoming shows, total past shows, subscriber count,
                    and profile completeness.
                  </p>
                </div>

                <div className="col-md-3 sec-1">
                  <Image
                    src="/images/about-us/use-tools.png"
                    alt="Use JCasp"
                    className="h-auto"
                    width={156}
                    height={144}
                  />
                  <h3>Use JCasp&apos;s Tools</h3>
                  <p>
                    JCasp offers a variety of features to help you make the
                    most of your experience on the platform. Access your work
                    calendar to add your own shows, which will be promoted on
                    the JCasp app and website. You can also use the earnings
                    tracking, open postings, and event details features for
                    JCasp Pro events. It&lsquo;s all free and designed to help
                    you succeed!
                  </p>
                </div>
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="tabs-text-2"
              role="tabpanel"
              aria-labelledby="tabs-text-2-tab"
            >
              <div className="row m0 guide">
                <div className="path">
                  <Image
                    src="/images/about-us/Path.png"
                    alt="Path"
                    className="h-auto"
                    width={1338}
                    height={131}
                  />
                </div>
                <div className="col-md-3 col-sm-12 col-xs-12 sec-1">
                  <Image
                    src="/images/about-us/create-profile.png"
                    alt="Create a Profile"
                    className="h-auto"
                    width={156}
                    height={144}
                  />
                  <h3>Create a Profile</h3>
                  <p>
                    When you sign up for JCasp, you&lsquo;ll create a free
                    artist profile. We&apos;ll ask you to upload a hi-res photo,
                    at least one YouTube link, and a bio. And don&lsquo;t forget
                    to provide your payment address so you can receive payment
                    for gigs.
                  </p>
                </div>

                <div className="col-md-3 sec-1">
                  <Image
                    src="/images/about-us/get-active.png"
                    alt="Active"
                    className="h-auto"
                    width={156}
                    height={144}
                  />
                  <h3>Get Active</h3>
                  <p>
                    Once your profile is approved, you&lsquo;ll be visible and
                    active on the website. Visitors can send you messages and
                    inquiries, and you can submit for open gigs by browsing
                    opportunities and sending the host a message.
                  </p>
                </div>

                <div className="col-md-3 sec-1">
                  <Image
                    src="/images/about-us/boost-visible.png"
                    alt="Boost Your Visibility"
                    className="h-auto"
                    width={156}
                    height={144}
                  />

                  <h3>Boost Your Visibility</h3>
                  <p>
                    Want to increase your visibility? You can add multiple
                    photos, YouTube videos, and upcoming shows to your profile.
                    The more complete your profile, the better! And don&lsquo;t
                    forget, factors that contribute to higher visibility include
                    total upcoming shows, total past shows, subscriber count,
                    and profile completeness.
                  </p>
                </div>

                <div className="col-md-3 sec-1">
                  <Image
                    src="/images/about-us/use-tools.png"
                    alt="Use JCasp"
                    className="h-auto"
                    width={156}
                    height={144}
                  />
                  <h3>Use JCasp&apos;s Tools</h3>
                  <p>
                    JCasp offers a variety of features to help you make the
                    most of your experience on the platform. Access your work
                    calendar to add your own shows, which will be promoted on
                    the JCasp app and website. You can also use the earnings
                    tracking, open postings, and event details features for
                    JCasp Pro events. It&lsquo;s all free and designed to help
                    you succeed!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default UseJCaspSection;
