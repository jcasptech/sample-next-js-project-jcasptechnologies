import Image from "next/image";
import howIsWorkStyle from "./howItWork.module.scss";

const FocusOnEfficiencySection = () => {
  return (
    <>
      <section className={`${howIsWorkStyle.extra_pad}`}>
        <div className="container-fluid sec-3">
          <div className="row m0">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <h2>We focus on efficiency</h2>
            </div>
            <div className="col-md-4"></div>
          </div>
          <div className="row m0">
            <div className="col-md-3 col-sm-12 col-xs-12"></div>
            <div className="col-md-6 col-sm-12 col-xs-12 sec-3-p">
              <p>
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
                The point of using Lorem Ipsum is that it has a more-or-less
              </p>
            </div>
            <div className="col-md-3"></div>
          </div>
          <div className="row m0">
            <div className="col-md-2 none "></div>
            <div className="col-md-4 card-pad">
              <div className="row  black-bg">
                <div className="col-md-3  col-xs-3 circle-h">
                  <div className="circle-small">
                    <div className="text">
                      78%
                      <br />
                      <span className="small"></span>
                    </div>
                    <svg>
                      <circle className="bg" cx="40" cy="40" r="37"></circle>
                      <circle
                        className="progress two"
                        cx="40"
                        cy="40"
                        r="37"
                      ></circle>
                    </svg>
                  </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-9 para-h">
                  <p>
                    It is a long established fact that a reader will be
                    distracted by the readable content of a page
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-4 card-pad">
              <div className="row  black-bg">
                <div className="col-md-3 col-sm-3  col-xs-3 circle-h">
                  <div className="circle-small">
                    <div className="text">
                      65%
                      <br />
                      <span className="small"></span>
                    </div>
                    <svg>
                      <circle className="bg" cx="40" cy="40" r="37"></circle>
                      <circle
                        className="progress three"
                        cx="40"
                        cy="40"
                        r="37"
                      ></circle>
                    </svg>
                  </div>
                </div>
                <div className="col-md-9  col-sm-9 col-xs-9 para-h">
                  <p>
                    It is a long established fact that a reader will be
                    distracted by the readable content of a page
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-2 none"></div>
          </div>
          <div className="row m0">
            <div className="col-md-2 none"></div>
            <div className="col-md-4 card-pad">
              <div className="row  black-bg">
                <div className="col-md-3 circle-h">
                  <div className="circle-small">
                    <div className="text">
                      10%
                      <br />
                      <span className="small"></span>
                    </div>
                    <svg>
                      <circle className="bg" cx="40" cy="40" r="37"></circle>
                      <circle
                        className="progress four"
                        cx="40"
                        cy="40"
                        r="37"
                      ></circle>
                    </svg>
                  </div>
                </div>
                <div className="col-md-9 para-h">
                  <p>
                    It is a long established fact that a reader will be
                    distracted by the readable content of a page
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-4 card-pad">
              <div className="row  black-bg">
                <div className="col-md-3 circle-h">
                  <div className="circle-small">
                    <div className="text">
                      99%
                      <br />
                      <span className="small"></span>
                    </div>
                    <svg>
                      <circle className="bg" cx="40" cy="40" r="37"></circle>
                      <circle
                        className="progress one"
                        cx="40"
                        cy="40"
                        r="37"
                      ></circle>
                    </svg>
                  </div>
                </div>
                <div className="col-md-9 para-h">
                  <p>
                    It is a long established fact that a reader will be
                    distracted by the readable content of a page
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-2 none"></div>
          </div>
          <div className="line-h">
            <hr />
          </div>
          <div className="row m0 ">
            <div className="col-md-1"></div>
            <div className="col-md-4 img-bottom">
              <Image
                src="/images/about-us/ceo.png"
                alt="CEO"
                className="h-auto"
                width={561}
                height={530}
              />
            </div>
            <div className="col-md-6 create-posting-text">
              <div className="row mo">
                <div className="col-md-1 col-2">
                  <Image
                    src="/images/about-us/inverted-comma.png"
                    alt=""
                    className="h-auto"
                    width={32}
                    height={22}
                  />
                </div>
                <div className="col-md-11 para-bottom col-10">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nunc vulputate libero et velit interdum, ac aliquet odio
                    mattis.Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit. Nunc vulputate libero et velit interdum, ac aliquet
                    odio mattis.
                  </p>

                  <p>
                    {/* <Image
                      src="/images/about-us/Line.png"
                      alt=""
                      className="h-auto"
                      width={109}
                      height={11}
                    /> */}
                    CEO Music Band Company
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-1"></div>
          </div>
        </div>
      </section>
    </>
  );
};

export default FocusOnEfficiencySection;
