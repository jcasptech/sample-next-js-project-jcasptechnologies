import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import HowitWorkScene from "./howItWorkScene";

const HowitWorkContainer = () => {
  return (
    <>
      <HowitWorkScene />
    </>
  );
};

HowitWorkContainer.Layout = MainLayoutComponent;

export default HowitWorkContainer;
