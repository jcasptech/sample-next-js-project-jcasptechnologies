import ApplicationDetalis from "@components/applicationDetails/applicationDetalis";
import NewsLetterSection from "@components/newsLetter";
import FocusOnEfficiencySection from "./focusOnEfficiencySection";
import howIsWorkStyle from "./howItWork.module.scss";
import TheMostJCaspSection from "./makeTheMostJCasp";
import UseJCaspSection from "./useJCaspSection";

const HowitWorkScene = () => {
  return (
    <>
      <section className={`section-pad ${howIsWorkStyle.pageHeader}`}>
        <div className="container-fluid">
          <div className="row m-0">
            <div className="col-12 pHead">
              <h1>How JCasp Helps</h1>
            </div>
          </div>
        </div>
      </section>
      <UseJCaspSection />
      <TheMostJCaspSection />
      <FocusOnEfficiencySection />
      <div className={`download_app`}>
        <ApplicationDetalis />
      </div>
      <NewsLetterSection />
    </>
  );
};
export default HowitWorkScene;
