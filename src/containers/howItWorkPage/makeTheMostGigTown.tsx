import howIsWorkStyle from "./howItWork.module.scss";
import Link from "next/link";
import { Col, Row } from "antd";
import Image from "next/image";

const TheMostJCaspSection = () => {
  return (
    <>
      <section className="section-pad pt-0">
        <div className={`container-fluid ${howIsWorkStyle.sec_two}`}>
          <div className="row m0">
            <div className="col-md-12">
              <h2>Make the Most of JCasp</h2>
            </div>
            <div className="col-md-4"></div>
          </div>
          <div className="row m0">
            <div className="col-md-3"></div>
            <div className="col-md-6">
              <p>
                The best way for working musicians to promote, organize, and
                grow their music career with an artist-friendly platform,
                verified booking opportunities, earnings tracking, and over $3
                million in artist earnings to date.
              </p>
            </div>
            <div className="col-md-3"></div>
          </div>

          <Row className="cp-1" align="middle">
            <Col xs={24} md={12} xl={12} className="create-posting-img-1">
              <Image
                src="/images/about-us/create-posting-1.png"
                alt="posting"
                width={900}
                height={700}
              />
            </Col>
            <Col xs={24} md={12} xl={12} className="create-posting-text-1">
              <p className="text-primary">Create Posting</p>
              <h3>Choose Artist Format & Music Style</h3>
              <p>
                As an artist on JCasp, you can browse through open gig
                opportunities listed on the platform. Open postings include
                important details like the date, time, general location, and
                performance fee, to help you determine if the gig is right for
                you. READ MORE There are two types of gig postings: Regular
                postings, where a user creates a posting for an event, and
                JCasp Pro postings, created by JCasp&apos;s internal booking
                team. JCasp Pro postings are verified and come with guaranteed
                payments, processed securely through the platform. Learn More
                about JCasp PRO.
              </p>

              <div className="col-md-12">
                <Link className="arrow" href="/create-postings">
                  Browse Postings{" "}
                  <Image
                    height={10}
                    width={17}
                    src="/images/about-us/orange-Arrow.png"
                    alt="Arrow"
                  />
                </Link>
              </div>
              <Image
                width={110}
                height={11}
                src="/images/about-us/underline.png"
                alt="underline"
              />
            </Col>
          </Row>

          <Row className="cp-2" align="middle">
            <Col xs={24} md={12} xl={12} className="create-posting-img-mobile">
              <Image
                width={767}
                height={700}
                src="/images/about-us/create-posting-2.png"
                alt="posting"
              />
            </Col>
            <Col xs={24} md={12} xl={12} className="create-posting-text-2 ">
              <p className="text-primary">Create Posting</p>
              <h3>Organize and promote your gig schedule</h3>
              <p>
                JCasp&apos;s built-in Gig Calendar is an essential tool for
                working musicians. It allows artists to organize their schedule,
                track earnings, submit for gigs, and promote upcoming events.
                Plus, with the ability to connect with subscribers and fans, the
                Gig Calendar is a great way to build and engage with your
                audience.
              </p>
              <div className="col-md-12">
                <Link className="arrow" href="/create-artist">
                  Create artist account{" "}
                  <Image
                    height={10}
                    width={17}
                    src="/images/about-us/orange-Arrow.png"
                    alt="orange"
                  />
                </Link>
              </div>

              <Image
                width={110}
                height={11}
                src="/images/about-us/underline.png"
                alt="underline"
              />
            </Col>
            <Col xs={24} md={12} xl={12} className="create-posting-img-desktop">
              <Image
                width={900}
                height={700}
                src="/images/about-us/create-posting-2.png"
                alt="posting"
              />
            </Col>
          </Row>

          <Row className="cp-3" align="middle">
            <Col xs={24} md={12} xl={12} className="create-posting-img-3">
              <Image
                width={900}
                height={700}
                src="/images/about-us/create-posting-3.png"
                alt="posting"
              />
            </Col>
            <Col xs={24} md={12} xl={12} className="create-posting-text-3">
              <p className="text-primary">Create Posting </p>
              <h3>Promote your Schedule</h3>
              <p>
                JCasp offers a range of powerful promotional features to help
                artists succeed, including an artist profile page on the mobile
                app and website, the ability to message subscribers, and to
                easily promote upcoming shows.
              </p>
              <div className="col-md-12">
                <Link className="arrow" href="/create-artist">
                  Create artist profile{" "}
                  <Image
                    height={10}
                    width={17}
                    src="/images/about-us/orange-Arrow.png"
                    alt="posting"
                  />
                </Link>
              </div>
              <Image
                width={110}
                height={11}
                src="/images/about-us/underline.png"
                alt="posting"
              />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
};
export default TheMostJCaspSection;
