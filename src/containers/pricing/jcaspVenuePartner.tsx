import Image from "next/image";
import pricingStyles from "./pricing.module.scss";
const JCaspVenuePartnerComponent = () => {
  return (
    <>
      <section className="section-pad venPart">
        <div className="container-fluid for-desktop">
          <div className="row m0">
            <div
              className="col-12 text-center"
              data-aos="fade-up"
              data-aos-delay="100"
              data-aos-duration="1200"
            >
              <h2 className={`sec-head ${pricingStyles.sec_head}`}>
                Venue partner with JCasp
              </h2>
            </div>

            <div className="row nest-hub">
              <div className="col-1 no-tablet"></div>
              <div
                className="col-6 col-md-3 col-lg-2 partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/viasat.png" alt="viasat" /> */}
                <Image
                  src="/images/viasat.png"
                  alt="viasat"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div
                className="col-6 col-md-3 col-lg-2 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/nuvasive_logo.png" alt="nuvasive_logo" /> */}
                <Image
                  src="/images/nuvasive_logo.png"
                  alt="nuvasive_logo"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div
                className="col-6  col-md-3 col-lg-2 partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/Qualcomm_logo.png" alt="Qualcomm_logo" /> */}
                <Image
                  src="/images/Qualcomm_logo.png"
                  alt="Qualcomm_logo"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div
                className="col-6  col-md-3 col-lg-2 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/westin-hotels.png" alt="westin-hotels" /> */}
                <Image
                  src="/images/westin-hotels.png"
                  alt="westin-hotels"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div
                className="col-6  col-md-3 col-lg-2 no-tablet no-nest partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/rixos-hotel.png" alt="rixos-hotel" /> */}
                <Image
                  src="/images/rixos-hotel.png"
                  alt="rixos-hotel"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div className="col-1 no-tablet"></div>
            </div>

            <div className="row nest-hub">
              <div className="col-2  no-tablet"></div>
              <div
                className="col-6 col-md-3 col-lg-2 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/sheraton-logo.png" alt="sheraton-logo" /> */}
                <Image
                  src="/images/sheraton-logo.png"
                  alt="sheraton-logo"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div
                className="col-6 col-md-3 col-lg-2  partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img
                  src="/images/shangri-la-hotel.png"
                  alt="shangri-la-hotel"
                /> */}
                <Image
                  src="/images/shangri-la-hotel.png"
                  alt="shangri-la-hotel"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div
                className="col-6 col-md-3 col-lg-2 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/royce-hotel.png" alt="royce-hotel" /> */}
                <Image
                  src="/images/royce-hotel.png"
                  alt="royce-hotel"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div className="col-12 col-md-3 col-lg-2 partner">
                {/* <img src="/images/hotel-hyatt.png" alt="hotel-hyatt" /> */}
                <Image
                  src="/images/hotel-hyatt.png"
                  alt="hotel-hyatt"
                  className="h-auto"
                  width={136}
                  height={63}
                />
              </div>
              <div className="col-2 no-tablet"></div>
            </div>
          </div>
        </div>
        <div className="container-fluid for-mobile">
          <div className="row m0">
            <div
              className="col-12 text-center"
              data-aos="fade-up"
              data-aos-delay="100"
              data-aos-duration="1200"
            >
              <h2 className={`sec-head ${pricingStyles.sec_head}`}>
                Venue partner with JCasp
              </h2>
            </div>

            <div className=" line">
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/viasat.png" alt="viasat" /> */}
                <Image
                  src="/images/viasat.png"
                  alt="viasat"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/nuvasive_logo.png" alt="nuvasive_logo" /> */}
                <Image
                  src="/images/nuvasive_logo.png"
                  alt="nuvasive_logo"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
            </div>

            <div className="line">
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/Qualcomm_logo.png" alt="Qualcomm_logo" /> */}
                <Image
                  src="/images/Qualcomm_logo.png"
                  alt="Qualcomm_logo"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/westin-hotels.png" alt="westin-hotels" /> */}
                <Image
                  src="/images/westin-hotels.png"
                  alt="westin-hotels"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
            </div>

            <div className="line">
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/rixos-hotel.png" alt="rixos-hotel" /> */}
                <Image
                  src="/images/rixos-hotel.png"
                  alt="rixos-hotel"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/sheraton-logo.png" alt="sheraton-logo" /> */}
                <Image
                  src="/images/sheraton-logo.png"
                  alt="sheraton-logo"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
            </div>

            <div className="line">
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img
                  src="/images/shangri-la-hotel.png"
                  alt="shangri-la-hotel"
                /> */}
                <Image
                  src="/images/shangri-la-hotel.png"
                  alt="shangri-la-hotel"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
              <div
                className="col-6 col-sm-3 partner"
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {/* <img src="/images/royce-hotel.png" alt="royce-hotel" /> */}
                <Image
                  src="/images/royce-hotel.png"
                  alt="royce-hotel"
                  className="h-auto"
                  width={140}
                  height={65}
                />
              </div>
            </div>
            {/* <div className="col-6 col-sm-3 partner">
              <img src="/images/hotel-hyatt.png" alt="hotel-hyatt" />
            </div> */}
          </div>
        </div>
      </section>
      <section className="mb-4">
        <div className="container-fluid">
          <div className={`row m0 faq_box pt-2 `}>
            <div
              className="col-12 col-sm-4 mb-4"
              data-aos="fade-up"
              data-aos-delay="100"
              data-aos-duration="1200"
            >
              <h2 className="sec-head">
                Frequently Asked Questions about JCasp Pro
              </h2>
            </div>
            <div
              className="col-12 col-sm-8"
              data-aos="fade-down"
              data-aos-delay="100"
              data-aos-duration="1200"
            >
              <div className="accordion" id="accordionExample">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingOne">
                    <button
                      className={`${pricingStyles.questionBorber} accordion-button collapsed `}
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Is there a free trial available ?
                    </button>
                  </h2>
                  <div
                    id="collapseOne"
                    className="accordion-collapse collapse show"
                    aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Yes, you can try us for free for 30 days. If you want,
                      we’ll provide you with a free, personalized 30-minute
                      onboarding call to get you up and running as soon as
                      possible. Lorem ipsum dolor sit amet, consectetur
                      adipiscing elit. Nunc vulputate libero et velit interdum,
                      ac aliquet odio mattis. Class aptent taciti sociosqu ad
                      litora torquent per conubia nostra, per inceptos
                      himenaeos. Curabitur tempus urna at turpis condimentum
                      lobortis. Ut commodo efficitur neque.
                    </div>
                  </div>
                </div>
                <div className="accordion-item">
                  <h2
                    className={`accordion-header ${pricingStyles.accordion_header}`}
                    id="headingTwo"
                  >
                    <button
                      className={` ${pricingStyles.questionBorber} accordion-button collapsed`}
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseTwo"
                    >
                      Can I change my plan later?
                    </button>
                  </h2>
                  <div
                    id="collapseTwo"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingTwo"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Yes, you can try us for free for 30 days. If you want,
                      we’ll provide you with a free, personalized 30-minute
                      onboarding call to get you up and running as soon as
                      possible. Lorem ipsum dolor sit amet, consectetur
                      adipiscing elit. Nunc vulputate libero et velit interdum,
                      ac aliquet odio mattis. Class aptent taciti sociosqu ad
                      litora torquent per conubia nostra, per inceptos
                      himenaeos. Curabitur tempus urna at turpis condimentum
                      lobortis. Ut commodo efficitur neque.
                    </div>
                  </div>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingThree">
                    <button
                      className={`${pricingStyles.questionBorber} accordion-button collapsed`}
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseThree"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      What is your cancellation policy?
                    </button>
                  </h2>
                  <div
                    id="collapseThree"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingThree"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      Yes, you can try us for free for 30 days. If you want,
                      we’ll provide you with a free, personalized 30-minute
                      onboarding call to get you up and running as soon as
                      possible. Lorem ipsum dolor sit amet, consectetur
                      adipiscing elit. Nunc vulputate libero et velit interdum,
                      ac aliquet odio mattis. Class aptent taciti sociosqu ad
                      litora torquent per conubia nostra, per inceptos
                      himenaeos. Curabitur tempus urna at turpis condimentum
                      lobortis. Ut commodo efficitur neque.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default JCaspVenuePartnerComponent;
