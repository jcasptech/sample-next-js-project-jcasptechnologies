import NewsLetterSection from "@components/newsLetter";
import { useRouter } from "next/router";
import JCaspVenuePartnerComponent from "./JCaspVenuePartner";
import PricingPlanComponent from "./pricingPlanComponent";
import pricingStyles from "./pricing.module.scss";

export interface JCaspPricingSceneProps {
  handlePaymentPlans: (d: any) => void;
  allPlans: any;
}
const JCaspPricingScene = (props: JCaspPricingSceneProps) => {
  const { handlePaymentPlans, allPlans } = props;
  const router = useRouter();

  return (
    <>
      <section className={`pageHeader ${pricingStyles.pageHeader}`}>
        <div className="container-fluid">
          <div className="row m0">
            <div className={`col-12 ${pricingStyles.pHead}`}>
              <h1>JCasp Solutions: Live Music Management and Promotion</h1>
            </div>
          </div>
        </div>
      </section>
      <section className="mt-3">
        <div className="container-fluid">
          <PricingPlanComponent
            {...{
              handlePaymentPlans,
              allPlans,
            }}
          />
        </div>
      </section>
      {/* JCasp light venue Partner  */}
      <JCaspVenuePartnerComponent />
      {/* JCasp light venue Partner  */}

      <NewsLetterSection />
    </>
  );
};

export default JCaspPricingScene;
