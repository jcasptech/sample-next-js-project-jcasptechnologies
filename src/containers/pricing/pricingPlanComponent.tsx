import Image from "next/image";
import { useRouter } from "next/router";
import pricingStyles from "./pricing.module.scss";
export interface PricingPlanComponentProps {
  handlePaymentPlans: (d: any) => void;
  allPlans: any;
}
const PricingPlanComponent = (props: PricingPlanComponentProps) => {
  const { handlePaymentPlans, allPlans } = props;
  const router = useRouter();

  return (
    <div className="row m0 priceBox">
      <div className="col-12 col-sm-12 col-lg-4 mnopad">
        <div className="row m0">
          <div
            className="col-12 title"
            data-aos="fade-up"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <h2 className={pricingStyles.pageSubHeader}>
              Live Music Programming Services
            </h2>
            <p>
              Our live entertainment management services provide restaurants,
              bars, hotels, resorts, and social clubs with two distinct tiers of
              service. This flexibility allows you to select the level of
              service that aligns with your budget and goals for elevating your
              entertainment offerings.
            </p>
          </div>
          <div
            className="col-12 meta"
            data-aos="fade-down"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <p>Would you like to schedule a consultation?</p>
            <p>Mail your query regarding JCasp Pro</p>
          </div>
          <div
            className={`col-12 contct ${pricingStyles.contct}`}
            data-aos="fade-up"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <p>
              <a href="mailto:contact@jcasptechnologies.com">
                Email us at contact@jcasptechnologies.com
              </a>
            </p>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-12 col-lg-8 mnopad">
        <div className="row m0">
          <div
            className="col-12 col-sm-6"
            data-aos="fade-down"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <div className="float-left w-100 purchBox popular">
              <div className={`mpop ${pricingStyles.mpop}`}>Most Popular</div>
              <div className="icon">
                {/* <img src="/images/popu.png" alt="popu" /> */}
                <Image
                  src="/images/popu.png"
                  alt="popu"
                  className="h-auto"
                  width={60}
                  height={60}
                />
              </div>
              <div className="float-left w-100">
                <div className={`p_name ${pricingStyles.p_name}`}>
                  JCasp LITE
                </div>
                <div className={`venue ${pricingStyles.venue}`}>Venue</div>
              </div>
              <div className="pr_box">
                $50.00 <span>/ Month</span>
              </div>
              {/* <p className="text row ">
                <div className="col-12 empty"></div>
              </p> */}
              <div className="button mb-3">
                <input
                  type="button"
                  className="goPro"
                  name="goPro"
                  value="Get Started with Pro Lite"
                  onClick={() => handlePaymentPlans(`${allPlans[0].objectId}`)}
                />
              </div>
              <ul>
                <li>
                  Live music calendar up-to-date each month on your custom venue
                  page.
                </li>
                <li>Embed code of your calendar for your website.</li>
                <li>
                  Dedicated Support: Access artist availability and personalized
                  support 7 days a week. Leverage our extensive network and
                  industry expertise to ensure your live music program&apos;s
                  success.
                </li>
              </ul>
              <p>&nbsp;</p>
            </div>
          </div>
          <div
            className="col-12 col-sm-6"
            data-aos="fade-up"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <div className="float-left w-100 purchBox">
              <div className="icon">
                {/* <img src="/images/pro.png" alt="pro" /> */}
                <Image
                  src="/images/pro.png"
                  alt="pro"
                  className="h-auto"
                  width={60}
                  height={60}
                />
              </div>
              <div className="float-left w-100">
                <div className={`p_name ${pricingStyles.p_name}`}>
                  JCasp Pro
                </div>
                <div className={`venue ${pricingStyles.venue}`}>Venue</div>
              </div>
              <div className={`pr_box`}>
                Custom Pricing <span>/ Month</span>
              </div>
              <p className="text">
                % booking fee deducted from your existing budge
              </p>
              <div className="button">
                <input
                  type="button"
                  className="goPro"
                  name="goPro"
                  value="Go Pro"
                  onClick={() => router.push("/contact-us")}
                />
              </div>
              <p>
                All the benefits of JCasp Pro Lite plus below benefits
                includes :
              </p>
              <ul>
                <li>
                  Full-Service music program management : artist
                  curation,scheduling, payment & promotion.
                </li>
                <li>Premium placement on the JCasp Interface.</li>
              </ul>
              <p className="note">
                For clients who need high-quality live music programming without
                a large time commitment.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default PricingPlanComponent;
