import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import { showToast } from "@components/ToastContainer";
import {
  getPricingPaymentsAPI,
  getPricingPaymentsPlansAPI,
} from "@redux/services/checkout.api";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import JCaspPricingScene from "./pricingScene";

export interface JCaspPricingContainerProps {}
const JCaspPricingContainer = (props: JCaspPricingContainerProps) => {
  const router = useRouter();
  const [createCheckoutSession, setCreateCheckoutSession] = useState("");

  const [allPlans, setAllPlan] = useState("");

  useEffect(() => {
    let getPlanId = async () => {
      try {
        let res = await getPricingPaymentsPlansAPI();
        setAllPlan(res);
      } catch (error) {}
    };
    getPlanId();
  }, []);

  const handlePaymentPlans = async (planId: any) => {
    let JCaspLightData = JSON.parse(
      localStorage.getItem("JCaspLight") || ""
    );
    if (JCaspLightData === "") {
      router.push("/JCasp-light");
    }
    if (planId != "") {
      try {
        const res: any = await getPricingPaymentsAPI({
          planId: planId,
          name: JCaspLightData.name,
          email: JCaspLightData.email,
          venueAddress: JCaspLightData.venueAddress,
          city: JCaspLightData.city,
          googlePlaceId: JCaspLightData.googlePlaceId,
        });

        if (res && res.api_status === 1) {
          if (res.url) {
            router.push(`${res.url}`);
            setCreateCheckoutSession("");
            localStorage.setItem("JCaspLight", JSON.stringify(""));
          } else {
            showToast(SUCCESS_MESSAGES.sumthingWentWorng, "warning");
          }
        } else {
          showToast(
            res?.message || SUCCESS_MESSAGES.sumthingWentWorng,
            "warning"
          );
        }
      } catch (error) {
        setCreateCheckoutSession("");
        router.push("/404");
      }
    }
  };

  return (
    <>
      <SEO
        {...{
          pageName: "/pricing",
        }}
      />

      <JCaspPricingScene
        {...{
          handlePaymentPlans,
          createCheckoutSession,
          allPlans,
        }}
      />
    </>
  );
};
JCaspPricingContainer.Layout = MainLayoutComponent;
export default JCaspPricingContainer;
