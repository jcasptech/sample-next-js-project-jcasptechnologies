import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { ArtistObject } from "@redux/slices/artists";
import { BrowseArtistDataResponse } from "@redux/slices/browseArtist/browseArtist";
import { Col, Row } from "antd";
import SingleArtist from "./single-artist";

export interface SubscribeArtistsSceneProps {
  artistData: BrowseArtistDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  handleArtistUnsubscribe: (d: any) => void;
  handleArtist: (d: any) => void;
}

const SubscribeArtistsScene = (props: SubscribeArtistsSceneProps) => {
  const {
    artistData,
    handleLoadMore,
    isLoading,
    handleArtist,
    handleArtistUnsubscribe,
  } = props;

  return (
    <>
      <div className={`main-section admin-panel-section`}>
        <section className="section h-75vs">
          <Row gutter={[30, 30]}>
            {artistData &&
              artistData?.list &&
              artistData?.list.length > 0 &&
              artistData.list.map((artist: ArtistObject, index: number) => (
                <SingleArtist
                  key={`artist-${index}`}
                  {...{
                    artist,
                    handleArtist,
                    handleArtistUnsubscribe,
                  }}
                />
              ))}
          </Row>

          {artistData && artistData?.list && artistData?.list.length <= 0 && (
            <Row gutter={[30, 30]} justify="center">
              <Col>
                <EmptyMessage description="No artists found." />
              </Col>
            </Row>
          )}

          {artistData?.hasMany && artistData?.hasMany === true && (
            <Row justify={"center"} className="mt-4">
              <Col>
                <Button
                  htmlType="button"
                  type="ghost"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={(e: any) => {
                    e.preventDefault();
                    handleLoadMore(true);
                  }}
                >
                  Load more
                </Button>
              </Col>
            </Row>
          )}
        </section>
      </div>
    </>
  );
};

export default SubscribeArtistsScene;
