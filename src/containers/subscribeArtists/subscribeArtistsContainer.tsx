import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { artistUnsubscribeAPI } from "@redux/services/artist.api";
import { whoAmI } from "@redux/services/auth.api";
import { LoginUserState, loginUserSuccess } from "@redux/slices/auth";
import {
  BrowseArtistState,
  fetchArtistFollows,
  loadMoreArtistFollows,
} from "@redux/slices/browseArtist/browseArtist";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import SubscribeArtistsScene from "./subscribeArtistsScene";

export interface subscribeArtistsContainerProps {}

const SubscribeArtistsContainer = (props: subscribeArtistsContainerProps) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [isActionLoading, setIsActionLoading] = useState(false);
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const [follows, setFollows] = useState<any[]>([]);

  const {
    browseArtist: { isLoading, data: artistData },
  }: {
    browseArtist: BrowseArtistState;
  } = useSelector((state: RootState) => ({
    browseArtist: state.browseArtist,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
      include: ["metadata"],
    },
  });

  const getData = () => {
    if (apiParam) {
      if (loginUserState?.data?.userType === LOGIN_USER_TYPE.USER) {
        dispatch(fetchArtistFollows(apiParam));
      }
    }
  };

  useEffect(() => {
    getData();
  }, [apiParam]);

  const handleLoadMore = (d: any) => {
    if (loginUserState?.data?.userType === LOGIN_USER_TYPE.USER) {
      apiParam.skip = (apiParam.skip || 0) + 8;
      dispatch(loadMoreArtistFollows(apiParam));
    }
  };

  const handleArtist = (artistSlug: string) => {
    if (artistSlug) {
      router.push(`/artist/${artistSlug}`);
    }
  };

  useEffect(() => {
    if (loginUserState?.data?.user?.follows) {
      setFollows(loginUserState?.data?.user?.follows);
    }
  }, [loginUserState?.data?.user]);

  const handleArtistUnsubscribe = async (artistId: string) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          await artistUnsubscribeAPI(artistId);
          showToast(SUCCESS_MESSAGES.unsubscribeArtist, "success");
          getData();
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      showToast(SUCCESS_MESSAGES.requiredLoginSubscribe, "warning");
    }
  };

  return (
    <>
      {isActionLoading && <DefaultSkeleton />}
      {isLoading && <DefaultSkeleton />}
      <Show
        when={["artists_subscriptions"]}
        fallback={<NoPermissionsComponent />}
      >
        <SubscribeArtistsScene
          {...{
            artistData,
            handleLoadMore,
            isLoading,
            follows,
            handleArtist,
            handleArtistUnsubscribe,
          }}
        />
      </Show>
    </>
  );
};

SubscribeArtistsContainer.Layout = AdminLayoutComponent;
export default SubscribeArtistsContainer;
