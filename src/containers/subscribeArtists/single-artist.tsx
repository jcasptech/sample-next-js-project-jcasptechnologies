import { BellIcon } from "@components/theme/icons/bellIcon";
import { ArtistObject } from "@redux/slices/artists";
import { Col } from "antd";
import "aos/dist/aos.css";
import Image from "next/image";
import router from "next/router";
import { capitalizeString } from "src/libs/helpers";
import Artiststyles from "./subscribeArtists.module.scss";

export interface SingleArtistProps {
  artist: ArtistObject;
  handleArtist: (d: any) => void;
  handleArtistUnsubscribe: (id: string) => void;
}

const SingleArtist = (props: SingleArtistProps) => {
  const { artist, handleArtist, handleArtistUnsubscribe } = props;

  return (
    <Col lg={6} md={8} sm={12} xs={24} className={Artiststyles.artist_card}>
      <div className={Artiststyles.thumb}>
        <Image
          className="cursor-pointer"
          src={artist.posterImage?.url || "/images/artist-placeholder.png"}
          alt={artist.name}
          onClick={(e) => handleArtist(artist.vanity)}
          width={425}
          height={260}
        />

        <div className={`${Artiststyles.thumb__catgrs}`}>
          {artist.tags &&
            artist.tags.map((tag, index) => (
              <span
                key={index}
                className="cursor-pointer"
                onClick={(e) => {
                  e.preventDefault();
                  router.push(`/browse?tag=${tag}`);
                }}
              >
                {capitalizeString(tag)}
              </span>
            ))}
        </div>

        <div className={`${Artiststyles.thumb__favrt}`}>
          <span
            onClick={(e) => {
              e.preventDefault();
              handleArtistUnsubscribe(artist.objectId);
            }}
            className="unfollow"
          >
            <BellIcon />
          </span>
        </div>
      </div>
      <div
        className={`title w-100 float-left cursor-pointer ${Artiststyles.title}`}
        onClick={(e) => handleArtist(artist.vanity)}
      >
        {capitalizeString(artist.name)}
      </div>
      <div
        className={`${Artiststyles.abstract} w-100 float-left two-line-and-text-ellipsis `}
      >
        {artist.metadata?.about
          ? `${
              artist.metadata.about.length >= 190
                ? artist.metadata.about.substring(0, 190) + "..."
                : artist.metadata.about
            }`
          : ""}
      </div>
    </Col>
  );
};

export default SingleArtist;
