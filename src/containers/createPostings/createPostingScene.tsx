import GigDetails from "@components/createPostingSteps/gigDetails";
import MusicDetails from "@components/createPostingSteps/music";
import OtherDetails from "@components/createPostingSteps/other";
import { ProgressIcon } from "@components/theme/icons/progressIcon";
import { CreatePostingsPayload } from "@redux/slices/postings";
import Image from "next/image";

export interface CreatePostingScaneProps {
  postingData: CreatePostingsPayload;
  currentStep: number;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
  setPostingData: (d: any) => void;
  createPosting: (d: any) => void;
  setCurrentStep: (d: any) => void;
}

const CreatePostingScene = (props: CreatePostingScaneProps) => {
  const {
    postingData,
    setPostingData,
    handlePrev,
    handleNext,
    currentStep,
    createPosting,
    setCurrentStep,
  } = props;

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="row m0">
            <div className="col-12 ptitle p-0"></div>

            <div className="multi_step_form float-left w-100 p-0">
              <div className="row m0" id="msform">
                <div className="col-12 p-0">
                  <div className="stepper-card float-left w-100">
                    <div className="row m0">
                      <div className="col-12 col-sm-4 col-lg-2 d-none d-sm-block">
                        <div className="navigation float-left w-100">
                          <a
                            className={`active step-nav d-flex justify-content-end`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(1);
                            }}
                          >
                            <div className="text-right">
                              <div>Gig Details</div>
                              <small>It is a long established fact</small>
                            </div>
                            <div>
                              <Image
                                src="/images/artist-steps/active-user.png"
                                alt="Gig Details"
                                width={44}
                                height={44}
                              />
                              {/* <img
                                src="/images/artist-steps/active-user.png"
                                alt="Gig Details"
                              /> */}
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 2 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(2);
                            }}
                          >
                            <div className="text-right">
                              <div>Music Format & Style</div>
                              <small>It is a long established fact</small>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 2 ? "active-" : ""
                                }song.png`}
                                alt="Music Format & Style"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 3 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(3);
                            }}
                          >
                            <div className="text-right">
                              <div>Other Information</div>
                              <small>It is a long established fact</small>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 3 ? "active-" : ""
                                }link.png`}
                                alt="Other Information"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>
                        </div>
                      </div>
                      <div className="col-12 col-sm-8 col-lg-10 stepper-right p-0">
                        <div className="w-100 float-left d-block d-sm-none">
                          <div className="steps-progress d-flex">
                            <div className="current-progress">
                              <ProgressIcon
                                strokeWidth={6}
                                sqSize={56}
                                percentage={(currentStep * 100) / 3}
                              ></ProgressIcon>
                              <span>{currentStep} of 3</span>
                            </div>
                            {currentStep === 1 && (
                              <div className="step-info">
                                <div>Gig Details</div>
                                <small>
                                  Fill out your preferences here and let the
                                  musicians come to you! If you&apos;re looking
                                  to hire a specific artist instead, find their
                                  profile and click the &quot;Message
                                  Artist&quot; button.
                                </small>
                              </div>
                            )}

                            {currentStep === 2 && (
                              <div className="step-info">
                                <div>Elements, Date & time</div>
                                <small>
                                  Select Artist Element & Date and time
                                </small>
                              </div>
                            )}

                            {currentStep === 3 && (
                              <div className="step-info">
                                <div>Other Information</div>
                                <small>
                                  Select which music format & style do you want
                                  in your event.
                                </small>
                              </div>
                            )}
                          </div>
                          <div className="w-100 border-bottom mt-3"></div>
                        </div>

                        <div className="w-100 float-left ps-5 stepcalPd d-none d-sm-block">
                          <p className="mb-0">
                            <span className="stepcal">
                              Step {currentStep}/3
                            </span>
                          </p>
                          <div className="stepInfoH float-left w-100">
                            <div
                              className={`step_info  ${
                                currentStep === 1 ? "impBlock" : "display-none"
                              }`}
                              data-set="1"
                            >
                              <h3 className="m-0">Gig Details</h3>
                              <p className="m-0">
                                Fill out your preferences here and let the
                                musicians come to you! If you&apos;re looking to
                                hire a specific artist instead, find their
                                profile and click the &quot;Message Artist&quot;
                                button.
                              </p>
                            </div>
                            <div
                              className={`step_info ${
                                currentStep === 2 ? "impBlock" : "display-none"
                              }`}
                            >
                              <h3 className="m-0">Music Format & Style</h3>
                              <p className="m-0">
                                Select which music format & style do you want in
                                your event.
                              </p>
                            </div>
                            <div
                              className={`step_info ${
                                currentStep === 3 ? "impBlock" : "display-none"
                              }`}
                            >
                              <h3 className="m-0">Other Information</h3>
                              <p className="m-0">
                                Select which music format & style do you want in
                                your event.
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className="ps-5 allSteps">
                          {currentStep === 1 && (
                            <GigDetails
                              {...{
                                postingData,
                                setPostingData,
                                handleNext,
                              }}
                            />
                          )}
                          {currentStep === 2 && (
                            <MusicDetails
                              {...{
                                postingData,
                                setPostingData,
                                handleNext,
                                handlePrev,
                              }}
                            />
                          )}
                          {currentStep === 3 && (
                            <OtherDetails
                              {...{
                                postingData,
                                setPostingData,
                                handleNext,
                                handlePrev,
                                createPosting,
                              }}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreatePostingScene;
