import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { postCreateAPI } from "@redux/services/post.api";
import { CreatePostingsPayload } from "@redux/slices/postings";
import { fetchTags } from "@redux/slices/tags";
import { useReCaptcha } from "next-recaptcha-v3";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import CreatePostingScene from "./createPostingScene";

export interface CreatePostingsContainerProps {}

const CreatePostingsContainer = (props: CreatePostingsContainerProps) => {
  const [currentStep, setCurrentStep] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [postingData, setPostingData] = useState<CreatePostingsPayload>({
    title: "",
    variety: "",
    promotion: false,
    dates: "",
    start: "",
    end: "",
    musicStyles: [],
    expectedAttandance: "",
    playSettings: "",
    offerAmount: "",
    note: "",
    googlePlaceId: "",
    venueId: "",
    placeName: "",
    equipmentProvided: true,
  });

  const { executeRecaptcha, loaded } = useReCaptcha();
  const dispatch = useDispatch();
  const router = useRouter();
  const { apiParam } = useList({
    queryParams: {},
  });

  const getTags = async () => {
    const recaptchaResponse = await executeRecaptcha("tag_list");
    dispatch(fetchTags(apiParam, recaptchaResponse));
  };

  useEffect(() => {
    if (loaded) {
      getTags();
    }
  }, [loaded]);

  const handlePrev = (index: any) => {
    if (currentStep > 1) {
      setCurrentStep(currentStep - 1);
    }
  };

  const handleNext = (data: any) => {
    setPostingData((prevState: any) => {
      return {
        ...prevState,
        ...data,
      };
    });
    if (currentStep < 3) {
      setCurrentStep(currentStep + 1);
    }
  };

  const createPosting = async (data: any) => {
    if (postingData) {
      postingData["note"] = data?.note;
      postingData["expectedAttandance"] = data?.expectedAttandance;
      postingData["playSettings"] = data?.playSettings;

      setIsLoading(true);
      try {
        await postCreateAPI(postingData);
        router.push("/active-posting");
        setIsLoading(false);
        showToast(SUCCESS_MESSAGES.postingCreatedSuccess, "success");
      } catch (error) {
        setIsLoading(false);
        showToast(SUCCESS_MESSAGES.postingCreateError, "warning");
      }
    }
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["create_posting"]} fallback={<NoPermissionsComponent />}>
        <CreatePostingScene
          {...{
            setPostingData,
            postingData,
            currentStep,
            handlePrev,
            handleNext,
            createPosting,
            setCurrentStep,
          }}
        />
      </Show>
    </>
  );
};

CreatePostingsContainer.Layout = AdminLayoutComponent;
export default CreatePostingsContainer;
