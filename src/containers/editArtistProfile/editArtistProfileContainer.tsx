import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { loginUserSuccess } from "@redux/actions";
import { RootState } from "@redux/reducers";
import {
  activateArtistAPI,
  deactivateArtistAPI,
  postArtistUpdateAPI,
  removeArtistAPI,
} from "@redux/services/artist.api";
import { whoAmI } from "@redux/services/auth.api";
import {
  photosPayload,
  songsPayload,
  UpdateArtistPayload,
  youtubeVideoLinksPayload,
} from "@redux/slices/artists";
import {
  fetchSelectedArtist,
  SeledtedArtistState,
} from "@redux/slices/selectedArtist";
import router from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { deepClone, getYoutubeId, setYoutubeId } from "src/libs/helpers";
import EditArtistProfileScane from "./editArtistProfileScane";

export interface EditArtistProfileContainerProps {}

const EditArtistProfileContainer = (props: EditArtistProfileContainerProps) => {
  const [currentStep, setCurrentStep] = useState(1);
  const [profileData, setProfileData] = useState<UpdateArtistPayload>({
    artistName: "",
    profileImage: "",
    artistElements: [],
    artistPhonenumber: "",
    artistEmailId: "",
    artistLocaleCity: "",
    artistLocaleState: "",
    artistLocaleCountry: "",
    youtubeId: "",
    paymentPreference: "paypal",
    paypalId: "",
    vemeoId: "",
    artistBio: "",
    JCaspVanityTag: "",
    songs: [],
    vemeoLink: "",
    facebookPage: "",
    instagramUsername: "",
    website: "",
    liveStreamUrl: "",
    patreonLink: "",
    spotifyLink: "",
    youtubeVideoLinks: [],
    photos: [],
    audioFiles: [],
    profileTheme: "dark",
    csvFile: "",
    csvName: "",
    isProfileImgUpdated: "false",
    youtubeChannel: "",
  });
  const dispatch = useDispatch();
  const selectArtistId: any = useSelector((state: RootState) => state.selectId);
  const [isUpdateLoading, setIsUpdateLoading] = useState(false);

  const [isProfileImgUpdated, setIsProfileImgUpdated] = useState("false");

  const {
    selectedArtist: { isLoading, data: selectedArtistData },
  }: {
    selectedArtist: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    selectedArtist: state.selectedArtist,
  }));

  useEffect(() => {
    if (selectArtistId?.data) {
      dispatch(fetchSelectedArtist(selectArtistId?.data));
    }
  }, [selectArtistId.data]);

  const handlePrev = (index: any) => {
    if (currentStep > 1) {
      setCurrentStep(currentStep - 1);
    }
  };

  const handleNext = (data: any) => {
    setProfileData((prevState: any) => {
      return {
        ...prevState,
        ...data,
      };
    });
    if (currentStep < 6) {
      setCurrentStep(currentStep + 1);
    }
  };

  let handlePostData = async (data: any) => {
    if (profileData) {
      const tmpData = deepClone(profileData);
      profileData["isProfileImgUpdated"] = isProfileImgUpdated;
      tmpData["profileTheme"] = data;
      tmpData["youtubeId"] = getYoutubeId(profileData.youtubeId) || "";

      delete tmpData.csvName;
      setIsUpdateLoading(true);
      try {
        await postArtistUpdateAPI(selectedArtistData.objectId, tmpData);

        setIsUpdateLoading(false);
        showToast(SUCCESS_MESSAGES.editArtistSuccess, "success");
        tmpData["youtubeId"] = setYoutubeId(profileData.youtubeId) || "";
        const auth = await whoAmI();
        dispatch(loginUserSuccess(auth));
        dispatch(fetchSelectedArtist(selectArtistId?.data));
        setCurrentStep(1);
      } catch (error) {
        setIsUpdateLoading(false);
        showToast(SUCCESS_MESSAGES.artistUpdateError, "warning");
      }
    }
  };

  const handleDeactiveArtist = async (artistId: string) => {
    if (artistId) {
      try {
        await deactivateArtistAPI(artistId);
        showToast(SUCCESS_MESSAGES.ArtistDeactivatedSuccess, "success");
        dispatch(fetchSelectedArtist(artistId));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleActiveArtist = async (artistId: string) => {
    if (artistId) {
      try {
        await activateArtistAPI(artistId);
        showToast(SUCCESS_MESSAGES.ArtistActivatedSuccess, "success");
        dispatch(fetchSelectedArtist(artistId));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRemoveArtist = async (artistId: string) => {
    if (artistId) {
      try {
        await removeArtistAPI(artistId);
        showToast(SUCCESS_MESSAGES.ArtistRemovedSuccess, "success");
        setTimeout(() => {
          router.reload();
        }, 1000);
      } catch (error) {
        console.log(error);
      }
    }
  };

  useEffect(() => {
    const tmpData = deepClone(profileData);
    if (tmpData.songs.length <= 0 || tmpData.youtubeVideoLinks.length <= 0) {
      if (tmpData.songs.length <= 0) {
        tmpData.songs.push({
          songName: "",
          songSingerName: "",
          url: "",
        });
      }

      if (tmpData.youtubeVideoLinks.length <= 0) {
        tmpData.youtubeVideoLinks.push({
          link: "",
        });
      }

      setProfileData(tmpData);
    }
  }, [profileData]);

  const handleAddSongs = () => {
    const tmpData = deepClone(profileData);
    tmpData.songs.push({
      songName: "",
      songSingerName: "",
      url: "",
    });
    setProfileData(tmpData);
  };

  const handleRemoveSongs = (formId: any) => {
    const tmpData = deepClone(profileData);
    tmpData.songs = tmpData.songs.filter(
      (song: any, index: number) => index !== formId
    );
    setProfileData(tmpData);
  };

  const handleAddVideolinks = () => {
    const tmpData = deepClone(profileData);
    if (tmpData.youtubeVideoLinks.length < 5) {
      tmpData.youtubeVideoLinks.push({
        link: "",
      });
      setProfileData(tmpData);
    }
  };

  const handleInputChange = (index: number, event: any) => {
    const tmpData = deepClone(profileData);
    tmpData.youtubeVideoLinks[index].link = event.target.value; // Update the value of the specific input field at the given index
    setProfileData(tmpData);
  };

  const handleRemoveVideoForm = (formId: number) => {
    const tmpData = deepClone(profileData);
    tmpData.youtubeVideoLinks = tmpData.youtubeVideoLinks.filter(
      (links: any, index: number) => index !== formId
    );
    setProfileData(tmpData);
  };

  useEffect(() => {
    const tmpSongs: songsPayload[] = [];
    const tmpAudiofile: any = [];
    selectedArtistData?.songs &&
      selectedArtistData?.songs?.map((song: any) => {
        if (song.audioFile) {
          tmpSongs.push({
            url: song?.audioFile?.url,
            songSingerName: song?.artistName,
            songName: song?.name,
          });

          tmpAudiofile.push({
            objectId: song?.objectId || "",
            url: song?.audioFile?.url || "",
            name: song?.name || "",
          });
        }
      });

    const tmpVideoLinks: youtubeVideoLinksPayload[] = [];
    if (selectedArtistData?.metadata?.youtubeVideoLinks?.length > 0) {
      selectedArtistData?.metadata?.youtubeVideoLinks?.map((link: any) => {
        tmpVideoLinks.push({ link });
      });
    } else {
      tmpVideoLinks.push({ link: "" });
    }

    const tmpPhotos: photosPayload[] = [];
    selectedArtistData?.photos?.map((photo: any) => {
      tmpPhotos.push({
        objectId: photo?.objectId,
        url: photo?.sourceImage?.url,
        name: photo?.name,
      });
    });

    let songsDataFile: any = null;
    if (selectedArtistData?.metadata?.csv) {
      songsDataFile = selectedArtistData?.metadata?.csv;
    } else {
      songsDataFile = selectedArtistData?.metadata?.pdf;
    }

    setProfileData({
      artistName: selectedArtistData?.name || "",
      profileImage: selectedArtistData?.iconImage?.url || "",
      artistElements: selectedArtistData?.tags || [],
      artistPhonenumber: selectedArtistData?.metadata?.phone || "",
      artistEmailId: selectedArtistData?.email || "",
      artistLocaleCity: selectedArtistData?.city || "",
      artistLocaleState: selectedArtistData?.metadata?.state || "",
      artistLocaleCountry: selectedArtistData?.country || "",
      youtubeId: setYoutubeId(selectedArtistData?.youtubeId) || "",
      paymentPreference: selectedArtistData?.banking?.paymentPreference || "",
      paypalId: selectedArtistData?.banking?.paypal || "",
      vemeoId: selectedArtistData?.banking?.venmo || "",
      artistBio: selectedArtistData?.metadata?.about || "",
      JCaspVanityTag: selectedArtistData?.vanity || "",
      songs: tmpSongs,
      vemeoLink: selectedArtistData?.metadata?.vemeoLink || "",
      facebookPage: selectedArtistData?.metadata?.facebookPage || "",
      instagramUsername: selectedArtistData?.metadata?.instagramUsername || "",
      website: selectedArtistData?.metadata?.website || "",
      liveStreamUrl: selectedArtistData?.metadata?.liveStreamUrl || "",
      patreonLink: selectedArtistData?.metadata?.patreonLink || "",
      spotifyLink: selectedArtistData?.metadata?.spotifyLink || "",
      youtubeVideoLinks: tmpVideoLinks,
      photos: tmpPhotos,
      audioFiles: tmpAudiofile || [],
      profileTheme: selectedArtistData?.metadata?.theme,
      csvFile: songsDataFile?.url || "",
      csvName: songsDataFile?.name || "",
      youtubeChannel: selectedArtistData?.metadata?.youtubeChannel || "",
    });
  }, [selectedArtistData]);

  return (
    <>
      {(isLoading || !selectArtistId?.data) && <DefaultSkeleton />}
      <Show when={["edit_artist"]} fallback={<NoPermissionsComponent />}>
        <EditArtistProfileScane
          {...{
            profileData,
            setProfileData,
            currentStep,
            handlePrev,
            handleNext,
            handlePostData,
            isUpdateLoading,
            handleAddSongs,
            handleRemoveSongs,
            handleAddVideolinks,
            handleRemoveVideoForm,
            setIsProfileImgUpdated,
            setCurrentStep,
            handleInputChange,
            selectedArtistData,
            handleDeactiveArtist,
            handleActiveArtist,
            handleRemoveArtist,
          }}
        />
      </Show>
    </>
  );
};

EditArtistProfileContainer.Layout = AdminLayoutComponent;
export default EditArtistProfileContainer;
