import Profile from "@components/artistProfileSteps/profile";
import { ProgressIcon } from "@components/theme/icons/progressIcon";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";

const ArtistLinks = dynamic(
  () => import("@components/artistProfileSteps/links"),
  { ssr: false }
);
const ArtistVideoLinks = dynamic(
  () => import("@components/artistProfileSteps/videoLinks"),
  { ssr: false }
);
const Photos = dynamic(() => import("@components/artistProfileSteps/photos"), {
  ssr: false,
});
// const ArtistAudios = dynamic(
//   () => import("@components/artistProfileSteps/audios"),
//   { ssr: false }
// );
const ArtistTheme = dynamic(
  () => import("@components/artistProfileSteps/theme"),
  { ssr: false }
);
const UploadCSV = dynamic(
  () => import("@components/artistProfileSteps/uploadCSV"),
  { ssr: false }
);
const DeactivateArtist = dynamic(
  () => import("@components/artistProfileSteps/deactivate"),
  { ssr: false }
);
// import ArtistLinks from "@components/artistProfileSteps/links";
// import ArtistVideoLinks from "@components/artistProfileSteps/videoLinks";
// import Photos from "@components/artistProfileSteps/photos";
import ArtistAudios from "@components/artistProfileSteps/audios";
import { Button, Popconfirm } from "antd";
import { ActiveArtistIcon } from "@components/theme/icons/activeArtistIcon";
import { RemoveArtistIcon } from "@components/theme/icons/removeArtistIcon";
import { DeactiveArtistIcon } from "@components/theme/icons/deactiveArtistIcon";
import Image from "next/image";
// import ArtistTheme from "@components/artistProfileSteps/theme";
// import UploadCSV from "@components/artistProfileSteps/uploadCSV";
// import DeactivateArtist from "@components/artistProfileSteps/deactivate";

export interface CreateArtistScaneProps {
  isUpdateLoading: boolean;
  profileData: UpdateArtistPayload;
  currentStep: number;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
  handlePostData: (d: any) => void;
  handleAddSongs: (d: any) => void;
  handleRemoveSongs: (d: any) => void;
  handleRemoveVideoForm: (d: any) => void;
  handleAddVideolinks: (d: any) => void;
  setIsProfileImgUpdated: (d: any) => void;
  setCurrentStep: (d: any) => void;
  handleInputChange: (index: number, event: any) => void;
  setProfileData: (d: any) => void;
  selectedArtistData: any;
  handleDeactiveArtist: (d: any) => void;
  handleActiveArtist: (d: any) => void;
  handleRemoveArtist: (d: any) => void;
}

const CreateArtistScane = (props: CreateArtistScaneProps) => {
  const {
    profileData,
    handlePrev,
    handleNext,
    currentStep,
    handlePostData,
    isUpdateLoading,
    handleAddSongs,
    handleRemoveSongs,
    handleRemoveVideoForm,
    handleAddVideolinks,
    setIsProfileImgUpdated,
    setCurrentStep,
    handleInputChange,
    setProfileData,
    selectedArtistData,
    handleDeactiveArtist,
    handleActiveArtist,
    handleRemoveArtist,
  } = props;
  const router = useRouter();

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="row m0">
            <div className="col-12 ptitle p-0"></div>

            <div className="multi_step_form float-left w-100 p-0">
              <div className="row m0" id="msform">
                <div className="col-12 p-0">
                  <div className="stepper-card float-left w-100">
                    <div className="row m0">
                      <div className="col-12 col-sm-4 col-lg-2 d-none d-sm-block">
                        <div className="navigation float-left w-100">
                          <a
                            className={`active step-nav d-flex justify-content-end`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(1);
                            }}
                          >
                            <div className="text-right">
                              <div>Artist Profile</div>
                            </div>
                            <div>
                              <Image
                                src="/images/artist-steps/active-user.png"
                                alt="active user"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>
                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 2 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(2);
                            }}
                          >
                            <div className="text-right">
                              <div>Songs List</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 2 ? "active-" : ""
                                }song.png`}
                                alt="song"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 3 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(3);
                            }}
                          >
                            <div className="text-right">
                              <div>Links</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 3 ? "active-" : ""
                                }link.png`}
                                alt="External Links"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 4 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(4);
                            }}
                          >
                            <div className="text-right">
                              <div>Video</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 4 ? "active-" : ""
                                }youtube.png`}
                                alt="Additional YouTube Video Links"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 5 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(5);
                            }}
                          >
                            <div className="text-right">
                              <div>Photos</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 5 ? "active-" : ""
                                }photos.png`}
                                alt="Upload Photos"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          {/* <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 6 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(6);
                            }}
                          >
                            <div className="text-right">
                              <div>Audio</div>
                            </div>
                            <div>
                              <img
                                src={`/images/artist-steps/${
                                  currentStep >= 6 ? "active-" : ""
                                }audio.png`}
                                alt="Audio Files"
                              />
                            </div>
                          </a> */}

                          <a
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 6 ? "active" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              setCurrentStep(6);
                            }}
                          >
                            <div className="text-right">
                              <div>Theme</div>
                            </div>
                            <div>
                              <Image
                                src={`/images/artist-steps/${
                                  currentStep >= 6 ? "active-" : ""
                                }theme.png`}
                                alt="Select Profile Theme"
                                width={44}
                                height={44}
                              />
                            </div>
                          </a>

                          {/* <a
                            href="javascript:;"
                            className={`step-nav d-flex justify-content-end ${
                              currentStep >= 8 ? "active" : ""
                            }`}
                            onClick={() => setCurrentStep(8)}
                          >
                            <div className="text-right">
                              <div>DEACTIVATE</div>
                            </div>
                            <div>
                              <img
                                src={`/images/artist-steps/${
                                  currentStep >= 8 ? "active-" : ""
                                }link.png`}
                                alt="External Links"
                              />
                            </div>
                          </a> */}
                        </div>
                      </div>
                      <div className="col-12 col-sm-8 col-lg-10 stepper-right p-0">
                        <div className="w-100 float-left d-block d-sm-none">
                          <div className="steps-progress d-flex">
                            <div className="current-progress">
                              <ProgressIcon
                                strokeWidth={6}
                                sqSize={56}
                                percentage={(currentStep * 100) / 6}
                              ></ProgressIcon>
                              <span>{currentStep} of 6</span>
                            </div>
                            {currentStep === 1 && (
                              <div className="step-info">
                                <div>
                                  {profileData?.artistName} Artist Profile
                                </div>
                                <small>
                                  Please fill below artist personal details to
                                  process next step.
                                </small>
                              </div>
                            )}
                            {currentStep === 2 && (
                              <div className="step-info">
                                <div>Upload Songs</div>
                                <small>
                                  Upload a CSV or PDF of your performance
                                  repertoire. This gives potential clients a
                                  preview of your live set and boosts your
                                  profile&lsquo;s visibility. A comprehensive
                                  song list can enhance your appeal and increase
                                  your chances of being booked.
                                </small>
                              </div>
                            )}

                            {currentStep === 3 && (
                              <div className="step-info">
                                <div>External Links</div>
                                <small>
                                  Enter your social media links , website link
                                  and Zoo links.
                                </small>
                              </div>
                            )}

                            {currentStep === 4 && (
                              <div className="step-info">
                                <div>Additional Video Links</div>
                                <small>
                                  Please add additional YouTube or Vimeo links.
                                  You can add up to 5 videos.
                                </small>
                              </div>
                            )}

                            {currentStep === 5 && (
                              <div className="step-info">
                                <div>Upload Photos</div>
                                <small>
                                  Upload additional Artist photos. We recommend
                                  photos that showcase your live performances
                                </small>
                              </div>
                            )}

                            {/* {currentStep === 6 && (
                              <div className="step-info">
                                <div>Audio Files</div>
                                <small>
                                  Upload your audio files here. Note: Files must
                                  be original works and legally distributable.
                                </small>
                              </div>
                            )} */}

                            {currentStep === 6 && (
                              <div className="step-info">
                                <div>Select Profile Theme</div>
                                <small>
                                  Select the profile mode for your profile.
                                </small>
                              </div>
                            )}
                            {/* {currentStep === 8 && (
                              <div className="step-info">
                                <div>DEACTIVE</div>
                                <small>Deactivate and Remove artist.</small>
                              </div>
                            )} */}
                          </div>
                          <div className="w-100 mt-3 artist-buttons-section">
                            <div className="deactive-buttons">
                              {selectedArtistData?.approved && (
                                <Popconfirm
                                  title={`Are you sure you want to Deactivate "${profileData?.artistName}"?`}
                                  okText="Yes, Deactivate"
                                  cancelText="Cancel"
                                  onConfirm={() =>
                                    handleDeactiveArtist(
                                      selectedArtistData?.objectId
                                    )
                                  }
                                >
                                  <button
                                    type="button"
                                    className=""
                                    title="Deactive Artist"
                                  >
                                    <DeactiveArtistIcon />
                                  </button>
                                </Popconfirm>
                              )}

                              {!selectedArtistData?.approved && (
                                <Popconfirm
                                  title={`Are you sure you want to Activate "${profileData?.artistName}"?`}
                                  okText="Yes, Activate"
                                  cancelText="Cancel"
                                  onConfirm={() =>
                                    handleActiveArtist(
                                      selectedArtistData?.objectId
                                    )
                                  }
                                >
                                  <button
                                    type="button"
                                    className=""
                                    title="Active Artist"
                                  >
                                    <ActiveArtistIcon />
                                  </button>
                                </Popconfirm>
                              )}
                            </div>
                            <div className="remove-buttons">
                              <Popconfirm
                                title="Are you sure you want to Remove this Artist?"
                                okText="Yes, Remove"
                                cancelText="Cancel"
                                onConfirm={() =>
                                  handleRemoveArtist(
                                    selectedArtistData?.objectId
                                  )
                                }
                              >
                                <button
                                  type="button"
                                  className=""
                                  title="Remove Artist"
                                >
                                  <RemoveArtistIcon />
                                </button>
                              </Popconfirm>
                            </div>
                          </div>
                          <div className="w-100 border-bottom mt-3"></div>
                        </div>

                        <div className="step-info-section">
                          <div className="step-info">
                            <div className="w-100 float-left ps-5 stepcalPd d-none d-sm-block">
                              <p className="mb-0">
                                <span className="stepcal">
                                  Step {currentStep}/6
                                </span>
                              </p>
                              <div className="stepInfoH float-left w-100">
                                <div
                                  className={`step_info  ${
                                    currentStep === 1
                                      ? "impBlock"
                                      : "display-none"
                                  }`}
                                  data-set="1"
                                >
                                  <h3 className="m-0">
                                    {profileData?.artistName} Artist Profile
                                  </h3>
                                  <p className="m-0">
                                    Please fill below artist personal details to
                                    process next step.
                                  </p>
                                </div>

                                <div
                                  className={`step_info ${
                                    currentStep === 2
                                      ? "impBlock"
                                      : "display-none"
                                  }`}
                                >
                                  <h3 className="m-0">Upload Songs</h3>
                                  <p className="m-0">
                                    Upload a CSV or PDF of your performance
                                    repertoire. This gives potential clients a
                                    preview of your live set and boosts your
                                    profile&lsquo;s visibility. A comprehensive
                                    song list can enhance your appeal and
                                    increase your chances of being booked.
                                  </p>
                                </div>
                                <div
                                  className={`step_info ${
                                    currentStep === 3
                                      ? "impBlock"
                                      : "display-none"
                                  }`}
                                >
                                  <h3 className="m-0">External Links</h3>
                                  <p className="m-0">
                                    Enter your social media links , website link
                                    and Zoo links.
                                  </p>
                                </div>
                                <div
                                  className={`step_info  ${
                                    currentStep === 4
                                      ? "impBlock"
                                      : "display-none"
                                  }`}
                                >
                                  <h3 className="m-0">
                                    Additional Video Links
                                  </h3>
                                  <p className="m-0">
                                    Please add additional YouTube or Vimeo
                                    links. You can add up to 5 videos.
                                  </p>
                                </div>
                                <div
                                  className={`step_info ${
                                    currentStep === 5
                                      ? "impBlock"
                                      : "display-none"
                                  }`}
                                >
                                  <h3 className="m-0">Upload Photos</h3>
                                  <p className="m-0">
                                    Upload additional Artist photos. We
                                    recommend photos that showcase your live
                                    performances.
                                  </p>
                                </div>
                                {/* <div
                                  className={`step_info ${
                                    currentStep === 6 ? "impBlock" : "display-none"
                                  }`}
                                >
                                  <h3 className="m-0">Audio Files</h3>
                                  <p className="m-0">
                                    Upload your audio files here. Note: Files
                                    must be original works and legally
                                    distributable.
                                  </p>
                                </div> */}
                                <div
                                  className={`step_info ${
                                    currentStep === 6
                                      ? "impBlock"
                                      : "display-none"
                                  }`}
                                >
                                  <h3 className="m-0">Select Profile Theme</h3>
                                  <p className="m-0">
                                    Select the profile mode for your profile.
                                  </p>
                                </div>

                                {/* <div
                                  className={`step_info ${
                                    currentStep === 8 ? "impBlock" : "display-none"
                                  }`}
                                >
                                  <h3 className="m-0">DEACTIVATE</h3>
                                  <p className="m-0">
                                    Deactivate and Remove artist.
                                  </p>
                                </div> */}
                              </div>
                            </div>
                          </div>
                          <div className="artist-buttons-section">
                            <div className="deactive-buttons">
                              {selectedArtistData?.approved && (
                                <Popconfirm
                                  title={`Are you sure you want to Deactivate "${profileData?.artistName}"?`}
                                  okText="Yes, Deactivate"
                                  cancelText="Cancel"
                                  onConfirm={() =>
                                    handleDeactiveArtist(
                                      selectedArtistData?.objectId
                                    )
                                  }
                                >
                                  <button
                                    type="button"
                                    className=""
                                    title="Deactive Artist"
                                  >
                                    <DeactiveArtistIcon />
                                  </button>
                                </Popconfirm>
                              )}

                              {!selectedArtistData?.approved && (
                                <Popconfirm
                                  title={`Are you sure you want to Activate "${profileData?.artistName}"?`}
                                  okText="Yes, Activate"
                                  cancelText="Cancel"
                                  onConfirm={() =>
                                    handleActiveArtist(
                                      selectedArtistData?.objectId
                                    )
                                  }
                                >
                                  <button
                                    type="button"
                                    className=""
                                    title="Active Artist"
                                  >
                                    <ActiveArtistIcon />
                                  </button>
                                </Popconfirm>
                              )}
                            </div>
                            <div className="remove-buttons">
                              <Popconfirm
                                title="Are you sure you want to Remove this Artist?"
                                okText="Yes, Remove"
                                cancelText="Cancel"
                                onConfirm={() =>
                                  handleRemoveArtist(
                                    selectedArtistData?.objectId
                                  )
                                }
                              >
                                <button
                                  type="button"
                                  className=""
                                  title="Remove Artist"
                                >
                                  <RemoveArtistIcon />
                                </button>
                              </Popconfirm>
                            </div>
                          </div>
                        </div>

                        <div className="ps-5 allSteps">
                          {currentStep === 1 && (
                            <Profile
                              {...{
                                profileData,
                                handleNext,
                                setIsProfileImgUpdated,
                              }}
                            />
                          )}

                          {currentStep === 3 && (
                            <ArtistLinks
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                              }}
                            />
                          )}
                          {currentStep === 4 && (
                            <ArtistVideoLinks
                              {...{
                                profileData,
                                setProfileData,
                                handleNext,
                                handlePrev,
                                handleAddVideolinks,
                                handleRemoveVideoForm,
                                handleInputChange,
                              }}
                            />
                          )}
                          {currentStep === 5 && (
                            <Photos
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                              }}
                            />
                          )}
                          {/* {currentStep === 6 && (
                            <ArtistAudios
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                              }}
                            />
                          )} */}
                          {currentStep === 6 && (
                            <ArtistTheme
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                                handlePostData,
                                isUpdateLoading,
                              }}
                            />
                          )}
                          {/* 
                          {currentStep === 8 && (
                            <DeactivateArtist
                              {...{
                                profileData,
                                handleNext,
                                handlePrev,
                                setCurrentStep,
                                selectedArtistData,
                              }}
                            />
                          )} */}
                          {currentStep === 2 && (
                            <UploadCSV
                              {...{
                                profileData,
                                selectedArtistData,
                                setCurrentStep,
                                handleNext,
                              }}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default CreateArtistScane;
