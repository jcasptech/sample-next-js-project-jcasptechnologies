import landingStyles from "./landing.module.scss";
const DescriptionJCaspLite = () => {
  return (
    <>
      <div className={landingStyles.descriptionJCaspLite}>
        <div className="descriptionJCasp">
          <p>
            Experience the flexibility and convenience of JCasp LITE, knowing
            you can cancel your subscription at any time without any hassle.
            Choose JCasp LITE for a budget-friendly solution to managing live
            music at your restaurant while still benefiting from the expertise
            and resources JCasp provides.
          </p>
        </div>
      </div>
    </>
  );
};
export default DescriptionJCaspLite;
