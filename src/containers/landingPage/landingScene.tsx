import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import DescriptionJCaspLite from "./descriptionSection";
import DownloadJCaspLight from "./downloadJCaspLight";
import JCaspLightFeatures from "./featureJCaspLight";
import { GetInTouch } from "./getInTouch";
import IntroduceJCaspLight from "./introduceJCaspLight";
import Pricing from "./pricing";

export interface LandingPageSceneProps {}

const LandingPageScene = (props: LandingPageSceneProps) => {
  const {} = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <IntroduceJCaspLight />
      <Pricing />
      <JCaspLightFeatures />
      <DescriptionJCaspLite />
      <GetInTouch />
      <DownloadJCaspLight />
    </>
  );
};

export default LandingPageScene;
