import landingStyles from "./landing.module.scss";
import { setEnquiryPhone, setEnquiryType } from "@redux/slices/general";
import { useDispatch } from "react-redux";
import { Button } from "@components/theme";
import Image from "next/image";

const JCaspLightFeatures = () => {
  const dispatch = useDispatch();

  const handleJCaspLight = (data: any) => {
    if (data) {
      dispatch(setEnquiryType(data));
      scrollToDiv();
    }
  };

  const scrollToDiv = () => {
    const divElement = document.getElementById("contactForm");
    if (divElement) {
      divElement.scrollIntoView({ behavior: "smooth" });
    }
  };

  return (
    <>
      <div className={landingStyles.JCaspLightFeatureSection}>
        <div className={"featHeader"}>
          <h2>JCasp LITE: Features at a Glance</h2>
          <div className="introGigParaghraph">
            <p>
              We understand your passion for hand-picking your live music.
              JCasp LITE simplifies bookings, supercharges promotion, and
              expands your access to a diverse array of artists.
            </p>
          </div>
        </div>
        <div className={landingStyles.gigFeature}>
          <div
            className={`featuresCollaps`}
            data-aos="fade-down"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <h2>JCasp LITE Features</h2>
            <div className="accordion" id="accordionExample">
              <div className={`accordion-item acordingToItem`}>
                <h2 className="accordion-header" id="headingOne1">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseOne1"
                    aria-expanded="true"
                    aria-controls="collapseOne1"
                  >
                    Premier Listing
                  </button>
                </h2>
                <div
                  id="collapseOne1"
                  className="accordion-collapse collapse show"
                  aria-labelledby="headingOne1"
                  data-bs-parent="#accordionExample"
                >
                  <div className="accordion-body">
                    Your venue shines in the spotlight with premium placement on
                    the JCasp website and both iOS and Android apps. Stand out
                    from the crowd and attract top talent and music lovers
                    alike.
                  </div>
                </div>
              </div>

              <div className={`accordion-item acordingToItem`}>
                <h2 className="accordion-header" id="headingTwo1">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseTwo1"
                    aria-expanded="false"
                    aria-controls="collapseTwo1"
                  >
                    Quick Access to Vetted Artists
                  </button>
                </h2>
                <div
                  id="collapseTwo1"
                  className="accordion-collapse collapse"
                  aria-labelledby="headingTwo1"
                  data-bs-parent="#accordionExample"
                >
                  <div className="accordion-body">
                    Accelerate your booking process with JCasp LITE. We
                    swiftly provide you with the availability and direct contact
                    information of the top artists that match your venue&lsquo;s
                    needs and budget.
                  </div>
                </div>
              </div>
              <div className={`accordion-item acordingToItem`}>
                <h2 className="accordion-header" id="headingThree1">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseThree1"
                    aria-expanded="false"
                    aria-controls="collapseThree1"
                  >
                    Custom Venue Calendar
                  </button>
                </h2>
                <div
                  id="collapseThree1"
                  className="accordion-collapse collapse"
                  aria-labelledby="headingThree1"
                  data-bs-parent="#accordionExample"
                >
                  <div className="accordion-body">
                    Effortlessly manage your live music schedule with our
                    tailored venue calendar. Complete with an embed code for
                    your website, this tool enhances patron engagement and
                    showcases your lineup. For an added touch, consider our
                    add-on option of monthly printed 4x6 calendars delivered to
                    you each month for wider distribution within your community.
                  </div>
                </div>
              </div>
            </div>
            <Button
              onClick={() => handleJCaspLight("Join JCasp LITE")}
              htmlType="submit"
              type="primary"
            >
              Join JCasp LITE
            </Button>
          </div>

          <div className="">
            <Image
              src="/images/landing-page/gigLitefeature1.png"
              alt="feature"
              width={738}
              height={668}
            />
            {/* <img src="/images/landing-page/gigLitefeature1.png" alt="feature" /> */}
          </div>
        </div>

        <div className={landingStyles.gigFeature}>
          <div className="imgForMobile">
            <Image
              src="/images/landing-page/gigLitefeature2.png"
              alt="feature"
              width={799}
              height={883}
            />
            {/* <img src="/images/landing-page/gigLitefeature2.png" alt="feature" /> */}
          </div>

          <div
            className={`imgForMobileC featuresCollaps pt-0`}
            data-aos="fade-down"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <h2>JCasp LITE Features</h2>
            <div className="accordion" id="accordionExampletwo">
              <div className={`accordion-item acordingToItem`}>
                <h2 className="accordion-header" id="headingOne">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseOne"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    Personalized Service
                  </button>
                </h2>
                <div
                  id="collapseOne"
                  className="accordion-collapse collapse show"
                  aria-labelledby="headingOne"
                  data-bs-parent="#accordionExampletwo"
                >
                  <div className="accordion-body">
                    At JCasp LITE, we see ourselves as partners in your live
                    music journey. Our dedicated team is always ready to assist,
                    providing guidance and tools to help you manage your music
                    program more smoothly and efficiently, while you remain in
                    control.
                  </div>
                </div>
              </div>

              <div className={`accordion-item acordingToItem`}>
                <h2 className="accordion-header" id="headingTwo">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseTwo"
                    aria-expanded="false"
                    aria-controls="collapseTwo"
                  >
                    On-Demand Assistance
                  </button>
                </h2>
                <div
                  id="collapseTwo"
                  className="accordion-collapse collapse"
                  aria-labelledby="headingTwo"
                  data-bs-parent="#accordionExampletwo"
                >
                  <div className="accordion-body">
                    Whenever you need help or have questions, our team is just a
                    call away. Benefit from our prompt and reliable on-demand
                    assistance to keep your music program running seamlessly.
                  </div>
                </div>
              </div>
              <div className={`accordion-item acordingToItem`}>
                <h2 className="accordion-header" id="headingThree">
                  <button
                    className="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseThree"
                    aria-expanded="false"
                    aria-controls="collapseThree"
                  >
                    Preferred Access to PRO
                  </button>
                </h2>
                <div
                  id="collapseThree"
                  className="accordion-collapse collapse"
                  aria-labelledby="headingThree"
                  data-bs-parent="#accordionExampletwo"
                >
                  <div className="accordion-body">
                    Unlock the power of JCasp PRO on your terms. As a JCasp
                    LITE subscriber, you enjoy preferential access to our
                    full-service offerings. From scheduling and curation to
                    payments and social media promotion, these services are
                    available whenever you need them. Experience flexibility and
                    efficiency tailored to your live music program.
                  </div>
                </div>
              </div>
            </div>
            <Button
              onClick={() => handleJCaspLight("Join JCasp LITE")}
              htmlType="submit"
              type="primary"
            >
              Join JCasp LITE
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};
export default JCaspLightFeatures;
