import landingStyles from "./landing.module.scss";
import Link from "next/link";
import Image from "next/image";

const DownloadJCaspLight = () => {
  return (
    <>
      <div className={`${landingStyles.download_container}`}>
        <div className="dwlPage">
          <Image
            src="/images/landing-page/left-side-screenshots.png"
            alt="feature"
            width={631}
            height={578}
          />
          {/* <img
            src="/images/landing-page/left-side-screenshots.png"
            alt="feature"
          /> */}
        </div>
        <div className="dwlPageApplication">
          <h2>Download the JCasp App Today!</h2>
          <p>
            Explore a world of artists and venues right from your smartphone.
            Discover the heart of live music with JCasp.
          </p>
          <div className="downlaodLink">
            <Link
              href="https://play.google.com/store/apps/details?id=com.JCasp.fender"
              target="_blank"
              data-aos="fade-down"
              className="hvr-float-shadow"
            >
              <Image
                src="/images/home/google-play.webp"
                alt="Download from playstore"
                width={170}
                height={51}
              />
            </Link>
            <Link
              href="https://apps.apple.com/us/app/JCasp-local-shows/id932936250"
              target="_blank"
              className="hvr-float-shadow"
              data-aos="fade-up"
            >
              <Image
                src="/images/home/app_store.webp"
                alt="Download from appstore"
                width={170}
                height={51}
              />
              {/* <img src="/images/app_store.png" alt="Download from appstore" /> */}
            </Link>
          </div>
        </div>
        <div className="dwlPage">
          <Image
            src="/images/landing-page/right-side-screenshots.png"
            alt="feature"
            width={631}
            height={578}
          />
          {/* <img
            src="/images/landing-page/right-side-screenshots.png"
            alt="feature"
          /> */}
        </div>
      </div>
    </>
  );
};
export default DownloadJCaspLight;
