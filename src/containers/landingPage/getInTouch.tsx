import { Button, DefaultLoader } from "@components/theme";
import {
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { landingPagerequestAPI } from "@redux/services/landing.api";
import {
  GeneralState,
  setEnquiryPhone,
  setEnquiryType,
} from "@redux/slices/general";
import { Col, Row } from "antd";
import { useReCaptcha } from "next-recaptcha-v3";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  LandingPageFormInputs,
  LandingPageFormValidateSchema,
} from "src/schemas/LandingPageFormSchema";
import landingPageStyles from "./landing.module.scss";

export interface GetInTouchProps {}

export const GetInTouch = (props: GetInTouchProps) => {
  const {} = props;

  const {
    general: { enquiryType, enquiryPhone },
  }: {
    general: GeneralState;
  } = useSelector((state: RootState) => ({
    general: state.general,
  }));
  const dispatch = useDispatch();

  const { executeRecaptcha, loaded } = useReCaptcha();

  const { register, handleSubmit, formState, reset, setValue } =
    useForm<LandingPageFormInputs>({
      resolver: yupResolver(LandingPageFormValidateSchema),
    });

  const [isLoading, setIsLoading] = useState(false);

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      const recaptchaResponse = await executeRecaptcha("contactEnquiries");
      await landingPagerequestAPI(data, recaptchaResponse);
      showToast(SUCCESS_MESSAGES.landingPageSuccessMessage, "success");
      setIsLoading(false);
      reset();
      dispatch(setEnquiryPhone(""));
      dispatch(setEnquiryType(""));
    } catch (error) {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (enquiryPhone) {
      setValue("phoneNumber", enquiryPhone);
    }
    if (enquiryType) {
      setValue("enquiryType", enquiryType);
    }
  }, [enquiryType, enquiryPhone]);

  return (
    <>
      <div className={landingPageStyles.get_in_touch}>
        <div className="information">
          <div>
            <Row>
              <Col span={24} className="space-bottom">
                <h3 className="black-heading mb-0">
                  Need Assistance? We&lsquo;re Here for You!
                </h3>
              </Col>
              <Col span={24} className="space-bottom">
                <p className="contect-description mb-0">
                  At JCasp, we&lsquo;re committed to your success. Whether you
                  have questions, need help with your account, or want to make
                  the most of your live music program, we&lsquo;re ready to
                  assist. Reach out to our team anytime!
                </p>
              </Col>
            </Row>

            <Row className="">
              <Col span={24} className="space-bottom">
                <Row>
                  <Col span={12} className="contact-box-spaces">
                    <div className="contact_information_box">
                      <div>
                        <Button htmlType="button" disabled type="primary">
                          <Image
                            src="/images/landing-page/phone-color.png"
                            alt="phone"
                            width={24}
                            height={24}
                          />
                          {/* <img
                            src="/images/landing-page/phone-color.png"
                            alt="phone"
                          /> */}
                        </Button>
                      </div>
                      <Link href="tel:415-918-61650">415-918-6165</Link>
                      <p>
                        Interested in our live music services? Call or text us
                        today. Our dedicated team is ready to help.
                      </p>
                    </div>
                  </Col>
                  <Col span={12} className="contact-box-spaces ">
                    <div className="contact_information_box">
                      <div>
                        <Button htmlType="button" disabled type="primary">
                          <Image
                            src="/images/landing-page/email-color.png"
                            alt="email"
                            width={24}
                            height={24}
                          />
                          {/* <img
                            src="/images/landing-page/email-color.png"
                            alt="email"
                          /> */}
                        </Button>
                      </div>
                      <Link href="mailto:contact@jcasptechnologies.com">
                        contact@jcasptechnologies.com
                      </Link>
                      <p>
                        Feel free to send us an email and we will follow up with
                        you shortly!
                      </p>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
            <div className="hide-on-small-device">
              <Row className="space-bottom space-top">
                <Col span={24}>
                  <h3 className="white-heading mb-0 text-white">
                    Powering Live Music with JCasp
                  </h3>
                </Col>
              </Row>
              <div className="logos">
                <div>
                  <Image
                    src="/images/landing-page/black-angus.png"
                    alt="black-angus"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/black-angus.png"
                    alt="black-angus"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/bridgessantafe.png"
                    alt="bridgessantafe"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/bridgessantafe.png"
                    alt="bridgessantafe"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/crosbysantafe.png"
                    alt="crosbysantafe"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/crosbysantafe.png"
                    alt="crosbysantafe"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/griffinclub.png"
                    alt="griffinclub"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/griffinclub.png"
                    alt="griffinclub"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/crossingscarlsbad.png"
                    alt="crossingscarlsbad"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/crossingscarlsbad.png"
                    alt="crossingscarlsbad"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/draftrepublix.png"
                    alt="draftrepublix"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/draftrepublix.png"
                    alt="draftrepublix"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/rubios.png"
                    alt="rubios"
                    width={115}
                    height={42}
                  />
                  {/* <img src="/images/landing-page/rubios.png" alt="rubios" /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/hilton.png"
                    alt="hilton"
                    width={115}
                    height={42}
                  />
                  {/* <img src="/images/landing-page/hilton.png" alt="hilton" /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/coto-de-caza.png"
                    alt="coto-de-caza"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/coto-de-caza.png"
                    alt="coto-de-caza"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/jcresorts.png"
                    alt="jcresorts"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/jcresorts.png"
                    alt="jcresorts"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/northcity.png"
                    alt="northcity"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/northcity.png"
                    alt="northcity"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/isolation-mode.png"
                    alt="isolation-mode"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/isolation-mode.png"
                    alt="isolation-mode"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/morgan-run.png"
                    alt="morgan-run"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/morgan-run.png"
                    alt="morgan-run"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/sun-outdoors.png"
                    alt="sun-outdoors"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/sun-outdoors.png"
                    alt="sun-outdoors"
                  /> */}
                </div>
                <div>
                  <Image
                    src="/images/landing-page/ucsd.png"
                    alt="ucsd"
                    width={115}
                    height={42}
                  />
                  {/* <img src="/images/landing-page/ucsd.png" alt="ucsd" /> */}
                </div>
                <div className="hide-on-three-columns">
                  <Image
                    src="/images/landing-page/bistro-seven.png"
                    alt="bistro-seven"
                    width={115}
                    height={42}
                  />
                  {/* <img
                    src="/images/landing-page/bistro-seven.png"
                    alt="bistro-seven"
                  /> */}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="contact-form">
          <form onSubmit={handleSubmit(onSubmit)} id="contactForm">
            <InputField
              {...{
                register,
                formState,
                defaultValue: "Get in Touch",
                type: "hidden",
                id: "enquiryType",
              }}
            />
            <div className="group">
              <div className="input-box">
                <InputField
                  {...{
                    register,
                    formState,
                    id: "firstName",
                    label: "First Name",
                    placeholder: "Enter First Name",
                  }}
                />
              </div>

              <div className="input-box">
                <InputField
                  {...{
                    register,
                    formState,
                    id: "lastName",
                    label: "Last Name",
                    placeholder: "Enter Last Name",
                  }}
                />
              </div>
            </div>
            <div className="group">
              <div className="input-box">
                <InputField
                  {...{
                    register,
                    formState,
                    id: "phoneNumber",
                    label: "Phone Number*",
                    placeholder: "Enter Phone Number",
                  }}
                />
              </div>

              <div className="input-box">
                <InputField
                  {...{
                    register,
                    formState,
                    id: "email",
                    label: "Email Address*",
                    placeholder: "Enter Email Address",
                  }}
                />
              </div>
            </div>

            <div className="group">
              <div className="input-box">
                <InputField
                  {...{
                    register,
                    formState,
                    id: "restaurantName",
                    label: "Restaurant Name",
                    placeholder: "Enter Restaurant Name",
                  }}
                />
              </div>

              <div className="input-box">
                <InputField
                  {...{
                    register,
                    formState,
                    id: "website",
                    label: "Website (if have)",
                    placeholder: "Enter Website Address",
                  }}
                />
              </div>
            </div>
            <div className="group">
              <div className="w-100">
                <TextAreaField
                  {...{
                    register,
                    formState,
                    id: "reason",
                    label: "Tell Us About Your Needs",
                    placeholder: "I’d like to know more about...",
                  }}
                />
              </div>
            </div>

            <div className="group mb-0">
              <div>
                <Button
                  htmlType="submit"
                  type="primary"
                  disabled={formState?.isSubmitting || isLoading}
                  loading={formState?.isSubmitting || isLoading}
                >
                  Send Message
                </Button>
              </div>
            </div>
            {(formState?.isSubmitting || isLoading) && <DefaultLoader />}
          </form>
        </div>
        <div className="show-on-small-device">
          <Row className="space-bottom">
            <Col span={24}>
              <h3 className="white-heading mb-0 text-white">
                Powering Live Music with JCasp
              </h3>
            </Col>
          </Row>
          <div className="logos">
            <div>
              <Image
                src="/images/landing-page/black-angus.png"
                alt="black-angus"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/black-angus.png"
                alt="black-angus"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/bridgessantafe.png"
                alt="bridgessantafe"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/bridgessantafe.png"
                alt="bridgessantafe"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/crosbysantafe.png"
                alt="crosbysantafe"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/crosbysantafe.png"
                alt="crosbysantafe"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/griffinclub.png"
                alt="griffinclub"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/griffinclub.png"
                alt="griffinclub"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/crossingscarlsbad.png"
                alt="crossingscarlsbad"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/crossingscarlsbad.png"
                alt="crossingscarlsbad"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/draftrepublix.png"
                alt="draftrepublix"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/draftrepublix.png"
                alt="draftrepublix"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/rubios.png"
                alt="rubios"
                width={115}
                height={42}
              />
              {/* <img src="/images/landing-page/rubios.png" alt="rubios" /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/hilton.png"
                alt="hilton"
                width={115}
                height={42}
              />
              {/* <img src="/images/landing-page/hilton.png" alt="hilton" /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/coto-de-caza.png"
                alt="coto-de-caza"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/coto-de-caza.png"
                alt="coto-de-caza"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/jcresorts.png"
                alt="jcresorts"
                width={115}
                height={42}
              />
              {/* <img src="/images/landing-page/jcresorts.png" alt="jcresorts" /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/northcity.png"
                alt="northcity"
                width={115}
                height={42}
              />
              {/* <img src="/images/landing-page/northcity.png" alt="northcity" /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/isolation-mode.png"
                alt="isolation-mode"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/isolation-mode.png"
                alt="isolation-mode"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/morgan-run.png"
                alt="morgan-run"
                width={115}
                height={42}
              />
              {/* <img src="/images/landing-page/morgan-run.png" alt="morgan-run" /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/sun-outdoors.png"
                alt="sun-outdoors"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/sun-outdoors.png"
                alt="sun-outdoors"
              /> */}
            </div>
            <div>
              <Image
                src="/images/landing-page/ucsd.png"
                alt="ucsd"
                width={115}
                height={42}
              />
              {/* <img src="/images/landing-page/ucsd.png" alt="ucsd" /> */}
            </div>
            <div className="hide-on-three-columns">
              <Image
                src="/images/landing-page/bistro-seven.png"
                alt="bistro-seven"
                width={115}
                height={42}
              />
              {/* <img
                src="/images/landing-page/bistro-seven.png"
                alt="bistro-seven"
              /> */}
            </div>
          </div>
        </div>
        <div className="pattern">
          <Image
            src="/images/landing-page/pattern.png"
            alt=""
            width={1900}
            height={240}
          />
          {/* <img src="/images/landing-page/pattern.png" alt="" /> */}
        </div>
      </div>
    </>
  );
};
