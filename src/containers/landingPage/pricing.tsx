import { Divider } from "antd";
import Link from "next/link";
import landingStyles from "./landing.module.scss";
import { useDispatch } from "react-redux";
import { setEnquiryPhone, setEnquiryType } from "@redux/slices/general";
import { Button } from "@components/theme";
import Image from "next/image";

const Pricing = () => {
  const dispatch = useDispatch();

  const handlePricingPlan = (data: any) => {
    if (data) {
      dispatch(setEnquiryType(data));
      scrollToDiv();
    }
  };

  const scrollToDiv = () => {
    const divElement = document.getElementById("contactForm");
    if (divElement) {
      divElement.scrollIntoView({ behavior: "smooth" });
    }
  };

  return (
    <>
      <div className={landingStyles.pricing}>
        <div className="content-box">
          <div className="description">
            <h2>Maximize Your Music Investment</h2>
            <p>No-Hassle Cancellation, Anytime</p>

            <Divider className="mt-2 mb-3" />
            <p>
              Do you require more information about JCasp’s services? <br />{" "}
              Feel free to send us an email at{" "}
              <Link href={"mailto:contact@jcasptechnologies.com"}>
                contact@jcasptechnologies.com
              </Link>{" "}
              or call us at <Link href={"tel:415-918-6165"}>415-918-6165</Link>
            </p>
            <Link href={"mailto:contact@jcasptechnologies.com"}>contact@jcasptechnologies.com</Link>
          </div>
          <div className="plan">
            <div className={`mpop`}>Most Popular</div>
            <div>
              <Image
                src={"/images/landing-page/price-box-progress.png"}
                width={60}
                height={60}
                alt="Landing Page"
              />
              {/* <img
                src="/images/landing-page/price-box-progress.png"
                alt="progress"
              /> */}
            </div>
            <strong>Monthly subscription</strong>
            <div className="price">
              <h2>
                $50.00 <span>/ Month</span>
              </h2>
            </div>
            <Button
              htmlType="button"
              type="primary"
              onClick={() => handlePricingPlan("Monthly subscription")}
            >
              Select Subscription
            </Button>
          </div>
          <div className="plan annPaln">
            <div className="tag-button">
              <Image
                src={"/images/landing-page/price-box-progress-up.png"}
                width={60}
                height={60}
                alt="Landing Page"
              />
              {/* <img
                src="/images/landing-page/price-box-progress-up.png"
                alt="progress"
              /> */}
              <Button htmlType="button" type="primary" disabled>
                4 Months Free
              </Button>
            </div>
            <strong>Annual Subscription</strong>
            <div className="price">
              <h2>
                $400.00 <span>/ Annual</span>
              </h2>
            </div>
            <Button
              htmlType="button"
              type="primary"
              onClick={() => handlePricingPlan("Annual Subscription")}
            >
              Get Started with JCasp LITE
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};
export default Pricing;
