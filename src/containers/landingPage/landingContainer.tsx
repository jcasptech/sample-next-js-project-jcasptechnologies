import { LandingLayoutComponent } from "@components/layout/landingLayout/landingLayoutComponent";
import LandingPageScene from "./landingScene";

const LandingPageContainer = () => {
  return (
    <>
      <LandingPageScene />
    </>
  );
};

LandingPageContainer.Layout = LandingLayoutComponent;

export default LandingPageContainer;
