import Image from "next/image";
import landingStyles from "./landing.module.scss";
const IntroduceJCaspLight = () => {
  return (
    <>
      <div className={landingStyles.introduce_JCasp_light}>
        <h2>Introducing JCasp LITE</h2>
        <div className="introGigLite">
          <p>
            JCasp LITE is designed specifically for restaurants that want to
            book their own music while still enjoying the benefits of
            JCasp&apos;s expertise and resources. Our affordable subscription
            options and flexible cancellation policy make it easy to access our
            valuable services without breaking the bank.
          </p>
        </div>
        <div className="introGigLiteImage">
          <Image
            src={"/images/landing-page/introduce-image.png"}
            width={1306}
            height={1073}
            alt="introduce JCasp"
          />
          {/* <img src="/images/landing-page/introduce-image.png" alt="" /> */}
        </div>
      </div>
    </>
  );
};
export default IntroduceJCaspLight;
