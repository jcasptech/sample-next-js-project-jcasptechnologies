import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { updatePasswordAPI } from "@redux/services/auth.api";
import { useReCaptcha } from "next-recaptcha-v3";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ChangePasswordFormInputs,
  ChangePasswordFormValidateSchema,
} from "src/schemas/changePasswordFormSchema";
import ChangePasswordScene from "./changePasswordScene";

const ChangePasswordContainer = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { executeRecaptcha, loaded } = useReCaptcha();

  const { register, handleSubmit, formState, reset } =
    useForm<ChangePasswordFormInputs>({
      resolver: yupResolver(ChangePasswordFormValidateSchema),
    });

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      await updatePasswordAPI(data);
      showToast(SUCCESS_MESSAGES.resetPasswordRequest, "success");
      reset();
      setIsLoading(false);
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  return (
    <ChangePasswordScene
      {...{
        handleSubmit,
        onSubmit,
        register,
        formState,
        isLoading,
      }}
    />
  );
};

ChangePasswordContainer.Layout = AdminLayoutComponent;
export default ChangePasswordContainer;
