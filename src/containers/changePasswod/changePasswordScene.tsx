import {
  InputPasswordField,
  InputPasswordFieldWithMessage,
} from "@components/theme/form/formFieldsComponent";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import Spinner from "@components/theme/spinner";
import { Button } from "@components/theme/button";
import React, { FormEvent } from "react";
import changePasswordStyles from "./changePassword.module.scss";

export interface ChangePasswordSceneProps {
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  register: any;
  formState: any;
  isLoading: boolean;
}
const ChangePasswordScene = (props: ChangePasswordSceneProps) => {
  const { handleSubmit, onSubmit, register, formState, isLoading } = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="container-fluid extra-pad-c extra-pad-r">
            <div className="row m0">
              <div className="col-12 col-sm-12 contact-text request-text">
                <div className="row m0">
                  <div className="col-0 col-sm-0 col-md-2"></div>
                  <div
                    className="col-12 col-sm-12 col-md-8 form_holder aos-init aos-animate"
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    <form
                      onSubmit={handleSubmit(onSubmit)}
                      className={`${changePasswordStyles.form} formB panel-form`}
                    >
                      <div className="fieldH ">
                        <InputPasswordFieldWithMessage
                          {...{
                            register,
                            formState,
                            id: "newPassword",
                            label: "New Password",
                            className: `input`,
                            placeholder: "Enter a new Password",
                          }}
                        />
                      </div>
                      <div className="fieldH">
                        <InputPasswordField
                          {...{
                            register,
                            formState,
                            id: "confirmPassword",
                            label: "Confirm Password",
                            className: `input`,
                            placeholder: "Enter confirm your Password",
                          }}
                        />
                      </div>
                      <div className="request-btn">
                        <Button
                          htmlType="submit"
                          loading={isLoading}
                          disabled={isLoading}
                        >
                          Change Password
                        </Button>
                      </div>
                    </form>
                  </div>
                  <div className="col-0 col-sm-0 col-md-2"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ChangePasswordScene;
