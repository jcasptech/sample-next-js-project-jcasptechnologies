import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import TermsScene from "./termsOfUseScene";

const TermsContainer = () => {
  return (
    <>
      <SEO
        {...{
          pageName: "/terms-of-use",
        }}
      />
      <TermsScene />
    </>
  );
};

TermsContainer.Layout = MainLayoutComponent;
export default TermsContainer;
