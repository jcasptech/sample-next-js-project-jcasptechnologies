import { Row } from "antd";
import Link from "next/link";

const TermsScene = () => {
  return (
    <>
      <div className="lkPage">
        <div className="lkPageHeader">
          <div className="flex justify-between">
            <h1 className="text-2xl fw-700"> Terms and Conditions</h1>
          </div>
        </div>
        <Row className="d-flex" align="middle">
          <div className="lkFormCard bg-white">
            <div className="lkPageContent">
              <div className="lkCard">
                <form>
                  <div>
                    <div className="lkCardContent">
                      <div id="termsAndConditions">
                        {/* <h1 className="text-xl font-semibold">

                        </h1> */}
                        <p>
                          <b>JCasp TERMS OF SERVICE</b>
                          <br />
                          PLEASE READ THESE TERMS AND CONDITIONS OF USE
                          CAREFULLY.
                        </p>
                        <br />
                        <p>
                          <strong>Effective Date: </strong> APRIL 5, 2020
                        </p>
                        <br />
                        <p>
                          Welcome to JCasp (“JCasp”, “we”, “us”, or “our”),
                          a service provided by Local Music LLC. JCasp is
                          responsible for the development of JCasp.com (the
                          “Website” or “Site), our mobile applications (the
                          “Apps”), and all related data, software, APIs, and
                          services (together with Site and App, “our Services”.)
                          We have created these Terms & Conditions (the “Terms”)
                          to ensure a clear understanding between JCasp and
                          any person using our Services (“User”, “you”, “your”).
                          These Terms, along with our Privacy Policy, apply to
                          your use of our Services.
                        </p>
                        <br />
                        <p>
                          Upon registering for our Services, you submit that you
                          have read and agreed to these Terms and Privacy
                          Policy. If you do not agree to these Terms, do not use
                          our Services. By accepting these Terms, you have also
                          acknowledged that you are (i) 18 years of age or more,
                          or (ii) 13 years of age or more with your parent(s)’
                          or legal guardian(s)’ permission.
                          <strong>
                            {" "}
                            As set forth in the section entitled Arbitration
                            Exclusive Remedy, below, you agree to submit any
                            dispute with JCasp to binding arbitration.
                          </strong>{" "}
                          JCasp reserves the right to amend our Terms at any
                          time. Any recently amended terms will be highlighted
                          and shall be effective immediately when they are
                          posted on the Site.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          Description of our Services
                        </h1>
                        <p>
                          For registered performers (“Artists”), our Services
                          facilitate your connection and interaction with music
                          fans and venues in your area. We do this by aiding the
                          ability of all users of the App (hereinafter, “Users”)
                          to listen to and search for (together, “Discover”)
                          your uploaded songs, videos, media and other
                          information, as well as send you messages or “post
                          gigs” (hereinafter, “Post”) in which you can submit
                          your interest and availability. JCasp is not an
                          agent, employer, or partner of Artists and is not
                          engaging in the services of a “Talent Agency” as
                          defined in California Labor Code section 1700.4.
                          JCasp simply acts as a platform allowing Artists to
                          connect and interact with Users.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="termsAndTermination">
                        <h1 className="text-xl font-semibold">Your Account</h1>
                        <p>
                          <i>For Artists:</i>
                        </p>
                        <ol>
                          <li>
                            <p>
                              <strong>Registration</strong>
                            </p>
                            <p>
                              Artists must register an account to be granted
                              access to certain Services, such as the ability to
                              be Discovered and Messaged through the App. To
                              register an Artist Profile, you must provide us
                              with:
                            </p>
                            <ol type="I">
                              <li>
                                A valid email address and password. The email
                                address and password that you provide will act
                                as your sign in credentials to access your
                                account.
                              </li>
                              <li>
                                Artist name. This name will be displayed to
                                Users who try to Discover and message you.
                              </li>
                              <li>
                                Profile Picture. This will act as a visual
                                representation of you to Users.
                              </li>
                              <li>
                                {" "}
                                At least one YouTube video of you and/or your
                                music. This video gives Users a preview and
                                gives you an opportunity to show off your
                                skills. This video will also be displayed on
                                your Artist Profile. The video must be linked
                                from YouTube. You will enter the URL of your
                                YouTube video into the registration field
                                provided.
                              </li>
                              <li>
                                At least one original song. This song will be
                                added to playlists for when a User tries to
                                Discover artists in your area. These songs will
                                also be displayed on your Artist Profile. You
                                must be the individual performing in the song.
                                Each song must be given 1 or 2 categorization
                                tags, as well as up to 5 “similar artist tags;
                                these will aid Users in Discovering your music.
                              </li>
                              <li>
                                Home zip code or city. This allows us to connect
                                you with Users near your home location.
                              </li>
                              <li>
                                Whether you are a band, soloist or DJ. This
                                allows us to help Users quickly search and
                                Discover specific music formats within JCasp.
                              </li>
                              <li>
                                {" "}
                                <p>
                                  Information. This information is where Artists
                                  input their biography or pertinent details
                                  about their typical performance. It helps
                                  Users learn more about Artists, i.e., if they
                                  play weddings or prefer small intimate venues.
                                </p>
                              </li>
                            </ol>
                          </li>{" "}
                          <li>
                            <p>
                              <strong> Account approval or rejection</strong>
                            </p>
                            <p>
                              Upon receiving registration of Artist Profile
                              information from an Artist, JCasp may review the
                              account to ensure account information is complete
                              and satisfactory.{" "}
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                One Account, Multiple Artist Profiles
                              </strong>{" "}
                            </p>
                            <p>
                              Each Artist is only entitled to one account
                              corresponding to his or her email address.
                              However, the Artist may register multiple Artist
                              Profiles in order to offer additional
                              configurations for the same Artist or to aid in
                              the management of different Artists. JCasp
                              reserves the right to limit the number of Artist
                              Profiles an Artist may register if the additional
                              Artist Profiles result in too much duplicative
                              content in the App experience.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>Bands</strong>
                            </p>
                            <p>
                              In the event you are registering an account on
                              behalf of a band, you agree that your
                              representations, warranties and agreements under
                              these Terms are being made on behalf of yourself
                              individually and all members of the band. In
                              furtherance of the foregoing, you represent and
                              warrant that all members of the band have read and
                              agree to these Terms.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong> Account Responsibilities</strong>
                            </p>
                            <p>
                              You, not JCasp, are solely responsible for
                              maintaining the security of your account.
                              Likewise, you are solely liable for any and all
                              text, audio, videos, photos, or data (together,
                              your “Content”) on your account, whether or not
                              you authorized such activity. You are also solely
                              responsible for making sure your account
                              information is truthful, accurate, and updated.
                            </p>
                          </li>
                        </ol>

                        <p>
                          <i>For Users:</i>
                        </p>
                        <ol>
                          <li>
                            <p>
                              <strong>Registration</strong>
                            </p>
                            <p>
                              Although most functionality of the App is
                              available for use without Registration,
                              Registration is required to access certain
                              functionalities. To register a user profile, you
                              must provide us with:
                            </p>
                            <ol type="I">
                              <li>
                                <p>
                                  A valid email address and password. The email
                                  address and password that you provide will act
                                  as your sign in credentials to access your
                                  account.
                                </p>
                              </li>
                              <p>Optionally, you may provide us with:</p>
                              <li>
                                <p>
                                  Profile Picture. This will act as a visual
                                  representation of you to Artists and Users.
                                </p>
                              </li>
                            </ol>
                          </li>

                          <li>
                            {" "}
                            <p>
                              <strong>Account Responsibilities</strong>
                            </p>
                            <p>
                              You, not JCasp, are solely responsible for
                              maintaining the security of your account. Photos,
                              or data (together, your “Content”) on your
                              account, whether or not you authorized such
                              activity. You are also solely responsible for
                              making sure your account information is truthful,
                              accurate, and updated.
                            </p>
                          </li>
                        </ol>

                        <p>
                          <i>For Both Users and Artists:</i>
                        </p>

                        <ol>
                          <li>
                            <p>
                              <strong>
                                Closing or suspending your account
                              </strong>
                            </p>
                            <p>
                              JCasp reserves the right to suspend or close
                              your account at any point in time for any reason,
                              without notice or liability. If your account has
                              been compromised or you would like to close your
                              account, please notify JCasp at{" "}
                              <Link href="mailto:help@jcasptechnologies.com">
                                help@jcasptechnologies.com.
                              </Link>{" "}
                              You maintain the right to close your account at
                              any time.
                            </p>
                          </li>

                          <li>
                            <p>
                              <strong>Subject to Terms and Conditions</strong>
                            </p>
                            <p>
                              Any and all activity on your account remains
                              subject to these Terms and Conditions.
                            </p>
                          </li>
                        </ol>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="fees">
                        <h1 className="text-xl font-semibold">
                          Your Use of Our Services
                        </h1>
                        <p>
                          <i>For Both Users and Artists:</i>
                        </p>

                        <ol>
                          <li>
                            <p>
                              <strong>License granted by us</strong>
                            </p>
                            <p>
                              Through your registration and compliance with our
                              Terms, JCasp grants you and only you a limited
                              license to access our Services. This license only
                              applies to personal use of our Services; the
                              license does not include reproducing, copying,
                              duplicating, selling, or any commercial use of our
                              Services whatsoever unless express written consent
                              is given by JCasp. This license is rendered null
                              upon any unauthorized use of our Services.
                            </p>
                          </li>

                          <li>
                            <p>
                              <strong>Code of conduct</strong>
                            </p>
                            <p>
                              You are solely liable for any and all activity on
                              your account, whether or not you authorized such
                              activity. You agree that:
                            </p>

                            <ol type="I">
                              <li>
                                Any form of communication that is deemed
                                abusive, culturally or ethnically offensive,
                                defamatory, indecent, obscene, profane, racist,
                                or sexually explicit content is strictly
                                prohibited.
                              </li>
                              <li>
                                Your use of our Services is consistent with any
                                and all applicable laws and regulations.
                              </li>
                              <li>
                                Your activity does not promote violence,
                                terrorism, illegal acts, or hatred on the basis
                                of sexual orientation, gender, ethnicity,
                                culture, or religion.
                              </li>
                              <li>
                                You will not transmit in any fashion any
                                material that contains software viruses, Trojan
                                horses, malware, or any other computer code or
                                files which will disrupt the functionality of
                                any of JCasp’s Services.
                              </li>
                              <li>
                                You will not post, copy, reproduce, or
                                distribute in any fashion copyrighted content or
                                other proprietary information without the prior
                                consent of the Content’s owner.
                              </li>
                              <li>
                                You will not represent yourself in JCasp as
                                anyone other than yourself or falsify your
                                identity in any way.
                              </li>
                              <li>
                                You will maintain current and accurate contact
                                information on your profile. JCasp may edit or
                                remove any information at any time.
                              </li>
                              <li>
                                You will not use another User’s account or
                                personal information without her or his express
                                permission.
                              </li>
                              <li>
                                You will not threaten, stalk, or otherwise
                                harass any person in any way.
                              </li>
                              <li>
                                You will not use our Services to advertise or
                                solicit to others or harvest personal
                                information from other Users for commercial use
                                of any kind.
                              </li>
                              <li>
                                You will not use our Services to spam, “troll”,
                                or otherwise disrupt the experience for other
                                Users or the functionality of our Services in
                                any way.
                              </li>
                              <li>
                                You will not use robots, spiders, or data mining
                                applications of any sort to reproduce or bypass
                                our Services in any way.
                              </li>
                              <li>
                                You will not knowingly disrupt or interfere with
                                our servers or anything related to the
                                functionality of our Services in any way.
                              </li>
                              <li>
                                You will not use our Services to spam, “troll”,
                                or otherwise disrupt the experience for other
                                Users or the functionality of our Services in
                                any way.
                              </li>
                              <li>
                                You will not offer to or in actuality sell,
                                transfer, rent, or lease your account to any
                                other person or third party without prior
                                written consent of JCasp.
                              </li>
                              <li>
                                You will not use our Services to spam, “troll”,
                                or otherwise disrupt the experience for other
                                Users or the functionality of our Services in
                                any way.
                              </li>
                              <li>
                                JCasp has the right, but not the obligation,
                                to supervise and settle disputes between Artists
                                and Users.
                              </li>
                              <li>
                                You will not attempt to “game” JCasp by
                                falsely inflating “likes”, ratings, or other
                                positive measures for Artists. Conversely, you
                                may not actively hinder the ability of other
                                Artists to participate within our Services.
                              </li>
                            </ol>
                          </li>
                        </ol>
                        <p>
                          In the event of that you witness breaches of this code
                          of conduct, please notify JCasp immediately at{" "}
                          <Link href="mailto:help@jcasptechnologies.com">
                            help@jcasptechnologies.com.
                          </Link>{" "}
                          JCasp reserves the right to suspend or close User or
                          Artist accounts, as well as edit or take down any
                          content or information made available through our
                          Services.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="useApplication">
                        <h1 className="text-xl font-semibold">
                          Content and Property Rights
                        </h1>
                        <p>
                          <i>For Artists:</i>
                        </p>

                        <ol>
                          <li>
                            <p>
                              <strong>Proprietary rights of Artist</strong>
                            </p>
                            <p>
                              As between you and JCasp, any and all text,
                              audio, sound recordings, compositions, pictures,
                              photos, videos, graphics, messages, comments,
                              reviews, data, or information (together,
                              “Content”) that you submit, upload, publish, or
                              display through our Services is solely owned,
                              controlled and/or operated by you. By accepting
                              these Terms, you acknowledge that JCasp does not
                              claim any ownership in and has no liability with
                              regards to your Content.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>Our rights to your Content</strong>
                            </p>
                            <p>
                              By accepting these Terms, you hereby give JCasp
                              the right to use, duplicate, reproduce,
                              distribute, digitally transmit, display, perform,
                              share, stream, modify, edit, and/or remove your
                              Content, including all sound recordings and
                              compositions (collectively, “Music”) therein, as
                              is reasonable and appropriate in connection with
                              the Services. This includes, without limitation,
                              the right to (i) publicly perform your Music
                              through our Services, (ii) stream your Music (both
                              interactive and non-interactive), (iii) include
                              your Music in playlists, and (iv) make copies of
                              your Music in connection with any of the
                              foregoing. If you believe we have misused your
                              Content, please contact us immediately at{" "}
                              <Link href="mailto:help@jcasptechnologies.com">
                                help@jcasptechnologies.com.
                              </Link>{" "}
                              The only remedy is that we will remove the
                              pertinent Content from our Services. We are not
                              liable for any misuse otherwise.
                            </p>
                            <p>
                              You represent that you either own or control any
                              and all rights in and to all Content as necessary
                              to grant JCasp the rights described above,
                              including all necessary rights with respect to all
                              Music. You also represent that all Content and
                              Music submitted to or posted on JCasp was not
                              produced pursuant to any guild or union collective
                              bargaining agreement (e.g., SAG-AFTRA, AFM), and
                              that JCasp will not owe any residuals,
                              royalties, monies, or other obligations to any
                              guild, union, or artist. JCasp does not and will
                              not assume any guild or union obligations. Just to
                              be safe, we thought it would help to give you two
                              examples:{" "}
                            </p>
                            <ul>
                              <li>
                                If you wrote a song with other people (including
                                members of your band) or if a piece of your
                                Music is based on an existing third party song,
                                you are agreeing that you have either acquired
                                all of the rights of the other songwriters,
                                received the other songwriters’ express
                                permission to post the Music to our Services, or
                                have received express permission to post the
                                Music to our Services from the owners of the
                                song’s publishing rights.
                              </li>
                              <li>
                                {" "}
                                <p>
                                  {" "}
                                  If you are signed to a record label and that
                                  label owns your sound recordings or publishing
                                  rights, you are agreeing that you have your
                                  record company’s express permission to post
                                  its sound recordings to our Services. You are
                                  also agreeing that the sound recording,
                                  including all samples or other portions of
                                  that sound recording, were not produced under
                                  any guild or union contract, including but not
                                  limited to any SAG-AFTRA or AFM contract.
                                </p>
                              </li>
                            </ul>
                            <p>
                              If you do not have these rights or are unclear,
                              please contact us immediately prior to posting the
                              Content at{" "}
                              <Link href="mailto:help@jcasptechnologies.com">
                                help@jcasptechnologies.com.
                              </Link>{" "}
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>Copyright infringement.</strong>
                            </p>
                            <p>
                              Without limiting anything in Section 3 above, you
                              may not post any unauthorized copyrighted material
                              that constitutes an infringement of third party
                              rights. Offenders of third party copyright laws
                              are subject to having their accounts terminated,
                              as well as being prosecuted in court by the owner
                              of such copyrighted Content.
                            </p>
                            <p>
                              If you are a third party content owner who
                              believes that any piece of Content uploaded by a
                              User infringes upon your copyrights, we will
                              respond promptly to any properly submitted notice
                              containing the information detailed below.
                              Pursuant to Title 17, United States Code, Section
                              512(c)(2), written notifications of claimed
                              copyright infringement should be sent to our
                              Designated Agent at the following contact
                              information:{" "}
                              <Link href="mailto:contact@jcasptechnologies.com">
                                contact@jcasptechnologies.com
                              </Link>
                            </p>
                            <p>
                              To be effective, the notification must be a
                              written communication that includes the following:
                            </p>
                            <ul>
                              <li>
                                A physical or electronic signature of person
                                authorized to act on behalf of the owner of an
                                exclusive right that is allegedly infringed;
                              </li>
                              <li>
                                Identification of the copyrighted work claimed
                                to have been infringed, or if multiple
                                copyrighted works at a single online site are
                                covered by a single notification, a
                                representative list of such works at that site;
                              </li>
                              <li>
                                Identification of the material that is claimed
                                to be infringing or to be the subject of
                                infringing activity and that is to be removed or
                                access to which is to be disabled, and
                                information reasonably sufficient to permit us
                                to locate the material;
                              </li>
                              <li>
                                Information reasonably sufficient to permit us
                                to contact the complaining party, such as an
                                address, telephone number, and if available, an
                                electronic mail address at which the complaining
                                party may be contacted;
                              </li>
                              <li>
                                A statement that the complaining party has a
                                good faith belief that use of the material in
                                the manner complained of is not authorized by
                                the copyright owner, its agent, or the law;
                              </li>
                              <li>
                                <p>
                                  {" "}
                                  A statement that the information in the
                                  notification is accurate, and under penalty of
                                  perjury, that the complaining party is
                                  authorized to act on behalf of the owner of an
                                  exclusive right that is allegedly infringed.
                                </p>
                              </li>
                            </ul>
                          </li>
                          <li>
                            <p>
                              <strong>Liability and risk of Content.</strong>
                            </p>
                            <p>
                              By accepting these Terms and Conditions, you
                              acknowledge that full liability and risk of posted
                              Content lie with the Artist.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>Reposting of Content.</strong>
                            </p>
                            <p>
                              You may not repost another User’s or Artist’s
                              Content without that said User’s or Artist’s
                              permission.
                            </p>
                          </li>
                        </ol>
                        <p>
                          <i>For Both Users and Artists:</i>
                        </p>
                        <ol>
                          <li>
                            <p>
                              <strong>Proprietary rights of JCasp</strong>
                            </p>
                            <p>
                              While JCasp grants you a limited license to
                              access our Services upon registration, JCasp
                              maintains ownership and all proprietary rights to
                              all facets of our Services, including but not
                              limited to our logo, name, features, interfaces,
                              code, APIs, and web domains. You are not permitted
                              to use, reproduce, duplicate, publish, distribute,
                              or display JCasp’s proprietary Services without
                              JCasp’s express written consent.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>Privacy Policy.</strong>
                            </p>
                            <p>
                              By accepting these Terms and Conditions, you also
                              accept our Privacy Policy. Please review our
                              Privacy Policy for relevant information
                              <Link
                                href="/privacy-policy"
                                className=""
                                target="_blank"
                              >
                                {" "}
                                here.
                              </Link>
                            </p>
                          </li>
                        </ol>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="restrictionsMaterials">
                        <h1 className="text-xl font-semibold">Messaging</h1>
                        <ol>
                          <li>
                            <p>
                              <strong>Who Can Send a Message</strong>
                            </p>
                            <p>
                              All Users that have completed the registration
                              process can send an artist a message on JCasp.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>How Users Can Send a Message</strong>
                            </p>
                            <p>
                              Users can send a message to any Artist on JCasp
                              by visiting the Artist Profile on the mobile app
                              or web platform. When a message is sent, the
                              artist will receive an email containing the User’s
                              message and the email address associated with the
                              User’s registered account. A response from the
                              Artist is not required, and is at their personal
                              discretion.
                            </p>
                          </li>
                        </ol>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="responsibleForServiceAccount">
                        <h1 className="text-xl font-semibold">
                          Post-a-Gig and Contacting Artists for Performance
                          Availability
                        </h1>
                        <p>
                          <strong>“Post-a-Gig”</strong>
                        </p>
                        <p>
                          Through this manner of contacting artists, Users
                          provide the a number of event details before “Posting”
                          the gig. When a gig is posted, relevant Artists will
                          be notified, and any Artists with a completed JCasp
                          Artist Account may begin to submit. Upon submitting
                          for a posting, the artist’s contact information will
                          be revealed to the User. The User is then free to
                          communicate with the artist outside of JCasp.
                          JCasp is in no way responsible for the conduct
                          demonstrated by the Artist or User, nor does JCasp
                          facilitate any type of payments that may have been
                          agreed upon between the User and Artist.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="privacy">
                        <h1 className="text-xl font-semibold">
                          Legal Disclaimers
                        </h1>
                        <h3>Limitation of Liability</h3>
                        <p>
                          IN NO SITUATION WILL JCasp’S LIABILITY FOR DAMAGES
                          BE IN EXCESS OF 100 USD OR THE TOTAL DOLLAR AMOUNTS
                          PAID BY YOU IN THE PREVIOUS 6 MONTHS LEADING UP TO THE
                          CLAIM. JCasp WILL HAVE NO LIABILITY FOR ANY DAMAGE
                          OR LOSS:
                        </p>
                        <ol type="I">
                          <li>
                            DERIVING FROM YOUR INABILITY TO ACCESS ANY OF OUR
                            SERVICES, CONTENT, OR RELATED PARTS.
                          </li>
                          <li>
                            RELATED TO YOUR COMPUTER HARDWARE, SOFTWARE, OR
                            DATA, INCLUDING DAMAGE OR LOSS FROM A SECURITY
                            BREACH
                          </li>
                          <li>
                            FROM ANY LEGAL ACTION WHATSOEVER AGAINST YOU,
                            WHETHER THE INVESTIGATED IS CONDUCTED BY JCasp OR
                            ANY LAW ENFORCEMENT AGENT OR AGENCY.
                          </li>
                          <li>
                            RELATED TO ANY TEMPORARY OR PERMANENT SUSPENSION OF
                            ACCESS TO OUR SERVICES OR CONTENT FOR ANY REASON
                          </li>
                          <li>
                            FROM ANY ERRORS, “BUGS”, OR INACCURACIES FOUND IN
                            OUR SERVICES OR RELATED CONTENT.
                          </li>
                          <li>
                            OF PROFITS, OPPORTUNITY, OR GOODWILL, DIRECTLY OR
                            INDIRECTLY.
                            <p>
                              BY ACCEPTING THESE TERMS, YOU ACCEPT THE RISK OF
                              DAMAGE OR LOSS OF ANY KIND. JCasp MUST BE
                              NOTIFIED IMMEDIATELY IN THE EVENT OF ANY CLAIM
                              RELATED TO YOUR USE OF OUR SERVICES. THESE TERMS
                              SHALL BE EXERCISED TO THEIR FULL EFFECT AS IS
                              ACCEPTABLE AND PERMITTED BY LAW. JCasp’S LIMITED
                              LIABILITY PERTAINS TO ALL OUR EMPLOYEES,
                              AFFILIATES, DIRECTORS, OFFICERS, OR SUBSIDIARIES.
                            </p>
                          </li>
                          <li>
                            RELATING TO INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE
                            OR CONSEQUENTIAL DAMAGES.
                          </li>
                        </ol>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="warrantyDisclaimer">
                        <h1 className="text-xl font-semibold">
                          Applicable Law and Jurisdiction
                        </h1>
                        <p>
                          In accepting our Terms and using our Services, you
                          acknowledge that these Terms will be governed by and
                          enforced in accordance with the laws of the State of
                          California. You and JCasp both agree to submit to
                          the exclusive jurisdiction of JAMS arbitration with
                          any proceeding to be held in either San Diego County
                          or Los Angeles County, California as set forth in the
                          section entitled Arbitration Exclusive Remedy, above.
                          You also acknowledge that the only remedy for any
                          breach of these Terms by JCasp or third party of any
                          kind is monetary damages as outlined by our Limitation
                          of Liability section in these Terms, and that you
                          waive any and all rights to injunctive or other forms
                          of equitable relief of any kind from or against
                          JCasp.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="a">
                        <h1 className="text-xl font-semibold">Indemnity</h1>
                        <p>
                          In accepting these Terms and using our Services, you
                          agree to indemnify, save, defend, and hold harmless
                          JCasp, our employees, affiliates, directors,
                          officers, and subsidiaries from any claims, damages,
                          demands, losses, costs, expenses, and reasonable
                          attorney fees arising from your use of Our Services,
                          any breach of our Terms, any third party copyright
                          infringement, or any activity from your account.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="b">
                        <h1 className="text-xl font-semibold">
                          Disclaimer of All Warranties and Assumption of Risk
                        </h1>
                        <p>
                          JCasp’S SERVICES ARE PROVIDED “AS IS”, “AS
                          AVAILABLE”, AND “WITH ALL FAULTS”, WITHOUT WARRANTIES
                          OF ANY KIND, EXPRESS OR IMPLIED. THIS PERTAINS TO, BUT
                          IS NOT LIMITED TO, WARRANTIES OF ACCURACY, TRUTH, ,
                          RELIABILITY, COMPLETENESS, NON-INFRINGEMENT,
                          TIMELINESS, SECURITY, DAMAGES, LOSSES, OR ANY AND ALL
                          IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A
                          PARTICULAR PURPOSE REGARDING OUR SERVICES. FOR
                          SITUATIONS IN WHICH STATE LAW PROHIBIT OR HAVE
                          LIMITATIONS ON THE DISCLAIMER OF IMPLIED OR OTHER
                          WARRANTIES LISTED IN THESE TERMS, STATE LAW HAS
                          JURISDICTION OVER THESE TERMS AND THESE DISCLAIMERS
                          MAY NOT APPLY TO YOU. YOU ACKNOWLEDGE THAT YOUR USE OF
                          OUR SERVICES IS AT YOUR OWN, SOLE RISK
                        </p>
                      </div>

                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="c">
                        <h1 className="text-xl font-semibold">Severability</h1>
                        <p>
                          If any provision in these Terms is deemed illegal,
                          unenforceable or invalid, that shall not affect the
                          legality, enforceability, or validity of any remaining
                          provision in these Terms.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="d">
                        <h1 className="text-xl font-semibold">
                          Third Party Agreements
                        </h1>
                        <p>
                          Through our Services you may encounter features that
                          grant you access to third party websites, databases,
                          servers, programs, software, information,
                          applications, or other similar services (together,
                          “Third Party Services” or “Their”). When you use these
                          Third Party Services, you may be subject to their own
                          terms and conditions, privacy policy, and other
                          requirements related to their use. JCasp is not
                          responsible or liable for the accuracy, legality, or
                          quality of any of Their content or operation in any
                          way, and does not express any warranty or endorsement
                          of any kind related to Third Party Services. These
                          Third Party Services are used solely to facilitate
                          your access to and use of JCasp. You use these Third
                          Party Services at your own risk.
                        </p>
                      </div>

                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="linkedSites">
                        <h1 className="text-xl font-semibold">
                          Mobile Application
                        </h1>
                        <p>
                          The following terms apply specifically to our Apps:
                        </p>
                        <ol type="I">
                          <li>
                            <p>
                              Application License. Subject to your compliance
                              with the Terms, JCasp grants you a limited
                              non-exclusive, non-transferable,
                              non-sublicensable, revocable license to download,
                              install and use our App on a single mobile device
                              or computer that you own or control and to run
                              such copy of the App solely for your own personal
                              use. Furthermore, with respect to any App accessed
                              through or downloaded from the Apple App Store (an
                              “App Store Sourced Application”), you will only
                              use the App Store Sourced Application (i) on an
                              Apple-branded product that runs the iOS (Apple’s
                              proprietary operating system) and (ii) as
                              permitted by the “Usage Rules” set forth in the
                              Apple App Store Terms of Service.
                            </p>
                          </li>
                          <li>
                            <p>
                              App Stores. You acknowledge and agree that the
                              availability of the App and our Services is
                              dependent on the third party from whom you
                              received the App license, e.g., the Apple iPhone
                              or Android app stores (in either case, “App
                              Store”). You acknowledge that these Terms are
                              between you and JCasp and not with the App
                              Store. JCasp, not the App Store, is solely
                              responsible for the our Services, including the
                              App, the content thereof, maintenance, support
                              services, and warranty therefor, and addressing
                              any claims relating thereto (e.g., product
                              liability, legal compliance or intellectual
                              property infringement). In order to use the App,
                              you must have access to a wireless network, and
                              you agree to pay all fees associated with such
                              access. You also agree to pay all fees (if any)
                              charged by the App Store in connection with our
                              Services. You agree to comply with, and your
                              license to use the App is conditioned upon your
                              compliance with, all applicable third-party terms
                              of agreement (e.g., the App Store’s terms and
                              policies) when using our Services. You acknowledge
                              that the App Store (and its subsidiaries) are
                              third-party beneficiaries of these Terms and will
                              have the right to enforce them.
                            </p>
                          </li>
                          <li>
                            <p>
                              Accessing and Download the Application from
                              iTunes. The following applies to any App Store
                              Sourced Application:
                            </p>
                            <ol type="a">
                              <li>
                                <p>
                                  You acknowledge and agree that (i) the Terms
                                  are concluded between you and JCasp only,
                                  and not Apple, and (ii) JCasp, not Apple, is
                                  solely responsible for the App Store Sourced
                                  Application and content thereof. Your use of
                                  the App Store Sourced Application must comply
                                  with the App Store Terms of Service.
                                </p>
                              </li>
                              <li>
                                <p>
                                  You acknowledge that Apple has no obligation
                                  whatsoever to furnish any maintenance and
                                  support services with respect to the App Store
                                  Sourced Application.
                                </p>
                              </li>
                              <li>
                                <p>
                                  In the event of any failure of the App Store
                                  Sourced Application to conform to any
                                  applicable warranty, you may notify Apple, and
                                  Apple will refund the purchase price for the
                                  App Store Sourced Application to you and to
                                  the maximum extent permitted by applicable
                                  law, Apple will have no other warranty
                                  obligation whatsoever with respect to the App
                                  Store Sourced Application. As between JCasp
                                  and Apple, any other claims, losses,
                                  liabilities, damages, costs or expenses
                                  attributable to any failure to conform to any
                                  warranty will be the sole responsibility of
                                  JCasp.
                                </p>
                              </li>
                              <li>
                                <p>
                                  You and JCasp acknowledge that, as between
                                  JCasp and Apple, Apple is not responsible
                                  for addressing any claims you have or any
                                  claims of any third party relating to the App
                                  Store Sourced Application or your possession
                                  and use of the App Store Sourced Application,
                                  including, but not limited to: (i) product
                                  liability claims; (ii) any claim that the App
                                  Store Sourced Application fails to conform to
                                  any applicable legal or regulatory
                                  requirement; and (iii) claims arising under
                                  consumer protection or similar legislation.
                                </p>
                              </li>
                              <li>
                                <p>
                                  You and JCasp acknowledge that, in the event
                                  of any third-party claim that the App Store
                                  Sourced Application or your possession and use
                                  of that App Store Sourced Application
                                  infringes that third party’s intellectual
                                  property rights, as between JCasp and Apple,
                                  JCasp, not Apple, will be solely responsible
                                  for the investigation, defense, settlement and
                                  discharge of any such intellectual property
                                  infringement claim to the extent required by
                                  these Terms.
                                </p>
                              </li>
                              <li>
                                <p>
                                  You and JCasp acknowledge and agree that
                                  Apple, and Apple’s subsidiaries, are
                                  third-party beneficiaries of these Terms as
                                  related to your license of the App Store
                                  Sourced Application, and that, upon your
                                  acceptance of the terms and conditions of
                                  these Terms, Apple will have the right (and
                                  will be deemed to have accepted the right) to
                                  enforce these Terms as related to your license
                                  of the App Store Sourced Application against
                                  you as a third-party beneficiary thereof. g.
                                </p>
                              </li>
                              <li>
                                <p>
                                  Without limiting any other terms of these
                                  Terms, you must comply with all applicable
                                  third-party terms of agreement when using the
                                  App Store Sourced Application.
                                </p>
                              </li>
                            </ol>
                          </li>
                        </ol>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="general">
                        <h1 className="text-xl font-semibold">
                          Concluding Remarks
                        </h1>
                        <ol type="1">
                          <li>
                            <p>
                              <strong>Changes to our Services</strong>
                            </p>
                            <p>
                              JCasp may modify, edit, update, or make changes
                              of any kind to our Services at any time without
                              prior warning.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>Notices</strong>
                            </p>
                            <p>
                              Notifications regarding important changes,
                              information, or updates related to our Services
                              will be sent via email to the email you provided
                              upon registration or push notification through the
                              App.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>Contacting JCasp</strong>
                            </p>
                            <p>
                              Please contact us at{" "}
                              <Link href="mailto:contact@jcasptechnologies.com">
                                contact@jcasptechnologies.com
                              </Link>{" "}
                              if you have any questions, concerns, or
                              suggestions
                            </p>
                          </li>
                        </ol>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Row>
      </div>
    </>
  );
};

export default TermsScene;
