import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { DefaultSkeleton } from "@components/theme";
import { RootState } from "@redux/reducers";
import { fetchShows, loadMoreShows, ShowsState } from "@redux/slices/shows";
import { useReCaptcha } from "next-recaptcha-v3";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DEFAULT_TABLE_LIMIT } from "src/libs/constants";
import useList from "src/libs/useList";
import BrowseOpenPostingsScene from "./browseOpenPostingsScene";
import { Show } from "react-redux-permission";
import { withRouter } from "next/router";
import { getShowFullDetailAPI } from "@redux/services/calendar.api";
import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import {
  fetchOpenPosting,
  loadMoreOpenPosting,
  OpenPostingObject,
  OpenPostingState,
} from "@redux/slices/openPosting";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import CalendarPostModal from "@components/modals/calendarPostingModal";
import { SeledtedArtistState } from "@redux/slices/artistList";
import { DateTime } from "luxon";
import moment from "moment";
import { fetchTags, TagsState } from "@redux/slices/tags";

export interface BrowseOpenPostingsContainerProps {}

const BrowseOpenPostingsContainer = (
  props: BrowseOpenPostingsContainerProps
) => {
  const dispatch = useDispatch();
  const [isPostModalOpen, setIsPostingModalOpen] = useState(false);
  const [tagOptions, setTagOptions] = useState<any>([]);
  const [selectedTags, setSelectedTags] = useState<any>(null);
  const [priceRange, SetPriceRange] = useState<number[]>([0, 2000]);
  const [dateRange, SetDateRange] = useState<
    [moment.Moment, moment.Moment] | null
  >(null);
  const [selectPosting, setSelectPosting] = useState<OpenPostingObject | null>(
    null
  );
  const [artistId, setArtistId] = useState(null);
  const { executeRecaptcha, loaded } = useReCaptcha();

  const {
    openPosting: { isLoading, data: openPostingData },
  }: {
    openPosting: OpenPostingState;
  } = useSelector((state: RootState) => ({
    openPosting: state.openPosting,
  }));

  const {
    selectedArtist: { data: selectedArtistData },
  }: {
    selectedArtist: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    selectedArtist: state.selectedArtist,
  }));

  const {
    tags: { data: tagsData },
  }: {
    tags: TagsState;
  } = useSelector((state: RootState) => ({
    tags: state.tags,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 9,
      skip: 0,
      include: ["selectedArtist", "venue"],
      orderByColumn: "startDate",
      orderBy: "ASCENDING",
      proceeds: priceRange,
    },
  });

  const getTags = async () => {
    const recaptchaResponse = await executeRecaptcha("tag_list");
    dispatch(fetchTags(apiParam, recaptchaResponse));
  };

  const handleTag = (d: any) => {
    setSelectedTags(d);
  };

  useEffect(() => {
    if (artistId && loaded) {
      apiParam.artistId = artistId;
      dispatch(fetchOpenPosting(apiParam));
      getTags();
    }
  }, [apiParam, artistId, loaded]);

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 9;
    dispatch(loadMoreOpenPosting(apiParam));
  };

  const handlePriceSliderChange = (d: number | number[]) => {
    if (Array.isArray(d)) {
      SetPriceRange(d);
    } else {
      SetPriceRange([0, 2000]);
    }
  };

  const disabledDatePast = (current: moment.Moment) => {
    const currentDate = DateTime.fromISO(current.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
      })
      .toUnixInteger();

    const currentWithTimezone = DateTime.fromISO(
      DateTime.utc()
        .set({
          hour: 0,
          minute: 0,
          second: 0,
        })
        .toISO({ includeOffset: false })
    ).toUnixInteger();

    return currentDate.valueOf() < currentWithTimezone.valueOf();
  };

  const handleDateChange = (d: any) => {
    SetDateRange(d);
  };

  const handleFilter = () => {
    if (priceRange) {
      apiParam.proceeds = priceRange;
    } else {
      delete apiParam.proceeds;
    }

    if (dateRange) {
      const startDate = dateRange[0].format("YYYY-MM-DD");
      const endDate = dateRange[1].format("YYYY-MM-DD");
      apiParam.dateRange = [startDate, endDate];
    } else {
      delete apiParam.dateRange;
    }

    if (selectedTags) {
      const tmp: any = [];
      selectedTags?.map((tag: any) => {
        tmp.push(tag?.value);
      });
      apiParam.tags = tmp?.join(",");
    } else {
      delete apiParam.tags;
    }

    apiParam.skip = 0;
    dispatch(fetchOpenPosting(apiParam));
  };

  useEffect(() => {
    if (selectPosting) {
      setIsPostingModalOpen(true);
    }
  }, [selectPosting]);

  useEffect(() => {
    if (selectedArtistData?.objectId) {
      setArtistId(selectedArtistData.objectId);
    }
  }, [selectedArtistData]);

  useEffect(() => {
    if (!isPostModalOpen) {
      setSelectPosting(null);
    }
  }, [isPostModalOpen]);

  useEffect(() => {
    if (tagsData && tagsData.length > 0) {
      const tmp = tagsData.map((tag) => ({
        value: tag.label,
        label: tag.label,
      }));
      setTagOptions(tmp);
    } else {
      setTagOptions([]);
    }
  }, [tagsData]);

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["open_posting"]} fallback={<NoPermissionsComponent />}>
        <BrowseOpenPostingsScene
          {...{
            openPostingData,
            handleLoadMore,
            isLoading,
            setSelectPosting,
            handlePriceSliderChange,
            priceRange,
            disabledDatePast,
            dateRange,
            handleDateChange,
            selectedTags,
            handleTag,
            tagOptions,
            handleFilter,
          }}
        />
      </Show>

      {isPostModalOpen && selectPosting?.objectId && artistId && (
        <CalendarPostModal
          {...{
            isOpen: isPostModalOpen,
            setIsPostingModalOpen,
            id: selectPosting.objectId,
            artistId: artistId,
            handleClose: (e) => {
              setSelectPosting(null);
              apiParam.skip = 0;
              dispatch(fetchOpenPosting(apiParam));
            },
          }}
        />
      )}
    </>
  );
};

BrowseOpenPostingsContainer.Layout = AdminLayoutComponent;

export default BrowseOpenPostingsContainer;
