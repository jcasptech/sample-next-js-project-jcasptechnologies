import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import {
  OpenPostingDataResponse,
  OpenPostingObject,
} from "@redux/slices/openPosting";
import { DatePicker } from "antd";
import moment from "moment";
import Link from "next/link";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";
import Select from "react-select";
import BrowseOpenPostingsStyles from "./browseOpenPostings.module.scss";
import Posting from "./posting";

export interface BrowseOpenPostingsSceneProps {
  openPostingData: OpenPostingDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  setSelectPosting: (d: any) => void;
  handlePriceSliderChange: (d: number | number[]) => void;
  priceRange: number[];
  disabledDatePast: (d: any) => boolean;
  dateRange: [moment.Moment, moment.Moment] | null;
  handleDateChange: (d: any) => void;
  selectedTags: any;
  tagOptions: any[];
  handleTag: (d: any) => void;
  handleFilter: (d: any) => void;
}

const BrowseOpenPostingsScene = (props: BrowseOpenPostingsSceneProps) => {
  const {
    openPostingData,
    handleLoadMore,
    isLoading,
    priceRange,
    dateRange,
    selectedTags,
    tagOptions,
    disabledDatePast,
    setSelectPosting,
    handlePriceSliderChange,
    handleDateChange,
    handleTag,
    handleFilter,
  } = props;

  return (
    <>
      <div className="browse-open-posting-page h-100vh">
        <div className="panel-inner-content">
          <div className={BrowseOpenPostingsStyles.container}>
            <div className="row ms-0 mb-4 mr-0">
              <div className="col-md-3 col-sm-12">
                <div className="price-range-slider">
                  <div className="range-labels">
                    <label>Proceeds:</label>
                    <span>{`$${priceRange[0]}`}</span>
                    <span>-</span>
                    <span>{`$${priceRange[1]}${
                      priceRange[1] === 2000 ? "+" : ""
                    }`}</span>
                  </div>
                  <Slider
                    range
                    startPoint={0}
                    max={2000}
                    step={100}
                    value={priceRange}
                    onChange={handlePriceSliderChange}
                  />
                </div>
              </div>
              <div className="col-md-4 col-sm-12">
                <div className="date-filters">
                  <label>Date Range</label>
                  <DatePicker.RangePicker
                    className="range-datepicker"
                    format={"YYYY-MM-DD"}
                    value={dateRange}
                    disabledDate={(currentDate) => {
                      return disabledDatePast(currentDate);
                    }}
                    onChange={handleDateChange}
                  />
                </div>
              </div>
              <div className="col-md-4 col-sm-12">
                <div className="tag-filters">
                  <label>Elements</label>
                  <Select
                    defaultValue={selectedTags || ""}
                    isMulti
                    className={`basic_multi_select selectField`}
                    classNamePrefix="select"
                    options={tagOptions}
                    value={selectedTags}
                    onChange={handleTag}
                    instanceId="artistElement"
                  />
                </div>
              </div>
              <div className="col-md-1 col-sm-12">
                <div className="actions">
                  <Button
                    className=""
                    htmlType="button"
                    type="primary"
                    onClick={handleFilter}
                  >
                    Filter
                  </Button>
                </div>
              </div>
            </div>
            <div className="postings row">
              {openPostingData &&
                openPostingData?.list?.map(
                  (posting: OpenPostingObject, index: number) => (
                    <Posting
                      key={index}
                      {...{
                        posting,
                        setSelectPosting,
                      }}
                    />
                  )
                )}

              {openPostingData?.list && openPostingData?.list?.length <= 0 && (
                <EmptyMessage description="No postings found." />
              )}

              {isLoading && <DefaultLoader />}
              {openPostingData.hasMany && (
                <div className="col-12 load_more">
                  <Link
                    href={"#"}
                    className={`hvr-float-shadow ${
                      isLoading ? "disabled" : ""
                    }`}
                    onClick={(e) => {
                      e.preventDefault();
                      handleLoadMore({
                        loadMore: true,
                      });
                    }}
                  >
                    Load more
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BrowseOpenPostingsScene;
