import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import PrivacyPolicyScene from "./privacyPolicyScene";

const PrivacyPolicyContainer = () => {
  return (
    <>
      <SEO
        {...{
          pageName: "/privacy-policy",
        }}
      />
      <PrivacyPolicyScene />
    </>
  );
};

PrivacyPolicyContainer.Layout = MainLayoutComponent;
export default PrivacyPolicyContainer;
