import { Row } from "antd";
import Link from "next/link";
import styles from "./privacyPolicy.module.scss";

const PrivacyPolicyScene = () => {
  return (
    <>
      <div className="lkPage">
        <div className="lkPageHeader">
          <div className="flex justify-between">
            <h1 className="text-2xl fw-700">
              Website and Mobile Application Privacy Policy
            </h1>
          </div>
        </div>
        <Row className="d-flex" align="middle">
          <div className="lkFormCard bg-white">
            <div className="lkPageContent">
              <div className="lkCard">
                <form>
                  <div>
                    <div className="lkCardContent">
                      <div className={styles.privacyPolicy}>
                        <p>
                          <strong>EFFECTIVE DATE:</strong> 09/2018
                        </p>
                        <br />
                        <p>
                          JCasp (“JCasp,” “we” or “us”) values your privacy.
                          In this privacy policy, we describe how we collect,
                          use, and disclose information that we obtain about
                          visitors to our website www.jcasptechnologies.com (the “Site”),
                          our mobile application (the “App”), and the services
                          available through our Site and App (collectively, the
                          “Services”). Please note that certain Services are
                          available only on our Site, e.g., artists may register
                          through our Site, but not through our App, whereas
                          certain Services only are available through our App,
                          e.g., interested consumers may book artists.
                        </p>
                        <br />
                        <p>
                          By visiting our Site or App, or using any of our
                          Services, you agree that your personal information
                          will be handled as described in this Policy. Your use
                          of our Site, App, or Services, and any dispute over
                          privacy, is subject to this Policy and our Terms of
                          Use, including its applicable limitations on damages
                          and the resolution of disputes. Our Terms of Use are
                          incorporated by reference into this Policy.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          Information We Collect About You
                        </h1>
                        <p>
                          We may collect information about you directly from you
                          and from third parties, as well as automatically
                          through your use of our Site, App or Services.
                        </p>
                        <p>
                          <strong>
                            <i> Information We Collect Directly From You.</i>
                          </strong>{" "}
                          The information that we collect from you depends on
                          whether you are an artist or a consumer seeking to
                          book an artist, as well if you are on our Site or our
                          App, and your interaction with our Services, and may
                          include the following:
                        </p>
                        <ul>
                          <li>
                            <p>
                              <u>Artists</u>. You may register your band through
                              our Site. To do so, we will collect: your name,
                              picture, email address, phone number, home zip
                              code, artist or band name. We will also collect
                              any information that you voluntarily provide to
                              us, such as audio and video files, a biography. We
                              use a third party to store your financial
                              information, and do not maintain it in our
                              systems.
                            </p>
                          </li>
                          <li>
                            <p>
                              <u>Consumers</u>. Currently, you may learn about
                              and message a registered artist only through our
                              App. To do so, we will require you to register to
                              use our App either directly with us (name and
                              email address) or through Facebook Connect (see
                              below re logging in through Facebook Connect). We
                              will request your gender, home location, and
                              credit or debit card information. You also have
                              the option of telling us about your favorite
                              artists and rating artists you have viewed.
                            </p>
                          </li>
                        </ul>
                        <p>
                          <strong>
                            <i>
                              Information We Collect From Social Networking
                              Sites.
                            </i>
                          </strong>{" "}
                          If you choose to log in through Facebook Connect, we
                          will collect information available through your public
                          Facebook profile, including profile picture, friends,
                          interests and likes, home location, and gender. We
                          store the information that we receive from Facebook
                          with other information that we collect from you or
                          receive about you.
                        </p>
                        <p>
                          Facebook controls the information it collects from
                          you. For information about what they may use and
                          disclose your information, including any information
                          you make public, please consult its privacy policy. We
                          have no control over how any third party site uses or
                          discloses the personal information it collects about
                          you.
                        </p>
                        <p>
                          <strong>
                            <i> Information We Collect Automatically.</i>
                          </strong>{" "}
                          We may automatically collect information, such as the
                          following, about your use of our Services (on our Site
                          and in our App) through cookies and other technologies
                          (including technologies designed for mobile apps):
                          your domain name; your browser type; your precise
                          location (geolocation), operating system type; device
                          name and model; pages or screens you view; links you
                          click; your IP address; the length of time you visit
                          or use our Services; and the referring URL, or the
                          webpage that led you to our Site.
                        </p>
                        <p>
                          We may combine information collected automatically
                          with other information that we have collected about
                          you.
                        </p>
                      </div>
                      {/* <div className="border-b border-b-grey-200 my-3"></div> */}
                      <div id="termsAndTermination">
                        <h1 className="text-xl font-semibold">
                          How We Use Your Information
                        </h1>
                        <p>
                          Our primary goals in collecting your Personal
                          information or Usage information are to enhance your
                          experience using the Services and to help support
                          local artists.
                        </p>
                        <p>
                          We use your information, including your personal
                          information, for the following purposes:
                        </p>
                        <ul>
                          <li>
                            To provide our Services to you (e.g., for our
                            Artists, to display information about your band,
                            and, for users, to tell you about upcoming events,
                            profile Artists; to communicate with you about your
                            use of our Services; to respond to your inquiries;
                            and for other customer service purposes.
                          </li>
                          <li>
                            To better understand how users access and use our
                            Service, both on an aggregated and individualized
                            basis; to respond to user desires and preferences;
                            and for other research and analytical purposes.
                          </li>
                          <li>
                            To tailor the content and information that we may
                            send or display to you; to offer personalized help
                            and instructions; and to otherwise personalize your
                            experiences while using our Site, App or Services.
                          </li>
                          <li>
                            To provide you with news and newsletters, special
                            offers, and promotions; to contact you (including
                            via email) about products or information we think
                            may interest you; and for other marketing,
                            advertising and promotional purposes.
                          </li>
                          <li>
                            To send push notifications to you. For example, we
                            may notify you of artists you may like that are
                            playing nearby, or of promotions that may be of
                            interest to you.
                          </li>
                          <li>To comply with applicable legal obligations.</li>
                        </ul>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="fees">
                        <h1 className="text-xl font-semibold">
                          How We Share Your Information
                        </h1>
                        <p>
                          We may share your information (which includes
                          information about artists, users, and other visitors
                          of our Site and our App, unless specifically limited
                          below), including personal information, as follows:
                        </p>
                        <ul>
                          <li>
                            <p>
                              <strong>
                                <i>Artists. </i>
                              </strong>{" "}
                              If you are an artist, any information that you
                              submit about your band (with the exception of
                              payment information) will be available on our Site
                              and App to other users of our Services, including
                              other Artists. Interested persons will be able to
                              view your profile, listen to music, and review any
                              other information that you choose to submit to us.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                <i>JCasp Users. </i>
                              </strong>{" "}
                              If a user submits information about an Artist,
                              such as a review or a comment, that information
                              will be available to all other users of our Site
                              and our App. When you submit a comment, your user
                              name will be posted along with your comment.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                <i>Service Providers. </i>
                              </strong>{" "}
                              We may disclose your information to vendors,
                              service providers, contractors, agents, or other
                              entities who perform functions on our behalf,
                              including entities that process payment
                              information on our behalf as well as assist us
                              with IT, marketing, and other functions.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                <i>Affiliates. </i>
                              </strong>{" "}
                              We may disclose your information to our affiliates
                              or subsidiaries for research, marketing and other
                              purposes consistent with this Policy.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                <i>Non-Affiliated Third Parties. </i>
                              </strong>{" "}
                              We may disclose your information with
                              non-affiliated third parties, who may use the
                              information to market or advertise to you products
                              of services that they think would be of interest
                              for you. Please see our special notice to
                              California users below.
                            </p>
                          </li>
                        </ul>
                        <p>
                          We also may share your information in the following
                          circumstances:
                        </p>
                        <ul>
                          <li>
                            <p>
                              <strong>
                                <i>Business Transfers. </i>
                              </strong>{" "}
                              If we are acquired by or merged with another
                              company, if substantially all of our assets are
                              transferred to another company or as part of a
                              bankruptcy proceeding, we may transfer your
                              information to another entity.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                <i>In Response to Legal Process. </i>
                              </strong>{" "}
                              We may disclose your information in order to
                              comply with the law, a judicial proceeding, court
                              order or other legal process, such as in response
                              to a court order or a subpoena.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                <i>To Protect Us and Others. </i>
                              </strong>{" "}
                              We also may disclose your information where we
                              believe it is necessary to investigate, prevent or
                              take action regarding illegal activities,
                              suspected fraud, situations involving potential
                              threats to the safety of any person, violations of
                              our Terms of Service or this Policy or as evidence
                              in litigation in which we are involved.
                            </p>
                          </li>
                          <li>
                            <p>
                              <strong>
                                <i>Aggregate and De-Identified Information. </i>
                              </strong>{" "}
                              We may share aggregate or de-identified
                              information about users with third parties for
                              marketing, advertising, research, or similar
                              purposes.
                            </p>
                          </li>
                        </ul>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="useApplication">
                        <h1 className="text-xl font-semibold">
                          Cookies and Other Tracking Mechanisms
                        </h1>
                        <p>
                          We and our service providers use cookies and other
                          tracking mechanisms to track information about your
                          use of our Site, App, or Services. We or our service
                          providers may combine this information with other
                          personal information we collect from you.
                        </p>
                        <p>
                          <strong>
                            <i>Do Not Track. </i>
                          </strong>{" "}
                          Our Site does not respond to Do Not Track signals.
                          However, we do not track your activities once you
                          leave our Site. You may, however, disable certain
                          tracking as discussed in this section (e.g., by
                          disabling cookies).
                        </p>
                        <p>
                          <strong>
                            <i>Cookies. </i>
                          </strong>{" "}
                          We or our service providers may use cookies to track
                          visitor activity on our Site. A cookie is a text file
                          that a website transfers to your computer’s hard drive
                          for record-keeping purposes. We or our service
                          providers may use cookies to track user activities on
                          our Site, such as the pages visited and time spent on
                          our Site. Most browsers allow users to refuse cookies.
                          The Help portion of the toolbar on most browsers will
                          tell you how to prevent your computer from accepting
                          new cookies, how to have the browser notify you when
                          you receive a new cookie, or how to disable cookies
                          altogether. Visitors to our Site who disable cookies
                          may not be able to browse certain areas of the Site.
                        </p>
                        <p>
                          <strong>
                            <i>Local Storage Objects. </i>
                          </strong>{" "}
                          We may use Flash Local Storage Objects (“Flash LSOs”)
                          to store your Site preferences and to personalize your
                          visit. Flash LSOs are different from browser cookies
                          because of the amount and type of data stored.
                          Typically, you cannot control, delete or disable the
                          acceptance of Flash LSOs through your web browser. For
                          more information on Flash LSOs, or to learn how to
                          manage your settings for Flash LSOs, go to the Adobe
                          Flash Player Help Page, choose “Global Storage
                          Settings Panel” and follow the instructions. To see
                          the Flash LSOs currently on your computer, choose
                          “Website Storage Settings Panel” and follow the
                          instructions to review and, if you choose, to delete
                          any specific Flash LSO.
                        </p>
                        <p>
                          <strong>
                            <i>
                              Clear GIFs, pixel tags and other technologies.{" "}
                            </i>
                          </strong>{" "}
                          Clear GIFs are tiny graphics with a unique identifier,
                          similar in function to cookies. In contrast to
                          cookies, which are stored on your computer’s hard
                          drive, clear GIFs are embedded invisibly on web pages.
                          We may use clear GIFs (also known as web beacons, web
                          bugs or pixel tags), in connection with our Site to,
                          among other things, track the activities of Site
                          visitors, help us manage content and compile
                          statistics about Site usage. We or our service
                          providers may also use clear GIFs in HTML emails to
                          our customers to help us track email response rates,
                          identify when our emails are viewed and track whether
                          our emails are forwarded.
                        </p>
                        <p>
                          <strong>
                            <i>Third-Party Analytics. </i>
                          </strong>{" "}
                          We or our service providers use automated devices and
                          applications to evaluate use of our Site and App. We
                          also may use other analytic means to evaluate our
                          Services. We or our service providers use these tools
                          to help us improve our Site, App, Services,
                          performance, and user experiences. These entities may
                          use cookies and other tracking technologies to perform
                          their services.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="restrictionsMaterials">
                        <h1 className="text-xl font-semibold">
                          Third-Party Ad Networks
                        </h1>
                        <p>
                          We may use third parties such as network advertisers
                          to display advertisements on our Site, to assist us in
                          displaying advertisements on third-party websites, and
                          to evaluate the success of our advertising campaigns.
                          Network advertisers are third parties that display
                          advertisements based on your visits to our Site as
                          well as other websites. This enables us and these
                          third parties to target advertisements by displaying
                          ads for products and services in which you might be
                          interested. Third-party ad network providers,
                          advertisers, sponsors and/or traffic measurement
                          services may use cookies, JavaScript, web beacons
                          (including clear GIFs), Flash LSOs and other
                          technologies to measure the effectiveness of their ads
                          and to personalize advertising content to you. These
                          third-party cookies and other technologies are
                          governed by each third party’s specific privacy
                          policy, not this Policy. We may provide these
                          third-party advertisers with information about your
                          usage of our Site and Services, as well as aggregate
                          or non-personally identifiable information about
                          visitors to our Site and users of our Services.
                        </p>
                        <p>
                          {" "}
                          You may opt out of many third-party ad networks,
                          including those operated by members of the Network
                          Advertising Initiative (“NAI”) and the Digital
                          Advertising Alliance (“DAA”). For more information
                          regarding this practice by NAI members and DAA
                          members, and your choices regarding having this
                          information used by these companies, including how to
                          opt-out of third-party ad networks operated by NAI and
                          DAA members, please visit their respective websites:
                          www.networkadvertising.org/optout_nonppii.asp (NAI)
                          and www.aboutads.info/choices (DAA).
                        </p>
                        <p>
                          Opting out of one or more NAI member or DAA member
                          networks (many of which will be the same) only means
                          that those members no longer will deliver targeted
                          content or ads to you. It does not mean you will no
                          longer receive any targeted content or ads on our Site
                          or other websites. You may continue to receive
                          advertisements, for example, based on the particular
                          website that you are viewing. Also, if your browsers
                          are configured to reject cookies when you visit this
                          opt-out page, or you subsequently erase your cookies,
                          use a different computer or change web browsers, your
                          NAI or DAA opt-out may no longer be effective.
                          Additional information is available on NAI’s and DAA’s
                          websites accessible by the above links.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="responsibleForServiceAccount">
                        <h1 className="text-xl font-semibold">
                          User-Generated Content
                        </h1>
                        <p>
                          We invite you to post content to our Service,
                          including your comments, video, pictures, and any
                          other information that you would like to be available
                          on our Services. If you post content to our Services,
                          all of the information that you post will be available
                          to all visitors to our Services. If you post your own
                          content on our Site, App or Services, your posting may
                          become public and we cannot prevent such information
                          from being used in a manner that may violate this
                          Policy, the law or your personal privacy.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="privacy">
                        <h1 className="text-xl font-semibold">
                          Third-Party Links
                        </h1>
                        <p>
                          Our Services may contain links to third-party
                          websites. Any access to and use of such linked
                          websites is not governed by this Policy, but instead
                          is governed by the privacy policies of those
                          third-party websites. We are not responsible for the
                          information practices of such third-party websites.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="warrantyDisclaimer">
                        <h1 className="text-xl font-semibold">
                          Security of My Personal Information
                        </h1>
                        <p>
                          We have implemented security measures designed to
                          protect the personal information we collect. Please be
                          aware that despite our best efforts, no data security
                          measures can guarantee 100% security. You should take
                          steps to protect against unauthorized access to your
                          password, mobile device and computer by, among other
                          things, signing off after using a shared computer,
                          choosing a robust password that nobody else knows or
                          can easily guess, and keeping your log-in and password
                          private. We are not responsible for any lost, stolen
                          or compromised passwords or for any activity on your
                          account via unauthorized password activity.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="a">
                        <h1 className="text-xl font-semibold">
                          Access to My Personal Information
                        </h1>
                        <p>
                          You may modify personal information that you have
                          submitted by logging into your account and updating
                          your profile information in your account settings on
                          our Site or in our App. Please note that copies of
                          information that you have updated, modified or deleted
                          may remain viewable in cached and archived pages of
                          the Site or App for a period of time.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="b">
                        <h1 className="text-xl font-semibold">
                          Special Information for California Consumers
                        </h1>
                        <p>
                          California residents may request a list of certain
                          third parties to which we have disclosed personally
                          identifiable information about you for their own
                          direct marketing purposes. You may make one request
                          per calendar year and your request is free of charge.
                          In your request, please attest to the fact that you
                          are a California resident and provide a current
                          California address for your response. You may request
                          this information in writing by contacting us at
                          contact@jcasptechnologies.com. Please allow up to thirty (30)
                          days for a response.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id=" c">
                        <h1 className="text-xl font-semibold">
                          Marketing Communications
                        </h1>
                        <p>
                          We may send periodic promotional or informational
                          emails to you. You may opt out of such communications
                          by following the opt-out instructions contained in the
                          email or by emailing us at contact@jcasptechnologies.com. Please
                          note that it may take up to 10 business days for us to
                          process opt-out requests. If you opt out of receiving
                          emails about recommendations or other information we
                          think may interest you, we may still send you emails
                          about your account or any services you have requested
                          or received from us.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="d">
                        <h1 className="text-xl font-semibold">
                          Children Under 13
                        </h1>
                        <p>
                          Our Services are not designed for children under 13,
                          and we do not knowingly collect information from
                          children under 13.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="linkedSites">
                        <h1 className="text-xl font-semibold">Contact Us</h1>
                        <p>
                          If you have questions about the privacy aspects of our
                          Services or would like to make a complaint, please
                          contact us at{" "}
                          <Link href="mailto:contact@jcasptechnologies.com">
                            contact@jcasptechnologies.com
                          </Link>
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="general">
                        <h1 className="text-xl font-semibold">
                          Changes to this Policy
                        </h1>
                        <p>
                          This Policy is current as of the Effective Date set
                          forth above. We may change this Policy from time to
                          time, so please be sure to check back periodically. We
                          will post any changes to this Policy on our Site. If
                          we make any changes to this Policy that materially
                          affect our practices with regard to the personal
                          information we have previously collected from you, we
                          will endeavor to provide you with notice in advance of
                          such change by highlighting the change on our Site.
                        </p>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Row>
      </div>
    </>
  );
};

export default PrivacyPolicyScene;
