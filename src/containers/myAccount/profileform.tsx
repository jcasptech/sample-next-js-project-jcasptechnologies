import { Button, DefaultSkeleton } from "@components/theme";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { postFileAPI } from "@redux/services/artist.api";
import { checkValidAPI } from "@redux/services/general.api";
import { default as NextImage } from "next/image";
import { useState } from "react";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import MyAccountStyles from "./myAccount.module.scss";

export interface ArtistProfileFormProps {
  handleSubmit: (d: any) => () => void;
  onSubmit: (d: any) => void;
  formState: any;
  register: any;
  setValue: any;
  loginUserState: any;
  selectedImage: string;
  setSelectedImage: (d: any) => void;
  isUpdateLoading: boolean;
  setIsProfileImgUpdated: (d: any) => void;
  setIsCheckedUsername: (d: any) => void;
  setIsCheckedEmail: (d: any) => void;
  ischeckedUsername: boolean | undefined;
  ischeckedEmail: boolean | undefined;
  control: any;
  handleCreateArtist: (d: any) => void;
}
const ArtistProfileForm = (props: ArtistProfileFormProps) => {
  const {
    handleSubmit,
    onSubmit,
    register,
    formState,
    setValue,
    loginUserState,
    selectedImage,
    setSelectedImage,
    isUpdateLoading,
    setIsProfileImgUpdated,
    setIsCheckedUsername,
    setIsCheckedEmail,
    ischeckedUsername,
    ischeckedEmail,
    control,
    handleCreateArtist,
  } = props;
  const [imageIsLoading, setImageIsLoading] = useState(false);
  const [iAmTheArtist, setIAmTheArtist] = useState(
    loginUserState?.data?.user?.iAmTheArtist || false
  );

  const onFileSelected = (e: any) => {
    let image = e.target.files[0];
    const allowedTypes = ["image/jpeg", "image/png", "image/jpg"];
    let isValidDimensions = false;
    const reader = new FileReader();
    reader.onload = function (readerEvent: any) {
      const img: any = new Image();

      img.onload = async function () {
        if (this.width >= 750 && this.height >= 586) {
          const formData = new FormData();
          formData.append("file", image);
          try {
            let response = await postFileAPI(formData);
            setSelectedImage(response?.url);
            setIsProfileImgUpdated("true");
            setImageIsLoading(false);
          } catch (error: any) {
            setImageIsLoading(false);
            showToast(error.message, "warning");
          }
        } else {
          setImageIsLoading(false);
          showToast(SUCCESS_MESSAGES.profileImage, "warning");
        }
      };
      img.src = readerEvent.target.result;
    };

    if (image && allowedTypes.includes(image.type)) {
      reader.readAsDataURL(image);
      setImageIsLoading(true);
    } else {
      showToast(SUCCESS_MESSAGES.inputFileType, "warning");
    }
  };

  const handleSelect = (e: any) => {
    e.preventDefault();
    let fileDialog = $('<input type="file">');
    fileDialog.click();
    fileDialog.on("change", onFileSelected);
  };

  const handleConfirmArtist = () => {
    const tmp = !iAmTheArtist;
    setValue("iAmTheArtist", tmp);
    setIAmTheArtist(tmp);
  };

  const { apiParam } = useList({
    queryParams: {},
  });

  const handleUsernameChange = (value: any, userName: string) => {
    if (value === userName) {
      return false;
    } else if (value) {
      apiParam.type = "userName";
      apiParam.content = value;
      getUsernameValidate();
    }
  };

  const getUsernameValidate = async () => {
    if (apiParam)
      try {
        let res = await checkValidAPI(apiParam);
        if (res.available) {
          setIsCheckedUsername(true);
        } else {
          setIsCheckedUsername(false);
        }
      } catch (error) {
        console.log(error);
      }
  };

  const handleEmailChange = (value: any, email: string) => {
    if (value === email) {
      return false;
    } else if (value) {
      apiParam.type = "email";
      apiParam.content = value;
      getValidate();
    }
  };

  const getValidate = async () => {
    if (apiParam)
      try {
        let res = await checkValidAPI(apiParam);
        if (res.available) {
          setIsCheckedEmail(true);
        } else {
          setIsCheckedEmail(false);
        }
      } catch (error) {
        console.log(error);
      }
  };

  const selectOptions = [
    { value: true, label: "YES" },
    { value: false, label: "NO" },
  ];

  return (
    <>
      {imageIsLoading && <DefaultSkeleton />}
      <form
        className={`${MyAccountStyles.my_account_form} panel-form edit-venue`}
        onSubmit={handleSubmit(onSubmit)}
        method="POST"
      >
        <fieldset>
          <div className="form-box new-fm-box">
            <div className="row align-center field-row">
              <div className="col-12 col-sm-4 pfp">
                <label>Profile Image</label> <br />
                {/* <span>Minimum Dimensions : 750px X 586px </span> */}
                <span>
                  This is your account profile image, not your artist image
                </span>
              </div>
              <div className="col-12 col-sm-8 profile_pic">
                <div className="profile-pic-edit">
                  <NextImage
                    src={selectedImage || "/images/artist-placeholder.png"}
                    alt="fname"
                    width={128}
                    height={128}
                  />
                  <span className="edit-pp">
                    <a
                      href="#"
                      onClick={handleSelect}
                      title="Upload profile photo"
                      id="btnImportData"
                    >
                      <NextImage
                        src="/images/edit-ic.svg"
                        alt="Edit"
                        width={36}
                        height={128}
                      />
                    </a>
                  </span>
                </div>
              </div>
            </div>
            <div className="row align-center field-row">
              <div className="col-12">
                <label>
                  <strong>Account Holder</strong>
                </label>
              </div>
            </div>
            <div className="row align-center field-row">
              <div className="col-12 col-sm-4">
                <label>First Name</label>
              </div>
              <div className="col-12 col-sm-8">
                <div className="input-group">
                  <InputField
                    {...{
                      register,
                      formState,
                      id: "firstName",
                      className: `input`,
                      placeholder: "First name",
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="row align-center field-row">
              <div className="col-12 col-sm-4">
                <label>Last Name</label>
              </div>
              <div className="col-12 col-sm-8">
                <div className="input-group">
                  <InputField
                    {...{
                      register,
                      formState,
                      id: "lastName",
                      className: `input`,
                      placeholder: "Last name",
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="row align-center field-row">
              <div className="col-12 col-sm-4">
                <label>Username</label>
              </div>
              <div className="col-12 col-sm-8">
                <div className="input-group">
                  <InputField
                    {...{
                      register,
                      formState,
                      id: "userName",
                      className: `input`,
                      placeholder: "Username",
                    }}
                    onChange={(e: any) =>
                      handleUsernameChange(
                        e.target.value,
                        loginUserState?.data?.user?.username
                      )
                    }
                  />
                  {ischeckedUsername ? (
                    <p className="text-danger">Username already exists</p>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
            <div className="row align-center field-row">
              <div className="col-12 col-sm-4">
                <label>Phone Number</label>
              </div>
              <div className="col-12 col-sm-8">
                <div className="input-group">
                  <InputField
                    {...{
                      register,
                      formState,
                      id: "phone",
                      className: `input`,
                      placeholder: "Artist Phone Number",
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="row align-center field-row">
              <div className="col-12 col-sm-4">
                <label>Email</label>
              </div>
              <div className="col-12 col-sm-8">
                <div className="input-group">
                  <InputField
                    {...{
                      register,
                      formState,
                      id: "email",
                      className: `input`,
                      placeholder: "Artist Email Id",
                    }}
                    onChange={(e: any) =>
                      handleEmailChange(
                        e.target.value,
                        loginUserState?.data?.user?.email
                      )
                    }
                  />
                  {ischeckedEmail ? (
                    <p className="text-danger">Email already exists</p>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
            {loginUserState?.data?.userType === LOGIN_USER_TYPE.MANAGER && (
              <div className="row align-center field-row">
                <div className="col-sm-4">
                  {/* <div className="input-group">
                    <SelectField
                      {...{
                        register,
                        formState,
                        name: "iAmTheArtist",
                        className: "inputField basic_multi_select ",
                        id: "iAmTheArtist",
                        placeholder: "Please Select Role",
                        control,
                        defaultValue: { value: false, label: "NO" },
                      }}
                      options={selectOptions}
                    />
                  </div> */}
                </div>
                <div className="col-sm-8">
                  <label className="conArtist" onClick={handleConfirmArtist}>
                    {iAmTheArtist && (
                      <NextImage
                        src="/images/general/checked-checkbox.png"
                        alt=""
                        className="h-auto"
                        width={24}
                        height={24}
                      />
                    )}

                    {!iAmTheArtist && (
                      <NextImage
                        src="/images/general/un-checked-checkbox.png"
                        alt=""
                        className="h-auto"
                        width={24}
                        height={24}
                      />
                    )}

                    <span className={`ms-2`}>
                      I confirm that I am either the artist(s) or an authorized
                      representative of the artist(s) managed under this
                      account.
                    </span>
                  </label>
                </div>
              </div>
            )}

            <div className="row align-center field-row">
              <div className="col-12 col-md-12 col-lg-12 text-end">
                <Button
                  htmlType="submit"
                  type="primary"
                  loading={isUpdateLoading}
                  disabled={isUpdateLoading}
                >
                  Save Profile
                </Button>
              </div>
            </div>
          </div>
        </fieldset>
      </form>
    </>
  );
};
export default ArtistProfileForm;
