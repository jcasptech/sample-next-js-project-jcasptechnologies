import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import {
  convertToArtistAPI,
  facebookLinkAPI,
  postUpdateUserAPI,
  whoAmI,
} from "@redux/services/auth.api";
import {
  FacebookLinkPayload,
  LoginUserState,
  loginUserSuccess,
} from "@redux/slices/auth";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  MyAccountPageFormInputs,
  MyAccountPageFormSchema,
} from "src/schemas/myAccountPageFormSchema";
import MyAccountScane from "./myAccountScene";

export interface MyAccountContainerProps {}

const MyAccountContainer = (props: MyAccountContainerProps) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const [selectedImage, setSelectedImage] = useState("");
  const [isUpdateLoading, setIsUpdateLoading] = useState(false);

  const [ischeckedEmail, setIsCheckedEmail] = useState(false);
  const [ischeckedUsername, setIsCheckedUsername] = useState(false);

  const [isProfileImgUpdated, setIsProfileImgUpdated] = useState("false");

  const {
    register,
    handleSubmit,
    formState,
    setFocus,
    control,
    setValue,
    reset,
  } = useForm<MyAccountPageFormInputs>({
    resolver: yupResolver(MyAccountPageFormSchema),
  });

  const handleFacebookLink = async (data: FacebookLinkPayload) => {
    try {
      const res = await facebookLinkAPI(data);
    } catch (error) {
      console.log("not Response");
    }
  };

  useEffect(() => {
    if (loginUserState.data.user) {
      setValue("firstName", loginUserState?.data?.user?.firstname || "");
      setValue("lastName", loginUserState?.data?.user?.lastname || "");
      setValue("userName", loginUserState?.data?.user?.username || "");
      setValue("phone", loginUserState?.data?.user?.phone || "");
      setValue(
        "iAmTheArtist",
        loginUserState?.data?.user?.iAmTheArtist || false
      );
      setValue("email", loginUserState?.data?.user?.email || "");
      setSelectedImage(loginUserState?.data?.user?.iconImage?.url || "");
    }
  }, [loginUserState]);

  const onSubmit = async (data: any) => {
    if (ischeckedUsername === false && ischeckedEmail === false) {
      setIsUpdateLoading(true);
      try {
        let res = await postUpdateUserAPI({
          profileImage: selectedImage,
          firstName: data.firstName,
          lastName: data.lastName,
          userName: data.userName,
          phone: data.phone,
          email: data.email,
          iAmTheArtist: data.iAmTheArtist === "true" ? true : false,
          isProfileImgUpdated: isProfileImgUpdated,
        });
        setIsUpdateLoading(false);
        const auth = await whoAmI();
        dispatch(loginUserSuccess(auth));
        showToast(SUCCESS_MESSAGES.userUpdateSuccess, "success");
      } catch (error) {
        setIsUpdateLoading(false);
        showToast(SUCCESS_MESSAGES.profileUpdateError, "warning");
      }
    }
  };

  const handleCreateArtist = async () => {
    try {
      await convertToArtistAPI();
      showToast(SUCCESS_MESSAGES.userUpdateSuccess, "success");
      const auth = await whoAmI();
      dispatch(loginUserSuccess(auth));
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Show when={["account_settings"]} fallback={<NoPermissionsComponent />}>
      <MyAccountScane
        {...{
          formState,
          register,
          setValue,
          handleSubmit,
          onSubmit,
          handleFacebookLink,
          loginUserState,
          selectedImage,
          setSelectedImage,
          isUpdateLoading,
          setIsProfileImgUpdated,
          setIsCheckedUsername,
          setIsCheckedEmail,
          ischeckedUsername,
          ischeckedEmail,
          control,
          handleCreateArtist,
        }}
      />
    </Show>
  );
};

MyAccountContainer.Layout = AdminLayoutComponent;
export default MyAccountContainer;
