import { FacebookLinkPayload, FacebookLoginPayload } from "@redux/slices/auth";
import { useRouter } from "next/router";
import ArtistProfileForm from "./profileform";
import FacebookLogin from "@greatsumini/react-facebook-login";
import { FACEBOOK_APP_ID, LOGIN_USER_TYPE } from "src/libs/constants";
import { Popconfirm } from "antd";
import ChangePassword from "@components/changePassword";
import { Button } from "@components/theme";
export interface MyAccountScaneProps {
  handleSubmit: (d: any) => () => void;
  onSubmit: (d: any) => void;
  formState: any;
  register: any;
  setValue: any;
  handleFacebookLink: (d: FacebookLinkPayload) => void;
  loginUserState: any;
  selectedImage: string;
  setSelectedImage: (d: any) => void;
  isUpdateLoading: boolean;
  setIsProfileImgUpdated: (d: any) => void;
  setIsCheckedUsername: (d: any) => void;
  setIsCheckedEmail: (d: any) => void;
  ischeckedUsername: boolean | undefined;
  ischeckedEmail: boolean | undefined;
  control: any;
  handleCreateArtist: (d: any) => void;
}

const MyAccountScane = (props: MyAccountScaneProps) => {
  const {
    handleSubmit,
    onSubmit,
    register,
    formState,
    setValue,
    handleFacebookLink,
    selectedImage,
    setSelectedImage,
    loginUserState,
    isUpdateLoading,
    setIsProfileImgUpdated,
    setIsCheckedUsername,
    setIsCheckedEmail,
    ischeckedUsername,
    ischeckedEmail,
    control,
    handleCreateArtist,
  } = props;

  return (
    <>
      <div className="panel-inner-content">
        <div className="row m0 final-ma">
          <div className="multi_step_form float-left w-100 p-0">
            <div className="row m0" id="msform">
              <div className="col-12 p-0">
                <div className="float-left w-100">
                  <div className="row m0">
                    <div className="col-12 col-lg-8 profile-form">
                      <ArtistProfileForm
                        {...{
                          onSubmit,
                          handleSubmit,
                          register,
                          setValue,
                          formState,
                          loginUserState,
                          selectedImage,
                          setSelectedImage,
                          isUpdateLoading,
                          setIsProfileImgUpdated,
                          setIsCheckedUsername,
                          setIsCheckedEmail,
                          ischeckedUsername,
                          ischeckedEmail,
                          control,
                          handleCreateArtist,
                        }}
                      />
                    </div>

                    <div className="col-12  col-lg-4  ">
                      {loginUserState?.data?.userType ===
                        LOGIN_USER_TYPE.USER && (
                        <div className="link_account w-100 new-fm-box float-left p-3">
                          <h2>Intended Artist Registration</h2>
                          <p>
                            In reference to my previous action, I had intended
                            to sign up as an artist.
                          </p>
                          <div className="vcenter">
                            {loginUserState?.data?.userType ===
                              LOGIN_USER_TYPE.USER && (
                              <Popconfirm
                                title="Are you sure you want sign up as an Artist?"
                                okText="Yes"
                                cancelText="No"
                                onConfirm={handleCreateArtist}
                              >
                                <Button htmlType="button" type="primary">
                                  I meant to sign up as an Artist
                                </Button>
                              </Popconfirm>
                            )}
                          </div>
                        </div>
                      )}
                      <div className="w-100 new-fm-box float-left p-3">
                        <h2>Change Password</h2>
                        <ChangePassword />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MyAccountScane;
