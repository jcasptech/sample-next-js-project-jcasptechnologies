import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { requestFeatureAPI } from "@redux/services/auth.api";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";

import {
  RequestFeatureFormValidateSchema,
  RequestFeatureInputs,
} from "src/schemas/requestFeatureSchema";

import RequestFeatureScene from "./requestFeatureScene";

const RequestFeatureContainer = () => {
  const { register, handleSubmit, formState, control, setFocus, reset } =
    useForm<RequestFeatureInputs>({
      resolver: yupResolver(RequestFeatureFormValidateSchema),
    });

  const [isLoading, setIsLoading] = useState(false);
  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      await requestFeatureAPI(data);
      showToast(SUCCESS_MESSAGES.requestFeature, "success");
      reset();
      setIsLoading(false);
      // router.push(`/login`);
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  return (
    <RequestFeatureScene
      {...{
        handleSubmit,
        onSubmit,
        register,
        formState,
        isLoading,
        control,
      }}
    />
  );
};

RequestFeatureContainer.Layout = AdminLayoutComponent;
export default RequestFeatureContainer;
