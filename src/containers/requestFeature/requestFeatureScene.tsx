import {
  InputField,
  SelectField,
  TextAreaField,
  TextAreaHTMLField,
} from "@components/theme/form/formFieldsComponent";
import requesFeatureStyle from "./requestFeature.module.scss";
import Spinner from "@components/theme/spinner";

export interface RequestFeatureSceneProps {
  handleSubmit: (d: any) => () => void;
  onSubmit: (d: any) => void;
  formState: any;
  register: any;
  control: any;
  isLoading: boolean;
}
const RequestFeatureScene = (props: RequestFeatureSceneProps) => {
  const { handleSubmit, onSubmit, register, control, formState, isLoading } =
    props;

  const selectOptions = [
    { value: "Artist", label: "Artist" },
    { value: "Venue", label: "Venue" },
    { value: "Music Fan", label: "Music Fan" },
  ];

  return (
    <div className="message-page h-100vh">
      <div className="panel-inner-content">
        <form
          className={`panel-form ${requesFeatureStyle.main_form}`}
          onSubmit={handleSubmit(onSubmit)}
          method="POST"
        >
          <fieldset>
            <div className=" h-100vh">
              <div className="col align-center field-row">
                <div className="col-12 col-sm-4">
                  <label className={requesFeatureStyle.label}>
                    Email Address
                  </label>
                </div>
                <div className="col-12 ">
                  <div className="">
                    <InputField
                      {...{
                        register,
                        formState,
                        id: "email",
                        className: `inputField`,
                        placeholder: "Enter your email address",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="col align-center field-row">
                <div className="col-12 col-sm-4">
                  <label className={requesFeatureStyle.label}>Full Name</label>
                </div>
                <div className="col-12">
                  <div className="">
                    <InputField
                      {...{
                        register,
                        formState,
                        id: "name",
                        className: `inputField`,
                        placeholder: "Enter Your name",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="col align-center field-row">
                <div className="col-12 col-sm-4">
                  <label className={requesFeatureStyle.label}>Select</label>
                </div>
                <div className="col-12 ">
                  <div className={`${requesFeatureStyle.select_field}`}>
                    <SelectField
                      {...{
                        register,
                        formState,
                        name: "userType",
                        className: "inputField basic_multi_select",
                        id: "userType",
                        placeholder: "Please Select Type",
                        control,
                      }}
                      options={selectOptions}
                    />
                  </div>
                </div>
              </div>
              <div className="col align-center field-row">
                <div className="col-12 col-sm-4">
                  <label className={requesFeatureStyle.label}>Message</label>
                </div>
                <div className="col-12 ">
                  <TextAreaField
                    {...{
                      register,
                      formState,
                      id: "message",
                      className: `${requesFeatureStyle.textAreaField}`,
                      placeholder: "Enter your Message",
                    }}
                  />
                </div>
              </div>

              <div className="row align-center field-row">
                <div className="col-12 col-sm-4"></div>
                <div className="col-12 ">
                  <button
                    type="submit"
                    className="next action-button1 link-button"
                  >
                    {isLoading && <Spinner />} Send Messsage
                  </button>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  );
};
export default RequestFeatureScene;
