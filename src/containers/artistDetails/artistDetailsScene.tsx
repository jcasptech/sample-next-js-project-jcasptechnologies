import { BellIcon } from "@components/theme/icons/bellIcon";
import { ChatIcon } from "@components/theme/icons/chatIcon";
import { ShareIcon } from "@components/theme/icons/shareIcon";
import moment from "moment";
import { useState } from "react";
import ArtistPhotos from "./artistPhotosTab";

import ArtistShows from "@components/artist/artistShow";
import ArtistVideo from "@components/artist/artistVideo";
import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { ArtistEditIcon } from "@components/theme/icons/artistEditIcon";
import { ArtistObject, ArtistRatingDataResponse } from "@redux/slices/artists";
import { artistShowsResponse } from "@redux/slices/artistShow";
import "aos/dist/aos.css";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  capitalizeString,
  downloadFile,
  getFormatReview,
} from "src/libs/helpers";
import ArtistStyles from "./artistDetails.module.scss";
import ArtistOverview from "./artistDetailsOverview";
import ArtistDetailsTags from "./artistDetailsTags";
import Image from "next/image";

export interface artistDetailProps {
  artistDetail: ArtistObject;
  artistShows: artistShowsResponse;
  artistRatings: ArtistRatingDataResponse;
  handleLoadMore: (d: any) => void;
  setIsMesaageModalOpen: (data: any) => void;
  setIsRatingModalOpen: (d: any) => void;
  setIsReviewModalOpen: (d: any) => void;
  theme: "dark" | "light";
  follows: any[];
  isArtistShowType: "upcoming" | "all";
  setIsArtistShowType: (d: any) => void;
  handleArtistUnsubscribe: (data: any) => void;
  handleArtistSubscribe: (data: any) => void;
  currentTab: "overView" | "video" | "audio" | "photos";
  setCurrentTab: (d: any) => void;
  isMoreVideo: boolean;
  endIndexVideo: number;
  handleLoadMoreVideo: (d: any) => void;
  songPlayed: string;
  setSongPlayed: (d: any) => void;
  isArtistEditable: boolean;
  isLoading: boolean | undefined;
  setIsShareModalOpen: (data: any) => void;
  songsFile: { url: string; name: string };
}

const ArtistDetailsScene = (Props: artistDetailProps) => {
  const {
    artistDetail,
    isLoading,
    artistRatings,
    handleLoadMore,
    theme,
    follows,
    setIsMesaageModalOpen,
    setIsRatingModalOpen,
    setIsReviewModalOpen,
    artistShows,
    isArtistShowType,
    setIsArtistShowType,
    handleArtistSubscribe,
    handleArtistUnsubscribe,
    currentTab,
    setCurrentTab,
    isMoreVideo,
    endIndexVideo,
    handleLoadMoreVideo,
    songPlayed,
    setSongPlayed,
    isArtistEditable,
    setIsShareModalOpen,
    songsFile,
  } = Props;
  const route = useRouter();
  const [videoTab, setVideoTab] = useState(false);
  const handleVideo = () => {
    setVideoTab(true);
  };

  return (
    <>
      <div
        className={`${ArtistStyles.section_pad} ${
          theme == "dark" ? "dark" : "artist-white"
        }`}
      >
        <div className="container-fluid position-relative">
          <div className={`${ArtistStyles.profileHeaderImage}`}>
            <Image
              className="w-100"
              src="/images/artist-detail/artist-profile-background-image.png"
              width={1888}
              height={204}
              alt="Profile Image"
            />
          </div>
          <div
            className={`${ArtistStyles.profileHeader} ${
              theme == "dark"
                ? ArtistStyles.profileHeaderDark
                : ArtistStyles.profileHeaderwhite
            }`}
          >
            <div className={`general`}>
              <div className="profileImage">
                <div
                  className="profileI"
                  style={{
                    backgroundImage: `url(${
                      artistDetail?.iconImage?.url ||
                      "/images/artist-placeholder.png"
                    })`,
                    backgroundSize: "cover",
                  }}
                ></div>
              </div>
              <div className="profileSummary">
                <div className="city">{artistDetail?.city || ""}</div>
                <div className="name">
                  {capitalizeString(artistDetail?.name || "")}
                </div>
                <div className="join">
                  <span>Joined &nbsp;</span>
                  <span>
                    <Image
                      src="/images/about-us/jcal-w.png"
                      alt="C"
                      width={20}
                      height={22}
                    />
                    &nbsp;&nbsp;
                    {moment(artistDetail?.metadata?.createdAt).format(
                      "MMMM, Y"
                    )}
                  </span>
                </div>
                <div className="social">
                  <a
                    className="position-relative cursor-pointer"
                    onClick={(e) => {
                      e.preventDefault();
                      follows.indexOf(artistDetail.objectId) > -1
                        ? handleArtistUnsubscribe(artistDetail?.objectId)
                        : handleArtistSubscribe(artistDetail?.objectId);
                    }}
                  >
                    {artistDetail?.objectId &&
                    follows.indexOf(artistDetail.objectId) > -1 ? (
                      <span className="unfollow">
                        <BellIcon color={theme} />
                      </span>
                    ) : (
                      <BellIcon color={theme} />
                    )}
                  </a>

                  <a
                    className="cursor-pointer"
                    onClick={() => setIsMesaageModalOpen(true)}
                  >
                    <ChatIcon color={theme} />
                  </a>

                  <a className="" onClick={() => setIsShareModalOpen(true)}>
                    <ShareIcon color={theme} />
                  </a>

                  {isArtistEditable && (
                    <a href="#">
                      <ArtistEditIcon color={theme} />
                    </a>
                  )}
                </div>
              </div>
            </div>
            <div className={`summary`}>
              <div className="boxes">
                <div
                  className="stat-small"
                  // data-aos="fade-up"
                  // data-aos-delay="100"
                  // data-aos-duration="1200"
                >
                  <div className="statBx float-left w-100 ">
                    <span className="n float-left w-100">
                      {getFormatReview(artistDetail?.upcomingShows || 0)}
                    </span>
                    <span className="tt float-left w-100">Upcoming Shows</span>
                  </div>
                </div>
                <div
                  className="stat-small"
                  // data-aos="fade-down"
                  // data-aos-delay="100"
                  // data-aos-duration="1200"
                >
                  <div className="statBx float-left w-100">
                    <span className="n float-left w-100">
                      {getFormatReview(artistDetail?.completedEvents || 0)}
                    </span>
                    <span className="tt float-left w-100">Completed Shows</span>
                  </div>
                </div>
                <div
                  className="stat-small"
                  // data-aos="fade-up"
                  // data-aos-delay="100"
                  // data-aos-duration="1200"
                >
                  <div className="statBx float-left w-100">
                    <span className="n float-left w-100">
                      {getFormatReview(artistDetail?.fanCount || 0)}
                    </span>
                    <span className="tt float-left w-100">Subscribers</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className={` artistD   ${theme == "dark" ? "dark" : "artist-white"} ${
          ArtistStyles.artist_detail_page
        }`}
      >
        <div className="container-fluid">
          <div className="row">
            {/*------ Artist Shows ------ */}
            <div className="col-xs-12 col-sm-6 col-md-5 col-lg-3 ">
              <div className="sidebar float-left w-100">
                <div className="row m0 artist_sec">
                  <div className={`col-12 title p-0 borderGray`}>
                    <h5 className="capitalize">{isArtistShowType} Shows</h5>
                    <a
                      className={`${
                        theme == "dark"
                          ? "redLnk capitalize"
                          : "whiteLnk capitalize"
                      }`}
                      onClick={(e) => {
                        e.preventDefault();
                        setIsArtistShowType(
                          isArtistShowType === "upcoming" ? "all" : "upcoming"
                        );
                      }}
                    >
                      {isArtistShowType === "upcoming" ? "all" : "upcoming"}{" "}
                      Shows &#8594;
                    </a>
                  </div>
                  <div
                    className={`USholder float-left w-100 
                  ${
                    theme == "light"
                      ? ArtistStyles.scrollBar
                      : ArtistStyles.scrollBar_dark
                  }`}
                  >
                    <div className="row">
                      {artistShows.list &&
                        artistShows.list?.length > 0 &&
                        artistShows.list.map((show, index) => {
                          return (
                            <ArtistShows
                              key={index}
                              {...{
                                show,
                                theme,
                              }}
                            />
                          );
                        })}
                      {isLoading && <DefaultLoader />}
                      {artistShows?.list && artistShows?.list?.length <= 0 && (
                        <EmptyMessage description="No shows found." />
                      )}
                      {artistShows?.hasMany && (
                        <div className="col-12 load_more">
                          <Link
                            href={"#"}
                            className={`hvr-float-shadow ${
                              isLoading ? "disabled" : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();
                              handleLoadMore(e);
                            }}
                          >
                            Load more Show
                          </Link>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              {/*------ Artist Shows ------ */}
              {/* artist details Tags  */}
              <div className="only-for-desktop">
                <ArtistDetailsTags
                  {...{
                    artistDetail,
                    theme,
                    artistRatings,
                    setIsRatingModalOpen,
                    setIsReviewModalOpen,
                  }}
                />
              </div>

              {/* artist details Tags  */}
            </div>
            <div className="col-xs-12 col-sm-6 col-md-7 col-lg-9 ">
              <div className="Atabs float-left w-100 ">
                <div
                  className="profiletabs borderGray"
                  // data-aos="fade-down"
                  // data-aos-delay="100"
                  // data-aos-duration="1200"
                >
                  <div className="nav-wrapper position-relative mb-2">
                    {/* <ArtistNavTab /> */}
                    <ul
                      className="nav nav-pills nav-fill flex-md-row"
                      id="tabs-text"
                      role="tablist"
                    >
                      <li className="nav-item">
                        <a
                          className={`nav-link mb-sm-3 mb-md-0 redLnk ${
                            currentTab === "overView" ? "active" : ""
                          }`}
                          id="tabs-text-1-tab"
                          data-bs-toggle="tab"
                          href="#tabs-text-1"
                          role="tab"
                          aria-controls="tabs-text-1"
                          aria-selected={currentTab === "overView"}
                          onClick={(e) => setCurrentTab("overView")}
                          event-key="tab1"
                        >
                          Overview
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className={`nav-link mb-sm-3 mb-md-0 ${
                            currentTab === "video" ? "active" : ""
                          }`}
                          id="tabs-text-2-tab"
                          data-bs-toggle="tab"
                          href="#tabs-text-2"
                          role="tab"
                          aria-controls="tabs-text-2"
                          aria-selected={currentTab === "video"}
                          onClick={(e) => setCurrentTab("video")}
                        >
                          Video
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className={`nav-link mb-sm-3 mb-md-0 ${
                            currentTab === "photos" ? "active" : ""
                          }`}
                          id="tabs-text-4-tab"
                          data-bs-toggle="tab"
                          href="#tabs-text-4"
                          role="tab"
                          aria-controls="tabs-text-4"
                          aria-selected={currentTab === "photos"}
                          onClick={(e) => setCurrentTab("photos")}
                        >
                          Photos
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          className={`nav-link mb-sm-3 mb-md-0 ${
                            currentTab === "audio" ? "active" : ""
                          }`}
                          id="tabs-text-3-tab"
                          data-bs-toggle="tab"
                          href="#tabs-text-3"
                          role="tab"
                          aria-controls="tabs-text-3"
                          aria-selected={currentTab === "audio"}
                          onClick={(e) => setCurrentTab("audio")}
                        >
                          Songs List
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                {/* <!-- Tab Content --> */}
                <div className="load_profile">
                  <div className="border-0">
                    <div className="p-0">
                      <div className="tab-content" id="tabcontent1">
                        <div
                          className={`tab-pane fade ${
                            currentTab === "overView" ? "show active" : ""
                          }`}
                          id="tabs-text-1"
                          role="tabpanel"
                          aria-labelledby="tabs-text-1-tab"
                        >
                          <ArtistOverview
                            {...{
                              artistDetail,
                              theme,
                              setCurrentTab,
                              songPlayed,
                              setSongPlayed,
                              songsFile,
                            }}
                          />
                        </div>
                        <div
                          className={`tab-pane fade ${
                            currentTab === "video" ? "show active" : ""
                          }`}
                          id="tabs-text-2"
                          role="tabpanel"
                          aria-labelledby="tabs-text-2-tab"
                        >
                          <div
                            className="row m0 pt-1 artist_sec"
                            // data-aos="fade-up"
                            // data-aos-delay="100"
                            // data-aos-duration="1200"
                          >
                            <div className="col-12 videos p-0">
                              <div className="row m0">
                                <div className="col-12 songList p-0">
                                  <div className="row">
                                    {artistDetail &&
                                      artistDetail.metadata?.youtubeVideoLinks
                                        ?.slice(0, endIndexVideo)
                                        ?.map(
                                          (youtubeId: any, index: number) => (
                                            <ArtistVideo
                                              key={index}
                                              {...{
                                                artistDetail,
                                                videoUrl: youtubeId,
                                              }}
                                            />
                                          )
                                        )}
                                    {!artistDetail?.metadata
                                      ?.youtubeVideoLinks && (
                                      <EmptyMessage description="No videos found." />
                                    )}
                                  </div>
                                  {isMoreVideo && (
                                    <div className="col-12 load_more">
                                      <Link
                                        href={"#"}
                                        className="hvr-float-shadow"
                                        onClick={(e) => {
                                          e.preventDefault();
                                          handleLoadMoreVideo(e);
                                        }}
                                      >
                                        Load More Video
                                      </Link>
                                    </div>
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div
                          className={`tab-pane fade  ${
                            currentTab === "audio" ? "show active" : ""
                          }`}
                          id="tabs-text-3"
                          role="tabpanel"
                          aria-labelledby="tabs-text-3-tab"
                        >
                          <div className="col-12 Audios p-0">
                            <div className="row">
                              <div className="col-12 text-center">
                                <Button
                                  htmlType="button"
                                  disabled={songsFile?.url ? false : true}
                                  onClick={() =>
                                    downloadFile(
                                      `${songsFile?.url}`,
                                      `${songsFile.name}`
                                    )
                                  }
                                  type="primary"
                                >
                                  {songsFile?.url
                                    ? "Download the uploaded file"
                                    : "Song List Unavailable"}
                                </Button>
                              </div>
                              {/* {artistDetail?.songs?.length > 0 &&
                                artistDetail?.songs?.map((song, index) => {
                                  return (
                                    <Song
                                      key={`song_${index}`}
                                      {...{
                                        songPlayed,
                                        setSongPlayed,
                                        song,
                                        theme,
                                      }}
                                    />
                                  );
                                })}
                              {artistDetail?.songs?.length <= 0 && <Empty />}
                              {artistDetail?.songs?.length > 10 && (
                                <div className="col-12 load_more ldm-m">
                                  <a
                                    onClick={(e) => e.preventDefault()}
                                    className="hvr-float-shadow"
                                  >
                                    Browse All
                                  </a>
                                </div>
                              )} */}
                            </div>
                          </div>
                        </div>

                        <div
                          className={`tab-pane fade ${
                            currentTab === "photos" ? "show active" : ""
                          }`}
                          id="tabs-text-4"
                          role="tabpanel"
                          aria-labelledby="tabs-text-4-tab"
                        >
                          <ArtistPhotos
                            {...{
                              artistDetail,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <!-- Tab Content --> */}
              </div>
            </div>

            <div className="only-for-mobile mt-3">
              <ArtistDetailsTags
                {...{
                  artistDetail,
                  theme,
                  artistRatings,
                  setIsRatingModalOpen,
                  setIsReviewModalOpen,
                }}
              />
            </div>
          </div>
        </div>
      </div>
      {/* Artist Rating Section */}
      {/* <ArtistDetailRating
        {...{
          theme,
          artistDetail,
          artistRatings,
          setIsRatingModalOpen,
          setIsReviewModalOpen,
        }}
      /> */}
      {/* Artist Rating Section */}
      {/* NewLetterSection */}
      {/* NewLetterSection */}
    </>
  );
};
export default ArtistDetailsScene;
