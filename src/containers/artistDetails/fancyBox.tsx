import React, { useRef, useEffect } from "react";

import { Fancybox as NativeFancybox } from "@fancyapps/ui";
import "@fancyapps/ui/dist/fancybox/fancybox.css";

export interface FancyboxProps {
  children?: React.ReactNode;
  delegate?: string;
  options?: any;
}

const Fancybox = (props: FancyboxProps) => {
  const containerRef = useRef(null);

  useEffect(() => {
    const container = containerRef.current;

    const delegate = props.delegate || "[data-fancybox]";
    const options = props.options || {};

    NativeFancybox.bind(container, delegate, options);

    return () => {
      NativeFancybox.unbind(container);
      NativeFancybox.close();
    };
  });

  return (
    <div className="row" ref={containerRef}>
      {props.children}
    </div>
  );
};

export default Fancybox;
