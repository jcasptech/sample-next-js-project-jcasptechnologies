import { PlusCircleFilled, PlusCircleOutlined } from "@ant-design/icons";
import { EmptyMessage } from "@components/theme/empty";
import { FacebookIcon } from "@components/theme/icons/facebookIcon";
import { InstagramIcon } from "@components/theme/icons/instagramIcon";
import { LiveStreamIcon } from "@components/theme/icons/liveStreamIcon";
import { PrevSlickIcon } from "@components/theme/icons/nextPrevIcon";
import { NextSlickIcon } from "@components/theme/icons/nextSlickIcon";
import { PatreonIcon } from "@components/theme/icons/patreonIcon";
import { QuoteIcon } from "@components/theme/icons/quoteIcon";
import { QuoteIconSmall } from "@components/theme/icons/quoteIconSmall";
import { SpotifyIcon } from "@components/theme/icons/spotifyIcon";
import { VimeoIcon } from "@components/theme/icons/vimeoIcon";
import { WebsiteIcon } from "@components/theme/icons/websiteIcon";
import { YoutubeIcon } from "@components/theme/icons/youtubeIcon";
import {
  ArtistObject,
  ArtistRatingDataResponse,
  ArtistRatingObject,
} from "@redux/slices/artists";
import { Avatar, Col, Divider, List, Row, Skeleton } from "antd";
import "aos/dist/aos.css";
import Image from "next/image";
import { useRouter } from "next/router";
import { useRef } from "react";
import Slider from "react-slick";
import { capitalizeString, uppercaseFirst } from "src/libs/helpers";
import ArtistStyles from "./artistDetails.module.scss";

export interface ShowsProp {
  artistDetail: ArtistObject;
  theme: "dark" | "light";
  artistRatings: ArtistRatingDataResponse;
  setIsRatingModalOpen: (d: any) => void;
  setIsReviewModalOpen: (d: any) => void;
}

const ArtistDetailsTags = (props: ShowsProp) => {
  const {
    artistDetail,
    theme,
    artistRatings,
    setIsRatingModalOpen,
    setIsReviewModalOpen,
  } = props;

  const router = useRouter();

  const sliderRef = useRef<any>(null);
  const settings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 10000,
    infinite: true,
    arrows: false,
    dots: false,
    pauseOnHover: false,
  };

  return (
    <>
      <div className="sidebar float-left w-100">
        <div className="row m0 artist_sec">
          <div className="col-12 title p-0 borderGray">
            <h5>Elements</h5>
          </div>
          <div className="USholder float-left w-100 mp-0">
            <div className="row">
              <div className="col-12 p-0">
                {artistDetail &&
                  artistDetail?.tags &&
                  Array.isArray(artistDetail?.tags) &&
                  artistDetail?.tags?.length > 0 &&
                  artistDetail?.tags?.map((tag: any, index: any) => (
                    <span
                      className="EleTag cursor-pointer capitalize"
                      key={index}
                      onClick={(e) => {
                        e.preventDefault();
                        router.push(`/browse?tag=${tag}`);
                      }}
                    >
                      {capitalizeString(tag)}
                    </span>
                  ))}

                {(!artistDetail?.tags || artistDetail?.tags?.length <= 0) && (
                  <EmptyMessage description="No elements found." />
                )}
              </div>
            </div>
          </div>
          <div className="col-12 title p-0 sideSocial borderGray d-flex justify-content-between">
            <h5>Shoutouts</h5>
            <PlusCircleOutlined
              className={`${
                theme === "dark" ? "text-white" : "text-dark"
              } f-20 cursor-pointer mb-1`}
              onClick={setIsRatingModalOpen}
            />
          </div>
          {/* <Row className="mt-3 ps-0 pe-0 h-400-max"  justify="center" align="middle">
            {artistRatings?.rating?.map((data: ArtistRatingObject, index: number) => {
              return <>
                <Col span={5}>
                  <Avatar 
                    alt={data?.user?.name}
                    size={48}
                    src={data?.user?.iconImage?.url || "/images/general/user-placeholder.png"}
                    shape="circle"
                  />
                </Col>
                <Col span={19}>
                  <h4 className={`${theme === "dark" ? "text-white" : "text-dark"}`}>{data?.user?.name}</h4>
                  <p className="two-line-and-text-ellipsis"><QuoteIconSmall theme={theme} /> {uppercaseFirst(data?.comment)}</p>
                </Col>
              </>
            })}
          </Row> */}

          <Slider ref={sliderRef} {...settings} className="artist_shoutouts">
            {artistRatings &&
              artistRatings?.rating?.length &&
              artistRatings?.rating?.map((data: any, index: number) => (
                <Row
                  className={`mt-3 h-400-max review_slide ${theme}`}
                  justify="center"
                  align="middle"
                  key={index}
                >
                  <Col span={24}>
                    <div className="name">
                      <Image
                        alt={data?.user?.name}
                        width={48}
                        height={48}
                        src={
                          data?.user?.iconImage?.url ||
                          "/images/general/user-placeholder.png"
                        }
                        className="profile-image"
                      />

                      <h4
                        className={`${
                          theme === "dark" ? "text-white" : "text-dark"
                        } d-flex`}
                      >
                        {data?.user?.name}&nbsp;
                        <Image
                          src="/images/about-us/blue-tic.png"
                          alt="Verified"
                          height={18}
                          width={18}
                        />
                      </h4>
                    </div>
                  </Col>
                  <Col span={24} className="mt-3">
                    <p
                      className={`${
                        theme === "dark" ? "text-white" : "text-dark"
                      }`}
                    >
                      <QuoteIconSmall theme={theme} />{" "}
                      {uppercaseFirst(data?.comment)}
                    </p>
                  </Col>
                </Row>
              ))}
          </Slider>

          <Row>
            <Col span={24}>
              {artistRatings?.rating?.length > 1 && (
                <div className={ArtistStyles.sliderIcon}>
                  <a onClick={() => sliderRef?.current?.slickPrev()}>
                    <PrevSlickIcon
                      {...{
                        theme,
                        className: "m-2 mt-3",
                      }}
                    />
                  </a>
                  <a onClick={() => sliderRef?.current?.slickNext()}>
                    <NextSlickIcon
                      {...{
                        theme,
                        className: "m-2 mt-3",
                      }}
                    />
                  </a>
                </div>
              )}
            </Col>
          </Row>

          <div className="col-12 title p-0 sideSocial borderGray">
            <h5>Follow on Social Media</h5>
          </div>
          <ul
            className="socialul"
            // data-aos="fade-down"
            // data-aos-delay="100"
            // data-aos-duration="1200"
          >
            {!artistDetail?.metadata?.facebookPage &&
              !artistDetail?.metadata?.instagramUsername &&
              !artistDetail?.metadata?.youtubeId &&
              !artistDetail?.metadata?.vemeoLink &&
              !artistDetail?.metadata?.website &&
              !artistDetail?.metadata?.liveStreamUrl &&
              !artistDetail?.metadata?.patreonLink &&
              !artistDetail?.metadata?.spotifyLink && (
                <EmptyMessage description="No social links found." />
              )}

            {artistDetail?.metadata?.facebookPage && (
              <li>
                <a
                  href={artistDetail?.metadata?.facebookPage || ""}
                  target="_blank"
                  title="Facebook"
                >
                  <FacebookIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.instagramUsername && (
              <li>
                <a
                  href={artistDetail?.metadata?.instagramUsername || ""}
                  target="_blank"
                  title="Instagram"
                >
                  <InstagramIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.youtubeId && (
              <li>
                <a
                  href={`https://www.youtube.com/watch?v=${artistDetail?.metadata?.youtubeId}`}
                  target="_blank"
                  title="YouTube"
                >
                  <YoutubeIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.vemeoLink && (
              <li>
                <a
                  href={artistDetail?.metadata?.vemeoLink}
                  target="_blank"
                  title="Vemeo"
                >
                  <VimeoIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.website && (
              <li>
                <a
                  href={artistDetail?.metadata?.website}
                  target="_blank"
                  title="Website"
                >
                  <WebsiteIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.liveStreamUrl && (
              <li>
                <a
                  href={artistDetail?.metadata?.liveStreamUrl}
                  target="_blank"
                  title="Live Stram"
                >
                  <LiveStreamIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.patreonLink && (
              <li>
                <a
                  href={artistDetail?.metadata?.patreonLink}
                  target="_blank"
                  title="Patreon"
                >
                  <PatreonIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.spotifyLink && (
              <li>
                <a
                  href={artistDetail?.metadata?.spotifyLink}
                  target="_blank"
                  title="Spotify"
                >
                  <SpotifyIcon {...{ theme }} />
                </a>
              </li>
            )}

            {artistDetail?.metadata?.youtubeChannel && (
              <li>
                <a
                  href={artistDetail?.metadata?.youtubeChannel}
                  target="_blank"
                  title="YouTube"
                >
                  <YoutubeIcon {...{ theme }} />
                </a>
              </li>
            )}
          </ul>
        </div>
      </div>
    </>
  );
};

export default ArtistDetailsTags;
