import ArtistStyles from "./artistDetails.module.scss";
import Slider from "react-slick";
import React, { useState, useRef } from "react";
import { ArtistObject, ArtistRatingDataResponse } from "@redux/slices/artists";
import { HalfStarIcon } from "@components/theme/icons/HalfStarIcon";
import { FullStartIcon } from "@components/theme/icons/fullStarIcon";
import { FullStarGrayIcon } from "@components/theme/icons/fullStarGrayIcon";
import { NextSlickIcon } from "@components/theme/icons/nextSlickIcon";
import { PrevSlickIcon } from "@components/theme/icons/nextPrevIcon";
import { getFormatReview } from "src/libs/helpers";
import Image from "next/image";

export interface ArtistDetailsRatingProps {
  artistDetail: ArtistObject;
  artistRatings: ArtistRatingDataResponse;
  theme: "dark" | "light";
  setIsRatingModalOpen: (d: any) => void;
  setIsReviewModalOpen: (d: any) => void;
}

const ArtistDetailRating = (props: ArtistDetailsRatingProps) => {
  const {
    artistRatings,
    artistDetail,
    theme,
    setIsRatingModalOpen,
    setIsReviewModalOpen,
  } = props;
  const sliderRef = useRef<any>(null);
  const settings = {
    slidesToShow: 4,
    slidesToScroll: 2,
    autoplay: false,
    autoplaySpeed: 2500,
    infinite: false,
    mobileFirst: true,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <>
      <div className={`foot section-pad ${theme == "dark" ? "dark" : ""}`}>
        <div className={ArtistStyles.artist_review_desktop_section}>
          <div>
            <h4
              className={` ${theme === "dark" ? "text-light" : "text-dark"} `}
            >
              What other think about this artist
            </h4>
          </div>
          <div className={ArtistStyles.artist_review_section}>
            <div className={`avarage_rating ${theme == "dark" ? "dark" : ""}`}>
              <div>{getFormatReview(artistDetail?.rating || 0)}</div>
              <div>
                {[1, 2, 3, 4, 5].map((star, index) => (
                  <span key={`star-${index}`}>
                    {star <= artistDetail?.rating ? (
                      <FullStartIcon />
                    ) : star > artistDetail?.rating &&
                      artistDetail?.rating > star - 1 ? (
                      <HalfStarIcon />
                    ) : (
                      <FullStarGrayIcon />
                    )}
                  </span>
                ))}
              </div>
              <div>
                ({getFormatReview(artistDetail?.ratingCount || 0)} Rating)
              </div>
            </div>
            <div className="avarage_rating_line">
              <div></div>
            </div>
            <div className={`rating_progress ${theme == "dark" ? "dark" : ""}`}>
              <div>
                <div>5 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star5?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star5?.total)}
                </div>
              </div>
              <div>
                <div>4 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star4?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star4?.total)}
                </div>
              </div>
              <div>
                <div>3 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star3?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star3?.total)}
                </div>
              </div>
              <div>
                <div>2 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star2?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star2?.total)}
                </div>
              </div>
              <div>
                <div>1 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star1?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star1?.total)}
                </div>
              </div>
            </div>
            <div className="rating_buttons">
              <div
                className={`${
                  theme === "dark"
                    ? ArtistStyles.load_more1
                    : ArtistStyles.load_more
                }`}
              >
                {artistDetail?.ratingCount > 0 && (
                  <a
                    onClick={() => setIsReviewModalOpen(true)}
                    className="hvr-float-shadow"
                  >
                    See All Review
                  </a>
                )}
              </div>
              <div className={`${ArtistStyles.load_moreReting}`}>
                <a
                  onClick={() => setIsRatingModalOpen(true)}
                  className="hvr-float-shadow"
                >
                  Write Review
                </a>
              </div>
            </div>
          </div>
        </div>

        <div className={ArtistStyles.artist_review_mobile_section}>
          <div className="first">
            <h4
              className={` ${theme === "dark" ? "text-light" : "text-dark"} `}
            >
              What other think about this artist
            </h4>
            <div className={`avarage_rating ${theme == "dark" ? "dark" : ""}`}>
              <div>{artistDetail?.rating || 0}</div>
              <div>
                {[1, 2, 3, 4, 5].map((star, index) => (
                  <span key={`star-${index}`}>
                    {star <= artistDetail?.rating ? (
                      <FullStartIcon />
                    ) : star > artistDetail?.rating &&
                      artistDetail?.rating > star - 1 ? (
                      <HalfStarIcon />
                    ) : (
                      <FullStarGrayIcon />
                    )}
                  </span>
                ))}
              </div>
              <div>
                ({getFormatReview(artistDetail?.ratingCount || 0)} Rating)
              </div>
            </div>
          </div>
          <div className="second">
            <div className={`rating_progress ${theme == "dark" ? "dark" : ""}`}>
              <div>
                <div>5 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star5?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star5?.total)}
                </div>
              </div>
              <div>
                <div>4 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star4?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star4?.total)}
                </div>
              </div>
              <div>
                <div>3 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star3?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star3?.total)}
                </div>
              </div>
              <div>
                <div>2 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star2?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star2?.total)}
                </div>
              </div>
              <div>
                <div>1 Star</div>
                <div className="progress">
                  <span></span>
                  <span
                    style={{
                      width: `${artistRatings?.ratingCount?.star1?.percentage}%`,
                    }}
                  ></span>
                </div>
                <div>
                  {getFormatReview(artistRatings?.ratingCount?.star1?.total)}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <Slider ref={sliderRef} {...settings}>
            {artistRatings &&
              artistRatings?.rating?.length &&
              artistRatings?.rating?.map((data: any, index: number) => (
                <div className="col-12 col-md-6 col-lg-3" key={index}>
                  <div className="card card-bg text-white">
                    <div
                      className={`card-j2 ${
                        theme === "dark"
                          ? ArtistStyles.review_RatingDark
                          : ArtistStyles.review_Ratinglight
                      }`}
                    >
                      <h4 className="capitalize">
                        {data.user.name || ""}{" "}
                        <Image
                          src="/images/landing-page/verified.png"
                          alt="verified"
                          width={32}
                          height={22}
                        />
                      </h4>
                      <div className="rating">
                        <ul>
                          {[1, 2, 3, 4, 5].map((star) => (
                            <li key={`star-${star}`}>
                              {star <= data?.rating ? (
                                <FullStartIcon />
                              ) : star > data?.rating &&
                                data?.rating > star - 1 ? (
                                <HalfStarIcon />
                              ) : (
                                <FullStarGrayIcon />
                              )}
                            </li>
                          ))}
                        </ul>
                      </div>
                      <p className={`card-text ${ArtistStyles.comment}`}>
                        {data?.comment || ""}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
          </Slider>
          {artistRatings?.rating?.length > 4 && (
            <div className={ArtistStyles.sliderIcon}>
              <a onClick={() => sliderRef?.current?.slickPrev()}>
                <PrevSlickIcon
                  {...{
                    theme,
                    className: "m-2 mt-3",
                  }}
                />
              </a>
              <a onClick={() => sliderRef?.current?.slickNext()}>
                <NextSlickIcon
                  {...{
                    theme,
                    className: "m-2 mt-3",
                  }}
                />
              </a>
            </div>
          )}
          {artistDetail?.ratingCount > 0 && (
            <div className="col-12 col-sm-2 mt-1">
              <div
                className={`col-12 ${ArtistStyles.load_moreM} load_more ldm-m for-mobile`}
              >
                <a
                  onClick={() => setIsReviewModalOpen(true)}
                  className="hvr-float-shadow"
                >
                  See All Review
                </a>
              </div>
            </div>
          )}
          <div className="col-12 col-sm-2 mt-1">
            <div
              className={`col-12 ${ArtistStyles.load_moreReting} load_more ldm-m for-mobile`}
            >
              <a
                className="hvr-float-shadow"
                onClick={() => setIsRatingModalOpen(true)}
              >
                Write Review
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* <div className={`foot section-pad ${theme == "dark" ? "dark" : ""}`}> */}
      {/* <div className="row averageCard for-mobile ">
          <div className="col-6 col-sm-4">
            <h4
              className={` ${theme === "dark" ? "text-light" : "text-dark"} `}
            >
              What other think about this artist
            </h4>
          </div>

          <div
            className={`col-6 col-md-2 ${
              theme == "dark" ? ArtistStyles.darkCard : "card-1"
            } for-mobile `}
          >
            <h4 className="col-12">{getFormatReview(artistDetail?.rating || 0)} </h4>
            <div className={`row ${ArtistStyles.avrage}`}>
              <div className={`col-12 ${ArtistStyles.avrage_Rating}`}>
                <ul>
                  {[1, 2, 3, 4, 5].map((star) => (
                    <li key={`star-${star}`} data-star={star}>
                      {star <= artistDetail?.rating ? (
                        <FullStartIcon />
                      ) : star > artistDetail?.rating &&
                        artistDetail?.rating > star - 1 ? (
                        <HalfStarIcon />
                      ) : (
                        <FullStarGrayIcon />
                      )}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            <div className="row">
              <h5 className="col-12">
                ({artistDetail?.ratingCount || 0} Rating){" "}
              </h5>
            </div>
          </div>
        </div>

        <div className="row card-con ">
          <div
            className={`col-12 col-md-3 col-lg-2 ${
              theme == "dark" ? ArtistStyles.darkCard : "card-1"
            } for-desktop`}
          >
            <h2>{artistDetail?.rating || 0} </h2>
            <div className={`row ${ArtistStyles.avrage}`}>
              <div className={`col-12 ${ArtistStyles.avrage_Rating}`}>
                <ul>
                  {[1, 2, 3, 4, 5].map((star) => (
                    <li key={`star-${star}`}>
                      {star <= artistDetail?.rating ? (
                        <FullStartIcon />
                      ) : star > artistDetail?.rating &&
                        artistDetail?.rating > star - 1 ? (
                        <HalfStarIcon />
                      ) : (
                        <FullStarGrayIcon />
                      )}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            <div className="row">
              <h5 className="col-12">
                ({artistDetail?.ratingCount || 0} Rating)
              </h5>
            </div>
          </div>
          <div
            className={
              theme === "dark" ? ArtistStyles.line2 : ArtistStyles.line
            }
          ></div>
          <div className={`col-12 col-md-3 p-0`}>
            <div
              className={`${
                theme === "dark"
                  ? ArtistStyles.ratingAverage
                  : ArtistStyles.ratingAveragelight
              }`}
            >
              <div>
                <p>5 star</p>
              </div>
              <div className={`col-8 progress ${ArtistStyles.progress}`}>
                <div
                  className={`progress-bar ${ArtistStyles.progress__progressBar}`}
                  role="progressbar"
                  style={{
                    width: `${artistRatings?.ratingCount?.star5?.percentage}%`,
                  }}
                  aria-valuenow={artistRatings?.ratingCount?.star5?.percentage}
                  aria-valuemin={0}
                  aria-valuemax={artistRatings?.ratingCount?.star5?.percentage}
                ></div>
              </div>
              <div>
                <p>{artistRatings?.ratingCount?.star5?.total}</p>
              </div>
            </div>
            <div
              className={` ${
                theme === "dark"
                  ? ArtistStyles.ratingAverage
                  : ArtistStyles.ratingAveragelight
              }`}
            >
              <div>
                <p>4 star</p>
              </div>
              <div className={`col-8 progress ${ArtistStyles.progress}`}>
                <div
                  className={`progress-bar ${ArtistStyles.progress__progressBar}`}
                  role="progressbar"
                  style={{
                    width: `${artistRatings?.ratingCount?.star4?.percentage}%`,
                  }}
                ></div>
              </div>
              <div>
                <p>{artistRatings?.ratingCount?.star4?.total}</p>
              </div>
            </div>
            <div
              className={`${
                theme === "dark"
                  ? ArtistStyles.ratingAverage
                  : ArtistStyles.ratingAveragelight
              }`}
            >
              <div>
                <p>3 star</p>
              </div>
              <div className={`col-8 progress ${ArtistStyles.progress}`}>
                <div
                  className={`progress-bar ${ArtistStyles.progress__progressBar}`}
                  role="progressbar"
                  style={{
                    width: `${artistRatings?.ratingCount?.star3?.percentage}%`,
                  }}
                ></div>
              </div>
              <div>
                <p>{artistRatings?.ratingCount?.star3?.total}</p>
              </div>
            </div>
            <div
              className={`${
                theme === "dark"
                  ? ArtistStyles.ratingAverage
                  : ArtistStyles.ratingAveragelight
              }`}
            >
              <div>
                <p>2 star</p>
              </div>
              <div className={`col-8 progress ${ArtistStyles.progress}`}>
                <div
                  className={`progress-bar ${ArtistStyles.progress__progressBar}`}
                  role="progressbar"
                  style={{
                    width: `${artistRatings?.ratingCount?.star2?.percentage}%`,
                  }}
                ></div>
              </div>
              <div>
                <p> {artistRatings?.ratingCount?.star2?.total}</p>
              </div>
            </div>
            <div
              className={`${
                theme === "dark"
                  ? ArtistStyles.ratingAverage
                  : ArtistStyles.ratingAveragelight
              }`}
            >
              <div>
                <p>1 star</p>
              </div>
              <div className={`col-8 progress ${ArtistStyles.progress}`}>
                <div
                  className={`progress-bar ${ArtistStyles.progress__progressBar}`}
                  role="progressbar"
                  style={{
                    width: `${artistRatings?.ratingCount?.star1?.percentage}%`,
                  }}
                ></div>
              </div>
              <div>
                <p>{artistRatings?.ratingCount?.star1?.total}</p>
              </div>
            </div>
          </div>
          <div className={`col-2 col-md-4 ${ArtistStyles.displayNone}`}></div>
          <div
            className={`col-12 col-md-2 ${
              theme === "dark"
                ? ArtistStyles.load_more1
                : ArtistStyles.load_more
            } ldm-m for-deskto`}
          >
            <a
              onClick={() => setIsReviewModalOpen(true)}
              className="hvr-float-shadow"
            >
              See All Review
            </a>
          </div>
          <div
            className={`col-12 col-md-2 ${ArtistStyles.load_moreReting} ldm-m for-desktop`}
          >
            <a
              onClick={() => setIsRatingModalOpen(true)}
              className="hvr-float-shadow"
            >
              Write Review
            </a>
          </div>
        </div> */}

      {/* </div> */}
    </>
  );
};
export default ArtistDetailRating;
