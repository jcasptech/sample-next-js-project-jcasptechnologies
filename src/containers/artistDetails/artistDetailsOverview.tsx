import { ArtistObject } from "@redux/slices/artists";
import React, { useEffect, useState } from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import Song from "@components/artist/song";
import { EmptyMessage } from "@components/theme/empty";
import ArtistVideo from "@components/artist/artistVideo";
import { downloadFile } from "src/libs/helpers";
import artistStyle from "./artistDetails.module.scss";
import { Button } from "@components/theme";

export interface ArtistDetailsOverviewProps {
  artistDetail: ArtistObject;
  theme: "dark" | "light";
  setCurrentTab: (d: any) => void;
  songPlayed: string;
  setSongPlayed: (d: any) => void;
  songsFile: { url: string; name: string };
}

const ArtistOverview = (props: ArtistDetailsOverviewProps) => {
  const {
    artistDetail,
    theme,
    setCurrentTab,
    songPlayed,
    setSongPlayed,
    songsFile,
  } = props;

  const [showFullText, setShowFullText] = useState(false);

  // const [songsFile, setSongsFile] = useState<any>({
  //   url: "",
  //   name: "",
  // });

  const handleShowFullText = () => {
    setShowFullText(!showFullText);
  };

  // useEffect(() => {
  //   if (artistDetail?.metadata?.csv) {
  //     setSongsFile({
  //       url: artistDetail?.metadata?.csv?.url || "",
  //       name: artistDetail?.metadata?.csv?.name || "",
  //     });
  //   }
  //   if (artistDetail?.metadata?.pdf) {
  //     setSongsFile({
  //       url: artistDetail?.metadata?.pdf?.url || "",
  //       name: artistDetail?.metadata?.pdf?.name || "",
  //     });
  //   }
  // }, [artistDetail]);

  return (
    <>
      <div className="row m0 artist_sec">
        <div className="col-12 title p-0 borderGray">
          <h5>About Artist</h5>
        </div>
        <div className="col-12 desc p-0">
          <p>
            {artistDetail?.metadata?.about && (
              <>
                {artistDetail?.metadata?.about?.length > 400 ? (
                  <>
                    {showFullText
                      ? artistDetail?.metadata?.about
                      : `${artistDetail?.metadata?.about?.substring(
                          0,
                          400
                        )} ...`}
                  </>
                ) : (
                  artistDetail?.metadata?.about
                )}
              </>
            )}
          </p>
          {artistDetail?.metadata?.about &&
            artistDetail?.metadata?.about?.length > 400 &&
            !showFullText && (
              <a className="readmore" onClick={handleShowFullText}>
                {showFullText ? "" : "Read more"}
              </a>
            )}

          {!artistDetail?.metadata?.about && (
            <EmptyMessage description="No details found." />
          )}
        </div>
      </div>
      <div
        className="row m0 artist_sec"
        // data-aos="fade-up"
        // data-aos-delay="100"
        // data-aos-duration="1200"
      >
        <div className="col-12 title p-0 borderGray">
          <h5>Artist Videos</h5>
          <a
            onClick={() => setCurrentTab("video")}
            className={theme === "dark" ? "redLnk" : "whiteLnk"}
          >
            Artist Videos &#8594;
          </a>
        </div>
        <div className="col-12 videos p-0">
          <div className="row m0">
            {artistDetail &&
              artistDetail.metadata?.youtubeVideoLinks
                ?.slice(0, 2)
                ?.map((youtubeId: any, index: number) => (
                  <ArtistVideo
                    key={`video_${index}`}
                    {...{
                      artistDetail,
                      videoUrl: youtubeId,
                    }}
                  />
                ))}
            {!artistDetail?.metadata?.youtubeVideoLinks && (
              <EmptyMessage description="No videos found." />
            )}
          </div>
        </div>
      </div>
      <div className="row m0 artist_sec">
        <div className="col-12 title borderGray">
          <h5>Song List</h5>
          <a
            onClick={() => setCurrentTab("audio")}
            className={theme === "dark" ? "redLnk" : "whiteLnk"}
          >
            Song List &#8594;
          </a>
        </div>

        <div className="col-12 songList p-0">
          <div className="row">
            <div className="text-center">
              <Button
                htmlType="button"
                disabled={songsFile?.url ? false : true}
                onClick={() =>
                  downloadFile(`${songsFile?.url}`, `${songsFile.name}`)
                }
                type="primary"
              >
                {songsFile?.url
                  ? "Download the uploaded file"
                  : "Song List Unavailable"}
              </Button>
            </div>
          </div>
        </div>
      </div>

      {/* <div
        className="row m0 artist_sec"
      >
        <div className="col-12 title borderGray">
          <h5>Audio Files</h5>
          <a
            onClick={() => setCurrentTab("audio")}
            className={theme === "dark" ? "redLnk" : "whiteLnk"}
          >
            Song List &#8594;
          </a>
        </div>
        <div className="col-12 Audios p-0">
          <div className="row">
            {artistDetail?.songs?.length > 0 &&
              artistDetail?.songs?.slice(0, 6).map((song, index) => {
                return (
                  <Song
                    key={`song_${index}`}
                    {...{
                      song,
                      theme,
                      songPlayed,
                      setSongPlayed,
                    }}
                  />
                );
              })}

            {artistDetail?.songs?.length > 6 && (
              <div className="col-12 load_more ldm-m">
                <a
                  onClick={() => setCurrentTab("audio")}
                  className="hvr-float-shadow"
                >
                  Load More File
                </a>
              </div>
            )}

            {artistDetail?.songs?.length <= 0 && <Empty />}
          </div>
        </div>
      </div> */}
    </>
  );
};
export default ArtistOverview;
