import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import ArtistClaimModal from "@components/modals/artistClaimModal";
import ArtistMessageModal from "@components/modals/artistMessageModal";
import ArtistRatingModal from "@components/modals/artistRatingModal";
import ArtistReviewModal from "@components/modals/artistReviewModal";
import { LoginModal } from "@components/modals/loginModal";
import ShareThisModal from "@components/modals/ShareThisModal";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import {
  addArtistSubscribeAPI,
  artistUnsubscribeAPI,
  getartistRatingAPI,
  getArtistVanityAPI,
} from "@redux/services/artist.api";
import { whoAmI } from "@redux/services/auth.api";
import { ArtistObject } from "@redux/slices/artists";
import {
  artistAllShows,
  artistloadMoreAllShows,
  artistloadMoreUpComingShows,
  artistShowsState,
  artistUpComingShows,
} from "@redux/slices/artistShow";
import { LoginUserState, loginUserSuccess } from "@redux/slices/auth";
import { setGeneralTheme } from "@redux/slices/general";
import { useReCaptcha } from "next-recaptcha-v3";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SITE_URL, SUCCESS_MESSAGES } from "src/libs/constants";
import { nextRedirect } from "src/libs/helpers";
import useList from "src/libs/useList";
import NotFoundScene from "../404/notFoundScene";
import ArtistDetailsScene from "./artistDetailsScene";

export interface ArtistDetailsContainerProps {
  artistVanity: string;
}

const ArtistDetailsContainer = (props: ArtistDetailsContainerProps) => {
  const { artistVanity } = props;

  const [artistDetail, setArtistDetail] = useState<ArtistObject | null>(null);
  const [isMessageModalOpen, setIsMesaageModalOpen] = useState(false);
  const [isArtistEditable, setIsArtistEditable] = useState(false);
  const [isRatingModalOpen, setIsRatingModalOpen] = useState(false);
  const [isReviewModalOpen, setIsReviewModalOpen] = useState(false);
  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [isClaimableModalOpen, setIsClaimableModalOpen] = useState(false);

  const [isArtistShowType, setIsArtistShowType] = useState<"upcoming" | "all">(
    "upcoming"
  );
  const [theme, setTheme] = useState<"dark" | "light">("light");
  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();
  const router = useRouter();
  const [follows, setFollows] = useState<any[]>([]);
  const [isActionLoading, setIsActionLoading] = useState(false);
  const [currentTab, setCurrentTab] = useState<
    "overView" | "video" | "audio" | "photos"
  >("overView");

  const [endIndexVideo, setEndIndexVideo] = useState<number>(4);
  const [isMoreVideo, setIsMoreVideo] = useState(false);
  const [artistRatings, setArtistRatings] = useState<any>(null);
  const [tmpArtistId, setTmpArtistId] = useState<any>(null);
  const [userAction, setUserAction] = useState<
    "artistSubscribe" | "artistMessage" | "artistShoutouts" | null
  >(null);
  const [songPlayed, setSongPlayed] = useState("");

  const [isShareModalOpen, setIsShareModalOpen] = useState(false);

  const [songsFile, setSongsFile] = useState<{ url: string; name: string }>({
    url: "",
    name: "",
  });

  useEffect(() => {
    if (artistDetail?.metadata?.extraYoutubeIds?.length > endIndexVideo) {
      setIsMoreVideo(true);
    } else {
      setIsMoreVideo(false);
    }
  }, [endIndexVideo]);

  const handleLoadMoreVideo = (e: any) => {
    setEndIndexVideo(endIndexVideo + 4);
  };

  const {
    artistShows: { isLoading, data: artistShows },
  }: {
    artistShows: artistShowsState;
  } = useSelector((state: RootState) => ({
    artistShows: state.artistShows,
  }));

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const { apiParam } = useList({
    queryParams: {
      take: 4,
      skip: 0,
    },
  });

  const loadArtistShow = async () => {
    if (artistDetail) {
      apiParam.take = 4;
      apiParam.skip = 0;
      if (isArtistShowType === "upcoming") {
        const recaptchaResponse = await executeRecaptcha(
          "artist_upcoming_shows"
        );
        dispatch(
          artistUpComingShows(
            artistDetail.objectId,
            apiParam,
            recaptchaResponse
          )
        );
      } else {
        const recaptchaResponse = await executeRecaptcha("artist_all_shows");
        dispatch(
          artistAllShows(artistDetail.objectId, apiParam, recaptchaResponse)
        );
      }
    }
  };

  const handleLoadMore = async () => {
    if (artistDetail) {
      if (apiParam?.take) {
        apiParam.skip = (apiParam.skip || 0) + 4;
        if (isArtistShowType === "upcoming") {
          const recaptchaResponse = await executeRecaptcha(
            "artist_upcoming_shows"
          );
          dispatch(
            artistloadMoreUpComingShows(
              artistDetail.objectId,
              apiParam,
              recaptchaResponse
            )
          );
        } else {
          const recaptchaResponse = await executeRecaptcha("artist_all_shows");
          dispatch(
            artistloadMoreAllShows(
              artistDetail.objectId,
              apiParam,
              recaptchaResponse
            )
          );
        }
      }
    }
  };

  useEffect(() => {
    if (loaded) {
      loadArtistShow();
    }
  }, [isArtistShowType, loaded]);

  useEffect(() => {
    if (artistDetail && artistDetail?.objectId && loaded) {
      if (artistDetail?.isClaimable) {
        // setIsClaimableModalOpen(true);
      } else {
        loadArtistShow();
      }
    }
  }, [artistDetail, loaded]);

  const handleArtistSubscribe = async (artistId: string) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          await addArtistSubscribeAPI(artistId);
          showToast(SUCCESS_MESSAGES.subscribeWebsite, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      setIsLoginModalOpen(true);
      setUserAction("artistSubscribe");
      setTmpArtistId(artistId);
      // showToast(SUCCESS_MESSAGES.requiredLoginSubscribe, "warning");
    }
  };

  const handleArtistUnsubscribe = async (artistId: string) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          await artistUnsubscribeAPI(artistId);
          showToast(SUCCESS_MESSAGES.unsubscribeWebsite, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      showToast(SUCCESS_MESSAGES.requiredLoginSubscribe, "warning");
    }
  };

  useEffect(() => {
    if (loginUserState?.data?.user?.follows) {
      setFollows(loginUserState?.data?.user?.follows);
    }
    if (
      loginUserState?.data?.user?.artists &&
      loginUserState?.data?.user?.artists?.includes(artistDetail?.objectId)
    ) {
      setIsArtistEditable(true);
    }
  }, [loginUserState?.data?.user]);

  useEffect(() => {
    const getArtistRatingData = async () => {
      if (artistDetail && artistDetail.objectId) {
        const recaptchaResponse = await executeRecaptcha("artist_rating");
        const artistRating: any = await getartistRatingAPI(
          artistDetail.objectId,
          recaptchaResponse
        );
        if (artistRating) {
          if (artistRating?.responseData) {
            setArtistRatings(artistRating.responseData);
          } else {
            setArtistRatings(artistRating);
          }
        }

        if (artistDetail?.metadata?.csv) {
          setSongsFile({
            url: artistDetail?.metadata?.csv?.url || "",
            name: artistDetail?.metadata?.csv?.name || "",
          });
        }
        if (artistDetail?.metadata?.pdf) {
          setSongsFile({
            url: artistDetail?.metadata?.pdf?.url || "",
            name: artistDetail?.metadata?.pdf?.name || "",
          });
        }
      }
    };
    getArtistRatingData();
  }, [artistDetail, loaded]);

  useEffect(() => {
    dispatch(setGeneralTheme(theme));
  }, [theme]);

  const getArtistDetails = async () => {
    try {
      const recaptchaResponse = await executeRecaptcha("artist_detail");
      const tmpA: any = await getArtistVanityAPI(
        artistVanity,
        recaptchaResponse
      );
      if (tmpA) {
        if (tmpA?.responseData) {
          setArtistDetail(tmpA.responseData);
        } else {
          setArtistDetail(tmpA);
        }

        if (tmpA?.metadata?.theme) {
          setTheme(tmpA?.metadata?.theme);
        }
      }
      setIsActionLoading(false);
    } catch (e) {
      setIsActionLoading(false);
    }
  };

  useEffect(() => {
    if (artistVanity && loaded) {
      setIsActionLoading(true);
      getArtistDetails();
    }
  }, [artistVanity, loaded]);

  // useEffect(() => {
  //   const handleLoad = () => {
  //     dispatch(setGeneralTheme(""));
  //   };

  //   const handleRouteChange = () => {
  //     handleLoad();
  //   };
  //   router.events.on("routeChangeComplete", handleRouteChange);
  // }, [router?.asPath]);

  return (
    <>
      {isLoading || (isActionLoading && <DefaultSkeleton />)}
      {isActionLoading && !artistDetail && <div className="h-75vs"></div>}

      {!isActionLoading && !artistDetail && (
        <>
          <NotFoundScene
            {...{
              buttonLabel: "Browse artists",
              buttonLink: "/browse",
              description:
                "This artist's profile is currently under maintenance.\n Come back later or check out some of the great musicians we have to offer!",
              heading: "TUNING IN PROGRESS",
            }}
          />
        </>
      )}

      {artistDetail && (
        <>
          <ArtistDetailsScene
            {...{
              artistDetail,
              artistRatings,
              artistShows,
              theme,
              follows,
              setIsMesaageModalOpen,
              setIsRatingModalOpen,
              setIsReviewModalOpen,
              handleLoadMore,
              isArtistShowType,
              setIsArtistShowType,
              handleArtistSubscribe,
              handleArtistUnsubscribe,
              currentTab,
              setCurrentTab,
              isMoreVideo,
              endIndexVideo,
              handleLoadMoreVideo,
              songPlayed,
              setSongPlayed,
              isArtistEditable,
              isLoading,
              setIsShareModalOpen,
              songsFile,
            }}
          />

          {isMessageModalOpen && (
            <ArtistMessageModal
              {...{
                artistDetail,
                isOpen: isMessageModalOpen,
                setIsMesaageModalOpen,
              }}
            />
          )}

          {isRatingModalOpen && (
            <ArtistRatingModal
              {...{
                artistDetail,
                isOpen: isRatingModalOpen,
                setIsRatingModalOpen,
              }}
            />
          )}

          {isReviewModalOpen && (
            <ArtistReviewModal
              {...{
                artistDetail,
                theme,
                isOpen: isReviewModalOpen,
                setIsReviewModalOpen,
                artistRatings,
              }}
            />
          )}

          {isShareModalOpen && (
            <ShareThisModal
              {...{
                isOpen: isShareModalOpen,
                url: `${SITE_URL}${router.asPath}`,
                setIsShareModalOpen,
              }}
            />
          )}

          {isClaimableModalOpen && (
            <ArtistClaimModal
              {...{
                artistDetail,
                onOk: () => {
                  router.back();
                },
              }}
            />
          )}
        </>
      )}

      {isLoginModalOpen && userAction && (
        <LoginModal
          {...{
            actionFrom: userAction,
            handleOk: () => {
              setIsLoginModalOpen(false);
              setUserAction(null);
              if (userAction === "artistSubscribe") {
                handleArtistSubscribe(tmpArtistId);
              }
            },
            isOpen: isLoginModalOpen,
            handleCancel: () => {
              setIsLoginModalOpen(false);
            },
          }}
        />
      )}
    </>
  );
};

ArtistDetailsContainer.getInitialProps = async (ctx: any) => {
  const artistVanity = ctx?.query?.vanity;
  // let artistDetail = null;
  // try {
  //   if (artistVanity) {
  //     const artistDetails: any = await getArtistVanityAPI(artistVanity);
  //     if (artistDetails) {
  //       if (artistDetails?.responseData) {
  //         artistDetail = artistDetails.responseData;
  //       } else {
  //         artistDetail = artistDetails;
  //       }
  //     }
  //   }
  // } catch (e: any) {
  //   nextRedirect({ ctx, location: "/404" });
  // }
  return {
    artistVanity,
  };
};

ArtistDetailsContainer.Layout = MainLayoutComponent;
export default ArtistDetailsContainer;
