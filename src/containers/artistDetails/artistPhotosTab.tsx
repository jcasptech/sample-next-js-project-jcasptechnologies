import { ArtistObject } from "@redux/slices/artists";
import React, { useEffect, useState } from "react";
import Fancybox from "./fancyBox";
import AOS from "aos";
import "aos/dist/aos.css";
import Link from "next/link";
import { EmptyMessage } from "@components/theme/empty";
import Image from "next/image";

export interface ArtistPhotosProps {
  artistDetail: ArtistObject;
}
const ArtistPhotos = (props: ArtistPhotosProps) => {
  const { artistDetail } = props;
  const [endIndex, setEndIndex] = useState<number>(12);
  const [isMore, setIsMore] = useState(false);

  useEffect(() => {
    if (artistDetail?.artistPhotos?.length > endIndex) {
      setIsMore(true);
    } else {
      setIsMore(false);
    }
  }, [endIndex]);

  const handleLoadMore = (e: any) => {
    setEndIndex(endIndex + 12);
  };

  return (
    <div className="row m0">
      <div className="col-12 p-0 ProfPGal">
        <div className="row text-center text-lg-start">
          <Fancybox
            options={{
              Carousel: {
                infinite: true,
              },
              slideshow: {
                autoStart: true,
                delay: 3000,
              },
            }}
          >
            {artistDetail &&
              artistDetail?.artistPhotos?.length > 0 &&
              artistDetail?.artistPhotos?.map((photo: any, index: number) => (
                <div className="col-lg-3 col-md-4 col-6" key={index}>
                  <a
                    href={photo?.sourceImage?.url || ""}
                    className="d-block mb-4 h-100 single_image"
                    data-fancybox="gallery"
                  >
                    <Image
                      className="img-fluid img-thumbnail"
                      src={photo?.thumbImage?.url || ""}
                      alt=""
                      width={270}
                      height={200}
                    />
                  </a>
                </div>
              ))}
          </Fancybox>
          {artistDetail?.artistPhotos?.length <= 0 && (
            <div className="pt-4">
              <EmptyMessage description="No photos found." />
            </div>
          )}
        </div>
        {isMore && (
          <div className="col-12 load_more">
            <Link
              href={"#"}
              className="hvr-float-shadow"
              onClick={(e) => {
                e.preventDefault();
                handleLoadMore(e);
              }}
            >
              Load More Photos
            </Link>
          </div>
        )}
      </div>
    </div>
  );
};
export default ArtistPhotos;
