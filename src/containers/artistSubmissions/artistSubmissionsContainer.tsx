import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import CalendarEventModal from "@components/modals/calendarEventModal";
import CalendarPostModal from "@components/modals/calendarPostingModal";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { withdrawSubmissionAPI } from "@redux/services/submitted-posting.api";
import { SeledtedArtistState } from "@redux/slices/artistList";
import {
  ConfirmedEventObject,
  ConfirmedEventState,
  fetchConfirmedEvent,
} from "@redux/slices/confirmedEvent";
import {
  fetchSubmittedPosting,
  loadMoreSubmittedPosting,
  SubmittedPostingObject,
  SubmittedPostingState,
} from "@redux/slices/submittedPosting";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import ArtistSubmissionsScene from "./artistSubmissionsScene";

export interface ArtistSubmissionsContainerProps {}

const ArtistSubmissionsContainer = (props: ArtistSubmissionsContainerProps) => {
  const dispatch = useDispatch();
  const [isPostModalOpen, setIsPostingModalOpen] = useState(false);
  const [isEventModalOpen, setIsEventModalOpen] = useState(false);
  const [submissionsType, setSubmissionsType] = useState<
    "submitted" | "confirmed"
  >("submitted");
  const [selectPosting, setSelectPosting] =
    useState<SubmittedPostingObject | null>(null);
  const [selectEvent, setSelectEvent] = useState<ConfirmedEventObject | null>(
    null
  );
  const [artistId, setArtistId] = useState(null);
  const [isActionLoading, setIsActionLoading] = useState(false);

  const {
    submittedPosting: { isLoading, data: postingData },
  }: {
    submittedPosting: SubmittedPostingState;
  } = useSelector((state: RootState) => ({
    submittedPosting: state.submittedPosting,
  }));

  const {
    confirmedEvent: { isLoading: eventIsLoading, data: eventData },
  }: {
    confirmedEvent: ConfirmedEventState;
  } = useSelector((state: RootState) => ({
    confirmedEvent: state.confirmedEvent,
  }));

  const {
    selectedArtist: { data: selectedArtistData },
  }: {
    selectedArtist: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    selectedArtist: state.selectedArtist,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 6,
      skip: 0,
    },
  });

  useEffect(() => {
    if (artistId) {
      if (submissionsType === "submitted") {
        dispatch(fetchSubmittedPosting(artistId, apiParam));
      } else {
        dispatch(fetchConfirmedEvent(artistId, apiParam));
      }
    }
  }, [apiParam, artistId]);

  const handleLoadMore = (d: any) => {
    if (artistId) {
      apiParam.skip = (apiParam.skip || 0) + 6;
      if (submissionsType === "submitted") {
        dispatch(loadMoreSubmittedPosting(artistId, apiParam));
      } else {
        dispatch(fetchConfirmedEvent(artistId, apiParam));
      }
    }
  };

  const handleWithdraw = async (d: SubmittedPostingObject) => {
    if (artistId && d?.objectId) {
      try {
        await withdrawSubmissionAPI(artistId, d?.objectId);
        showToast(SUCCESS_MESSAGES.withdrawSubmissionSuccess, "success");
        setIsActionLoading(false);

        if (submissionsType === "submitted") {
          dispatch(fetchSubmittedPosting(artistId, apiParam));
        } else {
          dispatch(fetchConfirmedEvent(artistId, apiParam));
        }
      } catch (error: any) {
        setIsActionLoading(false);
      }
    }
  };

  useEffect(() => {
    if (selectPosting) {
      setIsPostingModalOpen(true);
    } else {
      setIsPostingModalOpen(false);
    }
  }, [selectPosting]);

  useEffect(() => {
    if (selectEvent) {
      setIsEventModalOpen(true);
    } else {
      setIsEventModalOpen(false);
    }
  }, [selectEvent]);

  useEffect(() => {
    if (selectedArtistData?.objectId) {
      setArtistId(selectedArtistData.objectId);
    }
  }, [selectedArtistData]);

  useEffect(() => {
    if (!isPostModalOpen) {
      setSelectPosting(null);
    }
  }, [isPostModalOpen]);

  useEffect(() => {
    if (!isEventModalOpen) {
      setSelectEvent(null);
    }
  }, [isEventModalOpen]);

  useEffect(() => {
    apiParam.skip = 0;
    if (artistId) {
      if (submissionsType === "submitted") {
        dispatch(fetchSubmittedPosting(artistId, apiParam));
      } else {
        dispatch(fetchConfirmedEvent(artistId, apiParam));
      }
    }
  }, [submissionsType]);

  return (
    <>
      {(isLoading || isActionLoading || eventIsLoading) && <DefaultSkeleton />}
      <Show when={["artist_submissions"]} fallback={<NoPermissionsComponent />}>
        <ArtistSubmissionsScene
          {...{
            postingData,
            eventData,
            handleLoadMore,
            isLoading: isLoading || isActionLoading || eventIsLoading,
            setSelectPosting,
            setSelectEvent,
            submissionsType,
            setSubmissionsType,
            handleWithdraw,
          }}
        />
      </Show>

      {isPostModalOpen && selectPosting?.objectId && artistId && (
        <CalendarPostModal
          {...{
            isOpen: isPostModalOpen,
            setIsPostingModalOpen,
            id: selectPosting.objectId,
            artistId: artistId,
            isSubmitted: true,
            handleClose: (e) => {
              setSelectPosting(null);
            },
          }}
        />
      )}

      {isEventModalOpen && selectEvent?.objectId && artistId && (
        <CalendarEventModal
          {...{
            isOpen: isEventModalOpen,
            setIsEventModalOpen,
            id: selectEvent.objectId,
            handleClose: (e) => {
              setSelectEvent(null);
            },
          }}
        />
      )}
    </>
  );
};

ArtistSubmissionsContainer.Layout = AdminLayoutComponent;

export default ArtistSubmissionsContainer;
