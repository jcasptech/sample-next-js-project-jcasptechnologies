import { Button } from "@components/theme";
import { CalendarIcon } from "@components/theme/icons/calendarIcon";
import { ClockIcon } from "@components/theme/icons/clockIcon";
import { EyeIcon } from "@components/theme/icons/eyeIcon";
import { OpenPostingObject } from "@redux/slices/openPosting";
import { Divider, Popconfirm } from "antd";
import { DateTime } from "luxon";
import Image from "next/image";
import { useRouter } from "next/router";
import postingDetailsStyles from "./postingDetails.module.scss";

export interface PostingProps {
  posting: OpenPostingObject;
  setSelectPosting: (d: any) => void;
  handleWithdraw: (d: any) => void;
  submissionsType: "submitted" | "confirmed";
}
const Posting = (props: PostingProps) => {
  const { posting, setSelectPosting, handleWithdraw, submissionsType } = props;
  const router = useRouter();

  return (
    <>
      <div className="col-sm-12 col-lg-4 col-md-6">
        <div className={`${postingDetailsStyles.posting_detail}`}>
          <div className="image">
            <Image
              className={`cursor-pointer`}
              src={posting?.iconImage?.url || "/images/venue-placeholder.png"}
              alt={posting?.title}
              height={180}
              width={180}
            />
          </div>
          <div className="description">
            <div className="date_time cursor-pointer">
              <div>
                {/* <img src="/images/calic.png" alt="c" /> */}
                <CalendarIcon />
                &nbsp;
                {DateTime.fromISO(posting.startDate?.iso)
                  .setZone(posting?.timeZone || "America/Los_Angeles")
                  .toFormat("MMMM d, yyyy")}
              </div>
              <div>
                <ClockIcon />
                &nbsp;
                {DateTime.fromISO(posting?.startDate?.iso)
                  .setZone(posting?.timeZone || "America/Los_Angeles")
                  .toFormat("h:mm a")}
              </div>
            </div>
            <div className="title">
              <h3>{posting?.title}</h3>
            </div>
            <Divider className="divider" />

            <div className="row types">
              <div className="col-6">
                <label>Proceeds</label>
                <div>${posting?.artistProceeds || 0}</div>
              </div>
              <div className="col-6">
                <label>Type of event</label>
                <div className="capitalize">{posting?.variety}</div>
              </div>
              <div className="col-12">
                <label>Format</label>
                <div className="capitalize">{posting?.genres?.join(", ")}</div>
              </div>
            </div>
            <Divider className="divider" />

            <div className="actions">
              {submissionsType === "submitted" && (
                <Popconfirm
                  title="Are you sure you want to withdraw this submission?"
                  okText="Yes"
                  cancelText="No"
                  onConfirm={() => handleWithdraw(posting)}
                >
                  <Button htmlType="button" type="primary">
                    Withdraw
                  </Button>
                </Popconfirm>
              )}

              <Button
                htmlType="button"
                type="primary"
                onClick={(e) => {
                  e.preventDefault();
                  setSelectPosting(posting);
                }}
              >
                <EyeIcon />
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Posting;
