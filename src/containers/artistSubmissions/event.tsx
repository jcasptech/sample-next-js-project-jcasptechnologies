import { Button } from "@components/theme";
import { CalendarIcon } from "@components/theme/icons/calendarIcon";
import { ClockIcon } from "@components/theme/icons/clockIcon";
import { EyeIcon } from "@components/theme/icons/eyeIcon";
import { ConfirmedEventObject } from "@redux/slices/confirmedEvent";
import { Divider } from "antd";
import { DateTime } from "luxon";
import Image from "next/image";
import postingDetailsStyles from "./postingDetails.module.scss";

export interface EventProps {
  event: ConfirmedEventObject;
  setSelectEvent: (d: any) => void;
}
const Event = (props: EventProps) => {
  const { event, setSelectEvent } = props;

  return (
    <>
      <div className="col-sm-12 col-lg-4 col-md-6">
        <div className={`${postingDetailsStyles.posting_detail}`}>
          <div className="image">
            <Image
              className={`cursor-pointer`}
              src={event?.iconImage?.url || "/images/venue-placeholder.png"}
              alt={event?.title}
              height={180}
              width={180}
            />
          </div>
          <div className="description">
            <div className="date_time cursor-pointer">
              <div>
                {/* <img src="/images/calic.png" alt="c" /> */}
                <CalendarIcon />
                &nbsp;
                {DateTime.fromISO(event.startDate?.iso)
                  .setZone(event?.timeZone || "America/Los_Angeles")
                  .toFormat("MMMM d, yyyy")}
              </div>
              <div>
                {/* <img src="/images/home/clock.png" alt="c" /> */}
                <ClockIcon />
                &nbsp;
                {DateTime.fromISO(event?.startDate?.iso)
                  .setZone(event?.timeZone || "America/Los_Angeles")
                  .toFormat("h:mm a")}
              </div>
            </div>
            <div className="title">
              <h3>{event?.title}</h3>
            </div>
            <Divider className="divider" />

            <div className="row types">
              <div className="col-6">
                <label>Proceeds</label>
                <div>${event?.artistProceeds || 0}</div>
              </div>
              <div className="col-6">
                <label>Type of event</label>
                <div className="capitalize">{event?.variety}</div>
              </div>
              <div className="col-12">
                <label>Format</label>
                <div className="capitalize">{event?.genres?.join(", ")}</div>
              </div>
            </div>
            <Divider className="divider" />

            <div className="actions flex-end">
              <Button
                htmlType="button"
                type="primary"
                onClick={(e) => {
                  e.preventDefault();
                  setSelectEvent(event);
                }}
              >
                <EyeIcon />
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Event;
