import { DefaultLoader } from "@components/theme";
import {
  ConfirmedEventDataResponse,
  ConfirmedEventObject,
} from "@redux/slices/confirmedEvent";
import { OpenPostingObject } from "@redux/slices/openPosting";
import { SubmittedPostingDataResponse } from "@redux/slices/submittedPosting";
import { Empty } from "antd";
import Link from "next/link";
import "rc-slider/assets/index.css";
import ArtistSubmissionsStyles from "./artistSubmissions.module.scss";
import Event from "./event";
import Posting from "./posting";

export interface ArtistSubmissionsSceneProps {
  postingData: SubmittedPostingDataResponse;
  eventData: ConfirmedEventDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  setSelectPosting: (d: any) => void;
  setSelectEvent: (d: any) => void;
  submissionsType: "submitted" | "confirmed";
  setSubmissionsType: (d: "submitted" | "confirmed") => void;
  handleWithdraw: (d: any) => void;
}

const ArtistSubmissionsScene = (props: ArtistSubmissionsSceneProps) => {
  const {
    postingData,
    eventData,
    handleLoadMore,
    isLoading,
    submissionsType,
    setSubmissionsType,
    setSelectPosting,
    handleWithdraw,
    setSelectEvent,
  } = props;

  return (
    <>
      <div className="browse-open-posting-page h-100vh">
        <div className="panel-inner-content">
          <div className={ArtistSubmissionsStyles.container}>
            <div className="row m0 vcenter">
              <div className={`col-12 col-lg-6 col-md-6`}>
                <div className="nav-wrapper">
                  <ul
                    className="nav nav-pills nav-fill flex-row flex-md-row"
                    id="tabs-text"
                    role="tablist"
                  >
                    <li className="nav-item">
                      <a
                        className={`nav-link ${
                          submissionsType === "submitted"
                            ? "active disabled"
                            : ""
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setSubmissionsType("submitted");
                        }}
                      >
                        Submitted
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className={`nav-link ${
                          submissionsType === "confirmed"
                            ? "active disabled"
                            : ""
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setSubmissionsType("confirmed");
                        }}
                      >
                        Confirmed
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            {submissionsType === "submitted" && (
              <div className="postings row">
                {postingData &&
                  postingData?.list?.map(
                    (posting: OpenPostingObject, index: number) => (
                      <Posting
                        key={index}
                        {...{
                          posting,
                          setSelectPosting,
                          handleWithdraw,
                          submissionsType,
                        }}
                      />
                    )
                  )}

                {postingData?.list && postingData?.list?.length <= 0 && (
                  <Empty
                    image={`/images/general/no-data.svg`}
                    description="No posting found."
                  />
                )}

                {isLoading && <DefaultLoader />}
                {postingData.hasMany && (
                  <div className="col-12 load_more">
                    <Link
                      href={"#"}
                      className={`hvr-float-shadow ${
                        isLoading ? "disabled" : ""
                      }`}
                      onClick={(e) => {
                        e.preventDefault();
                        handleLoadMore({
                          loadMore: true,
                        });
                      }}
                    >
                      Load more
                    </Link>
                  </div>
                )}
              </div>
            )}

            {submissionsType === "confirmed" && (
              <div className="postings row">
                {eventData &&
                  eventData?.list?.map(
                    (event: ConfirmedEventObject, index: number) => (
                      <Event
                        key={index}
                        {...{
                          event,
                          setSelectEvent,
                        }}
                      />
                    )
                  )}

                {eventData?.list && eventData?.list?.length <= 0 && (
                  <Empty
                    image={`/images/general/no-data.svg`}
                    description="No confirmed posting found."
                  />
                )}

                {isLoading && <DefaultLoader />}
                {eventData.hasMany && (
                  <div className="col-12 load_more">
                    <Link
                      href={"#"}
                      className={`hvr-float-shadow ${
                        isLoading ? "disabled" : ""
                      }`}
                      onClick={(e) => {
                        e.preventDefault();
                        handleLoadMore({
                          loadMore: true,
                        });
                      }}
                    >
                      Load more
                    </Link>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default ArtistSubmissionsScene;
