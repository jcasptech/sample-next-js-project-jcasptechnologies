import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import AdminCalendarEventModal from "@components/modals/adminCalendarEventModal";
import AdminCalendarPostModal from "@components/modals/adminCalendarPostingModal";
import AdminAddEventModal from "@components/modals/adminCreateEventModal";
import CalendarShowsModal from "@components/modals/calendarShowsModal";
import CalendarAddNoteModal from "@components/modals/createNoteModal";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { FilterDropdownTypes } from "@components/theme/dropdown/filter";
import { CalendarClock } from "@components/theme/icons/calenderClockIcon";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { getMonthlySummaryAPI } from "@redux/services/calendar.api";
import {
  getApprovedArtistsData,
  getApprovedVenuesData,
  getExportUsersData,
  getNextMonthInvoiceData,
  getPreviousMonthInvoiceData,
  getValidEventsData,
} from "@redux/services/export.api";
import {
  generalSearchArtistsAPI,
  getVenuesSearchAPI,
} from "@redux/services/general.api";
import {
  CalendarState,
  eventObject,
  fetchAdminCalendar,
} from "@redux/slices/calendar";
import { GeneralState } from "@redux/slices/general";
import { SeledtedArtistState } from "@redux/slices/selectedArtist";
import { DateTime } from "luxon";
import { useRouter } from "next/router";
import Script from "next/script";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import AdminCalendarScane from "./adminCalendarScane";
import calendarStyles from "./calendarContainer.module.scss";

export interface CalendarContainerProps {}

const AdminCalendarContainer = (props: CalendarContainerProps) => {
  const calendarRef = useRef<any>(null);
  const pageRef = useRef<any>(null);

  const [modalPosition, setModalPosition] = useState({ top: 0, left: 0 });
  const [screenResolution, setScreenResolution] = useState({
    width: 0,
    height: 0,
  });

  const [isGodMode, setIsGodMode] = useState(false);
  const [clcikedDate, setClickedDate] = useState(new Date());
  const [isNoteModalOpen, setIsNoteModalOpen] = useState(false);
  const [isFirstTimeLoaded, setIsFirstTimeLoaded] = useState(false);
  const [isExportLoading, setIsExportLoading] = useState(false);
  const [monthlySummaryData, setMonthlySummaryData] = useState<any>(null);
  const selectArtistId: any = useSelector((state: RootState) => state.selectId);
  const [filters, setFilters] = useState<FilterDropdownTypes[]>([
    {
      label: "My Shows",
      options: [{ value: "shows", label: "Display shows", selected: false }],
    },
    {
      label: "JCasp PRO Bookings",
      options: [
        { value: "upcoming", label: "Upcoming", selected: true },
        { value: "completed", label: "Completed", selected: false },
        { value: "canceledByHost", label: "Canceled", selected: false },
      ],
    },
    {
      label: "Postings",
      options: [
        { value: "open", label: "Open", selected: true },
        { value: "removed", label: "Removed", selected: false },
        // { value: "nearMe", label: "Near Me", selected: false },
        // { value: "all", label: "All", selected: false },
      ],
    },
    {
      label: "Booking Inquiries",
      options: [
        { value: "submitted", label: "Submitted", selected: false },
        { value: "declined", label: "Declined by Artist", selected: false },
      ],
    },
    {
      label: "Notes",
      options: [{ value: "notes", label: "Display notes", selected: true }],
    },
  ]);

  const [allEvents, setAllEvents] = useState<any>([]);
  const [currentMonth, setCurrentMonth] = useState(
    DateTime.utc().toFormat("MMMM yyyy")
  );
  const [currentStart, setCurrentStart] = useState(null);

  const [calendarView, setCalendarView] = useState<
    | "dayGridMonth"
    | "timeGridWeek"
    | "timeGridDay"
    | "listMonth"
    | "listWeek"
    | "listDay"
  >("dayGridMonth");

  const [calendarExport, setCalendarExport] = useState<
    | "PreviousMonthInvoice"
    | "CurrentMonthInvoice"
    | "ApprovedArtistInvoice"
    | "AllUsersInvoice"
    | "ApprovedVenues"
    | "ValidEventsInvoice"
    | ""
  >("");

  const [isEventModalOpen, setIsEventModalOpen] = useState(false);
  const [isPostModalOpen, setIsPostingModalOpen] = useState(false);
  const [isShowModalOpen, setIsShowModalOpen] = useState(false);
  const [isNewEventModalOpen, setIsNewEventModalOpen] = useState(false);
  const [eventId, setEventId] = useState(null);

  const [artistOptions, setArtistOptions] = useState<any>([]);
  const [venueOptions, setVenueOptions] = useState<any>();

  const {
    calendarData: { isLoading, data: events },
  }: {
    calendarData: CalendarState;
  } = useSelector((state: RootState) => ({
    calendarData: state.calendarData,
  }));

  const {
    general: { currentLocation },
  }: {
    general: GeneralState;
  } = useSelector((state: RootState) => ({
    general: state.general,
  }));

  // const {
  //   artistList: { data: artistListData },
  // }: {
  //   artistList: SeledtedArtistState;
  // } = useSelector((state: RootState) => ({
  //   artistList: state.artistList,
  // }));

  const dispatch = useDispatch();
  const venueApiParam = useList({
    queryParams: {
      search: "",
    },
  });

  const artistApiParam = useList({
    queryParams: {
      search: "",
    },
  });

  const { apiParam } = useList({
    queryParams: {
      calendarQuery: {
        endDate: DateTime.utc().endOf("month").toFormat("yyyy-MM-dd"),
        startDate: DateTime.utc().startOf("month").toFormat("yyyy-MM-dd"),
        upcoming: true,
        notes: true,
        submitted: true,
        shows: true,
        open: true,
        isGodMode: false,
      },
    },
  });

  const handleGodMode = (d: any) => {
    setIsGodMode(!isGodMode);
  };

  const handleDateClick = (d: any) => {
    if (d?.date) {
      const currentDate = DateTime.utc().startOf("day").valueOf();
      if (currentDate < DateTime.fromJSDate(d.date).endOf("day").valueOf()) {
        setClickedDate(d.date);
        setEventId(null);
        handlePosition(d);
        setIsNoteModalOpen(true);
      }
    }
  };

  const handleDatesSet = (d: any) => {
    if (d?.view?.currentStart && apiParam.calendarQuery) {
      apiParam.calendarQuery.endDate = DateTime.fromJSDate(d.end).toFormat(
        "yyyy-MM-dd"
      );
      apiParam.calendarQuery.startDate = DateTime.fromJSDate(d.start).toFormat(
        "yyyy-MM-dd"
      );

      if (isFirstTimeLoaded) {
        dispatch(fetchAdminCalendar(apiParam));
      }
      // setCurrentMonth(
      //   DateTime.fromJSDate(d.view.currentStart).toFormat("MMMM yyyy")
      // );

      if (calendarRef.current) {
        const calendarApi = calendarRef.current.getApi();
        const currentMonth = calendarApi.view.title;
        setCurrentMonth(currentMonth);
        setCurrentStart(currentMonth);
      }
    }
  };

  const handleEventContent = (arg: any) => {
    let title = arg.event.title;
    let description = "";
    if (arg.event.extendedProps?.className === "show") {
      title = `Show @ ${arg.event.extendedProps?.city}`;
      description = `ft. ${arg.event.extendedProps?.artistName}`;
    } else if (arg.event.extendedProps?.className === "posting") {
      title = `Open posting : ${arg.event.title}`;
      description = `@ (${
        arg.event.extendedProps?.venueName
          ? arg.event.extendedProps?.venueName
          : ""
      })`;
    } else if (
      arg.event.extendedProps?.className === "completedEvent" ||
      arg.event.extendedProps?.className === "confirmedEvent" ||
      arg.event.extendedProps?.className === "canceledEvent"
    ) {
      title = `PRO Booking: ${arg.event.title}`;
      description = `PRO Booking: ft. ${arg.event.extendedProps?.artistName}`;
    } else if (arg.event.extendedProps?.className === "note") {
      title = `Note: ${arg.event.title}`;
    }

    return (
      <div
        className={`${calendarStyles.event} event-type-${arg.event.extendedProps?.className}`}
      >
        <div>
          <span>{title}</span>
        </div>
        <div>
          <span>
            <strong>
              {arg.event.extendedProps?.className === "note" ? (
                ""
              ) : (
                // DateTime.fromJSDate(arg.event.start)
                //   .toUTC()
                //   .toFormat("hh:mm a")
                <div>
                  <CalendarClock />{" "}
                  {DateTime.fromJSDate(arg.event.start)
                    .toUTC()
                    .toFormat("hh:mm a")}
                </div>
              )}
            </strong>
          </span>{" "}
        </div>
        {/* {description ? <div>{description}</div> : ""} */}
      </div>
    );
  };

  const handlePrevClick = () => {
    if (calendarRef?.current) {
      calendarRef.current?.getApi()?.prev();
    }
  };

  const handleNextClick = () => {
    if (calendarRef?.current) {
      calendarRef.current?.getApi()?.next();
    }
  };

  const handleTodayClick = () => {
    if (calendarRef?.current) {
      calendarRef.current?.getApi()?.today();
    }
  };

  const dayCellContent = (d: any) => {
    return <div>{DateTime.fromJSDate(d.date).toUTC().toFormat("dd")}</div>;
  };

  const dayHeaderContent = (d: any) => {
    let fullDayName: any = "";
    if (calendarView === "dayGridMonth" || calendarView === "timeGridWeek") {
      fullDayName =
        screenResolution.width <= 1024
          ? DateTime.fromJSDate(d.date).toFormat("EEE")
          : DateTime.fromJSDate(d.date).toFormat("cccc");
    } else {
      fullDayName =
        screenResolution.width <= 1024
          ? DateTime.fromJSDate(d.date).toFormat("MMMM d y")
          : DateTime.fromJSDate(d.date).toFormat("MMMM d y");
    }
    return <div>{fullDayName}</div>;
  };

  const handleEventClick = (d: any) => {
    handlePosition(d);
    if (d.event?.extendedProps) {
      const id = d.event.extendedProps.id;
      setEventId(id);
      const eventType = d.event.extendedProps.className;
      if (eventType === "show") {
        setIsShowModalOpen(true);
      } else if (eventType === "posting") {
        setIsPostingModalOpen(true);
      } else if (eventType === "note") {
        setClickedDate(d.event.start);
        setIsNoteModalOpen(true);
      } else {
        setIsEventModalOpen(true);
      }
    }
  };

  const handlePosition = (d: any) => {
    // if (window.innerWidth > 1024) {
    //   const { clientX, clientY } = d.jsEvent;
    //   // Get the client's screen width and height
    //   const screenWidth = window.innerWidth;
    //   const screenHeight = window.innerHeight;
    //   // Calculate the desired left position of the modal
    //   let desiredLeft = clientX + 150;
    //   // Adjust the left position if the modal would exceed the right edge of the screen
    //   const modalWidth = 500; // Adjust the width of the modal as needed
    //   if (clientX + modalWidth > screenWidth) {
    //     desiredLeft = (screenWidth - modalWidth) - 150;
    //   }
    //   // Calculate the desired top position of the modal
    //   let desiredTop = clientY - 250;
    //   // Adjust the top position if the modal would exceed the bottom edge of the screen
    //   const modalHeight = 500; // Adjust the height of the modal as needed
    //   if (clientY +  modalHeight > screenHeight) {
    //     desiredTop = clientY - modalHeight;
    //   }
    //   setModalPosition({ top: desiredTop, left: desiredLeft });
    // } else {
    //   setModalPosition({ top: 50, left: 20 });
    // }
  };

  const handlePreviousMounthInvoice = async () => {
    setIsExportLoading(true);
    try {
      await getPreviousMonthInvoiceData();
      showToast(SUCCESS_MESSAGES.previousMonthInvoiceExportSuccess, "success");
      setIsExportLoading(false);
    } catch (error) {
      console.log(error);
      setIsExportLoading(false);
    }
  };

  const handleCurrentMounthInvoice = async () => {
    setIsExportLoading(true);
    try {
      await getNextMonthInvoiceData();
      showToast(SUCCESS_MESSAGES.currentMonthInvoiceExportSuccess, "success");
      setIsExportLoading(false);
    } catch (error) {
      console.log(error);
      setIsExportLoading(false);
    }
  };

  const handleApprovedArtistInvoice = async () => {
    setIsExportLoading(true);
    try {
      await getApprovedArtistsData();
      showToast(
        SUCCESS_MESSAGES.approvedArtistsInvoiceExportSuccess,
        "success"
      );
      setIsExportLoading(false);
    } catch (error) {
      console.log(error);
      setIsExportLoading(false);
    }
  };

  const handleAllUsersInvoice = async () => {
    setIsExportLoading(true);
    try {
      await getExportUsersData();
      showToast(SUCCESS_MESSAGES.allUsersInvoiceExportSuccess, "success");
      setIsExportLoading(false);
    } catch (error) {
      console.log(error);
      setIsExportLoading(false);
    }
  };

  const handleApprovedVenues = async () => {
    setIsExportLoading(true);
    try {
      await getApprovedVenuesData();
      showToast(SUCCESS_MESSAGES.approvedVenuesExportSuccess, "success");
      setIsExportLoading(false);
    } catch (error) {
      console.log(error);
      setIsExportLoading(false);
    }
  };

  const handleValidEventsInvoice = async () => {
    setIsExportLoading(true);
    try {
      await getValidEventsData();
      showToast(SUCCESS_MESSAGES.validEventsExportSuccess, "success");
      setIsExportLoading(false);
    } catch (error) {
      console.log(error);
      setIsExportLoading(false);
    }
  };

  const getCalendarByArtistVenue = () => {
    if (apiParam) {
      dispatch(fetchAdminCalendar(apiParam));
    }
  };

  const handleArtistChange = (data: any) => {
    if (data && apiParam?.calendarQuery) {
      apiParam.calendarQuery.artistId = data.value;
    } else {
      delete apiParam?.calendarQuery?.artistId;
    }
    getCalendarByArtistVenue();
  };

  const handleVenueChange = (data: any) => {
    if (data && apiParam?.calendarQuery) {
      apiParam.calendarQuery.venueId = data.value;
    } else {
      delete apiParam?.calendarQuery?.venueId;
    }
    getCalendarByArtistVenue();
  };

  const getVenue = async () => {
    try {
      let response = await getVenuesSearchAPI(venueApiParam.apiParam);
      setVenueOptions(response);
    } catch (error) {
      console.log(error);
    }
  };

  const handleVenueInputChange = (data: any) => {
    if (data) {
      venueApiParam.apiParam.search = data;
      getVenue();
    }
  };

  const getArtist = async () => {
    try {
      const response = await generalSearchArtistsAPI(artistApiParam.apiParam);
      setArtistOptions(response);
    } catch (error) {
      console.log(error);
    }
  };

  const handleArtistInputChange = (data: any) => {
    if (data) {
      artistApiParam.apiParam.search = data;
      getArtist();
    }
  };

  useEffect(() => {
    if (calendarRef?.current) {
      calendarRef.current.getApi().changeView(calendarView);
    }
  }, [calendarView]);

  useEffect(() => {
    if (filters.length > 0) {
      filters.map((option) => {
        option?.options?.map((suboption) => {
          if (suboption.value === "upcoming" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["upcoming"] = true;
            } else {
              apiParam.calendarQuery["upcoming"] = false;
            }
          }

          if (suboption.value === "completed" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["completed"] = true;
            } else {
              apiParam.calendarQuery["completed"] = false;
            }
          }

          if (suboption.value === "canceledByHost" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["canceledByHost"] = true;
            } else {
              apiParam.calendarQuery["canceledByHost"] = false;
            }
          }

          if (suboption.value === "removed" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["removed"] = true;
            } else {
              apiParam.calendarQuery["removed"] = false;
            }
          }

          // if (suboption.value === "paid" && apiParam.calendarQuery) {
          //     if (suboption.selected) {
          //         apiParam.calendarQuery["paid"] = true;
          //     } else {
          //         apiParam.calendarQuery["paid"] = false;
          //     }
          // }
          // if (suboption.value === "unpaid" && apiParam.calendarQuery) {
          //     if (suboption.selected) {
          //         apiParam.calendarQuery["unpaid"] = true;
          //     } else {
          //         apiParam.calendarQuery["unpaid"] = false;
          //     }
          // }

          if (suboption.value === "open" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["open"] = true;
            } else {
              apiParam.calendarQuery["open"] = false;
            }
          }

          if (suboption.value === "submitted" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["submitted"] = true;
            } else {
              apiParam.calendarQuery["submitted"] = false;
            }
          }

          if (suboption.value === "nearMe" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["nearMe"] = true;
              apiParam.calendarQuery["latitude"] = currentLocation.Latitude;
              apiParam.calendarQuery["longitude"] = currentLocation.Longitude;
            } else {
              apiParam.calendarQuery["nearMe"] = false;
              apiParam.calendarQuery["latitude"] = undefined;
              apiParam.calendarQuery["longitude"] = undefined;
            }
          }

          if (suboption.value === "all" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["all"] = true;
            } else {
              apiParam.calendarQuery["all"] = false;
            }
          }

          if (suboption.value === "notes" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["notes"] = true;
            } else {
              apiParam.calendarQuery["notes"] = false;
            }
          }

          if (suboption.value === "shows" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["shows"] = true;
            } else {
              apiParam.calendarQuery["shows"] = false;
            }
          }

          if (suboption.value === "declined" && apiParam.calendarQuery) {
            if (suboption.selected) {
              apiParam.calendarQuery["declined"] = true;
            } else {
              apiParam.calendarQuery["declined"] = false;
            }
          }
        });
      });
      dispatch(fetchAdminCalendar(apiParam));
      setIsFirstTimeLoaded(true);
    }
  }, [filters]);

  useEffect(() => {
    if (events?.calendarData && Array.isArray(events.calendarData)) {
      const tmpEvents: any = [];
      events.calendarData.map((event: eventObject) => {
        tmpEvents.push({
          start: event.start,
          end: event.end,
          title: event.title,
          extendedProps: {
            ...event,
          },
        });
      });

      setAllEvents(tmpEvents);
    }
  }, [events]);

  useEffect(() => {
    if (calendarExport) {
      if (calendarExport === "PreviousMonthInvoice") {
        handlePreviousMounthInvoice();
      } else if (calendarExport === "CurrentMonthInvoice") {
        handleCurrentMounthInvoice();
      } else if (calendarExport === "ApprovedArtistInvoice") {
        handleApprovedArtistInvoice();
      } else if (calendarExport === "AllUsersInvoice") {
        handleAllUsersInvoice();
      } else if (calendarExport === "ApprovedVenues") {
        handleApprovedVenues();
      } else if (calendarExport === "ValidEventsInvoice") {
        handleApprovedVenues();
      }
    }
  }, [calendarExport]);

  useEffect(() => {
    getVenue();
  }, [venueApiParam.apiParam]);

  useEffect(() => {
    getArtist();
  }, [artistApiParam.apiParam]);

  // useEffect(() => {
  //   if (artistListData && artistListData.length > 0) {
  //     let tempData: any = [];
  //     artistListData?.map((item: any) => {
  //       tempData.push({
  //         value: item.objectId,
  //         label: item.name,
  //       });
  //     });
  //     setArtistOptions(tempData);
  //   }
  // }, [artistListData]);

  useEffect(() => {
    if (currentStart) {
      const handleMonthlySummaryAPI = async () => {
        try {
          const payload = {
            date: currentStart,
            isGodMode: isGodMode,
          };
          const res = await getMonthlySummaryAPI(payload);
          setMonthlySummaryData(res);
        } catch (error) {
          console.log(error);
        }
      };

      handleMonthlySummaryAPI();
    }
  }, [currentStart, isGodMode]);

  useEffect(() => {
    if (isFirstTimeLoaded && apiParam?.calendarQuery) {
      if (isGodMode) {
        apiParam.calendarQuery["isGodMode"] = true;
      } else {
        apiParam.calendarQuery["isGodMode"] = false;
      }

      dispatch(fetchAdminCalendar(apiParam));
    }
  }, [isGodMode]);

  useEffect(() => {
    if (calendarRef.current) {
      const calendarApi = calendarRef.current.getApi();
      const currentMonth = calendarApi.view.title;
      setCurrentMonth(currentMonth);
      setCurrentStart(currentMonth);
    }

    const handleResize = () => {
      setScreenResolution({
        width: window.innerWidth,
        height: window.innerHeight,
      });

      if (calendarRef.current && window.innerWidth <= 1024) {
        const calendarApi = calendarRef.current.getApi();
        const calendarWrapper = calendarApi.el.closest(".fc");
        let calendarHeight =
          window.innerHeight - calendarWrapper.offsetTop - 200;

        if (calendarHeight < 450) {
          calendarHeight = 450;
        }

        calendarApi.setOption("contentHeight", calendarHeight);
      } else if (calendarRef.current) {
        const calendarApi = calendarRef.current.getApi();
        calendarApi.setOption("contentHeight", "125vh");
      }
    };

    // Add event listener for window resize
    window.addEventListener("resize", handleResize);

    // Initial screen resolution
    setScreenResolution({
      width: window.innerWidth,
      height: window.innerHeight,
    });

    const resizeObserver = new ResizeObserver(handleResize);

    if (pageRef.current) {
      resizeObserver.observe(pageRef.current);
    }

    // Cleanup event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
      resizeObserver.disconnect();
    };
  }, []);

  return (
    <div ref={pageRef} style={{ position: "relative" }}>
      <Script src="https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js" />
      {isLoading && <DefaultSkeleton />}
      {isExportLoading && <DefaultSkeleton />}
      <Show when={["admin_calendar"]} fallback={<NoPermissionsComponent />}>
        <AdminCalendarScane
          {...{
            handleDateClick,
            handleDatesSet,
            allEvents,
            handleEventContent,
            calendarRef,
            handlePrevClick,
            handleNextClick,
            handleTodayClick,
            calendarView,
            setCalendarView,
            dayCellContent,
            dayHeaderContent,
            currentMonth,
            handleEventClick,
            filters,
            setFilters,
            setIsNewEventModalOpen,
            handlePreviousMounthInvoice,
            handleCurrentMounthInvoice,
            handleApprovedArtistInvoice,
            handleAllUsersInvoice,
            handleApprovedVenues,
            handleValidEventsInvoice,
            setCalendarExport,
            artistOptions,
            venueOptions,
            handleArtistChange,
            handleVenueInputChange,
            handleArtistInputChange,
            handleVenueChange,
            monthlySummaryData,
            handleGodMode,
            isGodMode,
          }}
        />
      </Show>

      {isEventModalOpen && (
        <AdminCalendarEventModal
          {...{
            isOpen: isEventModalOpen,
            setIsEventModalOpen,
            id: eventId,
            setIsExportLoading,
            handleClose: (e) => {
              dispatch(fetchAdminCalendar(apiParam));
            },
          }}
        />
      )}

      {isNoteModalOpen && (
        <CalendarAddNoteModal
          {...{
            isOpen: isNoteModalOpen,
            setIsNoteModalOpen,
            id: eventId,
            date: clcikedDate,
            modalPosition,
            handleClose: (e) => {
              dispatch(fetchAdminCalendar(apiParam));
            },
          }}
        />
      )}

      {isPostModalOpen && eventId && (
        <AdminCalendarPostModal
          {...{
            isOpen: isPostModalOpen,
            setIsPostingModalOpen,
            id: eventId,
            handleClose: (e) => {
              dispatch(fetchAdminCalendar(apiParam));
            },
          }}
        />
      )}

      {isShowModalOpen && (
        <CalendarShowsModal
          {...{
            isOpen: isShowModalOpen,
            setIsShowModalOpen,
            id: eventId,
            handleClose: (e) => {
              dispatch(fetchAdminCalendar(apiParam));
            },
          }}
        />
      )}

      {isNewEventModalOpen && (
        <AdminAddEventModal
          {...{
            isOpen: isNewEventModalOpen,
            setIsNewEventModalOpen,
            handleClose: (e) => {
              dispatch(fetchAdminCalendar(apiParam));
            },
          }}
        />
      )}
    </div>
  );
};

AdminCalendarContainer.Layout = AdminLayoutComponent;
export default AdminCalendarContainer;
