import CustomDropdown from "@components/theme/dropdown";
import FilterDropdown, {
  FilterDropdownTypes,
} from "@components/theme/dropdown/filter";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import FullCalendar from "@fullcalendar/react";
import timeGridPlugin from "@fullcalendar/timegrid";
import CalendarStyles from "./calendarContainer.module.scss";
import Autocomplete, { usePlacesWidget } from "react-google-autocomplete";
import { Divider } from "antd";
import ExportCustomDropdown from "@components/theme/dropdown/exportDropdown";
import Select from "react-select";
import { Button } from "@components/theme";
import Image from "next/image";

export interface AdminCalendarScaneProps {
  handleDateClick: (d: any) => void;
  handleDatesSet: (d: any) => void;
  allEvents: any[];
  handleEventContent: (d: any) => void;
  calendarRef: any;
  handlePrevClick: (d: any) => void;
  handleNextClick: (d: any) => void;
  handleTodayClick: (d: any) => void;
  calendarView:
    | "dayGridMonth"
    | "timeGridWeek"
    | "timeGridDay"
    | "listMonth"
    | "listWeek"
    | "listDay";
  setCalendarView: (d: any) => void;
  dayCellContent: (d: any) => void;
  dayHeaderContent: (d: any) => void;
  currentMonth: string;
  handleEventClick: (d: any) => void;
  filters: FilterDropdownTypes[];
  setFilters: (d: FilterDropdownTypes[]) => void;
  setIsNewEventModalOpen: (d: any) => void;
  handlePreviousMounthInvoice: (d: any) => void;
  handleCurrentMounthInvoice: (d: any) => void;
  handleApprovedArtistInvoice: (d: any) => void;
  handleAllUsersInvoice: (d: any) => void;
  handleApprovedVenues: (d: any) => void;
  handleValidEventsInvoice: (d: any) => void;
  setCalendarExport: (d: any) => void;
  artistOptions: any;
  venueOptions: any;
  handleArtistChange: (d: any) => void;
  handleVenueChange: (d: any) => void;
  handleVenueInputChange: (d: any) => void;
  handleArtistInputChange: (d: any) => void;
  monthlySummaryData: any;
  handleGodMode: (d: any) => void;
  isGodMode: boolean;
}

const AdminCalendarScane = (props: AdminCalendarScaneProps) => {
  const {
    handleDateClick,
    handleDatesSet,
    allEvents,
    handleEventContent,
    calendarRef,
    handlePrevClick,
    handleNextClick,
    handleTodayClick,
    calendarView,
    setCalendarView,
    dayCellContent,
    dayHeaderContent,
    currentMonth,
    handleEventClick,
    filters,
    setFilters,
    setIsNewEventModalOpen,
    setCalendarExport,
    artistOptions,
    venueOptions,
    handleArtistChange,
    handleVenueInputChange,
    handleArtistInputChange,
    handleVenueChange,
    monthlySummaryData,
    handleGodMode,
    isGodMode,
  } = props;

  return (
    <>
      <div className="message-page h-100vh">
        <div className={`panel-inner-content ${CalendarStyles.calendar_page}`}>
          <div className="card card2 border-0">
            <div className="card-body p-0 pb-4">
              <div className={`${CalendarStyles.calendar_heading}`}>
                <div>
                  <Button
                    htmlType="button"
                    type={isGodMode ? "ghost" : "primary"}
                    className="w-100 mb-3"
                    onClick={handleGodMode}
                  >
                    {isGodMode ? "Deactivate" : "Activate"} God Mode
                  </Button>
                </div>

                {monthlySummaryData && (
                  <div className="filter-4">
                    <div>
                      <label>
                        MONTHLY TOTAL REVENUE:
                        <span>
                          <strong>
                            ${monthlySummaryData.month_total_revenue || 0}
                          </strong>
                        </span>
                      </label>
                    </div>
                    <div>
                      <label>
                        NET REVENUE:
                        <span>
                          <strong>
                            ${monthlySummaryData.net_revenue || 0}
                          </strong>
                        </span>
                      </label>
                    </div>
                    <div>
                      <label>
                        NUMBER OF TOTAL GIGS:
                        <span>
                          <strong>
                            {monthlySummaryData.number_of_total_gigs || 0}
                          </strong>
                        </span>
                      </label>
                    </div>
                  </div>
                )}
                <div className="filter-1">
                  <div className="schedule">
                    <label>Schedule :</label>
                    <CustomDropdown
                      onChange={setCalendarView}
                      defaultValue={calendarView}
                      isShowCheckBox={true}
                      options={[
                        { value: "dayGridMonth", label: "Month" },
                        { value: "timeGridWeek", label: "Week" },
                        { value: "timeGridDay", label: "Day" },
                        { value: "listMonth", label: "List(Mo)" },
                        { value: "listWeek", label: "List(Wk)" },
                        { value: "listDay", label: "List(day)" },
                      ]}
                    />
                  </div>
                  <div className="calendar-info">
                    <div className="actions">
                      <button type="button" onClick={handlePrevClick}>
                        <Image
                          src="/images/general/left.svg"
                          alt="previous"
                          width={6}
                          height={11}
                        />
                      </button>
                      <div>
                        <Image
                          src="/images/general/calendar.svg"
                          alt="Calendar"
                          width={22}
                          height={19}
                        />
                        <div>{currentMonth}</div>
                      </div>
                      <button type="button" onClick={handleNextClick}>
                        <Image
                          src="/images/general/right.svg"
                          alt="Next"
                          width={6}
                          height={11}
                        />
                      </button>
                    </div>
                    <div className="today">
                      <span></span>
                      <button type="button" onClick={handleTodayClick}>
                        Today
                      </button>
                    </div>
                  </div>
                  <div className="calendar-filter">
                    <div className="schedule">
                      <ExportCustomDropdown
                        onChange={setCalendarExport}
                        placeholder="Export"
                        isShowCheckBox={true}
                        options={[
                          {
                            value: "PreviousMonthInvoice",
                            label: "Previous Month Invoice",
                          },
                          {
                            value: "CurrentMonthInvoice",
                            label: "Current Month Invoice",
                          },
                          {
                            value: "ApprovedArtistInvoice",
                            label: "Approved Artist Invoice",
                          },
                          {
                            value: "AllUsersInvoice",
                            label: "All Users Invoice",
                          },
                          {
                            value: "ApprovedVenues",
                            label: "Approved Venues Invoice",
                          },
                          {
                            value: "ValidEventsInvoice",
                            label: "Valid Events Invoice",
                          },
                        ]}
                      />
                    </div>
                    <div>
                      <FilterDropdown
                        onChange={setFilters}
                        placeholder="Filter calendar items"
                        options={filters}
                      />
                    </div>
                  </div>
                </div>
                <div className="mobile_filter">
                  <div className="schedule">
                    <label>Schedule :</label>
                    <CustomDropdown
                      onChange={setCalendarView}
                      defaultValue={calendarView}
                      isShowCheckBox={true}
                      options={[
                        { value: "dayGridMonth", label: "Month" },
                        { value: "timeGridWeek", label: "Week" },
                        { value: "timeGridDay", label: "Day" },
                        { value: "listMonth", label: "List(Mo)" },
                        { value: "listWeek", label: "List(Wk)" },
                        { value: "listDay", label: "List(day)" },
                      ]}
                    />
                  </div>
                  <div className="calendar-filter">
                    <div>
                      <FilterDropdown
                        onChange={setFilters}
                        placeholder="Filter calendar items"
                        options={filters}
                      />
                    </div>
                  </div>
                </div>
                <div className="filter-2">
                  <label>Filter by :</label>
                  <Select
                    className={`basic_multi_select selectAVField`}
                    placeholder="Select an Artist"
                    classNamePrefix="select"
                    isClearable={true}
                    options={artistOptions}
                    onChange={handleArtistChange}
                    onInputChange={(e) => handleArtistInputChange(e)}
                    instanceId="artist"
                  />
                  <Select
                    className={`basic_multi_select selectAVField`}
                    placeholder="Select an Venue"
                    classNamePrefix="select"
                    isClearable={true}
                    options={venueOptions}
                    onChange={handleVenueChange}
                    onInputChange={(e) => handleVenueInputChange(e)}
                    instanceId="venue"
                  />
                  <Button
                    htmlType="button"
                    onClick={() => {
                      setIsNewEventModalOpen(true);
                    }}
                  >
                    + Create an Event
                  </Button>
                </div>
              </div>
              <div className="pt-4">
                <FullCalendar
                  viewClassNames={`${CalendarStyles.calendar_view} ${
                    isGodMode ? CalendarStyles.calendar_god_mode : ""
                  }`}
                  dayCellClassNames={CalendarStyles.calendar_day_cell}
                  ref={calendarRef}
                  plugins={[
                    dayGridPlugin,
                    timeGridPlugin,
                    interactionPlugin,
                    listPlugin,
                  ]}
                  dateClick={handleDateClick}
                  datesSet={handleDatesSet}
                  events={allEvents}
                  eventContent={handleEventContent}
                  dayMaxEvents={true}
                  dayCellContent={dayCellContent}
                  dayHeaderContent={dayHeaderContent}
                  headerToolbar={false}
                  eventClick={handleEventClick}
                  timeZone={"utc"}
                  firstDay={1}
                  slotLabelInterval={{ minutes: 15 }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AdminCalendarScane;
