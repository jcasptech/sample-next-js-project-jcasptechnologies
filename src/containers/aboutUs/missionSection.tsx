import Image from "next/image";
import { useRouter } from "next/router";
import aboutUsStyle from "./aboutUsStyle.module.scss";

const MissionSection = () => {
  const router = useRouter();

  const handleSignUp = () => {
    router.push("/login");
  };

  return (
    <>
      <section className="section-pad pt-0">
        <div className={`container-fluid ${aboutUsStyle.about_us_mission}`}>
          <div className="blue-l">
            <Image
              src="/images/about-us/blue-a.png"
              alt=""
              width={203}
              height={394}
            />
          </div>
          <div className="row m0">
            <div className="col-2"> </div>
            <div
              className="col-md-4 mission"
              data-aos="fade-up"
              data-aos-delay="100"
              data-aos-duration="1200"
            >
              <Image
                src="/images/about-us/target-a.png"
                alt=""
                width={102}
                height={93}
              />
              <h2>Our Mission</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                vulputate libero et velit interdum, ac aliquet odio mattis.
                className aptent taciti sociosqu ad litora torquent per conubia
                nostra, per inceptos himenaeos. Curabitur tempus urna at turpis
                condimentum lobortis. Ut commodo efficitur neque.{" "}
              </p>
              <div className="rectangle">
                <hr />
              </div>
              <hr />
            </div>
            <div
              className="col-md-4 vision"
              data-aos="fade-up"
              data-aos-delay="100"
              data-aos-duration="1200"
            >
              <Image
                src="/images/about-us/vision-a.png"
                alt=""
                width={102}
                height={93}
              />
              <h2>Our Vission</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                vulputate libero et velit interdum, ac aliquet odio mattis.
                className aptent taciti sociosqu ad litora torquent per conubia
                nostra, per inceptos himenaeos. Curabitur tempus urna at turpis
                condimentum lobortis. Ut commodo efficitur neque.{" "}
              </p>
              <div className="rectangle">
                <hr />
              </div>
              <hr />
            </div>
          </div>
          <div className="beige-r">
            <Image
              src="/images/about-us/beige.png"
              alt=""
              width={176}
              height={174}
            />
          </div>
        </div>
      </section>
      <section
        className={`section-pad  ${aboutUsStyle.back_g}`}
        data-aos="fade-up"
        data-aos-delay="100"
        data-aos-duration="1200"
      >
        <div className="container-fluid">
          <div className="curious">
            <div className="row m-0 ">
              <div className="col-md-7">
                <h2>Curious about Gig Town’s platform</h2>
                <p>
                  Dictum est a, mattis tellus. Sed dignissim, metus nec
                  fringilla accumsan, risus sem sollicitudin lacus, ut interdum
                  tellus elit sed risus. Maecenas eget condimentum velit, sit
                  amet feugiat lectus. Class aptent taciti sociosqu.
                </p>
              </div>
              <div className="col-md-3 col-sm-0"></div>
              <div className="col-md-2 col-sm-3 ">
                <div className="  mob-ctr about-btn ">
                  <input
                    type="button"
                    onClick={handleSignUp}
                    className="button hvr-float-shadow"
                    value="Sign In"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default MissionSection;
