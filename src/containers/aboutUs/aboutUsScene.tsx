import NewsLetterSection from "@components/newsLetter";
import HelpingArtistSection from "./helpingArtistSection";
import aboutUsStyle from "./aboutUsStyle.module.scss";
import ChangingThinkAboutSection from "./changingSection";
import MissionSection from "./missionSection";
import AboutStorySection from "./storySection";

export interface AboutUsPageSceneProps {
  aboutUsData: any;
}
const AboutUsPageScene = (props: AboutUsPageSceneProps) => {
  const { aboutUsData } = props;
  return (
    <>
      <section className={`section-pad ${aboutUsStyle.pageHeader}`}>
        <div className="container-fluid">
          <div className="row m-0">
            <div className="col-12 pHead">
              <h1>About JCasp</h1>
            </div>
          </div>
        </div>
      </section>
      <HelpingArtistSection />
      <ChangingThinkAboutSection
        {...{
          aboutUsData,
        }}
      />
      <MissionSection />
      <AboutStorySection />
      <NewsLetterSection />
    </>
  );
};
export default AboutUsPageScene;
