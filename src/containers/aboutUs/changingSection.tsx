import Image from "next/image";
import { getFormatReview } from "src/libs/helpers";
import aboutUsStyle from "./aboutUsStyle.module.scss";
export interface ChangingThinkAboutSectionProps {
  aboutUsData: any;
}

const ChangingThinkAboutSection = (props: ChangingThinkAboutSectionProps) => {
  const { aboutUsData } = props;
  return (
    <>
      <section className={`"section-pad ${aboutUsStyle.colored}`}>
        <div className="container-fluid about-us-second ">
          <div className="row m0">
            <div className="col-md-6 a-heading">
              <h2
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                We’re changing the way people think about Gig
              </h2>
            </div>
            <div className="col-md-6 aos-init aos-animate a-para">
              <p
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                eu turpis molestie, dictum est a, mattis tellus. Sed dignissim,
                metus nec fringilla accumsan, risus sem sollicitudin lacus, ut
                interdum tellus elit sed risus. Maecenas eget condimentum velit,
                sit amet feugiat lectus. className aptent taciti sociosqu ad
                litora torquent per conubia nostra, per inceptos himenaeos.
                Praesent auctor purus luctus enim egestas, ac scelerisque ante
                pulvinar.
              </p>
              <p
                data-aos="fade-down"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {" "}
                Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor
                urna. Curabitur vel bibendum lorem. Morbi convallis convallis
                diam sit amet lacinia. Aliquam in elementum tellus.className
                aptent taciti sociosqu ad litora torquent per conubia nostra,
                per inceptos himenaeos. Praesent auctor purus luctus enim
                egestas, ac scelerisque ante pulvinar{" "}
              </p>
            </div>
            <hr />
            <div className="container">
              <div className="row card-deck">
                {/* <!--
                                    <div className="col-md-4 col-sm-10 col-xs-12 ">
                                        <div className="card">
                                            <img className="card-img" src="/images/about-us/flower.png" alt="Bologna">
                                                <div className="card-img-overlay  d-flex flex-column justify-content-end">
                                                    <h1 className="card-title">500+</h1>

                                                    <p className="card-text">Registered Venue, when arranges
                                                        Gig’s successfully. </p>
                                                </div>
       
        </div>
                                        </div> --> */}
                <div className="col-md-4 col-sm-12 col-xs-12 ">
                  <div className="card">
                    <div className="d-flex">
                      <div className="flex-size-1">&nbsp;</div>
                      <div className="flex-size-1">
                        <Image
                          className=""
                          src="/images/about-us/flower.png"
                          alt="Card image cap"
                          width={120}
                          height={121}
                        />
                      </div>
                    </div>
                    <div className="card-body">
                      <h2 className="card-title">
                        {getFormatReview(
                          aboutUsData && aboutUsData?.venueCount
                        ) || 0}
                        +
                      </h2>
                      <div className="a-design-l">
                        <Image
                          src="/images/about-us/orange-dots.png"
                          alt=""
                          width={40}
                          height={39}
                        />
                      </div>
                      <p className="card-text">
                        Registered Venue, when arranges Gig’s successfully.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 col-sm-12 col-xs-12">
                  <div className="card">
                    <div className="d-flex">
                      <div className="flex-size-1">&nbsp;</div>
                      <div className="flex-size-1">
                        <Image
                          className=""
                          src="/images/about-us/flower.png"
                          alt="Card image cap"
                          width={120}
                          height={121}
                        />
                      </div>
                    </div>
                    <div className="card-body">
                      <h2 className="card-title">
                        {getFormatReview(
                          aboutUsData && aboutUsData.artistCount
                        ) || 0}
                        +
                      </h2>
                      <p className="card-text">
                        Artist registered here, you can choose artist for your
                        gig.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 col-sm-12 col-xs-12 ">
                  <div className="card">
                    <div className="d-flex">
                      <div className="flex-size-1">&nbsp;</div>
                      <div className="flex-size-1">
                        <Image
                          className=""
                          src="/images/about-us/flower.png"
                          alt="Card image cap"
                          width={120}
                          height={121}
                        />
                      </div>
                    </div>
                    <div className="card-body">
                      <h2 className="card-title">
                        {getFormatReview(
                          aboutUsData && aboutUsData?.userCount
                        ) || 0}
                        +
                      </h2>
                      <p className="card-text">
                        {getFormatReview(
                          aboutUsData && aboutUsData?.userCount
                        ) || 0}{" "}
                        users choose Gig Town for their arrangement gigs.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="a-design-r">
                  <Image
                    src="/images/about-us/white-dots.png"
                    alt=""
                    width={40}
                    height={39}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className={aboutUsStyle.photoss}>
        <div className="row m0 box-f">
          <div className="col-md-2"></div>
          <div
            className="col-md-8 a-concert"
            data-aos="fade-down"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            {/* <img src="/images/about-us/concert.jpg" alt="" /> */}
            <Image
              src="/images/about-us/concert.jpg"
              alt=""
              width={966}
              height={520}
            />
          </div>
          <div className="col-2 white-dots">
            {/* <img src="/images/about-us/dots.png" alt="" /> */}
            <Image
              src="/images/about-us/dots.png"
              alt=""
              width={106}
              height={145}
            />
          </div>
        </div>
      </section>
    </>
  );
};
export default ChangingThinkAboutSection;
