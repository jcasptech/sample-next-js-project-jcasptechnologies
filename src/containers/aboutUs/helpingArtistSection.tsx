import aboutUsStyle from "./aboutUsStyle.module.scss";
import Link from "next/link";
import Image from "next/image";

export interface HelpingArtistSectionProps {}
const HelpingArtistSection = () => {
  return (
    <>
      <section className="section-pad">
        <div className={`container-fluid ${aboutUsStyle.about_us_top}`}>
          <div className="organize-gig-left">
            <Image
              src="/images/about-us/round-circles-blue.png"
              alt=""
              width={102}
              height={98}
            />
          </div>
          <div className="row m0">
            <div className="col-md-2 "></div>
            <div className="col-md-8 aos-init aos-animate about-us-head">
              <h2 data-aos="" data-aos-delay="100" data-aos-duration="1200">
                Helping Artist succeed through <br /> the power of JCasp
              </h2>
              <p data-aos="" data-aos-delay="100" data-aos-duration="1200">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                eu turpis molestie, dictum est a, mattis tellus. Sed dignissim,
                metus nec fringilla accumsan, risus sem sollicitudin lacus, ut
                interdum tellus elit sed risus. Maecenas eget condimentum velit,
                sit amet feugiat lectus. Class aptent taciti sociosqu ad litora
                torquent per conubia nostra, per inceptos himenaeos. Praesent
                auctor purus luctus enim egestas, ac scelerisque ante pulvinar.
                Donec ut rhoncus ex. Suspendisse ac rhoncus nisl, eu tempor
                urna. Curabitur vel bibendum lorem. Morbi convallis convallis
                diam sit amet lacinia. Aliquam in elementum tellus.className
                aptent taciti sociosqu ad litora torquent per conubia nostra,
                per inceptos himenaeos. Praesent auctor purus luctus enim
                egestas, ac scelerisque ante pulvinar{" "}
              </p>
              <div
                className="col-12 load_more mb-3 about-us-btn "
                data-aos=""
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                <Link href="/" className="hvr-float-shadow ">
                  Organize Gig
                </Link>
              </div>
            </div>
          </div>
          <div className="col-2"></div>
          {/* </div> */}
          <div className="organize-gig-right">
            <Image
              src="/images/about-us/round-circles-orange.png"
              alt=""
              width={128}
              height={194}
            />
            {/* <img src="/images/about-us/round-circles-orange.png" alt="" /> */}
          </div>
        </div>
      </section>
    </>
  );
};
export default HelpingArtistSection;
