import Image from "next/image";
import aboutUsStyle from "./aboutUsStyle.module.scss";

const AboutStorySection = () => {
  return (
    <>
      <section className={`section-pad ${aboutUsStyle.storySection}`}>
        <div className="container-fluid">
          <div className="row m0 ">
            <div className="col-12 col-sm-1 freckles">
              <Image
                src="/images/about-us/freckles.png"
                alt=""
                width={93}
                height={93}
              />
            </div>
            <div
              className="col-12 col-sm-5 blue-party "
              data-aos="fade-down"
              data-aos-delay="100"
              data-aos-duration="1200"
            >
              <Image
                src="/images/about-us/blue-party.png"
                alt=""
                width={763}
                height={697}
              />
            </div>
            <div className="col-12 col-sm-6 about-bottom">
              <h2
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                The Story of Gig Town
              </h2>
              <p
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                eu turpis molestie, dictum est a, mattis tellus. Sed dignissim,
                metus nec fringilla accumsan, risus sem sollicitudin lacus, ut
                interdum tellus elit sed risus. Maecenas eget condimentum velit,
                sit amet feugiat lectus. className aptent taciti sociosqu ad
                litora torquent per conubia nostra, per inceptos himenaeos.
                Praesent auctor purus luctus enim egestas, ac scelerisque ante
                pulvinar. Donec ut rhoncus ex
              </p>
              <p
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                Dictum est a, mattis tellus. Sed dignissim, metus nec fringilla
                accumsan, risus sem sollicitudin lacus, ut interdum tellus elit
                sed risus. Maecenas eget condimentum velit, sit amet feugiat
                lectus. className aptent taciti sociosqu
              </p>
              <p
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                {" "}
                fringilla accumsan, risus sem sollicitudin lacus, ut interdum
                tellus elit sed risus. Maecenas eget condimentum velit, sit amet
                feugiat lectus. className aptent taciti sociosqu ad litora
                torquent per conubia nostra, per inceptos himenaeos. Praesent
                auctor purus luctus enim egestas, ac scelerisque ante pulvinar.
                Donec ut rhoncus ex
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default AboutStorySection;
