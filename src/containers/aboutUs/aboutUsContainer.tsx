import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { aboutUsAPI } from "@redux/services/general.api";
import { useReCaptcha } from "next-recaptcha-v3";
import { useEffect, useState } from "react";
import AboutUsPageScene from "./aboutUsScene";

const AboutUsPageContainer = () => {
  const [aboutUsData, setAboutUsData] = useState<any>();
  const { executeRecaptcha, loaded } = useReCaptcha();
  let getData = async () => {
    try {
      const recaptchaResponse = await executeRecaptcha("about_us");
      let res = await aboutUsAPI(recaptchaResponse);
      setAboutUsData(res);
    } catch (error) {}
  };

  useEffect(() => {
    if (loaded) {
      getData();
    }
  }, [loaded]);

  return (
    <>
      <AboutUsPageScene
        {...{
          aboutUsData,
        }}
      />
    </>
  );
};

AboutUsPageContainer.Layout = MainLayoutComponent;

export default AboutUsPageContainer;
