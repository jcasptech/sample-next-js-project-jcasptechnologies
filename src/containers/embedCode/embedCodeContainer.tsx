import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import CreateEmbedcodeModal from "@components/modals/createEmbedcodeModal";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { deleteEmbedCodeAPI } from "@redux/services/embed-code.api";
import {
  EmbedcodeState,
  fetchEmbedCode,
  loadMoreEmbedcode,
} from "@redux/slices/embedCode";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import EmbedCodeScane from "./embedCodeScane";

const EmbedCodeContainer = () => {
  const dispatch = useDispatch();
  const [selectEventId, setSelectEventId] = useState("");

  const [isCreateEmbedcodeOpen, setIsCreateEmbedcodeOpen] = useState(false);

  const {
    embedCode: { isLoading, data: EmbedcodeData },
  }: {
    embedCode: EmbedcodeState;
  } = useSelector((state: RootState) => ({
    embedCode: state.embedCode,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 9,
      skip: 0,
      include: ["artist", "venue"],
    },
  });

  useEffect(() => {
    dispatch(fetchEmbedCode(apiParam));
  }, [apiParam]);

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 9;
    dispatch(loadMoreEmbedcode(apiParam));
  };

  const handleDeleteEmbedCode = async (data: any) => {
    if (data) {
      try {
        await deleteEmbedCodeAPI(data);
        showToast(SUCCESS_MESSAGES.embedCodeDeletedSuccess, "success");
        dispatch(fetchEmbedCode(apiParam));
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["embed_code"]} fallback={<NoPermissionsComponent />}>
        <EmbedCodeScane
          {...{
            EmbedcodeData,
            isLoading,
            handleLoadMore,
            handleDeleteEmbedCode,
            setIsCreateEmbedcodeOpen,
          }}
        />
      </Show>

      {isCreateEmbedcodeOpen && (
        <CreateEmbedcodeModal
          {...{
            isOpen: isCreateEmbedcodeOpen,
            setIsCreateEmbedcodeOpen,
            selectEventId,
            handleClose: (e) => {
              dispatch(fetchEmbedCode(apiParam));
            },
          }}
        />
      )}
    </>
  );
};

EmbedCodeContainer.Layout = AdminLayoutComponent;
export default EmbedCodeContainer;
