import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { showToast } from "@components/ToastContainer";
import {
  EmbedcodeDataResponse,
  EmbedcodeObject,
} from "@redux/slices/embedCode";
import { Popconfirm } from "antd";
import Clipboard from "clipboard";
import Image from "next/image";
import { useRouter } from "next/router";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import embedCodeStyles from "./embedCode.module.scss";

export interface EmbedCodeScaneProps {
  EmbedcodeData: EmbedcodeDataResponse | any;
  isLoading: boolean | undefined;
  handleLoadMore: (d: any) => void;
  handleDeleteEmbedCode: (d: any) => void;
  setIsCreateEmbedcodeOpen: (d: any) => void;
}

const EmbedCodeScane = (props: EmbedCodeScaneProps) => {
  const {
    EmbedcodeData,
    isLoading,
    handleLoadMore,
    handleDeleteEmbedCode,
    setIsCreateEmbedcodeOpen,
  } = props;

  const router = useRouter();
  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="row shows-venue-pad m0">
            <div className="register_btn btn-shows">
              <Button
                htmlType="button"
                type="primary"
                className={`float-right mb-3`}
                onClick={() => setIsCreateEmbedcodeOpen(true)}
              >
                Create New
              </Button>
            </div>
          </div>

          <div className="col-12 col-sm-12 col-lg-12 shows_holder ola">
            <div className="card card2 border-0">
              <div className="tab-content" id="tabcontent1">
                <div
                  className="tab-pane fade show active"
                  id="tabs-text-1"
                  role="tabpanel"
                >
                  <div className="row">
                    {EmbedcodeData &&
                      EmbedcodeData?.list &&
                      EmbedcodeData.list?.length > 0 &&
                      EmbedcodeData.list?.map(
                        (embedcode: EmbedcodeObject, index: number) => (
                          <div
                            key={index}
                            className={`col-sm-12 col-lg-4 ${embedCodeStyles.embedCodeCard}`}
                          >
                            <div
                              className="show_indi_card show_indi_card2"
                              // data-aos="fade-up"
                              // data-aos-delay="100"
                              // data-aos-duration="1200"
                            >
                              <div className="row m0 vcenter cardRows">
                                <div className="col-lg-4 col-md-4 col-sm-12 show_thumb">
                                  <Image
                                    src={
                                      embedcode?.category === "venue"
                                        ? embedcode?.venue?.iconImage?.url ||
                                          "/images/venue-placeholder.png"
                                        : embedcode?.artist?.iconImage?.url ||
                                          "/images/artist-placeholder.png"
                                    }
                                    alt={
                                      embedcode?.category === "venue"
                                        ? embedcode?.venue?.name
                                        : embedcode?.artist?.name
                                    }
                                    width={134}
                                    height={134}
                                  />
                                </div>
                                <div className=" col-lg-8  col-md-8 col-sm-12 show_meta show_meta2">
                                  <div className="calc mt">
                                    {embedcode?.category === "venue"
                                      ? "Venue"
                                      : "Artist"}
                                  </div>
                                  <div
                                    className="clock mt artname cursor-pointer"
                                    onClick={(e) => {
                                      e.preventDefault();
                                      {
                                        embedcode?.category === "venue"
                                          ? router.push(
                                              `/venue/${embedcode?.venue?.vanity}`
                                            )
                                          : router.push(
                                              `/artist/${embedcode?.artist?.vanity}`
                                            );
                                      }
                                    }}
                                  >
                                    {embedcode?.category === "venue"
                                      ? embedcode?.venue?.name
                                      : embedcode?.artist?.name}
                                  </div>
                                  <div className="title none-2">
                                    <div className="code-box">
                                      <div className="w-100">
                                        <div className="w-100 title-2">
                                          {embedcode?.embedCode || ""}
                                        </div>
                                        <div className="row">
                                          <div
                                            className="pull-right cursor-pointer col-3"
                                            onClick={(e: any) => {
                                              e.preventDefault();
                                              Clipboard.copy(
                                                `${embedcode?.embedCode}`
                                              );
                                              showToast(
                                                SUCCESS_MESSAGES.copyToClipboard,
                                                "success"
                                              );
                                            }}
                                          >
                                            <Image
                                              src="/images/general/copiedIcon.svg"
                                              alt="Copy"
                                              width={18}
                                              height={18}
                                              className="h-auto"
                                            />
                                          </div>
                                          <div className="col-6"></div>
                                          <div className="col-3 deleteButton">
                                            <Popconfirm
                                              title="Are you sure you want to delete?"
                                              okText="Yes"
                                              cancelText="No"
                                              onConfirm={() =>
                                                handleDeleteEmbedCode(
                                                  embedcode?.objectId
                                                )
                                              }
                                            >
                                              <Image
                                                src="/images/general/delete.png"
                                                alt="Delete"
                                                width={18}
                                                height={18}
                                                className={`hvr-float-shadow mt-1 cursor-pointer h-auto`}
                                              />
                                            </Popconfirm>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      )}
                    {EmbedcodeData?.list &&
                      EmbedcodeData?.list?.length <= 0 && (
                        <EmptyMessage description="No lists found." />
                      )}

                    {EmbedcodeData.hasMany && (
                      <div className="col-12 load_more">
                        <Button
                          type="ghost"
                          htmlType="button"
                          loading={isLoading}
                          disabled={isLoading}
                          onClick={(e) => {
                            e.preventDefault();
                            handleLoadMore({
                              loadMore: true,
                            });
                          }}
                        >
                          Load more Embed Code
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EmbedCodeScane;
