import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { getNewsLetterQuestionsData } from "@redux/services/export.api";
import {
  fetchQuestions,
  loadMoreQuestions,
  QuestionsState,
} from "@redux/slices/Questions";
import { useReCaptcha } from "next-recaptcha-v3";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import QuestionScene from "./questionModuleScane";

const QuestionsContainer = () => {
  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();

  const [isExportLoading, setIsExportLoading] = useState(false);
  const {
    questions: { isLoading, data: questionsData },
  }: {
    questions: QuestionsState;
  } = useSelector((state: RootState) => ({
    questions: state.questions,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 12,
      skip: 0,
    },
  });

  const handleFetchQuestions = async () => {
    const recaptchaResponse = await executeRecaptcha("questions");
    dispatch(fetchQuestions(apiParam, recaptchaResponse));
  };

  useEffect(() => {
    if (loaded) {
      handleFetchQuestions();
    }
  }, [apiParam, loaded]);

  const handleLoadMore = async (d: any) => {
    const recaptchaResponse = await executeRecaptcha("questions");
    apiParam.skip = (apiParam.skip || 0) + 9;
    dispatch(loadMoreQuestions(apiParam, recaptchaResponse));
  };

  const handleExportData = async (data: any) => {
    if (data) {
      setIsExportLoading(true);
      try {
        await getNewsLetterQuestionsData();
        showToast(SUCCESS_MESSAGES.exportedDataSuccess, "success");
        setIsExportLoading(false);
      } catch (error) {
        console.log(error);
        setIsExportLoading(false);
      }
    }
  };

  return (
    <>
      {isExportLoading && <DefaultSkeleton />}
      {isLoading && <DefaultSkeleton />}
      <Show when={["user_questions"]} fallback={<NoPermissionsComponent />}>
        <QuestionScene
          {...{
            questionsData,
            isLoading,
            handleLoadMore,
            handleExportData,
            isExportLoading,
          }}
        />
      </Show>
    </>
  );
};

QuestionsContainer.Layout = AdminLayoutComponent;
export default QuestionsContainer;
