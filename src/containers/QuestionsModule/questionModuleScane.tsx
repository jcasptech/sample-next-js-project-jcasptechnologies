import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { QuestionsTagsDataResponse } from "@redux/slices/Questions";
import moment from "moment";
import Styles from "./questionModule.module.scss";

export interface QuestionSceneProps {
  questionsData: QuestionsTagsDataResponse | any;
  isLoading: boolean | undefined;
  handleLoadMore: (d: any) => void;
  handleExportData: (d: any) => void;
  isExportLoading: boolean;
}

const QuestionScene = (props: QuestionSceneProps) => {
  const {
    questionsData,
    isLoading,
    handleLoadMore,
    handleExportData,
    isExportLoading,
  } = props;

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="row shows-venue-pad m0">
            <div className="register_btn btn-shows">
              <Button
                type="primary"
                htmlType="button"
                loading={isExportLoading}
                disabled={isExportLoading}
                className="float-right"
                onClick={() => handleExportData(true)}
              >
                Export All Data
              </Button>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-lg-12 shows_holder ola mt-4">
            <div className="card card2 border-0">
              <div className="tab-content" id="tabcontent1">
                <div
                  className="tab-pane fade show active"
                  id="tabs-text-1"
                  role="tabpanel"
                >
                  <div className={`row ${Styles.tagsRow}`}>
                    {questionsData &&
                      questionsData?.list &&
                      questionsData?.list?.length > 0 &&
                      questionsData.list?.map((data: any, index: any) => (
                        <div
                          className="col-12  col-md-6 col-sm-4 col-lg-4 message-hold"
                          key={index}
                        >
                          <div
                            className="show_indi_card aos-init aos-animate"
                            data-aos="fade-up"
                            data-aos-delay="100"
                            data-aos-duration="1200"
                          >
                            <div className="row m0 vcenter">
                              <div className="col-12 show_meta">
                                <div className="row message-btns">
                                  <div className="title title-m col-12 text-ellipsis">
                                    {moment(data.createdAt).format("MMMM D, Y")}
                                  </div>
                                  <div
                                    className={`${Styles.evented_description} col-12 pt-1`}
                                  >
                                    {data?.userQuestion || "Question"}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))}

                    {questionsData?.list &&
                      questionsData?.list?.length <= 0 && (
                        <EmptyMessage description="No lists found." />
                      )}

                    {questionsData.hasMany && (
                      <div className="col-12 load_more">
                        <Button
                          type="ghost"
                          htmlType="button"
                          loading={isLoading}
                          disabled={isLoading}
                          onClick={(e) => {
                            e.preventDefault();
                            handleLoadMore({
                              loadMore: true,
                            });
                          }}
                        >
                          Load more
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default QuestionScene;
