import { useRouter } from "next/router";
import dashboardStyles from "./dashboardStyle.module.scss";
import RequestFeatureModal from "@components/layout/requestFeactureModalLayout";
import { useState } from "react";
import { LoginUserState } from "@redux/slices/auth";
import { LOGIN_USER_TYPE } from "src/libs/constants";

export interface DashboardScaneProps {
  selectedArtistData: any;
  loginUserState: LoginUserState;
}

const DashboardScane = (props: DashboardScaneProps) => {
  const { selectedArtistData, loginUserState } = props;
  const router = useRouter();
  const [isrequestModalOpen, setIsRequestFeatureModalOpen] = useState(false);

  return (
    <>
      {isrequestModalOpen && (
        <RequestFeatureModal
          {...{
            isOpen: isrequestModalOpen,
            setIsRequestFeatureModalOpen,
          }}
        />
      )}
      <div className={`main-section admin-panel-section`}>
        <section className="section">
          <div className={`${dashboardStyles.dashboard}`}>
            <div>
              <div className="p-0">
                <h2>Overview</h2>
              </div>
              <div className="folders">
                {loginUserState?.data?.userType === LOGIN_USER_TYPE.ADMIN && (
                  <>
                    <div className="folder">
                      <div
                        className="orange"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/active-posting");
                        }}
                        title="Active Posting"
                      >
                        <div>
                          <span className="nav-label">Active Posting</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="green"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/message");
                        }}
                        title="Messages"
                      >
                        <div>
                          <span className="nav-label">Messages</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="skyblue"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/claimed-artist");
                        }}
                        title="Claimed Artist"
                      >
                        <div>
                          <span className="nav-label">Claimed Artist</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="magenta"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/user-shows");
                        }}
                        title="View/Add Shows"
                      >
                        <div>
                          <span className="nav-label">View/Add Shows</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="orange"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/admin-calendar");
                        }}
                        title="Calendar"
                      >
                        <div>
                          <span className="nav-label">Calendar</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="green"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/featured-artists");
                        }}
                        title="Featured Artists"
                      >
                        <div>
                          <span className="nav-label">Featured Artists</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="skyblue"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/featured-venues");
                        }}
                        title="Featured Venues"
                      >
                        <div>
                          <span className="nav-label">Featured Venues</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="magenta"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/embed-code");
                        }}
                        title="Embed Code"
                      >
                        <div>
                          <span className="nav-label">Embed Code</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="orange"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/artist-element");
                        }}
                        title="Artist's Element"
                      >
                        <div>
                          <span className="nav-label">
                            Artist&apos;s Element
                          </span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="green"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/user-questions");
                        }}
                        title="User Questions"
                      >
                        <div>
                          <span className="nav-label">User Questions</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="skyblue"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/venue-listing");
                        }}
                        title="Venue Listing"
                      >
                        <div>
                          <span className="nav-label">Venue Listing</span>
                        </div>
                      </div>
                    </div>
                  </>
                )}

                {loginUserState?.data?.userType ===
                  LOGIN_USER_TYPE.VENUE_ADMIN && (
                  <>
                    <div className="folder">
                      <div
                        className="orange"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/active-posting");
                        }}
                        title="Active Posting"
                      >
                        <div>
                          <span className="nav-label">Active Posting</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="green"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/venue-calendar");
                        }}
                        title="Calendar"
                      >
                        <div>
                          <span className="nav-label">Calendar</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="magenta"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/user-shows");
                        }}
                        title="View/Add Shows"
                      >
                        <div>
                          <span className="nav-label">View/Add Shows</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="skyblue"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/venue-listing");
                        }}
                        title="Venue Listing"
                      >
                        <div>
                          <span className="nav-label">Venue Listing</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="orange"
                        onClick={(e) => {
                          e.preventDefault();
                          setIsRequestFeatureModalOpen(true);
                        }}
                        title="Request a Feature"
                      >
                        <div>
                          <span className="nav-label">Request a Feature</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="magenta"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/contact-us");
                        }}
                        title="Contact Support"
                      >
                        <div>
                          <span className="nav-label">Contact Support</span>
                        </div>
                      </div>
                    </div>
                  </>
                )}

                {loginUserState?.data?.userType === LOGIN_USER_TYPE.USER && (
                  <>
                    <div className="folder">
                      <div
                        className="orange"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/active-posting");
                        }}
                        title="Active Posting"
                      >
                        <div>
                          <span className="nav-label">Active Posting</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="magenta"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/subscribe-artists");
                        }}
                        title="Subscribed Artists"
                      >
                        <div>
                          <span className="nav-label">Subscribed Artists</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="green"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/subscribe-venues");
                        }}
                        title="Subscribed Venues"
                      >
                        <div>
                          <span className="nav-label">Subscribed Venues</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="skyblue"
                        onClick={(e) => {
                          e.preventDefault();
                          setIsRequestFeatureModalOpen(true);
                        }}
                        title="Request a Feature"
                      >
                        <div>
                          <span className="nav-label">Request a Feature</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="magenta"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/contact-us");
                        }}
                        title="Contact Support"
                      >
                        <div>
                          <span className="nav-label">Contact Support</span>
                        </div>
                      </div>
                    </div>
                  </>
                )}

                {loginUserState?.data?.userType === LOGIN_USER_TYPE.MANAGER && (
                  <>
                    <div className={`folder`}>
                      <div
                        className={`orange`}
                        onClick={(e) => {
                          e.preventDefault();
                          if (!selectedArtistData?.objectId) {
                            router.push("/create-artist");
                          } else {
                            router.push("/shows");
                          }
                        }}
                        title="View/Add Shows"
                      >
                        <div>
                          <span className="nav-label">View/Add Shows</span>
                        </div>
                      </div>
                    </div>

                    <div className={`folder`}>
                      <div
                        className={`green`}
                        onClick={(e) => {
                          e.preventDefault();
                          if (!selectedArtistData?.objectId) {
                            router.push("/create-artist");
                          } else {
                            router.push("/calendar");
                          }
                        }}
                        title="My Calendar"
                      >
                        <div>
                          <span className="nav-label">My Calendar</span>
                        </div>
                      </div>
                    </div>

                    <div className={`folder`}>
                      <div
                        className={`magenta`}
                        onClick={(e) => {
                          e.preventDefault();
                          if (!selectedArtistData?.objectId) {
                            router.push("/create-artist");
                          } else {
                            router.push("/browse-open-postings");
                          }
                        }}
                        title="Browse Open Gigs"
                      >
                        <div>
                          <span className="nav-label">Browse Open Gigs</span>
                        </div>
                      </div>
                    </div>

                    <div className={`folder`}>
                      <div
                        className={`skyblue`}
                        onClick={(e) => {
                          e.preventDefault();
                          if (!selectedArtistData?.objectId) {
                            router.push("/create-artist");
                          } else {
                            router.push("/message");
                          }
                        }}
                        title="My Messages"
                      >
                        <div>
                          <span className="nav-label">My Messages</span>
                        </div>
                      </div>
                    </div>

                    <div className={`folder`}>
                      <div
                        className={`orange`}
                        onClick={(e) => {
                          e.preventDefault();
                          if (!selectedArtistData?.objectId) {
                            router.push("/create-artist");
                          } else {
                            router.push("/edit-artist-profile");
                          }
                        }}
                        title="Edit Artist Profile"
                      >
                        <div>
                          <span className="nav-label">Edit Artist Profile</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder disabled">
                      <div className="grey">
                        <div>
                          <span className="nav-label">Teacher Portal</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder disabled">
                      <div className="grey">
                        <div>
                          <span className="nav-label">Buy/Sell Gear</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder disabled">
                      <div className="grey">
                        <div>
                          <span className="nav-label">Backstage</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="skyblue"
                        onClick={(e) => {
                          e.preventDefault();
                          setIsRequestFeatureModalOpen(true);
                        }}
                        title="Request a Feature"
                      >
                        <div>
                          <span className="nav-label">Request a Feature</span>
                        </div>
                      </div>
                    </div>

                    <div className="folder">
                      <div
                        className="magenta"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push("/contact-us");
                        }}
                        title="Contact Support"
                      >
                        <div>
                          <span className="nav-label">Contact Support</span>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </section>
      </div>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          {/* <div className="col-12 col-sm-8 DB-stats">
              <div className="row">
                <div className="col-12 p-0">
                  <h5>Overview</h5>
                </div>
                <div className="col-12 col-sm-3 fldr">
                  <div className="float-left folder orange">
                    <div className="ti">No. of events</div>
                    <div className="dd">
                      <select>
                        <option>This Week</option>
                        <option>This Month</option>
                        <option>Today</option>
                      </select>
                    </div>
                    <div className="float-left w-100 everw">
                      <div className="num">26</div>
                      <div className="evt">Events</div>
                    </div>
                    <div className="float-left w-100 mt-20 eveln">
                      <img src="/images/general/dsArrow.svg" alt="G" />
                      +14 Event From last week
                    </div>
                  </div>
                </div>

                <div className="col-12 col-sm-3 fldr">
                  <div className="float-left folder grey">
                    <div className="ti">No. of events</div>
                    <div className="dd">
                      <select>
                        <option>This Week</option>
                        <option>This Month</option>
                        <option>Today</option>
                      </select>
                    </div>
                    <div className="float-left w-100 everw">
                      <div className="num">26</div>
                      <div className="evt">Events</div>
                    </div>
                    <div className="float-left w-100 mt-20 eveln">
                      <img src="/images/general/dsArrow.svg" alt="G" />
                      +14 Event From last week
                    </div>
                  </div>
                </div>

                <div className="col-12 col-sm-3 fldr">
                  <div className="float-left folder magenta">
                    <div className="ti">No. of events</div>
                    <div className="dd">
                      <select>
                        <option>This Week</option>
                        <option>This Month</option>
                        <option>Today</option>
                      </select>
                    </div>
                    <div className="float-left w-100 everw">
                      <div className="num">26</div>
                      <div className="evt">Events</div>
                    </div>
                    <div className="float-left w-100 mt-20 eveln">
                      <img src="/images/general/dsArrow.svg" alt="G" />
                      +14 Event From last week
                    </div>
                  </div>
                </div>

                <div className="col-12 col-sm-3 fldr">
                  <div className="float-left folder skyblue">
                    <div className="ti">No. of events</div>
                    <div className="dd">
                      <select>
                        <option>This Week</option>
                        <option>This Month</option>
                        <option>Today</option>
                      </select>
                    </div>
                    <div className="float-left w-100 everw">
                      <div className="num">26</div>
                      <div className="evt">Events</div>
                    </div>
                    <div className="float-left w-100 mt-20 eveln">
                      <img src="/images/general/dsArrow.svg" alt="G" />
                      +14 Event From last week
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-sm-3 dash-side for-mobile">
                <div className="row">
                  <div className="col-12 ptitle p-0">
                    <h5>Artist List</h5>
                  </div>
                  <div className="col-12 p-0">
                    <div className="artist-bx">
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">
                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">

                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">

                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">

                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">

                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">

                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">

                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                      <a href="#">
                        <div className="row lsTart m0">
                          <div className="col-3">

                            <img src="/images/artistsq.png" alt="N" />
                          </div>
                          <div className="col-9">
                            <span className="name">Jimbani Gupti</span>
                            <span className="band">Band</span>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div> */}

          {/* <div className="row m0">
                <div className="col-12 p-0">
                  <h5>Recent Event Details</h5>
                </div>
                <div className="col-12 p-0">

                  <table id="recEvents" className="dt-responsive">
                    <thead>
                      <tr>
                        <th>Artist Name</th>
                        <th>Date</th>
                        <th>Venue</th>
                        <th>Event Variety</th>
                        <th>Event Format</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                      <tr>
                        <td>
                          <img
                            src="/images/artistsq.png"
                            className="w-100 ppc"
                            alt="N"
                          />{" "}
                          Jimbani Gupti
                        </td>
                        <td>
                          <img src="/images/dtcal.svg" className="" alt="N" />
                          &nbsp;Feb 20th, Sunday
                        </td>
                        <td>Grand Pacific Palisades Resort & Hotel</td>
                        <td>Wedding</td>
                        <td>Band</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div> */}

          {/* </div> */}
          {/* <div className="col-12 col-sm-3 dash-side for-desktop">
              <div className="row">
                <div className="col-12 ptitle p-0">
                  <h5>Artist List</h5>
                </div>
                <div className="col-12 p-0">
                  <div className="artist-bx">
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                    <a href="#">
                      <div className="row lsTart m0">
                        <div className="col-3">

                          <img src="/images/artistsq.png" alt="N" />
                        </div>
                        <div className="col-9">
                          <span className="name">Jimbani Gupti</span>
                          <span className="band">Band</span>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div> */}
          {/* </div> */}
        </div>
      </div>
    </>
  );
};

export default DashboardScane;
