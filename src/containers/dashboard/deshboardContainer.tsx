import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { RootState } from "@redux/reducers";
import { SeledtedArtistState } from "@redux/slices/artistList";
import { LoginUserState } from "@redux/slices/auth";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import DashboardScane from "./deshboardScane";

export interface DashboardContainerProps {}

const DashboardContainer = (props: DashboardContainerProps) => {
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const {
    selectedArtist: { data: selectedArtistData },
  }: {
    selectedArtist: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    selectedArtist: state.selectedArtist,
  }));

  return (
    <>
      <Show when={["dashboard"]} fallback={<NoPermissionsComponent />}>
        <DashboardScane
          {...{
            selectedArtistData,
            loginUserState,
          }}
        />
      </Show>
    </>
  );
};

DashboardContainer.Layout = AdminLayoutComponent;
export default DashboardContainer;
