import { ShowsDataResponse } from "@redux/slices/shows";
import { Col, Row } from "antd";
import dynamic from "next/dynamic";
import Link from "next/link";

const Shows = dynamic(() => import("@components/shows/shows"));
const Map = dynamic(() => import("@components/googleMap/Map"));
const Tags = dynamic(() => import("@components/artist/tags"));
const Artists = dynamic(() => import("@components/artist/artists"));
const ApplicationDetalis = dynamic(
  () => import("@components/applicationDetails/applicationDetalis")
);
const NewsLetterSection = dynamic(() => import("@components/newsLetter"));
const DownloadAppPrompt = dynamic(
  () => import("@components/downloadAppPrompt")
);

import homeStyle from "./home.module.scss";

export interface HomeSceneProps {
  shows: ShowsDataResponse;
  isLoading?: boolean;
  handleFilter: (d: any) => void;
  clickedShow: any;
  setClickedShow: (d: any) => void;
  selectedTags: string[];
  setSelectedTags: (d: any) => void;
  showType?: string;
  setSelected: (d: any) => void;
  selected: any;
}

const HomeScene = (props: HomeSceneProps) => {
  const {
    shows,
    handleFilter,
    isLoading,
    clickedShow,
    setClickedShow,
    selectedTags,
    setSelectedTags,
    showType,
    setSelected,
    selected,
  } = props;

  return (
    <>
      <div className={`main-section`}>
        <section className="section">
          <Row gutter={[24, 8]} className={homeStyle.map_and_shows}>
            <Col lg={14} md={14} sm={24}>
              <Shows
                {...{
                  shows,
                  handleFilter,
                  isLoading,
                  setClickedShow,
                  showType,
                }}
              />
            </Col>
            <Col lg={10} md={10} sm={24} className="display-grid">
              <div className="h-100 h-min-300">
                <Map
                  {...{
                    shows,
                    clickedShow,
                    setSelected,
                    selected,
                  }}
                />
              </div>
            </Col>
          </Row>
        </section>

        <section className="section">
          <div className="row m0">
            <div className="col-12 text-center aart_holder">
              <h2 className={homeStyle.sec_head}>Active Artists</h2>
              <div className="row">
                <div className="col-3">&nbsp;</div>
                <div className="col-md-6 col-sm-12">
                  <p>
                    Discover artists with regular performances in your area.
                    Browse and experience live music close to home. For a
                    comprehensive and dynamic search of artists, venues, and
                    upcoming shows,{" "}
                    <Link href="/browse" className="click">
                      click here.
                    </Link>
                  </p>
                </div>
                <div className="col-3">&nbsp;</div>
              </div>
            </div>

            <Tags
              {...{
                selectedTags,
                setSelectedTags,
              }}
            />
            <div className={`row m0 load_artists ${homeStyle.artist_b}`}>
              <Artists
                {...{
                  selectedTags,
                }}
              />
            </div>
          </div>
        </section>

        <section className="section pb-0">
          <ApplicationDetalis />
        </section>

        <section className="section p-0">
          <NewsLetterSection />
        </section>
      </div>

      <DownloadAppPrompt />
    </>
  );
};

export default HomeScene;
