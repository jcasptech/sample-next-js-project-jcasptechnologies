import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { DefaultSkeleton } from "@components/theme";
import { RootState } from "@redux/reducers";
import { getShowFullDetailAPI } from "@redux/services/calendar.api";
import { fetchShows, loadMoreShows, ShowsState } from "@redux/slices/shows";
import { useReCaptcha } from "next-recaptcha-v3";
import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DEFAULT_TABLE_LIMIT } from "src/libs/constants";
import useList from "src/libs/useList";
import HomeScene from "./homeScene";

const SEO = dynamic(() => import("@components/SEO"));

export interface HomeContainerProps {
  isShowsId: any;
}

const HomeContainer = (props: HomeContainerProps) => {
  const { isShowsId } = props;

  const {
    shows: { isLoading, data: showsData },
  }: {
    shows: ShowsState;
  } = useSelector((state: RootState) => ({
    shows: state.shows,
  }));
  const [selected, setSelected] = useState<any>(null);

  const [clickedShow, setClickedShow] = useState(null);
  const [selectedTags, setSelectedTags] = useState([]);
  const { executeRecaptcha, loaded } = useReCaptcha();

  const dispatch = useDispatch();
  const { apiParam } = useList({
    queryParams: {
      take: DEFAULT_TABLE_LIMIT,
      skip: 0,
      shows: "TODAY",
      include: ["venue", "artist"],
    },
  });

  const getShowData = async () => {
    const recaptchaResponse = await executeRecaptcha("get_shows");
    dispatch(fetchShows(apiParam, recaptchaResponse));
  };

  const handleFilter = async (data: any) => {
    if (data.showType) {
      apiParam.shows = data.showType;
      apiParam.skip = 0;
      apiParam.take = DEFAULT_TABLE_LIMIT;
    }

    if (data.search) {
      apiParam.search = data.search;
    } else {
      apiParam.search = "";
    }

    const recaptchaResponse = await executeRecaptcha("get_shows");
    if (data.loadMore && apiParam?.take) {
      apiParam.skip = (apiParam.skip || 0) + DEFAULT_TABLE_LIMIT;
      dispatch(loadMoreShows(apiParam, recaptchaResponse));
    } else {
      dispatch(fetchShows(apiParam, recaptchaResponse));
    }
  };

  useEffect(() => {
    if (loaded) {
      getShowData();
    }
  }, [apiParam, loaded]);

  useEffect(() => {
    setSelectedTags([]);
  }, []);

  const handleGetShowSeletedData = async (id: any) => {
    if (id) {
      try {
        const res = await getShowFullDetailAPI(id);
        setSelected({
          position: {
            lat: res?.venue?.location?.latitude,
            lng: res?.venue?.location?.longitude,
          },
          venue: res.venue,
          showId: res.objectId,
          show: res,
        });
      } catch (error) {
        console.log(error);
      }
    }
  };

  useEffect(() => {
    if (isShowsId) {
      handleGetShowSeletedData(isShowsId);
    }
  }, [isShowsId]);

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <SEO
        {...{
          pageName: "/",
        }}
      />
      <HomeScene
        {...{
          shows: showsData,
          isLoading,
          handleFilter,
          clickedShow,
          setClickedShow,
          selectedTags,
          setSelectedTags,
          showType: apiParam.shows,
          setSelected,
          selected,
        }}
      />
    </>
  );
};

HomeContainer.Layout = MainLayoutComponent;

HomeContainer.getInitialProps = async (ctx: any) => {
  const showsId = ctx?.query?.showId;

  return {
    isShowsId: showsId ? showsId : null,
  };
};

export default HomeContainer;
