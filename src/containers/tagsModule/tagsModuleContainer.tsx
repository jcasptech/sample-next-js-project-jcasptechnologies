import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { postApprovedAPI } from "@redux/services/tags.api";
import {
  fetchUnApprovedTags,
  loadMoreUnApprovedTags,
  UnApprovedTagsState,
} from "@redux/slices/usApprovedTags";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import TagsScene from "./tagsModuleScane";

const TagsContainer = () => {
  const dispatch = useDispatch();

  const {
    unApprovedTags: { isLoading, data: TagsData },
  }: {
    unApprovedTags: UnApprovedTagsState;
  } = useSelector((state: RootState) => ({
    unApprovedTags: state.unApprovedTags,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 12,
      skip: 0,
      include: ["artist.iconImage"],
    },
  });

  useEffect(() => {
    dispatch(fetchUnApprovedTags(apiParam));
  }, [apiParam]);

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 9;
    dispatch(loadMoreUnApprovedTags(apiParam));
  };

  const handleApproved = async (data: any) => {
    if (data) {
      try {
        await postApprovedAPI(data);
        showToast(SUCCESS_MESSAGES.tagApprovedSuccess, "success");
        dispatch(fetchUnApprovedTags(apiParam));
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["artist_elements"]} fallback={<NoPermissionsComponent />}>
        <TagsScene
          {...{
            TagsData,
            isLoading,
            handleLoadMore,
            handleApproved,
          }}
        />
      </Show>
    </>
  );
};

TagsContainer.Layout = AdminLayoutComponent;
export default TagsContainer;
