import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import {
  UnApprovedTagsDataResponse,
  unApprovetagsObject,
} from "@redux/slices/usApprovedTags";
import { Popconfirm } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import { capitalizeString } from "src/libs/helpers";
import tagsStyles from "./tagsModule.module.scss";

export interface TagsSceneProps {
  TagsData: UnApprovedTagsDataResponse | any;
  isLoading: boolean | undefined;
  handleLoadMore: (d: any) => void;
  handleApproved: (d: any) => void;
}

const TagsScene = (props: TagsSceneProps) => {
  const { TagsData, isLoading, handleLoadMore, handleApproved } = props;

  const router = useRouter();
  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="col-12 col-sm-12 col-lg-12 shows_holder ola mt-4">
            <div className="card card2 border-0">
              <div className="tab-content" id="tabcontent1">
                <div
                  className="tab-pane fade show active"
                  id="tabs-text-1"
                  role="tabpanel"
                >
                  <div className={`row ${tagsStyles.tagsRow}`}>
                    {TagsData &&
                      TagsData?.list &&
                      TagsData?.list?.length > 0 &&
                      TagsData.list?.map(
                        (data: unApprovetagsObject, index: any) => (
                          <div
                            className="col-12  col-sm-4 col-lg-4 message-hold"
                            key={index}
                          >
                            <div
                              className="show_indi_card aos-init aos-animate"
                              data-aos="fade-up"
                              data-aos-delay="100"
                              data-aos-duration="1200"
                            >
                              <div className="row m0 vcenter">
                                <div className="col-12 show_meta">
                                  <div className="row message-btns artist-white">
                                    <div className="Header">
                                      <div className="me-1">Artist</div>
                                      <Link
                                        href={`/artist/${data?.artist?.vanity}`}
                                        target="_blank"
                                        className="title title-m text-ellipsis text-primary"
                                      >
                                        {data?.artist?.name || ""}
                                      </Link>
                                    </div>
                                    <div className={`col-12 pt-1 tagContent`}>
                                      <span className="EleTag capitalize">
                                        {capitalizeString(data.label)}
                                      </span>
                                    </div>
                                    <div className="col-12">
                                      <div className="button">
                                        <div className="approve-btn">
                                          <Popconfirm
                                            title="Are you sure you want to Approve?"
                                            okText="Yes, Approve"
                                            cancelText="Cancel"
                                            onConfirm={() =>
                                              handleApproved(data?.objectId)
                                            }
                                          >
                                            <Button
                                              type="primary"
                                              htmlType="button"
                                              className="float-right mt-1"
                                            >
                                              Approve
                                            </Button>
                                          </Popconfirm>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      )}

                    {TagsData?.list && TagsData?.list?.length <= 0 && (
                      <EmptyMessage description="No elements found." />
                    )}

                    {TagsData.hasMany && (
                      <div className="col-12 load_more">
                        <Button
                          type="ghost"
                          htmlType="button"
                          loading={isLoading}
                          disabled={isLoading}
                          onClick={(e) => {
                            e.preventDefault();
                            handleLoadMore({
                              loadMore: true,
                            });
                          }}
                        >
                          Load more
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TagsScene;
