import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { RootState } from "@redux/reducers";
import { AdminVenuesState, fetchAdminVenues } from "@redux/slices/adminVenues";
import { LoginUserState } from "@redux/slices/auth";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { deepClone } from "src/libs/helpers";
import useList from "src/libs/useList";
import VenuesScene from "./venuesScene";

const VenuesContainer = () => {
  const dispatch = useDispatch();

  const {
    adminVenues: { isLoading, data: venuesData },
  }: {
    adminVenues: AdminVenuesState;
  } = useSelector((state: RootState) => ({
    adminVenues: state.adminVenues,
  }));

  const { apiParam, setApiParam } = useList({
    queryParams: {
      take: 9,
      skip: 0,
    },
  });

  useEffect(() => {
    dispatch(fetchAdminVenues(apiParam));
  }, [dispatch, apiParam]);

  const handleLoadMore = (d: any) => {
    const tmp = deepClone(apiParam);
    tmp.skip = (tmp.skip || 0) + 9;
    setApiParam(tmp);
  };

  const handleFilter = (data: any) => {
    const tmp = deepClone(apiParam);
    if (data) {
      tmp.search = data || "";
    } else {
      tmp.search = "";
    }
    tmp.skip = 0;
    setApiParam(tmp);
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["venue_listing"]} fallback={<NoPermissionsComponent />}>
        <VenuesScene
          {...{
            venuesData,
            isLoading,
            handleLoadMore,
            handleFilter,
          }}
        />
      </Show>
    </>
  );
};

VenuesContainer.Layout = AdminLayoutComponent;
export default VenuesContainer;
