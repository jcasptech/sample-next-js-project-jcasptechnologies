import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { VenueObject } from "@redux/slices/venues";
import { Col, Row } from "antd";
import SingleVenue from "./single-venue";

export interface VenueSceneProps {
  venuesData: any;
  isLoading: boolean | undefined;
  handleLoadMore: (d: any) => void;
  handleFilter: (d: any) => void;
}
const VenuesScene = (props: VenueSceneProps) => {
  const { venuesData, isLoading, handleLoadMore, handleFilter } = props;

  return (
    <>
      <div className={`main-section admin-panel-section`}>
        <section className="section">
          <Row gutter={[20, 20]}>
            <Col span={24}>
              <div className="search_form w-100">
                <input
                  type="text"
                  placeholder="Search Venue"
                  className="search_art aos-init aos-animate"
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onChange={(e: any) => {
                    handleFilter(e.target?.value);
                  }}
                />
              </div>
            </Col>
          </Row>
        </section>
        <section className="section h-75vs">
          <Row gutter={[20, 20]}>
            {venuesData &&
              venuesData?.list &&
              venuesData?.list.length > 0 &&
              venuesData.list.map((venue: VenueObject, index: number) => (
                <SingleVenue
                  key={`venue-${index}`}
                  {...{
                    venue,
                  }}
                />
              ))}
          </Row>

          {venuesData && venuesData?.list && venuesData?.list.length <= 0 && (
            <Row gutter={[30, 30]} justify="center">
              <Col>
                <EmptyMessage description="No venues found." />
              </Col>
            </Row>
          )}

          {venuesData?.hasMany && venuesData?.hasMany === true && (
            <Row justify={"center"} className="mt-4">
              <Col>
                <Button
                  htmlType="button"
                  type="ghost"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={(e: any) => {
                    e.preventDefault();
                    handleLoadMore(true);
                  }}
                >
                  Load more
                </Button>
              </Col>
            </Row>
          )}
        </section>
      </div>

      {/* <div className="message-page h-100vh mt-4">
        <div className="panel-inner-content">
          <div className="row mb-3">
            <div className="col-12">
              <div className="search_form shows-search-form col-12">
                <input
                  type="text"
                  placeholder="Search for Venue"
                  className="search_art aos-init aos-animate"
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onChange={(e: any) => {
                    handleFilter(e.target?.value);
                  }}
                />
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-lg-12 shows_holder ola mt-2">
            <div className="card card2 border-0">
              <div className="tab-content" id="tabcontent1">
                <div
                  className="tab-pane fade show active"
                  id="tabs-text-1"
                  role="tabpanel"
                >
                  <div className={`row ${venuesStyles.venueRow}`}>
                    {venuesData &&
                      venuesData?.list &&
                      venuesData?.list?.length > 0 &&
                      venuesData?.list?.map(
                        (data: VenueObject, index: number) => (
                          <div
                            className="col-12 col-sm-12 col-md-6 col-lg-4 venuecards cursor-pointer"
                            key={index}
                            onClick={(e) => {
                              e.preventDefault();
                              router.push(`/venue/edit/${data?.objectId}`);
                            }}
                          >
                            <div
                              className={`show_indi_card shows-card aos-init aos-animate`}
                              data-aos="fade-up"
                              data-aos-delay="100"
                              data-aos-duration="1200"
                            >
                              <div className="row m0 vcenter">
                                <div className="col-sm-12 col-lg-4 col-md-4 show_thumb p-0">
                                  <img
                                    src={
                                      data?.iconImage?.url ||
                                      "/images/location-placeholder.png"
                                    }
                                    alt="venue title"
                                  />
                                </div>
                                <div className="col-sm-12  col-lg-8 col-md-8 show_meta">
                                  <div className="row message-btns">
                                    <div className="title title-m col-12 text-ellipsis">
                                      {data?.name || ""}
                                    </div>
                                    <div
                                      className={`${venuesStyles.evented_description} col-12 pt-1`}
                                    >
                                      {data?.about || " "}
                                    </div>
                                    <div className="addressDate">
                                      <span className="text-ellipsis">
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width="33"
                                          height="33"
                                          viewBox="0 0 33 33"
                                          fill="none"
                                        >
                                          <rect
                                            x="0.012207"
                                            width="32.3214"
                                            height="32.3214"
                                            rx="9.5063"
                                            fill="#F5F5F5"
                                          />
                                          <rect
                                            x="0.012207"
                                            width="32.3214"
                                            height="32.3214"
                                            rx="9.5063"
                                            fill="#F5F5F5"
                                          />
                                          <path
                                            d="M16.3199 25.6705C15.9191 25.5576 15.6231 25.2859 15.324 25.0165C14.2895 24.0874 13.2383 23.1765 12.2781 22.168C11.2603 21.0996 10.3406 19.9536 9.6433 18.6455C9.07751 17.5845 8.68173 16.4682 8.59022 15.2544C8.28503 11.1987 11.1381 7.50335 15.1623 6.7968C17.8566 6.32391 20.2387 7.04579 22.2231 8.94013C23.5889 10.2436 24.3688 11.8597 24.6085 13.7396C24.8031 15.2651 24.5086 16.6991 23.8694 18.0792C23.2261 19.4686 22.3179 20.6736 21.2959 21.8001C20.1597 23.0543 18.8711 24.1445 17.6197 25.2752C17.4131 25.4525 17.173 25.5865 16.9136 25.6691L16.3199 25.6705ZM9.72366 14.7151C9.71809 15.7101 9.97776 16.6443 10.3851 17.5418C11.0058 18.9089 11.9125 20.0776 12.9252 21.1702C14.0061 22.3371 15.2148 23.3688 16.3989 24.426C16.5638 24.5733 16.6702 24.5724 16.8365 24.4233C18.0057 23.3771 19.1995 22.357 20.274 21.2078C21.0855 20.3382 21.8264 19.4138 22.4196 18.3788C23.2488 16.9314 23.698 15.4031 23.436 13.7145C23.1624 11.9517 22.3439 10.4954 20.9675 9.37213C19.6204 8.27121 18.0596 7.76209 16.3111 7.83223C15.5011 7.86288 14.7032 8.0396 13.956 8.35389C12.702 8.87885 11.6312 9.76282 10.8782 10.8947C10.1253 12.0265 9.72374 13.3557 9.72413 14.7151H9.72366Z"
                                            fill="url(#paint0_radial_707_1974)"
                                          />
                                          <path
                                            d="M16.6006 17.9585C14.717 17.9487 13.0558 16.4539 13.0591 14.3942C13.0623 12.282 14.8094 10.8354 16.6318 10.8438C18.4467 10.8517 20.1789 12.3019 20.1738 14.4072C20.1682 16.4808 18.4848 17.9659 16.6006 17.9585ZM16.6081 16.7972C17.2427 16.802 17.8532 16.5546 18.3055 16.1094C18.7578 15.6642 19.0147 15.0576 19.0199 14.423C19.0334 13.1906 18.0755 11.9986 16.6155 11.9991C15.1555 11.9995 14.2134 13.1766 14.2139 14.3877C14.2111 14.7035 14.2711 15.0168 14.3902 15.3093C14.5094 15.6018 14.6854 15.8678 14.9081 16.0918C15.1308 16.3159 15.3957 16.4935 15.6875 16.6144C15.9793 16.7353 16.2922 16.7971 16.6081 16.7962V16.7972Z"
                                            fill="url(#paint1_radial_707_1974)"
                                          />
                                          <defs>
                                            <radialGradient
                                              id="paint0_radial_707_1974"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(13.249 10.1774) rotate(77.7326) scale(15.8551 13.5949)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint1_radial_707_1974"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(15.1277 12.1613) rotate(75.5978) scale(5.9853 5.95491)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                          </defs>
                                        </svg>
                                        &nbsp;{data?.address || ""}
                                        addresss
                                      </span>

                                      <span className="cal-o">
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width="33"
                                          height="33"
                                          viewBox="0 0 33 33"
                                          fill="none"
                                        >
                                          <rect
                                            x="0.0273438"
                                            width="32.3214"
                                            height="32.3214"
                                            rx="9.5063"
                                            fill="#F5F5F5"
                                          />
                                          <path
                                            d="M7.71973 13.6953H24.6644"
                                            stroke="url(#paint0_radial_707_1982)"
                                            strokeWidth="1.14076"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M20.4102 17.4141H20.4195"
                                            stroke="url(#paint1_radial_707_1982)"
                                            strokeWidth="1.90126"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M16.1924 17.4141H16.2018"
                                            stroke="url(#paint2_radial_707_1982)"
                                            strokeWidth="1.90126"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M11.9648 17.4141H11.9742"
                                            stroke="url(#paint3_radial_707_1982)"
                                            strokeWidth="1.90126"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M20.4102 21.1016H20.4195"
                                            stroke="url(#paint4_radial_707_1982)"
                                            strokeWidth="1.90126"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M16.1924 21.1016H16.2018"
                                            stroke="url(#paint5_radial_707_1982)"
                                            strokeWidth="1.90126"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M11.9648 21.1016H11.9742"
                                            stroke="url(#paint6_radial_707_1982)"
                                            strokeWidth="1.90126"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M20.0332 6.65625V9.78489"
                                            stroke="url(#paint7_radial_707_1982)"
                                            strokeWidth="1.14076"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            d="M12.3535 6.65625V9.78489"
                                            stroke="url(#paint8_radial_707_1982)"
                                            strokeWidth="1.14076"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <path
                                            fillRule="evenodd"
                                            clipRule="evenodd"
                                            d="M20.217 8.15625H12.1674C9.37561 8.15625 7.63184 9.71147 7.63184 12.5702V21.1734C7.63184 24.0771 9.37561 25.6682 12.1674 25.6682H20.2082C23.0088 25.6682 24.7438 24.104 24.7438 21.2453V12.5702C24.7526 9.71147 23.0176 8.15625 20.217 8.15625Z"
                                            stroke="url(#paint9_radial_707_1982)"
                                            strokeWidth="1.14076"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                          />
                                          <defs>
                                            <radialGradient
                                              id="paint0_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(12.6465 13.8805) rotate(12.9426) scale(3.63798 3.27957)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint1_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(20.4129 17.5992) rotate(89.8618) scale(0.814817 0.00811523)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint2_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(16.1951 17.5992) rotate(89.8618) scale(0.814817 0.00811523)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint3_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(11.9676 17.5992) rotate(89.8618) scale(0.814817 0.00811523)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint4_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(20.4129 21.2867) rotate(89.8618) scale(0.814817 0.00811523)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint5_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(16.1951 21.2867) rotate(89.8618) scale(0.814817 0.00811523)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint6_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(11.9676 21.2867) rotate(89.8618) scale(0.814817 0.00811523)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint7_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(20.324 7.23563) rotate(85.3077) scale(2.55783 0.861247)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint8_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(12.6443 7.23563) rotate(85.3077) scale(2.55783 0.861247)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                            <radialGradient
                                              id="paint9_radial_707_1982"
                                              cx="0"
                                              cy="0"
                                              r="1"
                                              gradientUnits="userSpaceOnUse"
                                              gradientTransform="translate(12.6073 11.3992) rotate(75.9134) scale(14.7114 14.3425)"
                                            >
                                              <stop stop-color="#1D1D1D" />
                                              <stop offset="1" />
                                            </radialGradient>
                                          </defs>
                                        </svg>
                                        &nbsp;
                                        {moment(data?.createdAt?.iso).format(
                                          "MMM Do YYYY"
                                        )}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      )}

                    {venuesData?.list && venuesData?.list?.length <= 0 && (
                      <Empty />
                    )}

                    {venuesData.hasMany && (
                      <div className="col-12 load_more mb-3">
                        <Button
                          type="ghost"
                          htmlType="button"
                          loading={isLoading}
                          disabled={isLoading}
                          onClick={(e) => {
                            e.preventDefault();
                            handleLoadMore({
                              loadMore: true,
                            });
                          }}
                        >
                          Load more Venues
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </>
  );
};
export default VenuesScene;
