import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { VenueObject, VenuesDataResponse } from "@redux/slices/venues";
import { Col, Row } from "antd";
import SingleVenue from "./single-venue";

export interface VenueSubscribeSceneProps {
  venuesData: VenuesDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  venueFollows: any[];
  handleVenueSubscribeActions: (
    id: string,
    type: "subscribe" | "unsubscribe"
  ) => void;
  handleVenue: (d: any) => void;
}

const VenueSubscribeScene = (props: VenueSubscribeSceneProps) => {
  const {
    venuesData,
    handleLoadMore,
    isLoading,
    handleVenueSubscribeActions,
    handleVenue,
  } = props;

  return (
    <>
      <div className={`main-section admin-panel-section`}>
        <section className="section h-75vs">
          <Row gutter={[30, 30]}>
            {venuesData &&
              venuesData?.list &&
              venuesData?.list.length > 0 &&
              venuesData.list.map((venue: VenueObject, index: number) => (
                <SingleVenue
                  key={`artist-${index}`}
                  {...{
                    venue,
                    handleVenue,
                    handleVenueSubscribeActions,
                  }}
                />
              ))}
          </Row>

          {venuesData && venuesData?.list && venuesData?.list.length <= 0 && (
            <Row gutter={[30, 30]} justify="center">
              <Col>
                <EmptyMessage description="No venues found." />
              </Col>
            </Row>
          )}

          {venuesData?.hasMany && venuesData?.hasMany === true && (
            <Row justify={"center"} className="mt-4">
              <Col>
                <Button
                  htmlType="button"
                  type="ghost"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={(e: any) => {
                    e.preventDefault();
                    handleLoadMore(true);
                  }}
                >
                  Load more
                </Button>
              </Col>
            </Row>
          )}
        </section>
      </div>
    </>
  );
};

export default VenueSubscribeScene;
