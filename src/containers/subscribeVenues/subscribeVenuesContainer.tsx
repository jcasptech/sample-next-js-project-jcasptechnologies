import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { whoAmI } from "@redux/services/auth.api";
import {
  venueSubscribeAPI,
  venueUnsubscribeAPI,
} from "@redux/services/venue.api";
import { LoginUserState, loginUserSuccess } from "@redux/slices/auth";
import { VenueState } from "@redux/slices/venueDetails";
import {
  fetchVenuesFollows,
  loadMoreVenuesFollows,
} from "@redux/slices/venues";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import SubscribeVenuesScene from "./subscribeVenuesScene";

export interface SubscribeVenuesContainerProps {}

const SubscribeVenuesContainer = (props: SubscribeVenuesContainerProps) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const [venueFollows, setVenueFollows] = useState<any[]>([]);
  const [isActionLoading, setIsActionLoading] = useState(false);
  const {
    venues: { isLoading, data: venuesData },
  }: {
    venues: VenueState;
  } = useSelector((state: RootState) => ({
    venues: state.venues,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
      include: ["metadata"],
    },
  });

  const getData = () => {
    if (apiParam) {
      if (loginUserState?.data?.userType === LOGIN_USER_TYPE.USER) {
        dispatch(fetchVenuesFollows(apiParam));
      }
    }
  };

  useEffect(() => {
    getData();
  }, [apiParam]);

  const handleLoadMore = (d: any) => {
    if (loginUserState?.data?.userType === LOGIN_USER_TYPE.USER) {
      apiParam.skip = (apiParam.skip || 0) + 8;
      dispatch(loadMoreVenuesFollows(apiParam));
    }
  };

  useEffect(() => {
    if (loginUserState?.data?.user?.venueFollows) {
      setVenueFollows(loginUserState?.data?.user?.venueFollows);
    }
  }, [loginUserState?.data?.user]);

  const handleVenueSubscribeActions = async (
    id: string,
    type: "subscribe" | "unsubscribe"
  ) => {
    const restrictMessage = SUCCESS_MESSAGES.requiredLoginSubscribeVenue;
    const successMessage =
      type === "subscribe"
        ? SUCCESS_MESSAGES.subscribeVenue
        : SUCCESS_MESSAGES.unsubscribeVenue;

    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          if (type === "subscribe") {
            await venueSubscribeAPI(id);
          } else {
            await venueUnsubscribeAPI(id);
          }
          showToast(successMessage, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
          getData();
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      showToast(restrictMessage, "warning");
    }
  };

  const handleVenue = (slug: string) => {
    if (slug) {
      router.push(`/venue/${slug}`);
    }
  };

  return (
    <>
      {isActionLoading && <DefaultSkeleton />}
      {isLoading && <DefaultSkeleton />}
      <Show
        when={["venues_subscriptions"]}
        fallback={<NoPermissionsComponent />}
      >
        <SubscribeVenuesScene
          {...{
            venuesData,
            handleLoadMore,
            isLoading,
            venueFollows,
            handleVenueSubscribeActions,
            handleVenue,
          }}
        />
      </Show>
    </>
  );
};

SubscribeVenuesContainer.Layout = AdminLayoutComponent;
export default SubscribeVenuesContainer;
