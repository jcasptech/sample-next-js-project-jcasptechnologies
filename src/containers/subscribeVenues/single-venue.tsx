import { BellIcon } from "@components/theme/icons/bellIcon";
import { LocationColorIcon } from "@components/theme/icons/locationColorIcon";
import { PhoneColorIcon } from "@components/theme/icons/phoneColorIcon";
import { WebsiteColorIcon } from "@components/theme/icons/websiteColorIcon";
import { VenueObject } from "@redux/slices/venues";
import { Col } from "antd";
import "aos/dist/aos.css";
import Image from "next/image";
import Link from "next/link";
import { capitalizeString } from "src/libs/helpers";
import VenueStyles from "./venueSubscribe.module.scss";

export interface SingleVenueProps {
  venue: VenueObject;
  handleVenue: (d: any) => void;
  handleVenueSubscribeActions: (
    id: string,
    type: "subscribe" | "unsubscribe"
  ) => void;
}

const SingleVenue = (props: SingleVenueProps) => {
  const { venue, handleVenue, handleVenueSubscribeActions } = props;

  return (
    <Col lg={6} md={8} sm={12} xs={24} className={VenueStyles.venue_card}>
      <div className={VenueStyles.thumb}>
        <Image
          className="cursor-pointer"
          src={venue.posterImage?.url || "/images/venue-placeholder.png"}
          alt={venue.name}
          onClick={(e) => handleVenue(venue.vanity)}
          width={425}
          height={260}
        />
        <div className={`${VenueStyles.thumb__favrt}`}>
          <span
            onClick={(e) => {
              e.preventDefault();
              handleVenueSubscribeActions(venue.objectId, "unsubscribe");
            }}
            className="unfollow"
          >
            <BellIcon />
          </span>
        </div>
      </div>

      <Link
        href={`/artist/${venue.vanity}`}
        target="_blank"
        className={`${VenueStyles.title}`}
      >
        {capitalizeString(venue.name)}
      </Link>

      <div className={`w-100 ${VenueStyles.abstract}`}>
        <div className="float-left w-100 d-flex justify-content-start align-items-center min-14">
          <LocationColorIcon /> &nbsp;{" "}
          <a
            href={
              venue?.address
                ? `https://www.google.com/maps/search/${encodeURI(
                    venue.address
                  )}`
                : "javascript:void(0)"
            }
            target="_blank"
            className="custom-text-muted venue-detail-link"
          >
            {venue?.address}
            {venue.state ? `, ${venue.state}` : ""}
          </a>
        </div>

        <div className="float-left w-100 d-flex justify-content-start align-items-center min-14">
          <WebsiteColorIcon /> &nbsp;{" "}
          <a
            href={venue?.website ? venue.website : "javascript:void(0)"}
            target="_blank"
            className="custom-text-muted venue-detail-link"
          >
            {venue?.website ? venue.website : null}
          </a>
        </div>

        <div className="float-left w-100 d-flex justify-content-start align-items-center min-14">
          <PhoneColorIcon /> &nbsp;{" "}
          <a
            href={venue?.phone ? `tel:${venue.phone}` : "javascript:void(0)"}
            className="custom-text-muted venue-detail-link"
          >
            {venue?.phone ? venue.phone : null}
          </a>
        </div>

        <div className="abstract w-100 float-left two-line-and-text-ellipsis">
          {venue?.about ? venue.about : ""}
        </div>
      </div>
    </Col>
  );
};

export default SingleVenue;
