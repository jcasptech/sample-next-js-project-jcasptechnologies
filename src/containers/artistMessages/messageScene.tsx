import { DefaultLoader } from "@components/theme";
import React, { useEffect, useState } from "react";
import { CalendarIcon } from "@components/theme/icons/calendarIcon";
import ArtistMessageStyles from "./artistMessage.module.scss";
import { LOGIN_USER_TYPE } from "src/libs/constants";
import { CheckMsgIcon } from "@components/theme/icons/checkMsgIcon";
import { DeleteMsgIcon } from "@components/theme/icons/deleteMsgIcon";
import { Popconfirm } from "antd";
import { EmptyMessage } from "@components/theme/empty";
import moment from "moment";
import Image from "next/image";

export interface MessageScaneProps {
  messageData: any;
  handleApproved: (d: any, id: any) => void;
  loginUserState: any;
  setSelectedId: (d: any) => void;
  allMessageData: any;
  setAllMessageData: any;
  userType: any;
  isMessageDataLoading: boolean;
  setIsMessageDataLoading: (d: any) => void;
  messageClick: (d: any) => void;
  name: string;
  image: string;
}

const MessageScane = (props: MessageScaneProps) => {
  const {
    messageData,
    loginUserState,
    handleApproved,
    setSelectedId,
    allMessageData,
    setAllMessageData,
    userType,
    isMessageDataLoading,
    setIsMessageDataLoading,
    messageClick,
    name,
    image,
  } = props;

  const formatMessageData = (message: any) => {
    const newData: any = [];
    allMessageData.map((newMessaage: any) => {
      if (newMessaage.objectId === message.objectId) {
        return;
      } else {
        newData.push(newMessaage);
      }
    });
    setAllMessageData(newData);
  };

  const approveDecline = (status: string, message: any) => {
    handleApproved(status, message);
    formatMessageData(message);
  };

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className={ArtistMessageStyles.messageContainer}>
            <div className="usersList">
              <div className="header">
                <h3>Messages</h3>
              </div>
              <div className="user-container">
                {messageData &&
                  messageData?.length > 0 &&
                  messageData?.map((message: any, index: any) => (
                    <div
                      key={message.objectId}
                      className="details cursor-pointer"
                      onClick={() => messageClick(message)}
                    >
                      <Image
                        src={
                          message?.receiver?.iconImage?.url ||
                          message?.source?.iconImage?.url ||
                          "/images/artist-placeholder.png"
                        }
                        alt={message?.receiver?.name}
                        height={50}
                        width={50}
                      />
                      <div className="mt-1 nameCon text-ellipsis">
                        <h5
                          className={`${
                            message?.receiver?.name === name ||
                            message?.source?.name === name
                              ? "selectActive"
                              : ""
                          } text-ellipsis`}
                        >
                          {message?.receiver?.name ||
                            message?.source?.name ||
                            ""}
                        </h5>
                        <span className="muted labelDes text-ellipsis">
                          {message?.content || ""}
                        </span>
                      </div>
                      <span className="muted messagetime mt-1">
                        {" "}
                        <h5></h5>
                      </span>
                    </div>
                  ))}

                {messageData && messageData?.length <= 0 && (
                  <div className="mt-4">
                    <EmptyMessage description="No message found." />
                  </div>
                )}
              </div>
            </div>

            <div className="userMessageList">
              <div className="header-two">
                {" "}
                <Image
                  src={image || "/images/artist-placeholder.png"}
                  alt={""}
                  height={50}
                  width={50}
                />{" "}
                <h3>{name || "Artist Name"}</h3>
              </div>
              {isMessageDataLoading && <DefaultLoader />}
              <div className="messageList">
                {!isMessageDataLoading &&
                  allMessageData &&
                  allMessageData.length > 0 &&
                  allMessageData?.map((message: any, index: any) => (
                    <div key={message.objectId} className="messagedetails">
                      <div className="first">
                        <div className="message">{message.content}</div>
                        <div className="mt-1 show_name_date">
                          <span className="muted user_m">
                            <svg
                              height="20px"
                              width="20px"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 448 512"
                            >
                              <path d="M304 128a80 80 0 1 0 -160 0 80 80 0 1 0 160 0zM96 128a128 128 0 1 1 256 0A128 128 0 1 1 96 128zM49.3 464H398.7c-8.9-63.3-63.3-112-129-112H178.3c-65.7 0-120.1 48.7-129 112zM0 482.3C0 383.8 79.8 304 178.3 304h91.4C368.2 304 448 383.8 448 482.3c0 16.4-13.3 29.7-29.7 29.7H29.7C13.3 512 0 498.7 0 482.3z" />
                            </svg>
                            &nbsp; {message.source.name}
                          </span>
                          <span className="muted">
                            <CalendarIcon />
                            &nbsp;{" "}
                            {moment(message.date?.iso)
                              .utc()
                              .format("D-MM-YYYY hh:mm A")}
                          </span>
                        </div>
                      </div>

                      {loginUserState.data?.userType ===
                        LOGIN_USER_TYPE.ADMIN && (
                        <div className="aproveReject">
                          <Popconfirm
                            title="Are you sure you want to Decline?"
                            okText="Yes,Decline"
                            cancelText="No"
                            onConfirm={() =>
                              approveDecline("declined", message)
                            }
                          >
                            <span>
                              <DeleteMsgIcon />
                            </span>
                          </Popconfirm>

                          <Popconfirm
                            title="Are you sure you want to Aprrove?"
                            okText="Yes, Aprrove"
                            cancelText="No"
                            onConfirm={() =>
                              approveDecline("confirmed", message)
                            }
                          >
                            <span>
                              <CheckMsgIcon />
                            </span>
                          </Popconfirm>
                        </div>
                      )}
                    </div>
                  ))}

                {allMessageData && allMessageData?.length <= 0 && (
                  <div className="mt-4">
                    <EmptyMessage description="No messages found." />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MessageScane;
