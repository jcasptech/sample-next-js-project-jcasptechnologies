import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import {
  getAdminArtistMessageAPI,
  getArtistUserMessageAPI,
} from "@redux/services/admin.api";
import { postArtistMessageApproveRejectAPI } from "@redux/services/artist.api";
import { SeledtedArtistState } from "@redux/slices/artistList";
import { LoginUserState } from "@redux/slices/auth";
import {
  ArtistListMessageState,
  fetchArtistListMessage,
  fetchUserListMessage,
} from "@redux/slices/messages/artistList";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import MessageScane from "./messageScene";

export interface MessageContainerProps {}

const MessageContainer = (props: MessageContainerProps) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [messageId, setMessageId] = useState("");
  const [name, setName]: any = useState("Artist Name");
  const [image, setImage]: any = useState("");

  const {
    selectedArtist: { data: selectedArtistData },
  }: {
    selectedArtist: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    selectedArtist: state.selectedArtist,
  }));

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const [selectedId, setSelectedId] = useState("");
  const [userType, setUserType] = useState("");
  const [allMessageData, setAllMessageData] = useState<any[]>([]);

  const [isMessageDataLoading, setIsMessageDataLoading] = useState(false);

  const {
    artistListMessage: { isLoading, data: messageData },
  }: {
    artistListMessage: ArtistListMessageState;
  } = useSelector((state: RootState) => ({
    artistListMessage: state.artistListMessage,
  }));

  useEffect(() => {
    if (loginUserState.data?.userType === LOGIN_USER_TYPE.ADMIN) {
      setUserType("admin");
      dispatch(fetchArtistListMessage());
    } else if (loginUserState.data?.userType === LOGIN_USER_TYPE.MANAGER) {
      if (selectedArtistData?.objectId) {
        setUserType("artist");
        dispatch(fetchUserListMessage(selectedArtistData?.objectId));
      }
    }
  }, [selectedArtistData]);

  const getHandleArtistMessage = async (id: string) => {
    if (id) {
      setIsMessageDataLoading(true);
      try {
        let getRes = await getAdminArtistMessageAPI(id);
        setAllMessageData(getRes || []);
        setIsMessageDataLoading(false);
      } catch (error) {
        setIsMessageDataLoading(false);
        console.log(error);
      }
    }
  };

  const getHandleUserMessage = async (source: string) => {
    if (source) {
      setIsMessageDataLoading(true);
      try {
        let getRes = await getArtistUserMessageAPI(
          selectedArtistData?.objectId,
          source
        );
        setAllMessageData(getRes || []);
        setIsMessageDataLoading(false);
      } catch (error) {
        setIsMessageDataLoading(false);
        console.log(error);
      }
    }
  };

  useEffect(() => {
    if (messageData) {
      if (userType === "admin") {
        setName(messageData[0]?.receiver?.name);
        setImage(messageData[0]?.receiver?.iconImage?.url);
        setSelectedId(messageData[0]?.receiver?.objectId || "");
      } else if (userType === "artist") {
        setSelectedId(messageData[0]?.source?.objectId || "");
        setName(messageData[0]?.source?.name);
        setImage(messageData[0]?.source?.iconImage?.url);
      }
    } else {
      setName("");
      setImage("");
      setSelectedId("");
    }
  }, [messageData]);

  const handleApprovedMessage = async (messageId: any, status: any) => {
    if (messageId) {
      try {
        await postArtistMessageApproveRejectAPI({
          messageId: messageId,
          status: status,
        });
        showToast(SUCCESS_MESSAGES.messageApprovedSuccess, "success");
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRejectMessage = async (messageId: any, status: any) => {
    if (messageId) {
      try {
        await postArtistMessageApproveRejectAPI({
          messageId: messageId,
          status: status,
        });
        showToast(SUCCESS_MESSAGES.messageDeclinedSuccess, "success");
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleApproved = (d: any, data: any) => {
    if (d == "confirmed") {
      setMessageId(data);
      handleApprovedMessage(data.objectId, d);
    } else {
      setMessageId(data);
      handleRejectMessage(data.objectId, d);
    }
  };

  useEffect(() => {
    if (selectedId) {
      if (loginUserState.data?.userType === LOGIN_USER_TYPE.ADMIN) {
        getHandleArtistMessage(selectedId);
      } else {
        getHandleUserMessage(selectedId);
      }
    } else {
      setAllMessageData([]);
    }
  }, [selectedId]);

  const messageClick = (message: any) => {
    if (userType === "admin") {
      setName(message?.receiver?.name);
      setImage(message?.receiver?.iconImage?.url);
      setSelectedId(message?.receiver?.objectId);
    } else {
      setSelectedId(message?.source?.objectId);
      setName(message?.source?.name);
      setImage(message?.source?.iconImage?.url);
    }
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["artist_messages"]} fallback={<NoPermissionsComponent />}>
        <MessageScane
          {...{
            messageData,
            handleApproved,
            loginUserState,
            setSelectedId,
            allMessageData,
            setAllMessageData,
            userType,
            isMessageDataLoading,
            setIsMessageDataLoading,
            messageClick,
            name,
            image,
          }}
        />
      </Show>
    </>
  );
};
MessageContainer.Layout = AdminLayoutComponent;
export default MessageContainer;
