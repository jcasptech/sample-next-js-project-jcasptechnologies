import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { getOrderDetailAPI } from "@redux/services/checkout.api";
import { orderDetail } from "@redux/slices/general";
import { useEffect, useState } from "react";
import { nextRedirect } from "src/libs/helpers";
import SuccessScene from "./successScene";
export interface SuccessContainerProps {
  orderDetail: orderDetail;
}
const SuccessContainer = (props: SuccessContainerProps) => {
  const { orderDetail } = props;
  const [planDetail, setPlanDetail] = useState(null);

  useEffect(() => {
    if (orderDetail?.subscriptionplan_details) {
      setPlanDetail(JSON.parse(orderDetail.subscriptionplan_details));
    }
  }, [orderDetail]);
  return (
    <>
      <SuccessScene
        {...{
          orderDetail,
          planDetail,
        }}
      />
    </>
  );
};

SuccessContainer.getInitialProps = async (ctx: any) => {
  const session_id = ctx?.query?.session_id;
  let orderDetail = null;
  try {
    if (session_id) {
      const orderDetailRes: any = await getOrderDetailAPI(session_id);

      if (orderDetailRes) {
        if (orderDetailRes?.responseData) {
          orderDetail = orderDetailRes.responseData;
        } else {
          orderDetail = orderDetailRes;
        }
      }
    }
  } catch (e: any) {
    nextRedirect({ ctx, location: "/404" });
  }
  return {
    orderDetail,
  };
};

SuccessContainer.Layout = MainLayoutComponent;
export default SuccessContainer;
