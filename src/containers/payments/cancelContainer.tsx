import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { getOrderDetailAPI } from "@redux/services/checkout.api";
import { useEffect, useState } from "react";
import { nextRedirect } from "src/libs/helpers";
import CancelScene from "./cancelScene";
export interface CancelContainerProps {
  orderDetail: any;
}
const CancelContainer = (props: CancelContainerProps) => {
  const { orderDetail } = props;
  const [planDetail, setPlanDetail] = useState(null);

  useEffect(() => {
    if (orderDetail?.subscriptionplan_details) {
      setPlanDetail(JSON.parse(orderDetail.subscriptionplan_details));
    }
  }, [orderDetail]);

  return (
    <>
      <CancelScene
        {...{
          orderDetail,
          planDetail,
        }}
      />
    </>
  );
};

CancelContainer.getInitialProps = async (ctx: any) => {
  const session_id = ctx?.query?.session_id;
  let orderDetail = null;
  try {
    if (session_id) {
      const orderDetailRes: any = await getOrderDetailAPI(session_id);
      if (orderDetailRes) {
        if (orderDetailRes?.responseData) {
          orderDetail = orderDetailRes.responseData;
        } else {
          orderDetail = orderDetailRes;
        }
      }
    }
  } catch (e: any) {
    nextRedirect({ ctx, location: "/404" });
  }
  return {
    orderDetail,
  };
};

CancelContainer.Layout = MainLayoutComponent;
export default CancelContainer;
