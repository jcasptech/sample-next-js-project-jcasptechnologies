import { orderDetail } from "@redux/slices/general";
import Link from "next/link";
import { DateTime } from "luxon";
import { Button } from "@components/theme";
import router from "next/router";
import Image from "next/image";

export interface SuccessSceneProps {
  orderDetail: orderDetail;
  planDetail: any;
}
const SuccessScene = (props: SuccessSceneProps) => {
  const { orderDetail, planDetail } = props;
  return (
    <>
      <div className="payment">
        <div className="card card-p mx-auto">
          <div className="tic-mark">
            <Image
              className="card-img-top img-c h-auto"
              src="/images/about-us/green-tic.png"
              alt="Card image cap"
              width={80}
              height={80}
            />
          </div>
          <div className="card-body c-text-h">
            <h5 className="card-title ">Payment Successful</h5>
            <h4 className="c-text-h">${planDetail?.cost}</h4>
            <p className="card-text c-text-2">
              <strong>Transaction ID:</strong>{" "}
              {orderDetail?.client_reference_id}
            </p>
            <p className="card-text">
              {DateTime.fromISO(orderDetail?.createdAt).toFormat(
                "dd LLL, yyyy h:mm a"
              )}
            </p>
            <div className="btn-pad">
              <Button
                type="primary"
                htmlType="button"
                onClick={() => {
                  router.push("/");
                }}
              >
                Go to Home
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SuccessScene;
