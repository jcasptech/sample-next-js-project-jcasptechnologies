import ArtistRating from "@components/artistRatingComponent/artistRating";
import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import Link from "next/link";
import forgotPasswordStyles from "./forgotPassword.module.scss";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { useEffect, useState } from "react";
import { Button } from "@components/theme/button";
import Spinner from "@components/theme/spinner";
import AOS from "aos";
import "aos/dist/aos.css";

export interface ForgotPasswordProps {
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  register: any;
  formState: any;
  isLoading: boolean;
}

const ForgotPassword = (props: ForgotPasswordProps) => {
  const { register, formState, handleSubmit, onSubmit, isLoading } = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <section className="section-pad">
        <div className="container-fluid">
          <div className={`row m0 ${forgotPasswordStyles.mainBorder}`}>
            <div
              className={`col-12 col-sm-6 ${forgotPasswordStyles.account_box}`}
            >
              <div className={`row m0  ${forgotPasswordStyles.secondBorder}`}>
                <div className={`col-12 ${forgotPasswordStyles.abLbl}`}>
                  <h3
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Forgot your password?
                  </h3>
                  <p
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    No worries. We&apos;ll send a reset link to your inbox.
                  </p>
                </div>

                <div
                  className="col-12 form_holder"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <form
                    className={`${forgotPasswordStyles.formB}`}
                    onSubmit={handleSubmit(onSubmit)}
                  >
                    <div className={`${forgotPasswordStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "email",
                          label: "Email Address",
                          className: `${forgotPasswordStyles.input}`,
                          placeholder: "Enter your Email address",
                        }}
                      />
                    </div>

                    <div
                      className={`${forgotPasswordStyles.formB_fieldH} ${forgotPasswordStyles.centerDiv} mob-ctr`}
                    >
                      <Button
                        htmlType="submit"
                        type="primary"
                        disabled={formState?.isSubmitting || isLoading}
                        loading={formState?.isSubmitting || isLoading}
                      >
                        Reset Password
                      </Button>
                    </div>
                    <div
                      className={`${forgotPasswordStyles.formB_fieldH}  ${forgotPasswordStyles.centerDiv} reglnk mob-ctr`}
                    >
                      <p>
                        Go to login page?{" "}
                        <Link href="/login" className="ps-1">
                          Log In
                        </Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ForgotPassword;
