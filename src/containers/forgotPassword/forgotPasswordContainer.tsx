import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { forgotPasswordAPI } from "@redux/services/auth.api";
import { useReCaptcha } from "next-recaptcha-v3";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ForgotPasswordFormInputs,
  ForgotPasswordFormValidateSchema,
} from "src/schemas/forgotPasswordFormSchema";
import ForgotPassword from "./forgotPasswordScene";

const ForgotPasswordContainer = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { executeRecaptcha, loaded } = useReCaptcha();

  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<ForgotPasswordFormInputs>({
      resolver: yupResolver(ForgotPasswordFormValidateSchema),
    });

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      const recaptchaResponse = await executeRecaptcha("forgot_password");
      await forgotPasswordAPI(data, recaptchaResponse);
      showToast(SUCCESS_MESSAGES.resetPasswordRequest, "success");
      reset();
      setIsLoading(false);
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  return (
    <>
      <SEO
        {...{
          pageName: "/forgot-password",
        }}
      />
      <ForgotPassword
        {...{
          handleSubmit,
          onSubmit,
          register,
          formState,
          isLoading,
        }}
      />
    </>
  );
};

ForgotPasswordContainer.Layout = MainLayoutComponent;
export default ForgotPasswordContainer;
