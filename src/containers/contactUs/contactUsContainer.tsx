import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { postContactUsAPI } from "@redux/services/auth.api";
import { useReCaptcha } from "next-recaptcha-v3";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ContactUsFormInputs,
  ContactUsFormValidateSchema,
} from "src/schemas/contactUsFormSchema";
import ContactUsScene from "./contactUsScane";

export interface ContactUsContainerProps {}
const ContactUsContainer = (props: ContactUsContainerProps) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const { executeRecaptcha, loaded } = useReCaptcha();

  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<ContactUsFormInputs>({
      resolver: yupResolver(ContactUsFormValidateSchema),
    });

  const onSubmit = async (data: any) => {
    if (data) {
      setIsLoading(true);
      try {
        const recaptchaResponse = await executeRecaptcha("contactus");
        await postContactUsAPI(data, recaptchaResponse);
        showToast(SUCCESS_MESSAGES.contactMessageSuccess, "success");
        reset();
        setIsLoading(false);
      } catch (error) {
        console.log(error);
        setIsLoading(false);
      }
    }
  };

  return (
    <>
      <SEO
        {...{
          pageName: "/contact-us",
        }}
      />
      <ContactUsScene
        {...{
          register,
          formState,
          handleSubmit,
          onSubmit,
          isLoading,
        }}
      />
    </>
  );
};
ContactUsContainer.Layout = MainLayoutComponent;
export default ContactUsContainer;
