import NewsLetterSection from "@components/newsLetter";
import { useRouter } from "next/router";
import {
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import Spinner from "@components/theme/spinner";
import { Button } from "@components/theme";
import Image from "next/image";

export interface ContactUsSceneProps {
  register: any;
  formState: any;
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  isLoading: boolean;
}
const ContactUsScene = (props: ContactUsSceneProps) => {
  const { register, formState, handleSubmit, onSubmit, isLoading } = props;
  const router = useRouter();

  return (
    <>
      <section className="section-pad">
        <div className="container-fluid">
          <div className="row m0">
            <div className="col-12 col-sm-5 contact-text">
              <div className="row m0">
                <div className="col-12 contact-us">
                  <h3
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Contact Us <br /> We&apos;re Here To Help.
                  </h3>
                  <p
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Our team is here to answer any questions and help you with
                    whatever you need. Please fill out the form below and a
                    member of our team will get back to you as soon as possible.
                  </p>
                </div>

                <div
                  className="col-12 form_holder"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <form className="formB" onSubmit={handleSubmit(onSubmit)}>
                    <div className="fieldH">
                      <label>Full Name</label>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "name",
                          className: `input`,
                          placeholder: "Enter Full Name",
                        }}
                      />
                    </div>
                    <div className="fieldH">
                      <label>Email Address</label>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "email",
                          className: `input`,
                          placeholder: "Enter Email address",
                        }}
                      />
                    </div>
                    <div className="fieldH">
                      <label>Message</label>
                      <TextAreaField
                        {...{
                          register,
                          formState,
                          className: "input",
                          id: "message",
                          placeholder: "Write Your Message",
                        }}
                      />
                    </div>

                    <div className="fieldH mob-ctr submit-con">
                      <Button
                        htmlType="submit"
                        type="primary"
                        loading={isLoading}
                        disabled={isLoading}
                      >
                        Send Message
                      </Button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-7 contact-box">
              <div className="row m0">
                <div
                  className="col-12 maincontact"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  {/* <img src="/images/about-us/contact-us.png" alt="artist" /> */}
                  <Image
                    src="/images/about-us/contact-us.png"
                    alt="artist"
                    className="h-auto"
                    width={897}
                    height={874}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <NewsLetterSection />
    </>
  );
};

export default ContactUsScene;
