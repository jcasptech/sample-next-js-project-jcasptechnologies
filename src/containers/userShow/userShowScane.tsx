import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import UserShows from "@components/userShows";
import { UserShowsDataResponse } from "@redux/slices/userShows";
import UserShowsStyle from "./userShows.module.scss";

export interface UserShowsSceneProps {
  userShowsData: UserShowsDataResponse;
  handleFilter: (d: any) => void;
  isLoading: boolean | undefined;
  setIsNewShowModalOpen: (d: any) => void;
  handleDelete: (d: any) => void;
}
const UserShowsScene = (props: UserShowsSceneProps) => {
  const {
    userShowsData,
    handleFilter,
    isLoading,
    setIsNewShowModalOpen,
    handleDelete,
  } = props;
  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          {/* Row of Add new */}
          <div className={`row m0 ${UserShowsStyle.shows}`}>
            <div className="">
              <Button
                type="primary"
                htmlType="button"
                className={`float-right`}
                onClick={() => setIsNewShowModalOpen(true)}
              >
                Add New Show
              </Button>
            </div>
          </div>
          {/* Row of Add new */}

          {/* <div className="row m0">
            <div className="col-12">
              <div className="search_form shows-search-form col-12">
                <input
                  type="text"
                  placeholder="Search for Artist / Venue"
                  className="search_art aos-init aos-animate"
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onKeyUp={(e: any) => {
                    handleFilter({
                      search: e.target?.value || "",
                    });
                  }}
                /> */}
          {/* <!-- <div className="srchCnt aos-init aos-animate" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1200">
                           
                        </div> --> */}
          {/* </div>
            </div>
          </div> */}

          <div className="col-12 col-sm-12 col-lg-12 shows_holder">
            {/* <!-- Tab Content --> */}

            <div className="card card2 border-0">
              <div className="card-body">
                <div className="tab-content" id="tabcontent1">
                  <div
                    className="tab-pane fade show active"
                    id="tabs-text-1"
                    role="tabpanel"
                  >
                    <div className="row">
                      {userShowsData &&
                        userShowsData?.list &&
                        userShowsData?.list?.length > 0 &&
                        userShowsData?.list?.map((shows: any, index) => (
                          <UserShows
                            key={index}
                            {...{
                              shows,
                              handleDelete,
                            }}
                          />
                        ))}
                      {isLoading && <DefaultLoader />}
                      {userShowsData && userShowsData.hasMany && (
                        <div className="col-12 load_more">
                          <Button
                            htmlType="button"
                            type="ghost"
                            loading={isLoading}
                            disabled={isLoading}
                            onClick={(e) => {
                              e.preventDefault();
                              handleFilter({
                                loadMore: true,
                              });
                            }}
                          >
                            Load more Shows
                          </Button>
                        </div>
                      )}
                      {userShowsData &&
                        userShowsData?.list &&
                        userShowsData?.list?.length <= 0 && (
                          <EmptyMessage description="No shows found." />
                        )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserShowsScene;
