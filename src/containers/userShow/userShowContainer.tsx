import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import AdminAddShowModal from "@components/modals/adminAddShowModal";
import ArtistAddShowModal from "@components/modals/artistAddShowModal";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { deleteShowsAPI } from "@redux/services/shows.api";
import { LoginUserState } from "@redux/slices/auth";
import {
  fetchAdimnShows,
  fetchArtistShows,
  loadMoreAdminShows,
  loadMoreArtistShows,
  UserShowsState,
} from "@redux/slices/userShows";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import UserShowsScene from "./userShowScane";

const UserShowContainer = () => {
  const dispatch = useDispatch();
  const selectArtistId: any = useSelector((state: RootState) => state.selectId);
  const [isShowModalOpen, setIsNewShowModalOpen] = useState(false);
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const {
    userShows: { isLoading, data: userShowsData },
  }: {
    userShows: UserShowsState;
  } = useSelector((state: RootState) => ({
    userShows: state.userShows,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 9,
      skip: 0,
      include: ["artist", "venue"],
    },
  });

  const getData = () => {
    if (
      selectArtistId?.data &&
      loginUserState?.data.userType === LOGIN_USER_TYPE.MANAGER
    ) {
      dispatch(fetchArtistShows(selectArtistId?.data, apiParam));
    } else {
      dispatch(fetchAdimnShows(apiParam));
    }
  };

  const handleFilter = (data: any) => {
    if (data.search) {
      apiParam.search = data.search;
      getData();
    } else {
      apiParam.search = "";
    }
    if (
      data.loadMore &&
      loginUserState?.data.userType === LOGIN_USER_TYPE.MANAGER
    ) {
      apiParam.skip = (apiParam.skip || 0) + 9;
      dispatch(loadMoreArtistShows(selectArtistId?.data, apiParam));
    } else {
      apiParam.skip = (apiParam.skip || 0) + 9;
      dispatch(loadMoreAdminShows(apiParam));
    }
  };

  useEffect(() => {
    getData();
  }, [selectArtistId.data, apiParam]);

  const handleDelete = (id: any) => {
    if (id) {
      try {
        deleteShowsAPI(id);
        showToast(SUCCESS_MESSAGES.deleteShows, "success");
        getData();
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["users_shows"]} fallback={<NoPermissionsComponent />}>
        <UserShowsScene
          {...{
            userShowsData,
            handleFilter,
            isLoading,
            setIsNewShowModalOpen,
            handleDelete,
          }}
        />
      </Show>

      {loginUserState?.data.userType === LOGIN_USER_TYPE.MANAGER &&
        isShowModalOpen && (
          <ArtistAddShowModal
            {...{
              setIsNewShowModalOpen,
              isOpen: isShowModalOpen,
              artistId: selectArtistId.data,
              handleClose: (e) => {
                getData();
              },
            }}
          />
        )}

      {(loginUserState?.data.userType === LOGIN_USER_TYPE.ADMIN ||
        loginUserState?.data.userType === LOGIN_USER_TYPE.VENUE_ADMIN) &&
        isShowModalOpen && (
          <AdminAddShowModal
            {...{
              setIsNewShowModalOpen,
              isOpen: isShowModalOpen,
              artistId: selectArtistId.data,
              handleClose: (e) => {
                getData();
              },
            }}
          />
        )}
    </>
  );
};

UserShowContainer.Layout = AdminLayoutComponent;
export default UserShowContainer;
