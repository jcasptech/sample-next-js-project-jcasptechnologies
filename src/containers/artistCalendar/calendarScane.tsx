import CustomDropdown from "@components/theme/dropdown";
import FilterDropdown, {
  FilterDropdownTypes,
} from "@components/theme/dropdown/filter";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import FullCalendar from "@fullcalendar/react";
import timeGridPlugin from "@fullcalendar/timegrid";
import CalendarStyles from "./calendarContainer.module.scss";
import { Button } from "@components/theme";
import Image from "next/image";

export interface CalendarScaneProps {
  handleDateClick: (d: any) => void;
  handleDatesSet: (d: any) => void;
  allEvents: any[];
  handleEventContent: (d: any) => void;
  calendarRef: any;
  handlePrevClick: (d: any) => void;
  handleNextClick: (d: any) => void;
  handleTodayClick: (d: any) => void;
  calendarView:
    | "dayGridMonth"
    | "timeGridWeek"
    | "timeGridDay"
    | "listMonth"
    | "listWeek"
    | "listDay";
  setCalendarView: (d: any) => void;
  dayCellContent: (d: any) => void;
  dayHeaderContent: (d: any) => void;
  currentMonth: string;
  handleEventClick: (d: any) => void;
  filters: FilterDropdownTypes[];
  setFilters: (d: FilterDropdownTypes[]) => void;
  setIsNewShowModalOpen: (d: any) => void;
}

const CalendarScane = (props: CalendarScaneProps) => {
  const {
    handleDateClick,
    handleDatesSet,
    allEvents,
    handleEventContent,
    calendarRef,
    handlePrevClick,
    handleNextClick,
    handleTodayClick,
    calendarView,
    setCalendarView,
    dayCellContent,
    dayHeaderContent,
    currentMonth,
    handleEventClick,
    filters,
    setFilters,
    setIsNewShowModalOpen,
  } = props;

  return (
    <>
      <div className="message-page h-100vh">
        <div className={`panel-inner-content ${CalendarStyles.calendar_page}`}>
          <div className="card card2 border-0">
            <div className="card-body p-0 pb-4">
              <div className={`${CalendarStyles.calendar_heading}`}>
                <div className="filter-1">
                  <div className="schedule">
                    <label>Schedule :</label>
                    <CustomDropdown
                      onChange={setCalendarView}
                      defaultValue={calendarView}
                      isShowCheckBox={true}
                      options={[
                        { value: "dayGridMonth", label: "Month" },
                        { value: "timeGridWeek", label: "Week" },
                        { value: "timeGridDay", label: "Day" },
                        { value: "listMonth", label: "List(Mo)" },
                        { value: "listWeek", label: "List(Wk)" },
                        { value: "listDay", label: "List(day)" },
                      ]}
                    />
                  </div>
                  <div className="calendar-info">
                    <div className="actions">
                      <button type="button" onClick={handlePrevClick}>
                        <Image
                          src="/images/general/left.svg"
                          alt="previous"
                          width={6}
                          height={11}
                        />
                      </button>
                      <div>
                        <Image
                          src="/images/general/calendar.svg"
                          alt="Calendar"
                          width={22}
                          height={19}
                        />
                        <div>{currentMonth}</div>
                      </div>
                      <button type="button" onClick={handleNextClick}>
                        <Image
                          src="/images/general/right.svg"
                          alt="Next"
                          width={6}
                          height={11}
                        />
                      </button>
                    </div>
                    <div className="today">
                      <span></span>
                      <button type="button" onClick={handleTodayClick}>
                        Today
                      </button>
                    </div>
                  </div>
                  <div className="calendar-filter">
                    <div>
                      <FilterDropdown
                        onChange={setFilters}
                        placeholder="Filter calendar items"
                        options={filters}
                      />
                    </div>
                  </div>
                </div>
                <div className="mobile_filter">
                  <div className="schedule">
                    <label>Schedule :</label>
                    <CustomDropdown
                      onChange={setCalendarView}
                      defaultValue={calendarView}
                      isShowCheckBox={true}
                      options={[
                        { value: "dayGridMonth", label: "Month" },
                        { value: "timeGridWeek", label: "Week" },
                        { value: "timeGridDay", label: "Day" },
                        { value: "listMonth", label: "List(Mo)" },
                        { value: "listWeek", label: "List(Wk)" },
                        { value: "listDay", label: "List(day)" },
                      ]}
                    />
                  </div>
                  <div className="calendar-filter">
                    <div>
                      <FilterDropdown
                        onChange={setFilters}
                        placeholder="Filter calendar items"
                        options={filters}
                      />
                    </div>
                  </div>
                </div>
                <div className="filter-2">
                  <Button
                    htmlType="button"
                    type="primary"
                    className={`mt-1 float-right`}
                    onClick={() => {
                      setIsNewShowModalOpen(true);
                    }}
                  >
                    Add Show
                  </Button>
                </div>
              </div>
              <div className="pt-4">
                <FullCalendar
                  viewClassNames={CalendarStyles.calendar_view}
                  dayCellClassNames={CalendarStyles.calendar_day_cell}
                  ref={calendarRef}
                  plugins={[
                    dayGridPlugin,
                    timeGridPlugin,
                    interactionPlugin,
                    listPlugin,
                  ]}
                  dateClick={handleDateClick}
                  datesSet={handleDatesSet}
                  events={allEvents}
                  eventContent={handleEventContent}
                  dayMaxEvents={true}
                  dayCellContent={dayCellContent}
                  dayHeaderContent={dayHeaderContent}
                  headerToolbar={false}
                  eventClick={handleEventClick}
                  timeZone={"utc"}
                  firstDay={1}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CalendarScane;
