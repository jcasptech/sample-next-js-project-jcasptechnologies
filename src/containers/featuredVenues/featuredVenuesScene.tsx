import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { FeaturedVenueDataResponse } from "@redux/slices/featuredVenue/featuredVenue";
import { VenueObject } from "@redux/slices/venues";
import { Col, Row } from "antd";
import SingleVenue from "./single-venue";

export interface FeaturedVenuesSceneProps {
  featuredVenues: FeaturedVenueDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  isActionLoading: boolean;
  handleVenue: (d: any) => void;
  handleFilter: (d: any) => void;
  setIsEdit: (d: VenueObject) => void;
  generateScore: (d: any) => void;
}

const FeaturedVenuesScene = (props: FeaturedVenuesSceneProps) => {
  const {
    featuredVenues,
    handleLoadMore,
    isLoading,
    isActionLoading,
    handleVenue,
    handleFilter,
    setIsEdit,
    generateScore,
  } = props;

  return (
    <>
      <div className={`main-section admin-panel-section`}>
        <section className="section">
          <Row gutter={[20, 20]}>
            <Col sm={12} xs={24}>
              <div className="search_form w-100">
                <input
                  type="text"
                  placeholder="Search Venue"
                  className="search_art aos-init aos-animate"
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onChange={(e: any) => {
                    handleFilter({ search: e.target?.value || "" });
                  }}
                />
              </div>
            </Col>
            <Col sm={12} xs={24}>
              <Button
                type="primary"
                htmlType="button"
                className="float-right"
                onClick={generateScore}
                loading={isActionLoading}
                disabled={isActionLoading}
              >
                Generate Score
              </Button>
            </Col>
          </Row>
        </section>
        <section className="section h-75vs">
          <Row gutter={[20, 20]}>
            {featuredVenues &&
              featuredVenues?.list &&
              featuredVenues?.list.length > 0 &&
              featuredVenues.list.map((venue: VenueObject, index: number) => (
                <SingleVenue
                  key={`venue-${index}`}
                  {...{
                    venue,
                    handleVenue,
                    setIsEdit,
                  }}
                />
              ))}
          </Row>

          {featuredVenues &&
            featuredVenues?.list &&
            featuredVenues?.list.length <= 0 && (
              <Row gutter={[30, 30]} justify="center">
                <Col>
                  <EmptyMessage description="No venues found." />
                </Col>
              </Row>
            )}

          {featuredVenues?.hasMany && featuredVenues?.hasMany === true && (
            <Row justify={"center"} className="mt-4">
              <Col>
                <Button
                  htmlType="button"
                  type="ghost"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={(e: any) => {
                    e.preventDefault();
                    handleLoadMore(true);
                  }}
                >
                  Load more
                </Button>
              </Col>
            </Row>
          )}
        </section>
      </div>
    </>
  );
};

export default FeaturedVenuesScene;
