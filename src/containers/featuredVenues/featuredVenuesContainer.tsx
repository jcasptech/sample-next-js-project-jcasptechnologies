import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import VenueScoreModal from "@components/modals/venueScoreModal";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { generatesScoreFeaturedVenueAPI } from "@redux/services/venue.api";
import {
  FeaturedVenueState,
  fetchFeaturedVenue,
  loadMoreFeaturedVenue,
} from "@redux/slices/featuredVenue/featuredVenue";
import { VenueObject } from "@redux/slices/venues";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Show } from "react-redux-permission";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import FeaturedVenuesScene from "./featuredVenuesScene";

export interface FeaturedVenuesContainerProps {}

const FeaturedVenuesContainer = (props: FeaturedVenuesContainerProps) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const [isEdit, setIsEdit] = useState<VenueObject | null>(null);
  const [isActionLoading, setIsActionLoading] = useState(false);

  const {
    featuredVenue: { isLoading, data: featuredVenues },
  }: {
    featuredVenue: FeaturedVenueState;
  } = useSelector((state: RootState) => ({
    featuredVenue: state.featuredVenue,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
    },
  });

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 8;
    dispatch(loadMoreFeaturedVenue(apiParam));
  };

  const handleVenue = (venueSlug: string) => {
    if (venueSlug) {
      router.push(`/venue/${venueSlug}`);
    }
  };

  const handleFilter = (data: any) => {
    apiParam.skip = 0;
    apiParam.search = data?.search;
    dispatch(fetchFeaturedVenue(apiParam));
  };

  const generateScore = async () => {
    try {
      setIsActionLoading(true);
      await generatesScoreFeaturedVenueAPI();
      showToast(SUCCESS_MESSAGES.generatesScoreSuccess, "success");
      setIsActionLoading(false);
      apiParam.skip = 0;
      apiParam.search = "";
      dispatch(fetchFeaturedVenue(apiParam));
    } catch (error) {
      console.log(error);
      setIsActionLoading(false);
    }
  };

  useEffect(() => {
    dispatch(fetchFeaturedVenue(apiParam));
  }, []);

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["featured_venues"]} fallback={<NoPermissionsComponent />}>
        <FeaturedVenuesScene
          {...{
            featuredVenues,
            handleLoadMore,
            isLoading,
            isActionLoading,
            handleVenue,
            handleFilter,
            setIsEdit,
            generateScore,
          }}
        />
      </Show>

      {isEdit && (
        <VenueScoreModal
          {...{
            venueId: isEdit.objectId,
            handleClose: (e) => {
              setIsEdit(null);
              if (e) {
                apiParam.skip = 0;
                apiParam.search = "";
                dispatch(fetchFeaturedVenue(apiParam));
              }
            },
          }}
        />
      )}
    </>
  );
};

FeaturedVenuesContainer.Layout = AdminLayoutComponent;
export default FeaturedVenuesContainer;
