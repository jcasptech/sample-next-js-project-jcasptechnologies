import { Button } from "@components/theme/button";
import {
  InputField,
  InputPasswordField,
  InputPasswordFieldWithMessage,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import FacebookLogin from "@greatsumini/react-facebook-login";
import { FacebookLoginPayload } from "@redux/slices/auth";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";
import { FACEBOOK_APP_ID } from "src/libs/constants";
import signUpStyles from "./signUp.module.scss";

export interface SignUpPageProps {
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  register: any;
  formState: any;
  handleFacebookLogin: (d: FacebookLoginPayload) => void;
  isLoading: boolean;
  handleChecked: (d: any) => void;
}

const SignUpPage = (props: SignUpPageProps) => {
  const {
    register,
    formState,
    handleSubmit,
    onSubmit,
    handleFacebookLogin,
    isLoading,
    handleChecked,
  } = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <section className="section-pad">
        <div className="container-fluid">
          <div className={`row m0  ${signUpStyles.mainBox}`}>
            <div className={`col-12 col-sm-6 ${signUpStyles.account_box}`}>
              <div className={`row m0 ${signUpStyles.borderDiv}`}>
                <div className={`col-12 ${signUpStyles.abLbl}`}>
                  <h3
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Get Started with JCasp
                  </h3>
                  <p
                    className={signUpStyles.makeOffer}
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Sign up for JCasp to connect with artists and discover
                    live music in your area.
                  </p>
                </div>
                <div className={`col-12 fbSign ${signUpStyles.fbSign}`}>
                  <FacebookLogin
                    className="hvr-float-shadow facebook-button"
                    appId={FACEBOOK_APP_ID}
                    onSuccess={(response) => {
                      handleFacebookLogin({
                        facebookToken: response.accessToken,
                        type: "SIGNUP",
                      });
                    }}
                    onFail={(error) => {
                      showToast("Login Failed!", "error");
                    }}
                  >
                    <Image
                      src="/images/fb-in.png"
                      alt="Signin with Facebook"
                      width={503}
                      height={52}
                      className="h-auto"
                    />
                  </FacebookLogin>
                </div>
                <div
                  className={`col-12 ${signUpStyles.orline}`}
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <p className={signUpStyles.lineThrough}>or with email</p>
                </div>
                <div
                  className="col-12 form_holder"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <form
                    className={`${signUpStyles.formB}`}
                    onSubmit={handleSubmit(onSubmit)}
                  >
                    <div className={`${signUpStyles.formB_fieldH}`}>
                      <div className={signUpStyles.formFl}>
                        <div className={`${signUpStyles.formFl__firstInput}`}>
                          <InputField
                            {...{
                              register,
                              formState,
                              id: "firstName",
                              label: "First Name",
                              className: `${signUpStyles.input}`,
                              placeholder: "Enter first name",
                              autoComplete: false,
                            }}
                          />
                        </div>
                        <div className={`${signUpStyles.formFl__lastInput}`}>
                          <InputField
                            {...{
                              register,
                              formState,
                              id: "lastName",
                              label: "Last Name",
                              className: `${signUpStyles.input}`,
                              placeholder: "Enter last name",
                              autoComplete: false,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className={`${signUpStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "userName",
                          label: "Create Username",
                          className: `${signUpStyles.input}`,
                          placeholder: "Enter username",
                          autoComplete: false,
                        }}
                      />
                    </div>
                    <div className={`${signUpStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "email",
                          label: "Email Address",
                          className: `${signUpStyles.input}`,
                          placeholder: "Enter Email Address",
                          autoComplete: false,
                        }}
                      />
                    </div>
                    <div className={`${signUpStyles.formB_fieldH}`}>
                      <InputPasswordFieldWithMessage
                        {...{
                          register,
                          formState,
                          id: "password",
                          label: "Create Password",
                          className: `${signUpStyles.input}`,
                          placeholder: "Enter your Password",
                          autoComplete: false,
                        }}
                      />
                    </div>

                    <div className={`${signUpStyles.formB_fieldH}`}>
                      <InputPasswordField
                        {...{
                          register,
                          formState,
                          id: "confirmPassword",
                          label: "Confirm Password",
                          className: `${signUpStyles.input}`,
                          placeholder: "Enter confirm your Password",
                          autoComplete: false,
                        }}
                      />
                    </div>

                    <div className={`${signUpStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "referral",
                          label: "Referral/How Did You Hear About Us",
                          className: `${signUpStyles.input}`,
                          placeholder: "Instagram, Venue, Friend, etc.",
                          autoComplete: false,
                        }}
                      />
                    </div>

                    <div className="panel-form">
                      <div className="input-group">
                        <div className="checkbox checkbox-info">
                          <input
                            type="checkbox"
                            id="makeArtistCheckbox"
                            className="valid"
                            aria-required="true"
                            aria-invalid="false"
                            onClick={handleChecked}
                          />
                          <label htmlFor="makeArtistCheckbox">
                            I am Signing Up as an Artist
                          </label>
                        </div>
                        <div />
                      </div>
                    </div>

                    <div
                      className={`${signUpStyles.formB_fieldH} ${signUpStyles.centerDiv} mob-ctr`}
                    >
                      <Button
                        htmlType="submit"
                        type="primary"
                        disabled={formState?.isSubmitting || isLoading}
                        loading={formState?.isSubmitting || isLoading}
                      >
                        Sign Up
                      </Button>
                    </div>
                    <div
                      className={`${signUpStyles.formB_fieldH} ${signUpStyles.centerDiv} reglnk mob-ctr`}
                    >
                      <p>
                        Already have an account?{" "}
                        <Link href="/login" className="ps-1">
                          Log In
                        </Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            {/* <--Artist Rating--> */}
            {/* <ArtistRating {...{ redirectPage: "/login" }} /> */}
            {/* <--Artist Rating--> */}
          </div>
        </div>
      </section>
    </>
  );
};
export default SignUpPage;
