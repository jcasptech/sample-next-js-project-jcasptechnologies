import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { signUpAPI } from "@redux/services/auth.api";
import { doFacebookLogin, FacebookLoginPayload } from "@redux/slices/auth";
import { useReCaptcha } from "next-recaptcha-v3";
import router from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  SignupFormInputs,
  SignupFormValidateSchema,
} from "src/schemas/signupFormSchema";
import SignUpPage from "./signUpScene";

const SignUpContainer = () => {
  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();

  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<SignupFormInputs>({
      resolver: yupResolver(SignupFormValidateSchema),
    });

  const [isLoading, setIsLoading] = useState(false);
  const [isChecked, setIsChecked] = useState(false);

  const handleChecked = (d: any) => {
    setIsChecked(!isChecked);
  };

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    data["isArtist"] = isChecked ? "true" : "false";
    try {
      const recaptchaResponse = await executeRecaptcha("signup");
      await signUpAPI(data, recaptchaResponse);
      showToast(SUCCESS_MESSAGES.signUp, "success");
      reset();
      setIsLoading(false);
      router.push(`/login`);
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  const handleFacebookLogin = async (data: FacebookLoginPayload) => {
    const recaptchaResponse = await executeRecaptcha("signup");
    dispatch(doFacebookLogin(data, recaptchaResponse));
  };

  return (
    <>
      <SEO
        {...{
          pageName: "/signup",
        }}
      />

      <SignUpPage
        {...{
          handleSubmit,
          onSubmit,
          register,
          formState,
          handleFacebookLogin,
          isLoading,
          handleChecked,
        }}
      />
    </>
  );
};

SignUpContainer.Layout = MainLayoutComponent;
export default SignUpContainer;
