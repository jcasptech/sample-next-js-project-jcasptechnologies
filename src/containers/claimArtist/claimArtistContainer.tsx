import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@redux/reducers";
import { useEffect, useState } from "react";
import useList from "src/libs/useList";
import { DefaultSkeleton } from "@components/theme";
import ArtistClaimScene from "./claimArtistScane";
import {
  fetchClaimArtist,
  loadMoreClaimArtist,
  CliamArtistState,
} from "@redux/slices/claimArtist";
import { useRouter } from "next/router";
import CreateClaimArtistModal from "@components/modals/createclaimArtistModal";
import { Show } from "react-redux-permission";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";

const ClaimArtistContainer = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [isClaimed, setIsClaimed] = useState("false");
  const [isClaimArtistModalOpen, setIsclaimArtistModalOpen] = useState(false);
  const {
    claimArtist: { isLoading, data: claimArtistData },
  }: {
    claimArtist: CliamArtistState;
  } = useSelector((state: RootState) => ({
    claimArtist: state.claimArtist,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
      isClaimed: "false",
    },
  });

  const getData = () => {
    if (apiParam) {
      dispatch(fetchClaimArtist(apiParam));
    }
  };

  useEffect(() => {
    apiParam.isClaimed = isClaimed;
    apiParam.take = 8;
    apiParam.skip = 0;
    getData();
  }, [isClaimed]);

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 8;
    dispatch(loadMoreClaimArtist(apiParam));
  };

  const handleFilter = (data: any) => {
    if (data.search) {
      apiParam.search = data.search || "";
      getData();
    } else {
      apiParam.search = "";
      getData();
    }
  };

  const handleArtist = (artistSlug: string) => {
    if (artistSlug) {
      router.push(`/artist/${artistSlug}`);
    }
  };

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["claimed_artists"]} fallback={<NoPermissionsComponent />}>
        <ArtistClaimScene
          {...{
            handleFilter,
            setIsClaimed,
            isClaimed,
            claimArtistData,
            isLoading,
            handleLoadMore,
            handleArtist,
            setIsclaimArtistModalOpen,
          }}
        />
      </Show>

      {isClaimArtistModalOpen && (
        <CreateClaimArtistModal
          {...{
            isOpen: isClaimArtistModalOpen,
            setIsclaimArtistModalOpen,
            handleClose: (e) => {
              dispatch(fetchClaimArtist(apiParam));
            },
          }}
        />
      )}
    </>
  );
};

ClaimArtistContainer.Layout = AdminLayoutComponent;
export default ClaimArtistContainer;
