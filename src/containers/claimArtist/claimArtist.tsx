import { BellIcon } from "@components/theme/icons/bellIcon";
import { ArtistObject } from "@redux/slices/artists";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { capitalizeString, getFormatReview } from "src/libs/helpers";
import claimArtistStyle from "./claimArtist.module.scss";

export interface ClaimArtistProps {
  artist: ArtistObject;
  handleArtist: (d: any) => void;
}

const ClaimArtist = (props: ClaimArtistProps) => {
  const { artist, handleArtist } = props;

  const router = useRouter();

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <div
      className="col-12 col-sm-4 col-md-3 acHold"
      // data-aos="fade-down"
      // data-aos-delay="100"
      // data-aos-duration="1200"
    >
      <div className="ArtCard float-left w-100">
        <div className={claimArtistStyle.thumb}>
          <Image
            className="cursor-pointer"
            src={artist.posterImage?.url || "/images/artist-placeholder.png"}
            alt={artist.name}
            onClick={(e) => handleArtist(artist.vanity)}
            width={375}
            height={260}
          />
        </div>
        <div
          className={`title w-100 float-left cursor-pointer ${claimArtistStyle.title}`}
          onClick={(e) => handleArtist(artist.vanity)}
        >
          {capitalizeString(artist.name)}
        </div>
        {/* <!-- note: provision is provided upto 2 lines in the title --> */}
        {/* <div className="reviews float-left w-100 vcenter">
          <img src="/images/rate.png" alt="rate" />
          &nbsp;{artist.rating} ( {getFormatReview(artist.ratingCount)} Reviews
          )
        </div> */}
        <div className="abstract w-100 float-left two-line-and-text-ellipsis ">
          {artist.metadata?.about
            ? `${
                artist.metadata.about.length >= 190
                  ? artist.metadata.about.substring(0, 190) + "..."
                  : artist.metadata.about
              }`
            : ""}
        </div>
      </div>
    </div>
  );
};

export default ClaimArtist;
