import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { ArtistObject } from "@redux/slices/artists";
import { ClaimArtistDataResponse } from "@redux/slices/claimArtist";
import Artist from "../browseArtist/artist";
import claimStyle from "./claimArtist.module.scss";
import claimArtistStyle from "./claimArtist.module.scss";
import Link from "next/link";
import ClaimArtist from "./claimArtist";

export interface ArtistClaimSceneProps {
  handleLoadMore: (d: any) => void;
  handleFilter: (d: any) => void;
  setIsClaimed: (d: any) => void;
  isClaimed: string;
  claimArtistData: ClaimArtistDataResponse;
  isLoading: boolean | undefined;
  handleArtist: (d: any) => void;
  setIsclaimArtistModalOpen: (d: any) => void;
}

const ArtistClaimScene = (props: ArtistClaimSceneProps) => {
  const {
    handleFilter,
    setIsClaimed,
    isClaimed,
    claimArtistData,
    isLoading,
    handleLoadMore,
    handleArtist,
    setIsclaimArtistModalOpen,
  } = props;

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          {/* Row of Add new */}
          <div className="row shows-venue-pad m0">
            <div className="register_btn btn-shows">
              <Button
                type="primary"
                htmlType="button"
                className={`float-right mb-2`}
                onClick={() => setIsclaimArtistModalOpen(true)}
              >
                Create New
              </Button>
            </div>
          </div>
          {/* Row of Add new */}

          <div className="row m0 vcenter">
            <div className={`col-12 col-lg-6 ${claimStyle.searchInput}`}>
              <div className="search_form shows-search-form col-12">
                <input
                  type="text"
                  placeholder="Search for Artist"
                  className="search_art aos-init aos-animate"
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                  onChange={(e: any) => {
                    handleFilter({ search: e.target?.value || "" });
                  }}
                />
              </div>
            </div>
            <div
              className={`col-12 col-lg-6 col-md-6 ${claimArtistStyle.show_tab}`}
            >
              <div className="nav-wrapper">
                <ul
                  className="nav nav-pills nav-fill flex-row flex-md-row"
                  id="tabs-text"
                  role="tablist"
                >
                  <li className="nav-item">
                    <a
                      className={`nav-link ${
                        isClaimed === "false" ? "active disabled" : ""
                      }`}
                      onClick={(e) => {
                        e.preventDefault();
                        setIsClaimed("false");
                      }}
                    >
                      Unclaimed Artist
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className={`nav-link ${
                        isClaimed === "true" ? "active disabled" : ""
                      }`}
                      onClick={(e) => {
                        e.preventDefault();
                        setIsClaimed("true");
                      }}
                    >
                      Claimed Artist
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div className={`row m0 load_artists`}>
            {claimArtistData &&
              claimArtistData?.list &&
              claimArtistData?.list.length > 0 &&
              claimArtistData.list.map(
                (artist: ArtistObject, index: number) => (
                  <ClaimArtist
                    key={`artist-${index}`}
                    {...{
                      artist,
                      handleArtist,
                    }}
                  />
                )
              )}

            {claimArtistData &&
              claimArtistData?.list &&
              claimArtistData?.list.length <= 0 && (
                <EmptyMessage description="No artists found." />
              )}

            {claimArtistData?.hasMany && claimArtistData?.hasMany === true && (
              <div className={`col-12 load_more mb-3`}>
                <Button
                  type="ghost"
                  htmlType="button"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={(e: any) => {
                    e.preventDefault();
                    handleLoadMore("artist");
                  }}
                >
                  Load more Artists
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default ArtistClaimScene;
