import { Row } from "antd";
import Link from "next/link";

const ChatGTPtermsPageScene = () => {
  return (
    <>
      <div className="lkPage">
        <div className="lkPageHeader">
          <div className="flex justify-between">
            <h1 className="text-2xl fw-700">JCasp Plugin Terms of Use</h1>
          </div>
        </div>
        <Row className="d-flex" align="middle">
          <div className="lkFormCard bg-white">
            <div className="lkPageContent">
              <div className="lkCard">
                <form>
                  <div>
                    <div className="lkCardContent">
                      <div id="termsAndConditions">
                        <p>
                          <strong>Last Updated:</strong> [July 29, 2023]
                        </p>
                        <p>
                          <strong>Effective Date:</strong> [July 29, 2023]
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          1. Introduction
                        </h1>
                        <p>
                          Welcome to the JCasp plugin (&quot;Plugin&quot;).
                          This Plugin is a tool provided by Local Music, LLC
                          (&quot;we&quot;, &quot;us&quot;, &quot;our&quot;) that
                          offers information about local musicians and live
                          music events, designed to enhance your musical
                          experience. By using our Plugin, you
                          (&quot;User&quot;, &quot;you&quot;, &quot;your&quot;)
                          agree to these Terms of Use. If you do not agree,
                          please do not use the Plugin.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          2. Description of Service
                        </h1>
                        <p>
                          The Plugin allows users to get information about local
                          musicians and where they are performing. The Plugin
                          engages in chat and provides links to artist and venue
                          profiles on JCasp. The service is provided &quot;as
                          is&quot; and may change or be discontinued at any
                          time. The Plugin is subject to the terms and
                          conditions of the platform it&apos;s hosted on,
                          including OpenAI&apos;s use and data policies.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          3. User Privacy
                        </h1>
                        <p>
                          We respect the privacy of Plugin users and comply with
                          all applicable data protection and privacy laws in the
                          collection, use, and disclosure of your personal
                          information. We may collect certain usage data and
                          information about you when you use the Plugin, such as
                          prompts entered, clicks on links, and any information
                          you directly provide to us. We use this information to
                          analyze Plugin usage, improve the Plugin, and
                          customize the user experience.
                        </p>
                        <p>
                          We will not share any personal user data with third
                          parties, except as necessary to provide the Plugin
                          services, comply with the law, or protect our rights.
                          Information may be aggregated for reporting purposes,
                          but these reports will not identify individual users.
                        </p>
                        <p>
                          For more detailed information about our privacy
                          practices please see our Terms and Privacy Page. Our
                          Privacy Policy is incorporated into these Terms of
                          Use. By agreeing to these Terms, you agree to the
                          collection and use of your information as set forth in
                          our Privacy Policy.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          4. User Responsibilities
                        </h1>
                        <p>
                          Users are responsible for the data they provide to the
                          Plugin and any consequences resulting from their
                          misuse of the Plugin, including any legal or financial
                          implications. Users should not send personal or
                          company sensitive data when using the Plugin. Users
                          agree to use the Plugin in compliance with all
                          applicable laws, rules, and regulations. Any misuse of
                          the Plugin may result in termination of your access to
                          the Plugin.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          5. User Conduct
                        </h1>
                        <p>
                          You agree to use the Plugin in compliance with all
                          laws and regulations. Unlawful purposes include, but
                          are not limited to, activities that violate local,
                          state, national, or international laws, or activities
                          that infringe upon the rights of others. Damage,
                          impair, or harm includes actions that disrupt our
                          services, servers, or networks. Violation may result
                          in termination of Plugin access.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>

                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          6. Intellectual Property
                        </h1>
                        <p>
                          The Plugin and all content are protected by copyright
                          and trademark laws. Users acknowledge that they do not
                          acquire any ownership rights by using the Plugin or
                          its content. You may not use our copyrights or
                          trademarks without prior written consent.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>

                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          7. Disclaimers
                        </h1>
                        <p>
                          The Plugin is provided &quot;as is&quot; without
                          warranty of any kind. We disclaim any warranties
                          express or implied relating to merchantability,
                          fitness for particular purpose, non-infringement,
                          accuracy, or arising from the course of dealing. We
                          are not responsible for the content, accuracy,
                          legality, or any other aspect of external sites or
                          services that the Plugin may link to.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>

                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          8. Contact and Support
                        </h1>
                        <p>
                          For support or to report issues, users can contact us
                          at contact@jcasptechnologies.com. We strive to respond to all
                          inquiries in a timely manner.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>

                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          9. Limitation of Liability
                        </h1>
                        <p>
                          To the maximum extent permitted by law, we disclaim
                          all liability, whether based in contract, tort
                          (including negligence), strict liability, or
                          otherwise, and do not accept any liability for any
                          loss or damage (direct or indirect) incurred by you or
                          any user in connection with the use of the Plugin or
                          its services. We shall not be liable for any indirect,
                          incidental, special, consequential, or punitive
                          damages, including but not limited to, loss of
                          profits, data, use, goodwill, or other intangible
                          losses, resulting from your access to or use of the
                          Plugin.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>

                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          10. Modifications to Terms
                        </h1>
                        <p>
                          We may modify these Terms at any time. We will notify
                          users of any changes to these Terms by posting the
                          updated Terms on the Plugin. Your continued Plugin use
                          after changes constitutes acceptance. It is your
                          responsibility to check these Terms periodically for
                          updates.
                        </p>
                      </div>
                      <div className="border-b border-b-grey-200 my-3"></div>
                      <div id="applicationDescription">
                        <h1 className="text-xl font-semibold">
                          11. Governing Law
                        </h1>
                        <p>
                          These Terms are governed by the laws of the State of
                          California, without regard to conflict of law rules.
                          Any disputes relating to these Terms must be resolved
                          in courts located in the State of California.
                        </p>
                        <p>
                          By using the JCasp Plugin you agree to the Terms of
                          Use. If you do not agree, discontinue use.
                        </p>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Row>
      </div>
    </>
  );
};

export default ChatGTPtermsPageScene;
