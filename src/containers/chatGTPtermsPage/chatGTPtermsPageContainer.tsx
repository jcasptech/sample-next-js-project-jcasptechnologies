import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import ChatGTPtermsPageScene from "./chatGTPtermsPageScene";

const ChatGTPtermsPageContainer = () => {
  return (
    <>
      <SEO
        {...{
          pageName: "/chat-gpt-plugin-terms-of-use",
        }}
      />
      <ChatGTPtermsPageScene />
    </>
  );
};

ChatGTPtermsPageContainer.Layout = MainLayoutComponent;
export default ChatGTPtermsPageContainer;
