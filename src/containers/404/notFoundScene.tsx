import { Button } from "@components/theme";
import { Col, Row } from "antd";
import Image from "next/image";
import router from "next/router";
import notFoundStyles from "./notFound.module.scss";

export interface NotFoundProps {
  heading?: string;
  description?: string;
  buttonLabel?: string;
  buttonLink?: string;
}

const NotFoundScene = (props: NotFoundProps) => {
  const {
    heading = "ARE YOU LOST?",
    description = "Looks like you landed on a page that doesn't exist.",
    buttonLabel = "Back to Home Page",
    buttonLink = "/",
  } = props;

  return (
    <>
      <div className={`main-section ${notFoundStyles.notfound_background}`}>
        <section className="section">
          <Row justify={"center"} align="middle" className="h-100">
            <Col span={24} className="text-center">
              <Image
                src={"/images/general/logo.png"}
                width={100}
                height={100}
                alt="JCasp"
              />

              <h2>{heading}</h2>
              <p>{description}</p>
              <Button
                htmlType="button"
                type="primary"
                onClick={() => router.push(buttonLink)}
              >
                {buttonLabel}
              </Button>
            </Col>
          </Row>
        </section>
        <Image
          src={"/images/general/aux.png"}
          width={40}
          height={512}
          alt="JCasp"
          className="backImage"
        />
      </div>
      {/* <section className={`section-pad ${notFoundStyles.notfound_background}`}>
        <div className="container-fluid">
          <div className="row m0">
            <div className={`col-12 text-center`}>
            
            <Image 
              src={'/images/general/logo.png'}
              width={100}
              height={100}
              alt="JCasp"

            />

            <h2>TUNING IN PROGRESS</h2>
            <p>This artist's profile is currently under maintenance.<br/>
              Come back later or check out some of the great musicians we have to offer!</p>
            <Button
              htmlType="button"
              type="primary"
              onClick={() => router.push('/browse')}
            >
              Browse artists
            </Button>
            </div>
          </div>
        </div>
      </section> */}
    </>
  );
};
export default NotFoundScene;
