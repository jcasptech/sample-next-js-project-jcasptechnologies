import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import NotFoundScene from "./notFoundScene";

const NotFoundContainer = () => {
  return <NotFoundScene />;
};

NotFoundContainer.Layout = MainLayoutComponent;

export default NotFoundContainer;
