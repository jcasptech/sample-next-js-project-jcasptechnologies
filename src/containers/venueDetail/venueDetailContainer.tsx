import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import { LoginModal } from "@components/modals/loginModal";
import VideoModal from "@components/modals/videoSongModal";
import { DefaultSkeleton } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { whoAmI } from "@redux/services/auth.api";
import {
  getVenueDetailAPI,
  venueSubscribeAPI,
  venueUnsubscribeAPI,
} from "@redux/services/venue.api";
import { LoginUserState, loginUserSuccess } from "@redux/slices/auth";
import {
  fetchUpcomingVenues,
  loadMoreUpcomingVenues,
  upcomingVenuesState,
} from "@redux/slices/upComingVenues";
import { VenuesDetilsDataResponse } from "@redux/slices/venues";
import { useReCaptcha } from "next-recaptcha-v3";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { nextRedirect } from "src/libs/helpers";
import useList from "src/libs/useList";
import NotFoundScene from "../404/notFoundScene";
import VenueDetailScene from "./venueDetailScene";

export interface VenueDetailContainerProps {
  venueVanity: string;
}

const VenueDetailContainer = (props: VenueDetailContainerProps) => {
  const { venueVanity } = props;

  const dispatch = useDispatch();
  const [venueDetail, setVenueDetail] =
    useState<VenuesDetilsDataResponse | null>(null);
  const [isActionLoading, setIsActionLoading] = useState(true);
  const [venueId, setVenueId] = useState<any>(null);
  const [venueFollows, setVenueFollows] = useState<any[]>([]);
  const [sliderPhotos, setSliderPhotos] = useState<any[]>([]);
  const [sliderDisplay, setSliderDisplay] = useState(false);
  const { executeRecaptcha, loaded } = useReCaptcha();
  const [isVideoModalOpen, setIsVideoModalOpen] = useState(false);
  const [youtubeId, setYoutubeId] = useState("");
  const [nextSlide, setNextSlide] = useState(1);
  const [prevSlide, setPrevSlide] = useState<number>();

  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [userAction, setUserAction] = useState<"venueSubscribe" | null>(null);
  const [tmpVenueType, setTmpVenueType] = useState<any>(null);

  const sliderRef = useRef<any>(null);
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const handleVenueSubscribeActions = async (
    type: "subscribe" | "unsubscribe"
  ) => {
    if (venueId) {
      const restrictMessage = SUCCESS_MESSAGES.requiredLoginSubscribeVenue;
      const successMessage =
        type === "subscribe"
          ? SUCCESS_MESSAGES.subscribeVenue
          : SUCCESS_MESSAGES.unsubscribeVenue;

      if (loginUserState?.isLogin) {
        const userId = loginUserState?.data?.user?.objectId;
        if (userId) {
          setIsActionLoading(true);
          try {
            if (type === "subscribe") {
              await venueSubscribeAPI(venueId);
            } else {
              await venueUnsubscribeAPI(venueId);
            }
            showToast(successMessage, "success");
            setIsActionLoading(false);
            const auth = await whoAmI();
            dispatch(loginUserSuccess(auth));
          } catch (error: any) {
            setIsActionLoading(false);
          }
        }
      } else {
        setIsLoginModalOpen(true);
        setUserAction("venueSubscribe");
        setTmpVenueType(type);
        // showToast(restrictMessage, "warning");
      }
    }
  };

  useEffect(() => {
    if (loginUserState?.data?.user?.venueFollows) {
      setVenueFollows(loginUserState?.data?.user?.venueFollows);
    }
  }, [loginUserState?.data?.user]);

  useEffect(() => {
    if (venueDetail && venueDetail?.venue?.objectId) {
      setVenueId(venueDetail?.venue?.objectId);

      if (venueDetail?.venuePhotos?.length > 0) {
        if (venueDetail?.venuePhotos.length === 1) {
          setSliderPhotos([
            venueDetail?.venuePhotos[0],
            venueDetail?.venuePhotos[0],
          ]);
        } else {
          setSliderPhotos(venueDetail?.venuePhotos);
        }

        setSliderDisplay(true);
      } else if (venueDetail?.venue?.posterImage?.url) {
        setSliderPhotos([
          venueDetail.venue.posterImage.url,
          venueDetail.venue.posterImage.url,
        ]);
        setSliderDisplay(true);
      } else {
        setSliderPhotos([
          "/images/venue-placeholder.png",
          "/images/venue-placeholder.png",
        ]);
        setSliderDisplay(true);
      }
    }
  }, [venueDetail]);

  const {
    upcomingVenues: { isLoading, data: upcomingVenues },
  }: {
    upcomingVenues: upcomingVenuesState;
  } = useSelector((state: RootState) => ({
    upcomingVenues: state.upcomingVenues,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
      include: "artist",
    },
  });

  useEffect(() => {
    if (loaded && venueId && apiParam) {
      const getupcomingVenue = async () => {
        const recaptchaResponse = await executeRecaptcha("upcoming_venues");
        dispatch(fetchUpcomingVenues(apiParam, venueId, recaptchaResponse));
      };

      getupcomingVenue();
    }
  }, [dispatch, executeRecaptcha, venueId, apiParam, loaded]);

  const handleLoadMoreEvents = async (data: any) => {
    if (apiParam?.take) {
      apiParam.skip = (apiParam.skip || 0) + 8;
      const recaptchaResponse = await executeRecaptcha("upcoming_venues");
      dispatch(loadMoreUpcomingVenues(apiParam, venueId, recaptchaResponse));
    }
  };

  const settings = {
    speed: 1000,
    arrows: false,
    dots: false,
    focusOnSelect: true,
    className: "center venue-slider",
    centerMode: true,
    infinite: true,
    slidesPerRow: 1,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerPadding: "0",
    swipe: true,
  };

  // Manage custom Slider
  const handleCustomSlider = (type: any) => {
    const venueSlider: any = $(".venue-slider");
    if (type === "prev") {
      venueSlider.slick("slickPrev");
    } else {
      venueSlider.slick("slickNext");
    }
  };

  useEffect(() => {
    $(document).ready(function () {
      const venueSlider: any = $(".venue-slider");
      venueSlider
        .on("init", (event: any, slick: any, currentSlide: any) => {
          const cur = $(slick.$slides[slick.currentSlide]),
            next = cur.next(),
            prev = cur.prev();
          prev.addClass("slick-sprev");
          next.addClass("slick-snext");
          cur.removeClass("slick-snext").removeClass("slick-sprev");
          slick.$prev = prev;
          slick.$next = next;
        })
        .on(
          "beforeChange",
          (event: any, slick: any, currentSlide: any, nextSlide: any) => {
            const cur = $(slick.$slides[nextSlide]);
            slick.$prev.removeClass("slick-sprev");
            slick.$next.removeClass("slick-snext");
            const next = cur.next(),
              prev = cur.prev();
            prev.prev();
            prev.next();
            prev.addClass("slick-sprev");
            next.addClass("slick-snext");
            slick.$prev = prev;
            slick.$next = next;
            cur.removeClass("slick-next").removeClass("slick-sprev");
          }
        );

      venueSlider.slick({
        speed: 1000,
        arrows: false,
        dots: false,
        focusOnSelect: true,
        infinite: true,
        centerMode: true,
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerPadding: "0",
        swipe: true,
      });
    });
  }, [sliderDisplay]);

  const handleVideoModal = (data: any) => {
    setIsVideoModalOpen(true);
    setYoutubeId(`https://www.youtube.com/watch?v=${data}`);
  };

  useEffect(() => {
    if (venueVanity && loaded) {
      const getVenueDetails = async () => {
        try {
          const recaptchaResponse = await executeRecaptcha("venue_detail");
          const tmpA: any = await getVenueDetailAPI(
            venueVanity,
            recaptchaResponse
          );
          if (tmpA) {
            if (tmpA?.responseData) {
              setVenueDetail(tmpA.responseData);
            } else {
              setVenueDetail(tmpA);
            }
          }
          setIsActionLoading(false);
        } catch (e) {
          setIsActionLoading(false);
        }
      };
      getVenueDetails();
    }
  }, [venueVanity, loaded, executeRecaptcha]);

  return (
    <>
      {(isLoading || isActionLoading) && <DefaultSkeleton />}

      {isActionLoading && !venueDetail && <div className="h-75vs"></div>}

      {!isActionLoading && !venueDetail && (
        <>
          <NotFoundScene
            {...{
              buttonLabel: "Browse Venues",
              buttonLink: "/browse",
              description:
                "This venue's profile is currently under maintenance.\n Come back later or check out some of the great musicians we have to offer!",
              heading: "TUNING IN PROGRESS",
            }}
          />
        </>
      )}

      {venueDetail && (
        <>
          <VenueDetailScene
            {...{
              venueDetail,
              venueFollows,
              handleVenueSubscribeActions,
              upcomingVenues,
              handleLoadMoreEvents,
              sliderRef,
              handleVideoModal,
              handleCustomSlider,
              sliderPhotos,
              sliderDisplay,
              isLoading,
            }}
          />
          {isVideoModalOpen && (
            <VideoModal
              {...{
                setIsVideoModalOpen,
                isVideoModalOpen,
                youtubeId,
                setYoutubeId,
              }}
            />
          )}

          {isLoginModalOpen && userAction && (
            <LoginModal
              {...{
                actionFrom: userAction,
                handleOk: () => {
                  setIsLoginModalOpen(false);
                  setUserAction(null);
                  if (userAction === "venueSubscribe") {
                    handleVenueSubscribeActions(tmpVenueType);
                  }
                },
                isOpen: isLoginModalOpen,
                handleCancel: () => {
                  setIsLoginModalOpen(false);
                },
              }}
            />
          )}
        </>
      )}
    </>
  );
};

VenueDetailContainer.getInitialProps = async (ctx: any) => {
  const venueVanity = ctx?.query?.vanity;

  return {
    venueVanity,
  };
};

VenueDetailContainer.Layout = MainLayoutComponent;
export default VenueDetailContainer;
