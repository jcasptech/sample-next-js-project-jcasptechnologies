import { DefaultLoader } from "@components/theme";
import { GoogleMap, InfoWindow, Marker } from "@react-google-maps/api";
import { RootState } from "@redux/reducers";
import { GeneralState } from "@redux/slices/general";
import { VenuesDetilsDataResponse } from "@redux/slices/venues";
import type { NextPage } from "next";
import { SetStateAction, useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { loadScript } from "src/libs/helpers";
import { GOOGLE_API_KEY } from "src/libs/constants";

export interface MapProps {
  venueDetail?: VenuesDetilsDataResponse;
}
const VenueMap = (props: MapProps) => {
  const { venueDetail } = props;

  const [centerLocation, setCenterLocation] = useState({
    lat: 32.883,
    lng: -117.156,
  });
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (venueDetail?.venue?.location) {
      setCenterLocation({
        lat: venueDetail?.venue?.location?.latitude,
        lng: venueDetail?.venue?.location?.longitude,
      });
    }
  }, [venueDetail]);

  const handleScriptLoad = () => {
    setIsLoading(false);
  };

  useEffect(() => {
    loadScript(
      `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=places`,
      () => handleScriptLoad(),
      true
    );
  }, []);

  return (
    <>
      {isLoading && <DefaultLoader />}

      {!isLoading && (
        <GoogleMap
          options={{
            clickableIcons: false,
            disableDefaultUI: false,
            mapTypeControl: false,
            fullscreenControl: false,
            zoomControl: false,
            scaleControl: true,
            streetViewControl: false,
            // styles: [
            //   {
            //     featureType: "all",
            //     stylers: [{ saturation: -80 }, { lightness: 30 }],
            //   },
            // ],
            zoom: 10,
            minZoom: 3,
            mapTypeId: "roadmap",
          }}
          zoom={10}
          center={centerLocation}
          mapTypeId={"roadmap"}
          mapContainerStyle={{
            width: "100%",
            height: "100%",
            borderRadius: "15px",
          }}
          onLoad={() => console.log("Map Component Loaded...")}
        >
          <Marker
            position={centerLocation}
            icon="/images/general/map-marker.png"
            // onClick={() => onMarkerClick(mark)}
          />
        </GoogleMap>
      )}
    </>
  );
};

export default VenueMap;
