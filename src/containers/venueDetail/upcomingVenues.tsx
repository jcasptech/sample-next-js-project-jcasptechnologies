import ShareThisModal from "@components/modals/ShareThisModal";
import { CalendarIcon } from "@components/theme/icons/calendarIcon";
import { ClockIcon } from "@components/theme/icons/clockIcon";
import { VenueShareIcon } from "@components/theme/icons/venueShareIcon";
import { WhitePlayIcon } from "@components/theme/icons/whitePlayIcon";
import { UpcomingVenueObject } from "@redux/slices/upComingVenues";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { SITE_URL } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import venueDetailStyles from "./venueDetail.module.scss";

export interface UpcomingVenuesProps {
  upcomingVenues: UpcomingVenueObject;
  handleVideoModal: (d: any) => void;
}
const UpcomingVenues = (props: UpcomingVenuesProps) => {
  const { upcomingVenues, handleVideoModal } = props;
  const router = useRouter();
  const [url, setUrl] = useState("");
  const [isShareModalOpen, setIsShareModalOpen] = useState(false);

  const handleArtist = (data: string) => {
    if (data) {
      router.push(`/artist/${data}`);
    }
  };

  const handleShareModalOpen = (data: any) => {
    if (data) {
      setIsShareModalOpen(true);
      setUrl(data);
    }
  };

  return (
    <>
      {isShareModalOpen && (
        <ShareThisModal
          {...{
            isOpen: isShareModalOpen,
            url: url || "",
            setIsShareModalOpen,
          }}
        />
      )}
      <div className="col-12 col-sm-6 up_schedule mnopad">
        <div className="show_indi_card">
          <div className="row m0 vcenter">
            <div
              className={` col-3 show_thumb p-0 ${venueDetailStyles.iconThumb}`}
            >
              <Image
                className={`${venueDetailStyles.thumbImg} cursor-pointer h-auto`}
                src={upcomingVenues?.artistIconImage?.url}
                alt="venue title"
                width={144}
                height={144}
              />
              <span
                className={`vw ${venueDetailStyles.PlayIcon} cursor-pointer`}
                onClick={() =>
                  handleVideoModal(upcomingVenues?.artist?.youtubeId)
                }
              >
                <WhitePlayIcon
                  {...{ className: `${venueDetailStyles.WhiteIcon}` }}
                />
              </span>
            </div>
            <div className={` col-9 show_meta ${venueDetailStyles.show_meta}`}>
              <div className="">
                <div className="calc mt sfonts">
                  <span>
                    <CalendarIcon />
                  </span>
                  &nbsp;
                  {moment(upcomingVenues.startDate?.iso).format("MMM D, Y")}
                </div>
                <div
                  className={`clock-2 mt sfonts ${venueDetailStyles.clockPosition}`}
                >
                  <ClockIcon />
                  &nbsp;
                  {moment(upcomingVenues.startDate?.iso)
                    .utc()
                    .format("hh:mm A")}
                </div>
                <div
                  className={`${venueDetailStyles.sshare} sshare-2 mt sfonts`}
                >
                  <a
                    onClick={() =>
                      handleShareModalOpen(
                        `${SITE_URL}?showId=${upcomingVenues.objectId}`
                      )
                    }
                  >
                    <VenueShareIcon
                      {...{
                        className: venueDetailStyles.shareIcon,
                      }}
                    />
                  </a>
                </div>
              </div>
              <div
                className="title title-2 text-ellipsis cursor-pointer"
                onClick={(e) => handleArtist(upcomingVenues?.artist?.vanity)}
              >
                {capitalizeString(upcomingVenues?.artistName)}
              </div>
              <div className={`desc desc-2 ${venueDetailStyles.desc}`}>
                {upcomingVenues?.artist?.metadata?.about || "about of artist"}
              </div>
              <div className={`tags tags2 ${venueDetailStyles.list}`}>
                {upcomingVenues?.artist?.genres &&
                  upcomingVenues?.artist?.genres?.length > 0 &&
                  upcomingVenues?.artist?.genres?.map(
                    (genres: any, index: number) => (
                      <span className={venueDetailStyles.tags} key={index}>
                        {capitalizeString(genres)}
                      </span>
                    )
                  )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default UpcomingVenues;
