import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { HelpIcon } from "@components/theme/icons/helpIcon";
import { LocationColorIcon } from "@components/theme/icons/locationColorIcon";
import { PrevSlickIcon } from "@components/theme/icons/nextPrevIcon";
import { NextSlickIcon } from "@components/theme/icons/nextSlickIcon";
import { OpenTableColorIcon } from "@components/theme/icons/openTableColorIcon";
import { PhoneColorIcon } from "@components/theme/icons/phoneColorIcon";
import { WebsiteColorIcon } from "@components/theme/icons/websiteColorIcon";
import { UpcomingVenuesDataResponse } from "@redux/slices/upComingVenues";
import { VenuesDetilsDataResponse } from "@redux/slices/venues";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import { useEffect } from "react";
import Slider from "react-slick";
import { Tooltip } from "react-tooltip";
import { capitalizeString } from "src/libs/helpers";
import VenueMap from "./map";
import UpcomingVenues from "./upcomingVenues";
import venueDetailStyles from "./venueDetail.module.scss";

export interface VenueDetailProps {
  venueDetail: VenuesDetilsDataResponse;
  upcomingVenues: UpcomingVenuesDataResponse;
  venueFollows: any[];
  handleVenueSubscribeActions: (type: "subscribe" | "unsubscribe") => void;
  handleLoadMoreEvents: (d: any) => void;
  sliderRef: any;
  handleVideoModal: (d: any) => void;
  handleCustomSlider: (d: any) => void;
  sliderPhotos: string[];
  sliderDisplay: boolean;
  isLoading?: boolean;
}

const VenueDetailScene = (Props: VenueDetailProps) => {
  const {
    venueDetail,
    venueFollows,
    handleVenueSubscribeActions,
    upcomingVenues,
    handleLoadMoreEvents,
    sliderRef,
    handleVideoModal,
    handleCustomSlider,
    sliderPhotos,
    sliderDisplay,
    isLoading,
  } = Props;

  const setting = {
    dots: false,
    dotsClass: "slick-dots custom-dots",
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: "linear",
    arrows: false,
  };

  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <>
      <div className={`venue-detail section-pad`}>
        <div className="container-fluid">
          <div className="row m0 swap_row">
            <div
              className={`col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 vnIntro ${venueDetailStyles.vnIntro}`}
            >
              <div className="row">
                <div className="col-12">
                  <div className="row m0 align-items-end">
                    <div className="col-sm-12 col-md-9">
                      <div className="title capitalize text-ellipsis pt-0">
                        {capitalizeString(venueDetail?.venue?.name)}
                      </div>
                      <div className="col-12">
                        <div className="venue_detail_pills">
                          <div className="pill">{venueDetail?.venue?.city}</div>
                          <div>
                            {venueDetail?.venue?.objectId &&
                            venueFollows.indexOf(venueDetail?.venue?.objectId) >
                              -1 ? (
                              <span
                                onClick={(e) => {
                                  e.preventDefault();
                                  handleVenueSubscribeActions("unsubscribe");
                                }}
                                data-tooltip-id="unscubscribe-tooltip"
                                data-tooltip-content="Unsubscribe venue"
                                className="cursor-pointer button-pill"
                              >
                                {/* <VenueUnsubscribeIcon /> */}
                                <span className="pill colored">
                                  Unsubscribe
                                </span>
                                <HelpIcon />
                                <Tooltip
                                  id="unscubscribe-tooltip"
                                  place="bottom"
                                />
                              </span>
                            ) : (
                              <span
                                onClick={(e) => {
                                  e.preventDefault();
                                  handleVenueSubscribeActions("subscribe");
                                }}
                                data-tooltip-id="scubscribe-tooltip"
                                data-tooltip-content="Subscribe to support this venue's live music program and stay informed about upcoming events. Subscribers may occasionally receive exclusive discounts and special offers from participating venues."
                                className="cursor-pointer button-pill"
                              >
                                {/* <VenueSubscribeIcon /> */}
                                <span className="pill colored">Subscribe</span>
                                <HelpIcon />
                                <Tooltip
                                  id="scubscribe-tooltip"
                                  place="bottom"
                                />
                              </span>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="desc">{venueDetail?.venue?.about}</div>
                    </div>
                    <div className="col-sm-12 col-md-3 for-desktop">
                      <ul className="p-0 social d-flex justify-content-end mb-0 ">
                        {venueDetail?.venue?.address && (
                          <li>
                            <a
                              href={`https://www.google.com/maps/search/${encodeURI(
                                venueDetail?.venue?.address
                              )}`}
                              target="_blank"
                              className="venue-color-social"
                            >
                              <LocationColorIcon />
                            </a>
                          </li>
                        )}

                        {venueDetail?.venue?.phone && (
                          <li>
                            <a
                              href={`tel:${venueDetail?.venue?.phone}`}
                              className="venue-color-social"
                            >
                              <PhoneColorIcon />
                            </a>
                          </li>
                        )}

                        {venueDetail?.venue?.website && (
                          <li>
                            <a
                              href={venueDetail?.venue?.website}
                              target="_blank"
                              className="venue-color-social"
                            >
                              <WebsiteColorIcon />
                            </a>
                          </li>
                        )}

                        <li>
                          <a
                            href="https://dev.opentable.com/partner-portal"
                            target="_blank"
                            className="venue-color-social"
                          >
                            <OpenTableColorIcon />
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="col-12 up-content mt-2"></div>
                  </div>

                  <div className="slider for-mobile">
                    <Slider {...setting}>
                      {sliderPhotos &&
                        sliderPhotos.map((photo: any, index) => (
                          <div key={index}>
                            <Image
                              src={photo || "/images/venue-placeholder.png"}
                              alt="Slide"
                              className="h-auto"
                              width={400}
                              height={275}
                            />
                          </div>
                        ))}
                    </Slider>
                  </div>

                  <div className="col-12 col-sm-12 col-md-7 contac">
                    <div className="row for-mobile">
                      {venueDetail?.venue?.address && (
                        <div className="d-flex align-item-center p-1">
                          <a
                            href={`https://www.google.com/maps/search/${encodeURI(
                              venueDetail?.venue?.address
                            )}`}
                            target="_blank"
                            className="venue-color-social"
                          >
                            <LocationColorIcon />
                          </a>
                          <div className="m-l-1 text-break">
                            <a
                              href={`https://www.google.com/maps/search/${encodeURI(
                                venueDetail?.venue?.address
                              )}`}
                              target="_blank"
                              className="no-decoration"
                            >
                              {venueDetail?.venue?.address}
                            </a>
                          </div>
                        </div>
                      )}

                      {venueDetail?.venue?.phone && (
                        <div className="d-flex align-item-center p-1">
                          <a
                            href={`tel:${venueDetail?.venue?.phone}`}
                            target="_blank"
                            className="venue-color-social"
                          >
                            <PhoneColorIcon />
                          </a>
                          <div className="m-l-1 text-break">
                            <a
                              href={`tel:${venueDetail?.venue?.phone}`}
                              className="no-decoration"
                            >
                              {venueDetail?.venue?.phone || "-"}
                            </a>
                          </div>
                        </div>
                      )}

                      {venueDetail?.venue?.website && (
                        <div className="d-flex align-item-center p-1">
                          <a
                            href={venueDetail?.venue?.website}
                            target="_blank"
                            className="venue-color-social"
                          >
                            <WebsiteColorIcon />
                          </a>
                          <div className="m-l-1 text-break">
                            <a
                              href={venueDetail?.venue?.website}
                              target="_blank"
                              className="no-decoration"
                            >
                              {venueDetail?.venue?.website}
                            </a>
                          </div>
                        </div>
                      )}

                      <div className="d-flex align-item-center p-1">
                        <a
                          href="https://dev.opentable.com/partner-portal"
                          target="_blank"
                          className="venue-color-social"
                        >
                          <OpenTableColorIcon />
                        </a>
                        <div className="m-l-1 text-break">
                          Reservation link to Open Table :{" "}
                          <a
                            href="https://dev.opentable.com/partner-portal"
                            target="_blank"
                            className="no-decoration color-blue"
                          >
                            https://dev.opentable.com/partner-portal
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div
                  className={`col-12 col-sm-12 col-md-12 col-lg-5 vnslider for-mobile ${venueDetailStyles.vnslider2}`}
                >
                  <div className="col-12 col-sm-6 col-lg-5 col-md-12 mapHold ">
                    {/* <!-- note: Event Info popup is designed for one sample only. You need to make it dynamic the way you want --> */}
                    <div id="venueMap_mobile" className="maphold-2">
                      <VenueMap
                        {...{
                          venueDetail,
                        }}
                      />
                    </div>
                  </div>
                </div>

                <div className="row m0 head vcenter text-center">
                  <div className="col-12 title title-2">
                    <h2 className="">Upcoming Schedule</h2>
                  </div>
                </div>
                <div className="upcoming-schedules col-12 row m0">
                  {upcomingVenues &&
                    upcomingVenues?.list &&
                    upcomingVenues?.list?.length > 0 &&
                    upcomingVenues?.list?.map((upcomingVenues, index) => (
                      <UpcomingVenues
                        key={index}
                        {...{
                          upcomingVenues,
                          handleVideoModal,
                        }}
                      />
                    ))}
                  {upcomingVenues?.list &&
                    upcomingVenues?.list?.length <= 0 && (
                      <EmptyMessage description="No shows found." />
                    )}

                  {upcomingVenues && upcomingVenues?.hasMany && (
                    <div className="col-12 load_more mb-1">
                      <Button
                        type="ghost"
                        htmlType="button"
                        loading={isLoading}
                        disabled={isLoading}
                        onClick={(e) => {
                          e.preventDefault();
                          handleLoadMoreEvents(e);
                        }}
                      >
                        See More
                      </Button>
                    </div>
                  )}
                </div>
              </div>
            </div>

            <div className="col-12 col-sm-7 col-md-12 col-lg-12 col-xl-4 vnslider vnslider-2 for-desktop">
              {sliderDisplay && (
                <div className="container new-slider">
                  <div className="rev_slider">
                    <div className="venue-slider" ref={sliderRef}>
                      {sliderPhotos &&
                        sliderPhotos?.map((photo: any, index) => (
                          <div className={`rev_slide`} key={index}>
                            {/* <img
                              src={photo || "/images/venue-placeholder.png"}
                              alt="Slide"
                              className="img"
                            /> */}
                            <Image
                              src={photo || "/images/venue-placeholder.png"}
                              alt="Slide"
                              className="img h-auto"
                              width={400}
                              height={275}
                            />
                          </div>
                        ))}
                    </div>

                    {sliderPhotos.length > 1 && (
                      <div className={venueDetailStyles.sliderIcon}>
                        <a onClick={() => handleCustomSlider("prev")}>
                          <PrevSlickIcon
                            {...{
                              className: `${venueDetailStyles.prevIcon}`,
                            }}
                          />
                        </a>
                        <a onClick={() => handleCustomSlider("next")}>
                          <NextSlickIcon
                            {...{
                              className: `${venueDetailStyles.nextIcon}`,
                            }}
                          />
                        </a>
                      </div>
                    )}
                  </div>
                </div>
              )}

              <div className="col-12 col-sm-12 map m0">
                <div className="container">
                  <div id="venueMap">
                    <VenueMap
                      {...{
                        venueDetail,
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default VenueDetailScene;
