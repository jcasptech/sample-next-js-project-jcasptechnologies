import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import JCaspLightScane from "./JCaspLightScane";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useRouter } from "next/router";
import {
  JCaspLightFormInputs,
  JCaspLightFormValidateSchema,
} from "src/schemas/JCaspLightFormShema";
import { useEffect, useState } from "react";
import { getLatestFiveReviewAPI } from "@redux/services/latestFiveReview";
import SEO from "@components/SEO";

export interface JCaspLightContainerProps {}
const JCaspLightContainer = (props: JCaspLightContainerProps) => {
  const router = useRouter();
  const { register, handleSubmit, formState, setFocus, reset, setValue } =
    useForm<JCaspLightFormInputs>({
      resolver: yupResolver(JCaspLightFormValidateSchema),
    });
  const [letestFiveReviewData, setletestFiveReviewData] = useState<any>();
  const onSubmit = async (data: any) => {
    if (data) {
      localStorage.setItem("JCaspLight", JSON.stringify(data));
      router.push("/pricing");
    }
  };

  useEffect(() => {
    let getletestFiveReviewData = async () => {
      try {
        let res = await getLatestFiveReviewAPI();
        setletestFiveReviewData(res);
      } catch (error) {}
    };
    getletestFiveReviewData();
  }, []);

  const settings = {
    speed: 1000,
    arrows: false,
    dots: false,
    focusOnSelect: true,
    infinite: true,
    centerMode: true,
    slidesPerRow: 1,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerPadding: "0",
    swipe: true,
  };

  return (
    <>
      <SEO
        {...{
          pageName: "/JCasp-light",
        }}
      />
      <JCaspLightScane
        {...{
          register,
          formState,
          handleSubmit,
          onSubmit,
          settings,
          letestFiveReviewData,
          router,
          setValue,
        }}
      />
    </>
  );
};
JCaspLightContainer.Layout = MainLayoutComponent;
export default JCaspLightContainer;
