import LetestReviews from "@components/letestReviews";
import { Button } from "@components/theme";
import {
  InputField,
  InputGoogleAutoCompleteField,
} from "@components/theme/form/formFieldsComponent";
import Image from "next/image";
import React from "react";
import { FormGroup, Spinner } from "react-bootstrap";
import joinJCaspLightStyles from "./joinJCaspLight.module.scss";

export interface JCaspLightScaneProps {
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  register: any;
  formState: any;
  settings: any;
  letestFiveReviewData: any;
  router: any;
  setValue: any;
}
const JCaspLightScane = (props: JCaspLightScaneProps) => {
  const {
    register,
    formState,
    handleSubmit,
    onSubmit,
    settings,
    letestFiveReviewData,
    router,
    setValue,
  } = props;

  const handlePlaceSelect = (place: any) => {
    // Handle the selected place data here
  };
  return (
    <>
      <section className="section-pad">
        <div className="container-fluid">
          <div className="row m0">
            <div className="col-12 col-sm-12 col-lg-12 col-xl-6 col-md-12 account_box account_box2">
              <div className="row">
                <div
                  className={`${joinJCaspLightStyles.abLbl} col-12  para-j m-0`}
                >
                  <h2
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Join JCasp Light
                  </h2>
                  <p
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Create your account now! and hire artist and make offers to
                    artist
                  </p>
                </div>
                <div className={`col-12`}>
                  <div className={`${joinJCaspLightStyles.join_bg}`}>
                    <div className="col-8">
                      <p>
                        Pay to started JCasp Light Lorem ipsum is simply dummy
                        text
                      </p>
                    </div>
                    <div className="col-1"></div>
                    <div className="col-3 m-0">
                      <h3>$50.00</h3>
                    </div>
                  </div>
                </div>
                <div
                  className="col-12 form_holder"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <form className="formB" onSubmit={handleSubmit(onSubmit)}>
                    <div className="fieldH">
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "name",
                          label: "Contact Name",
                          className: `input`,
                          placeholder: "Enter Contact name",
                        }}
                      />
                    </div>
                    <div className="fieldH">
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "email",
                          label: "Email Address",
                          className: "input",
                          placeholder: "Enter Your Email Address",
                        }}
                      />
                    </div>
                    <div className="col-12 fieldH">
                      <InputGoogleAutoCompleteField
                        {...{
                          register,
                          formState,
                          id: "venueAddress",
                          label: "Venue Address",
                          className: "input",
                          placeholder: "Enter Venue Address",
                          googleAutoCompleteConfig: {
                            setValue,
                            autoCompleteId: "venueAddress",
                            autoPopulateFields: {
                              city: "city",
                              googlePlaceId: "googlePlaceId",
                            },
                          },
                        }}
                      />

                      <InputField
                        {...{
                          register,
                          formState,
                          id: "googlePlaceId",
                          type: "hidden",
                        }}
                      />
                    </div>
                    <div className="col-12 fieldH position-relative mb-0">
                      <div id="mapMoveHere"></div>
                    </div>
                    <div className="col-12 fieldH">
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "city",
                          name: "city",
                          label: "City",
                          className: "input",
                          placeholder: "Enter City",
                        }}
                      />
                    </div>
                    <div className="fieldH ">
                      <Button
                        htmlType="submit"
                        type="primary"
                        disabled={formState?.isSubmitting}
                        loading={formState?.isSubmitting}
                      >
                        Proceed to Pay
                      </Button>
                    </div>
                    <div className="fieldH reglnk pay-j">
                      <p>
                        <a
                          onClick={(e) => {
                            e.preventDefault();
                            router.back();
                          }}
                          className=""
                        >
                          <Image
                            src="/images/about-us/Arrow-7.png"
                            alt=""
                            width={13}
                            height={9}
                          />
                          {/* <img src="/images/about-us/Arrow-7.png" alt="" /> Go */}
                          To Back
                        </a>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div
              className={`col-12 col-sm-7 col-md-12 col-xl-6 col-lg-12 vnslider vnslider-2 ${joinJCaspLightStyles.letestReview}`}
            >
              <LetestReviews
                {...{
                  settings,
                  letestFiveReviewData,
                }}
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default JCaspLightScane;
