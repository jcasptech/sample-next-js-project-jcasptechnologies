import { AdminLayoutComponent } from "@components/layout/adminLayout/adminLayoutComponent";
import ActivePostingScene from "./activePostingScene";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@redux/reducers";
import {
  fetchActivePosting,
  ActivePostingState,
  loadMoreActivePosting,
} from "@redux/slices/activePosting";
import { useEffect, useState } from "react";
import useList from "src/libs/useList";
import { DefaultSkeleton } from "@components/theme";
import { Show } from "react-redux-permission";
import NoPermissionsComponent from "@components/noPermission/noPermissionsComponent";

const ActivePostingContainer = () => {
  const dispatch = useDispatch();
  const [selectEventId, setSelectEventId] = useState("");

  const [detailsOpen, setDetailsOpen] = useState("details");

  const {
    activePosting: { isLoading, data: activePostingData },
  }: {
    activePosting: ActivePostingState;
  } = useSelector((state: RootState) => ({
    activePosting: state.activePosting,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 8,
      skip: 0,
      include: ["selectedArtist", "venue"],
      orderByColumn: "startDate",
      orderBy: "ASCENDING",
    },
  });

  useEffect(() => {
    dispatch(fetchActivePosting(apiParam));
  }, [apiParam]);

  const handleLoadMore = (d: any) => {
    apiParam.skip = (apiParam.skip || 0) + 8;
    dispatch(loadMoreActivePosting(apiParam));
  };

  useEffect(() => {
    if (selectEventId === "") {
      dispatch(fetchActivePosting(apiParam));
    }
  }, [selectEventId]);

  return (
    <>
      {isLoading && <DefaultSkeleton />}
      <Show when={["active_posting"]} fallback={<NoPermissionsComponent />}>
        <ActivePostingScene
          {...{
            activePostingData,
            handleLoadMore,
            isLoading,
            selectEventId,
            setSelectEventId,
            setDetailsOpen,
            detailsOpen,
          }}
        />
      </Show>
    </>
  );
};

ActivePostingContainer.Layout = AdminLayoutComponent;
export default ActivePostingContainer;
