import ActivePostingCard from "@components/postingCard/activePostingCard";
import ActivePostingSelectEvent from "@components/postingCard/activePostingSelectEvent";
import PostBidsCard from "@components/postingCard/postBidsCard";
import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { ActivePostingDataResponse } from "@redux/slices/activePosting";
import Image from "next/image";
import Link from "next/link";
import React from "preact/compat";
import activePostingStyles from "./activePosting.module.scss";

export interface ActivePostingSceneProps {
  activePostingData: ActivePostingDataResponse;
  handleLoadMore: (d: any) => void;
  isLoading: boolean | undefined;
  selectEventId: string;
  setSelectEventId: (d: any) => void;
  setDetailsOpen: (d: any) => void;
  detailsOpen: string;
}
const ActivePostingScene = (props: ActivePostingSceneProps) => {
  const {
    activePostingData,
    handleLoadMore,
    isLoading,
    selectEventId,
    setSelectEventId,
    setDetailsOpen,
    detailsOpen,
  } = props;

  return (
    <>
      <div className="message-page h-100vh">
        <div className="panel-inner-content">
          <div className="row m0 final-ap">
            <div className="col-12 ">
              <div className="row p-0">
                <div className="col-12 col-sm-12 col-lg-7 posting-list">
                  <div className="row">
                    {activePostingData &&
                      activePostingData?.list?.map((postData: any, index) => (
                        <ActivePostingCard
                          key={index}
                          {...{
                            postData,
                            setSelectEventId,
                            setDetailsOpen,
                          }}
                        />
                      ))}
                    {activePostingData?.list &&
                      activePostingData?.list?.length <= 0 && (
                        <EmptyMessage description="No posting found." />
                      )}

                    {activePostingData.hasMany && (
                      <div className="col-12 load_more">
                        <Button
                          type="ghost"
                          htmlType="button"
                          loading={isLoading}
                          disabled={isLoading}
                          onClick={(e) => {
                            e.preventDefault();
                            handleLoadMore({
                              loadMore: true,
                            });
                          }}
                        >
                          Load more Post
                        </Button>
                      </div>
                    )}
                  </div>
                  <div id="postingDetails"></div>
                </div>

                <div className="col-12 col-sm-12 col-lg-4 viewPost">
                  {selectEventId !== "" ? (
                    <>
                      <div className="col-12 ptitle p-0">
                        <h3>Your Selected Event </h3>
                      </div>
                      {detailsOpen === "details" ? (
                        <ActivePostingSelectEvent
                          {...{
                            selectEventId,
                            setDetailsOpen,
                          }}
                        />
                      ) : (
                        <PostBidsCard
                          {...{
                            selectEventId,
                            setDetailsOpen,
                            setSelectEventId,
                          }}
                        />
                      )}
                    </>
                  ) : (
                    <div className={activePostingStyles.unSelectedData}>
                      <Image
                        src="/images/general/selectPost.svg"
                        alt=""
                        width={70}
                        height={70}
                      />
                      <p>
                        Please select any event to see further details of that
                        event
                      </p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ActivePostingScene;
