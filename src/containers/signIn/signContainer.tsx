import { MainLayoutComponent } from "@components/layout/mainLayout/mainLayoutComponent";
import SEO from "@components/SEO";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import {
  doFacebookLogin,
  doLogin,
  FacebookLoginPayload,
  fetchLoginUser,
  loginReset,
  LoginState,
  loginSuccess,
  LoginUserState,
} from "@redux/slices/auth";
import { useReCaptcha } from "next-recaptcha-v3";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useCookies } from "react-cookie";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import useLoginStatus from "src/libs/useLoginStatus";
import {
  LoginFormInputs,
  LoginFormValidateSchema,
} from "src/schemas/loginFormSchema";
import SignInScene from "./signInScene";

const SignContainer = () => {
  const { register, handleSubmit, formState, setFocus } =
    useForm<LoginFormInputs>({
      resolver: yupResolver(LoginFormValidateSchema),
    });

  const router = useRouter();
  const loginState: LoginState = useSelector((state: RootState) => state.login);

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();

  const isLogin = useLoginStatus(loginState);
  const [cookies]: any = useCookies(["authApp"]);

  useEffect(() => {
    if (cookies.authApp && isLogin) {
      router.push("/");
    }
    return () => {
      dispatch(loginReset());
    };
  }, [cookies?.authApp, isLogin]);

  const onSubmit = async (values: any) => {
    const recaptchaResponse = await executeRecaptcha("login");
    dispatch(doLogin(values, recaptchaResponse));
  };

  const handleFacebookLogin = async (data: FacebookLoginPayload) => {
    const recaptchaResponse = await executeRecaptcha("login");
    dispatch(doFacebookLogin(data, recaptchaResponse));
  };

  useEffect(() => {
    if (loginState?.data?.user && loginState?.data?.accessToken) {
      dispatch(
        fetchLoginUser({
          Authorization: `Bearer ${loginState?.data?.accessToken}`,
        })
      );
    }
  }, [loginState]);

  useEffect(() => {
    setFocus("username");
  }, [loaded]);

  return (
    <>
      <SEO
        {...{
          pageName: "/login",
        }}
      />

      <SignInScene
        {...{
          handleSubmit,
          onSubmit,
          loginState,
          loginUserState,
          register,
          formState,
          handleFacebookLogin,
        }}
      />
    </>
  );
};

SignContainer.Layout = MainLayoutComponent;
export default SignContainer;
