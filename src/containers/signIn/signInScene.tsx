import { Button } from "@components/theme/button";
import {
  FormGroup,
  InputField,
  SignInPasswordField,
} from "@components/theme/form/formFieldsComponent";
import FacebookLogin from "@greatsumini/react-facebook-login";
import {
  FacebookLoginPayload,
  LoginState,
  LoginUserState,
} from "@redux/slices/auth";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";
import { FACEBOOK_APP_ID } from "src/libs/constants";
import signInStyles from "./signIn.module.scss";

export interface signInSceneProps {
  handleSubmit: (data: any) => () => void;
  onSubmit: (d: any) => void;
  loginState: LoginState;
  loginUserState: LoginUserState;
  register: any;
  formState: any;
  handleFacebookLogin: (d: FacebookLoginPayload) => void;
}

const SignInScene = (props: signInSceneProps) => {
  const {
    register,
    formState,
    handleSubmit,
    onSubmit,
    loginUserState,
    loginState,
    handleFacebookLogin,
  } = props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <section className="section-pad">
        <div className={`container-fluid ${signInStyles.main_box}`}>
          <div className="row m0">
            <div className={`col-12 col-sm-6 ${signInStyles.account_box}`}>
              <div className={`row m0 ${signInStyles.borderDiv}`}>
                <div className={`col-12 ${signInStyles.abLbl}`}>
                  <h3
                    data-aos="fade-down"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Sign in to JCasp
                  </h3>
                  <p
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="1200"
                  >
                    Continue with facebook or enter your details
                  </p>
                </div>
                <div className={`col-12 ${signInStyles.fbSign}`}>
                  <FacebookLogin
                    className="hvr-float-shadow facebook-button"
                    appId={FACEBOOK_APP_ID}
                    onSuccess={(response) => {
                      handleFacebookLogin({
                        facebookToken: response.accessToken,
                        type: "SIGNIN",
                      });
                    }}
                    onFail={(error) => {}}
                  >
                    <Image
                      src="/images/fb-in.png"
                      alt="Signin with Facebook"
                      width={503}
                      height={52}
                      className="h-auto"
                    />
                  </FacebookLogin>
                </div>
                <div
                  className={`col-12 ${signInStyles.orline}`}
                  data-aos="fade-up"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <p className={signInStyles.lineThrough}>or with email</p>
                </div>
                <div
                  className="col-12 form_holder"
                  data-aos="fade-down"
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <form
                    className={`${signInStyles.formB}`}
                    onSubmit={handleSubmit(onSubmit)}
                  >
                    <FormGroup className={`${signInStyles.formB_fieldH}`}>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "username",
                          label: "Email Address or Username",
                          className: `${signInStyles.input}`,
                          placeholder: "Enter your Email Address or Username",
                        }}
                      />
                    </FormGroup>

                    <FormGroup className={`${signInStyles.formB_fieldH}`}>
                      <SignInPasswordField
                        {...{
                          register,
                          formState,
                          id: "password",
                          label: "Password",
                          className: `${signInStyles.input}`,
                          placeholder: "Enter your Password",
                        }}
                      />
                    </FormGroup>

                    <div
                      className={`${signInStyles.formB_fieldH} forgotlnk`}
                    ></div>

                    <div
                      className={`${signInStyles.formB_fieldH} ${signInStyles.submitBtn} mob-ctr`}
                    >
                      <Button
                        htmlType="submit"
                        loading={
                          formState?.isSubmitting ||
                          loginState.isLoading ||
                          loginUserState.isLoading
                        }
                        disabled={
                          formState?.isSubmitting ||
                          loginState.isLoading ||
                          loginUserState.isLoading
                        }
                        type="primary"
                      >
                        Sign In
                      </Button>
                    </div>

                    <div
                      className={`${signInStyles.formB_fieldH} reglnk mob-ctr`}
                    >
                      <Link
                        href="/forgot-password"
                        className={`${signInStyles.formLnk} ${signInStyles.submitBtn} mb-2`}
                      >
                        Forgot Password?
                      </Link>
                      <p className={`${signInStyles.submitBtn}`}>
                        Don&apos;t have an account?{" "}
                        <Link href="/signup" className="ps-1">
                          Signup now
                        </Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default SignInScene;
