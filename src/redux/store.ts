import { AnyAction, configureStore, Store } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { createWrapper, HYDRATE, MakeStore } from "next-redux-wrapper";
import rootReducer, { RootState } from "./reducers";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";

const reducer = (state: any, action: AnyAction) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };

    return nextState;
  } else {
    return rootReducer(state, action);
  }
};

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// export const makeStore = () => {
//   const store = createStore(persistedReducer);
//   const persistor = persistStore(store);
//   return { store, persistor };
// };

// export const getStore = () => {
//   return configureStore({
//     reducer,
//   });
// };

// setupListeners(getStore().dispatch);

// // export type AppDispatch = typeof getStore().dispatch;
// // export const useAppDispatch = () => useDispatch<AppDispatch>();

// /**
//  * @param initialState The store's initial state (on the client side, the state of the server-side store is passed here)
//  */
// const makeStore: MakeStore<Store<RootState>> = () => {
//   return getStore();
// };

// export const wrapper = createWrapper<Store<RootState>>(makeStore, {
//   debug: true,
// });

// export default makeStore;

// export const makeStore = () => {
//   const isServer = typeof window === "undefined";
//   if (isServer) {
//     return configureStore({
//       reducer,
//     });
//   } else {
//     const persistConfig = {
//       key: "nextjs",
//       storage,
//       expires: 1 * 60 * 60 * 1000,
//     };
//     const persistedReducer = persistReducer(persistConfig, rootReducer);
//     let store: any = configureStore({
//       reducer: persistedReducer,
//     });
//     store.__persistor = persistStore(store); // Nasty hack
//     return store;
//   }
// };

// type Store = ReturnType<typeof makeStore>;

// export type AppDispatch = Store["dispatch"];
// export type RootState = ReturnType<Store["getState"]>;

// export const wrapper = createWrapper(makeStore, { debug: false });

// export default makeStore;

export const getStore = () => {
  return configureStore({
    reducer,
  });
};

setupListeners(getStore().dispatch);

// export type AppDispatch = typeof getStore().dispatch;
// export const useAppDispatch = () => useDispatch<AppDispatch>();

/**
 * @param initialState The store's initial state (on the client side, the state of the server-side store is passed here)
 */
const makeStore: MakeStore<Store<RootState>> = () => {
  return getStore();
};

export const wrapper = createWrapper<Store<RootState>>(makeStore, {
  debug: true,
});

export default makeStore;
