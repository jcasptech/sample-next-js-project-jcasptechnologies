import { boolean, DateLocale } from "yup/lib/locale";

import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getUpcomingAPI } from "@redux/services/shows.api";

export interface userObject {
  __type: string;
  className: string;
  objectId: string;
}

export interface upcomingEventObject {
  attendanceCount: number;
  calendarAddCount: number;
  timeZone: string;
  earnings: number;
  promoteOnJCasp: boolean;
  visible: number;
  artist: any;
  artistName: string;
  artistIconImage: any;
  event: any;
  startDate: any;
  venue: any;
  iconImage: any;
  location: any;
  name: string;
  posterImage: any;
  sourceImage: any;
  venueName: string;
  createdAt: Date;
  updatedAt: DateLocale;
  calendarAdds: any;
  calendar_adds: any;
  objectId: string;
}

export interface upcomingEventDataResponse {
  list?: upcomingEventObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface upcomingEventSuccessResponse extends SuccessResponse {
  data: upcomingEventDataResponse;
}

export interface UpcomingEventState {
  data?: upcomingEventDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const upcomingEventInitialState: UpcomingEventState = {
  data: {},
  error: null,
  isLoading: false,
};

export const upcomingEventSlice = createSlice({
  name: "upcomingEvent",
  initialState: deepClone(upcomingEventInitialState),
  reducers: {
    upcomingEventStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    upcomingEventSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    upcomingEventUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    upcomingEventFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    upcomingEventReset: () => {
      return deepClone(upcomingEventInitialState);
    },
  },
});

export const {
  upcomingEventStarted,
  upcomingEventSuccess,
  upcomingEventFail,
  upcomingEventReset,
  upcomingEventUpdate,
} = upcomingEventSlice.actions;

export const fetchUpcomingEvent =
  (queryParams: QueryParams, id: string, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(upcomingEventStarted());
    try {
      const upcomingEventData: upcomingEventDataResponse = await getUpcomingAPI(
        queryParams,
        id,
        recaptchaResponse
      );

      if (upcomingEventData) {
        dispatch(upcomingEventSuccess(upcomingEventData));
      }
    } catch (e: any) {
      dispatch(upcomingEventFail(e.message));
    }
  };

export const loadMoreUpcomingEvent =
  (queryParams: QueryParams, id: string, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(upcomingEventStarted());
    try {
      const upcomingEventData: upcomingEventDataResponse = await getUpcomingAPI(
        queryParams,
        id,
        recaptchaResponse
      );

      if (upcomingEventData) {
        dispatch(upcomingEventUpdate(upcomingEventData));
      }
    } catch (e: any) {
      dispatch(upcomingEventFail(e.message));
    }
  };
