import { boolean, DateLocale } from "yup/lib/locale";
import { getShowsAPI } from "@redux/services/shows.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { VenueObject } from "../venues";

export interface userObject {
  __type: string;
  className: string;
  objectId: string;
}

export interface ShowsObject {
  attendanceCount: number;
  calendarAddCount: number;
  timeZone: string;
  earnings: number;
  promoteOnJCasp: boolean;
  visible: number;
  artist: any;
  artistName: string;
  artistIconImage: any;
  event: any;
  startDate: any;
  venue: VenueObject;
  iconImage: any;
  location: any;
  name: string;
  posterImage: any;
  sourceImage: any;
  venueName: string;
  createdAt: Date;
  updatedAt: DateLocale;
  calendarAdds: any;
  calendar_adds: any;
  objectId: string;
}

export interface ShowsDataResponse {
  list?: ShowsObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ShowsSuccessResponse extends SuccessResponse {
  data: ShowsDataResponse;
}

export interface ShowsState {
  data?: ShowsDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const showsInitialState: ShowsState = {
  data: {},
  error: null,
  isLoading: false,
};

export const ShowsSlice = createSlice({
  name: "shows",
  initialState: deepClone(showsInitialState),
  reducers: {
    ShowsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ShowsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ShowsUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    ShowsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ShowsReset: () => {
      return deepClone(showsInitialState);
    },
  },
});

export const {
  ShowsStarted,
  ShowsSuccess,
  ShowsUpdate,
  ShowsFail,
  ShowsReset,
} = ShowsSlice.actions;

export const fetchShows =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(ShowsStarted());
    try {
      const showsData: ShowsDataResponse = await getShowsAPI(
        queryParams,
        recaptchaResponse
      );

      if (showsData) {
        dispatch(ShowsSuccess(showsData));
      }
    } catch (e: any) {
      dispatch(ShowsFail(e.message));
    }
  };

export const loadMoreShows =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(ShowsStarted());
    try {
      const showsData: ShowsDataResponse = await getShowsAPI(
        queryParams,
        recaptchaResponse
      );
      if (showsData) {
        dispatch(ShowsUpdate(showsData));
      }
    } catch (e: any) {
      dispatch(ShowsFail(e.message));
    }
  };
