import { DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getActivePostingAPI } from "@redux/services/active-posting.api";

export interface ActivePostingObject {
  equipmentProvided: boolean;
  selectedArtist?: any;
  address: string;
  location: any;
  endDate: any;
  note: string;
  updatedAt: string;
  timeZone: string;
  serviceCharge: string;
  startDate: any;
  variety: string;
  attendanceCategory: string;
  genres: string[];
  createdAt: string;
  city: string;
  totalCost: number;
  artistProceeds: number;
  artistFormats: string;
  serviceFee: number;
  status: string;
  state: string;
  playSetting: string;
  title: string;
  normalizedTitle: string;
  zipcode: number;
  posterImage: any;
  iconImage: any;
  submittedArtists: any;
  venue: any;
  objectId: string;
  bidCount: number;
}

export interface ActivePostingDataResponse {
  list?: ActivePostingObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ActivePostingSuccessResponse extends SuccessResponse {
  data: ActivePostingDataResponse;
}

export interface ActivePostingState {
  data?: ActivePostingDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const activePostingInitialState: ActivePostingState = {
  data: {},
  error: null,
  isLoading: false,
};

export const activePostingSlice = createSlice({
  name: "activePosting",
  initialState: deepClone(activePostingInitialState),
  reducers: {
    ActivePostingStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ActivePostingSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ActivePostingUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    ActivePostingFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ActivePostingReset: () => {
      return deepClone(activePostingInitialState);
    },
  },
});

export const {
  ActivePostingStarted,
  ActivePostingSuccess,
  ActivePostingUpdate,
  ActivePostingFail,
  ActivePostingReset,
} = activePostingSlice.actions;

export const fetchActivePosting =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ActivePostingStarted());
    try {
      const userShowsData: ActivePostingDataResponse =
        await getActivePostingAPI(queryParams);
      if (userShowsData) {
        dispatch(ActivePostingSuccess(userShowsData));
      }
    } catch (e: any) {
      dispatch(ActivePostingFail(e.message));
    }
  };

export const loadMoreActivePosting =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ActivePostingStarted());
    try {
      const userShowsData: ActivePostingDataResponse =
        await getActivePostingAPI(queryParams);
      if (userShowsData) {
        dispatch(ActivePostingUpdate(userShowsData));
      }
    } catch (e: any) {
      dispatch(ActivePostingFail(e.message));
    }
  };
