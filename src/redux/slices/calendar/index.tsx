import { boolean, DateLocale } from "yup/lib/locale";
import {
  CalendarQueryParams,
  QueryParams,
  SuccessResponse,
} from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { VenueObject } from "../venues";
import {
  getCalendarAPI,
  getAdminCalendarAPI,
  getVenueCalendarAPI,
} from "@redux/services/calendar.api";

export interface CalendarObject {
  attendanceCount: number;
  calendarAddCount: number;
  timeZone: string;
  earnings: number;
  promoteOnJCasp: boolean;
  visible: number;
  artist: any;
  artistName: string;
  artistIconImage: any;
  event: any;
  startDate: any;
  venue: VenueObject;
  iconImage: any;
  location: any;
  name: string;
  posterImage: any;
  sourceImage: any;
  venueName: string;
  createdAt: Date;
  updatedAt: DateLocale;
  calendarAdds: any;
  calendar_adds: any;
  objectId: string;
}

export interface eventObject {
  artistId?: string;
  artistName?: string;
  backgroundColor: string;
  className: string;
  end: string;
  id?: string;
  start: string;
  status: string;
  tagColor: string;
  title: string;
  totalCost?: number;
  unreadMessages?: boolean;
}

export interface CalendarDataResponse {
  monthlyEarning?: number;
  yearlyEarning?: number;
  calendarData: eventObject[];
}

export interface CalendarSuccessResponse extends SuccessResponse {
  data: CalendarDataResponse;
}

export interface CalendarState {
  data?: CalendarDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const CalendarInitialState: CalendarState = {
  data: {},
  error: null,
  isLoading: false,
};

export const calendarSlice = createSlice({
  name: "Calendar",
  initialState: deepClone(CalendarInitialState),
  reducers: {
    CalendarStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    CalendarSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    CalendarUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    CalendarFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    CalendarReset: () => {
      return deepClone(CalendarInitialState);
    },
  },
});

export const {
  CalendarStarted,
  CalendarSuccess,
  CalendarUpdate,
  CalendarFail,
  CalendarReset,
} = calendarSlice.actions;

export const fetchCalendar =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(CalendarStarted());
    try {
      const CalendarData: CalendarDataResponse = await getCalendarAPI(
        queryParams
      );

      if (CalendarData) {
        dispatch(CalendarSuccess(CalendarData));
      }
    } catch (e: any) {
      dispatch(CalendarFail(e.message));
    }
  };

export const fetchAdminCalendar =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(CalendarStarted());
    try {
      const CalendarData: CalendarDataResponse = await getAdminCalendarAPI(
        queryParams
      );

      if (CalendarData) {
        dispatch(CalendarSuccess(CalendarData));
      }
    } catch (e: any) {
      dispatch(CalendarFail(e.message));
    }
  };

export const fetchVenueCalendar =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(CalendarStarted());
    try {
      const CalendarData: CalendarDataResponse = await getVenueCalendarAPI(
        queryParams
      );

      if (CalendarData) {
        dispatch(CalendarSuccess(CalendarData));
      }
    } catch (e: any) {
      dispatch(CalendarFail(e.message));
    }
  };
