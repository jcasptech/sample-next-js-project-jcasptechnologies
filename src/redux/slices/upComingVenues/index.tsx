import { QueryParams, SuccessResponse } from "@redux/services/types";
import { getUpcomingVenueAPI } from "@redux/services/venue.api";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface UpcomingVenueObject {
  attendanceCount: number;
  calendarAddCount: number;
  timeZone: string;
  earnings: number;
  promoteOnJCasp: boolean;
  venue: {
    className: string;
    objectId: string;
  };
  artist: any;
  startDate: any;
  name: string;
  visible: number;
  artistName: string;
  artistIconImage: {
    name: string;
    url: string;
  };
  song: {
    objectId: string;
  };
  venueName: string;
  iconImage: {
    name: string;
    url: string;
  };
  location: {
    latitude: number;
    longitude: number;
  };
  posterImage: {
    name: string;
    url: string;
  };
  sourceImage: {
    __type: File;
    name: string;
    url: string;
  };
  createdAt: string;
  updatedAt: string;
  attendees: any;
  calendarAdds: any;
  role: any;
  objectId: string;
}

export interface UpcomingVenuesDataResponse {
  list?: UpcomingVenueObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface VenuesSuccessResponse extends SuccessResponse {
  data: UpcomingVenuesDataResponse;
}

export interface upcomingVenuesState {
  data?: UpcomingVenuesDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const upcomingVenuesInitialState: upcomingVenuesState = {
  data: {},
  error: null,
  isLoading: false,
};

export const upcomingVenuesSlice = createSlice({
  name: "upcomingVenues",
  initialState: deepClone(upcomingVenuesInitialState),
  reducers: {
    upcomingVenuesStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    upcomingVenuesSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    upcomingVenuesUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    upcomingVenuesFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    upcomingVenuesReset: () => {
      return deepClone(upcomingVenuesInitialState);
    },
  },
});

export const {
  upcomingVenuesStarted,
  upcomingVenuesSuccess,
  upcomingVenuesFail,
  upcomingVenuesReset,
  upcomingVenuesUpdate,
} = upcomingVenuesSlice.actions;

export const fetchUpcomingVenues =
  (queryParams: QueryParams, id: string, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(upcomingVenuesStarted());
    try {
      const venuesData: UpcomingVenuesDataResponse = await getUpcomingVenueAPI(
        queryParams,
        id,
        recaptchaResponse
      );

      if (venuesData) {
        dispatch(upcomingVenuesSuccess(venuesData));
      }
    } catch (e: any) {
      dispatch(upcomingVenuesFail(e.message));
    }
  };

export const loadMoreUpcomingVenues =
  (queryParams: QueryParams, id: string, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(upcomingVenuesStarted());
    try {
      const venuesData: UpcomingVenuesDataResponse = await getUpcomingVenueAPI(
        queryParams,
        id,
        recaptchaResponse
      );

      if (venuesData) {
        dispatch(upcomingVenuesUpdate(venuesData));
      }
    } catch (e: any) {
      dispatch(upcomingVenuesFail(e.message));
    }
  };
