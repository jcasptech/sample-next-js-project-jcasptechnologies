import { getArtistAPI } from "@redux/services/artist.api";
import { getClaimUnclaimedArtistAPI } from "@redux/services/claim-unClaimed.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { ArtistObject } from "../artists";

export interface ClaimArtistDataResponse {
  list?: ArtistObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ClaimArtistSuccessResponse extends SuccessResponse {
  data: ClaimArtistDataResponse;
}

export interface CliamArtistState {
  data?: ClaimArtistDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const claimArtistInitialState: CliamArtistState = {
  data: {},
  error: null,
  isLoading: false,
};

export const claimArtistSlice = createSlice({
  name: "claimArtist",
  initialState: deepClone(claimArtistInitialState),
  reducers: {
    claimArtistStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    claimArtistSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    claimArtistUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    claimArtistFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    claimArtistReset: () => {
      return deepClone(claimArtistInitialState);
    },
  },
});

export const {
  claimArtistStarted,
  claimArtistSuccess,
  claimArtistFail,
  claimArtistReset,
  claimArtistUpdate,
} = claimArtistSlice.actions;

export const fetchClaimArtist =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(claimArtistStarted());
    try {
      const artistData: ClaimArtistDataResponse =
        await getClaimUnclaimedArtistAPI(queryParams);
      if (artistData) {
        dispatch(claimArtistSuccess(artistData));
      }
    } catch (e: any) {
      dispatch(claimArtistFail(e.message));
    }
  };

export const loadMoreClaimArtist =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(claimArtistStarted());
    try {
      const artistData: ClaimArtistDataResponse =
        await getClaimUnclaimedArtistAPI(queryParams);

      if (artistData) {
        dispatch(claimArtistUpdate(artistData));
      }
    } catch (e: any) {
      dispatch(claimArtistFail(e.message));
    }
  };
