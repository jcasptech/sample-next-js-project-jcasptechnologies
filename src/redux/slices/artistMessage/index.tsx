import { getAdminMessageAPI } from "@redux/services/admin.api";
import { getArtistMessageAPI } from "@redux/services/artist.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface ArtistMessageObject {
  checkinCount: number;
  date: any;
  source: any;
  artists: any;
  name: string;
  email: string;
  avatarImage: any;
  createdAt: Date;
  updatedAt: Date;
  objectId: string;
  content: string;
  iconImage?: any;
  posterImage?: any;
  sourceImage?: any;
  receiver?: any;
  vanity: string;
  fans?: any;
  profileImage?: any;
}

export interface ArtistMessageDataResponse {
  list?: ArtistMessageObject[];
  total?: number;
  result?: ArtistMessageObject[];
  hasMany?: boolean;
  count?: number;
}

export interface ArtistMessageSuccessResponse extends SuccessResponse {
  data: ArtistMessageDataResponse;
}

export interface ArtistMessageState {
  data?: ArtistMessageDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const messageInitialState: ArtistMessageState = {
  data: {},
  error: null,
  isLoading: false,
};

export const messageSlice = createSlice({
  name: "messages",
  initialState: deepClone(messageInitialState),
  reducers: {
    artistMessageStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    artistMessageSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    artistMessageUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    artistMessageFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    artistMessageReset: () => {
      return deepClone(messageInitialState);
    },
  },
});

export const {
  artistMessageStarted,
  artistMessageSuccess,
  artistMessageFail,
  artistMessageReset,
  artistMessageUpdate,
} = messageSlice.actions;

export const fetchArtistMessage =
  (id: any, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(artistMessageStarted());
    try {
      const artistMessageData: ArtistMessageDataResponse =
        await getArtistMessageAPI(id, queryParams);

      if (artistMessageData) {
        dispatch(artistMessageSuccess(artistMessageData));
      }
    } catch (e: any) {
      dispatch(artistMessageFail(e.message));
    }
  };

export const loadMoreArtistMessage =
  (id: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(artistMessageStarted());
    try {
      const artistMessageData: ArtistMessageDataResponse =
        await getArtistMessageAPI(id, queryParams);
      if (artistMessageData) {
        dispatch(artistMessageUpdate(artistMessageData));
      }
    } catch (e: any) {
      dispatch(artistMessageFail(e.message));
    }
  };

export const fetchAdminMessage =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(artistMessageStarted());
    try {
      const artistMessageData: ArtistMessageDataResponse =
        await getAdminMessageAPI(queryParams);

      if (artistMessageData) {
        dispatch(artistMessageSuccess(artistMessageData));
      }
    } catch (e: any) {
      dispatch(artistMessageFail(e.message));
    }
  };

export const loadMoreAdminMessage =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(artistMessageStarted());
    try {
      const artistMessageData: ArtistMessageDataResponse =
        await getAdminMessageAPI(queryParams);
      if (artistMessageData) {
        dispatch(artistMessageUpdate(artistMessageData));
      }
    } catch (e: any) {
      dispatch(artistMessageFail(e.message));
    }
  };
