import { QueryParams, SuccessResponse } from "@redux/services/types";
import { getFeaturedVenueAPI } from "@redux/services/venue.api";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { VenueObject } from "../venues";

export interface FeaturedVenueDataResponse {
  list?: VenueObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface VenueSuccessResponse extends SuccessResponse {
  data: FeaturedVenueDataResponse;
}

export interface FeaturedVenueState {
  data?: FeaturedVenueDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const featuredVenueInitialState: FeaturedVenueState = {
  data: {},
  error: null,
  isLoading: false,
};

export const featuredVenueSlice = createSlice({
  name: "featuredVenue",
  initialState: deepClone(featuredVenueInitialState),
  reducers: {
    featuredVenueStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    featuredVenueSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    featuredVenueUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    featuredVenueFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    featuredVenueReset: () => {
      return deepClone(featuredVenueInitialState);
    },
  },
});

export const {
  featuredVenueStarted,
  featuredVenueSuccess,
  featuredVenueFail,
  featuredVenueReset,
  featuredVenueUpdate,
} = featuredVenueSlice.actions;

export const fetchFeaturedVenue =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(featuredVenueStarted());
    try {
      const VenueData: FeaturedVenueDataResponse = await getFeaturedVenueAPI(
        queryParams
      );

      if (VenueData) {
        dispatch(featuredVenueSuccess(VenueData));
      }
    } catch (e: any) {
      dispatch(featuredVenueFail(e.message));
    }
  };

export const loadMoreFeaturedVenue =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(featuredVenueStarted());
    try {
      const featuredVenueData: FeaturedVenueDataResponse =
        await getFeaturedVenueAPI(queryParams);

      if (featuredVenueData) {
        dispatch(featuredVenueUpdate(featuredVenueData));
      }
    } catch (e: any) {
      dispatch(featuredVenueFail(e.message));
    }
  };
