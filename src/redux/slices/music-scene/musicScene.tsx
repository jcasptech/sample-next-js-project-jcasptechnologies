import { getMusicSceneAPI } from "@redux/services/artist.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface tagsObject {
  state: string;
  city: string;
  location: any;
  radioChannel: number;
  available: boolean;
  country: string;
  timeZone: string;
  createdAt: string;
  updatedAt: string;
  vanity: string;
  objectId: string;
}

export interface MusicSceneState {
  data?: tagsObject[];
  isLoading?: boolean;
  error?: string | null;
}

const musicSceneInitialState: MusicSceneState = {
  data: [],
  error: null,
  isLoading: false,
};

export const musicSceneSlice = createSlice({
  name: "musicScene",
  initialState: deepClone(musicSceneInitialState),
  reducers: {
    musicSceneStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    musicSceneSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    musicSceneFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    musicSceneReset: () => {
      return deepClone(musicSceneInitialState);
    },
  },
});

export const {
  musicSceneStarted,
  musicSceneSuccess,
  musicSceneFail,
  musicSceneReset,
} = musicSceneSlice.actions;

export const fetchMusicScene =
  (recaptchaResponse: string) => async (dispatch: any) => {
    dispatch(musicSceneStarted());
    try {
      const musicSceneData: MusicSceneState = await getMusicSceneAPI(
        recaptchaResponse
      );

      if (musicSceneData) {
        dispatch(musicSceneSuccess(musicSceneData));
      }
    } catch (e: any) {
      dispatch(musicSceneFail(e.message));
    }
  };
