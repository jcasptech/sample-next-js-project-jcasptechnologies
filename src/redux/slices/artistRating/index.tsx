import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getartistRatingAPI } from "@redux/services/artist.api";

export interface getartistRatingAPIObject {
  createdAt: string;
  updatedAt: string;
  objectId: string;
}

export interface artistRatingDataResponse {
  statusCode: number;
  status: number;
  message: string;
  responseData: getartistRatingAPIObject;
}

export interface songsSuccessResponse extends SuccessResponse {
  data: artistRatingDataResponse;
}

export interface ArtistRatingState {
  data?: artistRatingDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const artistRatingInitialState: ArtistRatingState = {
  data: {},
  error: null,
  isLoading: false,
};

export const artistDetailsRatingSlice = createSlice({
  name: "artistDetailsRating",
  initialState: deepClone(artistRatingInitialState),
  reducers: {
    artistRatingStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    artistRatingSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    artistRatingUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    artistRatingFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    artistRatingReset: () => {
      return deepClone(artistRatingInitialState);
    },
  },
});

export const {
  artistRatingStarted,
  artistRatingSuccess,
  artistRatingUpdate,
  artistRatingFail,
  artistRatingReset,
} = artistDetailsRatingSlice.actions;

export const fatchArtistDetailRating =
  (id: any, recaptchaResponse: string) => async (dispatch: any) => {
    dispatch(artistRatingStarted());
    try {
      const artistDetailsAllshowsData: artistRatingDataResponse =
        await getartistRatingAPI(id, recaptchaResponse);

      if (artistDetailsAllshowsData) {
        dispatch(artistRatingSuccess(artistDetailsAllshowsData));
      }
    } catch (e: any) {
      dispatch(artistRatingFail(e.message));
    }
  };

export const artistDetailsloadMoreAllShows =
  (queryParams: QueryParams, id: any, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistRatingStarted());
    try {
      const artistRatingData: artistRatingDataResponse =
        await getartistRatingAPI(id, recaptchaResponse);
      if (artistRatingData) {
        dispatch(artistRatingUpdate(artistRatingData));
      }
    } catch (e: any) {
      dispatch(artistRatingFail(e.message));
    }
  };
