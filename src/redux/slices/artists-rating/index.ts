import { boolean, DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getArtistRatingAPI } from "@redux/services/artist.api";
import { ArtistObject } from "../artists";

export interface ArtistRatingSuccessResponse extends SuccessResponse {
  data: ArtistObject[];
}

export interface ArtistRatingState {
  data?: ArtistObject[] | any;
  isLoading?: boolean;
  error?: string | null;
}

const artistRatingInitialState: ArtistRatingState = {
  data: [],
  error: null,
  isLoading: false,
};

export const artistRatingSlice = createSlice({
  name: "artistRating",
  initialState: deepClone(artistRatingInitialState),
  reducers: {
    ArtistRatingStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ArtistRatingSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ArtistRatingFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ArtistRatingReset: () => {
      return deepClone(artistRatingInitialState);
    },
  },
});

export const {
  ArtistRatingStarted,
  ArtistRatingSuccess,
  ArtistRatingFail,
  ArtistRatingReset,
} = artistRatingSlice.actions;

export const fetchAritstRating =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ArtistRatingStarted());
    try {
      const ArtistRatingData: ArtistObject = await getArtistRatingAPI(
        queryParams
      );

      if (ArtistRatingData) {
        dispatch(ArtistRatingSuccess(ArtistRatingData));
      }
    } catch (e: any) {
      dispatch(ArtistRatingFail(e.message));
    }
  };
