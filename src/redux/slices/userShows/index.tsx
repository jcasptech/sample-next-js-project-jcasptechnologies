import { boolean, DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { VenueObject } from "../venues";
import { getAdminShowsAPI, getArtistShowsAPI } from "@redux/services/shows.api";

export interface userObject {
  __type: string;
  className: string;
  objectId: string;
}

export interface UserShowsObject {
  attendanceCount: number;
  calendarAddCount: number;
  timeZone: string;
  earnings: number;
  promoteOnJCasp: boolean;
  visible: number;
  artist: any;
  artistName: string;
  artistIconImage: any;
  event: any;
  startDate: any;
  venue: VenueObject;
  iconImage: any;
  location: any;
  name: string;
  posterImage: any;
  sourceImage: any;
  venueName: string;
  createdAt: Date;
  updatedAt: DateLocale;
  calendarAdds: any;
  calendar_adds: any;
  objectId: string;
}

export interface UserShowsDataResponse {
  list?: UserShowsObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ShowsSuccessResponse extends SuccessResponse {
  data: UserShowsDataResponse;
}

export interface UserShowsState {
  data?: UserShowsDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const userShowsInitialState: UserShowsState = {
  data: {},
  error: null,
  isLoading: false,
};

export const UserShowsSlice = createSlice({
  name: "userShows",
  initialState: deepClone(userShowsInitialState),
  reducers: {
    UserShowsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    UserShowsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    UserShowsUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    UserShowsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    UserShowsReset: () => {
      return deepClone(userShowsInitialState);
    },
  },
});

export const {
  UserShowsStarted,
  UserShowsSuccess,
  UserShowsUpdate,
  UserShowsFail,
  UserShowsReset,
} = UserShowsSlice.actions;

export const fetchArtistShows =
  (id: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(UserShowsStarted());
    try {
      const userShowsData: UserShowsDataResponse = await getArtistShowsAPI(
        id,
        queryParams
      );
      if (userShowsData) {
        dispatch(UserShowsSuccess(userShowsData));
      }
    } catch (e: any) {
      dispatch(UserShowsFail(e.message));
    }
  };

export const loadMoreArtistShows =
  (id: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(UserShowsStarted());
    try {
      const userShowsData: UserShowsDataResponse = await getArtistShowsAPI(
        id,
        queryParams
      );
      if (userShowsData) {
        dispatch(UserShowsUpdate(userShowsData));
      }
    } catch (e: any) {
      dispatch(UserShowsFail(e.message));
    }
  };

export const fetchAdimnShows =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(UserShowsStarted());
    try {
      const userShowsData: UserShowsDataResponse = await getAdminShowsAPI(
        queryParams
      );

      if (userShowsData) {
        dispatch(UserShowsSuccess(userShowsData));
      }
    } catch (e: any) {
      dispatch(UserShowsFail(e.message));
    }
  };

export const loadMoreAdminShows =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(UserShowsStarted());
    try {
      const userShowsData: UserShowsDataResponse = await getAdminShowsAPI(
        queryParams
      );
      if (userShowsData) {
        dispatch(UserShowsUpdate(userShowsData));
      }
    } catch (e: any) {
      dispatch(UserShowsFail(e.message));
    }
  };
