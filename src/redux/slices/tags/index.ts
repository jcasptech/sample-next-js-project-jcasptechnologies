import { getTagsAPI } from "@redux/services/tags.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface tagsObject {
  tag: string;
  label: string;
  url: string;
  sourceImage: any;
  createdAt: string;
  updatedAt: string;
  objectId: string;
}

export interface TagsDataResponse {
  list?: tagsObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface tagsSuccessResponse extends SuccessResponse {
  data: tagsObject[];
}

export interface TagsState {
  data?: tagsObject[];
  isLoading?: boolean;
  error?: string | null;
}

const tagsInitialState: TagsState = {
  data: [],
  error: null,
  isLoading: false,
};

export const tagsSlice = createSlice({
  name: "tags",
  initialState: deepClone(tagsInitialState),
  reducers: {
    tagsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    tagsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    tagsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    tagsReset: () => {
      return deepClone(tagsInitialState);
    },
  },
});

export const { tagsStarted, tagsSuccess, tagsFail, tagsReset } =
  tagsSlice.actions;

export const fetchTags =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(tagsStarted());
    try {
      const tagsData: TagsDataResponse = await getTagsAPI(
        queryParams,
        recaptchaResponse
      );

      if (tagsData) {
        dispatch(tagsSuccess(tagsData));
      }
    } catch (e: any) {
      dispatch(tagsFail(e.message));
    }
  };
