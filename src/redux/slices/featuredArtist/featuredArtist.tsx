import { getFeaturedArtistAPI } from "@redux/services/artist.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { ArtistObject } from "../artists";

export interface FeaturedArtistDataResponse {
  list?: ArtistObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ArtistSuccessResponse extends SuccessResponse {
  data: FeaturedArtistDataResponse;
}

export interface FeaturedArtistState {
  data?: FeaturedArtistDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const featuredArtistInitialState: FeaturedArtistState = {
  data: {},
  error: null,
  isLoading: false,
};

export const featuredArtistSlice = createSlice({
  name: "featuredArtist",
  initialState: deepClone(featuredArtistInitialState),
  reducers: {
    featuredArtistStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    featuredArtistSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    featuredArtistUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    featuredArtistFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    featuredArtistReset: () => {
      return deepClone(featuredArtistInitialState);
    },
  },
});

export const {
  featuredArtistStarted,
  featuredArtistSuccess,
  featuredArtistFail,
  featuredArtistReset,
  featuredArtistUpdate,
} = featuredArtistSlice.actions;

export const fetchFeaturedArtist =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(featuredArtistStarted());
    try {
      const artistData: FeaturedArtistDataResponse = await getFeaturedArtistAPI(
        queryParams
      );

      if (artistData) {
        dispatch(featuredArtistSuccess(artistData));
      }
    } catch (e: any) {
      dispatch(featuredArtistFail(e.message));
    }
  };

export const loadMoreFeaturedArtist =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(featuredArtistStarted());
    try {
      const featuredArtistData: FeaturedArtistDataResponse =
        await getFeaturedArtistAPI(queryParams);

      if (featuredArtistData) {
        dispatch(featuredArtistUpdate(featuredArtistData));
      }
    } catch (e: any) {
      dispatch(featuredArtistFail(e.message));
    }
  };
