export interface CreatePostingsPayload {
  title: string;
  variety: string;
  venueId?: string;
  placeName?: string;
  promotion: boolean;
  dates: string;
  start: any;
  end: any;
  musicStyles: string[];
  expectedAttandance: string;
  playSettings: string;
  offerAmount: string;
  note: string;
  googlePlaceId?: string;
  equipmentProvided: boolean;
}
