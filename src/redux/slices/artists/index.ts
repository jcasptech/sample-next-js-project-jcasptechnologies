import { getArtistAPI } from "@redux/services/artist.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { string } from "yup/lib/locale";
import { UserObject } from "../auth";

export interface photosPayload {
  objectId?: string;
  url: string;
  name: string;
}

export interface youtubeVideoLinksPayload {
  link: string;
}

export interface songsPayload {
  url: string;
  songSingerName: string;
  songName: string;
}

export interface audiosPayload {
  url: string;
  name: string;
}

export interface UpdateArtistPayload {
  artistName: string;
  profileImage: string;
  artistElements: string[];
  artistPhonenumber: string;
  artistEmailId: string;
  artistLocaleCity: string;
  artistLocaleState: string;
  artistLocaleCountry: string;
  youtubeId: string;
  paymentPreference: string;
  paypalId: string;
  vemeoId: string;
  artistBio: string;
  JCaspVanityTag: string;
  songs: songsPayload[];
  vemeoLink: string;
  facebookPage: string;
  instagramUsername: string;
  website: string;
  liveStreamUrl: string;
  patreonLink: string;
  spotifyLink: string;
  youtubeVideoLinks: any[];
  photos: photosPayload[];
  audioFiles: audiosPayload[];
  profileTheme: "dark" | "light";
  csvFile: string;
  csvName?: string;
  isProfileImgUpdated?: string;
  youtubeChannel?: string;
}

export interface SongObject {
  likeCount: number;
  playCount: number;
  position: number;
  promoted: number;
  artist?: any;
  name: string;
  url: any;
  audioFile: any;
  duration: number;
  genres: string[];
  createdAt: Date;
  updatedAt: Date;
  artistName: string;
  format: string;
  location?: {
    latitude: number;
    longitude: number;
  };
  likes?: any;
  objectId: string;
}

export interface ArtistMetadata {
  about: string;
  theme: "dark" | "light";
  name: string;
  createdAt: Date;
  updatedAt: Date;
  artist: any;
  csv?: any;
  pdf?: any;
  city: string;
  format: string;
  location?: {
    latitude: number;
    longitude: number;
  };
  phone: string;
  state: string;
  country: string;
  youtubeId: string;
  extraYoutubeIds: any;
  youtubeVideoLinks: any;
  facebookUsername: string;
  facebookPageId: string;
  spotifyLink: string;
  instagramUsername: string;
  vemeoLink: string;
  zoom: string;
  website: string;
  patreonLink: string;
  liveStreamUrl: string;
  facebookPage: string;
  youtubeChannel: string;
  objectId: string;
}
export interface BankingObject {
  paypal?: string;
  paymentPreference: string;
  venmo?: string;
}
export interface ArtistObject {
  checkinCount: number;
  completedBookingCount: number;
  fanCount: number;
  playCount: number;
  rating: number;
  ratingCount: number;
  shareCount: number;
  completedEvents: number;
  sortPriority: number;
  reliabilityRate: number;
  isClaimed: boolean;
  isClaimable: boolean;
  user: any;
  name: string;
  artistPhotos: any;
  city: string;
  youtubeId: string;
  format: string;
  approved: boolean;
  artistDetails: any;
  metadata?: ArtistMetadata;
  banking: BankingObject;
  approvedDate: any;
  normalizedName: string;
  state: string;
  email: string;
  isSoloist: boolean;
  featuredSong: any;
  songs: SongObject[];
  location: any;
  createdAt: Date;
  updatedAt: Date;
  objectId: string;
  iconImage?: any;
  posterImage?: any;
  sourceImage?: any;
  vanity: string;
  tags?: string[];
  fans?: any;
  profileImage?: any;
  upcomingShows?: number;
  fromSignup: boolean;
  genres: string[];
  phone: string;
  adminScore: number;
  isAdminScore: boolean;
  isProfileComplete: boolean;
  lastMonthShowsCount: number;
}
export interface ArtistRatingObject {
  createdAt?: string;
  date?: any;
  objectId: string;
  rating: number;
  comment: string;
  updatedAt?: string;
  user?: UserObject;
}
export interface ArtistRatingDataResponse {
  rating: ArtistRatingObject[];
  ratingCount: any;
}

export interface ArtistDataResponse {
  list?: ArtistObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ArtistSuccessResponse extends SuccessResponse {
  data: ArtistDataResponse;
}

export interface ArtistState {
  data?: ArtistDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const artistInitialState: ArtistState = {
  data: {},
  error: null,
  isLoading: false,
};

export const artistSlice = createSlice({
  name: "artist",
  initialState: deepClone(artistInitialState),
  reducers: {
    artistStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    artistSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    artistUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    artistFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    artistReset: () => {
      return deepClone(artistInitialState);
    },
  },
});

export const {
  artistStarted,
  artistSuccess,
  artistFail,
  artistReset,
  artistUpdate,
} = artistSlice.actions;

export const fetchArtist =
  (queryParams: QueryParams, tags: string[], recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistStarted());
    try {
      const artistData: ArtistDataResponse = await getArtistAPI(
        queryParams,
        tags,
        recaptchaResponse
      );

      if (artistData) {
        dispatch(artistSuccess(artistData));
      }
    } catch (e: any) {
      dispatch(artistFail(e.message));
    }
  };

export const loadMoreArtist =
  (queryParams: QueryParams, tags: string[], recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistStarted());
    try {
      const artistData: ArtistDataResponse = await getArtistAPI(
        queryParams,
        tags,
        recaptchaResponse
      );

      if (artistData) {
        dispatch(artistUpdate(artistData));
      }
    } catch (e: any) {
      dispatch(artistFail(e.message));
    }
  };
