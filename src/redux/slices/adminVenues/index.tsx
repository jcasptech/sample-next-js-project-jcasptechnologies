import { boolean, DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { VenueObject } from "../venues";
import { getAdminShowsAPI, getArtistShowsAPI } from "@redux/services/shows.api";
import {
  getAdminVenueAPI,
  getVenueAdminVenueAPI,
} from "@redux/services/venue.api";

export interface adminVenuesDataResponse {
  list?: VenueObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface AdminVeunesSuccessResponse extends SuccessResponse {
  data: adminVenuesDataResponse | any;
}

export interface AdminVenuesState {
  data?: adminVenuesDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const adminVenuesInitialState: AdminVenuesState = {
  data: {},
  error: null,
  isLoading: false,
};

export const adminVenuesSlice = createSlice({
  name: "adminVenues",
  initialState: deepClone(adminVenuesInitialState),
  reducers: {
    adminVenuesStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    adminVenuesSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    adminVenuesUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    adminVenuesFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    adminVenuesReset: () => {
      return deepClone(adminVenuesInitialState);
    },
  },
});

export const {
  adminVenuesStarted,
  adminVenuesSuccess,
  adminVenuesUpdate,
  adminVenuesFail,
  adminVenuesReset,
} = adminVenuesSlice.actions;

export const fetchAdminVenues =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(adminVenuesStarted());
    try {
      const adminVenuesData: adminVenuesDataResponse = await getAdminVenueAPI(
        queryParams
      );
      if (adminVenuesData) {
        if (queryParams.skip === 0) {
          dispatch(adminVenuesSuccess(adminVenuesData));
        } else {
          dispatch(adminVenuesUpdate(adminVenuesData));
        }
      }
    } catch (e: any) {
      dispatch(adminVenuesFail(e.message));
    }
  };
