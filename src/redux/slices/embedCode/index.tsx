import { boolean, DateLocale } from "yup/lib/locale";
import { getShowsAPI } from "@redux/services/shows.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getEmbedCodeAPI } from "@redux/services/embed-code.api";

export interface EmbedcodeObject {
  category: string;
  embedCode: string;
  venue: any;
  artist: any;
  startDate: any;
  name: string;
  createdAt: Date;
  updatedAt: DateLocale;
  objectId: string;
}

export interface EmbedcodeDataResponse {
  list?: EmbedcodeObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface EmbedcodeSuccessResponse extends SuccessResponse {
  data: EmbedcodeDataResponse;
}

export interface EmbedcodeState {
  data?: EmbedcodeDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const embedcodeInitialState: EmbedcodeState = {
  data: {},
  error: null,
  isLoading: false,
};

export const embedCodeSlice = createSlice({
  name: "embedCode",
  initialState: deepClone(embedcodeInitialState),
  reducers: {
    EmbedcodeStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    EmbedcodeSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    EmbedcodeUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    EmbedcodeFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    EmbedcodeReset: () => {
      return deepClone(embedcodeInitialState);
    },
  },
});

export const {
  EmbedcodeStarted,
  EmbedcodeSuccess,
  EmbedcodeUpdate,
  EmbedcodeFail,
  EmbedcodeReset,
} = embedCodeSlice.actions;

export const fetchEmbedCode =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(EmbedcodeStarted());
    try {
      const showsData: EmbedcodeDataResponse = await getEmbedCodeAPI(
        queryParams
      );

      if (showsData) {
        dispatch(EmbedcodeSuccess(showsData));
      }
    } catch (e: any) {
      dispatch(EmbedcodeFail(e.message));
    }
  };

export const loadMoreEmbedcode =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(EmbedcodeStarted());
    try {
      const showsData: EmbedcodeDataResponse = await getEmbedCodeAPI(
        queryParams
      );
      if (showsData) {
        dispatch(EmbedcodeUpdate(showsData));
      }
    } catch (e: any) {
      dispatch(EmbedcodeFail(e.message));
    }
  };
