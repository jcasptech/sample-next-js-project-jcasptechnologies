import { getFaqAPI } from "@redux/services/faq.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface FaqObject {
  id: string;
  question: string;
  answer: string;
  status: string;
  faqFor: string;
  createdBy: string;
  updatedBy: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface FaqDataResponse {
  list?: FaqObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface FaqSuccessResponse extends SuccessResponse {
  data: FaqDataResponse;
}

export interface FaqState {
  data?: FaqDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const faqInitialState: FaqState = {
  data: {},
  error: null,
  isLoading: false,
};

export const faqSlice = createSlice({
  name: "faq",
  initialState: deepClone(faqInitialState),
  reducers: {
    faqStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    faqSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    faqFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    faqReset: () => {
      return deepClone(faqInitialState);
    },
  },
});

export const { faqStarted, faqSuccess, faqFail, faqReset } = faqSlice.actions;

export const fetchFaq = (queryParams: QueryParams) => async (dispatch: any) => {
  dispatch(faqStarted());
  try {
    const faqData: FaqDataResponse = await getFaqAPI(queryParams);

    if (faqData) {
      dispatch(faqSuccess(faqData));
    }
  } catch (e: any) {
    dispatch(faqFail(e.message));
  }
};
