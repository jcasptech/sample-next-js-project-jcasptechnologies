import { DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getActivePostingAPI } from "@redux/services/active-posting.api";
import { getOpenPostingAPI } from "@redux/services/open-posting.api";

export interface OpenPostingObject {
  equipmentProvided: boolean;
  selectedArtist?: any;
  address: string;
  location: any;
  endDate: any;
  note: string;
  updatedAt: string;
  timeZone: string;
  serviceCharge: string;
  startDate: any;
  variety: string;
  attendanceCategory: string;
  genres: string[];
  createdAt: string;
  city: string;
  totalCost: number;
  artistProceeds: number;
  artistFormats: string;
  serviceFee: number;
  status: string;
  state: string;
  playSetting: string;
  title: string;
  normalizedTitle: string;
  zipcode: number;
  posterImage: any;
  iconImage: any;
  submittedArtists: any;
  venue: any;
  objectId: string;
  bidCount: number;
}

export interface OpenPostingDataResponse {
  list?: OpenPostingObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface OpenPostingSuccessResponse extends SuccessResponse {
  data: OpenPostingDataResponse;
}

export interface OpenPostingState {
  data?: OpenPostingDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const openPostingInitialState: OpenPostingState = {
  data: {},
  error: null,
  isLoading: false,
};

export const openPostingSlice = createSlice({
  name: "openPosting",
  initialState: deepClone(openPostingInitialState),
  reducers: {
    OpenPostingStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    OpenPostingSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    OpenPostingUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    OpenPostingFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    OpenPostingReset: () => {
      return deepClone(openPostingInitialState);
    },
  },
});

export const {
  OpenPostingStarted,
  OpenPostingSuccess,
  OpenPostingUpdate,
  OpenPostingFail,
  OpenPostingReset,
} = openPostingSlice.actions;

export const fetchOpenPosting =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(OpenPostingStarted());
    try {
      const openPostingData: OpenPostingDataResponse = await getOpenPostingAPI(
        queryParams
      );
      if (openPostingData) {
        dispatch(OpenPostingSuccess(openPostingData));
      }
    } catch (e: any) {
      dispatch(OpenPostingFail(e.message));
    }
  };

export const loadMoreOpenPosting =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(OpenPostingStarted());
    try {
      const openPostingData: OpenPostingDataResponse = await getOpenPostingAPI(
        queryParams
      );
      if (openPostingData) {
        dispatch(OpenPostingUpdate(openPostingData));
      }
    } catch (e: any) {
      dispatch(OpenPostingFail(e.message));
    }
  };
