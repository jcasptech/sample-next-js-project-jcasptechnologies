import { DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getActivePostingAPI } from "@redux/services/active-posting.api";
import { getOpenPostingAPI } from "@redux/services/open-posting.api";
import { getConfirmedEventsAPI } from "@redux/services/submitted-posting.api";
import { ArtistObject } from "../artists";
import { VenueObject } from "../venues";

export interface ConfirmedEventObject {
  artist: ArtistObject | any;
  attendanceCategory: string;
  bookingStatus: string;
  confirmedDate: any;
  createdAt: any;
  endDate: any;
  note: string;
  objectId: string;
  playSetting: string;
  startDate: any;
  title: string;
  totalCost: number;
  updatedAt: any;
  variety: string;
  venue: VenueObject | any;
  iconImage: any;
  timeZone: string;
  artistProceeds: number;
  genres: string[];
}

export interface ConfirmedEventDataResponse {
  list?: ConfirmedEventObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ConfirmedEventSuccessResponse extends SuccessResponse {
  data: ConfirmedEventDataResponse;
}

export interface ConfirmedEventState {
  data?: ConfirmedEventDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const confirmedEventInitialState: ConfirmedEventState = {
  data: {},
  error: null,
  isLoading: false,
};

export const confirmedEventSlice = createSlice({
  name: "confirmedEvent",
  initialState: deepClone(confirmedEventInitialState),
  reducers: {
    ConfirmedEventStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ConfirmedEventSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ConfirmedEventUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    ConfirmedEventFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ConfirmedEventReset: () => {
      return deepClone(confirmedEventInitialState);
    },
  },
});

export const {
  ConfirmedEventStarted,
  ConfirmedEventSuccess,
  ConfirmedEventUpdate,
  ConfirmedEventFail,
  ConfirmedEventReset,
} = confirmedEventSlice.actions;

export const fetchConfirmedEvent =
  (artistId: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ConfirmedEventStarted());
    try {
      const ConfirmedEventData: ConfirmedEventDataResponse =
        await getConfirmedEventsAPI(artistId, queryParams);
      if (ConfirmedEventData) {
        dispatch(ConfirmedEventSuccess(ConfirmedEventData));
      }
    } catch (e: any) {
      dispatch(ConfirmedEventFail(e.message));
    }
  };

export const loadMoreConfirmedEvent =
  (artistId: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ConfirmedEventStarted());
    try {
      const ConfirmedEventData: ConfirmedEventDataResponse =
        await getConfirmedEventsAPI(artistId, queryParams);
      if (ConfirmedEventData) {
        dispatch(ConfirmedEventUpdate(ConfirmedEventData));
      }
    } catch (e: any) {
      dispatch(ConfirmedEventFail(e.message));
    }
  };
