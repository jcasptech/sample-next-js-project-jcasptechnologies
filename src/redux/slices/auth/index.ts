import {
  facebookLoginAPI,
  getLoginUser,
  loginAPI,
  logoutAPI,
} from "@redux/services/auth.api";
import { createSlice } from "@reduxjs/toolkit";
import { definePermission } from "react-redux-permission";
import { PERMISSIONS } from "src/libs/constants";
import { deepClone } from "src/libs/helpers";
import { selectIdReset } from "../artistId";
import { ArtistListReset } from "../artistList";

export interface SessionObject {
  id: string;
  orgId: string;
  userId: string;
  accessToken: string;
  geoIPCountry: string;
  ipAddress: string;
  userAgent: string;
  status: string;
  loginAt: Date;
  expireAt: Date;
  expiredAt: Date;
  createdAt: Date;
  updatedAt: Date;
}

export interface UserObject {
  username: string;
  artists?: any;
  firstname: string;
  surname: string;
  lastname: string;
  name: string;
  email: string;
  role: number;
  emailVerified: boolean;
  createdAt: Date;
  updatedAt: Date;
  objectId: string;
  follows?: string[];
  iconImage?: any;
}

export interface LoginPayload {
  username: string;
  password: string;
}

export interface FacebookLinkPayload {
  facebookToken: string;
}
export interface FacebookLoginPayload {
  facebookToken: string;
  type: "SIGNIN" | "SIGNUP";
}

export interface LoginUserPayload {
  Authorization: string;
}

export interface LogoutUserPayload {
  sessionId: string;
  noApiCall?: boolean;
}

export interface LoginDataResponse {
  accessToken?: string;
  userType?: "USER" | "ADMIN" | "MANAGER" | "VENUE_ADMIN";
  user?: UserObject;
}

export interface LoginSuccessResponse {
  data: LoginDataResponse;
}

export interface LoginState {
  data?: LoginDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

export interface LoginUserState {
  data?: LoginDataResponse | any;
  isLogin?: boolean;
  error?: string | null;
  isLoading?: boolean;
}

export interface ChangePasswordPayload {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}

export interface ForgotPasswordPayload {
  email: string;
  token: string;
}

export interface ResetPasswordPayload {
  resetPasswordHash?: string;
  password: string;
  token: string;
  newPassword?: string;
  confirmPassword: string;
}

const loginInitialState: LoginState = {
  data: {},
  error: null,
  isLoading: false,
};

const loginUserInitialState: LoginUserState = {
  data: {},
  error: null,
  isLogin: false,
  isLoading: false,
};

export const loginSlice = createSlice({
  name: "login",
  initialState: deepClone(loginInitialState),
  reducers: {
    loginStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    loginSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    loginFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    loginReset: () => {
      return deepClone(loginInitialState);
    },
  },
});

export const loginUserSlice = createSlice({
  name: "loginUser",
  initialState: deepClone(loginUserInitialState),
  reducers: {
    loginUserStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    loginUserSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
          isLogin: true,
        },
      };
    },
    loginUserUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            ...state.data,
            ...action.payload,
          },
          error: null,
          isLoading: false,
          isLogin: true,
        },
      };
    },
    loginUserFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    loginUserReset: () => {
      return deepClone(loginUserInitialState);
    },
  },
});

export const { loginSuccess, loginStarted, loginFail, loginReset } =
  loginSlice.actions;

export const {
  loginUserSuccess,
  loginUserStarted,
  loginUserUpdate,
  loginUserFail,
  loginUserReset,
} = loginUserSlice.actions;

export const doLogin =
  (data: LoginPayload, recaptchaResponse: string) => async (dispatch: any) => {
    dispatch(loginStarted());
    try {
      const auth: LoginSuccessResponse = await loginAPI(
        data,
        recaptchaResponse
      );
      if (auth) {
        dispatch(loginSuccess(auth));
      }
    } catch (e: any) {
      dispatch(loginFail(e.message));
    }
  };

export const doFacebookLogin =
  (data: FacebookLoginPayload, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(loginStarted());
    try {
      const auth: LoginSuccessResponse = await facebookLoginAPI(
        data,
        recaptchaResponse
      );
      if (auth) {
        dispatch(loginSuccess(auth));
      }
    } catch (e: any) {
      dispatch(loginFail(e.message));
    }
  };

export const fetchLoginUser =
  (data: LoginUserPayload) => async (dispatch: any) => {
    dispatch(loginUserStarted());
    try {
      const auth: LoginDataResponse = await getLoginUser(data);
      if (auth) {
        dispatch(loginUserSuccess(auth));
        if (auth?.userType) {
          dispatch(
            definePermission(
              PERMISSIONS["COMMON"].concat(PERMISSIONS[auth?.userType])
            )
          );
        }
      }
    } catch (e: any) {
      dispatch(loginReset());
      dispatch(loginUserReset());
      dispatch(ArtistListReset());
      dispatch(selectIdReset());
      dispatch(loginUserFail(e.message));
    }
  };

export const logoutUser =
  (data: LogoutUserPayload) => async (dispatch: any) => {
    try {
      dispatch(loginReset());
      dispatch(loginUserReset());
      if (!data.noApiCall) {
        await logoutAPI();
      }
    } catch (e: any) {
      console.log("LogoutUser", e);
    }
  };
