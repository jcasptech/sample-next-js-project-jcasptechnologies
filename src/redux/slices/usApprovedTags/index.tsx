import { getTagsAPI, getUnapprovedAPI } from "@redux/services/tags.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface unApprovetagsObject {
  tag: string;
  label: string;
  url: string;
  artist: any;
  sourceImage: any;
  createdAt: string;
  updatedAt: string;
  objectId: string;
}

export interface UnApprovedTagsDataResponse {
  list?: unApprovetagsObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface tagsSuccessResponse extends SuccessResponse {
  data: unApprovetagsObject[];
}

export interface UnApprovedTagsState {
  data?: unApprovetagsObject[];
  isLoading?: boolean;
  error?: string | null;
}

const tagsInitialState: UnApprovedTagsState = {
  data: [],
  error: null,
  isLoading: false,
};

export const unapporvedtagsSlice = createSlice({
  name: "unApprovedTags",
  initialState: deepClone(tagsInitialState),
  reducers: {
    tagsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    tagsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    tagsUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    tagsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    tagsReset: () => {
      return deepClone(tagsInitialState);
    },
  },
});

export const { tagsStarted, tagsSuccess, tagsUpdate, tagsFail, tagsReset } =
  unapporvedtagsSlice.actions;

export const fetchUnApprovedTags =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(tagsStarted());
    try {
      const unApprovedtagsData: UnApprovedTagsDataResponse =
        await getUnapprovedAPI(queryParams);

      if (unApprovedtagsData) {
        dispatch(tagsSuccess(unApprovedtagsData));
      }
    } catch (e: any) {
      dispatch(tagsFail(e.message));
    }
  };

export const loadMoreUnApprovedTags =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(tagsStarted());
    try {
      const unApprovedtagsData: UnApprovedTagsDataResponse =
        await getUnapprovedAPI(queryParams);

      if (unApprovedtagsData) {
        dispatch(tagsUpdate(unApprovedtagsData));
      }
    } catch (e: any) {
      dispatch(tagsFail(e.message));
    }
  };
