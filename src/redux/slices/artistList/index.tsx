import {
  getArtistDetailAPI,
  getUserArtistAPI,
} from "@redux/services/artist.api";
import { SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { ArtistObject } from "../artists";

export interface sortArtist {
  updatedAt: Date;
  createdAt: Date;
  name: String;
  objectId: String;
}
export interface SelectedArtistDataResponse {
  list?: sortArtist[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ArtistListSuccessResponse extends SuccessResponse {
  data: SelectedArtistDataResponse;
}

export interface SeledtedArtistState {
  data?: SelectedArtistDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const ArtistListInitialState: SeledtedArtistState = {
  data: {},
  error: null,
  isLoading: false,
};

export const artistListSlice = createSlice({
  name: "artistList",
  initialState: deepClone(ArtistListInitialState),
  reducers: {
    ArtistListStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ArtistListSuccess: (state, action) => {
      return {
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ArtistListUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    ArtistListFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ArtistListReset: () => {
      return deepClone(ArtistListInitialState);
    },
  },
});

export const {
  ArtistListStarted,
  ArtistListSuccess,
  ArtistListFail,
  ArtistListReset,
  ArtistListUpdate,
} = artistListSlice.actions;

export const fetchArtistList = (artistIds: any[]) => async (dispatch: any) => {
  dispatch(ArtistListStarted());
  try {
    const ArtistListData: ArtistListSuccessResponse = await getUserArtistAPI(
      artistIds
    );
    if (ArtistListData) {
      dispatch(ArtistListSuccess(ArtistListData));
    }
  } catch (e: any) {
    dispatch(ArtistListFail(e.message));
  }
};
