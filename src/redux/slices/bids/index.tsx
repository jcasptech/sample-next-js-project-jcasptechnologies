import {
  FileObject,
  PostingObject,
  PostingParseObject,
  QueryParams,
  SuccessResponse,
} from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getBidsAPI } from "@redux/services/bids.api";
import { ArtistObject } from "../artists";

export interface bidObject {
  note: string;
  status: string;
  chatEnabled: boolean;
  posting?: PostingParseObject | PostingObject;
  artist: ArtistObject;
  sortPriority: number;
  unreadMessagesArtist: number;
  unreadMessagesUser: number;
  artistName: string;
  artistIconImage?: FileObject;
  objectId: string;
}

export interface BidsDataResponse {
  list?: bidObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface BidsSuccessResponse extends SuccessResponse {
  data: BidsDataResponse;
}

export interface BidsState {
  data?: BidsDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const bidsInitialState: BidsState = {
  data: {},
  error: null,
  isLoading: false,
};

export const bidsSlice = createSlice({
  name: "bids",
  initialState: deepClone(bidsInitialState),
  reducers: {
    BidsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    BidsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    BidsUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    BidsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    BidsReset: () => {
      return deepClone(bidsInitialState);
    },
  },
});

export const { BidsStarted, BidsSuccess, BidsUpdate, BidsFail, BidsReset } =
  bidsSlice.actions;

export const fetchBids =
  (id: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(BidsStarted());
    try {
      const bidsData: BidsDataResponse = await getBidsAPI(id, queryParams);
      if (bidsData) {
        dispatch(BidsSuccess(bidsData));
      }
    } catch (e: any) {
      dispatch(BidsFail(e.message));
    }
  };

export const loadMoreBids =
  (id: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(BidsStarted());
    try {
      const bidsData: BidsDataResponse = await getBidsAPI(id, queryParams);
      if (bidsData) {
        dispatch(BidsUpdate(bidsData));
      }
    } catch (e: any) {
      dispatch(BidsFail(e.message));
    }
  };
