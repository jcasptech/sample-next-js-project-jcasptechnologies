import { boolean, DateLocale } from "yup/lib/locale";
import { getShowsAPI } from "@redux/services/shows.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import {
  getArtistAllShowAPI,
  getArtistUpcomingShowAPI,
} from "@redux/services/artist.api";

export interface userObject {
  __type: string;
  className: string;
  objectId: string;
}

export interface showsObject {
  startDate: any;
  name: string;
  venueName: string;
  iconImage: any;
  createdAt: string;
  updatedAt: string;
  calendarAdds: any;
  calendar_adds: any;
  role: any;
  objectId: string;
}

export interface artistShowsResponse {
  list?: showsObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface artistShowsState {
  data?: artistShowsResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const artistShowsInitialState: artistShowsState = {
  data: {},
  error: null,
  isLoading: false,
};

export const artistShowsSlice = createSlice({
  name: "artistShows",
  initialState: deepClone(artistShowsInitialState),
  reducers: {
    artistShowsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    artistShowsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    artistShowsUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    artistShowsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    artistShowsReset: () => {
      return deepClone(artistShowsInitialState);
    },
  },
});

export const {
  artistShowsStarted,
  artistShowsSuccess,
  artistShowsUpdate,
  artistShowsFail,
  artistShowsReset,
} = artistShowsSlice.actions;

export const artistAllShows =
  (id: any, queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistShowsStarted());
    try {
      const artistAllshowsData: artistShowsResponse = await getArtistAllShowAPI(
        id,
        queryParams,
        recaptchaResponse
      );

      if (artistAllshowsData) {
        dispatch(artistShowsSuccess(artistAllshowsData));
      }
    } catch (e: any) {
      dispatch(artistShowsFail(e.message));
    }
  };

export const artistloadMoreAllShows =
  (id: string, queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistShowsStarted());
    try {
      const artistAllshowsData: artistShowsResponse = await getArtistAllShowAPI(
        id,
        queryParams,
        recaptchaResponse
      );
      if (artistAllshowsData) {
        dispatch(artistShowsUpdate(artistAllshowsData));
      }
    } catch (e: any) {
      dispatch(artistShowsFail(e.message));
    }
  };

export const artistUpComingShows =
  (id: any, queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistShowsStarted());
    try {
      const artistAllshowsData: artistShowsResponse =
        await getArtistUpcomingShowAPI(id, queryParams, recaptchaResponse);
      if (artistAllshowsData) {
        dispatch(artistShowsSuccess(artistAllshowsData));
      }
    } catch (e: any) {
      dispatch(artistShowsFail(e.message));
    }
  };

export const artistloadMoreUpComingShows =
  (id: string, queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistShowsStarted());
    try {
      const artistAllshowsData: artistShowsResponse =
        await getArtistUpcomingShowAPI(id, queryParams, recaptchaResponse);
      if (artistAllshowsData) {
        dispatch(artistShowsUpdate(artistAllshowsData));
      }
    } catch (e: any) {
      dispatch(artistShowsFail(e.message));
    }
  };
