import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getPostBidsAPI } from "@redux/services/active-posting.api";

export interface bidsObject {
  note: string;
  status: string;
  chatEnabled: false;
  posting: any;
  artist: any;
  sortPriority: 10;
  unreadMessagesArtist: 0;
  unreadMessagesUser: 0;
  artistName: string;
  artistIconImage: any;
  createdAt: string;
  updatedAt: string;
  objectId: string;
}

export interface ActivebidsDataResponse {
  list?: bidsObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ActiveBidsSuccessResponse extends SuccessResponse {
  data: ActivebidsDataResponse;
}

export interface ActiveBidsState {
  data?: ActivebidsDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const activePostingBidsInitialState: ActiveBidsState = {
  data: {},
  error: null,
  isLoading: false,
};

export const activePostingBidsSlice = createSlice({
  name: "activePostingBids",
  initialState: deepClone(activePostingBidsInitialState),
  reducers: {
    ActivePostingBidsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ActivePostingBidsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ActivePostingBidsUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    ActivePostingBidsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ActivePostingBidsReset: () => {
      return deepClone(activePostingBidsInitialState);
    },
  },
});

export const {
  ActivePostingBidsStarted,
  ActivePostingBidsSuccess,
  ActivePostingBidsUpdate,
  ActivePostingBidsFail,
  ActivePostingBidsReset,
} = activePostingBidsSlice.actions;

export const fetchActivePostingBids =
  (id: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ActivePostingBidsStarted());
    try {
      const userShowsData: ActivebidsDataResponse = await getPostBidsAPI(
        id,
        queryParams
      );
      if (userShowsData) {
        dispatch(ActivePostingBidsSuccess(userShowsData));
      }
    } catch (e: any) {
      dispatch(ActivePostingBidsFail(e.message));
    }
  };

export const loadMoreActivePostingBids =
  (id: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ActivePostingBidsStarted());
    try {
      const userShowsData: ActivebidsDataResponse = await getPostBidsAPI(
        id,
        queryParams
      );
      if (userShowsData) {
        dispatch(ActivePostingBidsUpdate(userShowsData));
      }
    } catch (e: any) {
      dispatch(ActivePostingBidsFail(e.message));
    }
  };
