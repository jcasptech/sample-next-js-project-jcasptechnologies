import { getBrowseArtistAPI } from "@redux/services/artist.api";
import { getArtistFollowsAPI } from "@redux/services/auth.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { ArtistObject } from "../artists";

export interface BrowseArtistDataResponse {
  list?: ArtistObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ArtistSuccessResponse extends SuccessResponse {
  data: BrowseArtistDataResponse;
}

export interface BrowseArtistState {
  data?: BrowseArtistDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const artistInitialState: BrowseArtistState = {
  data: {},
  error: null,
  isLoading: false,
};

export const browseArtistSlice = createSlice({
  name: "browseArtist",
  initialState: deepClone(artistInitialState),
  reducers: {
    artistStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    artistSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    artistUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    artistFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    artistReset: () => {
      return deepClone(artistInitialState);
    },
  },
});

export const {
  artistStarted,
  artistSuccess,
  artistFail,
  artistReset,
  artistUpdate,
} = browseArtistSlice.actions;

export const fetchBrowseArtist =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistStarted());
    try {
      const artistData: BrowseArtistDataResponse = await getBrowseArtistAPI(
        queryParams,
        recaptchaResponse
      );

      if (artistData) {
        dispatch(artistSuccess(artistData));
      }
    } catch (e: any) {
      dispatch(artistFail(e.message));
    }
  };

export const loadMoreBrowseArtist =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(artistStarted());
    try {
      const browseArtistData: BrowseArtistDataResponse =
        await getBrowseArtistAPI(queryParams, recaptchaResponse);

      if (browseArtistData) {
        dispatch(artistUpdate(browseArtistData));
      }
    } catch (e: any) {
      dispatch(artistFail(e.message));
    }
  };

export const fetchArtistFollows =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(artistStarted());
    try {
      const artistData: BrowseArtistDataResponse = await getArtistFollowsAPI(
        queryParams
      );

      if (artistData) {
        dispatch(artistSuccess(artistData));
      }
    } catch (e: any) {
      dispatch(artistFail(e.message));
    }
  };

export const loadMoreArtistFollows =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(artistStarted());
    try {
      const browseArtistData: BrowseArtistDataResponse =
        await getArtistFollowsAPI(queryParams);
      if (browseArtistData) {
        dispatch(artistUpdate(browseArtistData));
      }
    } catch (e: any) {
      dispatch(artistFail(e.message));
    }
  };
