import { getArtistAPI } from "@redux/services/artist.api";
import { getVenueFollowsAPI } from "@redux/services/auth.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { getVenuesAPI } from "@redux/services/venue.api";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface VenueObject {
  googlePlaceId: string;
  timeZone: string;
  location?: {
    latitude: number;
    longitude: number;
  };
  about: string;
  name: string;
  website: string;
  city: string;
  state: string;
  address: string;
  sourceImage?: {
    name: string;
    url: string;
  };
  approved: boolean;
  selectable: 1;
  normalizedName: string;
  posterImage?: {
    name: string;
    url: string;
  };
  iconImage?: {
    name: string;
    url: string;
  };
  vanity: string;
  createdAt: Date;
  updatedAt: Date;
  phone: string;
  objectId: string;
  advanceSheetUrl: string;
  instagramUsername: string;
  facebookPageId: string;
  linkedInURL: string;
  viewCount: number;
  sortPriority: number;
  adminScore: number;
  isAdminScore: boolean;
  fanCount: number;
  nextMonthUpcomingShowCount: number;
  checkinCount: number;
}
export interface VenuesDetilsDataResponse {
  venue: VenueObject;
  venuePhotos: string[];
}

export interface VenuesDataResponse {
  list?: VenueObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface VenuesSuccessResponse extends SuccessResponse {
  data: VenuesDataResponse;
}

export interface VenuesState {
  data?: VenuesDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const venuesInitialState: VenuesState = {
  data: {},
  error: null,
  isLoading: false,
};

export const venuesSlice = createSlice({
  name: "venues",
  initialState: deepClone(venuesInitialState),
  reducers: {
    venuesStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    venuesSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    venuesUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    venuesFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    venuesReset: () => {
      return deepClone(venuesInitialState);
    },
  },
});

export const {
  venuesStarted,
  venuesSuccess,
  venuesFail,
  venuesReset,
  venuesUpdate,
} = venuesSlice.actions;

export const fetchVenues =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(venuesStarted());
    try {
      const venuesData: VenuesDataResponse = await getVenuesAPI(
        queryParams,
        recaptchaResponse
      );

      if (venuesData) {
        dispatch(venuesSuccess(venuesData));
      }
    } catch (e: any) {
      dispatch(venuesFail(e.message));
    }
  };

export const loadMoreVenues =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(venuesStarted());
    try {
      const venuesData: VenuesDataResponse = await getVenuesAPI(
        queryParams,
        recaptchaResponse
      );

      if (venuesData) {
        dispatch(venuesUpdate(venuesData));
      }
    } catch (e: any) {
      dispatch(venuesFail(e.message));
    }
  };

export const fetchVenuesFollows =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(venuesStarted());
    try {
      const venuesData: VenuesDataResponse = await getVenueFollowsAPI(
        queryParams
      );

      if (venuesData) {
        dispatch(venuesSuccess(venuesData));
      }
    } catch (e: any) {
      dispatch(venuesFail(e.message));
    }
  };

export const loadMoreVenuesFollows =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(venuesStarted());
    try {
      const venuesData: VenuesDataResponse = await getVenueFollowsAPI(
        queryParams
      );

      if (venuesData) {
        dispatch(venuesUpdate(venuesData));
      }
    } catch (e: any) {
      dispatch(venuesFail(e.message));
    }
  };
