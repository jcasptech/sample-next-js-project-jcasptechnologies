import { DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getConfirmPostingAPI } from "@redux/services/active-posting.api";

export interface confirmPostingObject {
  equipmentProvided: boolean;
  address: string;
  location: any;
  endDate: any;
  note: string;
  updatedAt: string;
  timeZone: string;
  serviceCharge: string;
  startDate: any;
  variety: string;
  attendanceCategory: string;
  genres: string[];
  createdAt: string;
  city: string;
  totalCost: number;
  artistProceeds: number;
  artistFormats: string;
  serviceFee: number;
  status: string;
  state: string;
  playSetting: string;
  title: string;
  normalizedTitle: string;
  zipcode: number;
  posterImage: any;
  iconImage: any;
  submittedArtists: any;
  venue: any;
  objectId: string;
  bidCount: number;
}

export interface ConfirmPostingDataResponse {
  list?: confirmPostingObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface ConfirmPostingSuccessResponse extends SuccessResponse {
  data: ConfirmPostingDataResponse;
}

export interface ConfirmPostingState {
  data?: ConfirmPostingDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const confirmPostingInitialState: ConfirmPostingState = {
  data: {},
  error: null,
  isLoading: false,
};

export const confirmPostingSlice = createSlice({
  name: "confirmPosting",
  initialState: deepClone(confirmPostingInitialState),
  reducers: {
    ConfirmPostingStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ConfirmPostingSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ConfirmPostingUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    ConfirmPostingFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ConfirmPostingReset: () => {
      return deepClone(confirmPostingInitialState);
    },
  },
});

export const {
  ConfirmPostingStarted,
  ConfirmPostingSuccess,
  ConfirmPostingUpdate,
  ConfirmPostingFail,
  ConfirmPostingReset,
} = confirmPostingSlice.actions;

export const fetchConfirmPosting =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ConfirmPostingStarted());
    try {
      const userShowsData: ConfirmPostingDataResponse =
        await getConfirmPostingAPI(queryParams);
      if (userShowsData) {
        dispatch(ConfirmPostingSuccess(userShowsData));
      }
    } catch (e: any) {
      dispatch(ConfirmPostingFail(e.message));
    }
  };

export const loadMoreConfirmPosting =
  (queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(ConfirmPostingStarted());
    try {
      const userShowsData: ConfirmPostingDataResponse =
        await getConfirmPostingAPI(queryParams);
      if (userShowsData) {
        dispatch(ConfirmPostingUpdate(userShowsData));
      }
    } catch (e: any) {
      dispatch(ConfirmPostingFail(e.message));
    }
  };
