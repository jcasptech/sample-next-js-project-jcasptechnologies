import { QueryParams, SuccessResponse } from "@redux/services/types";
import { getVenueAPI } from "@redux/services/venue.api";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface venueObject {
  googlePlaceId: string;
  timeZone: string;
  location: any;
  name: string;
  city: string;
  state: string;
  sourceImage: any;
  approved: boolean;
  selectable: number;
  normalizedName: string;
  posterImage: any;
  iconImage: any;
  vanity: string;
  createdAt: Date;
  updatedAt: Date;
  address: string;
  objectId: string;
}

export interface venueDataResponse {
  list?: venueObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface venueSuccessResponse extends SuccessResponse {
  data: venueDataResponse;
}

export interface VenueState {
  data?: venueDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const venueInitialState: VenueState = {
  data: {},
  error: null,
  isLoading: false,
};

export const venueSlice = createSlice({
  name: "venueDetails",
  initialState: deepClone(venueInitialState),
  reducers: {
    venueStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    venueSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    venueFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    venueReset: () => {
      return deepClone(venueInitialState);
    },
  },
});

export const { venueStarted, venueSuccess, venueFail, venueReset } =
  venueSlice.actions;

export const fetchVenue =
  (queryParams: QueryParams, id: string, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(venueStarted());
    try {
      const venueData: venueDataResponse = await getVenueAPI(
        queryParams,
        id,
        recaptchaResponse
      );

      if (venueData) {
        dispatch(venueSuccess(venueData));
      }
    } catch (e: any) {
      dispatch(venueFail(e.message));
    }
  };
