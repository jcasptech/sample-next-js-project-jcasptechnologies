import {
  getArtistDetailAPI,
  getUserArtistAPI,
} from "@redux/services/artist.api";
import { SuccessResponse } from "@redux/services/types";
import { getVenueUserListAPI } from "@redux/services/venue.api";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { ArtistObject } from "../artists";

export interface useListObject {
  updatedAt: Date;
  createdAt: Date;
  name: string;
  objectId: string;
  surname: string;
  username: string;
  role: 3;
  referral: string;
  artists: any;
}

export interface venueUserListDataResponse {
  list?: useListObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface venueUserListSuccessResponse extends SuccessResponse {
  data: venueUserListDataResponse;
}

export interface VenueUserState {
  data?: venueUserListDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const venueUserListInitialState: VenueUserState = {
  data: {},
  error: null,
  isLoading: false,
};

export const venueUserListSlice = createSlice({
  name: "venueUserList",
  initialState: deepClone(venueUserListInitialState),
  reducers: {
    venueUserListStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    venueUserListSuccess: (state, action) => {
      return {
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    venueUserListUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    venueUserListFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    venueUserListReset: () => {
      return deepClone(venueUserListInitialState);
    },
  },
});

export const {
  venueUserListStarted,
  venueUserListSuccess,
  venueUserListFail,
  venueUserListReset,
  venueUserListUpdate,
} = venueUserListSlice.actions;

export const fetchvenueUserList = () => async (dispatch: any) => {
  dispatch(venueUserListStarted());
  try {
    const venueUserListData: venueUserListSuccessResponse =
      await getVenueUserListAPI();
    if (venueUserListData) {
      dispatch(venueUserListSuccess(venueUserListData));
    }
  } catch (e: any) {
    dispatch(venueUserListFail(e.message));
  }
};
