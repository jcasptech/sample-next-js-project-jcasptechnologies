import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

const selectIdInitialState = {
  data: "",
  error: null,
  isLoading: false,
};

export const selectIdSlice = createSlice({
  name: "selectId",
  initialState: deepClone(selectIdInitialState),
  reducers: {
    selectIdStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    selectIdSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    selectIdFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    selectIdReset: () => {
      return deepClone(selectIdInitialState);
    },
  },
});

export const { selectIdStarted, selectIdSuccess, selectIdFail, selectIdReset } =
  selectIdSlice.actions;
