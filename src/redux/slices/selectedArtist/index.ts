import { getArtistDetailAPI } from "@redux/services/artist.api";
import { SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { ArtistObject } from "../artists";

export interface SelectedArtistDataResponse {
  list?: ArtistObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface selectedArtistSuccessResponse extends SuccessResponse {
  data: SelectedArtistDataResponse;
}

export interface SeledtedArtistState {
  data?: SelectedArtistDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const selectedArtistInitialState: SeledtedArtistState = {
  data: {},
  error: null,
  isLoading: false,
};

export const selectedArtistSlice = createSlice({
  name: "selectedArtist",
  initialState: deepClone(selectedArtistInitialState),
  reducers: {
    selectedArtistStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    selectedArtistSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    selectedArtistUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    selectedArtistFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    selectedArtistReset: () => {
      return deepClone(selectedArtistInitialState);
    },
  },
});

export const {
  selectedArtistStarted,
  selectedArtistSuccess,
  selectedArtistFail,
  selectedArtistReset,
  selectedArtistUpdate,
} = selectedArtistSlice.actions;

export const fetchSelectedArtist = (id: string) => async (dispatch: any) => {
  dispatch(selectedArtistStarted());
  try {
    const selectedArtistData: selectedArtistSuccessResponse =
      await getArtistDetailAPI(id);
    if (selectedArtistData) {
      dispatch(selectedArtistSuccess(selectedArtistData));
    }
  } catch (e: any) {
    dispatch(selectedArtistFail(e.message));
  }
};
