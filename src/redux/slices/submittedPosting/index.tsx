import { DateLocale } from "yup/lib/locale";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";
import { getActivePostingAPI } from "@redux/services/active-posting.api";
import { getOpenPostingAPI } from "@redux/services/open-posting.api";
import { getSubmittedPostingAPI } from "@redux/services/submitted-posting.api";

export interface SubmittedPostingObject {
  equipmentProvided: boolean;
  selectedArtist?: any;
  address: string;
  location: any;
  endDate: any;
  note: string;
  updatedAt: string;
  timeZone: string;
  serviceCharge: string;
  startDate: any;
  variety: string;
  attendanceCategory: string;
  genres: string[];
  createdAt: string;
  city: string;
  totalCost: number;
  artistProceeds: number;
  artistFormats: string;
  serviceFee: number;
  status: string;
  state: string;
  playSetting: string;
  title: string;
  normalizedTitle: string;
  zipcode: number;
  posterImage: any;
  iconImage: any;
  submittedArtists: any;
  venue: any;
  objectId: string;
  bidCount: number;
}

export interface SubmittedPostingDataResponse {
  list?: SubmittedPostingObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface SubmittedPostingSuccessResponse extends SuccessResponse {
  data: SubmittedPostingDataResponse;
}

export interface SubmittedPostingState {
  data?: SubmittedPostingDataResponse | any;
  isLoading?: boolean;
  error?: string | null;
}

const submittedPostingInitialState: SubmittedPostingState = {
  data: {},
  error: null,
  isLoading: false,
};

export const submittedPostingSlice = createSlice({
  name: "submittedPosting",
  initialState: deepClone(submittedPostingInitialState),
  reducers: {
    SubmittedPostingStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    SubmittedPostingSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    SubmittedPostingUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    SubmittedPostingFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    SubmittedPostingReset: () => {
      return deepClone(submittedPostingInitialState);
    },
  },
});

export const {
  SubmittedPostingStarted,
  SubmittedPostingSuccess,
  SubmittedPostingUpdate,
  SubmittedPostingFail,
  SubmittedPostingReset,
} = submittedPostingSlice.actions;

export const fetchSubmittedPosting =
  (artistId: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(SubmittedPostingStarted());
    try {
      const SubmittedPostingData: SubmittedPostingDataResponse =
        await getSubmittedPostingAPI(artistId, queryParams);
      if (SubmittedPostingData) {
        dispatch(SubmittedPostingSuccess(SubmittedPostingData));
      }
    } catch (e: any) {
      dispatch(SubmittedPostingFail(e.message));
    }
  };

export const loadMoreSubmittedPosting =
  (artistId: string, queryParams: QueryParams) => async (dispatch: any) => {
    dispatch(SubmittedPostingStarted());
    try {
      const SubmittedPostingData: SubmittedPostingDataResponse =
        await getSubmittedPostingAPI(artistId, queryParams);
      if (SubmittedPostingData) {
        dispatch(SubmittedPostingUpdate(SubmittedPostingData));
      }
    } catch (e: any) {
      dispatch(SubmittedPostingFail(e.message));
    }
  };
