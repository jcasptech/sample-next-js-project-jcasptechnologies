import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface GeneralState {
  currentLocation: {
    Latitude: number;
    Longitude: number;
    City?: string;
  };
  theme: string;
  enquiryType: string;
  enquiryPhone: string;
}

export interface orderDetail {
  client_reference_id: string;
  email: string;
  subscriptionplan_id: string;
  subscriptionplan_details: string;
  data_json: string;
  payment_status: string;
  created: string;
  modified: string;
  createdAt: string;
  updatedAt: string;
  checkout_session: string;
  order_id: string;
  objectId: string;
}

const generalInitialState: GeneralState = {
  currentLocation: {
    Latitude: 32.883,
    Longitude: -117.156,
  },
  theme: "",
  enquiryType: "Get in Touch",
  enquiryPhone: "",
};

export const generalSlice = createSlice({
  name: "general",
  initialState: deepClone(generalInitialState),
  reducers: {
    setLocation: (state, action) => {
      return {
        ...state,
        currentLocation: action.payload,
      };
    },
    setEnquiryType: (state, action) => {
      return {
        ...state,
        enquiryType: action.payload,
      };
    },
    setEnquiryPhone: (state, action) => {
      return {
        ...state,
        enquiryPhone: action.payload,
      };
    },
    setGeneralTheme: (state, action) => {
      return {
        ...state,
        theme: action.payload,
      };
    },
    generalReset: () => {
      return deepClone(generalInitialState);
    },
  },
});

export const {
  setLocation,
  setGeneralTheme,
  generalReset,
  setEnquiryType,
  setEnquiryPhone,
} = generalSlice.actions;
