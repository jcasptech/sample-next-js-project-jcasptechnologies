import { getQuestionsAPI } from "@redux/services/admin.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface questionsObject {
  userQuestion: string;
  createdAt: string;
  updatedAt: string;
  objectId: string;
}

export interface QuestionsTagsDataResponse {
  list?: questionsObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface tagsSuccessResponse extends SuccessResponse {
  data: questionsObject[];
}

export interface QuestionsState {
  data?: questionsObject[];
  isLoading?: boolean;
  error?: string | null;
}

const questionsInitialState: QuestionsState = {
  data: [],
  error: null,
  isLoading: false,
};

export const questionsSlice = createSlice({
  name: "questions",
  initialState: deepClone(questionsInitialState),
  reducers: {
    questionsStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    questionsSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    questionsUpdate: (state, action) => {
      return {
        ...state,
        ...{
          data: {
            list: [...state.data.list, ...action.payload.list],
            hasMany: action.payload.hasMany,
            count: action.payload.count,
            total: action.payload.total,
          },
          error: null,
          isLoading: false,
        },
      };
    },
    questionsFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    tagsReset: () => {
      return deepClone(questionsInitialState);
    },
  },
});

export const {
  questionsStarted,
  questionsSuccess,
  questionsUpdate,
  questionsFail,
  tagsReset,
} = questionsSlice.actions;

export const fetchQuestions =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(questionsStarted());
    try {
      const unApprovedtagsData: QuestionsTagsDataResponse =
        await getQuestionsAPI(queryParams, recaptchaResponse);

      if (unApprovedtagsData) {
        dispatch(questionsSuccess(unApprovedtagsData));
      }
    } catch (e: any) {
      dispatch(questionsFail(e.message));
    }
  };

export const loadMoreQuestions =
  (queryParams: QueryParams, recaptchaResponse: string) =>
  async (dispatch: any) => {
    dispatch(questionsStarted());
    try {
      const questionsData: QuestionsTagsDataResponse = await getQuestionsAPI(
        queryParams,
        recaptchaResponse
      );

      if (questionsData) {
        dispatch(questionsUpdate(questionsData));
      }
    } catch (e: any) {
      dispatch(questionsFail(e.message));
    }
  };
