import {
  getAdminArtistMessageListAPI,
  getArtistUserListAPI,
} from "@redux/services/admin.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

// export interface ArtistListMessageObject {
//     tag: string;
//     label: string;
//     url: string;
//     sourceImage: any;
//     createdAt: string;
//     updatedAt: string;
//     objectId: string;
// }

export interface ArtistListMessageDataResponse {
  data: any;
}

export interface ArtistListMessageSuccessResponse extends SuccessResponse {
  data: any;
}

export interface ArtistListMessageState {
  data?: any;
  isLoading?: boolean;
  error?: string | null;
}

const artistListMessageInitialState: ArtistListMessageState = {
  data: [],
  error: null,
  isLoading: false,
};

export const artistListMessageSlice = createSlice({
  name: "artistListMessage",
  initialState: deepClone(artistListMessageInitialState),
  reducers: {
    ArtistListMessageStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    ArtistListMessageSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    ArtistListMessageFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    ArtistListMessageReset: () => {
      return deepClone(artistListMessageInitialState);
    },
  },
});

export const {
  ArtistListMessageStarted,
  ArtistListMessageSuccess,
  ArtistListMessageFail,
  ArtistListMessageReset,
} = artistListMessageSlice.actions;

export const fetchArtistListMessage = () => async (dispatch: any) => {
  dispatch(ArtistListMessageStarted());
  try {
    const ArtistListMessageData: ArtistListMessageDataResponse =
      await getAdminArtistMessageListAPI();

    if (ArtistListMessageData) {
      dispatch(ArtistListMessageSuccess(ArtistListMessageData));
    }
  } catch (e: any) {
    dispatch(ArtistListMessageFail(e.message));
  }
};

export const fetchUserListMessage = (id: string) => async (dispatch: any) => {
  dispatch(ArtistListMessageStarted());
  try {
    const UserListMessageData: ArtistListMessageDataResponse =
      await getArtistUserListAPI(id);

    if (UserListMessageData) {
      dispatch(ArtistListMessageSuccess(UserListMessageData));
    }
  } catch (e: any) {
    dispatch(ArtistListMessageFail(e.message));
  }
};
