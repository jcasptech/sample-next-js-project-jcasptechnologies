import { getArtistUserListAPI } from "@redux/services/admin.api";
import { QueryParams, SuccessResponse } from "@redux/services/types";
import { createSlice } from "@reduxjs/toolkit";
import { deepClone } from "src/libs/helpers";

export interface UserListMessageObject {
  tag: string;
  label: string;
  url: string;
  sourceImage: any;
  createdAt: string;
  updatedAt: string;
  objectId: string;
}

export interface UserListMessageDataResponse {
  list?: UserListMessageObject[];
  total?: number;
  hasMany?: boolean;
  count?: number;
}

export interface UserListMessageSuccessResponse extends SuccessResponse {
  data: UserListMessageObject[];
}

export interface UserListMessageState {
  data?: UserListMessageObject[];
  isLoading?: boolean;
  error?: string | null;
}

const userListMessageInitialState: UserListMessageState = {
  data: [],
  error: null,
  isLoading: false,
};

export const UserListMessageSlice = createSlice({
  name: "userListMessage",
  initialState: deepClone(userListMessageInitialState),
  reducers: {
    UserListMessageStarted: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    UserListMessageSuccess: (state, action) => {
      return {
        ...state,
        ...{
          data: action.payload,
          error: null,
          isLoading: false,
        },
      };
    },
    UserListMessageFail: (state, action) => {
      return {
        ...state,
        ...{ error: action.payload, isLoading: false },
      };
    },
    UserListMessageReset: () => {
      return deepClone(userListMessageInitialState);
    },
  },
});

export const {
  UserListMessageStarted,
  UserListMessageSuccess,
  UserListMessageFail,
  UserListMessageReset,
} = UserListMessageSlice.actions;

// export const fetchUserListMessage =
//     () =>
//         async (dispatch: any) => {
//             dispatch(UserListMessageStarted());
//             try {
//                 const UserListMessageData: UserListMessageDataResponse = await getArtistUserListAPI();

//                 if (UserListMessageData) {
//                     dispatch(UserListMessageSuccess(UserListMessageData));
//                 }
//             } catch (e: any) {
//                 dispatch(UserListMessageFail(e.message));
//             }
//         };
