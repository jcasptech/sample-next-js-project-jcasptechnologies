import { TagsDataResponse } from "@redux/slices/tags";
import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getTagsAPI = async (
  queryParams: QueryParams,
  recaptchaResponse: string
): Promise<TagsDataResponse> => {
  return fetch({
    url: "/api/tags",
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getUnapprovedAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: "/api/tags/getUnapprovedTags",
    method: "GET",
    params: queryParams,
  });
};

export const postApprovedAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/tags/${id}/approve`,
    method: "POST",
  });
};
