import { QueryParams } from "./types";
import { fetch } from "src/libs/helpers";

export const getPreviousMonthInvoiceData = async (): Promise<any> => {
  return fetch({
    url: `/api/Exports/getPreviousMonthInvoiceData`,
    method: "GET",
  });
};

export const getNextMonthInvoiceData = async (): Promise<any> => {
  return fetch({
    url: `/api/Exports/getMonthInvoiceData`,
    method: "GET",
  });
};

export const getApprovedArtistsData = async (): Promise<any> => {
  return fetch({
    url: `/api/Exports/getApprovedArtistsData`,
    method: "GET",
  });
};

export const getExportUsersData = async (): Promise<any> => {
  return fetch({
    url: `/api/Exports/getUsersData`,
    method: "GET",
  });
};

export const getApprovedVenuesData = async (): Promise<any> => {
  return fetch({
    url: `/api/Exports/getApprovedVenuesData`,
    method: "GET",
  });
};

export const getValidEventsData = async (): Promise<any> => {
  return fetch({
    url: `/api/Exports/getValidEventsData`,
    method: "GET",
  });
};

export const getInvoiceData = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/Invoice/getInvoice`,
    method: "GET",
    params: queryParams,
  });
};

export const getUcomingScheduleAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/Exports/getUcomingSchedule`,
    method: "GET",
    params: queryParams,
  });
};

export const getExportEventDetailsAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/Exports/exportEventDetails`,
    method: "GET",
    params: queryParams,
  });
};

export const getNewsLetterQuestionsData = async (): Promise<any> => {
  return fetch({
    url: `/api/Exports/getNewsLetterQuestionsData`,
    method: "GET",
  });
};
