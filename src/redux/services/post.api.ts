import { CreatePostingsPayload } from "@redux/slices/postings";
import { TEMPORARY_REDIRECT_STATUS } from "next/dist/shared/lib/constants";
import { deepClone } from "src/libs/helpers";
import { fetch } from "src/libs/helpers";

export const postCreateAPI = async (
  data: CreatePostingsPayload
): Promise<any> => {
  const tmpData = deepClone(data);
  let attandance = "";
  if (tmpData.expectedAttandance === "<50") {
    attandance = "min";
  }
  if (tmpData.expectedAttandance === "50-100") {
    attandance = "small";
  }
  if (tmpData.expectedAttandance === "100-200") {
    attandance = "medium";
  }
  if (tmpData.expectedAttandance === "+200") {
    attandance = "max";
  }

  const musicStyles = [];
  for (let i = 0; i < tmpData.musicStyles.length; i++) {
    musicStyles.push(tmpData.musicStyles[i].value.toLowerCase());
  }

  const postData = {
    title: tmpData.title,
    variety: tmpData.variety,
    promotion: tmpData.promotion,
    musicStyles: musicStyles,
    expectedAttandance: attandance,
    playSettings: tmpData.playSettings,
    offerAmount: tmpData.offerAmount,
    note: tmpData.note,
    dates: tmpData.dates,
    start: tmpData.start,
    end: tmpData.end,
    googlePlaceId: tmpData.googlePlaceId,
    venueId: tmpData.venueId,
    equipmentProvided: data.equipmentProvided,
  };

  return fetch({
    url: `/api/posting/create`,
    method: "POST",
    data: postData,
  });
};
