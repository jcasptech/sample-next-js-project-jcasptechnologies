import { fetch } from "src/libs/helpers";

export const updatePatientAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/patient/update",
    method: "POST",
    data: payload,
  });
};

export const updateProviderAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/provider/update",
    method: "POST",
    data: payload,
  });
};
