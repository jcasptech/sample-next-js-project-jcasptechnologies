import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

//test comment 1
export const updateUserAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/user/update",
    method: "POST",
    data: payload,
  });
};

export const getUserServicesAPI = async (): Promise<any> => {
  return fetch({
    url: "user/service",
    method: "GET",
  });
};

export const uploadFileAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/upload/uploadFile",
    method: "POST",
    data: payload,
  });
};

export const deleteFileAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/upload/deleteFile",
    method: "POST",
    data: payload,
  });
};

// export const updatePatientAPI = async ({ payload }: any): Promise<any> => {
//     return fetch({
//         url: "/patient/profile",
//         method: "PATCH",
//         data: payload,
//     });
// };

// export const updateProviderAPI = async ({ payload }: any): Promise<any> => {
//     return fetch({
//         url: "/provider/profile",
//         method: "PATCH",
//         data: payload,
//     });
// };

export const feedbackAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/user/feedback",
    method: "POST",
    data: payload,
  });
};

export const rateReviewAPI = async ({ payload, id }: any): Promise<any> => {
  return fetch({
    url: `user/appointment/${id}/addReview`,
    method: "POST",
    data: payload,
  });
};

export const getDashboardData = async (): Promise<any> => {
  return fetch({
    url: `user/dashboard/totalCounts`,
  });
};

export const getAvailabilityCount = async (): Promise<any> => {
  return fetch({
    url: `user/dashboard/getAvailabilityCount`,
  });
};

export const getVenuesSearchAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: "api/general/search/venues",
    method: "GET",
    params: queryParams,
  });
};

export const checkValidAPI = async (queryParams: QueryParams): Promise<any> => {
  return fetch({
    url: "/api/general/chackValid",
    method: "GET",
    params: queryParams,
  });
};

export const aboutUsAPI = async (recaptchaResponse: string): Promise<any> => {
  return fetch({
    url: "/api/general/about-us",
    method: "GET",
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const generalSearchArtistsAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: "/api/general/search/artists",
    method: "GET",
    params: queryParams,
  });
};

export const downloadAPPSMSAPI = async (
  payload: any,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: "/api/general/downloadAPPSMS",
    method: "POST",
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
    data: payload,
  });
};
