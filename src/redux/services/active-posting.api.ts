import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getActivePostingAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/posting/active-posting`,
    method: "GET",
    params: queryParams,
  });
};

export const getPostBidsAPI = async (
  id: string,
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/posting/${id}/posting-bids`,
    method: "GET",
    params: queryParams,
  });
};

export const postConfirmPostAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/posting/${id}/confirmPosting`,
    method: "POST",
  });
};

export const getConfirmPostingAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/posting/confirmed-posting`,
    method: "GET",
    params: queryParams,
  });
};
