import { fetch } from "src/libs/helpers";

export const createRoomAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/twilio/createRoom",
    method: "POST",
    data: payload,
  });
};

export const addLogAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/twilio/addLog",
    method: "POST",
    data: payload,
  });
};
