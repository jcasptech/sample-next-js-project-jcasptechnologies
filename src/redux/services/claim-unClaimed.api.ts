import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getClaimUnclaimedArtistAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/ClaimedArtist/claimed-artists`,
    method: "GET",
    params: queryParams,
  });
};

export const createClaimedArtistAPI = async (data: any): Promise<any> => {
  return fetch({
    url: `/api/ClaimedArtist/create`,
    method: "POST",
    data: data,
  });
};
