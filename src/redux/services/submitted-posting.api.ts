import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getSubmittedPostingAPI = async (
  artistId: string,
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/posting/${artistId}/artist-submitted-posting`,
    method: "GET",
    params: queryParams,
  });
};

export const getConfirmedEventsAPI = async (
  artistId: string,
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/event/${artistId}/artist-confirmed-events`,
    method: "GET",
    params: queryParams,
  });
};

export const withdrawSubmissionAPI = async (
  artistId: string,
  postingId: string
): Promise<any> => {
  return fetch({
    url: `/api/posting/withdraw-submission`,
    method: "POST",
    data: {
      artistId,
      postingId,
    },
  });
};
