import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getFaqAPI = async (queryParams: QueryParams): Promise<any> => {
  return fetch({
    url: "/user/faq",
    method: "GET",
    params: queryParams,
  });
};
