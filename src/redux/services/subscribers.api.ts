import { fetch } from "src/libs/helpers";

export const addSubscriberQuestionsAPI = async (
  payload: any,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: "/api/subscribers/addSubscriberQuestions",
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};
