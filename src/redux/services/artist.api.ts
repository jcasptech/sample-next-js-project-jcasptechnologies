import { UpdateArtistPayload } from "@redux/slices/artists";
import { data } from "jquery";
import { deepClone, fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getArtistAPI = async (
  queryParams: QueryParams,
  tags: string[],
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: "/api/artist",
    method: "GET",
    params: { ...queryParams, tags: tags },
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getArtistRatingAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: "/api/artist/latestFive",
    method: "GET",
    params: queryParams,
  });
};

export const ArtistSignUpAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/artist/signup`,
    method: "POST",
    data: payload,
  });
};

export const getBrowseArtistAPI = async (
  queryParams: QueryParams,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: "/api/artist/browseArtist",
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getMusicSceneAPI = async (
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: "/api/musicscene",
    method: "GET",
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getArtistDetailUpdateAPI = async (
  id: any,
  data: any
): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/update`,
    method: "POST",
    data: data,
  });
};

export const getArtistVanityAPI = async (
  artistVanity: any,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/artist/${artistVanity}/details`,
    method: "GET",
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getArtistUpcomingShowAPI = async (
  id: any,
  queryParams: QueryParams,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/upcomingShows`,
    method: "GET",
    data: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getArtistAllShowAPI = async (
  id: any,
  queryParams: QueryParams,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/allShows`,
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getartistRatingAPI = async (
  id: any,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/rateList`,
    method: "GET",
    data: id,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const addArtistSubscribeAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/subscribe`,
    method: "POST",
  });
};

export const artistUnsubscribeAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/unsubscribe`,
    method: "POST",
  });
};

export const artistMessageAPI = async (
  id: string,
  payload: any
): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/message`,
    method: "POST",
    data: payload,
  });
};

export const artistWriteRatingAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/artist/write-rate-review`,
    method: "POST",
    data: payload,
  });
};

export const getArtistDetailAPI = async (id: any): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/fullDetails`,
    method: "GET",
    data: id,
  });
};

export const getUserArtistAPI = async (artistIds: any[]): Promise<any> => {
  return fetch({
    url: `/api/artist/user-atists`,
    method: "GET",
    params: { artistIds },
  });
};

export const getArtistMessageAPI = async (
  id: string,
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/getMessages`,
    method: "GET",
    params: queryParams,
  });
};

export const postArtistMessageApproveRejectAPI = async (
  data: any
): Promise<any> => {
  return fetch({
    url: `/api/Messages/approve-reject-messages`,
    method: "POST",
    data: data,
  });
};

export const postFileAPI = async (data: any): Promise<any> => {
  return fetch({
    url: `/api/files`,
    method: "POST",
    data: data,
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

export const deleteFileAPI = async (data: any): Promise<any> => {
  return fetch({
    url: `/api/files/delete`,
    method: "DELETE",
    data: data,
  });
};

export const postArtistUpdateAPI = async (
  id: string,
  data: UpdateArtistPayload
): Promise<any> => {
  const tmpData = deepClone(data);
  const tmpPhotos: any = [];
  const tmpAudios: any = [];
  const tmpVideoLinks: any = [];
  data.photos.map((photo) => {
    tmpPhotos.push(photo.url);
  });
  data.audioFiles.map((audio) => {
    tmpAudios.push(audio.url);
  });
  data.youtubeVideoLinks.map((links) => {
    tmpVideoLinks.push(links.link);
  });

  tmpData.photos = tmpPhotos;
  tmpData.audioFiles = tmpAudios;
  tmpData.youtubeVideoLinks = tmpVideoLinks;

  return fetch({
    url: `/api/artist/${id}/update`,
    method: "POST",
    data: tmpData,
  });
};

export const createArtistAPI = async (
  data: UpdateArtistPayload
): Promise<any> => {
  const tmpData = deepClone(data);
  const tmpPhotos: any = [];
  const tmpAudios: any = [];
  const tmpVideoLinks: any = [];
  data.photos.map((photo) => {
    tmpPhotos.push(photo.url);
  });
  data.audioFiles.map((audio) => {
    tmpAudios.push(audio.url);
  });
  data.youtubeVideoLinks.map((links) => {
    tmpVideoLinks.push(links.link);
  });

  tmpData.photos = tmpPhotos;
  tmpData.audioFiles = tmpAudios;
  tmpData.youtubeVideoLinks = tmpVideoLinks;

  return fetch({
    url: `/api/artist/create`,
    method: "POST",
    data: tmpData,
  });
};

export const postUploadCSVFileAPI = async (data: any): Promise<any> => {
  return fetch({
    url: `/api/artist/uploadSongs`,
    method: "POST",
    data: data,
  });
};

export const removeArtistAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/removeArtist`,
    method: "POST",
  });
};

export const deactivateArtistAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/deactivate-artist`,
    method: "POST",
  });
};

export const activateArtistAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/activate-artist`,
    method: "POST",
  });
};

export const deleteSongsAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/artist/${id}/deleteSongs`,
    method: "DELETE",
  });
};

export const artistClaimAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/ClaimedArtist/claim`,
    method: "POST",
    data: payload,
  });
};

export const getFeaturedArtistAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/sortPriority/admin/getArtists`,
    method: "GET",
    params: queryParams,
  });
};

export const updateFeaturedArtistAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/sortPriority/admin/updateArtistScore`,
    method: "POST",
    data: payload,
  });
};

export const generatesScoreFeaturedArtistAPI = async (): Promise<any> => {
  return fetch({
    url: `/api/sortPriority/admin/regenerateArtistPriority`,
    method: "POST",
  });
};
