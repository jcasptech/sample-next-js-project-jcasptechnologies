import { QueryParams } from "./types";
import { fetch } from "src/libs/helpers";

export const getCalendarAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  const calendar = queryParams?.calendarQuery;
  return fetch({
    url: `/api/calendar/getData`,
    method: "GET",
    params: calendar,
  });
};

export const getEventFullDetailAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/event/${id}/full-details`,
    method: "GET",
  });
};

export const getNoteFullDetailAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/BookingNote/${id}/get-fullDetails`,
    method: "GET",
  });
};

export const sendExpeditedPayment = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/event/expedited-payment`,
    method: "POST",
    data: {
      eventId: id,
    },
  });
};

export const createNote = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/BookingNote/create-booking`,
    method: "POST",
    data: payload,
  });
};

export const deleteNotes = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/BookingNote/${id}/delete`,
    method: "DELETE",
  });
};

export const getPostingFullDetailAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/posting/${id}/full-details`,
    method: "GET",
  });
};

export const submitGigAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/posting/submit-gig`,
    method: "POST",
    data: payload,
  });
};

export const getShowFullDetailAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/shows/${id}/full-details`,
    method: "GET",
  });
};

export const updateShowAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/adminShows/update/show`,
    method: "POST",
    data: payload,
  });
};

export const updateCalendarEventAPI = async (
  id: string | null,
  payload: any
): Promise<any> => {
  return fetch({
    url: `/api/general/${id}/updateCalendarEvent`,
    method: "POST",
    data: payload,
  });
};

export const deleteShowAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/ArtistShows/delete-show`,
    method: "DELETE",
    data: payload,
  });
};

export const getAdminCalendarAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  const calendar = queryParams?.calendarQuery;
  return fetch({
    url: `/api/calendar/admin/getData`,
    method: "GET",
    params: calendar,
  });
};

export const updateAdminCalendarEventAPI = async (
  id: string | null,
  payload: any
): Promise<any> => {
  return fetch({
    url: `/api/general/${id}/adminUpadte`,
    method: "POST",
    data: payload,
  });
};

export const updateAdminCalendarPostAPI = async (
  id: string | null,
  payload: any
): Promise<any> => {
  return fetch({
    url: `/api/general/admin/${id}/update`,
    method: "POST",
    data: payload,
  });
};

export const createProEventAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/general/createProEvent`,
    method: "POST",
    data: payload,
  });
};

export const cancelEventAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/event/${id}/cancel`,
    method: "POST",
  });
};

export const promotePostingAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/general/promotePosting`,
    method: "POST",
    data: payload,
  });
};

export const getEventBookingNoteAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/BookingNote/${id}/get-event-booking-notes`,
    method: "GET",
  });
};

export const createEventBookingNoteAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/BookingNote/create-event-booking-note`,
    method: "POST",
    data: payload,
  });
};

export const getVenueCalendarAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  const calendar = queryParams?.calendarQuery;
  return fetch({
    url: `/api/calendar/getVenueData`,
    method: "GET",
    params: calendar,
  });
};

export const getMonthlySummaryAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/calendar/getMonthlySummary`,
    method: "POST",
    data: payload,
  });
};
