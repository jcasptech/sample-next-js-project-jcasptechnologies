export interface CalendarQueryParams {
  startDate: string;
  endDate: string;
  artistId?: string;
  orderBy?: string;
  shows?: boolean;
  notes?: boolean;
  upcoming?: boolean;
  completed?: boolean;
  paid?: boolean;
  unpaid?: boolean;
  open?: boolean;
  submitted?: boolean;
  nearMe?: boolean;
  all?: boolean;
  latitude?: number;
  longitude?: number;
}

export interface QueryParams {
  take?: number;
  skip?: number;
  include?: string[];
  orderBy?: string;
  orderByColumn?: string;
  tags?: string;
  shows?: string;
  range?: string;
  venueId?: string;
  search?: string;
  email?: string;
  id?: string;
  type?: string;
  content?: string;
  calendarQuery?: CalendarQueryParams;
}

export interface SuccessResponse {
  count: number;
  total: number;
  hasMany: boolean;
}

export interface FileObject {
  name: string;
  url: string;
  __type: string;
}

export interface PostingParseObject {
  className: string;
  objectId: string;
  __type: string;
}

export interface PostingObject {
  equipmentProvided: boolean;
  selectedArtist?: any;
  address: string;
  location: any;
  endDate: any;
  note: string;
  updatedAt: string;
  timeZone: string;
  serviceCharge: string;
  startDate: any;
  variety: string;
  attendanceCategory: string;
  genres: string[];
  createdAt: string;
  city: string;
  totalCost: number;
  artistProceeds: number;
  artistFormats: string;
  serviceFee: number;
  status: string;
  state: string;
  playSetting: string;
  title: string;
  normalizedTitle: string;
  zipcode: number;
  posterImage: any;
  iconImage: any;
  submittedArtists: any;
  venue: any;
  objectId: string;
  bidCount: number;
}
