import { EmbedcodeDataResponse } from "@redux/slices/embedCode";
import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getEmbedCodeAPI = async (
  queryParams: QueryParams
): Promise<EmbedcodeDataResponse> => {
  return fetch({
    url: `/api/EmbedCode/get-paginated`,
    method: "GET",
    params: queryParams,
  });
};

export const deleteEmbedCodeAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/EmbedCode/${id}/delete-embedCode`,
    method: "DELETE",
  });
};

export const createEmbedCodeAPI = async (data: any): Promise<any> => {
  return fetch({
    url: `/api/EmbedCode/create`,
    method: "POST",
    data: data,
  });
};
