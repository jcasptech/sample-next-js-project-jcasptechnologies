import { fetch } from "src/libs/helpers";

export const getLatestFiveReviewAPI = async (): Promise<any> => {
  return await fetch({
    url: `/api/artist/top5Reviews`,
    method: "GET",
  });
};
