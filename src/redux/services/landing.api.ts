import { fetch } from "src/libs/helpers";

//test comment 1
export const landingPagerequestAPI = async (
  payload: any,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: "/api/LandingPage/submit-request",
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};
