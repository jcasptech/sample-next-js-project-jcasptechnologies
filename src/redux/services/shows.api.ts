import { ShowsDataResponse } from "@redux/slices/shows";
import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getShowsAPI = async (
  queryParams: QueryParams,
  recaptchaResponse: string
): Promise<ShowsDataResponse> => {
  return fetch({
    url: "/api/shows",
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getUpcomingAPI = async (
  queryParams: QueryParams,
  id: string,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/venue/${id}/upcomingEvents`,
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getAdminShowsAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/adminShows/admin/upcoming-shows`,
    method: "GET",
    params: queryParams,
  });
};

export const getArtistShowsAPI = async (
  id: string,
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/ArtistShows/artist/${id}/upcoming-shows`,
    method: "GET",
    params: queryParams,
  });
};

export const createAdminShowsAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/adminShows/create-show`,
    method: "POST",
    data: payload,
  });
};

export const createArtistShowsAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/artistShows/create-show`,
    method: "POST",
    data: payload,
  });
};

export const deleteShowsAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/shows/${id}/delete`,
    method: "DELETE",
  });
};
