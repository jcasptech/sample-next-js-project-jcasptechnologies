import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getVenuesAPI = async (
  queryParams: QueryParams,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: "/api/venue",
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getVenueDetailAPI = async (
  vanity: string,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/venue/${vanity}/details`,
    method: "GET",
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const venueSubscribeAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/venue/${id}/subscribe`,
    method: "POST",
  });
};

export const venueUnsubscribeAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/venue/${id}/unsubscribe`,
    method: "POST",
  });
};

export const getVenueFullDetailAPI = async (vanity: string): Promise<any> => {
  return fetch({
    url: `/api/venue/${vanity}/fullDetails`,
    method: "GET",
  });
};

export const getVenueAPI = async (
  queryParams: QueryParams,
  id: string,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/venue/${id}`,
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getUpcomingVenueAPI = async (
  queryParams: QueryParams,
  id: string,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/venue/${id}/upcomingEvents`,
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getAdminVenueAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/venue/admin-venue`,
    method: "GET",
    params: queryParams,
  });
};

export const getVenueFullDetailsAPI = async (
  id: string,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/venue/${id}`,
    method: "GET",
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const updateVenueDetailsAPI = async (
  id: string,
  data: string
): Promise<any> => {
  return fetch({
    url: `/api/venue/${id}/edit`,
    method: "POST",
    data: data,
  });
};

export const getVenueAdminVenueAPI = async (
  id: string,
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/venueAdmin/${id}/getVenues`,
    method: "GET",
    params: queryParams,
  });
};

export const getVenueUserListAPI = async (): Promise<any> => {
  return fetch({
    url: `/api/venueAdmin/getVenueAdmin`,
    method: "GET",
  });
};

export const getFeaturedVenueAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/sortPriority/admin/getVenues`,
    method: "GET",
    params: queryParams,
  });
};

export const updateFeaturedVenueAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/sortPriority/admin/updateVenueScore`,
    method: "POST",
    data: payload,
  });
};

export const generatesScoreFeaturedVenueAPI = async (): Promise<any> => {
  return fetch({
    url: `/api/sortPriority/admin/regenerateVenuePriority`,
    method: "POST",
  });
};
