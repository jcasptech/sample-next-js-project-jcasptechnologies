import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getOpenPostingAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/posting/open-posting`,
    method: "GET",
    params: queryParams,
  });
};
