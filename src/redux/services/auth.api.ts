import {
  LoginPayload,
  LoginSuccessResponse,
  ChangePasswordPayload,
  ForgotPasswordPayload,
  ResetPasswordPayload,
  LoginDataResponse,
  FacebookLoginPayload,
  FacebookLinkPayload,
} from "@redux/slices/auth";
import { fetch } from "src/libs/helpers";
import { ChangePasswordFormInputs } from "src/schemas/changePasswordFormSchema";
import { QueryParams } from "./types";

export const loginAPI = async (
  payload: LoginPayload,
  recaptchaResponse: string
): Promise<LoginSuccessResponse> => {
  return fetch({
    url: "/api/authentication/signin",
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const facebookLoginAPI = async (
  payload: FacebookLoginPayload,
  recaptchaResponse: string
): Promise<LoginSuccessResponse> => {
  return fetch({
    url: "/api/authentication/facebook-signin-signup",
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const getLoginUser = async ({
  Authorization,
}: any): Promise<LoginDataResponse> => {
  return fetch({
    url: `/api/authentication/whoAmI`,
    headers: { Authorization },
  });
};

export const whoAmI = async (): Promise<LoginDataResponse> => {
  return fetch({
    url: `/api/authentication/whoAmI`,
  });
};

export const logoutAPI = async (): Promise<any> => {
  return fetch({
    url: `/api/authentication/logout`,
    method: "DELETE",
  });
};

export const changePasswordAPI = async (
  payload: ChangePasswordPayload
): Promise<any> => {
  return fetch({
    url: `/auth/changePassword`,
    method: "PATCH",
    data: payload,
  });
};

export const forgotPasswordAPI = async (
  payload: ForgotPasswordPayload,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/authentication/resetPassword/request`,
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const otpVerificationAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: "/auth/loginVerifyOtp",
    method: "POST",
    data: payload,
  });
};

export const forgotOtpVerificationAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: "/auth/forgotPasswordVerifyOtp",
    method: "POST",
    data: payload,
  });
};

export const resendAuthOtpAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: "/auth/loginResendOtp",
    method: "POST",
    data: payload,
  });
};

export const resendOtpAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: "/api/artist",
    method: "GET",
    data: payload,
  });
};

export const resetPasswordAPI = async (
  payload: ResetPasswordPayload,
  recaptchaResponse: string
): Promise<ResetPasswordPayload> => {
  return fetch({
    url: "/api/authentication/resetPassword/reset",
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const verifyOtpAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `user/verifyOtp`,
    method: "POST",
    data: payload,
  });
};
export const resendUserOtpAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/user/resendOtp`,
    method: "POST",
    data: payload,
  });
};
export const signUpAPI = async (
  payload: any,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/authentication/signup`,
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};
export const requestFeatureAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/authentication/request-feature`,
    method: "POST",
    data: payload,
  });
};

export const updateProfileAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/auth/profile`,
    method: "PATCH",
    data: payload,
  });
};

export const loginAsAdminAPI = async (sessionId: any): Promise<any> => {
  return fetch({
    url: `/auth/loginFromAdmin/${sessionId}`,
    method: "GET",
  });
};

export const popupShowedAPI = async (): Promise<any> => {
  return fetch({
    url: `/auth/popupShowed`,
    method: "PATCH",
  });
};

export const facebookLinkAPI = async (
  payload: FacebookLinkPayload
): Promise<LoginSuccessResponse> => {
  return fetch({
    url: "/api/authentication/link-facebook",
    method: "POST",
    data: payload,
  });
};

export const updatePasswordAPI = async (
  payload: ChangePasswordFormInputs
): Promise<any> => {
  return fetch({
    url: `/api/authentication/update-password`,
    method: "POST",
    data: payload,
  });
};

export const postUpdateUserAPI = async (payload: any): Promise<any> => {
  return fetch({
    url: `/api/authentication/update-user`,
    method: "POST",
    data: payload,
  });
};

export const postContactUsAPI = async (
  payload: any,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/contactUs`,
    method: "POST",
    data: payload,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

export const convertToArtistAPI = async (): Promise<any> => {
  return fetch({
    url: `/api/authentication/convertToArtist`,
    method: "POST",
  });
};

export const getArtistFollowsAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/authentication/getArtistFollows`,
    method: "GET",
    params: queryParams,
  });
};

export const getVenueFollowsAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/authentication/getVenueFollows`,
    method: "GET",
    params: queryParams,
  });
};
