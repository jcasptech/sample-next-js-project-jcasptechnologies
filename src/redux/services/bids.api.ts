import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export interface bidPayload {
  bidId: string;
  isRecommended?: boolean;
  isHide?: boolean;
  isReviewed?: boolean;
  isRecommendedChanged?: boolean;
}

export const getBidsAPI = async (
  id: string,
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/posting/${id}/posting-bids`,
    method: "GET",
    params: queryParams,
  });
};

export const postConfirmPostAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/posting/${id}/confirmPosting`,
    method: "POST",
  });
};

export const bidActionsAPI = async (bidPayload: bidPayload): Promise<any> => {
  return fetch({
    url: `/api/posting/bid-hide-review`,
    method: "POST",
    data: bidPayload,
  });
};
