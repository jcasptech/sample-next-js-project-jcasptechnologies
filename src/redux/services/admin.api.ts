import { deepClone, fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getAdminMessageAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: `/api/Messages`,
    method: "GET",
    params: queryParams,
  });
};

export const getQuestionsAPI = async (
  queryParams: QueryParams,
  recaptchaResponse: string
): Promise<any> => {
  return fetch({
    url: `/api/subscribers/questions-list`,
    method: "GET",
    params: queryParams,
    headers: {
      "x-recaptcha-token": recaptchaResponse,
    },
  });
};

/// new messages API
export const getAdminArtistMessageListAPI = async (): Promise<any> => {
  return fetch({
    url: `/api/Messages/adminMessageList`,
    method: "GET",
  });
};

export const getAdminArtistMessageAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/Messages/admin/${id}/getMessages`,
    method: "GET",
  });
};
export const approveDeclineMessageAPI = async (data: any): Promise<any> => {
  const tmpData = deepClone(data);
  const newData = {
    messageId: tmpData.messageId,
    status: tmpData.status,
  };
  return fetch({
    url: `/api/Messages/approve-reject-messages`,
    method: "POST",
    data: newData,
  });
};

export const getArtistUserListAPI = async (id: string): Promise<any> => {
  return fetch({
    url: `/api/Messages/${id}/messageList`, //api/Messages/artistId/getMessages?userId=456
    method: "GET",
  });
};

export const getArtistUserMessageAPI = async (
  id: string,
  selectedId: string
): Promise<any> => {
  return fetch({
    url: `/api/Messages/${id}/getMessages?source=${selectedId}`,
    method: "GET",
  });
};
