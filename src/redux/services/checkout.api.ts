import { fetch } from "src/libs/helpers";

export const getPricingPaymentsAPI = async (data: any): Promise<any> => {
  return await fetch({
    url: `/api/payments/checkout`,
    method: "POST",
    data: data,
  });
};

export const getPricingPaymentsPlansAPI = async (): Promise<any> => {
  return await fetch({
    url: `/api/payments/plans`,
    method: "GET",
  });
};

export const getOrderDetailAPI = async (session_id: string): Promise<any> => {
  return await fetch({
    url: `/api/payments/${session_id}/details`,
    method: "GET",
  });
};
