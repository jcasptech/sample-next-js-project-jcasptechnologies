import { fetch } from "src/libs/helpers";
import { QueryParams } from "./types";

export const getPaymentMethodAPI = async (
  queryParams: QueryParams
): Promise<any> => {
  return fetch({
    url: "/patient/payment-method",
    method: "GET",
    params: queryParams,
  });
};

export const getAllPaymentMethodAPI = async (): Promise<any> => {
  return fetch({
    url: "/patient/payment-method",
    method: "GET",
  });
};

export const addPaymentMethodAPI = async ({ payload }: any): Promise<any> => {
  return fetch({
    url: "/patient/payment-method",
    method: "POST",
    data: payload,
  });
};

export const deletePaymentMethodAPI = async ({
  paymentMethodId,
}: any): Promise<any> => {
  return fetch({
    url: `/patient/payment-method/${paymentMethodId}`,
    method: "DELETE",
  });
};
