import { combineReducers } from "redux";

import { loginSlice, loginUserSlice } from "./slices/auth";
import { faqSlice } from "./slices/faq";
import { artistSlice } from "./slices/artists";
import { ShowsSlice } from "./slices/shows";
import { venueSlice } from "./slices/venueDetails";
import { tagsSlice } from "./slices/tags";
import { upcomingEventSlice } from "./slices/shows/upcomingEvent";
import { generalSlice } from "./slices/general";
import { artistRatingSlice } from "./slices/artists-rating";
import { browseArtistSlice } from "./slices/browseArtist/browseArtist";
import { musicSceneSlice } from "./slices/music-scene/musicScene";
import { artistShowsSlice } from "./slices/artistShow";
import { artistDetailsRatingSlice } from "./slices/artistRating";
import { venuesSlice } from "./slices/venues";
import { upcomingVenuesSlice } from "./slices/upComingVenues";
import { selectIdSlice } from "./slices/UserId";
import { selectedArtistSlice } from "./slices/selectedArtist";
import { messageSlice } from "./slices/artistMessage";
import { artistListSlice } from "./slices/artistList";
import { calendarSlice } from "./slices/calendar";
import { UserShowsSlice } from "./slices/userShows";
import { activePostingSlice } from "./slices/activePosting";
import { activePostingBidsSlice } from "./slices/activePostingBids";
import { confirmPostingSlice } from "./slices/confirmPosting";
import { embedCodeSlice } from "./slices/embedCode";
import { claimArtistSlice } from "./slices/claimArtist";
import { unapporvedtagsSlice } from "./slices/usApprovedTags";
import { questionsSlice } from "./slices/Questions";
import { adminVenuesSlice } from "./slices/adminVenues";
import { artistListMessageSlice } from "./slices/messages/artistList";
import { venueUserListSlice } from "./slices/venueUserList";
import { openPostingSlice } from "./slices/openPosting";
import { permissionsReducer as permissions } from "react-redux-permission";
import { submittedPostingSlice } from "./slices/submittedPosting";
import { confirmedEventSlice } from "./slices/confirmedEvent";
import { bidsSlice } from "./slices/bids";
import { featuredArtistSlice } from "./slices/featuredArtist/featuredArtist";
import { featuredVenueSlice } from "./slices/featuredVenue/featuredVenue";

const rootReducer = combineReducers({
  login: loginSlice.reducer,
  loginUser: loginUserSlice.reducer,
  faq: faqSlice.reducer,
  artist: artistSlice.reducer,
  artistRating: artistRatingSlice.reducer,
  shows: ShowsSlice.reducer,
  venueDetails: venueSlice.reducer,
  tags: tagsSlice.reducer,
  upcomingEvent: upcomingEventSlice.reducer,
  general: generalSlice.reducer,
  browseArtist: browseArtistSlice.reducer,
  musicScene: musicSceneSlice.reducer,
  artistShows: artistShowsSlice.reducer,
  artistDetailsRanting: artistDetailsRatingSlice.reducer,
  venues: venuesSlice.reducer,
  upcomingVenues: upcomingVenuesSlice.reducer,
  selectId: selectIdSlice.reducer,
  selectedArtist: selectedArtistSlice.reducer,
  messages: messageSlice.reducer,
  artistList: artistListSlice.reducer,
  calendarData: calendarSlice.reducer,
  userShows: UserShowsSlice.reducer,
  activePosting: activePostingSlice.reducer,
  confirmPosting: confirmPostingSlice.reducer,
  activePostingBids: activePostingBidsSlice.reducer,
  embedCode: embedCodeSlice.reducer,
  claimArtist: claimArtistSlice.reducer,
  unApprovedTags: unapporvedtagsSlice.reducer,
  questions: questionsSlice.reducer,
  adminVenues: adminVenuesSlice.reducer,
  artistListMessage: artistListMessageSlice.reducer,
  venueUserList: venueUserListSlice.reducer,
  openPosting: openPostingSlice.reducer,
  submittedPosting: submittedPostingSlice.reducer,
  confirmedEvent: confirmedEventSlice.reducer,
  bids: bidsSlice.reducer,
  featuredArtist: featuredArtistSlice.reducer,
  featuredVenue: featuredVenueSlice.reducer,
  permissions,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
