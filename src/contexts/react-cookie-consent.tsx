import React from "react";
import CookieConsent from "react-cookie-consent";

const MyCookieComponent = () => {
  const handleDecline = () => {
    console.log("Cookie Declined");
  };

  return (
    <div>
      <CookieConsent location="bottom" cookieName="myCookie">
        <p>
          We use cookies to ensure that we give you the best experience on our
          website. If you continue to use this site we will assume that you are
          happy with it.<a href="#">Find out more</a>
        </p>
      </CookieConsent>
    </div>
  );
};

export default MyCookieComponent;
