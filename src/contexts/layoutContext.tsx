/* eslint-disable react-hooks/exhaustive-deps */
import { showToast } from "@components/ToastContainer";
import { loginReset } from "@redux/actions";
import { RootState } from "@redux/reducers";
import { selectIdReset } from "@redux/slices/artistId";
import { ArtistListReset } from "@redux/slices/artistList";
import {
  LoginUserState,
  loginUserUpdate,
  logoutUser,
} from "@redux/slices/auth";
import { setGeneralTheme } from "@redux/slices/general";
import { AxiosInstance } from "axios";
import { useRouter } from "next/router";
import { createContext, useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useDispatch, useSelector } from "react-redux";
import { API, deepClone } from "src/libs/helpers";
import MyCookieComponent from "./react-cookie-consent";

export interface LayoutContextModel {
  doLogout?: () => void;
}

const initialState: LayoutContextModel = {};

export const LayoutContext = createContext(initialState);

export const LayoutContextProvider = ({ children }: any) => {
  const [cookie, , removeCookie]: any = useCookies(["authApp"]);
  const dispatch = useDispatch();
  const router = useRouter();

  const axiosInstance: AxiosInstance = API();
  const { data: loginUser }: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const [authApp, setAuthApp] = useState<any>(null);

  const doLogout = async (noApiCall = false) => {
    try {
      if (authApp?.session?.id) {
        await dispatch(
          logoutUser({
            noApiCall,
            sessionId: authApp.session.id,
          })
        );
      }
    } catch (e) {
      console.log(e);
    }

    if (noApiCall) {
      const tmpD = deepClone(loginUser);
      delete tmpD.token;
      dispatch(loginUserUpdate(tmpD));
      dispatch(loginReset());
      // dispatch(loginUserReset());
      dispatch(ArtistListReset());
      dispatch(selectIdReset());
    }
    removeCookie("authApp", { path: "/" });
    router.reload();
  };

  useEffect(() => {
    if (
      authApp &&
      authApp?.accessToken &&
      !(loginUser && loginUser?.user && loginUser?.user?.objectId)
    ) {
      doLogout();
    }
  }, [authApp]);

  useEffect(() => {
    if (cookie?.authApp) {
      setAuthApp(cookie?.authApp);
    }
  }, [cookie?.authApp]);

  useEffect(() => {
    if (router.pathname !== "/artist/[vanity]") {
      setTimeout(() => {
        dispatch(setGeneralTheme(""));
      }, 300);
    }
  }, []);
  // Add a request interceptor
  axiosInstance.interceptors.request.use(
    (c: any) => {
      if (authApp && authApp.accessToken) {
        if (!(c.headers && c.headers["authorization"])) {
          c.headers["authorization"] = `Bearer ${authApp?.accessToken}`;
        }
      }
      return c;
    },
    (error: any) => {
      // Do something with request error
      return Promise.reject(error);
    }
  );

  axiosInstance.interceptors.response.use(
    (response: any) => {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data

      if (response?.data?.responseData) {
        return {
          data: response?.data?.responseData,
        };
      }
      return response;
      // let res = response
      // if (res?.data) {
      //   res = {
      //     data: await CryptoJSAesJson.decrypt(res.data)
      //   }
      // }

      // if (res?.data?.responseData) {
      //   return {
      //     data: res?.data?.responseData,
      //   };
      // }
      // return res;
    },
    (error: any) => {
      if (
        (error && error.response && error.response.status === 401) ||
        error.response.status === 406
      ) {
        doLogout(true);
      }
      if (error?.response?.status >= 400 && error?.response?.status <= 499) {
        showToast(error?.response?.data?.message || error.message, "error");
      }
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject((error.response && error.response.data) || error);
    }
  );

  // Add a request interceptor
  axiosInstance.interceptors.request.use(
    (c: any) => {
      return c;
    },
    (error: any) => {
      // Do something with request error
      return Promise.reject(error);
    }
  );

  axiosInstance.interceptors.response.use(
    (response: any) => {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    },
    (error: any) => {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(
        (error && error.response && error.response.data) || error
      );
    }
  );
  // }, []);

  return (
    <LayoutContext.Provider
      value={{
        doLogout,
      }}
    >
      <MyCookieComponent />
      {children}
    </LayoutContext.Provider>
  );
};
