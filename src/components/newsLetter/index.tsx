import { SendOutlined } from "@ant-design/icons";
import { Button } from "@components/theme";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { addSubscriberQuestionsAPI } from "@redux/services/subscribers.api";
import AOS from "aos";
import "aos/dist/aos.css";
import { useReCaptcha } from "next-recaptcha-v3";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  NewsLatterFormInputs,
  NewsLatterFormValidateSchema,
} from "src/schemas/NewsLatterFormSchema";
import newsLetterStyles from "./newsLetter.module.scss";

export interface newsLetterProps {}

const NewsLetterSection = (props: newsLetterProps) => {
  const { register, handleSubmit, formState, setValue, reset } =
    useForm<NewsLatterFormInputs>({
      resolver: yupResolver(NewsLatterFormValidateSchema),
    });

  const [isLoading, setIsLoading] = useState(false);
  const { executeRecaptcha } = useReCaptcha();

  const onSubmit = async (data: NewsLatterFormInputs) => {
    setIsLoading(true);
    try {
      const recaptchaResponse = await executeRecaptcha("subscribe_website");
      await addSubscriberQuestionsAPI(data, recaptchaResponse);
      showToast(SUCCESS_MESSAGES.answerSubmit, "success");
      reset();
      setIsLoading(false);
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <section className={`section-pad ${newsLetterStyles.section}`}>
        <div className="container-fluid">
          <div className={`row m0 ${newsLetterStyles.tcenter}`}>
            <div
              className={`col-12 col-sm-12 col-lg-6 ${newsLetterStyles.nl_cta_hold}`}
            >
              <form
                className={`${newsLetterStyles.nl_form} mb-3`}
                onSubmit={handleSubmit(onSubmit)}
              >
                <div className={`${newsLetterStyles.nl_form__input_group}`}>
                  <InputField
                    {...{
                      register,
                      formState,
                      id: "userQuestion",
                      className: `form-control ${newsLetterStyles.nl_input}`,
                    }}
                    type="text"
                    placeholder="What is your favourite place to see live music?"
                    showError={false}
                  />
                  <Button
                    htmlType="submit"
                    type="primary"
                    disabled={isLoading || formState?.isSubmitting}
                    loading={isLoading || formState?.isSubmitting}
                  >
                    <SendOutlined className="f-22" />
                  </Button>
                </div>
                {formState &&
                  formState?.errors &&
                  formState?.errors["userQuestion"] &&
                  formState?.errors["userQuestion"].message && (
                    <span className="ant-typography ant-typography-danger text-danger ms-3 block mt-1">
                      {formState.errors["userQuestion"].message}
                    </span>
                  )}
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default NewsLetterSection;
