import { Button } from "@components/theme";
import {
  InputPasswordField,
  InputPasswordFieldWithMessage,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { updatePasswordAPI } from "@redux/services/auth.api";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ChangePasswordFormInputs,
  ChangePasswordFormValidateSchema,
} from "src/schemas/changePasswordFormSchema";

const ChangePassword = () => {
  const [isLoading, setIsLoading] = useState(false);

  const { register, handleSubmit, formState, reset } =
    useForm<ChangePasswordFormInputs>({
      resolver: yupResolver(ChangePasswordFormValidateSchema),
    });

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      await updatePasswordAPI(data);
      showToast(SUCCESS_MESSAGES.resetPassword, "success");
      window.location.reload();
      setIsLoading(false);
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={`formB`}>
      <div className="fieldH ">
        <InputPasswordFieldWithMessage
          {...{
            register,
            formState,
            id: "newPassword",
            label: "New Password",
            className: `input`,
            placeholder: "Enter a new Password",
          }}
        />
      </div>
      <div className="fieldH">
        <InputPasswordField
          {...{
            register,
            formState,
            id: "confirmPassword",
            label: "Confirm Password",
            className: `input`,
            placeholder: "Enter confirm your Password",
          }}
        />
      </div>
      <div className="request-btn text-center">
        <Button
          htmlType="submit"
          type="primary"
          loading={isLoading || formState.isSubmitting}
          disabled={isLoading || formState.isSubmitting}
        >
          Change Password
        </Button>
      </div>
    </form>
  );
};

export default ChangePassword;
