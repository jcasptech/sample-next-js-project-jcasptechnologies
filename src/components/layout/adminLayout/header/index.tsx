import React, { useContext, useState } from "react";
import { LayoutContext, LayoutContextModel } from "src/contexts/layoutContext";
import Link from "next/link";
import { LoginUserState } from "@redux/slices/auth";
import { useSelector } from "react-redux";
import { RootState } from "@redux/reducers";
import { DashboardIcon } from "@components/theme/icons/dashboardIcon";
import { LogoutIcon } from "@components/theme/icons/logoutIcon";
import adminLayoutStyles from "../adminLayoutStyles.module.scss";
import RequestFeatureModal from "@components/layout/requestFeactureModalLayout";
import headerStyles from "./header.module.scss";
import { LOGIN_USER_TYPE } from "src/libs/constants";
import { AccountIcon } from "@components/theme/icons/accountIcon";
import { Divider } from "antd";
import Image from "next/image";

export interface AdminLayoutHeaderProps {
  pathname: string;
  handleToggleCollapse: (d: any) => void;
}
const AdminLayoutHeader = (props: AdminLayoutHeaderProps) => {
  const { pathname, handleToggleCollapse } = props;
  const { doLogout }: LayoutContextModel = useContext(LayoutContext);

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  return (
    <>
      {/* {isrequestModalOpen && (
        <RequestFeatureModal
          {...{
            isOpen: isrequestModalOpen,
            setIsRequestFeatureModalOpen,
          }}
        />
      )} */}
      <nav className="navbar fixed-top1 navbar-expand-lg navbar-light bg-light secondary-menu ">
        <div className="container-fluid mp-0">
          <a
            className="toggle-vmenu"
            onClick={(e) => {
              e.preventDefault();
              handleToggleCollapse(e);
            }}
          >
            <Image
              src="/images/white-m-trigger.svg"
              alt="T"
              width={25}
              height={19}
            />
            {/* <img src="/images/white-m-trigger.svg" alt="T" /> */}
          </a>
          <div className=" ptitle p-0">
            <h3 className="capitalize">
              {" "}
              {pathname
                .replaceAll("/", " ")
                .replaceAll("-", " ")
                .replaceAll("[id]", "") || ""}{" "}
            </h3>
          </div>

          <div
            className="collapse1 navbar-collapse justify-content-end"
            id="navbarNav"
          >
            <ul className="navbar-nav">
              <li className="nav-item account-dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <Image
                    src={
                      loginUserState.data?.user?.avatarImage?.url ||
                      "/images/artist-placeholder.png"
                    }
                    alt={loginUserState.data?.user?.name}
                    className="ppic"
                    height={50}
                    width={50}
                  />
                  {/* <img
                    src={
                      loginUserState.data?.user?.avatarImage?.url ||
                      "/images/artist-placeholder.png"
                    }
                    alt={loginUserState.data?.user?.name}
                    className="ppic"
                  /> */}
                  {loginUserState.data?.user?.name}
                </a>
                <ul
                  className={`dropdown-menu ${adminLayoutStyles.dMenu}`}
                  aria-labelledby="navbarDropdown"
                >
                  <li>
                    <Link className="dropdown-item" href="/dashboard">
                      <DashboardIcon />
                      My Dashboard
                    </Link>
                  </li>

                  <li>
                    <Link className="dropdown-item" href="/my-account">
                      <AccountIcon />
                      My Account
                    </Link>
                  </li>

                  <li>
                    <Divider className="mt-2 mb-2" />
                    <Link className="dropdown-item" href="#" onClick={doLogout}>
                      <LogoutIcon />
                      Logout
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};
export default AdminLayoutHeader;
