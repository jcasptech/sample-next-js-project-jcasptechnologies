import { RootState } from "@redux/reducers";
import { fetchArtistList, SeledtedArtistState } from "@redux/slices/artistList";
import { LoginUserState } from "@redux/slices/auth";
import { fetchSelectedArtist } from "@redux/slices/selectedArtist";
import { selectIdSuccess } from "@redux/slices/UserId";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { LOGIN_USER_TYPE } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import adminLayoutStyles from "../adminLayoutStyles.module.scss";
import RequestFeatureModal from "@components/layout/requestFeactureModalLayout";
import Image from "next/image";
import { ArrowDownIcon } from "@components/theme/icons/arrowDownIcon";
import { ArrowUpIcon } from "@components/theme/icons/arrowUpIcon";

export interface AdminLayoutSideBarProps {
  isCollapse: boolean;
  setIsCollapse: (d: any) => void;
}
const AdminLayoutSideBar = (props: AdminLayoutSideBarProps) => {
  const { isCollapse, setIsCollapse } = props;
  const dispatch = useDispatch();
  const router = useRouter();

  const [subscriptionSubMenuExpanded, setSubscriptionSubMenuExpanded] =
    useState(false);
  const [featuredSubMenuExpanded, setFeaturedSubMenuExpanded] = useState(false);
  const [adminGeneralSubMenuExpanded, setAdminGeneralSubMenuExpanded] =
    useState(false);
  const [bookingSubMenuExpanded, setBookingSubMenuExpanded] = useState(false);
  const [gigSubMenuExpanded, setGigsSubMenuExpanded] = useState(false);

  const [isrequestModalOpen, setIsRequestFeatureModalOpen] = useState(false);
  const [windowInnerWidth, setWindowInnerWidth] = useState<any>();

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const selectArtistId: any = useSelector((state: RootState) => state.selectId);

  const [artistDown, setArtistDown] = useState(false);
  const {
    selectedArtist: { data: selectedArtistData },
  }: {
    selectedArtist: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    selectedArtist: state.selectedArtist,
  }));

  const [selectId, setSelectId] = useState("");

  const {
    artistList: { isLoading, data: artistListData },
  }: {
    artistList: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    artistList: state.artistList,
  }));

  useEffect(() => {
    if (artistListData) {
      dispatch(selectIdSuccess(artistListData[0]?.objectId));
    }
  }, [artistListData]);

  useEffect(() => {
    if (selectArtistId?.data) {
      dispatch(fetchSelectedArtist(selectArtistId?.data));
    }
  }, [selectArtistId.data]);

  useEffect(() => {
    if (loginUserState?.data?.user?.artists.length > 0) {
      dispatch(fetchArtistList(loginUserState?.data?.user?.artists));
    }
  }, [loginUserState?.data?.user?.artists]);

  useEffect(() => {
    if (
      router.asPath === "/create-postings" ||
      router.asPath === "/active-posting" ||
      router.asPath === "/confirm-posting" ||
      router.asPath === "/browse-open-postings" ||
      router.asPath === "/artist-submissions" ||
      router.asPath === "/subscribe-artists" ||
      router.asPath === "/subscribe-venues" ||
      router.asPath === "/featured-artists" ||
      router.asPath === "/featured-venues" ||
      router.asPath === "/embed-code" ||
      router.asPath === "/artist-element" ||
      router.asPath === "/user-questions" ||
      router.asPath === "/venue-listing"
    ) {
    } else {
      setSubscriptionSubMenuExpanded(false);
      setBookingSubMenuExpanded(false);
      setGigsSubMenuExpanded(false);
      setFeaturedSubMenuExpanded(false);
      setAdminGeneralSubMenuExpanded(false);
    }

    if (
      (router.asPath === "/create-postings" ||
        router.asPath === "/active-posting" ||
        router.asPath === "/confirm-posting") &&
      !isCollapse
    ) {
      setBookingSubMenuExpanded(true);
    }

    if (
      (router.asPath === "/browse-open-postings" ||
        router.asPath === "/artist-submissions") &&
      !isCollapse
    ) {
      setGigsSubMenuExpanded(true);
    }

    if (
      (router.asPath === "/subscribe-artists" ||
        router.asPath === "/subscribe-venues") &&
      !isCollapse
    ) {
      setSubscriptionSubMenuExpanded(true);
    }

    if (
      (router.asPath === "/featured-artists" ||
        router.asPath === "/featured-venues") &&
      !isCollapse
    ) {
      setFeaturedSubMenuExpanded(true);
    }

    if (
      (router.asPath === "/embed-code" ||
        router.asPath === "/artist-element" ||
        router.asPath === "/user-questions" ||
        router.asPath === "/venue-listing") &&
      !isCollapse
    ) {
      setAdminGeneralSubMenuExpanded(true);
    }
  }, [router]);

  useEffect(() => {
    if (selectId) {
      dispatch(selectIdSuccess(selectId));
    }
  }, [selectId]);

  // useEffect(() => {
  //   if (isCollapse) {
  //     $(".reqN").hide();
  //     setLogoSrc("/images/Logo.png");
  //     setSubMenuExpanded(true);
  //   } else {
  //     $(".reqN").show();
  //     setLogoSrc("/images/panel-logo.png");
  //     setSubMenuExpanded(false);
  //   }
  // }, [isCollapse])

  useEffect(() => {
    if (bookingSubMenuExpanded) {
      setIsCollapse(false);
      setSubscriptionSubMenuExpanded(false);
      setGigsSubMenuExpanded(false);
      setFeaturedSubMenuExpanded(false);
      setAdminGeneralSubMenuExpanded(false);
    }
  }, [bookingSubMenuExpanded]);

  useEffect(() => {
    if (subscriptionSubMenuExpanded) {
      setIsCollapse(false);
      setBookingSubMenuExpanded(false);
      setGigsSubMenuExpanded(false);
      setFeaturedSubMenuExpanded(false);
      setAdminGeneralSubMenuExpanded(false);
    }
  }, [subscriptionSubMenuExpanded]);

  useEffect(() => {
    if (gigSubMenuExpanded) {
      setIsCollapse(false);
      setBookingSubMenuExpanded(false);
      setSubscriptionSubMenuExpanded(false);
      setFeaturedSubMenuExpanded(false);
      setAdminGeneralSubMenuExpanded(false);
    }
  }, [gigSubMenuExpanded]);

  useEffect(() => {
    if (featuredSubMenuExpanded) {
      setIsCollapse(false);
      setBookingSubMenuExpanded(false);
      setSubscriptionSubMenuExpanded(false);
      setGigsSubMenuExpanded(false);
      setAdminGeneralSubMenuExpanded(false);
    }
  }, [featuredSubMenuExpanded]);

  useEffect(() => {
    if (adminGeneralSubMenuExpanded) {
      setIsCollapse(false);
      setBookingSubMenuExpanded(false);
      setSubscriptionSubMenuExpanded(false);
      setGigsSubMenuExpanded(false);
      setFeaturedSubMenuExpanded(false);
    }
  }, [adminGeneralSubMenuExpanded]);

  let divRef: any = useRef(null);

  return (
    <>
      {isrequestModalOpen && (
        <RequestFeatureModal
          {...{
            isOpen: isrequestModalOpen,
            setIsRequestFeatureModalOpen,
          }}
        />
      )}
      <nav
        className={`navbar-primary panel-menu ${isCollapse ? "collapsed" : ""}`}
      >
        <Link href="/" className="navbar-brand panel-logo">
          <Image
            src={isCollapse ? "/images/Logo.png" : "/images/panel-logo.png"}
            alt="JCasp"
            id="mainLogo"
            width={isCollapse ? 71 : 147}
            height={isCollapse ? 60 : 55}
          />
          {/* <img
            src={isCollapse ? "/images/Logo.png" : "/images/panel-logo.png"}
            alt="JCasp"
            id="mainLogo"
          /> */}
        </Link>

        {loginUserState?.data.userType === LOGIN_USER_TYPE.MANAGER &&
          artistListData &&
          artistListData?.length > 0 && (
            <div className="filter_artist float-left w-100" ref={divRef}>
              <div className="art-select">
                {selectedArtistData && (
                  <button
                    className="btn-select w-100"
                    value=""
                    onClick={() => setArtistDown(!artistDown)}
                  >
                    <li>
                      <Image
                        src={
                          selectedArtistData?.iconImage
                            ? selectedArtistData?.iconImage?.url
                            : "/images/artist-placeholder.png"
                        }
                        alt=""
                        height={50}
                        width={50}
                      />
                      <div className="text-white">
                        {selectedArtistData?.name}
                      </div>
                      {artistDown ? (
                        <span className="artistUP"></span>
                      ) : (
                        <span className="artistDown"></span>
                      )}
                    </li>
                  </button>
                )}

                <div className={artistDown ? "b" : "b d-none"}>
                  <ul id="a">
                    {artistListData &&
                      artistListData?.length > 0 &&
                      artistListData?.map((artist: any, index: any) => (
                        <li
                          onClick={() => {
                            setSelectId(artist?.objectId);
                            setArtistDown(!artistDown);
                          }}
                          key={index}
                        >
                          <Image
                            src={
                              artist?.iconImage?.url ||
                              "/images/artist-placeholder.png"
                            }
                            alt=""
                            height={50}
                            width={50}
                          />
                          <div className="text-white">
                            {capitalizeString(artist.name)}
                          </div>
                        </li>
                      ))}
                  </ul>
                </div>
              </div>
            </div>
          )}

        <ul className="navbar-primary-menu">
          {loginUserState?.data?.userType !== LOGIN_USER_TYPE.MANAGER && (
            <li className={`sub-menu ${adminLayoutStyles.other_links}`}>
              <a
                onClick={(e) => {
                  e.preventDefault();
                  setBookingSubMenuExpanded(!bookingSubMenuExpanded);
                }}
              >
                {/* <img src="/images/aric.svg" alt="AR" /> */}
                <Image
                  src="/images/general/artists.png"
                  alt="Hire Artist"
                  width={22}
                  height={26}
                />
                <span className="nav-label">
                  Hire Artist
                  <span
                    className={
                      bookingSubMenuExpanded ? "menu-is-open" : "menu-is-closed"
                    }
                  >
                    <ArrowDownIcon />
                  </span>
                </span>
              </a>
              <ul className={bookingSubMenuExpanded ? "" : "d-none"}>
                <li>
                  <Link
                    href="/create-postings"
                    className={
                      router.asPath === "/create-postings" ? "active" : ""
                    }
                  >
                    {/* <img src="/images/general/create-new.png" alt="AC" />{" "} */}
                    <Image
                      src="/images/general/create-new.png"
                      alt="Create Postings"
                      width={27}
                      height={26}
                    />
                    <span className="nav-label">Create Postings</span>
                  </Link>
                </li>
                <li>
                  <Link
                    href="/active-posting"
                    className={
                      router.asPath === "/active-posting" ? "active" : ""
                    }
                  >
                    {/* <img src="/images/general/list.png" alt="AC" />{" "} */}
                    <Image
                      src="/images/general/list.png"
                      alt="Active Posting"
                      width={27}
                      height={26}
                    />
                    <span className="nav-label">Active Posting</span>
                  </Link>
                </li>
                {loginUserState?.data.userType !== LOGIN_USER_TYPE.USER && (
                  <li>
                    <Link
                      href="/confirm-posting"
                      className={
                        router.asPath === "/confirm-posting" ? "active" : ""
                      }
                    >
                      {/* <img src="/images/general/list.png" alt="AC" />{" "} */}
                      <Image
                        src="/images/general/list.png"
                        alt="Confirm Posting"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Confirm Posting</span>
                    </Link>
                  </li>
                )}
              </ul>
            </li>
          )}

          {loginUserState?.data.userType === LOGIN_USER_TYPE.USER && (
            <>
              <li className={`sub-menu ${adminLayoutStyles.other_links}`}>
                <a
                  onClick={(e) => {
                    e.preventDefault();
                    setSubscriptionSubMenuExpanded(
                      !subscriptionSubMenuExpanded
                    );
                  }}
                >
                  {/* <img src="/images/aric.svg" alt="AR" /> */}
                  <Image
                    src="/images/general/artists.png"
                    alt="Subscriptions"
                    width={22}
                    height={26}
                  />
                  <span className="nav-label">
                    Subscriptions
                    <span
                      className={
                        subscriptionSubMenuExpanded
                          ? "menu-is-open"
                          : "menu-is-closed"
                      }
                    >
                      <ArrowDownIcon />
                    </span>
                  </span>
                </a>
                <ul className={subscriptionSubMenuExpanded ? "" : "d-none"}>
                  <li>
                    <Link
                      href="/subscribe-artists"
                      className={
                        router.asPath === "/subscribe-artists" ? "active" : ""
                      }
                    >
                      {/* <img src="/images/general/artists.png" alt="Artists" />{" "} */}
                      <Image
                        src="/images/general/list.png"
                        alt="Artists"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Artists</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/subscribe-venues"
                      className={
                        router.asPath === "/subscribe-venues" ? "active" : ""
                      }
                    >
                      {/* <img src="/images/general/list.png" alt="Venues" />{" "} */}
                      <Image
                        src="/images/general/list.png"
                        alt="Venues"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Venues</span>
                    </Link>
                  </li>
                </ul>
              </li>
              <li>
                <Link
                  href="/my-account"
                  className={router.asPath === "/my-account" ? "active" : ""}
                >
                  {/* <img src="/images/menu/artist-profile.png" alt="CA" /> */}
                  <Image
                    src="/images/menu/artist-profile.png"
                    alt="Account Settings"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Account Settings</span>
                </Link>
              </li>
            </>
          )}

          {loginUserState?.data.userType === LOGIN_USER_TYPE.MANAGER && (
            <>
              {selectedArtistData?.objectId && (
                <>
                  <li>
                    <Link
                      href="/edit-artist-profile"
                      className={
                        router.asPath === "/edit-artist-profile" ? "active" : ""
                      }
                    >
                      {/* <img src="/images/menu/artist-profile.png" alt="CP" /> */}
                      <Image
                        src="/images/menu/artist-profile.png"
                        alt="Account Settings"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Artist Profile</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/message"
                      className={router.asPath === "/message" ? "active" : ""}
                    >
                      {/* <img src="/images/mmic.svg" alt="MM" /> */}
                      <Image
                        src="/images/general/messages.svg"
                        alt="My Messages"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">My Messages</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/shows"
                      className={router.asPath === "/shows" ? "active" : ""}
                    >
                      {/* <img src="/images/sic.svg" alt="SH" /> */}
                      <Image
                        src="/images/general/shows.svg"
                        alt="Shows"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Shows</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/calendar"
                      className={router.asPath === "/calendar" ? "active" : ""}
                    >
                      {/* <img src="/images/calic.svg" alt="CA" /> */}
                      <Image
                        src="/images/general/calendar-menu-1.svg"
                        alt="Calendar"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Calendar</span>
                    </Link>
                  </li>
                  <li className={`sub-menu ${adminLayoutStyles.other_links}`}>
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        setGigsSubMenuExpanded(!gigSubMenuExpanded);
                      }}
                    >
                      {/* <img src="/images/aric.svg" alt="AR" /> */}
                      <Image
                        src="/images/general/artists.png"
                        alt="Gig Posting"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">
                        Gig Posting
                        <span
                          className={
                            gigSubMenuExpanded
                              ? "menu-is-open"
                              : "menu-is-closed"
                          }
                        >
                          <ArrowDownIcon />
                        </span>
                      </span>
                    </a>
                    <ul className={gigSubMenuExpanded ? "" : "d-none"}>
                      <li>
                        <Link
                          href="/browse-open-postings"
                          className={
                            router.asPath === "/browse-open-postings"
                              ? "active"
                              : ""
                          }
                        >
                          {/* <img src="/images/general/discIcon.svg" alt="AC" />{" "} */}
                          <Image
                            src="/images/general/discIcon.svg"
                            alt="Browse Open Postings"
                            width={8}
                            height={8}
                          />
                          <span className="nav-label">
                            Browse Open Postings
                          </span>
                        </Link>
                      </li>
                      <li>
                        <Link
                          href="/artist-submissions"
                          className={
                            router.asPath === "/artist-submissions"
                              ? "active"
                              : ""
                          }
                        >
                          {/* <img src="/images/general/discIcon.svg" alt="AC" />{" "} */}
                          <Image
                            src="/images/general/discIcon.svg"
                            alt="My Submissions"
                            width={8}
                            height={8}
                          />
                          <span className="nav-label">My Submissions</span>
                        </Link>
                      </li>
                    </ul>
                  </li>
                </>
              )}

              <li>
                <Link
                  href="/my-account"
                  className={router.asPath === "/my-account" ? "active" : ""}
                >
                  {/* <img src="/images/menu/artist-profile.png" alt="CA" /> */}
                  <Image
                    src="/images/menu/artist-profile.png"
                    alt="Account Settings"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Account Settings</span>
                </Link>
              </li>

              <li>
                <a
                  onClick={(e) => {
                    e.preventDefault();
                    setIsRequestFeatureModalOpen(true);
                  }}
                  className={isrequestModalOpen ? "active" : ""}
                >
                  {/* <img src="/images/general/requestIcon.svg" alt="CA" /> */}
                  <Image
                    src="/images/general/requestIcon.svg"
                    alt="Request New Feature"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Request New Feature</span>
                </a>
              </li>

              {!isCollapse && (
                <div className="reqN">
                  <Link href="/create-artist" className="rnf">
                    Create New Artist &#8594;
                  </Link>
                </div>
              )}
            </>
          )}

          {loginUserState?.data?.userType === LOGIN_USER_TYPE.VENUE_ADMIN && (
            <>
              <li>
                <Link
                  href="/venue-calendar"
                  className={
                    router.asPath === "/venue-calendar" ? "active" : ""
                  }
                >
                  {/* <img src="/images/calic.svg" alt="CA" /> */}
                  <Image
                    src="/images/general/calendar-menu-1.svg"
                    alt="Calendar"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Calendar</span>
                </Link>
              </li>

              <li>
                <Link href="/user-shows">
                  {/* <img src="/images/general/shows.png" alt="Shows" /> */}
                  <Image
                    src="/images/general/shows.svg"
                    alt="Shows"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Shows</span>
                </Link>
              </li>

              <li>
                <Link
                  href="/venue-listing"
                  className={router.asPath === "/venue-listing" ? "active" : ""}
                >
                  {/* <img src="/images/general/list.png" alt="Venue Listing" /> */}
                  <Image
                    src="/images/general/list.png"
                    alt="Venues"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Venues</span>
                </Link>
              </li>

              <li>
                <Link
                  href="/my-account"
                  className={router.asPath === "/my-account" ? "active" : ""}
                >
                  {/* <img src="/images/menu/artist-profile.png" alt="CA" /> */}
                  <Image
                    src="/images/menu/artist-profile.png"
                    alt="Account Settings"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Account Settings</span>
                </Link>
              </li>
            </>
          )}

          {loginUserState?.data.userType === LOGIN_USER_TYPE.ADMIN && (
            <>
              <li>
                <Link
                  href="/message"
                  className={router.asPath === "/message" ? "active" : ""}
                >
                  {/* <img src="/images/mmic.svg" alt="MM" /> */}
                  <Image
                    src="/images/general/messages.svg"
                    alt="My Messages"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Messages</span>
                </Link>
              </li>
              <li>
                <Link
                  href="/claimed-artist"
                  className={
                    router.asPath === "/claimed-artist" ? "active" : ""
                  }
                >
                  {/* <img src="/images/general/artists.png" alt="Artists" /> */}
                  <Image
                    src="/images/general/artists.png"
                    alt="Artists"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Artists</span>
                </Link>
              </li>
              <li>
                <Link href="/user-shows">
                  {/* <img src="/images/general/shows.png" alt="Shows" /> */}
                  <Image
                    src="/images/general/shows.svg"
                    alt="Shows"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Shows</span>
                </Link>
              </li>
              <li>
                <Link
                  href="/admin-calendar"
                  className={
                    router.asPath === "/admin-calendar" ? "active" : ""
                  }
                >
                  {/* <img src="/images/general/calendar-menu.png" alt="Calendar" /> */}
                  <Image
                    src="/images/general/calendar-menu.png"
                    alt="Calendar"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">Calendar</span>
                </Link>
              </li>
              <li className={`sub-menu ${adminLayoutStyles.other_links}`}>
                <a
                  onClick={(e) => {
                    e.preventDefault();
                    setFeaturedSubMenuExpanded(!featuredSubMenuExpanded);
                  }}
                >
                  {/* <img
                    src="/images/general/featured.png"
                    alt="Featured Artists & Venues"
                  /> */}
                  <Image
                    src="/images/general/featured.png"
                    alt="Featured Artists & Venues"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">
                    Featured Artists & Venues
                    <span
                      className={
                        featuredSubMenuExpanded
                          ? "menu-is-open"
                          : "menu-is-closed"
                      }
                    >
                      <ArrowDownIcon />
                    </span>
                  </span>
                </a>
                <ul className={featuredSubMenuExpanded ? "" : "d-none"}>
                  <li>
                    <Link
                      href="/featured-artists"
                      className={
                        router.asPath === "/featured-artists" ? "active" : ""
                      }
                    >
                      {/* <img src="/images/general/artists.png" alt="AC" />{" "} */}
                      <Image
                        src="/images/general/artists.png"
                        alt="Artists"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Artists</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/featured-venues"
                      className={
                        router.asPath === "/featured-venues" ? "active" : ""
                      }
                    >
                      {/* <img src="/images/general/list.png" alt="AC" />{" "} */}
                      <Image
                        src="/images/general/list.png"
                        alt="Venues"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Venues</span>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className={`sub-menu ${adminLayoutStyles.other_links}`}>
                <a
                  onClick={(e) => {
                    e.preventDefault();
                    setAdminGeneralSubMenuExpanded(
                      !adminGeneralSubMenuExpanded
                    );
                  }}
                >
                  {/* <img
                    src="/images/general/modules.png"
                    alt="General Modules"
                  /> */}
                  <Image
                    src="/images/general/modules.png"
                    alt="General Modules"
                    width={27}
                    height={26}
                  />
                  <span className="nav-label">
                    General Modules
                    <span
                      className={
                        adminGeneralSubMenuExpanded
                          ? "menu-is-open"
                          : "menu-is-closed"
                      }
                    >
                      <ArrowDownIcon />
                    </span>
                  </span>
                </a>
                <ul className={adminGeneralSubMenuExpanded ? "" : "d-none"}>
                  <li>
                    <Link
                      href="/embed-code"
                      className={
                        router.asPath === "/embed-code" ? "active" : ""
                      }
                    >
                      {/* <img
                        src="/images/general/embed-code.png"
                        alt="Embed Code"
                      /> */}
                      <Image
                        src="/images/general/embed-code.png"
                        alt="Embed Code"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Embed Code</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/artist-element"
                      className={
                        router.asPath === "/artist-element" ? "active" : ""
                      }
                    >
                      {/* <img
                        src="/images/general/list.png"
                        alt="Artist Element"
                      /> */}
                      <Image
                        src="/images/general/list.png"
                        alt="Artist Element"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Artist Element</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/user-questions"
                      className={
                        router.asPath === "/user-questions" ? "active" : ""
                      }
                    >
                      {/* <img
                        src="/images/general/list.png"
                        alt="User Questions"
                      /> */}
                      <Image
                        src="/images/general/list.png"
                        alt="User Questions"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">User Questions</span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/venue-listing"
                      className={
                        router.asPath === "/venue-listing" ? "active" : ""
                      }
                    >
                      {/* <img src="/images/general/list.png" alt="Venue Listing" /> */}
                      <Image
                        src="/images/general/list.png"
                        alt="Venue Listing"
                        width={27}
                        height={26}
                      />
                      <span className="nav-label">Venues</span>
                    </Link>
                  </li>
                </ul>
              </li>
            </>
          )}
        </ul>

        {loginUserState?.data.userType !== LOGIN_USER_TYPE.MANAGER &&
          !isCollapse && (
            <div className="reqN">
              <a
                onClick={(e) => {
                  e.preventDefault();
                  setIsRequestFeatureModalOpen(true);
                }}
                className="rnf"
              >
                Request New Feature &#8594;
              </a>
            </div>
          )}
      </nav>
    </>
  );
};
export default AdminLayoutSideBar;
