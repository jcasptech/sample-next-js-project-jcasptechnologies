import { RootState } from "@redux/reducers";
import { LoginUserState } from "@redux/slices/auth";
import { fetchSelectedArtist } from "@redux/slices/selectedArtist";
import { selectIdSuccess } from "@redux/slices/UserId";
import { Router, useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AdminLayoutHeader from "./header";
import AdminLayoutSideBar from "./sidebar";

export const AdminLayoutComponent: React.FC = ({ children }: any) => {
  const router = useRouter();
  const [isCollapse, setIsCollapse] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  const handleToggleCollapse = () => {
    setIsCollapse(!isCollapse);
  };

  const handleBlur = () => {
    if (isMobile) {
      if (!isCollapse) {
        setIsCollapse(true);
      }
    }
  };

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth <= 992) {
        setIsCollapse(true);
        setIsMobile(true);
      } else {
        setIsCollapse(false);
        setIsMobile(false);
      }
    };

    // Initial check
    handleResize();

    // Attach the event listener
    window.addEventListener("resize", handleResize);

    // Clean up the event listener on unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  Router.events.on("routeChangeComplete", () => {
    if (isMobile) {
      setIsCollapse(true);
    }
  });

  return (
    <>
      <div className="panel-body">
        <AdminLayoutSideBar
          {...{
            isCollapse,
            setIsCollapse,
          }}
        />
        <div className="panel-content">
          <div className="container-fluid p-0" onBlur={handleBlur}>
            <AdminLayoutHeader
              {...{
                pathname: router.pathname,
                handleToggleCollapse,
              }}
            />
            {children}
          </div>
        </div>
      </div>
    </>
  );
};
