import {
  faFacebookF,
  faInstagram,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { RootState } from "@redux/reducers";
import Image from "next/image";
import Link from "next/link";
import { useSelector } from "react-redux";
import footerStyles from "./footerComponent.module.scss";

const FooterComponent = () => {
  const general: any = useSelector((state: RootState) => state.general);

  return (
    <>
      <footer
        className={
          general?.theme == "dark"
            ? footerStyles.footerDark
            : footerStyles.footer
        }
      >
        <div className="container-fluid">
          <div className={`row m0 ${footerStyles.footer__widgets}`}>
            <div className="col-12 col-sm-10 col-lg-10 widgetHold">
              <div
                className={`${footerStyles.footer__foot_line} float-left w-100`}
              >
                <h5 className="font-harmoniaSansW01-semibold">
                  Live Music Near You
                </h5>
              </div>
              <div
                className={`${footerStyles.footer__foot_menu} float-left w-100`}
              >
                <ul className={footerStyles.menu}>
                  <li className={footerStyles.menus}>
                    <Link href={"/"}>Show</Link>
                  </li>
                  <li className={footerStyles.menus}>
                    <Link href={"/browse"}>Artist</Link>
                  </li>
                  <li className={footerStyles.menus}>
                    <Link href={"/terms-of-use"} target="_blank">
                      Terms
                    </Link>
                  </li>
                  <li className={footerStyles.menus}>
                    <Link href={"/privacy-policy"} target="_blank">
                      Privacy Policy
                    </Link>
                  </li>
                  {/* <li className={footerStyles.menus}>
                    <Link
                      href={"/chat-gpt-plugin-terms-of-use"}
                      target="_blank"
                    >
                      Chat GPT Plugin Terms of Use
                    </Link>
                  </li> */}
                  <li className={footerStyles.menus}>
                    <Link href={"/contact-us"}>Contact Us</Link>
                  </li>
                  <li className={footerStyles.menus}>
                    <Link href={"/faq"}>FAQ</Link>
                  </li>
                </ul>
              </div>
              <div className={`${footerStyles.footer__foot_social} w-100`}>
                <ul>
                  <li>
                    <Link
                      href="https://www.facebook.com/JCaspmusic"
                      target="_blank"
                      className={footerStyles.socialLinks}
                    >
                      <FontAwesomeIcon icon={faFacebookF} />
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="https://www.instagram.com/JCaspmusic/"
                      target="_blank"
                      className={footerStyles.socialLinks}
                    >
                      <FontAwesomeIcon icon={faInstagram} />
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="https://twitter.com/JCaspmusic"
                      target="_blank"
                      className={footerStyles.socialLinks}
                    >
                      <FontAwesomeIcon icon={faTwitter} />
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            <div
              className={`col-12 col-sm-2 col-lg-2 ${footerStyles.footer__foot_download}`}
            >
              <h6 className="">Download JCasp &nbsp;</h6>
              <ul className={footerStyles.downloadLinks}>
                <li>
                  <Link
                    href="https://play.google.com/store/apps/details?id=com.JCasp.fender"
                    className="hvr-float-shadow"
                    target="_blank"
                  >
                    <Image
                      src="/images/home/google-play.webp"
                      alt="Download from playstore"
                      width={170}
                      height={51}
                    />
                  </Link>
                </li>
                <li>
                  <Link
                    href="https://apps.apple.com/us/app/JCasp-local-shows/id932936250"
                    className="hvr-float-shadow"
                    target="_blank"
                  >
                    <Image
                      src="/images/home/app_store.webp"
                      alt="Download from appstore"
                      width={170}
                      height={51}
                    />
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className={`row m0 ${footerStyles.footer__copyright}`}>
            <div className={`col-12 col-sm-6 ${footerStyles.text}`}>
              Copyright 2023 JCasp | All rights reserved.
            </div>
            <div className={`col-12 col-sm-6 ${footerStyles.credit}`}>
              Design & Developed by{" "}
              <Link href="https://jjcasptechnologies.com/" target="_blank">
                JCaspTechnologies
              </Link>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};
export default FooterComponent;
