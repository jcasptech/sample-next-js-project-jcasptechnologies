import { Logo } from "@components/theme";
import { AccountIcon } from "@components/theme/icons/accountIcon";
import { DashboardIcon } from "@components/theme/icons/dashboardIcon";
import { LogoutIcon } from "@components/theme/icons/logoutIcon";
import { RootState } from "@redux/reducers";
import { LoginUserState } from "@redux/slices/auth";
import { Divider } from "antd";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { useSelector } from "react-redux";
import { LayoutContext, LayoutContextModel } from "src/contexts/layoutContext";
import mainLayoutStyles from "../mainLayoutStyles.module.scss";

export const HeaderComponent = () => {
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const router = useRouter();

  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const { doLogout }: LayoutContextModel = useContext(LayoutContext);
  const general: any = useSelector((state: RootState) => state.general);

  return (
    <>
      <nav
        className={`navbar navbar-expand-lg ${
          general?.theme === "dark"
            ? mainLayoutStyles.gigt_navDark
            : mainLayoutStyles.gigt_nav
        }`}
      >
        <div className="container-fluid">
          <Link
            href="/"
            className={"navbar-brand"}
            onClick={(e: any) => {
              e.preventDefault();
              router.push("/");
            }}
          >
            <Logo />
          </Link>
          <button
            className={`navbar-toggler ${mainLayoutStyles.mobileMenu}`}
            type="button"
            onClick={() => {
              setIsMobileMenuOpen(!isMobileMenuOpen);
            }}
          >
            <span
              className={`${
                general?.theme == "dark"
                  ? "navbar-toggler-icon dark"
                  : "navbar-toggler-icon"
              }`}
            ></span>
          </button>

          <div
            className={`collapse navbar-collapse ${
              isMobileMenuOpen ? "show" : ""
            }`}
            id="navbarSupportedContent"
          >
            <ul
              className={`navbar-nav main_navigation mb-2 mb-lg-0 align-items-left ${
                general.theme === "dark"
                  ? mainLayoutStyles.main_navigationDark
                  : mainLayoutStyles.main_navigation
              }`}
            >
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/" ? mainLayoutStyles.active_route : ""
                  }`}
                  aria-current="page"
                  href="/"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={
                      general?.theme === "dark"
                        ? "/images/frameShow.png"
                        : "/images/showsic.png"
                    }
                    alt="Shows"
                    width={20}
                    height={20}
                  />
                  &nbsp;Shows
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/browse"
                      ? mainLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/browse"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={
                      general?.theme === "dark"
                        ? "/images/Vector.png"
                        : "/images/browsearic.png"
                    }
                    alt="browse"
                    width={20}
                    height={20}
                  />
                  &nbsp;Browse
                </Link>
              </li>
              {/* <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/how-it-works"
                      ? mainLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/how-it-works"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={
                      general?.theme === "dark"
                        ? "/images/frameIt.png"
                        : "/images/hiwic.png"
                    }
                    alt="How It Works"
                    width={20}
                    height={20}
                  />
                  &nbsp;How It Works
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/about-us"
                      ? mainLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/about-us"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={
                      general.theme === "dark"
                        ? "/images/aboutWhiteImage.png"
                        : "/images/auic.png"
                    }
                    alt="About us"
                    width={20}
                    height={20}
                  />
                  &nbsp;About Us
                </Link>
              </li> 
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/contact-us"
                      ? mainLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/contact-us"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={
                      general.theme === "dark"
                        ? "/images/emailVector.png"
                        : "/images/cuic.png"
                    }
                    alt="About Us"
                    width={20}
                    height={20}
                  />
                  &nbsp;Contact Us
                </Link>
              </li>*/}

              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/services"
                      ? mainLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/services"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={
                      general.theme === "dark"
                        ? "/images/menu/customer-service-white.png"
                        : "/images/menu/customer-service-black.png"
                    }
                    alt="Services"
                    width={20}
                    height={20}
                  />
                  &nbsp;Services
                </Link>
              </li>

              {!loginUserState?.isLogin && (
                <li className={`nav-item`}>
                  <div className={mainLayoutStyles.submenu}>
                    <Link
                      href="/login"
                      className="hvr-float-shadow"
                      onClick={() => {
                        setIsMobileMenuOpen(false);
                      }}
                    >
                      Log In
                    </Link>
                    <span>or</span>
                    <Link
                      href="/signup"
                      className={`hvr-float-shadow ${
                        router.pathname == "/signup"
                          ? mainLayoutStyles.active_route
                          : ""
                      }`}
                      onClick={() => {
                        setIsMobileMenuOpen(false);
                      }}
                    >
                      Register
                    </Link>
                  </div>
                </li>
              )}
            </ul>
          </div>

          {loginUserState && loginUserState?.isLogin && (
            <nav
              className={`navbar fixed-top1 navbar-expand-lg navbar-light secondary-menu ${
                isMobileMenuOpen ? "" : "loginMenu"
              }`}
            >
              <div className="container-fluid mnopad">
                <div
                  className="collapse1 navbar-collapse justify-content-end"
                  id="navbarNav"
                >
                  <ul className="navbar-nav">
                    <li className="nav-item  account-dropdown">
                      <a
                        className={`${
                          general?.theme === "dark"
                            ? "nav-link-dark"
                            : "nav-link"
                        } dropdown-toggle capitalize`}
                        href="#"
                        id="navbarDropdown"
                        role="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        <Image
                          src={
                            loginUserState.data?.user?.avatarImage?.url ||
                            "/images/artist-placeholder.png"
                          }
                          alt={loginUserState.data?.user?.name}
                          className="ppic"
                          width={50}
                          height={50}
                        />
                        {loginUserState.data?.user?.name}
                      </a>
                      <ul
                        className="dropdown-menu  mt-2"
                        aria-labelledby="navbarDropdown"
                      >
                        <li>
                          <Link className="dropdown-item" href="/dashboard">
                            <DashboardIcon />
                            My Dashboard
                          </Link>
                        </li>
                        <li>
                          <Link className="dropdown-item" href="/my-account">
                            <AccountIcon />
                            My Account
                          </Link>
                        </li>
                        <li>
                          <Divider className="mt-2 mb-2" />
                          <Link
                            className="dropdown-item"
                            href="#"
                            onClick={doLogout}
                          >
                            <LogoutIcon />
                            Logout
                          </Link>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
          )}

          {!loginUserState?.isLogin && (
            <>
              <div
                className={`${
                  general.theme === "dark"
                    ? mainLayoutStyles.menu_actsDark
                    : mainLayoutStyles.menu_acts
                }`}
              >
                <Link href="/login" className="hvr-float-shadow">
                  Log In
                </Link>
                <span>or</span>
                <Link
                  href="/signup"
                  className={`hvr-float-shadow ${
                    router.pathname == "/signup"
                      ? mainLayoutStyles.active_route
                      : ""
                  }`}
                >
                  Register
                </Link>
              </div>
            </>
          )}
        </div>
      </nav>
    </>
  );
};
