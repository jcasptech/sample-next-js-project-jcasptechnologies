import React from "react";
import FooterComponent from "./footer/footerComponent";
import { HeaderComponent } from "./header/headerComponent";

export const MainLayoutComponent: React.FC = ({ children }: any) => {
  return (
    <>
      <HeaderComponent />
      {children}
      <FooterComponent />
    </>
  );
};
