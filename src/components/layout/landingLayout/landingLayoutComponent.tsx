import React from "react";
import FooterComponent from "../mainLayout/footer/footerComponent";
import { HeaderComponent } from "./header/headerComponent";

export const LandingLayoutComponent: React.FC = ({ children }: any) => {
  return (
    <>
      <HeaderComponent />
      {children}
      <FooterComponent />
    </>
  );
};
