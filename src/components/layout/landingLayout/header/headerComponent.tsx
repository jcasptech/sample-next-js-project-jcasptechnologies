import { Button, Logo } from "@components/theme";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { FullStartIcon } from "@components/theme/icons/fullStarIcon";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { LoginUserState } from "@redux/slices/auth";
import { setEnquiryPhone, setEnquiryType } from "@redux/slices/general";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { LayoutContext, LayoutContextModel } from "src/contexts/layoutContext";
import {
  LandingPagePhoneInputs,
  LandingPagePhoneValidateSchema,
} from "src/schemas/LandingPageFormSchema";
import landingLayoutStyles from "../landingLayoutStyles.module.scss";

export const HeaderComponent = () => {
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const router = useRouter();
  const dispatch = useDispatch();

  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const { doLogout }: LayoutContextModel = useContext(LayoutContext);

  const { register, handleSubmit, formState } = useForm<LandingPagePhoneInputs>(
    {
      resolver: yupResolver(LandingPagePhoneValidateSchema),
    }
  );

  const onSubmit = (data: any) => {
    if (data?.phone) {
      dispatch(setEnquiryPhone(data.phone));
      dispatch(setEnquiryType("Schedule a Call"));
      scrollToDiv();
    }
  };
  const [isLoading, setIsLoading] = useState(false);

  const scrollToDiv = () => {
    const divElement = document.getElementById("contactForm");
    if (divElement) {
      divElement.scrollIntoView({ behavior: "smooth" });
    }
  };

  useEffect(() => {
    dispatch(setEnquiryPhone(""));
    dispatch(setEnquiryType(""));
  }, []);

  return (
    <div className={landingLayoutStyles.header}>
      <nav className={`navbar navbar-expand-lg`}>
        <div className="container-fluid">
          <Link
            href="/"
            className={"navbar-brand"}
            onClick={(e: any) => {
              e.preventDefault();
              router.push("/");
            }}
          >
            <Logo />
          </Link>

          <button
            className={`navbar-toggler ${landingLayoutStyles.mobileMenu}`}
            type="button"
            onClick={() => {
              setIsMobileMenuOpen(!isMobileMenuOpen);
            }}
          >
            <span className={`navbar-toggler-icon dark`}></span>
          </button>

          <div
            className={`collapse navbar-collapse ${
              isMobileMenuOpen ? "show" : ""
            }`}
            id="navbarSupportedContent"
          >
            <ul
              className={`navbar-nav main_navigation mb-2 mb-lg-0 align-items-left`}
            >
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/"
                      ? landingLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={"/images/menu/header-show-white.png"}
                    alt="Shows"
                    width={20}
                    height={20}
                  />
                  {/* <img src={"/images/menu/header-show-white.png"} alt="Shows" /> */}
                  &nbsp;Shows
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/browse"
                      ? landingLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/browse"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={"/images/menu/header-browse-white.png"}
                    alt="Browse"
                    width={20}
                    height={20}
                  />
                  {/* <img
                    src={"/images/menu/header-browse-white.png"}
                    alt="Browse"
                  /> */}
                  &nbsp;Browse
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/services"
                      ? landingLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/services"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <Image
                    src={"/images/menu/customer-service-white.png"}
                    alt="Services"
                    width={20}
                    height={20}
                  />
                  &nbsp;Services
                </Link>
              </li>

              {/* <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/how-it-works"
                      ? landingLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/how-it-works"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <img
                    src={"/images/menu/header-how-it-works-white.png"}
                    alt="How It Works"
                  />
                  &nbsp;How It Works
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/about-us"
                      ? landingLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/about-us"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <img
                    src={"/images/menu/header-about-us-white.png"}
                    alt="About Us"
                  />
                  &nbsp;About Us
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={`nav-link ${
                    router.asPath === "/contact-us"
                      ? landingLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  href="/contact-us"
                  onClick={() => {
                    setIsMobileMenuOpen(false);
                  }}
                >
                  <img
                    src={"/images/menu/header-contact-us-white.png"}
                    alt="Contact Us"
                  />
                  &nbsp;Contact Us
                </Link>
              </li> */}

              <li className="nav-item register_button_mobile">
                <a
                  className={`nav-link ${
                    router.asPath === "/signup"
                      ? landingLayoutStyles.active_route
                      : ""
                  }`}
                  aria-current="page"
                  onClick={(e) => {
                    e.preventDefault();
                    setIsMobileMenuOpen(false);
                    scrollToDiv();
                  }}
                >
                  <Button
                    htmlType="button"
                    type="primary"
                    onClick={() => {
                      scrollToDiv();
                    }}
                  >
                    Contact Us
                  </Button>
                </a>
              </li>
            </ul>
          </div>

          <div className="register_button">
            <Button
              htmlType="button"
              type="primary"
              onClick={() => {
                scrollToDiv();
              }}
            >
              Contact Us
            </Button>
          </div>
        </div>
      </nav>

      <div className={`${landingLayoutStyles.schedule_a_call} container-fluid`}>
        <div className="call_section">
          <h1>Your Ideal Live Music Co-Pilot for Restaurants</h1>
          <p>
            Elevate your dining experience with live music using JCasp LITE.
            We simplify the process of booking your own music while providing
            the expertise and resources you need. Dive into a world of diverse
            artists and genres, and create unforgettable nights for your
            patrons.
          </p>

          <form
            className={`call_input_section`}
            onSubmit={handleSubmit(onSubmit)}
            data-aos="fade-down"
          >
            <div className={`call_input_group`}>
              <InputField
                {...{
                  register,
                }}
                id="phone"
                formState={formState}
                type="text"
                className={`form-control`}
                placeholder="Enter Your Contact Number"
                showError={false}
              />
              <Button
                className={`btn btn-outline-secondary`}
                htmlType="submit"
                disabled={isLoading || formState?.isSubmitting}
                loading={isLoading || formState?.isSubmitting}
              >
                <span>Schedule a Call</span>
                <Image
                  src="/images/landing-page/send-icon.png"
                  alt="Send"
                  width={18}
                  height={18}
                />
                {/* <img src="/images/landing-page/send-icon.png" alt="Send" /> */}
              </Button>
            </div>
            {formState?.errors &&
              formState?.errors?.phone &&
              formState?.errors?.phone?.message && (
                <span className="text-danger">
                  {formState?.errors?.phone?.message}
                </span>
              )}
            <Image
              src="/images/landing-page/button-effect.png"
              alt="Send"
              width={50}
              height={32}
            />
            {/* <img src="/images/landing-page/button-effect.png" alt="" /> */}
          </form>

          <div className="latest_artist_profile">
            <ul>
              <li>
                <Image
                  src="/images/landing-page/artist-1.png"
                  alt="artist 1"
                  width={45}
                  height={46}
                />
                {/* <img src="/images/landing-page/artist-1.png" alt="artist 1" /> */}
              </li>
              <li>
                <Image
                  src="/images/landing-page/artist-2.png"
                  alt="artist 2"
                  width={45}
                  height={46}
                />
                {/* <img src="/images/landing-page/artist-2.png" alt="artist 2" /> */}
              </li>
              <li>
                <Image
                  src="/images/landing-page/artist-3.png"
                  alt="artist 3"
                  width={45}
                  height={46}
                />
                {/* <img src="/images/landing-page/artist-3.png" alt="artist 3" /> */}
              </li>
              <li>
                <Image
                  src="/images/landing-page/artist-4.png"
                  alt="artist 4"
                  width={45}
                  height={46}
                />
                {/* <img src="/images/landing-page/artist-4.png" alt="artist 4" /> */}
              </li>
              <li>
                <Image
                  src="/images/landing-page/artist-5.png"
                  alt="artist 5"
                  width={45}
                  height={46}
                />
                {/* <img src="/images/landing-page/artist-5.png" alt="artist 5" /> */}
              </li>
            </ul>
            <div className="rating">
              <ul>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>5.0</li>
              </ul>
              <p>Exceptional Client Satisfaction</p>
            </div>
          </div>
        </div>
        <div className="review_section">
          <div className="review_box">
            <div className="information">
              <strong>
                Chastity
                <Image
                  src="/images/landing-page/verified.png"
                  alt="verified"
                  width={32}
                  height={22}
                />
                {/* <img src="/images/landing-page/verified.png" alt="verified" /> */}
              </strong>

              <ul>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
              </ul>
            </div>
            <div className="review">
              <p>
                JCasp has made it incredibly easy to host and promote live
                music at all of our locations. They have streamlined the entire
                process so we can focus on what we do best.
                <i>- Director of Marketing, Black Angus Restaurants</i>
              </p>
            </div>
          </div>

          <div className="review_box">
            <div className="information">
              <strong>
                Eva
                {/* <img src="/images/landing-page/verified.png" alt="verified" /> */}
                <Image
                  src="/images/landing-page/verified.png"
                  alt="verified"
                  width={32}
                  height={22}
                />
              </strong>

              <ul>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
              </ul>
            </div>
            <div className="review">
              <p>
                They provide a one-stop-shop for booking music / entertainment
                and has been a dream to work with so far.
                <i>- AGM, Sun Communities</i>
              </p>
            </div>
          </div>

          <div className="review_box">
            <div className="information">
              <strong>
                Jamie
                {/* <img src="/images/landing-page/verified.png" alt="verified" /> */}
                <Image
                  src="/images/landing-page/verified.png"
                  alt="verified"
                  width={32}
                  height={22}
                />
              </strong>

              <ul>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
              </ul>
            </div>
            <div className="review">
              <p>
                Over the past seven years JCasp has showered my restaurant
                with fantastic entertainers. Our guests are thrilled to have
                live music back after this past year.
                <i>- Owner, Cafe Luna</i>
              </p>
            </div>
          </div>

          <div className="review_box">
            <div className="information">
              <strong>
                Alyssa
                {/* <img src="/images/landing-page/verified.png" alt="verified" /> */}
                <Image
                  src="/images/landing-page/verified.png"
                  alt="verified"
                  width={32}
                  height={22}
                />
              </strong>

              <ul>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
                <li>
                  <FullStartIcon />
                </li>
              </ul>
            </div>
            <div className="review">
              <p>
                JCasp makes it so easy for us to host live music. Their vetted
                musicians are always professional, punctual, extremely talented,
                and easy to work with. We love their service!
                <i>- Manager, Farmer and the Seahorse</i>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
