import authLayoutStyles from "./authLayoutStyles.module.scss";
import { Logo } from "../../theme";
import Link from "next/link";
import { useRouter } from "next/router";

const AuthLayoutComponent = ({ children }: any) => {
  const currentYear = new Date().getFullYear();
  const router = useRouter();

  return (
    <>
      <div className={authLayoutStyles.layoutContainer}>
        <div
          className={`${authLayoutStyles.layoutContainer__logoContainer} border-bottom-light-primary flex items-center`}
        >
          <Link href="/">
            <div
              onClick={(e: any) => {
                e.preventDefault();
                if (router.asPath !== "/onboarding") {
                  router.push("/login");
                }
              }}
            >
              <a>
                <Logo className="ml-7" />
              </a>
            </div>
          </Link>
        </div>
        <div className={authLayoutStyles.layoutWrapper}>
          <div className={`${authLayoutStyles.layoutSection}`}>
            <div
              className={`${authLayoutStyles.layoutContainer__formContainer}`}
            >
              {children}
            </div>
            <div
              className={`${authLayoutStyles.layoutContainer__authFooter} bottom-0 right-0 left-0`}
            >
              <p className="f-15">
                © {currentYear} Healthcare GH Limited. All Rights
                Reserved.&nbsp;
                <Link href="/terms-of-use">
                  <a
                    target="_blank"
                    className="text-primary underline hover:underline"
                  >
                    Terms of Use
                  </a>
                </Link>
                &nbsp;and&nbsp;
                <Link href="/privacy-policy">
                  <a
                    target="_blank"
                    className="text-primary underline hover:underline"
                  >
                    Privacy Policy
                  </a>
                </Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AuthLayoutComponent;
