import { Button } from "@components/theme";
import {
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { requestFeatureAPI } from "@redux/services/auth.api";
import { LoginUserState } from "@redux/slices/auth";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import Select from "react-select";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  RequestFeatureFormValidateSchema,
  RequestFeatureInputs,
} from "src/schemas/requestFeatureSchema";
import requesFeatureStyle from "./requestFeature.module.scss";

export interface RequestFeatureModalProps {
  isOpen: boolean;
  setIsRequestFeatureModalOpen: (data: any) => void;
}

const RequestFeatureModal = (props: RequestFeatureModalProps) => {
  const { isOpen, setIsRequestFeatureModalOpen } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const [userType, setUsertype] = useState("");
  const {
    register,
    handleSubmit,
    formState,
    control,
    setValue,
    setFocus,
    reset,
  } = useForm<RequestFeatureInputs>({
    resolver: yupResolver(RequestFeatureFormValidateSchema),
  });

  useEffect(() => {
    if (isOpen) {
      setIsModalOpen(isOpen);
    }
  }, [isOpen]);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsRequestFeatureModalOpen(false);
  };

  const selectOptions = [
    { value: "Artist", label: "Artist" },
    { value: "Venue", label: "Venue" },
    { value: "Music Fan", label: "Music Fan" },
  ];

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      await requestFeatureAPI(data);
      showToast(SUCCESS_MESSAGES.requestFeature, "success");
      reset();
      setIsModalOpen(false);
      setIsRequestFeatureModalOpen(false);
      setIsLoading(false);
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  const handleUserType = (data: any) => {
    setUsertype(data.value);
    setValue("userType", data.value);
  };

  return (
    <>
      <Modal
        show={isModalOpen}
        className={requesFeatureStyle.modalStyle}
        onHide={handleCancel}
      >
        <Modal.Header closeButton className={requesFeatureStyle.header}>
          <h1>Request a Feature</h1>
        </Modal.Header>
        <Modal.Body>
          <div>
            <div className="GigExperience">
              <h2>Shape Your JCasp Experience</h2>
            </div>
            <div className="des">
              <p>
                Hey there, JCasp community! As a small, dedicated team,
                we&apos;re deeply committed to providing you with the best
                experience on our platform. We genuinely value your feedback and
                ideas. If you have a suggestion for a cool new feature or an
                improvement to an existing one, please share it with us through
                the form below. Your insights are invaluable in helping us
                evolve JCasp to better serve artists and music enthusiasts
                alike. Thank you for your patience and ongoing support as we
                continually work to enhance the platform.
              </p>
            </div>
          </div>

          <form
            className={`panel-form ${requesFeatureStyle.main_form}`}
            onSubmit={handleSubmit(onSubmit)}
            method="POST"
          >
            <fieldset>
              <div className="">
                <div className="col align-center field-row">
                  <div className="col-12 col-sm-4">
                    <label className={requesFeatureStyle.label}>
                      Email Address
                    </label>
                  </div>
                  <div className="col-12 ">
                    <div className="">
                      <InputField
                        {...{
                          register,
                          formState,
                          defaultValue: `${
                            loginUserState?.data?.user?.email || ""
                          }`,
                          id: "email",
                          className: `inputField`,
                          placeholder: "Enter your email address",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="col align-center field-row">
                  <div className="col-12 col-sm-4">
                    <label className={requesFeatureStyle.label}>
                      Full Name
                    </label>
                  </div>
                  <div className="col-12">
                    <div className="">
                      <InputField
                        {...{
                          register,
                          formState,
                          defaultValue: `${
                            loginUserState?.data?.user?.name || ""
                          }`,
                          id: "name",
                          className: `inputField`,
                          placeholder: "Enter Your name",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="col align-center field-row">
                  <div className="col-12 col-sm-4">
                    <label className={requesFeatureStyle.label}>Select</label>
                  </div>
                  <div className="col-12 ">
                    <div className={`${requesFeatureStyle.select_field}`}>
                      <Select
                        name="userType"
                        className="basic_multi_select"
                        id="userType"
                        options={selectOptions}
                        onChange={handleUserType}
                        instanceId="userType"
                      />
                      {formState &&
                        formState?.errors &&
                        formState?.errors["userType"] &&
                        formState?.errors["userType"].message && (
                          <span className="text-danger">
                            {formState?.errors["userType"].message}
                          </span>
                        )}
                    </div>
                  </div>
                </div>
                <div className="col align-center field-row">
                  <div className="col-12 col-sm-4">
                    <label className={requesFeatureStyle.label}>Message</label>
                  </div>
                  <div className="col-12 ">
                    <TextAreaField
                      {...{
                        register,
                        formState,
                        id: "message",
                        className: `${requesFeatureStyle.textAreaField}`,
                        placeholder: "Enter your Message",
                      }}
                    />
                  </div>
                </div>

                <div className="row align-center field-row">
                  <div className="col-12 col-sm-4"></div>
                  <div className="col-12 ">
                    <Button
                      htmlType="submit"
                      type="primary"
                      className="float-right"
                      loading={isLoading}
                      disabled={isLoading}
                    >
                      Send Messsage
                    </Button>
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default RequestFeatureModal;
