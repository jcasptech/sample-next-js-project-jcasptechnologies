import React from "react";
import Head from "next/head";
import { SEO_DETAILS, SEO_DETAILS_PAGES } from "src/libs/seo";
import { BASE_URL_NAME } from "src/libs/constants";

export interface SEOProps {
  pageName: string;
}

const SEO = (props: SEOProps) => {
  const { pageName } = props;

  let seoDetails = SEO_DETAILS_PAGES[pageName];
  if (!seoDetails) {
    seoDetails = {
      title: "Local Shows on JCasp",
      description:
        "Find local, live music happening tonight on our shows calendar",
      url: `${BASE_URL_NAME}`,
    };
  }

  return (
    <Head>
      <title>{seoDetails?.title}</title>

      <meta name="description" content={seoDetails?.description} />
      <meta property="og:title" content={seoDetails?.title} />
      <meta property="og:description" content={seoDetails?.description} />
      <meta property="og:image" content={SEO_DETAILS?.image} />
      <meta property="og:url" content={seoDetails?.url} />
      <meta property="og:site_name" content={SEO_DETAILS?.siteName} />
      <meta property="og:type" content="website" />
      <meta property="og:locale" content={SEO_DETAILS?.locale} />

      <meta name="twitter:widgets:csp" content="on" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content={SEO_DETAILS?.twitter} />
      <meta name="twitter:app:name:iphone" content={SEO_DETAILS?.siteName} />
      <meta name="twitter:app:name:ipad" content={SEO_DETAILS?.siteName} />
      <meta
        name="twitter:app:name:googleplay"
        content={SEO_DETAILS?.siteName}
      />
      <meta name="twitter:app:id:iphone" content={SEO_DETAILS?.phone} />
      <meta name="twitter:app:id:ipad" content={SEO_DETAILS?.phone} />
      <meta
        name="twitter:app:id:googleplay"
        content={SEO_DETAILS?.googleplay}
      />
      <meta name="twitter:app:url:iphone" content="JCasp://" />
      <meta name="twitter:app:url:ipad" content="JCasp://" />
      <meta name="twitter:app:url:googleplay" content="JCasp://" />
      <meta name="twitter:url" content={seoDetails?.url} />
      <meta name="twitter:title" content={seoDetails?.title} />
      <meta name="twitter:description" content={seoDetails?.description} />
      <meta name="twitter:image" content={SEO_DETAILS?.image} />
    </Head>
  );
};

export default SEO;
