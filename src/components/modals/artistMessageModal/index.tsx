import { Button } from "@components/theme/button";
import {
  FormGroup,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { artistMessageAPI } from "@redux/services/artist.api";
import { ArtistObject } from "@redux/slices/artists";
import { LoginUserState } from "@redux/slices/auth";
import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ArtistMessageInputs,
  ArtistMessageSchema,
} from "src/schemas/artistMessageSchema";
import { LoginModal } from "../loginModal";
import modalstyle from "./artistMessageStyle.module.scss";

export interface ArtistMessageModalProps {
  artistDetail: ArtistObject;
  isOpen: boolean;
  setIsMesaageModalOpen: (data: any) => void;
}

const ArtistMessageModal = (props: ArtistMessageModalProps) => {
  const { isOpen, setIsMesaageModalOpen, artistDetail } = props;
  const [text, setText] = useState(1000);
  const [isLoading, setIsLoading] = useState(false);
  const [userAction, setUserAction] = useState<"artistMessage" | null>(null);
  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [tmpData, setTmpData] = useState<any>(null);

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<ArtistMessageInputs>({
      resolver: yupResolver(ArtistMessageSchema),
    });

  // useEffect(() => {
  //   // if (isOpen) {
  //     setIsModalOpen(isOpen);
  //     // } else {
  //     //   // showToast(SUCCESS_MESSAGES.requiredLoginMessage, "warning");
  //     // }
  //   // }
  // }, [isOpen]);

  const onSubmit = async (e: any) => {
    if (loginUserState?.isLogin) {
      setIsLoading(true);
      try {
        await artistMessageAPI(artistDetail.objectId, {
          content: e.message,
        });
        setIsLoading(false);
        showToast(SUCCESS_MESSAGES.sendArtistMessage, "success");
        setIsMesaageModalOpen(false);
      } catch (e: any) {
        setIsLoading(false);
        console.log(e);
      }
    } else {
      setIsLoginModalOpen(true);
      setUserAction("artistMessage");
      setTmpData(e);
    }
  };

  const handleChange = (e: any) => {
    let length = e.target.value;
    if (length.length <= 1000) {
      setText(1000 - length.length);
    } else {
      setText(0);
    }
  };

  const handleCancel = () => {
    setIsMesaageModalOpen(false);
  };

  return (
    <>
      <Modal
        show={isOpen}
        className={modalstyle.modal}
        onHide={handleCancel}
        size="sm"
      >
        <h2>Message {artistDetail?.name}</h2>
        <Modal.Header closeButton className={modalstyle.header}></Modal.Header>
        <Modal.Body className={`${modalstyle.body}`}>
          <Row className="mt-4" justify={"center"}>
            <Col span={24} className="">
              <form onSubmit={handleSubmit(onSubmit)}>
                <FormGroup className={`mb-3 w-100`}>
                  <TextAreaField
                    {...{
                      register,
                      formState,
                      id: "message",
                      label: "MESSAGE",
                      className: "w-100 p-2",
                      placeholder:
                        "Enter your message to the artist here.Remember to include your contact information, so the artist can respond.",
                    }}
                    onChange={(e) => handleChange(e)}
                  />
                </FormGroup>
                <div className={modalstyle.cremain}>
                  <p className="text-muted">{text} characters remaining</p>
                </div>
                <div
                  className={`${modalstyle.modal_footer}  shows-btn  panel-form`}
                >
                  <div className="request-btn">
                    <Button
                      htmlType="submit"
                      type="primary"
                      disabled={formState?.isSubmitting || isLoading}
                      loading={formState?.isSubmitting || isLoading}
                    >
                      Send Message
                    </Button>
                  </div>
                </div>
              </form>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>

      {isLoginModalOpen && userAction && (
        <LoginModal
          {...{
            actionFrom: userAction,
            handleOk: () => {
              setIsLoginModalOpen(false);
              setUserAction(null);
              onSubmit(tmpData);
            },
            isOpen: isLoginModalOpen,
            handleCancel: () => {
              setIsLoginModalOpen(false);
            },
          }}
        />
      )}
    </>
  );
};
export default ArtistMessageModal;
