/* eslint-disable @next/next/no-img-element, jsx-a11y/alt-text */
import { Button } from "@components/theme/button";
import CustomCheckbox from "@components/theme/checkbox";
import {
  FormGroup,
  InputField,
  InputPasswordField,
  InputPasswordFieldWithMessage,
  SignInPasswordField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import FacebookLogin from "@greatsumini/react-facebook-login";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { signUpAPI } from "@redux/services/auth.api";
import {
  doFacebookLogin,
  doLogin,
  FacebookLoginPayload,
  fetchLoginUser,
  LoginState,
  LoginUserState,
} from "@redux/slices/auth";
import { Col, Divider, Row } from "antd";
import { useReCaptcha } from "next-recaptcha-v3";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useCookies } from "react-cookie";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import {
  FACEBOOK_APP_ID,
  SUCCESS_MESSAGES,
  TYPES_OF_ACTION_USERS,
} from "src/libs/constants";
import useLoginStatus from "src/libs/useLoginStatus";
import {
  LoginFormInputs,
  LoginFormValidateSchema,
} from "src/schemas/loginFormSchema";
import {
  SignupFormInputs,
  SignupFormValidateSchema,
} from "src/schemas/signupFormSchema";
import LoginModalStyles from "./loginModal.module.scss";

export interface LoginModalProps {
  actionFrom:
    | "artistSubscribe"
    | "artistMessage"
    | "artistShoutouts"
    | "venueSubscribe";
  handleOk: (data: any) => void;
  handleCancel: () => void;
}

export const LoginModal = (props: LoginModalProps) => {
  const { handleOk, handleCancel, actionFrom } = props;
  const loginState: LoginState = useSelector((state: RootState) => state.login);
  const isLogin = useLoginStatus(loginState);

  const dispatch = useDispatch();
  const { executeRecaptcha } = useReCaptcha();
  const [cookies]: any = useCookies(["authApp"]);
  const [actionType, setActionType] = useState<"signin" | "signup">("signin");
  const [isLoading, setIsLoading] = useState(false);
  const [isArtist, setIsArtist] = useState(false);

  const { register, handleSubmit, formState } = useForm<LoginFormInputs>({
    resolver: yupResolver(LoginFormValidateSchema),
  });

  const SignupForm = useForm<SignupFormInputs>({
    resolver: yupResolver(SignupFormValidateSchema),
  });

  const onSubmit = async (values: any) => {
    setIsLoading(true);
    const recaptchaResponse = await executeRecaptcha("login");
    dispatch(doLogin(values, recaptchaResponse));
  };

  const onSignUpSubmit = async (data: any) => {
    setIsLoading(true);
    data["isArtist"] = isArtist ? "true" : "false";
    try {
      const recaptchaResponse = await executeRecaptcha("signup");
      await signUpAPI(data, recaptchaResponse);
      showToast(SUCCESS_MESSAGES.signUp, "success");
      SignupForm.reset();
      setIsLoading(false);
      setActionType("signin");
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  const handleFacebookLogin = async (data: FacebookLoginPayload) => {
    const recaptchaResponse = await executeRecaptcha("login");
    dispatch(doFacebookLogin(data, recaptchaResponse));
  };

  useEffect(() => {
    if (isLogin) {
      setTimeout(() => {
        handleOk(true);
      }, 1000);
    }
  }, [isLogin]);

  useEffect(() => {
    if (loginState?.data?.user && loginState?.data?.accessToken) {
      dispatch(
        fetchLoginUser({
          Authorization: `Bearer ${loginState?.data?.accessToken}`,
        })
      );
    }
  }, [loginState]);

  return (
    <Modal
      show
      className={LoginModalStyles.modal}
      onHide={handleCancel}
      size="sm"
    >
      <h2>
        {actionType === "signin" && `Sign in to JCasp`}
        {actionType === "signup" && `Get Started with JCasp`}
      </h2>
      <Modal.Header closeButton className={`${LoginModalStyles.header}`}>
        <span className="text-green">{TYPES_OF_ACTION_USERS[actionFrom]}</span>
      </Modal.Header>
      <Modal.Body className={`${LoginModalStyles.body}`}>
        {actionType === "signin" && (
          <Row className="mt-4" justify={"center"}>
            <Col span={24} className="">
              <p className="text-muted justify-center">
                Continue with facebook or enter your details
              </p>
              <div className={`${LoginModalStyles.fbSign} mt-4`}>
                <FacebookLogin
                  className="hvr-float-shadow facebook-button"
                  appId={FACEBOOK_APP_ID}
                  onSuccess={(response) => {
                    handleFacebookLogin({
                      facebookToken: response.accessToken,
                      type: "SIGNIN",
                    });
                  }}
                  onFail={(error) => {}}
                >
                  <Image
                    src="/images/fb-in.png"
                    alt="Signin with Facebook"
                    width={503}
                    height={52}
                    className="h-auto"
                  />
                </FacebookLogin>
              </div>
              <div className={`col-12 ${LoginModalStyles.orline}`}>
                <p className={LoginModalStyles.lineThrough}>or with email</p>
              </div>
            </Col>
          </Row>
        )}

        {actionType === "signin" && (
          <form onSubmit={handleSubmit(onSubmit)}>
            <FormGroup className={`mb-3`}>
              <InputField
                {...{
                  register,
                  formState,
                  id: "username",
                  label: "Email Address or Username",
                  className: `inputField`,
                  placeholder: "Enter your Email Address or Username",
                }}
              />
            </FormGroup>

            <FormGroup className={`mb-3`}>
              <SignInPasswordField
                {...{
                  register,
                  formState,
                  id: "password",
                  label: "Password",
                  className: `inputField`,
                  placeholder: "Enter your Password",
                }}
              />
            </FormGroup>

            <Row justify={"center"} className="mb-3">
              <Col>
                <Button
                  htmlType="submit"
                  loading={formState?.isSubmitting || isLoading}
                  disabled={formState?.isSubmitting || isLoading}
                  type="primary"
                >
                  Sign In
                </Button>
              </Col>
            </Row>

            <div className="text-center">
              <Link
                href="/forgot-password"
                className={`text-muted mb-2 is-link`}
              >
                Forgot Password?
              </Link>
              <p className="text-muted justify-center">
                Don&apos;t have an account?{" "}
                <span
                  className="text-dark is-link"
                  onClick={() => {
                    setActionType("signup");
                  }}
                >
                  Signup now
                </span>
              </p>
            </div>
          </form>
        )}

        {actionType === "signup" && (
          <form
            onSubmit={SignupForm.handleSubmit(onSignUpSubmit)}
            className=" mt-4"
          >
            <Row gutter={[20, 0]}>
              <Col sm={12} xs={24}>
                <FormGroup className={`mb-3`}>
                  <InputField
                    {...{
                      register: SignupForm.register,
                      formState: SignupForm.formState,
                      id: "firstName",
                      label: "First Name",
                      className: `inputField`,
                      placeholder: "Enter first name",
                      autoComplete: false,
                    }}
                  />
                </FormGroup>
              </Col>
              <Col sm={12} xs={24}>
                <FormGroup className={`mb-3`}>
                  <InputField
                    {...{
                      register: SignupForm.register,
                      formState: SignupForm.formState,
                      id: "lastName",
                      label: "Last Name",
                      className: `inputField`,
                      placeholder: "Enter last name",
                      autoComplete: false,
                    }}
                  />
                </FormGroup>
              </Col>
            </Row>

            <FormGroup className={`mb-3`}>
              <InputField
                {...{
                  register: SignupForm.register,
                  formState: SignupForm.formState,
                  id: "userName",
                  label: "Create Username",
                  className: `inputField`,
                  placeholder: "Enter username",
                  autoComplete: false,
                }}
              />
            </FormGroup>

            <FormGroup className={`mb-3`}>
              <InputField
                {...{
                  register: SignupForm.register,
                  formState: SignupForm.formState,
                  id: "email",
                  label: "Email Address",
                  className: `inputField`,
                  placeholder: "Enter Email Address",
                  autoComplete: false,
                }}
              />
            </FormGroup>

            <FormGroup className={`mb-3`}>
              <InputPasswordFieldWithMessage
                {...{
                  register: SignupForm.register,
                  formState: SignupForm.formState,
                  id: "password",
                  label: "Create Password",
                  className: `inputField`,
                  placeholder: "Enter your Password",
                  autoComplete: false,
                }}
              />
            </FormGroup>

            <FormGroup className={`mb-3`}>
              <InputPasswordField
                {...{
                  register: SignupForm.register,
                  formState: SignupForm.formState,
                  id: "confirmPassword",
                  label: "Confirm Password",
                  className: `inputField`,
                  placeholder: "Enter confirm your Password",
                  autoComplete: false,
                }}
              />
            </FormGroup>

            <FormGroup className={`mb-3`}>
              <InputField
                {...{
                  register: SignupForm.register,
                  formState: SignupForm.formState,
                  id: "referral",
                  label: "Referral/How Did You Hear About Us",
                  className: `inputField`,
                  placeholder: "Instagram, Venue, Friend, etc.",
                  autoComplete: false,
                }}
              />
            </FormGroup>

            <FormGroup className={`mb-3`}>
              <CustomCheckbox
                {...{
                  handleOnChange: (v) => {
                    setIsArtist(v);
                  },
                  label: "I am Signing Up as an Artist",
                  isBeforeLabel: true,
                  value: isArtist,
                }}
              />
            </FormGroup>

            <Row justify={"center"} className="mb-3">
              <Col>
                <Button
                  htmlType="submit"
                  loading={formState?.isSubmitting || isLoading}
                  disabled={formState?.isSubmitting || isLoading}
                  type="primary"
                >
                  Sign Up
                </Button>
              </Col>
            </Row>

            <div className="text-center">
              <p className="text-muted justify-center">
                Already have an account?{" "}
                <span
                  className="text-dark is-link"
                  onClick={() => {
                    setActionType("signin");
                  }}
                >
                  Log In
                </span>
              </p>
            </div>
          </form>
        )}

        <Divider />
      </Modal.Body>
    </Modal>
  );
};
