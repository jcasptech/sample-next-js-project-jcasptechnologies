import { Button, DefaultLoader } from "@components/theme";
import CustomCheckbox from "@components/theme/checkbox";
import {
  FormGroup,
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { EmailIcon } from "@components/theme/icons/emailIcon";
import { PhoneColorIcon } from "@components/theme/icons/phoneColorIcon";
import { PhoneIcon } from "@components/theme/icons/phoneIcon";
import CustomTimePicker from "@components/theme/timepicker";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  getArtistDetailAPI,
  updateFeaturedArtistAPI,
} from "@redux/services/artist.api";
import { getEventFullDetailAPI } from "@redux/services/calendar.api";
import { ArtistObject } from "@redux/slices/artists";
import { Col, DatePicker, Divider, Popconfirm, Row } from "antd";
import { DateTime } from "luxon";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import {
  artistScoreInputs,
  artistScoreSchema,
} from "src/schemas/artistScoreSchema";
import { AdminEventInputs, AdminEventSchema } from "src/schemas/eventSchema";
import ModalStyles from "./index.module.scss";

export interface ArtistScoreModalProps {
  artistId: string;
  handleClose: (data: boolean) => void;
}
const ArtistScoreModal = (props: ArtistScoreModalProps) => {
  const { artistId, handleClose } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [isUpdated, setIsUpdated] = useState(false);
  const [isFormLoading, setIsFormLoading] = useState(false);
  const [isOverrideScore, setIsOverrideScore] = useState(false);
  const [artistData, setArtistData] = useState<ArtistObject | null>(null);

  const { register, handleSubmit, formState, setValue, control, reset } =
    useForm<artistScoreInputs>({
      resolver: yupResolver(artistScoreSchema),
    });

  const handleCancel = () => {
    handleClose(isUpdated);
  };
  const getFullDetail = async () => {
    if (artistId) {
      try {
        const data = await getArtistDetailAPI(artistId);
        setArtistData(data);
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
      }
    }
  };

  const onSubmit = async (data: artistScoreInputs) => {
    try {
      setIsFormLoading(true);
      await updateFeaturedArtistAPI({
        ...data,
        artistId,
      });
      showToast(SUCCESS_MESSAGES.scoreUpdateSuccess, "success");
      setIsFormLoading(false);
      setIsUpdated(true);
    } catch (error) {
      console.log(error);
      setIsFormLoading(false);
    }
  };

  const isCompletedProfile = () => {
    const videosCount = artistData?.metadata?.youtubeVideoLinks?.length || 0;
    const isBiography = artistData?.metadata?.about ? true : false;
    const isSongsList =
      artistData?.metadata?.csv || artistData?.metadata?.pdf ? true : false;
    const photoCount = 2;

    return videosCount > 2 && isBiography && isSongsList && photoCount > 2
      ? "Yes"
      : "No";
  };

  useEffect(() => {
    setValue("isAdminScore", isOverrideScore);
  }, [isOverrideScore]);

  useEffect(() => {
    if (artistData) {
      setValue("score", artistData.sortPriority || 0);
      setValue("adminScore", artistData.adminScore || 0);
      setIsOverrideScore(artistData.isAdminScore || false);
    }
  }, [artistData]);

  useEffect(() => {
    if (artistId) {
      getFullDetail();
    }
  }, [artistId]);

  return (
    <>
      <Modal show className={ModalStyles.modal} onHide={handleCancel} size="lg">
        <h2> Manage Score</h2>
        {isLoading && !artistData && <DefaultLoader />}
        {!isLoading && artistData && (
          <>
            <Modal.Header closeButton className={`${ModalStyles.header}`}>
              <h3>{artistData?.name}</h3>
            </Modal.Header>
            <Modal.Body className={`${ModalStyles.body}`}>
              <Row className="mt-4">
                <Col>
                  <h3>Artist Information</h3>
                </Col>
              </Row>

              <Row className="artist-infomation">
                <Col sm={20} xs={24}>
                  <div className="artists mb-2">
                    <div>
                      <label>Artist Name: </label>
                      <p>{capitalizeString(artistData?.name)}</p>
                    </div>
                    <div>
                      <label>Artist Type: </label>
                      <p>{capitalizeString(artistData?.format)}</p>
                    </div>
                  </div>
                  {artistData?.phone && (
                    <p>
                      <PhoneIcon />
                      {artistData?.phone}
                    </p>
                  )}

                  {artistData?.email && (
                    <p>
                      <EmailIcon />
                      {artistData?.email}
                    </p>
                  )}
                </Col>
                <Col sm={4} xs={24}>
                  {artistData?.iconImage?.url && (
                    <Image
                      className="profile-image"
                      height={120}
                      width={120}
                      src={
                        artistData?.iconImage?.url ||
                        "/images/artist-placeholder.png"
                      }
                      alt={artistData.name}
                    />
                  )}
                </Col>
              </Row>

              <Row className="mt-3">
                <Col>
                  <h3>Score Information</h3>
                </Col>
              </Row>

              <form
                onSubmit={handleSubmit(onSubmit)}
                className="score-information"
              >
                <Row align={"top"} gutter={[20, 20]}>
                  <Col sm={12} xs={24}>
                    <InputField
                      {...{
                        register,
                        formState,
                        className: "inputField",
                        id: "score",
                        placeholder: "Enter Score",
                        type: "number",
                        label: "Current Score",
                        readOnly: true,
                      }}
                    />
                    <div className="mt-2">
                      <CustomCheckbox
                        {...{
                          handleOnChange: (d) => {
                            setIsOverrideScore(d);
                          },
                          label: "Want to override score?",
                          isBeforeLabel: true,
                          value: isOverrideScore,
                        }}
                      />
                    </div>

                    {isOverrideScore && (
                      <div className="mt-2">
                        <InputField
                          {...{
                            register,
                            formState,
                            className: "inputField",
                            id: "adminScore",
                            placeholder: "Enter Score",
                            type: "number",
                            label: "Admin Score",
                          }}
                        />
                      </div>
                    )}
                  </Col>
                  <Col sm={12} xs={24}>
                    <div>
                      <label>
                        Completed Profile?{" "}
                        <span>
                          {artistData?.isProfileComplete ? "Yes" : "No"}
                        </span>{" "}
                      </label>
                    </div>
                    <div>
                      <label>
                        Number of Subscribers:{" "}
                        <span>{artistData?.fanCount || 0}</span>{" "}
                      </label>
                    </div>
                    <div>
                      <label>
                        Last Month Show Count:{" "}
                        <span>{artistData?.lastMonthShowsCount || 0}</span>
                      </label>
                    </div>
                    <div>
                      <label>
                        Check-in Count:{" "}
                        <span>{artistData?.checkinCount || 0}</span>
                      </label>
                    </div>
                  </Col>

                  <Col span={24}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={formState.isLoading || isFormLoading}
                      disabled={formState.isLoading || isFormLoading}
                    >
                      Save Score
                    </Button>
                  </Col>
                </Row>
              </form>

              <Divider />
            </Modal.Body>
          </>
        )}
      </Modal>
    </>
  );
};
export default ArtistScoreModal;
