import { showToast } from "@components/ToastContainer";
import { postArtistMessageApproveRejectAPI } from "@redux/services/artist.api";
import { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import modalstyle from "./approveModal.module.scss";

export interface ArtistMessageApprovedModalProps {
  isApprovedModal: boolean;
  setIsApprovedModal: (d: any) => void;
  messageId: string;
  handleClose: (d: any) => void;
}
const ArtistMessageApprovedModal = (props: ArtistMessageApprovedModalProps) => {
  const { isApprovedModal, setIsApprovedModal, messageId, handleClose } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsApprovedModal(false);
  };

  const handleApproved = async (status: any) => {
    if (messageId) {
      try {
        await postArtistMessageApproveRejectAPI({
          messageId: messageId,
          status: status,
        });
        handleClose(status);
        showToast(SUCCESS_MESSAGES.messageApprovedSuccess, "success");
        setIsApprovedModal(false);
      } catch (error) {
        console.log(error);
      }
    }
  };
  return (
    <>
      <Modal
        className={`modal fade approve-reject ${modalstyle.main_modal}`}
        show={isApprovedModal}
        onHide={handleCancel}
      >
        <Modal.Body className={modalstyle.main_body}>
          <div className={modalstyle.main_body}>
            <div className={`${modalstyle.modalContentApprove} modal-content`}>
              <div className="modal-body">
                Are you sure! you want to approve ?
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary cancel"
                  data-dismiss="modal"
                  onClick={() => handleCancel()}
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="btn btn-success approve-btn-2"
                  onClick={() => handleApproved("confirmed")}
                >
                  Yes, Approve
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ArtistMessageApprovedModal;
