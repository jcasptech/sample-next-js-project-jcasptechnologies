import { Button, DefaultLoader } from "@components/theme";
import { TextAreaField } from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  getPostingFullDetailAPI,
  submitGigAPI,
} from "@redux/services/calendar.api";
import { Col, Row } from "antd";
import { DateTime } from "luxon";
import Image from "next/image";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import { PostingInputs, PostingSchema } from "src/schemas/postingSchema";
import CalendrPostModalStyles from "./calendarPostModal.module.scss";

export interface CaledarPostModalProps {
  isOpen: boolean;
  id: string | null;
  artistId: string;
  handleClose: (data: any) => void;
  setIsPostingModalOpen: (data: any) => void;
  isSubmitted?: boolean;
}
const CalendarPostModal = (props: CaledarPostModalProps) => {
  const {
    isOpen,
    id,
    setIsPostingModalOpen,
    handleClose,
    artistId,
    isSubmitted,
  } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [postData, setPostData] = useState<any>();
  const [isFormLoading, setIsFormLoading] = useState(false);

  const { register, handleSubmit, formState, control, reset } =
    useForm<PostingInputs>({
      resolver: yupResolver(PostingSchema),
    });

  const handleCancel = () => {
    setIsPostingModalOpen(false);
  };

  const getFullDetail = async () => {
    if (id) {
      setIsLoading(true);
      try {
        const data = await getPostingFullDetailAPI(id);
        setPostData(data);
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
      }
    }
  };

  const onSubmit = async (data: any) => {
    setIsFormLoading(true);
    try {
      data["postId"] = id;
      data["artistId"] = artistId;
      await submitGigAPI(data);
      showToast(SUCCESS_MESSAGES.submitGig, "success");
      reset();
      handleClose(true);
      setIsPostingModalOpen(false);
      setIsFormLoading(false);
    } catch (error) {
      setIsFormLoading(false);
    }
  };

  const getFormatSubmittedArtist = () => {
    let format = "<5";
    if (postData?.submittedArtists?.length > 0) {
      if (postData?.submittedArtists?.length < 5) {
        format = "<5";
      } else if (
        postData?.submittedArtists?.length >= 5 &&
        postData?.submittedArtists?.length < 10
      ) {
        format = ">5";
      } else if (
        postData?.submittedArtists?.length >= 10 &&
        postData?.submittedArtists?.length < 50
      ) {
        format = ">10";
      } else if (
        postData?.submittedArtists?.length >= 50 &&
        postData?.submittedArtists?.length < 100
      ) {
        format = "<50";
      } else {
        format = ">50";
      }
    }
    return format;
  };

  useEffect(() => {
    if (id) {
      getFullDetail();
    }
  }, [id]);

  return (
    <>
      <Modal
        show={isOpen}
        onHide={handleCancel}
        size="lg"
        className={CalendrPostModalStyles.modal}
      >
        <h2>Open Gig Posting</h2>
        <Modal.Header
          closeButton
          className={`${CalendrPostModalStyles.header}`}
        >
          <h3>
            {postData?.title}
            <p>
              {postData?.startDate && postData?.startDate?.iso && (
                <>
                  {DateTime.fromISO(postData?.startDate?.iso)
                    .setZone(postData?.timeZone)
                    .toFormat("EEEE, MMMM d, yyyy")}{" "}
                  from{" "}
                  {DateTime.fromISO(postData?.startDate?.iso)
                    .setZone(postData?.timeZone)
                    .toFormat("h:mm a")}{" "}
                  -{" "}
                  {DateTime.fromISO(postData?.endDate?.iso)
                    .setZone(postData?.timeZone)
                    .toFormat("h:mm a")}
                </>
              )}
            </p>
          </h3>
        </Modal.Header>
        <Modal.Body className={`${CalendrPostModalStyles.body}`}>
          {isLoading && <DefaultLoader />}
          {!isLoading && (
            <>
              <Row>
                <Col>
                  <h3>About the event</h3>
                </Col>
              </Row>

              <Row className="event-infomation  mb-4">
                <Col span={24}>
                  <p>{postData?.note}</p>
                </Col>
              </Row>

              <Row className="event-details">
                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/location.png"
                      alt="location"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Location
                  </div>
                  <div>
                    {postData?.city}{" "}
                    {postData?.zipcode ? `, ${postData?.zipcode}` : ""}
                  </div>
                </Col>
                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/calendar.png"
                      alt="Date and Time"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Date and Time
                  </div>
                  <div>
                    {DateTime.fromISO(postData?.startDate?.iso)
                      .setZone(postData?.timeZone)
                      .toFormat("EEEE, MMMM d, yyyy")}{" "}
                    from{" "}
                    {DateTime.fromISO(postData?.startDate?.iso)
                      .setZone(postData?.timeZone)
                      .toFormat("h:mm a")}{" "}
                    -{" "}
                    {DateTime.fromISO(postData?.endDate?.iso)
                      .setZone(postData?.timeZone)
                      .toFormat("h:mm a")}
                  </div>
                </Col>

                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/types-of-events.png"
                      alt="Type of event"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Type of event
                  </div>
                  <div>{capitalizeString(postData?.variety)}</div>
                </Col>

                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/users.png"
                      alt="Type of artist wanted"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Type of artist wanted
                  </div>
                  <div>{capitalizeString(postData?.genres?.join(", "))}</div>
                </Col>

                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/users.png"
                      alt="Expected attendance"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Expected attendance
                  </div>
                  <div>
                    {postData?.attendanceCategory === "min" && "<50"}
                    {postData?.attendanceCategory === "small" && "50-100"}
                    {postData?.attendanceCategory === "medium" && "100-200"}
                    {postData?.attendanceCategory === "max" && ">200"}
                  </div>
                </Col>

                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/indoor-outdoor.png"
                      alt="Indoor/Outdoor"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Indoor/Outdoor
                  </div>
                  <div>{capitalizeString(postData?.playSetting)}</div>
                </Col>

                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/equipment.png"
                      alt="Should the artist bring their own PA system?"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Should the artist bring their own PA system?
                  </div>
                  <div>
                    {postData?.equipmentProvided === true ? "Yes" : "No"}
                  </div>
                </Col>

                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/users.png"
                      alt="Total Artist submitted"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Total Artist submitted
                  </div>
                  <div>{getFormatSubmittedArtist()}</div>
                </Col>

                <Col span={24} className="detail">
                  <div className="label">
                    <Image
                      src="/images/general/tag.png"
                      alt="Proceeds"
                      height={26}
                      width={26}
                      className="h-auto"
                    />
                    Proceeds
                  </div>
                  <div>${postData?.artistProceeds || 0}</div>
                </Col>
              </Row>

              {!isSubmitted && (
                <>
                  <Row className="mt-4">
                    <Col>
                      <h3>Submit to Posting</h3>
                    </Col>
                  </Row>
                  <Row className="">
                    <Col span={24}>
                      <form onSubmit={handleSubmit(onSubmit)}>
                        <TextAreaField
                          {...{
                            register,
                            formState,
                            className: "notes",
                            id: "note",
                            defaultValue: "",
                            placeholder:
                              "Let the client know why you'd be a good fit for this event",
                          }}
                        />

                        <Button
                          htmlType="submit"
                          type="primary"
                          disabled={isFormLoading || formState?.isSubmitting}
                          loading={isFormLoading || formState?.isSubmitting}
                        >
                          Submit
                        </Button>
                        <p className="button-after-content">
                          {getFormatSubmittedArtist()} artists submitted
                        </p>
                      </form>
                    </Col>
                  </Row>
                </>
              )}
            </>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default CalendarPostModal;
