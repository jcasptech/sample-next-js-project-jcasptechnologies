import { Button } from "@components/theme/button";
import {
  FormGroup,
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { artistClaimAPI } from "@redux/services/artist.api";
import { ArtistObject } from "@redux/slices/artists";
import { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { ClaimArtistInputs } from "src/schemas/claimArtistFormValidationSchema";
import { ClaimArtistSchema } from "src/schemas/claimArtistSchema";
import modalstyle from "./artistClaimStyle.module.scss";

export interface ArtistClaimModalProps {
  artistDetail: ArtistObject;
  onOk: (d: any) => void;
}

const ArtistClaimModal = (props: ArtistClaimModalProps) => {
  const { artistDetail, onOk } = props;
  const [isLoading, setIsLoading] = useState(false);
  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<ClaimArtistInputs>({
      resolver: yupResolver(ClaimArtistSchema),
    });

  const onSubmit = async (data: any) => {
    setIsLoading(true);
    try {
      data.artistId = artistDetail?.objectId;
      await artistClaimAPI(data);
      setIsLoading(false);
      showToast(SUCCESS_MESSAGES.ClaimArtistSuccess, "success");
      onOk(true);
    } catch (e: any) {
      setIsLoading(false);
      console.log(e);
    }
  };

  return (
    <>
      <Modal className={modalstyle.modal} show={true} size="lg">
        <Modal.Header closeButton className={modalstyle.header}>
          <h2 className="modal-title modal-e text-center" id="inputModalLabel">
            This artist&apos;s profile is private or unclaimed.
          </h2>
        </Modal.Header>
        <Modal.Body>
          <div className={`${modalstyle.modalBody}`} role="document">
            <div
              className="p-y-2 modal-lg modal-aca"
              style={{ border: "none" }}
            >
              <p className="custom-text-muted text-center">
                Please send a message to reach the artist or claim this artist
                profile
              </p>
              <p className="custom-text-muted text-center">
                Are you this artist? Please let us know so we can transfer
                ownership to you. JCasp is a free platform for artists, owned
                and operated by artists. There are no signup fees, we will not
                ask you for money.
              </p>
              <div className="modal-body m-x-auto">
                <form className="formB" onSubmit={handleSubmit(onSubmit)}>
                  <div className="fieldH ">
                    <FormGroup>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "name",
                          label: "Name",
                          className: `input`,
                          placeholder: "Enter your name",
                        }}
                      />
                    </FormGroup>
                  </div>

                  <div className="fieldH ">
                    <FormGroup>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "phone",
                          label: "Phone",
                          className: `input`,
                          placeholder: "Enter your phone",
                        }}
                      />
                    </FormGroup>
                  </div>

                  <div className="fieldH ">
                    <FormGroup>
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "email",
                          label: "Email",
                          className: `input`,
                          placeholder: "Enter your email address",
                        }}
                      />
                    </FormGroup>
                  </div>

                  <div className="fieldH ">
                    <FormGroup>
                      <TextAreaField
                        {...{
                          register,
                          formState,
                          id: "message",
                          label: "Message",
                          className: `input`,
                          placeholder: "Enter your message",
                        }}
                      />
                    </FormGroup>
                  </div>

                  <div
                    className={`${modalstyle.modal_footer}  shows-btn  panel-form`}
                  >
                    <div className="request-btn">
                      <Button
                        htmlType="submit"
                        type="primary"
                        disabled={formState?.isSubmitting || isLoading}
                        loading={formState?.isSubmitting || isLoading}
                      >
                        Claim Artist Profile
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ArtistClaimModal;
