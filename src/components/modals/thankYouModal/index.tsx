/* eslint-disable @next/next/no-img-element, jsx-a11y/alt-text */
import { Button } from "@components/theme";
import { Modal } from "antd";
import React from "react";
import ThankYouModalStyles from "./thankYouModal.module.scss";

export interface thankYouModalProps {
  isOpen: boolean;
  handleOk: (data: any) => void;
}

export const ThankYouModal = (props: thankYouModalProps) => {
  const { isOpen, handleOk } = props;

  return (
    <Modal
      className={ThankYouModalStyles.thankyou}
      visible={isOpen}
      centered
      width={700}
      onOk={handleOk}
      footer={[
        <Button htmlType="button" onClick={handleOk} key={0}>
          Okay
        </Button>,
      ]}
    >
      <form>
        <div className=" flex justify-center mt-4 mb-3">
          <img src="/images/thank-you.svg" />
        </div>
        <h1 className="text-4xl font-semibold text-center mb-3">
          Thank you for your feedback
        </h1>
        <p className="text-center f-16">
          Your feedback makes Virtual a better place for other patients.
        </p>
      </form>
    </Modal>
  );
};
