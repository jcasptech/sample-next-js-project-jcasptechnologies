import Spinner from "@components/theme/spinner";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { generalSearchArtistsAPI } from "@redux/services/general.api";
import { createAdminShowsAPI } from "@redux/services/shows.api";
import { AdminVenuesState } from "@redux/slices/adminVenues";
import { LoginUserState } from "@redux/slices/auth";
import { Col, DatePicker, Row, TimePicker } from "antd";
import { DateTime } from "luxon";
import moment from "moment";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import { deepClone } from "src/libs/helpers";
import useList from "src/libs/useList";
import {
  AdminAddShowsFormValidationProps,
  AdminAddShowsFormValidationSchema,
} from "src/schemas/adminAddShowFormSchema";
import modalStyles from "./artistAddShow.module.scss";

export interface AdminAddShowModalProps {
  isOpen: boolean;
  artistId: string | null;
  handleClose: (data: any) => void;
  setIsNewShowModalOpen: (data: any) => void;
}

const VenueAdminAddShowModal = (props: AdminAddShowModalProps) => {
  const { isOpen, setIsNewShowModalOpen, artistId, handleClose } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [venueOptions, setVenueOptions] = useState<any>([]);
  const [venueId, setVenueId] = useState("");
  const [isShowLoading, setIsShowModalOpen] = useState(false);
  const [googlePlaceId, setGooglePlaceId] = useState("");
  const [venueData, setVenueData] = useState<any>({
    place: "",
    place_id: "",
  });
  const [venueAddress, setVenueAddress] = useState({
    value: "",
    label: "",
  });
  const [showDetails, setShowDetails] = useState<any>([
    {
      date: "",
      startTime: "",
      artistId: "",
      artistName: "",
    },
  ]);

  const [artistData, setArtistData] = useState<any>([]);
  const dispatch = useDispatch();
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<AdminAddShowsFormValidationProps>({
      resolver: yupResolver(AdminAddShowsFormValidationSchema),
    });

  useEffect(() => {
    if (isOpen) {
      setIsModalOpen(isOpen);
    }
  }, [isOpen]);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsNewShowModalOpen(false);
  };

  const {
    adminVenues: { isLoading, data: venuesData },
  }: {
    adminVenues: AdminVenuesState;
  } = useSelector((state: RootState) => ({
    adminVenues: state.adminVenues,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 9,
      skip: 0,
      search: "",
    },
  });

  const getData = () => {
    if (apiParam) {
      if (loginUserState?.data?.user?.role === LOGIN_USER_TYPE.VENUE_ADMIN) {
        // dispatch(
        //   fetchVenueAdminVenues(loginUserState?.data?.user?.objectId, apiParam)
        // );
      }
    }
  };

  let getAllArtistList = async () => {
    if (apiParam) {
      delete apiParam.take;
      delete apiParam.skip;
      try {
        let res = await generalSearchArtistsAPI(apiParam);
        setArtistData(res);
      } catch (error) {
        console.log(error, "error");
      }
    }
  };

  useEffect(() => {
    if (apiParam) {
      getData();
      getAllArtistList();
    }
  }, [apiParam]);

  const handleVenueChange = (data: any) => {
    if (data) {
      apiParam.search = data;
      getData();
    } else {
      apiParam.search = "";
    }
  };

  useEffect(() => {
    if (venuesData && venuesData?.list?.length) {
      const newData: any = [];
      venuesData?.list?.map((venue: any, index: number) => {
        newData.push({
          value: venue.objectId,
          label: venue.name,
        });
        setVenueOptions(newData);
      });
    }
  }, [venuesData]);

  useEffect(() => {
    if (showDetails) {
      setValue(`showDetails` as any, undefined);
      showDetails.map((item: any, index: any) => {
        setValue(`showDetails[${index}].startTime` as any, item.startTime);
        setValue(`showDetails[${index}].date` as any, item.date);
        setValue(`showDetails[${index}].artistId` as any, item.artistId);
      });
    }
  }, [showDetails]);

  const handleChangeDate = (e: any, index: number) => {
    const formatDate = moment(e);
    const date = formatDate.format("YYYY-MM-DD");
    const updatedItems = [...showDetails];
    updatedItems[index] = { ...updatedItems[index], date: date };
    setShowDetails(updatedItems);
  };

  const handleChangeTime = (e: any, index: any) => {
    const formattedTime = moment(e).format("hh:mm A").toString();
    const updatedItems = [...showDetails];
    updatedItems[index] = { ...updatedItems[index], startTime: formattedTime };
    setShowDetails(updatedItems);
  };

  const handleChange = (data: any) => {
    setVenueId(data.value);
    setValue("venueSelect", data.value);
  };

  const addNewShow = () => {
    const tmpData = deepClone(showDetails);
    tmpData.push({
      date: tmpData[tmpData.length - 1].date || "",
      startTime: tmpData[tmpData.length - 1].startTime || "",
      artistId: "",
      artistName: "",
    });
    setShowDetails(tmpData);
  };

  const removeShow = (key: any) => {
    const tmpData = deepClone(showDetails);
    const tempData = tmpData.filter((item: any, index: any) => index != key);
    setShowDetails(tempData);
  };

  const handleArtistChange = (data: any, index: number) => {
    const updatedItems = [...showDetails];
    updatedItems[index] = {
      ...updatedItems[index],
      artistId: data.value,
      artistName: data.label,
    };
    setShowDetails(updatedItems);
  };

  const handleChangeArtist = (d: any, index: any) => {
    if (d) {
      apiParam.search = d;
      getAllArtistList();
    }
  };

  const onSubmit = async (data: any) => {
    if (venueId) {
      data["venueId"] = venueId;
    }
    if (googlePlaceId) {
      data["googlePlaceId"] = googlePlaceId;
    }
    delete data.venueSelect;
    if (data) {
      setIsShowModalOpen(true);
      try {
        let res = await createAdminShowsAPI(data);
        setIsShowModalOpen(false);
        setIsNewShowModalOpen(false);
        setIsShowModalOpen(false);
        handleClose(true);
        showToast(SUCCESS_MESSAGES.showAddedSuccess, "success");
      } catch (error) {
        setIsModalOpen(false);
        setIsShowModalOpen(false);
        setIsNewShowModalOpen(false);
      }
    }
  };

  const disabledDatePast = (current: moment.Moment) => {
    const currentDate = DateTime.fromISO(current.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
      })
      .toUnixInteger();

    const currentWithTimezone = DateTime.fromISO(
      DateTime.utc()
        .set({
          hour: 0,
          minute: 0,
          second: 0,
        })
        .toISO({ includeOffset: false })
    ).toUnixInteger();

    return currentDate.valueOf() < currentWithTimezone.valueOf();
  };

  return (
    <>
      <Modal
        show={isModalOpen}
        className={modalStyles.modal}
        onHide={handleCancel}
      >
        <Modal.Header closeButton className={modalStyles.header}>
          <h2 className="modal-title modal-e text-center" id="inputModalLabel">
            Add New Show
          </h2>
        </Modal.Header>
        <Modal.Body className={modalStyles.modal_body}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Row>
              <Col span={10}>
                <div className={`${modalStyles.venueSelectlabel} `}>
                  <label>Venue</label>
                </div>
              </Col>
              <Col span={14}>
                <div className={`venueSearch`}></div>
              </Col>
            </Row>

            <div className={modalStyles.venueSelect}>
              {venueAddress.value === "" && (
                <>
                  <Select
                    className={`basic_multi_select selectField`}
                    classNamePrefix="Enter venue name to search"
                    placeholder="Enter venue name to search"
                    options={venueOptions}
                    onInputChange={handleVenueChange}
                    onChange={handleChange}
                    instanceId="venueName"
                  />

                  {formState &&
                    formState?.errors &&
                    formState?.errors["venueSelect"] &&
                    formState?.errors["venueSelect"].message && (
                      <span className="ant-typography ant-typography-danger text-danger block mt-1">
                        {formState.errors["venueSelect"].message}
                      </span>
                    )}
                </>
              )}
            </div>
            {showDetails?.map((form: any, index: any) => (
              <div key={index} className={index !== 0 ? "dividar mt-3" : ""}>
                <div id="dynamicRows" className="mt-3">
                  <div
                    className="row shows-m-bottom custom-rows  custom-rows"
                    id="row_1"
                  >
                    <div className="form-group col-md-4 col-sm-12">
                      <div className="row shows-venue-pad">
                        <div className="formB col-md-12 col-12">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label>Artist</label>
                            <div className="">
                              <Select
                                className={`basic_multi_select selectField`}
                                classNamePrefix="Select a Artist"
                                placeholder="Search & Select a Artist"
                                onInputChange={(e) =>
                                  handleChangeArtist(e, index)
                                }
                                onChange={(d) => handleArtistChange(d, index)}
                                options={artistData}
                                instanceId="artist"
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]
                                  ?.artistId?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.artistId?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group col-md-4 col-sm-12">
                      <div className="row shows-venue-pad">
                        <div className="formB col-md-12 col-12">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label>Dates</label>
                            <div className="">
                              <DatePicker
                                id={`showDetails[${index}].date`}
                                className={modalStyles.datePicker}
                                placeholder="Select Date"
                                onChange={(e) => handleChangeDate(e, index)}
                                value={form.date && moment(form.date)}
                                disabledDate={(currentDate) => {
                                  return disabledDatePast(currentDate);
                                }}
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]?.date
                                  ?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.date?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group col-md-4 col-sm-12">
                      <div className="row shows-venue-pad align-center">
                        <div className="shows-venue formB col-md-8 col-8">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label htmlFor="time">Start Time</label>
                            <div className="timePiker">
                              <TimePicker
                                format="hh:mm A"
                                placeholder="Select time"
                                id={`showDetails[${index}].date`}
                                className={modalStyles.timePicker}
                                onChange={(e) => handleChangeTime(e, index)}
                                value={
                                  form.startTime &&
                                  moment(form.startTime, "hh:mm A")
                                }
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]
                                  ?.startTime?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.startTime?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}

            <div className="modal-footer shows-btn mt-5">
              <button
                type="button"
                className="btn hvr-float-shadow btn-secondary cancel"
                data-bs-dismiss="modal"
                onClick={handleCancel}
              >
                Close
              </button>
              <div className={modalStyles.request_btn}>
                <button
                  type="submit"
                  className="hvr-float-shadow action-button link-button"
                  value="Save"
                >
                  {isShowLoading && <Spinner />}
                  Save
                </button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default VenueAdminAddShowModal;
