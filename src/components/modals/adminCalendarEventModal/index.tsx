import { DeleteFilled } from "@ant-design/icons";
import { Button, DefaultLoader } from "@components/theme";
import {
  FormGroup,
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { DeleteIcon } from "@components/theme/icons/deleteIcon";
import { EmailIcon } from "@components/theme/icons/emailIcon";
import { PhoneColorIcon } from "@components/theme/icons/phoneColorIcon";
import { PhoneIcon } from "@components/theme/icons/phoneIcon";
import CustomTimePicker from "@components/theme/timepicker";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import {
  cancelEventAPI,
  createEventBookingNoteAPI,
  deleteNotes,
  getEventBookingNoteAPI,
  getEventFullDetailAPI,
  sendExpeditedPayment,
  updateAdminCalendarEventAPI,
} from "@redux/services/calendar.api";
import {
  getExportEventDetailsAPI,
  getInvoiceData,
  getUcomingScheduleAPI,
} from "@redux/services/export.api";
import { LoginUserState } from "@redux/slices/auth";
import { Col, DatePicker, Divider, Popconfirm, Row, TimePicker } from "antd";
import { DateTime } from "luxon";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import useList from "src/libs/useList";
import { AdminEventInputs, AdminEventSchema } from "src/schemas/eventSchema";
import Swal from "sweetalert2";
import EventModalStyles from "./eventModal.module.scss";

export interface AdminCalendarModalProps {
  isOpen: boolean;
  id: string | null;
  setIsEventModalOpen: (data: any) => void;
  handleClose: (data: any) => void;
  setIsExportLoading: (data: any) => void;
}
const AdminCalendarEventModal = (props: AdminCalendarModalProps) => {
  const { isOpen, id, setIsEventModalOpen, handleClose, setIsExportLoading } =
    props;
  const [isLoading, setIsLoading] = useState(true);
  const [deleteNoteIsLoading, setDeleteNoteIsIsLoading] = useState(false);
  const [isFormLoading, setIsFormLoading] = useState(false);
  const [showExpeditedPayment, setShowExpeditedPayment] = useState(true);
  const [eventData, setEventData] = useState<any>();
  const [paymentPreferance, setPaymentPreferance] = useState<
    "paypal" | "venmo" | ""
  >("");
  const [newPaymentPre, setNewPaymentPre] = useState("");
  const [date, setDate] = useState("");
  const [selectedStart, setSelectedStart] = useState<any>();
  const [selectedEnd, setSelectedEnd] = useState<any>();

  const [isTitle, setIsTitle] = useState(false);
  const [isDatetime, setIsDateTime] = useState(false);
  const [updateTitle, setUpdateTitle] = useState("");

  const [noteData, setNoteData] = useState<any>();

  const [isNoteUpdate, setIsNoteUpdate] = useState(false);
  const [isChangedSection, setIsChangedSection] = useState<
    "title" | "datetime" | "proceed" | "total" | null
  >(null);
  const [isEventDataUpdating, setIsEventDataUpdating] = useState(false);
  const [eventEditableData, setEventEditableData] = useState<{
    title: string;
    date: string;
    startTime: string;
    endTime: string;
    proceeds: number;
    total: number;
  }>({
    title: "",
    date: "",
    startTime: "",
    endTime: "",
    proceeds: 0,
    total: 0,
  });

  const { register, handleSubmit, formState, setValue, control, reset } =
    useForm<AdminEventInputs>({
      resolver: yupResolver(AdminEventSchema),
    });

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );
  const handleCancel = () => {
    setIsEventModalOpen(false);
    handleClose(true);
  };

  const getFullDetail = async () => {
    if (id) {
      try {
        const data = await getEventFullDetailAPI(id);
        const notData = await getEventBookingNoteAPI(id);
        setEventData(data);
        setNoteData(notData);
        if (data?.requestExpeditedPayment === true) {
          setShowExpeditedPayment(true);
        } else {
          setShowExpeditedPayment(false);
        }
        setPaymentPreferance(data?.artist?.banking?.paymentPreference || "");
        setNewPaymentPre(data?.artist?.banking?.paymentPreference || "");
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
      }
    }
  };

  const onSubmitEventData = async () => {
    setIsEventDataUpdating(true);
    try {
      await updateAdminCalendarEventAPI(id, {
        title: eventEditableData.title,
        totalCost: eventEditableData.total,
        startTime: DateTime.fromFormat(
          eventEditableData.startTime,
          "HH:mm:ss"
        ).toFormat("hh:mm a"),
        endTime: DateTime.fromFormat(
          eventEditableData.endTime,
          "HH:mm:ss"
        ).toFormat("hh:mm a"),
        artistProceeds: eventEditableData.proceeds,
        bookingDate: eventEditableData.date,
      });
      showToast(SUCCESS_MESSAGES.eventUpdateSuccess, "success");
      setIsEventDataUpdating(false);
      setIsChangedSection(null);
    } catch (error) {
      setIsEventDataUpdating(false);
    }
  };

  useEffect(() => {
    if (eventData) {
      setEventEditableData({
        title: eventData?.title,
        date: DateTime.fromISO(eventData?.startDate?.iso)
          .setZone(eventData?.timeZone)
          .toFormat("yyyy-M-d"),
        startTime: DateTime.fromISO(eventData?.startDate?.iso)
          .setZone(eventData?.timeZone)
          .toFormat("HH:mm:ss"),
        endTime: DateTime.fromISO(eventData?.endDate?.iso)
          .setZone(eventData?.timeZone)
          .toFormat("HH:mm:ss"),
        proceeds: eventData?.artistProceeds || 0,
        total: eventData?.totalCost || 0,
      });
    }
  }, [eventData]);

  const handleRequestExpeditedPayment = async () => {
    if (id) {
      setIsFormLoading(true);
      try {
        await sendExpeditedPayment(id);
        showToast(SUCCESS_MESSAGES.expeditedPayment, "success");
        setIsEventModalOpen(false);
        setShowExpeditedPayment(false);
        setIsFormLoading(false);
      } catch (error: any) {
        setIsFormLoading(false);
      }
    }
  };

  useEffect(() => {
    if (id) {
      getFullDetail();
    }
  }, [id]);

  const onSubmit = async (data: AdminEventInputs) => {
    if (data) {
      setIsFormLoading(true);
      try {
        await createEventBookingNoteAPI({
          eventId: id,
          note: data.note,
        });
        showToast(SUCCESS_MESSAGES.eventUpdateSuccess, "success");
        setIsFormLoading(false);
        getFullDetail();
        setIsNoteUpdate(false);
        reset();
      } catch (error) {
        handleClose(true);
        handleCancel();
        setIsFormLoading(false);
      }
    }
  };

  const disabledDatePast = (current: moment.Moment) => {
    const currentDate = DateTime.fromISO(current.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
      })
      .toUnixInteger();

    const currentWithTimezone = DateTime.fromISO(
      DateTime.utc()
        .set({
          hour: 0,
          minute: 0,
          second: 0,
        })
        .toISO({ includeOffset: false })
    ).toUnixInteger();

    return currentDate.valueOf() < currentWithTimezone.valueOf();
  };

  const handleSweetalert = async () => {
    Swal.fire({
      title: "Are you sure you want to request expedited payment?",
      text: "The expedited payment is 10% fee.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.isConfirmed) {
        handleRequestExpeditedPayment();
      }
    });
  };

  const { apiParam } = useList({
    queryParams: {
      eventId: id || "",
      venueId: eventData?.venue?.objectId || "",
      range: "",
      userEmail: loginUserState?.data?.user?.email,
    },
  });

  const handleExportData = (data: any) => {
    apiParam.range = data;
    apiParam.venueId = eventData?.venue?.objectId;
    handleCurrentInvoice();
  };

  const handleCurrentInvoice = async () => {
    delete apiParam.userEmail;
    delete apiParam.eventId;
    if (id && apiParam.range) {
      setIsExportLoading(true);
      try {
        await getInvoiceData(apiParam);
        showToast(SUCCESS_MESSAGES.exportInvoice, "success");
        setIsExportLoading(false);
      } catch (error) {
        console.log(error);
        setIsExportLoading(false);
      }
    }
  };

  const handleExportEventDetails = async () => {
    delete apiParam.venueId;
    delete apiParam.range;
    apiParam["eventId"] = id || "";
    if (id && apiParam.userEmail) {
      setIsExportLoading(true);
      try {
        await getExportEventDetailsAPI(apiParam);
        showToast(SUCCESS_MESSAGES.exportEvent, "success");
        setIsExportLoading(false);
      } catch (error) {
        setIsExportLoading(false);
      }
    }
  };

  const handleExportSchedule = async () => {
    delete apiParam.eventId;
    if (id && apiParam.userEmail) {
      setIsExportLoading(true);
      try {
        await getUcomingScheduleAPI(apiParam);
        showToast(SUCCESS_MESSAGES.exportSchedule, "success");
        setIsExportLoading(false);
      } catch (error) {
        console.log(error);
        setIsExportLoading(false);
      }
    }
  };

  const handleCancelEvent = async () => {
    if (id) {
      try {
        await cancelEventAPI(id);
        showToast(SUCCESS_MESSAGES.eventCancelledSuccess, "success");
        handleClose(true);
        setIsEventModalOpen(false);
      } catch (error) {
        console.log(error);
        setIsExportLoading(false);
      }
    }
  };

  const handleDeleteNote = async (data: any) => {
    if (data) {
      setDeleteNoteIsIsLoading(true);
      try {
        await deleteNotes(data);
        showToast(SUCCESS_MESSAGES.deleteNotes, "success");
        setDeleteNoteIsIsLoading(false);
        getFullDetail();
      } catch (err: any) {
        setDeleteNoteIsIsLoading(false);
      }
    }
  };

  return (
    <>
      <Modal
        show={isOpen}
        className={EventModalStyles.modal}
        onHide={handleCancel}
        size="lg"
      >
        <h2> JCasp PRO Booking</h2>
        {isLoading && <DefaultLoader />}
        {!isLoading && (
          <>
            <Modal.Header
              closeButton
              className={`${EventModalStyles.header} ${eventData?.bookingStatus}`}
            >
              <h3>
                {eventData?.title}
                <p>
                  {eventData?.startDate && eventData?.startDate?.iso && (
                    <>
                      {DateTime.fromISO(eventData?.startDate?.iso)
                        .setZone(eventData?.timeZone)
                        .toFormat("EEEE, MMMM d, yyyy")}{" "}
                      from{" "}
                      {DateTime.fromISO(eventData?.startDate?.iso)
                        .setZone(eventData?.timeZone)
                        .toFormat("h:mm a")}{" "}
                      -{" "}
                      {DateTime.fromISO(eventData?.endDate?.iso)
                        .setZone(eventData?.timeZone)
                        .toFormat("h:mm a")}
                    </>
                  )}
                </p>
              </h3>
            </Modal.Header>
            <Modal.Body className={`${EventModalStyles.body}`}>
              <>
                {eventData?.bookingStatus === "confirmed" && (
                  <div className="headerPart">
                    <Row align="middle" gutter={[10, 10]}>
                      <Col md={isChangedSection === "title" ? 21 : 24} xs={24}>
                        <FormGroup className="w-100">
                          <InputField
                            {...{
                              register,
                              formState,
                              className: "inputField",
                              value: eventEditableData.title,
                              id: "title",
                              onChange: (e) => {
                                setEventEditableData({
                                  ...eventEditableData,
                                  title: e.target.value,
                                });
                                setIsChangedSection("title");
                              },
                            }}
                          />
                        </FormGroup>
                      </Col>

                      {isChangedSection === "title" && (
                        <Col md={3} xs={24}>
                          <Button
                            htmlType="button"
                            type="primary"
                            className="border-radius-10"
                            disabled={isEventDataUpdating}
                            loading={isEventDataUpdating}
                            onClick={onSubmitEventData}
                          >
                            <svg
                              width="26"
                              height="26"
                              viewBox="0 0 26 26"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <circle cx="13" cy="13" r="13" fill="white" />
                              <path
                                d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                                fill="#FF6C2C"
                              />
                            </svg>
                          </Button>
                        </Col>
                      )}
                    </Row>

                    <div className="dateAndTime">
                      <DatePicker
                        id="bookingDate"
                        placeholder="Select Date"
                        value={moment(eventEditableData.date)}
                        onChange={(e) => {
                          if (e) {
                            setEventEditableData({
                              ...eventEditableData,
                              date: e?.format("YYYY-MM-DD"),
                            });
                          }
                          setIsChangedSection("datetime");
                        }}
                        disabledDate={(currentDate) => {
                          return disabledDatePast(currentDate);
                        }}
                      />
                      from
                      <CustomTimePicker
                        {...{
                          handleOnChange: (d) => {
                            setEventEditableData({
                              ...eventEditableData,
                              startTime: d,
                            });
                            setIsChangedSection("datetime");
                          },
                          value: eventEditableData.startTime,
                        }}
                      />
                      -
                      <CustomTimePicker
                        {...{
                          handleOnChange: (d) => {
                            setEventEditableData({
                              ...eventEditableData,
                              endTime: d,
                            });
                            setIsChangedSection("datetime");
                          },
                          value: eventEditableData.endTime,
                        }}
                      />
                      PDT
                      {isChangedSection === "datetime" && (
                        <Button
                          htmlType="button"
                          type="primary"
                          className="border-radius-10"
                          disabled={isEventDataUpdating}
                          loading={isEventDataUpdating}
                          onClick={onSubmitEventData}
                        >
                          <svg
                            width="26"
                            height="26"
                            viewBox="0 0 26 26"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <circle cx="13" cy="13" r="13" fill="white" />
                            <path
                              d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                              fill="#FF6C2C"
                            />
                          </svg>
                        </Button>
                      )}
                    </div>
                  </div>
                )}

                <Row className="mt-0">
                  <Col>
                    <h3>Event Information</h3>
                  </Col>
                </Row>

                <Row className="event-infomation">
                  <Col span={20}>
                    <p className="cursor-pointer">
                      <Link
                        className="linksAv"
                        target={"_blank"}
                        href={`/artist/${eventData?.artist?.vanity}`}
                      >
                        <label>Artist:</label>{" "}
                        {capitalizeString(eventData?.artist?.name)}
                      </Link>
                    </p>
                    {eventData?.venue ? (
                      <div>
                        <p>
                          <Link
                            className="linksAv"
                            target={"_blank"}
                            href={`/venue/${eventData?.venue?.vanity}`}
                          >
                            <label>Venue:</label>{" "}
                            {capitalizeString(eventData?.venue?.name)}
                          </Link>
                        </p>
                        <p>
                          <label>Address:</label> {eventData?.venue?.address}
                        </p>
                      </div>
                    ) : (
                      ""
                    )}
                  </Col>
                  <Col span={4}>
                    {eventData?.venue?.iconImage?.url && (
                      <Image
                        src={
                          eventData?.venue?.iconImage?.url ||
                          "/images/venue-placeholder.png"
                        }
                        alt="venue Image"
                        height={112}
                        width={112}
                      />
                    )}
                  </Col>
                </Row>

                <Row className="mt-4">
                  <Col>
                    <h3>Booking Pro Contact</h3>
                  </Col>
                </Row>

                <Row className="booking-contact">
                  <Col>
                    <p>
                      <label> Name: </label> Joe Cardillo
                    </p>

                    <div className="dflex mt-3">
                      <p className="me-2">
                        <EmailIcon /> Joe@jcasptechnologies.com
                      </p>
                      <p className="ms-2">
                        <PhoneIcon /> (510) 400-6477
                      </p>
                    </div>
                  </Col>
                </Row>

                <Row className="mt-4">
                  <Col>
                    <h3>Payment</h3>
                  </Col>
                </Row>
                <Row className="payment-information" gutter={[10, 10]}>
                  <Col sm={24} md={12}>
                    <label>Total Cost:</label>
                    <InputField
                      {...{
                        register,
                        formState,
                        className: "inputField",
                        id: "totalCost",
                        placeholder: "Total Cost",
                        type: "number",
                        value: String(eventEditableData.total),
                        onChange: (d: any) => {
                          setEventEditableData({
                            ...eventEditableData,
                            total: parseInt(d.target.value),
                          });
                          setIsChangedSection("total");
                        },
                        readOnly: eventData?.bookingStatus !== "confirmed",
                        disabled:
                          loginUserState?.data?.user?.role ===
                          LOGIN_USER_TYPE.VENUE_ADMIN,
                      }}
                    />

                    <label>Artist Proceeds:</label>

                    <InputField
                      {...{
                        register,
                        formState,
                        className: "inputField",
                        id: "artistProceeds",
                        placeholder: "Artist Proceeds",
                        type: "number",
                        value: String(eventEditableData.proceeds),
                        onChange: (d: any) => {
                          setEventEditableData({
                            ...eventEditableData,
                            proceeds: parseInt(d.target.value),
                          });
                          setIsChangedSection("total");
                        },
                        readOnly: eventData?.bookingStatus !== "confirmed",
                        disabled:
                          loginUserState?.data?.user?.role ===
                          LOGIN_USER_TYPE.VENUE_ADMIN,
                      }}
                    />

                    {isChangedSection === "total" && (
                      <Button
                        htmlType="button"
                        type="primary"
                        className="border-radius-10 w-30 p-2 mt-2"
                        disabled={isEventDataUpdating}
                        loading={isEventDataUpdating}
                        onClick={onSubmitEventData}
                      >
                        <svg
                          width="26"
                          height="26"
                          viewBox="0 0 26 26"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <circle cx="13" cy="13" r="13" fill="white" />
                          <path
                            d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                            fill="#FF6C2C"
                          />
                        </svg>
                      </Button>
                    )}
                  </Col>
                  <Col sm={24} md={12}>
                    <label>Paidout:</label>
                    <p>{eventData?.transaction?.paidout ? "Yes" : "No"}</p>

                    <label>Estimated Payout:</label>
                    <p>
                      {DateTime.fromISO(eventData?.startDate?.iso)
                        .setZone(eventData?.timeZone)
                        .plus({ days: 30 })
                        .toFormat("MM/dd/yyyy")}
                    </p>

                    {eventData?.artist?.banking?.paymentPreference && (
                      <>
                        <label>Preference:</label>
                        <p>
                          <span>
                            <Image
                              src={`/images/general/${
                                eventData?.artist?.banking
                                  ?.paymentPreference === "paypal"
                                  ? "paypal.png"
                                  : "vimeo.png"
                              }`}
                              className="no-border-radius"
                              alt="Payment Preference"
                              width={24}
                              height={24}
                            />
                          </span>
                          {eventData?.artist?.banking?.paymentPreference ===
                          "paypal"
                            ? eventData?.artist?.banking?.paypal
                            : eventData?.artist?.banking?.venmo}
                        </p>
                      </>
                    )}
                  </Col>
                  <Col sm={24} md={24}>
                    {loginUserState?.data?.userType ===
                      LOGIN_USER_TYPE.ADMIN && (
                      <Button
                        htmlType="button"
                        type="primary"
                        className="float-right"
                        disabled={
                          showExpeditedPayment ||
                          isFormLoading ||
                          eventData?.bookingStatus !== "confirmed"
                        }
                        loading={isFormLoading}
                        onClick={handleSweetalert}
                      >
                        Request Expedited Payment
                      </Button>
                    )}
                  </Col>
                </Row>

                <Divider />
                {noteData?.length > 0 && (
                  <Row className="mt-2">
                    <Col span={24} className="noteSection">
                      {noteData?.map((n: any, index: number) => (
                        <div className="note-history" key={index}>
                          <div>
                            <div>
                              <strong>
                                {DateTime.fromISO(n.date?.iso).toFormat(
                                  "MM/dd/yyyy h:mm a"
                                )}
                              </strong>
                            </div>
                            <div>
                              <p>{n.note}</p>
                            </div>
                          </div>
                          <div>
                            <Popconfirm
                              title="Are you sure to delete?"
                              okText="Yes"
                              cancelText="No"
                              onConfirm={() => handleDeleteNote(n?.objectId)}
                              className="cursor-pointer"
                            >
                              <Button
                                type="primary"
                                htmlType="button"
                                className="border-radius-10 p-2"
                                disabled={deleteNoteIsLoading}
                                loading={deleteNoteIsLoading}
                              >
                                <DeleteIcon width={25} height={25} />
                              </Button>
                            </Popconfirm>
                          </div>
                        </div>
                      ))}
                    </Col>
                  </Row>
                )}

                <Row className="mt-2">
                  <Col>
                    <h3>Note</h3>
                  </Col>
                </Row>
                <form onSubmit={handleSubmit(onSubmit)}>
                  <Row align="middle" gutter={[10, 10]}>
                    <Col md={isNoteUpdate ? 21 : 24} xs={24}>
                      <FormGroup className="w-100">
                        <TextAreaField
                          {...{
                            register,
                            formState,
                            className: "w-100 p-2",
                            id: "note",
                            defaultValue: "",
                            readOnly: eventData?.bookingStatus !== "confirmed",
                            placeholder:
                              "Enter some additional information about the event...",
                            onChange: (e) => {
                              setIsNoteUpdate(true);
                            },
                          }}
                        />
                      </FormGroup>
                    </Col>
                    {isNoteUpdate && (
                      <Col md={3} xs={24}>
                        <Button
                          htmlType="submit"
                          type="primary"
                          className="float-right border-radius-10"
                          disabled={isFormLoading || formState?.isSubmitting}
                          loading={isFormLoading || formState?.isSubmitting}
                        >
                          <svg
                            width="26"
                            height="26"
                            viewBox="0 0 26 26"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <circle cx="13" cy="13" r="13" fill="white" />
                            <path
                              d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                              fill="#FF6C2C"
                            />
                          </svg>
                        </Button>
                      </Col>
                    )}
                  </Row>
                </form>

                <Divider />
                {loginUserState?.data?.userType === LOGIN_USER_TYPE.ADMIN &&
                  eventData?.bookingStatus !== "canceled" &&
                  eventData?.bookingStatus !== "declined" &&
                  eventData?.bookingStatus !== "archived" && (
                    <div className="allExport">
                      <div>
                        <Button
                          htmlType="button"
                          type="primary"
                          onClick={handleExportEventDetails}
                        >
                          Export Event Details
                        </Button>
                        {eventData.venue && (
                          <Button
                            htmlType="button"
                            type="primary"
                            onClick={handleExportSchedule}
                          >
                            Export Schedule
                          </Button>
                        )}
                      </div>
                      {eventData.venue && (
                        <div>
                          <Button
                            htmlType="button"
                            type="primary"
                            onClick={() => handleExportData("CurrrentMonth")}
                          >
                            Generate Current Month Venue Invoice
                          </Button>
                          <Button
                            htmlType="button"
                            type="primary"
                            onClick={() => handleExportData("NextMonth")}
                          >
                            Generate Next Month Venue Invoice
                          </Button>
                        </div>
                      )}
                      {eventData.venue && (
                        <div>
                          <Button
                            htmlType="button"
                            type="primary"
                            onClick={() => handleExportData("CurrentWeek")}
                          >
                            Generate Current Week Venue Invoice
                          </Button>
                          <Button
                            htmlType="button"
                            type="primary"
                            onClick={() => handleExportData("NextWeek")}
                          >
                            Generate Next Week Venue Invoice
                          </Button>
                        </div>
                      )}
                      <div>
                        <Popconfirm
                          title="Are you sure you want to cancel?"
                          okText="Yes"
                          cancelText="No"
                          onConfirm={handleCancelEvent}
                        >
                          <Button htmlType="button" type="ghost">
                            Cancel Event
                          </Button>
                        </Popconfirm>
                      </div>
                    </div>
                  )}
              </>
            </Modal.Body>
          </>
        )}
      </Modal>
    </>
  );
};
export default AdminCalendarEventModal;
