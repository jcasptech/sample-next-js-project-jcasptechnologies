import { yupResolver } from "@hookform/resolvers/yup";
import { getVenuesSearchAPI } from "@redux/services/general.api";
import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import Select from "react-select";
import { deepClone } from "src/libs/helpers";
import useList from "src/libs/useList";
import {
  AddShowsFormValidationProps,
  AddShowsFormValidationSchema,
} from "src/schemas/addShowFormSchema";
import modalStyles from "./artistAddShow.module.scss";
// import { TimePicker } from "antd";
import { Button } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { createArtistShowsAPI } from "@redux/services/shows.api";
import moment from "moment";
import DatePicker, { DateObject } from "react-multi-date-picker";
import DatePanel from "react-multi-date-picker/plugins/date_panel";
import TimePicker from "react-multi-date-picker/plugins/time_picker";
import { DATE_FORMAT, SUCCESS_MESSAGES } from "src/libs/constants";
import VenueSearchModal from "../venueSearchModal";
import Image from "next/image";

export interface ArtistAddShowModalProps {
  isOpen: boolean;
  artistId: string | null;
  handleClose: (data: any) => void;
  setIsNewShowModalOpen: (data: any) => void;
}
const ArtistAddShowModal = (props: ArtistAddShowModalProps) => {
  const { isOpen, setIsNewShowModalOpen, artistId, handleClose } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [venueOptions, setVenueOptions] = useState<any>([]);
  const [venueId, setVenueId] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [googlePlaceId, setGooglePlaceId] = useState("");
  const [isVenueModalOpen, setIsVenueModalOpen] = useState(false);
  const today = new Date();
  const tomorrow = new Date();

  tomorrow.setDate(tomorrow.getDate() + 1);

  let dates = moment(today);
  const [dateValue, setdateValue] = useState([dates.format(DATE_FORMAT)]);

  let hn = 18;
  let mn = 0o0;

  const [values, setValues] = useState(
    new DateObject().set({
      hour: hn,
      minute: mn,
      format: "PM",
    })
  );
  const timess = values.format("hh:mm A");

  const [venueData, setVenueData] = useState<any>({
    place: "",
    place_id: "",
  });
  const [venueAddress, setVenueAddress] = useState({
    value: "",
    label: "",
  });

  const [showDetails, setShowDetails] = useState<any>([
    {
      date: dateValue || [],
      startTime: timess || "",
    },
  ]);

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<AddShowsFormValidationProps>({
      resolver: yupResolver(AddShowsFormValidationSchema),
    });

  useEffect(() => {
    if (isOpen) {
      setIsModalOpen(isOpen);
    }
  }, [isOpen]);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsNewShowModalOpen(false);
  };

  const handleChangeDate = (e: any, index: number) => {
    const formatDate = moment(e);
    const date = formatDate.format("YYYY-MM-DD");
    const updatedItems = [...showDetails];
    updatedItems[index] = { ...updatedItems[index], date: date };
    setShowDetails(updatedItems);
  };

  const handleChangeTime = (e: any, index: any) => {
    const formattedTime = moment(e).format("hh:mm A").toString();
    const updatedItems = [...showDetails];
    updatedItems[index] = { ...updatedItems[index], startTime: formattedTime };
    setShowDetails(updatedItems);
  };

  useEffect(() => {
    if (showDetails) {
      setValue(`showDetails` as any, undefined);
      showDetails.map((item: any, index: any) => {
        setValue(`showDetails[${index}].startTime` as any, item.startTime);
        setValue(`showDetails[${index}].date` as any, item.date);
      });
    }
  }, [showDetails]);

  const { apiParam } = useList({
    queryParams: {
      search: "",
    },
  });

  const getVenue = async () => {
    try {
      let response = await getVenuesSearchAPI(apiParam);
      setVenueOptions(response);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getVenue();
  }, [apiParam]);

  const handleVenueChange = (data: any) => {
    if (data) {
      apiParam.search = data;
      getVenue();
    }
  };

  const handleChange = (data: any) => {
    setVenueId(data.value);
    setValue("venueSelect", data.value);
  };

  const addNewShow = () => {
    const tmpData = deepClone(showDetails);
    tmpData.push({
      date: dateValue || [],
      startTime: timess || "",
    });
    setShowDetails(tmpData);
  };

  const removeShow = (key: any) => {
    const tmpData = deepClone(showDetails);
    const tempData = tmpData.filter((item: any, index: any) => index != key);
    setShowDetails(tempData);
  };

  const onSubmit = async (data: any) => {
    if (venueId) {
      data["venueId"] = venueId;
    }
    if (googlePlaceId) {
      data["googlePlaceId"] = googlePlaceId;
    }
    data?.showDetails.map((item: any) => {
      item["artistId"] = artistId;
    });

    delete data.venueSelect;
    let tempData = deepClone(data);
    if (data) {
      setIsLoading(true);
      try {
        await createArtistShowsAPI(tempData);
        setIsModalOpen(false);
        setIsNewShowModalOpen(false);
        setIsLoading(false);
        handleClose(true);
        showToast(SUCCESS_MESSAGES.showAddedSuccess, "success");
      } catch (error) {
        setIsModalOpen(false);
        setIsLoading(false);
        setIsNewShowModalOpen(false);
      }
    }
  };

  useEffect(() => {
    if (venueData.place_id !== "") {
      setVenueAddress({
        value: venueData.place,
        label: venueData.place,
      });
      setValue("venueSelect", venueData.place_id);
      setVenueId("");
      setGooglePlaceId(venueData.place_id);
    }
  }, [venueData]);

  const handledateValues = (value: any, index: number) => {
    const datesArr: any = [];

    value.map((date: any, i: number) => {
      datesArr.push(date.format());
    });
    const updatedItems = [...showDetails];
    updatedItems[index] = {
      ...updatedItems[index],
      date: datesArr,
    };
    setdateValue(datesArr);
    setShowDetails(updatedItems);
  };

  const handleTimeValue = (value: any, index: number) => {
    const updatedItems = [...showDetails];
    updatedItems[index] = {
      ...updatedItems[index],
      startTime: value.format(),
    };
    setShowDetails(updatedItems);
  };

  return (
    <>
      {isVenueModalOpen && (
        <VenueSearchModal
          {...{
            isOpen: isVenueModalOpen,
            setIsVenueModalOpen,
            setVenueData,
          }}
        />
      )}

      <Modal show={isModalOpen} onHide={handleCancel}>
        <Modal.Header closeButton className={modalStyles.header}>
          <h2 className="modal-title modal-e text-center" id="inputModalLabel">
            Add New Show
          </h2>
        </Modal.Header>
        <Modal.Body className={modalStyles.modal_body}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Row>
              <Col span={10}>
                <div className={`${modalStyles.venueSelectlabel} `}>
                  <label>JCasp Venue</label>
                </div>
              </Col>
              <Col span={14}>
                <div className={`venueSearch`}>
                  <a onClick={() => setIsVenueModalOpen(true)}>
                    Can&apos;t find? Create a Venue
                  </a>
                </div>
              </Col>
            </Row>

            <div className={modalStyles.venueSelect}>
              {venueAddress.value !== "" && (
                <Select
                  className={`basic_multi_select selectField`}
                  classNamePrefix="Enter venue name to search"
                  placeholder="Enter venue name to search"
                  defaultValue={venueAddress}
                  value={venueAddress || ""}
                  options={venueOptions}
                  onInputChange={handleVenueChange}
                  onChange={handleChange}
                  isDisabled={venueAddress.value !== ""}
                  instanceId="venue"
                />
              )}

              {venueAddress.value === "" && (
                <>
                  <Select
                    className={`basic_multi_select selectField`}
                    classNamePrefix="Enter venue name to search"
                    placeholder="Enter venue name to search"
                    options={venueOptions}
                    onInputChange={handleVenueChange}
                    onChange={handleChange}
                    instanceId="venuename"
                  />

                  {formState &&
                    formState?.errors &&
                    formState?.errors["venueSelect"] &&
                    formState?.errors["venueSelect"].message && (
                      <span className="ant-typography ant-typography-danger text-danger block mt-1">
                        {formState.errors["venueSelect"].message}
                      </span>
                    )}
                </>
              )}
            </div>
            {showDetails?.map((form: any, index: any) => (
              <div key={index} className={index !== 0 ? "dividar mt-3" : ""}>
                <div id="dynamicRows" className="mt-3">
                  <div
                    className="row shows-m-bottom custom-rows  custom-rows"
                    id="row_1"
                  >
                    <div className="form-group col-md-6 col-sm-12">
                      <div className="row shows-venue-pad">
                        <div className="formB col-md-12 col-12">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label>Dates</label>
                            <div className="">
                              <DatePicker
                                multiple
                                placeholder="Select Date"
                                format={DATE_FORMAT}
                                value={dateValue}
                                minDate={new Date()}
                                onChange={(value) =>
                                  handledateValues(value, index)
                                }
                                plugins={[<DatePanel key={index} />]}
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]?.date
                                  ?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.date?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group col-md-6 col-sm-12">
                      <div className="row shows-venue-pad align-center">
                        <div className="shows-venue formB col-md-8 col-8">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label htmlFor="time">Start Time</label>
                            <div className="timePiker">
                              <DatePicker
                                disableDayPicker
                                id={`showDetails[${index}].date`}
                                format="hh:mm A"
                                value={values}
                                placeholder="Select time"
                                onChange={(value) =>
                                  handleTimeValue(value, index)
                                }
                                plugins={[
                                  <TimePicker
                                    hideSeconds
                                    key={index}
                                    mStep={15}
                                  />,
                                ]}
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]
                                  ?.startTime?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.startTime?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                        <div className="icons col-4 col-md-4 mt-3">
                          {index === 0 && (
                            <div className="addIcon" onClick={addNewShow}>
                              <Image
                                src="images/general/addIcon.svg"
                                alt="Add new show"
                                width={40}
                                height={40}
                              />
                            </div>
                          )}

                          {index !== 0 && (
                            <div
                              className="addIcon"
                              onClick={() => removeShow(index)}
                            >
                              <Image
                                src="images/general/removeIcon.svg"
                                alt="Remove show"
                                width={40}
                                height={40}
                              />
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="DatesContainer">
                  {showDetails[index]?.date &&
                    showDetails[index]?.date?.length > 0 &&
                    showDetails[index]?.date?.map(
                      (item: any, index: number) => (
                        <span key={index} className="Multidates">
                          {item}{" "}
                        </span>
                      )
                    )}
                </div>
              </div>
            ))}

            <div className="modal-footer shows-btn mt-1">
              <Button
                htmlType="button"
                type="ghost"
                className=""
                onClick={handleCancel}
              >
                Close
              </Button>
              <div>
                <Button
                  htmlType="submit"
                  type="primary"
                  className=""
                  loading={isLoading}
                  disabled={isLoading}
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default ArtistAddShowModal;
