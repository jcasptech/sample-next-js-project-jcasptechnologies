import { FullStarGrayIcon } from "@components/theme/icons/fullStarGrayIcon";
import { FullStartIcon } from "@components/theme/icons/fullStarIcon";
import { HalfStarIcon } from "@components/theme/icons/HalfStarIcon";
import { showToast } from "@components/ToastContainer";
import { ArtistObject, ArtistRatingDataResponse } from "@redux/slices/artists";
import Image from "next/image";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import modalstyle from "../modal.module.scss";

export interface ArtistReviewModalProps {
  artistDetail: ArtistObject;
  artistRatings: ArtistRatingDataResponse;
  isOpen: boolean;
  theme: "dark" | "light";
  setIsReviewModalOpen: (data: any) => void;
}
const ArtistReviewModal = (props: ArtistReviewModalProps) => {
  const { isOpen, setIsReviewModalOpen, artistRatings, theme, artistDetail } =
    props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [rating, setRating] = useState(0);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsReviewModalOpen(false);
  };
  const handleChange = (e: any) => {
    setRating(e);
  };
  useEffect(() => {
    if (artistRatings?.rating?.length > 0) {
      setIsModalOpen(isOpen);
    } else {
      showToast(SUCCESS_MESSAGES.artistReviewMessage, "warning");
      setIsModalOpen(false);
      setIsReviewModalOpen(false);
    }
  }, [isOpen]);

  return (
    <>
      <Modal show={isModalOpen} onHide={handleCancel}>
        <Modal.Header closeButton className={modalstyle.header}></Modal.Header>
        <Modal.Body className={modalstyle.modal_body}>
          <h4 className="modal-title text-xs-center text-center modal-e text-raven">
            {artistDetail?.name} Review
          </h4>
          <div className={modalstyle.madal_scrollbar}>
            {artistRatings &&
              artistRatings?.rating?.length &&
              artistRatings?.rating?.map((review: any, index: number) => (
                <div className="row cards-j" key={index}>
                  <div className="card card-bg text-white">
                    <div
                      className={`card-j ${
                        theme === "dark"
                          ? modalstyle.review_card
                          : modalstyle.review_card_white
                      }`}
                    >
                      <h4 className="capitalize">
                        {review?.user?.name}{" "}
                        <Image
                          src="/images/landing-page/verified.png"
                          alt="verified"
                          width={32}
                          height={22}
                        />
                      </h4>
                      <div className="rating">
                        <ul>
                          {[5, 4, 3, 2, 1].map((star) => (
                            <li key={`star-${star}`}>
                              {star <= review?.rating ? (
                                <FullStartIcon />
                              ) : star > review?.rating &&
                                review?.rating > star - 1 ? (
                                <HalfStarIcon />
                              ) : (
                                <FullStarGrayIcon />
                              )}
                            </li>
                          ))}
                        </ul>
                      </div>

                      <p className={`card-text ${modalstyle.card_text}`}>
                        {review.comment}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default ArtistReviewModal;
