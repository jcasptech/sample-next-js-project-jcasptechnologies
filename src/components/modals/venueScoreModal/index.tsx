import { Button, DefaultLoader } from "@components/theme";
import CustomCheckbox from "@components/theme/checkbox";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { LocationIcon } from "@components/theme/icons/locationIcon";
import { PhoneIcon } from "@components/theme/icons/phoneIcon";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { updateFeaturedVenueAPI } from "@redux/services/venue.api";
import { getVenueFullDetailAPI } from "@redux/services/venue.api";
import { VenueObject } from "@redux/slices/venues";
import { Col, Divider, Row } from "antd";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import { artistScoreInputs } from "src/schemas/artistScoreSchema";
import {
  venueScoreInputs,
  venueScoreSchema,
} from "src/schemas/venueScoreSchema";
import ModalStyles from "./index.module.scss";

export interface VenueScoreModalProps {
  venueId: string;
  handleClose: (data: boolean) => void;
}
const VenueScoreModal = (props: VenueScoreModalProps) => {
  const { venueId, handleClose } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [isUpdated, setIsUpdated] = useState(false);
  const [isFormLoading, setIsFormLoading] = useState(false);
  const [isOverrideScore, setIsOverrideScore] = useState(false);
  const [venueData, setVenuetData] = useState<VenueObject | null>(null);

  const { register, handleSubmit, formState, setValue, control, reset } =
    useForm<venueScoreInputs>({
      resolver: yupResolver(venueScoreSchema),
    });

  const handleCancel = () => {
    handleClose(isUpdated);
  };
  const getFullDetail = async () => {
    if (venueId) {
      try {
        const data = await getVenueFullDetailAPI(venueId);
        setVenuetData(data);
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
      }
    }
  };

  const onSubmit = async (data: artistScoreInputs) => {
    try {
      setIsFormLoading(true);
      await updateFeaturedVenueAPI({
        ...data,
        venueId,
      });
      showToast(SUCCESS_MESSAGES.scoreUpdateSuccess, "success");
      setIsFormLoading(false);
      setIsUpdated(true);
    } catch (error) {
      console.log(error);
      setIsFormLoading(false);
    }
  };

  useEffect(() => {
    setValue("isAdminScore", isOverrideScore);
  }, [isOverrideScore]);

  useEffect(() => {
    if (venueData) {
      setValue("score", venueData.sortPriority || 0);
      setValue("adminScore", venueData.adminScore || 0);
      setIsOverrideScore(venueData.isAdminScore || false);
    }
  }, [venueData]);

  useEffect(() => {
    if (venueId) {
      getFullDetail();
    }
  }, [venueId]);

  return (
    <>
      <Modal show className={ModalStyles.modal} onHide={handleCancel} size="lg">
        <h2> Manage Score</h2>
        {(isLoading || !venueData) && <DefaultLoader />}
        {!isLoading && venueData && (
          <>
            <Modal.Header closeButton className={`${ModalStyles.header}`}>
              <h3>{venueData?.name}</h3>
            </Modal.Header>
            <Modal.Body className={`${ModalStyles.body}`}>
              <Row className="mt-4">
                <Col>
                  <h3>Venue Information</h3>
                </Col>
              </Row>

              <Row className="artist-infomation">
                <Col sm={20} xs={24}>
                  <div className="artists mb-2">
                    <div>
                      <label>Venue Name: </label>
                      <p>{capitalizeString(venueData?.name)}</p>
                    </div>
                    <div>
                      <label>City/State: </label>
                      <p>
                        {capitalizeString(venueData?.city)},{" "}
                        {capitalizeString(venueData?.state)}
                      </p>
                    </div>
                  </div>
                  {venueData?.phone && (
                    <p>
                      <PhoneIcon />
                      {venueData?.phone}
                    </p>
                  )}

                  {venueData?.city && (
                    <p>
                      <LocationIcon />
                      <Link
                        href={`https://www.google.com/maps/search/?api=1&query=${venueData?.location?.latitude},${venueData?.location?.longitude}`}
                        target="_blank"
                      >
                        {venueData?.address}, {venueData?.city}
                      </Link>
                    </p>
                  )}
                </Col>
                <Col sm={4} xs={24}>
                  {venueData?.iconImage?.url && (
                    <Image
                      className="profile-image"
                      height={120}
                      width={120}
                      src={
                        venueData?.iconImage?.url ||
                        "/images/venue-placeholder.png"
                      }
                      alt={venueData.name}
                    />
                  )}
                </Col>
              </Row>

              <Row className="mt-3">
                <Col>
                  <h3>Score Information</h3>
                </Col>
              </Row>

              <form
                onSubmit={handleSubmit(onSubmit)}
                className="score-information"
              >
                <Row align={"top"} gutter={[20, 20]}>
                  <Col sm={12} xs={24}>
                    <InputField
                      {...{
                        register,
                        formState,
                        className: "inputField",
                        id: "score",
                        placeholder: "Enter Score",
                        type: "number",
                        label: "Current Score",
                        readOnly: true,
                      }}
                    />
                    <div className="mt-2">
                      <CustomCheckbox
                        {...{
                          handleOnChange: (d) => {
                            setIsOverrideScore(d);
                          },
                          label: "Want to override score?",
                          isBeforeLabel: true,
                          value: isOverrideScore,
                        }}
                      />
                    </div>

                    {isOverrideScore && (
                      <div className="mt-2">
                        <InputField
                          {...{
                            register,
                            formState,
                            className: "inputField",
                            id: "adminScore",
                            placeholder: "Enter Score",
                            type: "number",
                            label: "Admin Score",
                          }}
                        />
                      </div>
                    )}
                  </Col>
                  <Col sm={12} xs={24}>
                    <div>
                      <label>
                        Number of Subscribers:{" "}
                        <span>{venueData?.fanCount || 0}</span>{" "}
                      </label>
                    </div>
                    <div>
                      <label>
                        Next Month Show Count:{" "}
                        <span>
                          {venueData?.nextMonthUpcomingShowCount || 0}
                        </span>
                      </label>
                    </div>
                    <div>
                      <label>
                        Check-in Count:{" "}
                        <span>{venueData?.checkinCount || 0}</span>
                      </label>
                    </div>
                  </Col>

                  <Col span={24}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={formState.isLoading || isFormLoading}
                      disabled={formState.isLoading || isFormLoading}
                    >
                      Save Score
                    </Button>
                  </Col>
                </Row>
              </form>

              <Divider />
            </Modal.Body>
          </>
        )}
      </Modal>
    </>
  );
};
export default VenueScoreModal;
