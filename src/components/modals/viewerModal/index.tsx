/* eslint-disable @next/next/no-img-element, jsx-a11y/alt-text */
import { Button } from "@components/theme";
import { Modal } from "antd";
import axios from "axios";
import fileDownload from "js-file-download";
import Image from "next/image";
import React, { useEffect, useRef, useState } from "react";
import {
  acceptedCertificateFile,
  acceptedCertificateImage,
} from "src/libs/constants";
import ViewerModalStyles from "./viewerModal.module.scss";

export interface viewerModalProps {
  isOpen: boolean;
  url: string | null;
  handleOk: (data: any) => void;
}

export const ViewerModal = (props: viewerModalProps) => {
  const { isOpen, handleOk, url } = props;

  const [isPDF, setIsPDF] = useState(false);
  const [fileName, setFileName] = useState("");
  const [isImage, setIsImage] = useState(false);
  const downloadRef: any = useRef(null);

  const handleDownload = () => {
    if (url) {
      axios
        .get(url, {
          responseType: "blob",
        })
        .then((res) => {
          fileDownload(res.data, fileName);
        })
        .catch((error) => {
          console.log(error);
          downloadRef?.current?.click();
        });
    }
  };

  const getURLExtension = (url: string) => {
    let extension = null;
    const arrayString = url.split(/[#?]/);

    if (Array.isArray(arrayString) && arrayString.length > 0) {
      const tmpArray: Array<string> = arrayString[0].split(".");
      if (tmpArray.length > 0) {
        extension = tmpArray.pop();
        extension = extension?.trim();
        setFileName(`${Date.now()}.${extension}`);
      }
    }
    return extension;
  };

  useEffect(() => {
    if (url) {
      const extension = getURLExtension(url);
      if (acceptedCertificateFile.includes(extension)) {
        setIsPDF(true);
      }

      if (acceptedCertificateImage.includes(extension)) {
        setIsImage(true);
      }
    }
  }, [url]);

  return (
    <Modal
      className={ViewerModalStyles.viewer}
      visible={isOpen}
      centered
      width={isPDF ? "80%" : 700}
      onCancel={handleOk}
      onOk={handleOk}
      footer={[
        <div className="flex justify-center mb-4" key={0}>
          <Button htmlType="button" onClick={handleOk} key={0}>
            Close
          </Button>
          <Button
            htmlType="button"
            onClick={handleDownload}
            disabled={url ? false : true}
            key={1}
          >
            Download
          </Button>
        </div>,
      ]}
    >
      <div className=" flex justify-center mt-4 mb-3">
        {isImage && (
          <Image src={url ? url : ""} alt="Image" height={100} width={100} />
        )}

        {isPDF && (
          <iframe
            src={url ? url : ""}
            className={ViewerModalStyles.iframeTag}
          ></iframe>
        )}

        {url && <a href={url} download target={"_blank"} ref={downloadRef} />}
      </div>
    </Modal>
  );
};
