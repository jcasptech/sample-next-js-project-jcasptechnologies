import { DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { postConfirmPostAPI } from "@redux/services/active-posting.api";
import {
  ActiveBidsState,
  fetchActivePostingBids,
  loadMoreActivePostingBids,
} from "@redux/slices/activePostingBids";
import { LoginUserState } from "@redux/slices/auth";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useDispatch, useSelector } from "react-redux";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import Swal from "sweetalert2";
import ArtistContactModal from "../activePostingArtistContactModal";
import bidsModalstyle from "./bidsModal.module.scss";

export interface ActivePostingBidsModalProps {
  isOpen: boolean;
  setIsBidsModalOpen: (data: any) => void;
  selectEventId: string;
}
const ActivePostingBidsModal = (props: ActivePostingBidsModalProps) => {
  const { isOpen, setIsBidsModalOpen, selectEventId } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isContactModalOpen, setIsContactModalOpen] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();

  const [artistData, setArtistData] = useState<any>();
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const handleCancel = () => {
    setIsBidsModalOpen(false);
    setIsModalOpen(false);
  };

  useEffect(() => {
    if (isOpen) {
      setIsModalOpen(isOpen);
    }
  }, [isOpen]);

  const {
    bids: { isLoading, data: bidsData },
  }: {
    bids: ActiveBidsState;
  } = useSelector((state: RootState) => ({
    bids: state.activePostingBids,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 4,
      skip: 0,
      include: ["artist"],
    },
  });

  const handleLoadMore = (d: any) => {
    if (selectEventId) {
      apiParam.skip = (apiParam.skip || 0) + 4;
      dispatch(loadMoreActivePostingBids(selectEventId, apiParam));
    }
  };

  useEffect(() => {
    if (selectEventId) {
      dispatch(fetchActivePostingBids(selectEventId, apiParam));
    }
  }, [selectEventId]);

  const handleProConfirm = async (data: any) => {
    Swal.fire({
      title: "Are you sure you want to PRO Confirm JCasp Artist?",
      text: "PRO confirming means that when you select an artist, an event will be created with all the parameters of this posting, but the posting and event will not be associated with one another. The posting will remain up and unchanged.",
      showCancelButton: true,
      confirmButtonColor: "#0b4121",
      cancelButtonColor: "#454a54",
      confirmButtonText: "Yes, PRO Confirm",
    }).then((result) => {
      const postData = async () => {
        try {
          await postConfirmPostAPI(data);
          showToast(SUCCESS_MESSAGES.postingConfirmSuccess, "success");
        } catch (error) {
          console.log(error);
        }
      };
      postData();
    });
  };

  const handleContactModal = (data: any) => {
    if (data) {
      setArtistData(data);
      setIsContactModalOpen(true);
    }
  };

  return (
    <>
      {isContactModalOpen && (
        <ArtistContactModal
          {...{
            isOpen: isContactModalOpen,
            setIsContactModalOpen,
            artistData,
          }}
        />
      )}
      <Modal show={isModalOpen} onHide={handleCancel}>
        <Modal.Header closeButton className={bidsModalstyle.header}>
          <h3>Interested Artists ({bidsData?.total})</h3>
        </Modal.Header>
        <Modal.Body className={bidsModalstyle.modal_body}>
          {bidsData &&
            bidsData?.list?.length &&
            bidsData?.list?.map((bids: any, index: number) => (
              <div className="col-12 message-hold" key={index}>
                <div
                  className="show_indi_card"
                  data-aos=""
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <div className="row m0 vcenter ">
                    <div className="col-sm-4 col-md-5 col-lg-5 col-xl-3 col-4 show_thumb p-0 imageArtist">
                      {/* <img
                        src={
                          bids.artistIconImage?.url ||
                          "/images/artist-placeholder.png"
                        }
                        alt="venue title"
                      /> */}
                    </div>
                    <div className="col-sm-8 col-md-7 col-lg-7 col-xl-9 col-8 show_meta">
                      <div className="row message-btns">
                        <div className="row title title-m text-ellipsis">
                          <div className="col-8 text-ellipsis">
                            {" "}
                            {bids.artistName || ""}
                          </div>
                          <div className="addressImage col-4 text-ellipsis">
                            {/* {bids?.artist?.address && (
                              <img
                                className="input-icon-left"
                                src="https://d3ixo9465z3fpc.cloudfront.net/assets/np/pin-3e3516a38120d1a6a0e373f4cecee67a3670c5c54278ab15390d853f748a048e.svg"
                                alt="Pin"
                              />
                            )} */}
                            {bids?.artist?.address || ""}{" "}
                          </div>
                        </div>
                        <div className="message col-12">
                          {bids.note || ""}
                          Message
                        </div>
                        <div className="button">
                          <div className="profile">
                            <Link
                              href={`/artist/${bids.artist.vanity}`}
                              target="_blank"
                              className="hvr-float-shadow"
                              data-toggle="modal"
                              data-target="#approveModal"
                            >
                              View Artist Profile
                            </Link>
                          </div>

                          <div className="contact">
                            <a
                              href="#"
                              className="hvr-float-shadow"
                              data-toggle="modal"
                              data-target="#approveModal"
                              onClick={() => handleContactModal(bids?.artist)}
                            >
                              Contact
                            </a>
                          </div>
                          {loginUserState.data?.userType !==
                            LOGIN_USER_TYPE.MANAGER && (
                            <div className="confirm">
                              <a
                                href="#"
                                className="hvr-float-shadow"
                                data-toggle="modal"
                                data-target="#deleteModal"
                                onClick={() => handleProConfirm(bids?.objectId)}
                              >
                                PRO Confirm
                              </a>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          {bidsData?.list && bidsData?.list?.length <= 0 && (
            <EmptyMessage description="No bids found." />
          )}

          {isLoading && <DefaultLoader />}
          {bidsData.hasMany && (
            <div className="col-12 load_more">
              <Link
                href={"#"}
                className={`hvr-float-shadow ${isLoading ? "disabled" : ""}`}
                onClick={(e) => {
                  e.preventDefault();
                  handleLoadMore({
                    loadMore: true,
                  });
                }}
              >
                Load more bids
              </Link>
            </div>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default ActivePostingBidsModal;
