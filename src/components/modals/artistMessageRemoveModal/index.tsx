import { showToast } from "@components/ToastContainer";
import { postArtistMessageApproveRejectAPI } from "@redux/services/artist.api";
import { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import modalstyle from "./removeModal.module.scss";

export interface ArtistMessageRemoveModalProps {
  isRemovedModal: boolean;
  setIsRemoveModal: (d: any) => void;
  messageId: string;
  handleClose: (d: any) => void;
}
const ArtistMessageRemoveModal = (props: ArtistMessageRemoveModalProps) => {
  const { isRemovedModal, setIsRemoveModal, messageId, handleClose } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsRemoveModal(false);
  };

  const handleRemove = async (status: any) => {
    if (messageId) {
      try {
        await postArtistMessageApproveRejectAPI({
          messageId: messageId,
          status: status,
        });
        showToast(SUCCESS_MESSAGES.messageDeclinedSuccess, "success");
        handleClose(status);
        setIsModalOpen(false);
        setIsRemoveModal(false);
      } catch (error) {
        console.log(error);
      }
    }
  };
  return (
    <>
      <Modal
        className={`modal fade approve-reject ${modalstyle.main_modal}`}
        show={isRemovedModal}
        onHide={handleCancel}
      >
        <Modal.Body className={modalstyle.main_body}>
          <div className={modalstyle.main_body}>
            <div className={`${modalstyle.modalContentApprove} modal-content`}>
              <div className="modal-body">
                Are you sure! you want to Decline ?
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary cancel"
                  data-dismiss="modal"
                  onClick={() => handleCancel()}
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="btn btn-danger remove-btn-2"
                  onClick={() => handleRemove("declined")}
                >
                  Yes, Decline
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ArtistMessageRemoveModal;
