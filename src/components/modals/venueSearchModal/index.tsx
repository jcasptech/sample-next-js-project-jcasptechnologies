import Modal from "react-bootstrap/Modal";
import venueModalstyle from "./venueSearch.module.scss";
import React, { useState, useEffect, useRef } from "react";
import { GoogleMap, InfoWindow, Marker } from "@react-google-maps/api";
import Autocomplete, { usePlacesWidget } from "react-google-autocomplete";
import { GOOGLE_API_KEY } from "src/libs/constants";
import { useForm } from "react-hook-form";
import { loadScript } from "src/libs/helpers";
import { DefaultLoader } from "@components/theme";
import Image from "next/image";

export interface ArtistReviewModalProps {
  isOpen: boolean;
  setIsVenueModalOpen: (data: any) => void;
  setVenueData: (data: any) => void;
}
const VenueSearchModal = (props: ArtistReviewModalProps) => {
  const { isOpen, setIsVenueModalOpen, setVenueData } = props;

  const [isLoading, setIsLoading] = useState(true);
  const [placeData, setPlaceData] = useState({
    place: "",
    place_id: "",
  });

  const { register, formState } = useForm<any>();

  const handleCancel = () => {
    setIsVenueModalOpen(false);
  };

  const [centerLocation, setCenterLocation] = useState({
    lat: 32.883,
    lng: -117.156,
  });

  const handleSelect = (place: any) => {
    setPlaceData({
      place: place?.formatted_address,
      place_id: place?.place_id,
    });
    const lat = place.geometry.location.lat();
    const lng = place.geometry.location.lng();
    setCenterLocation({
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
    });
  };

  const handleSubmit = () => {
    if (placeData.place_id !== "") {
      setVenueData(placeData);
      setIsVenueModalOpen(false);
    }
  };

  useEffect(() => {
    setTimeout(function () {
      jQuery(".pac-container").prependTo("#mapMoveHere");
    }, 300);
  }, []);

  const handleScriptLoad = () => {
    setIsLoading(false);
  };

  useEffect(() => {
    loadScript(
      `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=places`,
      () => handleScriptLoad()
    );
  }, []);

  return (
    <>
      <Modal show={isOpen} onHide={handleCancel}>
        <Modal.Header closeButton className={venueModalstyle.header}>
          {" "}
        </Modal.Header>
        <Modal.Body className={venueModalstyle.modal_body}>
          {isLoading && <DefaultLoader />}

          {!isLoading && (
            <>
              <div
                id="address-container"
                className="display-table w-100 m-b-1 position-rel"
              >
                <Image
                  className="input-icon-left"
                  src="/images/general/locationPin.svg"
                  alt="Pin"
                  width={24}
                  height={24}
                />
                <Autocomplete
                  // ref={inputEl}
                  placeholder="Search for venue name or address"
                  apiKey={"AIzaSyBt76hsi8D_EmmZkQE4pIztnAGotzte-1I"}
                  onPlaceSelected={(place) => handleSelect(place)}
                  options={{
                    types: [
                      "bar",
                      "restaurant",
                      "shopping_mall",
                      "stadium",
                      "tourist_attraction",
                    ],
                  }}
                />
              </div>
              <div id="mapMoveHere"></div>
              <div className="googleMap">
                <GoogleMap
                  options={{
                    clickableIcons: false,
                    disableDefaultUI: false,
                    mapTypeControl: false,
                    fullscreenControl: false,
                    zoomControl: false,
                    scaleControl: true,
                    streetViewControl: false,
                    zoomControlOptions: {
                      position: google.maps.ControlPosition.RIGHT_TOP, // Change position to top left corner
                    },
                    styles: [
                      {
                        featureType: "all",
                        stylers: [{ saturation: -80 }, { lightness: 30 }],
                      },
                    ],
                    zoom: 10,
                    minZoom: 3,
                    mapTypeId: "roadmap",
                  }}
                  zoom={10}
                  center={centerLocation}
                  mapTypeId={"roadmap"}
                  mapContainerStyle={{
                    width: "20%",
                    height: "20%",
                    borderRadius: "15px",
                  }}
                  onLoad={() => console.log("Map Component Loaded...")}
                >
                  <Marker
                    position={centerLocation}
                    icon="/images/general/map-marker.png"
                    // onClick={() => onMarkerClick(mark)}
                  />
                </GoogleMap>
              </div>

              <div className="submitbutton">
                <button type="button" onClick={handleSubmit}>
                  Confirm Venue
                </button>
              </div>
            </>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default VenueSearchModal;
