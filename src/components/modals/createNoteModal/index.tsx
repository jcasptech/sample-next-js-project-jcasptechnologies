import { Button, DefaultLoader } from "@components/theme";
import { TextAreaField } from "@components/theme/form/formFieldsComponent";
import { DeleteIcon } from "@components/theme/icons/deleteIcon";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  createNote,
  deleteNotes,
  getNoteFullDetailAPI,
} from "@redux/services/calendar.api";
import { Popconfirm } from "antd";
import { DateTime } from "luxon";
import { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { NotesInputs, NotesSchema } from "src/schemas/notesSchema";
import CreateNoteModalStyles from "./createNoteModal.module.scss";

export interface CaledarAddNoteModalProps {
  isOpen: boolean;
  id: string | null;
  date: Date;
  setIsNoteModalOpen: (data: any) => void;
  handleClose: (d: any) => void;
}
const CalendarAddNoteModal = (props: CaledarAddNoteModalProps) => {
  const { isOpen, id, setIsNoteModalOpen, date, handleClose } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [noteData, setNoteData] = useState<any>();
  const [isFormLoading, setIsFormLoading] = useState(false);
  const [deleteNoteIsLoading, setDeleteNoteIsIsLoading] = useState(false);

  const { register, handleSubmit, formState, control, reset } =
    useForm<NotesInputs>({
      resolver: yupResolver(NotesSchema),
    });

  const handleCancel = () => {
    setIsNoteModalOpen(false);
  };

  const onSubmit = async (data: any) => {
    setIsFormLoading(true);
    try {
      if (id) {
        data["parentId"] = id;
      }

      data["date"] = DateTime.fromJSDate(date).toUTC().toISO();
      await createNote(data);
      showToast(SUCCESS_MESSAGES.createNotes, "success");
      reset();
      handleClose(true);
      setIsNoteModalOpen(false);
      setIsFormLoading(false);
    } catch (error) {
      setIsFormLoading(false);
    }
  };

  const getFullDetail = async () => {
    if (id) {
      setIsLoading(true);
      try {
        const data = await getNoteFullDetailAPI(id);
        setNoteData(data);
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
      }
    }
  };

  const handleDeleteNotes = async () => {
    if (id) {
      setDeleteNoteIsIsLoading(true);
      try {
        await deleteNotes(id);
        showToast(SUCCESS_MESSAGES.deleteNotes, "success");
        setDeleteNoteIsIsLoading(false);
        handleCancel();
        handleClose(true);
      } catch (err: any) {
        setDeleteNoteIsIsLoading(false);
      }
    }
  };

  useEffect(() => {
    if (id) {
      getFullDetail();
    } else {
      setIsLoading(false);
    }
  }, [id]);

  const handleDeleteNote = async (d: any) => {
    if (d) {
      setDeleteNoteIsIsLoading(true);
      try {
        await deleteNotes(d);
        showToast(SUCCESS_MESSAGES.deleteNotes, "success");
        handleClose(true);
        handleCancel();
        setDeleteNoteIsIsLoading(false);
      } catch (err: any) {
        setDeleteNoteIsIsLoading(false);
      }
    }
  };

  return (
    <>
      <Modal
        show={isOpen}
        onHide={handleCancel}
        size="lg"
        className={CreateNoteModalStyles.modal}
        closable={false}
      >
        <Modal.Header closeButton className={`${CreateNoteModalStyles.header}`}>
          <h3>
            Booking Note
            <p className="mb-1">
              {DateTime.fromJSDate(date).toFormat("EEEE, MMMM d, yyyy")}
            </p>
          </h3>
        </Modal.Header>
        <Modal.Body className={`${CreateNoteModalStyles.body}`}>
          {isLoading && <DefaultLoader />}
          {!isLoading && (
            <>
              <div className="notes-list">
                {noteData?.note && (
                  <div className="note-history" key={-1}>
                    <div>
                      <div>
                        <strong>
                          {DateTime.fromISO(noteData.date?.iso).toFormat(
                            "MM/dd/yyyy h:mm a"
                          )}
                        </strong>
                      </div>
                      <div>
                        <p>{noteData?.note}</p>
                      </div>
                    </div>
                    <div>
                      <Popconfirm
                        title="Are you sure to delete?"
                        okText="Yes"
                        cancelText="No"
                        onConfirm={() => handleDeleteNote(noteData?.objectId)}
                      >
                        <Button
                          type="primary"
                          htmlType="button"
                          className="border-radius-10 p-2"
                          disabled={deleteNoteIsLoading}
                        >
                          <DeleteIcon width={25} height={25} />
                        </Button>
                      </Popconfirm>
                    </div>
                  </div>
                )}
                {noteData?.child?.map((n: any, index: number) => (
                  <div className="note-history" key={index}>
                    <div>
                      <div>
                        <strong>
                          {DateTime.fromISO(n.date?.iso).toFormat(
                            "MM/dd/yyyy h:mm a"
                          )}
                        </strong>
                      </div>
                      <div>
                        <p>{n.note}</p>
                      </div>
                    </div>
                    <div>
                      <Popconfirm
                        title="Are you sure to delete?"
                        okText="Yes"
                        cancelText="No"
                        onConfirm={() => handleDeleteNote(n?.objectId)}
                      >
                        <Button
                          type="primary"
                          htmlType="button"
                          className="border-radius-10 p-2"
                          disabled={deleteNoteIsLoading}
                        >
                          <DeleteIcon width={25} height={25} />
                        </Button>
                      </Popconfirm>
                    </div>
                  </div>
                ))}
              </div>

              <form onSubmit={handleSubmit(onSubmit)}>
                <TextAreaField
                  {...{
                    register,
                    formState,
                    className: "notes",
                    id: "note",
                    placeholder:
                      "Add a note here for convenience. Only you can view this note.",
                  }}
                />

                <Row>
                  <Col span={12}>
                    <Button
                      htmlType="submit"
                      type="primary"
                      disabled={isFormLoading || formState?.isSubmitting}
                      loading={isFormLoading || formState?.isSubmitting}
                    >
                      Save Note
                    </Button>
                  </Col>
                  <Col span={12} className="justify-content-end">
                    {id && (
                      <Popconfirm
                        title="Are you sure you want to delete?"
                        okText="Yes"
                        cancelText="No"
                        onConfirm={handleDeleteNotes}
                      >
                        <Button htmlType="button" type="ghost">
                          Delete
                        </Button>
                      </Popconfirm>
                    )}
                  </Col>
                </Row>
              </form>
            </>
          )}
          {isFormLoading && <DefaultLoader />}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default CalendarAddNoteModal;
