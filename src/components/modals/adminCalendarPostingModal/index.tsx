import Bids from "@components/bids";
import { Button, DefaultLoader } from "@components/theme";
import {
  FormGroup,
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import CustomTimePicker from "@components/theme/timepicker";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import {
  getPostingFullDetailAPI,
  updateAdminCalendarPostAPI,
} from "@redux/services/calendar.api";
import { LoginUserState } from "@redux/slices/auth";
import { Col, DatePicker, Divider, Row } from "antd";
import { DateTime } from "luxon";
import moment from "moment";
import Image from "next/image";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import {
  DATE_TIME_FORMAT,
  DEFAULT_TIME_ZONE,
  LOGIN_USER_TYPE,
  SUCCESS_MESSAGES,
} from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import { AdminPostingInputs, PostingSchema } from "src/schemas/postingSchema";
import CalendrPostModalStyles from "./calendarPostModal.module.scss";
import Promote from "./promote";

export interface CaledarPostModalProps {
  isOpen: boolean;
  id: string;
  handleClose: (data: any) => void;
  setIsPostingModalOpen: (data: any) => void;
}
const AdminCalendarPostModal = (props: CaledarPostModalProps) => {
  const { isOpen, id, setIsPostingModalOpen, handleClose } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [postData, setPostData] = useState<any>();
  const [isFormLoading, setIsFormLoading] = useState(false);
  const [isNoteUpdate, setIsNoteUpdate] = useState(false);
  const [isChangedSection, setIsChangedSection] = useState<
    "title" | "datetime" | "proceed" | "total" | null
  >(null);
  const [isPostDataUpdating, setIspostDataUpdating] = useState(false);
  const [postEditableData, setPostEditableData] = useState<{
    title: string;
    date: string;
    startTime: string;
    endTime: string;
    proceeds: number;
    total: number;
  }>({
    title: "",
    date: "",
    startTime: "",
    endTime: "",
    proceeds: 0,
    total: 0,
  });

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<AdminPostingInputs>({
      resolver: yupResolver(PostingSchema),
    });

  const handleCancel = () => {
    setIsPostingModalOpen(false);
    handleClose(true);
  };

  const getFullDetail = async () => {
    if (id) {
      setIsLoading(true);
      try {
        const data = await getPostingFullDetailAPI(id);
        setPostData(data);
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
      }
    }
  };

  useEffect(() => {
    if (postData) {
      setPostEditableData({
        title: postData?.title,
        date: DateTime.fromISO(postData?.startDate?.iso)
          .setZone(postData?.timeZone)
          .toFormat("yyyy-M-d"),
        startTime: DateTime.fromISO(postData?.startDate?.iso)
          .setZone(postData?.timeZone)
          .toFormat("HH:mm:ss"),
        endTime: DateTime.fromISO(postData?.endDate?.iso)
          .setZone(postData?.timeZone)
          .toFormat("HH:mm:ss"),
        proceeds: postData?.artistProceeds || 0,
        total: postData?.totalCost || 0,
      });
    }
  }, [postData]);

  const onSubmitPostData = async () => {
    setIspostDataUpdating(true);
    try {
      await updateAdminCalendarPostAPI(id, {
        title: postEditableData.title,
        totalCost: postEditableData.total,
        startTime: DateTime.fromFormat(
          postEditableData.startTime,
          "HH:mm:ss"
        ).toFormat("hh:mm a"),
        endTime: DateTime.fromFormat(
          postEditableData.endTime,
          "HH:mm:ss"
        ).toFormat("hh:mm a"),
        artistProceeds: postEditableData.proceeds,
        bookingDate: postEditableData.date,
      });
      showToast(SUCCESS_MESSAGES.postingUpdateSuccess, "success");
      setIspostDataUpdating(false);
      setIsChangedSection(null);
    } catch (error) {
      setIspostDataUpdating(false);
    }
  };

  const onSubmit = async (data: any) => {
    setIsFormLoading(true);
    try {
      await updateAdminCalendarPostAPI(id, {
        note: data.note,
      });
      showToast(SUCCESS_MESSAGES.postingNoteUpdateSuccess, "success");
      setIsFormLoading(false);
      setIsNoteUpdate(false);
    } catch (error) {
      setIsFormLoading(false);
    }
  };

  const getFormatSubmittedArtist = () => {
    let format = "<5";
    if (postData?.submittedArtists?.length > 0) {
      if (postData?.submittedArtists?.length < 5) {
        format = "<5";
      } else if (
        postData?.submittedArtists?.length >= 5 &&
        postData?.submittedArtists?.length < 10
      ) {
        format = ">5";
      } else if (
        postData?.submittedArtists?.length >= 10 &&
        postData?.submittedArtists?.length < 50
      ) {
        format = ">10";
      } else if (
        postData?.submittedArtists?.length >= 50 &&
        postData?.submittedArtists?.length < 100
      ) {
        format = "<50";
      } else {
        format = ">50";
      }
    }
    return format;
  };

  useEffect(() => {
    if (id) {
      getFullDetail();
    }
  }, [id]);

  const disabledDatePast = (current: moment.Moment) => {
    const currentDate = DateTime.fromISO(current.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
      })
      .toUnixInteger();

    const currentWithTimezone = DateTime.fromISO(
      DateTime.utc()
        .set({
          hour: 0,
          minute: 0,
          second: 0,
        })
        .toISO({ includeOffset: false })
    ).toUnixInteger();

    return currentDate.valueOf() < currentWithTimezone.valueOf();
  };

  return (
    <>
      <Modal
        show={isOpen}
        onHide={handleCancel}
        size="lg"
        className={CalendrPostModalStyles.modal}
      >
        <h2>Open Gig Posting</h2>
        <Modal.Header
          closeButton
          className={`${CalendrPostModalStyles.header}`}
        >
          <h3>
            {postData?.title}
            <p>
              {postData?.startDate && postData?.startDate?.iso && (
                <>
                  {DateTime.fromISO(postData?.startDate?.iso)
                    .setZone(postData?.timeZone)
                    .toFormat("EEEE, MMMM d, yyyy")}{" "}
                  from{" "}
                  {DateTime.fromISO(postData?.startDate?.iso)
                    .setZone(postData?.timeZone)
                    .toFormat("h:mm a")}{" "}
                  -{" "}
                  {DateTime.fromISO(postData?.endDate?.iso)
                    .setZone(postData?.timeZone)
                    .toFormat("h:mm a")}
                </>
              )}
            </p>
          </h3>
        </Modal.Header>
        <Modal.Body className={`${CalendrPostModalStyles.body}`}>
          {isLoading && <DefaultLoader />}
          {!isLoading && (
            <>
              <div className="headerPart">
                <Row align="middle" gutter={[10, 10]}>
                  <Col md={isChangedSection === "title" ? 21 : 24} xs={24}>
                    <FormGroup className="w-100">
                      <InputField
                        {...{
                          register,
                          formState,
                          className: "inputField",
                          value: postEditableData.title,
                          id: "title",
                          onChange: (e) => {
                            setPostEditableData({
                              ...postEditableData,
                              title: e.target.value,
                            });
                            setIsChangedSection("title");
                          },
                        }}
                      />
                    </FormGroup>
                  </Col>

                  {isChangedSection === "title" && (
                    <Col md={3} xs={24}>
                      <Button
                        htmlType="button"
                        type="primary"
                        className="border-radius-10"
                        disabled={isPostDataUpdating}
                        loading={isPostDataUpdating}
                        onClick={onSubmitPostData}
                      >
                        <svg
                          width="26"
                          height="26"
                          viewBox="0 0 26 26"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <circle cx="13" cy="13" r="13" fill="white" />
                          <path
                            d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                            fill="#FF6C2C"
                          />
                        </svg>
                      </Button>
                    </Col>
                  )}
                </Row>
                <div className="dateAndTime">
                  <DatePicker
                    id="bookingDate"
                    placeholder="Select Date"
                    value={moment(postEditableData.date)}
                    onChange={(e) => {
                      if (e) {
                        setPostEditableData({
                          ...postEditableData,
                          date: e?.format("YYYY-MM-DD"),
                        });
                      }
                      setIsChangedSection("datetime");
                    }}
                    disabledDate={(currentDate) => {
                      return disabledDatePast(currentDate);
                    }}
                  />
                  from
                  <CustomTimePicker
                    {...{
                      handleOnChange: (d) => {
                        setPostEditableData({
                          ...postEditableData,
                          startTime: d,
                        });
                        setIsChangedSection("datetime");
                      },
                      value: postEditableData.startTime,
                    }}
                  />
                  -
                  <CustomTimePicker
                    {...{
                      handleOnChange: (d) => {
                        setPostEditableData({
                          ...postEditableData,
                          endTime: d,
                        });
                        setIsChangedSection("datetime");
                      },
                      value: postEditableData.endTime,
                    }}
                  />
                  PDT
                  {isChangedSection === "datetime" && (
                    <Button
                      htmlType="button"
                      type="primary"
                      className="border-radius-10"
                      disabled={isPostDataUpdating}
                      loading={isPostDataUpdating}
                      onClick={onSubmitPostData}
                    >
                      <svg
                        width="26"
                        height="26"
                        viewBox="0 0 26 26"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <circle cx="13" cy="13" r="13" fill="white" />
                        <path
                          d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                          fill="#FF6C2C"
                        />
                      </svg>
                    </Button>
                  )}
                </div>
              </div>
              <>
                <Row>
                  <Col>
                    <h3>About the event</h3>
                  </Col>
                </Row>

                <Row className="event-infomation  mb-4">
                  <Col span={24}>
                    <p>{postData?.note}</p>
                  </Col>
                </Row>

                <Row className="event-details">
                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/location.png"
                        alt="location"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Location
                    </div>
                    <div>
                      {postData?.city}{" "}
                      {postData?.zipcode ? `, ${postData?.zipcode}` : ""}
                    </div>
                  </Col>
                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/calendar.png"
                        alt="Date and Time"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Date and Time
                    </div>
                    <div>
                      {DateTime.fromISO(postData?.startDate?.iso)
                        .setZone(postData?.timeZone)
                        .toFormat("EEEE, MMMM d, yyyy")}{" "}
                      from{" "}
                      {DateTime.fromISO(postData?.startDate?.iso)
                        .setZone(postData?.timeZone)
                        .toFormat("h:mm a")}{" "}
                      -{" "}
                      {DateTime.fromISO(postData?.endDate?.iso)
                        .setZone(postData?.timeZone)
                        .toFormat("h:mm a")}
                    </div>
                  </Col>

                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/types-of-events.png"
                        alt="Type of event"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Type of event
                    </div>
                    <div>{capitalizeString(postData?.variety)}</div>
                  </Col>

                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/users.png"
                        alt="Type of artist wanted"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Type of artist wanted
                    </div>
                    <div>{capitalizeString(postData?.genres?.join(", "))}</div>
                  </Col>

                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/users.png"
                        alt="Expected attendance"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Expected attendance
                    </div>
                    <div>
                      {postData?.attendanceCategory === "min" && "<50"}
                      {postData?.attendanceCategory === "small" && "50-100"}
                      {postData?.attendanceCategory === "medium" && "100-200"}
                      {postData?.attendanceCategory === "max" && ">200"}
                    </div>
                  </Col>

                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/indoor-outdoor.png"
                        alt="Indoor/Outdoor"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Indoor/Outdoor
                    </div>
                    <div>{capitalizeString(postData?.playSetting)}</div>
                  </Col>

                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/equipment.png"
                        alt="Should the artist bring their own PA system?"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Should the artist bring their own PA system?
                    </div>
                    <div>
                      {postData?.equipmentProvided === true ? "Yes" : "No"}
                    </div>
                  </Col>

                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/users.png"
                        alt="Total Artist submitted"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Total Artist submitted
                    </div>
                    <div>{getFormatSubmittedArtist()}</div>
                  </Col>

                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/tag.png"
                        alt="Proceeds"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Proceeds
                    </div>
                    <div>
                      ${postData?.artistProceeds || 0}
                      {/* {loginUserState?.data?.user?.role ===
                      LOGIN_USER_TYPE.VENUE_ADMIN ? (
                        postData?.artistProceeds || 0
                      ) : (
                        <div>
                          <FormGroup className="d-flex gap-5">
                            <InputField
                              {...{
                                register,
                                formState,
                                className: "inputField",
                                id: "artistProceeds",
                                placeholder: "Artist Proceeds",
                                type: "number",
                                value: String(postEditableData.proceeds),
                                onChange: (d: any) => {
                                  setPostEditableData({
                                    ...postEditableData,
                                    proceeds: parseInt(d.target.value)
                                  })
                                  setIsChangedSection('proceed')
                                }
                              }}
                            />

                            {isChangedSection === "proceed" && (
                              <Button
                                htmlType="button"
                                type="primary"
                                className="border-radius-10 w-30 p-2"
                                disabled={isPostDataUpdating}
                                loading={isPostDataUpdating}
                                onClick={onSubmitPostData}
                              >
                                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <circle cx="13" cy="13" r="13" fill="white"/>
                                  <path d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z" fill="#FF6C2C"/>
                                </svg>
                              </Button>
                            )}
                          </FormGroup>
                        </div>
                      )} */}
                    </div>
                  </Col>
                  <Col span={24} className="detail">
                    <div className="label">
                      <Image
                        src="/images/general/tag.png"
                        alt="Total Cost"
                        height={26}
                        width={26}
                        className="h-auto"
                      />
                      Total Cost
                    </div>
                    <div>
                      {loginUserState?.data?.user?.role ===
                      LOGIN_USER_TYPE.VENUE_ADMIN ? (
                        postData?.totalCost || 0
                      ) : (
                        <div>
                          <FormGroup className="d-flex gap-5">
                            <InputField
                              {...{
                                register,
                                formState,
                                className: "inputField",
                                id: "totalCost",
                                placeholder: "Total Cost",
                                type: "number",
                                value: String(postEditableData.total),
                                onChange: (d: any) => {
                                  setPostEditableData({
                                    ...postEditableData,
                                    total: parseInt(d.target.value),
                                  });
                                  setIsChangedSection("total");
                                },
                              }}
                            />

                            {isChangedSection === "total" && (
                              <Button
                                htmlType="button"
                                type="primary"
                                className="border-radius-10 w-30 p-2"
                                disabled={isPostDataUpdating}
                                loading={isPostDataUpdating}
                                onClick={onSubmitPostData}
                              >
                                <svg
                                  width="26"
                                  height="26"
                                  viewBox="0 0 26 26"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <circle cx="13" cy="13" r="13" fill="white" />
                                  <path
                                    d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                                    fill="#FF6C2C"
                                  />
                                </svg>
                              </Button>
                            )}
                          </FormGroup>
                        </div>
                      )}
                    </div>
                  </Col>
                </Row>

                <Row className="mt-4">
                  <Col>
                    <h3>Note</h3>
                  </Col>
                </Row>

                <form onSubmit={handleSubmit(onSubmit)}>
                  <Row align="middle" gutter={[10, 10]}>
                    <Col md={isNoteUpdate ? 21 : 24} xs={24}>
                      <FormGroup className="w-100">
                        <TextAreaField
                          {...{
                            register,
                            formState,
                            className: "w-100 p-2",
                            id: "note",
                            defaultValue: postData?.note || "",
                            placeholder:
                              "Let the client know why you'd be a good fit for this event",
                            onChange: (e) => {
                              setIsNoteUpdate(true);
                            },
                          }}
                        />
                      </FormGroup>
                    </Col>
                    {isNoteUpdate && (
                      <Col md={3} xs={24}>
                        <Button
                          htmlType="submit"
                          type="primary"
                          className="float-right border-radius-10"
                          disabled={isFormLoading || formState?.isSubmitting}
                          loading={isFormLoading || formState?.isSubmitting}
                        >
                          <svg
                            width="26"
                            height="26"
                            viewBox="0 0 26 26"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <circle cx="13" cy="13" r="13" fill="white" />
                            <path
                              d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                              fill="#FF6C2C"
                            />
                          </svg>
                        </Button>
                      </Col>
                    )}
                  </Row>
                </form>

                {loginUserState?.data?.userType === LOGIN_USER_TYPE.ADMIN && (
                  <>
                    <Row>
                      <Col span={24}>
                        <Divider className="mb-2 mt-2" />
                      </Col>
                    </Row>

                    <Promote
                      {...{
                        onPromote: () => {},
                        postId: id,
                        postingDate: DateTime.fromISO(
                          postData?.startDate?.iso,
                          {
                            zone: DEFAULT_TIME_ZONE,
                          }
                        ).toFormat(DATE_TIME_FORMAT),
                      }}
                    />

                    <Row>
                      <Col span={24}>
                        <Divider className="mb-2 mt-2" />
                        <h3>Interested Artists</h3>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={24} className="mt-2 mb-2">
                        <Bids
                          {...{
                            postId: id,
                            onConfirmed: (d) => {
                              handleClose(true);
                              setIsPostingModalOpen(false);
                            },
                          }}
                        />
                      </Col>
                    </Row>
                  </>
                )}
              </>
            </>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default AdminCalendarPostModal;
