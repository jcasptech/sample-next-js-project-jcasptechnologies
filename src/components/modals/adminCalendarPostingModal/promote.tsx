import { Button } from "@components/theme";
import {
  FormGroup,
  InputField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import MultipleChoiceCheckbox from "@components/theme/multiple-choice-checkbox";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { promotePostingAPI } from "@redux/services/calendar.api";
import { Col, Divider, Row } from "antd";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  PromotePostingInputs,
  PromotePostingSchema,
} from "src/schemas/promotePostingSchema";

export interface PromoteProps {
  postId: string;
  postingDate: string;
  onPromote: (d: boolean) => void;
}

const Promote = (props: PromoteProps) => {
  const { postId, onPromote, postingDate } = props;
  const [promotePostingType, setPromotePostingType] = useState<
    "email" | "push" | "text"
  >("email");
  const [isLoading, setIsLoading] = useState(false);

  const { register, handleSubmit, formState, setValue, reset } =
    useForm<PromotePostingInputs>({
      resolver: yupResolver(PromotePostingSchema),
    });

  const onSubmit = async (data: PromotePostingInputs) => {
    setIsLoading(true);
    try {
      const res = await promotePostingAPI({
        postId,
        method: promotePostingType,
        radius: data.radius,
        message: data.message,
      });

      if (res?.message) {
        showToast(SUCCESS_MESSAGES.postingPromotSuccess, "success");
      }
      setIsLoading(false);
      reset();
      onPromote(true);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    setValue("type", promotePostingType);
  }, [promotePostingType]);

  return (
    <div className="">
      <Row className="mt-2 mb-2">
        <Col>
          <h3>Promote Posting to Relevant Artists</h3>
        </Col>
      </Row>

      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={[16, 8]}>
          <Col span={24}>Method</Col>
          <Col sm={8} xs={24}>
            <MultipleChoiceCheckbox
              {...{
                handleOnChange: (d) => {
                  if (d) {
                    setPromotePostingType("email");
                  }
                },
                label: "Email",
                isBeforeLabel: true,
                value: promotePostingType === "email",
              }}
            />
          </Col>

          <Col sm={8} xs={24}>
            <MultipleChoiceCheckbox
              {...{
                handleOnChange: (d) => {
                  if (d) {
                    setPromotePostingType("push");
                  }
                },
                label: "Push",
                isBeforeLabel: true,
                value: promotePostingType === "push",
              }}
            />
          </Col>

          <Col sm={8} xs={24}>
            <MultipleChoiceCheckbox
              {...{
                handleOnChange: (d) => {
                  if (d) {
                    setPromotePostingType("text");
                  }
                },
                label: "Text",
                isBeforeLabel: true,
                value: promotePostingType === "text",
              }}
            />
          </Col>

          <InputField
            {...{
              type: "hidden",
              register,
              formState,
              id: "type",
              defaultValue: promotePostingType,
            }}
          />

          {(promotePostingType === "text" || promotePostingType === "push") && (
            <FormGroup className="w-100">
              <TextAreaField
                {...{
                  register,
                  formState,
                  className: "w-100 p-2",
                  label: "Message",
                  id: "message",
                  placeholder: `Enter the message you'd like to send to relevant artists in the area, or leave empty to use the default message ("A new JCasp gig has been posted for ${postingDate}. Check the app to see if you're a good fit!"). A link to the gig will be automatically included either way.`,
                }}
              />
            </FormGroup>
          )}

          <InputField
            {...{
              type: "number",
              register,
              formState,
              id: "radius",
              className: "inputField",
              label: "Radius(mi)",
              defaultValue: "50",
              placeholder: "Radius",
            }}
          />

          <Col span={24}>
            <Button
              htmlType="submit"
              type="primary"
              className="w-100"
              loading={isLoading || formState.isSubmitting}
              disabled={isLoading || formState.isSubmitting}
            >
              Promote
            </Button>
          </Col>
        </Row>
      </form>

      <Divider className="mb-0 mt-0" />
    </div>
  );
};

export default Promote;
