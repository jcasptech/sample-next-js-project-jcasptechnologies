import { Button } from "@components/theme";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { createProEventAPI } from "@redux/services/calendar.api";
import { getVenuesSearchAPI } from "@redux/services/general.api";
import { SeledtedArtistState } from "@redux/slices/artistList";
import { DatePicker, Divider, TimePicker } from "antd";
import { DateTime } from "luxon";
import moment from "moment";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import Select from "react-select";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import {
  AdminAddEventsFormValidationInputs,
  AdminAddEventsFormValidationSchema,
} from "src/schemas/adminAddEventFormSchema";
import modalStyles from "./adminAddEvent.module.scss";

export interface AdminAddEventModalProps {
  isOpen: boolean;
  handleClose: (data: any) => void;
  setIsNewEventModalOpen: (data: any) => void;
}

const AdminAddEventModal = (props: AdminAddEventModalProps) => {
  const { isOpen, setIsNewEventModalOpen, handleClose } = props;
  const [venueOptions, setVenueOptions] = useState<any>([]);
  const [venueId, setVenueId] = useState("");
  const [isShowLoading, setIsShowModalOpen] = useState(false);
  const [isVenueModalOpen, setIsVenueModalOpen] = useState(false);
  const [variety, setVariety] = useState("");
  const [formats, setFormats] = useState<any>([]);

  const [artistData, setArtistData] = useState<any>([]);
  const [toggaleStatus, setToggaleStatus] = useState<any>(true);

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<AdminAddEventsFormValidationInputs>({
      resolver: yupResolver(AdminAddEventsFormValidationSchema),
    });

  const handleCancel = () => {
    setIsNewEventModalOpen(false);
  };

  const {
    artistList: { isLoading, data: artistListData },
  }: {
    artistList: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    artistList: state.artistList,
  }));

  useEffect(() => {
    if (artistListData) {
      artistListData?.map((artist: any, index: number) => {
        const newData = {
          value: artist.objectId,
          label: artist.name,
        };
        setArtistData((prevData: any) => [...prevData, newData]);
        return null;
      });
    }
  }, [artistListData]);

  const handleFormatsChange = (format: string) => {
    setToggaleStatus(!toggaleStatus);
    let isAvailable = false;
    formats.map((item: any) => {
      if (item === format) {
        isAvailable = true;
      }
    });
    if (isAvailable) {
      const newArr = formats.filter((item: any) => {
        return item !== format;
      });
      setFormats(newArr);
      setValue("formats", newArr.join());
    } else {
      const newArr = formats;
      newArr.push(format);
      setFormats(newArr);
      setValue("formats", newArr.join());
    }
  };

  const handleChangeDate = (e: any) => {
    const formatDate = moment(e);
    const date = formatDate.format("YYYY-MM-DD");
    setValue("bookingDate", date);
  };

  const handleChangeStartTime = (e: any) => {
    const formattedTime = moment(e).format("hh:mm A").toString();
    setValue("startTime", formattedTime);
  };
  const handleChangeEndTime = (e: any) => {
    const formattedTime = moment(e).format("hh:mm A").toString();
    setValue("endTime", formattedTime);
  };

  const handleArtistChange = (data: any) => {
    setValue("artistId", data.value);
  };

  const { apiParam } = useList({
    queryParams: {
      search: "",
    },
  });

  const getVenue = async () => {
    try {
      let response = await getVenuesSearchAPI(apiParam);
      setVenueOptions(response);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getVenue();
  }, [apiParam]);

  const handleVenueChange = (data: any) => {
    if (data) {
      apiParam.search = data;
      getVenue();
    }
  };

  const handleChange = (data: any) => {
    setVenueId(data.value);
    setValue("venueId", data.value);
  };

  const onSubmit = async (data: any) => {
    data["formats"] = data["formats"].split(",");

    if (data) {
      try {
        await createProEventAPI(data);
        setIsNewEventModalOpen(false);
        handleClose(true);
        showToast(SUCCESS_MESSAGES.eventCreatedSuccess, "success");
      } catch (error) {
        setIsNewEventModalOpen(false);
      }
    }
  };

  const disabledDatePast = (current: moment.Moment) => {
    const currentDate = DateTime.fromISO(current.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
      })
      .toUnixInteger();

    const currentWithTimezone = DateTime.fromISO(
      DateTime.utc()
        .set({
          hour: 0,
          minute: 0,
          second: 0,
        })
        .toISO({ includeOffset: false })
    ).toUnixInteger();

    return currentDate.valueOf() < currentWithTimezone.valueOf();
  };

  return (
    <>
      <Modal show={isOpen} onHide={handleCancel}>
        <Modal.Header closeButton className={modalStyles.header}>
          <h2 className="modal-title modal-e text-center" id="inputModalLabel">
            Create an Event
          </h2>
        </Modal.Header>
        <Divider className="m-0" />
        <Modal.Body className={modalStyles.modal_body}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Title</label>
              </div>
              <div className={`${modalStyles.venueSelect} `}>
                <InputField
                  {...{
                    register,
                    formState,
                    id: "title",
                    className: `inputField`,
                    placeholder: "Title",
                  }}
                />
              </div>
            </div>

            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Venue</label>
              </div>
              <div className={`${modalStyles.venueSelect} `}>
                <Select
                  className={`basic_multi_select selectField`}
                  classNamePrefix="Select a Venue"
                  placeholder="Select a Venue"
                  options={venueOptions}
                  onInputChange={handleVenueChange}
                  onChange={handleChange}
                  instanceId="venue"
                />
              </div>
              {formState &&
                formState?.errors &&
                formState?.errors["venueId"] && (
                  <span className="text-danger">
                    {formState?.errors["venueId"]?.message}
                  </span>
                )}
            </div>

            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Artist</label>
              </div>
              <div className={`${modalStyles.venueSelect} `}>
                <Select
                  className={`basic_multi_select selectField`}
                  classNamePrefix="Select a Artist"
                  placeholder="Select a Artist"
                  onChange={(d) => handleArtistChange(d)}
                  options={artistData}
                  instanceId="artist"
                />
              </div>
              {formState &&
                formState?.errors &&
                formState?.errors["artistId"] && (
                  <span className="text-danger">
                    {formState?.errors["artistId"]?.message}
                  </span>
                )}
            </div>

            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Variety</label>
              </div>
              <div className={`${modalStyles.venueSelect} `}>
                <div className="row">
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe 
                      ${modalStyles.checkbox} 
                      ${variety === "corporate" ? "checked" : ""}`}
                      onClick={() => {
                        setVariety("corporate");
                        setValue("variety", "corporate");
                      }}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="corporateBox"
                          className=""
                        />
                        Corporate
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe ${modalStyles.checkbox} ${
                        variety === "private" ? "checked" : ""
                      }`}
                      onClick={() => {
                        setVariety("private");
                        setValue("variety", "private");
                      }}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="privateBox"
                          className=""
                        />
                        Private
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe ${modalStyles.checkbox} ${
                        variety === "wedding" ? "checked" : ""
                      }`}
                      onClick={() => {
                        setVariety("wedding");
                        setValue("variety", "wedding");
                      }}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="weddingbox"
                          className=""
                        />
                        Wedding
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe ${modalStyles.checkbox} ${
                        variety === "restaurant" ? "checked" : ""
                      }`}
                      onClick={() => {
                        setVariety("restaurant");
                        setValue("variety", "restaurant");
                      }}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="restaurantbox"
                          className=""
                        />
                        Restaurant
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe ${modalStyles.checkbox} ${
                        variety === "music venue" ? "checked" : ""
                      }`}
                      onClick={() => {
                        setVariety("music venue");
                        setValue("variety", "music venue");
                      }}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="music"
                          className=""
                        />
                        Music Venue
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe ${modalStyles.checkbox} ${
                        variety === "other" ? "checked" : ""
                      }`}
                      onClick={() => {
                        setVariety("other");
                        setValue("variety", "other");
                      }}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="otherbox"
                          className=""
                        />
                        Other
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {formState &&
                formState?.errors &&
                formState?.errors["variety"] && (
                  <span className="text-danger">
                    {formState?.errors["variety"]?.message}
                  </span>
                )}
            </div>

            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Preferred Formats</label>
              </div>
              <div className={`${modalStyles.venueSelect} `}>
                <div className={`row  ${modalStyles.formatsDiv}`}>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      onClick={() => handleFormatsChange("soloist")}
                      className={`input-group checkMe 
                      ${modalStyles.checkbox}
                      ${formats.includes("soloist") ? "checked" : ""}`}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="soloist"
                          className=""
                        />
                        Soloist
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe ${modalStyles.checkbox} ${
                        formats.includes("band") ? "checked" : ""
                      }`}
                      onClick={() => handleFormatsChange("band")}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="band"
                          className=""
                        />
                        Band
                      </div>
                    </div>
                  </div>
                  <div className="col-12 col-sm-6 col-lg-4 Tickchk">
                    <div
                      className={`input-group checkMe ${modalStyles.checkbox} ${
                        formats.includes("dj") ? "checked" : ""
                      }`}
                      onClick={() => handleFormatsChange("dj")}
                    >
                      <div className="checkbox">
                        <input
                          type="checkbox"
                          name="checkboxes"
                          value="1"
                          id="dj"
                          className=""
                        />
                        DJ
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {formState &&
                formState?.errors &&
                formState?.errors["formats"] && (
                  <span className="text-danger">
                    {formState?.errors["formats"]?.message}
                  </span>
                )}
            </div>

            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Total Cost</label>
              </div>
              <div className={`${modalStyles.venueSelect} `}>
                <InputField
                  {...{
                    register,
                    formState,
                    id: "totalCost",
                    className: `inputField`,
                    placeholder: "Total Cost",
                    type: "number",
                  }}
                />
              </div>
            </div>

            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Artist Proceeds</label>
              </div>
              <div className={`${modalStyles.venueSelect} `}>
                <InputField
                  {...{
                    register,
                    formState,
                    id: "artistProceeds",
                    className: `inputField`,
                    placeholder: "Artist Proceeds",
                    type: "number",
                  }}
                />
              </div>
            </div>

            <div className={`${modalStyles.inputLabel} `}>
              <div className={`${modalStyles.venueSelectlabel} `}>
                <label>Date</label>
              </div>

              <div className={`${modalStyles.venueSelect} `}>
                <div className={`formB ${modalStyles.DateSelect}`}>
                  <DatePicker
                    className={modalStyles.datePicker}
                    placeholder="Select Date"
                    onChange={(e) => handleChangeDate(e)}
                    disabledDate={(currentDate) => {
                      return disabledDatePast(currentDate);
                    }}
                  />
                  {formState &&
                    formState?.errors &&
                    formState?.errors["bookingDate"] && (
                      <span className="text-danger">
                        {formState?.errors["bookingDate"]?.message}
                      </span>
                    )}
                </div>
                <div
                  className={`shows-venue formB col-md-12 col-12  ${modalStyles.TimeSelect}`}
                >
                  <TimePicker
                    format="hh:mm A"
                    placeholder="Start time"
                    className={modalStyles.datePicker}
                    onChange={(e) => handleChangeStartTime(e)}
                  />
                  {formState &&
                    formState?.errors &&
                    formState?.errors["startTime"] && (
                      <span className="text-danger">
                        {formState?.errors["startTime"]?.message}
                      </span>
                    )}
                </div>
                <div
                  className={`shows-venue formB col-md-12 col-12  ${modalStyles.TimeSelect}`}
                >
                  <TimePicker
                    format="hh:mm A"
                    placeholder="End time"
                    className={modalStyles.datePicker}
                    onChange={(e) => handleChangeEndTime(e)}
                  />
                  {formState &&
                    formState?.errors &&
                    formState?.errors["endTime"] && (
                      <span className="text-danger">
                        {formState?.errors["endTime"]?.message}
                      </span>
                    )}
                </div>
              </div>
            </div>

            <div className="modal-footer shows-btn mt-5">
              <Button
                htmlType="button"
                type="ghost"
                className="float-right"
                onClick={handleCancel}
                disabled={isShowLoading}
              >
                Close
              </Button>
              <div className={modalStyles.request_btn}>
                <Button
                  htmlType="submit"
                  type="primary"
                  className="float-right"
                  loading={isShowLoading}
                  disabled={isShowLoading}
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default AdminAddEventModal;
