import { Button } from "@components/theme/button";
import CustomCheckbox from "@components/theme/checkbox";
import {
  FormGroup,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { artistWriteRatingAPI } from "@redux/services/artist.api";
import { ArtistObject } from "@redux/slices/artists";
import { LoginUserState } from "@redux/slices/auth";
import { Col, Divider, Rate, Row } from "antd";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ArtistMessageInputs,
  ArtistMessageSchema,
} from "src/schemas/artistMessageSchema";
import { LoginModal } from "../loginModal";
import modalstyle from "./artistRating.module.scss";
export interface ArtistRatingModalProps {
  artistDetail: ArtistObject;
  isOpen: boolean;
  setIsRatingModalOpen: (data: any) => void;
}
const ArtistRatingModal = (props: ArtistRatingModalProps) => {
  const { setIsRatingModalOpen, artistDetail } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [rating, setRating] = useState(0);
  const [isChecked, setIsChecked] = useState(false);

  const [userAction, setUserAction] = useState<"artistShoutouts" | null>(null);
  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [tmpData, setTmpData] = useState<any>(null);

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const { register, handleSubmit, formState, setFocus, reset } =
    useForm<ArtistMessageInputs>({
      resolver: yupResolver(ArtistMessageSchema),
    });

  // useEffect(() => {
  //   if (isOpen) {
  //     if (loginUserState?.isLogin) {
  //       const userId = loginUserState?.data?.user?.objectId;
  //       if (userId) {
  //         setIsModalOpen(isOpen);
  //       }
  //     } else {
  //       setIsRatingModalOpen(false);
  //       showToast(SUCCESS_MESSAGES.requiredLoginRatingMessage, "warning");
  //     }
  //   }
  // }, [isOpen]);

  const onSubmit = async (e: any) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      try {
        setIsLoading(false);
        await artistWriteRatingAPI({
          artistId: artistDetail.objectId,
          userId: userId,
          rating: rating,
          comment: e.message,
          anonymous: isChecked,
        });
        showToast(SUCCESS_MESSAGES.sendArtistRatingMessage, "success");
        setIsRatingModalOpen(false);
      } catch (e: any) {
        setIsLoading(false);
        console.log(e, "catch");
      }
    } else {
      setIsLoginModalOpen(true);
      setUserAction("artistShoutouts");
      setTmpData(e);
    }
  };

  const handleCancel = () => {
    setIsRatingModalOpen(false);
  };
  const handleChange = (e: any) => {
    setRating(e);
  };

  const handleCheckboxChange = (event: any) => {
    if (event?.target?.checked) {
      setIsChecked(event.target.checked);
    } else {
      setIsChecked(!isChecked);
    }
  };

  return (
    <>
      <Modal className={modalstyle.modal} size="sm" show onHide={handleCancel}>
        <h2>Send a Shoutout</h2>
        <Modal.Header closeButton className={modalstyle.header}></Modal.Header>
        <Modal.Body className={`${modalstyle.body}`}>
          <Row className="mt-4" justify={"center"}>
            <Col span={24} className="">
              <form className="formB" onSubmit={handleSubmit(onSubmit)}>
                <Rate
                  allowHalf
                  onChange={(e) => handleChange(e)}
                  defaultValue={0}
                  className={modalstyle.rateImage}
                />

                <FormGroup className={`mb-3 w-100`}>
                  <TextAreaField
                    {...{
                      register,
                      formState,
                      id: "message",
                      label: "MESSAGE",
                      className: "w-100 p-2",
                      placeholder:
                        "Did a performance or a musical moment stand out to you? Share your appreciation here. Give a shoutout to the artist and let them know how their music touched you.",
                    }}
                  />
                </FormGroup>
                <FormGroup className={`mb-3 w-100`}>
                  <CustomCheckbox
                    {...{
                      handleOnChange: handleCheckboxChange,
                      label: "I would like my review to be anonymous",
                      value: isChecked,
                      isBeforeLabel: true,
                    }}
                  />
                </FormGroup>

                <Divider />
                <div>
                  <Button
                    htmlType="submit"
                    type="primary"
                    disabled={formState?.isSubmitting || isLoading}
                    loading={formState?.isSubmitting || isLoading}
                  >
                    Send a Shoutout
                  </Button>
                </div>
              </form>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
      {isLoginModalOpen && userAction && (
        <LoginModal
          {...{
            actionFrom: userAction,
            handleOk: () => {
              setIsLoginModalOpen(false);
              setUserAction(null);
              onSubmit(tmpData);
            },
            isOpen: isLoginModalOpen,
            handleCancel: () => {
              setIsLoginModalOpen(false);
            },
          }}
        />
      )}
    </>
  );
};
export default ArtistRatingModal;
