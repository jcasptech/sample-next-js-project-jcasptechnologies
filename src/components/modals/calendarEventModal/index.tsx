import { Button, DefaultLoader } from "@components/theme";
import { TextAreaField } from "@components/theme/form/formFieldsComponent";
import { EmailIcon } from "@components/theme/icons/emailIcon";
import { PhoneIcon } from "@components/theme/icons/phoneIcon";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import {
  getEventFullDetailAPI,
  sendExpeditedPayment,
  updateCalendarEventAPI,
} from "@redux/services/calendar.api";
import { LoginUserState } from "@redux/slices/auth";
import { Col, Divider, Row } from "antd";
import { DateTime } from "luxon";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import { EventInputs, EventSchema } from "src/schemas/eventSchema";
import Swal from "sweetalert2";
import EventModalStyles from "./eventModal.module.scss";

export interface CalendarEventProps {
  isOpen: boolean;
  id: string | null;
  setIsEventModalOpen: (data: any) => void;
  handleClose: (data: any) => void;
}
const CalendarEventModal = (props: CalendarEventProps) => {
  const { isOpen, id, setIsEventModalOpen, handleClose } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [isFormLoading, setIsFormLoading] = useState(false);
  const [showExpeditedPayment, setShowExpeditedPayment] = useState(true);
  const [eventData, setEventData] = useState<any>();
  const [paymentPreferance, setPaymentPreferance] = useState<
    "paypal" | "venmo" | ""
  >("");
  const [newPaymentPre, setNewPaymentPre] = useState("");

  const { register, handleSubmit, formState, control, reset } =
    useForm<EventInputs>({
      resolver: yupResolver(EventSchema),
    });

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const handlePaymentPreferance = (d: any) => {
    if (eventData?.bookingStatus === "confirmed") {
      if (d === "vemno") {
        setPaymentPreferance(d);
      } else {
        setPaymentPreferance(d);
      }
    }
  };

  const handleCancel = () => {
    setIsEventModalOpen(false);
  };

  const getFullDetail = async () => {
    if (id) {
      try {
        const data = await getEventFullDetailAPI(id);
        if (data?.requestExpeditedPayment === true) {
          setShowExpeditedPayment(true);
        } else {
          setShowExpeditedPayment(false);
        }
        setEventData(data);
        setPaymentPreferance(data?.artist?.banking?.paymentPreference || "");
        setNewPaymentPre(data?.artist?.banking?.paymentPreference || "");
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
      }
    }
  };

  const handleRequestExpeditedPayment = async () => {
    if (id) {
      setIsFormLoading(true);
      try {
        await sendExpeditedPayment(id);
        showToast(SUCCESS_MESSAGES.expeditedPayment, "success");
        setIsEventModalOpen(false);
        setShowExpeditedPayment(false);
        setIsFormLoading(false);
      } catch (error: any) {
        setIsFormLoading(false);
      }
    }
  };

  useEffect(() => {
    if (id) {
      getFullDetail();
    }
  }, [id]);

  useEffect(() => {
    let updateEve = async () => {
      if (paymentPreferance !== newPaymentPre) {
        setIsFormLoading(true);
        try {
          await updateCalendarEventAPI(id, {
            paymentMethod: paymentPreferance,
          });
          showToast(SUCCESS_MESSAGES.eventUpdateSuccess, "success");
          handleClose(true);
          handleCancel();
          setIsFormLoading(false);
        } catch (error) {
          console.log(error);
          handleCancel();
          setIsFormLoading(false);
        }
      }
    };
    updateEve();
  }, [paymentPreferance]);

  const onSubmit = async (data: any) => {
    if (paymentPreferance && data) {
      setIsFormLoading(true);
      try {
        await updateCalendarEventAPI(id, data);
        showToast(SUCCESS_MESSAGES.eventUpdateSuccess, "success");
        handleClose(true);
        handleCancel();
        setIsFormLoading(false);
      } catch (error) {
        console.log(error);
        handleClose(true);
        handleCancel();
        setIsFormLoading(false);
      }
    }
  };

  const handleSweetalert = async () => {
    Swal.fire({
      title: "Are you sure you want to request expedited payment?",
      text: "The expedited payment is 10% fee.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.isConfirmed) {
        handleRequestExpeditedPayment();
      }
    });
  };

  return (
    <>
      <Modal
        show={isOpen}
        className={EventModalStyles.modal}
        onHide={handleCancel}
        size="lg"
      >
        <h2> JCasp PRO Booking</h2>
        <Modal.Header
          closeButton
          className={`${EventModalStyles.header} ${eventData?.bookingStatus}`}
        >
          <h3>
            PRO Booking: {eventData?.title}
            <p>
              {eventData?.startDate && eventData?.startDate?.iso && (
                <>
                  {DateTime.fromISO(eventData?.startDate?.iso)
                    .setZone(eventData?.timeZone)
                    .toFormat("EEEE, MMMM d, yyyy")}{" "}
                  from{" "}
                  {DateTime.fromISO(eventData?.startDate?.iso)
                    .setZone(eventData?.timeZone)
                    .toFormat("h:mm a")}{" "}
                  -{" "}
                  {DateTime.fromISO(eventData?.endDate?.iso)
                    .setZone(eventData?.timeZone)
                    .toFormat("h:mm a")}
                </>
              )}
            </p>
          </h3>
        </Modal.Header>
        <Modal.Body className={`${EventModalStyles.body}`}>
          {isLoading && <DefaultLoader />}
          {!isLoading && (
            <>
              <Row className="mt-0">
                <Col>
                  <h3>Event Information</h3>
                </Col>
              </Row>

              <Row className="event-infomation">
                <Col span={20}>
                  <p className="cursor-pointer">
                    <Link
                      className="linksAv"
                      target={"_blank"}
                      href={`/artist/${eventData?.artist?.vanity}`}
                    >
                      <label>Artist:</label>{" "}
                      {capitalizeString(eventData?.artist?.name)}
                    </Link>
                  </p>
                  {eventData?.venue ? (
                    <div>
                      <p>
                        <Link
                          className="linksAv"
                          target={"_blank"}
                          href={`/venue/${eventData?.venue?.vanity}`}
                        >
                          <label>Venue:</label>{" "}
                          {capitalizeString(eventData?.venue?.name)}
                        </Link>
                      </p>
                      <p>
                        <label>Address:</label> {eventData?.venue?.address}
                      </p>
                    </div>
                  ) : (
                    ""
                  )}
                  {eventData?.venue?.advanceSheet?.url && (
                    <Link
                      href={
                        eventData?.venue?.advanceSheet?.url ||
                        "javascript:void(0)"
                      }
                      target="_blank"
                    >
                      Advance Sheet Url
                    </Link>
                  )}
                </Col>
                <Col span={4}>
                  {eventData?.venue?.iconImage?.url && (
                    <Image
                      src={
                        eventData?.venue?.iconImage?.url ||
                        "/images/venue-placeholder.png"
                      }
                      alt="venue Image"
                      height={112}
                      width={112}
                    />
                  )}
                </Col>
              </Row>

              {/* <Row className="mt-4">
                <Col>
                  <h3>Artist Information</h3>
                </Col>
              </Row>

              <Row className="artist-infomation">
                <Col span={20}>
                  <div className="artists mb-2">
                    <div>
                      <label>Artist Name: </label>
                      <p>{capitalizeString(eventData?.artist?.name)}</p>
                    </div>
                    <div>
                      <label>Artist Type: </label>
                      <p>{capitalizeString(eventData?.artist?.format)}</p>
                    </div>
                  </div>
                  {eventData?.artist?.phone && (
                    <p>
                      <img
                        src="/images/general/phone.png"
                        className="no-border-radius"
                      />
                      {eventData?.artist?.phone}
                    </p>
                  )}

                  {eventData?.artist?.email && (
                    <p>
                      <img
                        src="/images/general/email.png"
                        className="no-border-radius"
                      />{" "}
                      {eventData?.artist?.email}
                    </p>
                  )}
                </Col>
                <Col span={4}>
                  {eventData?.artist?.iconImage?.url && (
                    <img
                      src={
                        eventData?.artist?.iconImage?.url ||
                        "/images/artist-placeholder.png"
                      }
                    />
                  )}
                </Col>
              </Row> */}

              <Row className="mt-4">
                <Col>
                  <h3>Booking PRO Contact</h3>
                </Col>
              </Row>
              <Row className="booking-contact">
                <Col>
                  <p>
                    <label> Name: </label> Joe Cardillo
                  </p>

                  <div className="d-flex mt-3">
                    <p className="me-2">
                      <EmailIcon /> Joe@jcasptechnologies.com
                    </p>
                    <p className="ms-2">
                      <PhoneIcon /> (510) 400-6477
                    </p>
                  </div>
                </Col>
              </Row>

              <Row className="mt-4">
                <Col>
                  <h3>Payment</h3>
                </Col>
              </Row>
              <Row className="payment-information">
                <Col className="budget">
                  <div>
                    <label>Artist Proceeds:</label>
                    <p>$ {eventData?.artistProceeds || 0}</p>
                  </div>

                  <div>
                    <label>Paidout:</label>
                    <p>{eventData?.transaction?.paidout ? "Yes" : "No"}</p>
                  </div>

                  <div>
                    <label>Estimated Payout:</label>
                    <p>
                      {DateTime.fromISO(eventData?.startDate?.iso)
                        .setZone(eventData?.timeZone)
                        .plus({ days: 30 })
                        .toFormat("MM/dd/yyyy")}
                    </p>
                  </div>
                </Col>

                <div className="paymentContainer">
                  <div className="paymentM">
                    <span
                      onClick={() => handlePaymentPreferance("paypal")}
                      className={
                        eventData?.bookingStatus === "completed"
                          ? ""
                          : "cursor-pointer"
                      }
                    >
                      <Image
                        src={
                          paymentPreferance === "paypal"
                            ? "/images/general/checked-checkbox.png"
                            : "/images/general/un-checked-checkbox.png"
                        }
                        className="checkboxImg no-border-radius"
                        alt="Payment Preferance"
                        width={23}
                        height={24}
                      />
                    </span>
                    <span>
                      <Image
                        src="/images/general/paypal.png"
                        className="no-border-radius"
                        alt="paypal"
                        width={15}
                        height={14}
                      />
                      {/* <img
                        src="/images/general/paypal.png"
                        className="no-border-radius"
                      /> */}
                    </span>
                    <span>PayPal</span>
                  </div>

                  <div className="paymentM">
                    <span
                      onClick={() => handlePaymentPreferance("venmo")}
                      className={
                        eventData?.bookingStatus === "completed"
                          ? ""
                          : "cursor-pointer"
                      }
                    >
                      <Image
                        src={
                          paymentPreferance === "venmo"
                            ? "/images/general/checked-checkbox.png"
                            : "/images/general/un-checked-checkbox.png"
                        }
                        className="checkboxImg no-border-radius"
                        alt="Payment Preferance"
                        width={23}
                        height={24}
                      />
                    </span>
                    <span>
                      <Image
                        src="/images/general/vimeo.png"
                        className="no-border-radius"
                        alt="vimeo"
                        width={15}
                        height={14}
                      />
                      {/* <img
                        src="/images/general/vimeo.png"
                        className="no-border-radius"
                      /> */}
                    </span>
                    <span>Venmo</span>
                  </div>
                </div>
                <Col className="expeditedPy">
                  <Button
                    htmlType="button"
                    disabled={
                      showExpeditedPayment ||
                      isFormLoading ||
                      eventData?.bookingStatus === "completed"
                    }
                    loading={isFormLoading}
                    onClick={handleSweetalert}
                  >
                    Request Expedited Payment
                  </Button>
                </Col>
                {/* <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="logTips">
                    <label>Log Tips: $</label>
                    <InputField
                      {...{
                        register,
                        formState,
                        className: "inputField",
                        id: "logTips",
                        placeholder: "Log Tips",
                        type: "number",
                        defaultValue: eventData?.logTips || 0,
                        disabled:
                          loginUserState.data?.userType ===
                          LOGIN_USER_TYPE.ADMIN,
                      }}
                    />
                    {eventData?.bookingStatus === "confirmed" && (
                      <Button
                        type="submit"
                        className={`hvr-float-shadow mt-1 showPy`}
                        disabled={isFormLoading || formState?.isSubmitting}
                      >
                        Save
                      </Button>
                    )}
                  </div>
                </form> */}
              </Row>

              <Row className="mt-4"></Row>

              <Row className="mt-2">
                <Col>
                  <h3>Note</h3>
                  {/* <span>Traffic might be heavy, leave a little early…</span> */}
                </Col>
              </Row>
              <form onSubmit={handleSubmit(onSubmit)}>
                <TextAreaField
                  {...{
                    register,
                    formState,
                    className: "notes",
                    defaultValue: eventData.note || "",
                    id: "note",
                    placeholder:
                      "Enter some additional information about the event...",
                  }}
                />
                {eventData?.bookingStatus === "confirmed" && (
                  <Row>
                    <Col className="expeditedPy">
                      <Button
                        htmlType="submit"
                        type="primary"
                        disabled={isFormLoading || formState?.isSubmitting}
                        loading={isFormLoading || formState?.isSubmitting}
                      >
                        Save Note
                      </Button>
                    </Col>
                  </Row>
                )}
              </form>
              <Divider />
              {isFormLoading && <DefaultLoader />}
            </>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default CalendarEventModal;
