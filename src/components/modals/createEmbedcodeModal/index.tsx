import { Button } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { createEmbedCodeAPI } from "@redux/services/embed-code.api";
import { getVenuesSearchAPI } from "@redux/services/general.api";
import { SeledtedArtistState } from "@redux/slices/artistList";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import Select from "react-select";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import {
  CreateEmbedCodeFormValidationProps,
  CreateEmbedCodeFormValidationSchema,
} from "src/schemas/createEmbedCodeFormSchema";
import modalStyle from "./createEmbed.module.scss";

export interface CreateEmbedcodeModalProps {
  isOpen: boolean;
  setIsCreateEmbedcodeOpen: (d: any) => void;
  handleClose: (d: any) => void;
}
const CreateEmbedcodeModal = (props: CreateEmbedcodeModalProps) => {
  const { isOpen, setIsCreateEmbedcodeOpen, handleClose } = props;
  const [isSubmitLoading, setIsSubmitLoading] = useState(false);

  const [selectedCategory, setSelectedCategory] = useState("venue");
  const [venueOptions, setVenueOptions] = useState<any>([]);
  const [venueId, setVenueId] = useState("");

  const { register, handleSubmit, formState, setValue, reset } =
    useForm<CreateEmbedCodeFormValidationProps>({
      resolver: yupResolver(CreateEmbedCodeFormValidationSchema),
    });

  const handleCancel = () => {
    setIsCreateEmbedcodeOpen(false);
  };

  const categoryOptions: any = [
    { value: "artist", label: "Artist" },
    { value: "venue", label: "Venue" },
  ];

  const defaultOption: any = [{ value: "venue", label: "Venue" }];

  useEffect(() => {
    setValue("category", "venue");
  }, []);

  const { apiParam } = useList({
    queryParams: {
      search: "",
    },
  });

  const getVenue = async () => {
    try {
      let response = await getVenuesSearchAPI(apiParam);
      setVenueOptions(response);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getVenue();
  }, [apiParam]);

  const handleVenueChange = (data: any) => {
    if (selectedCategory === "venue") {
      if (data) {
        apiParam.search = data;
        getVenue();
      }
    }
  };

  const handleCategoryChange = (data: any) => {
    reset();
    setValue("category", data.value);
    setSelectedCategory(data.value);
  };

  const handleChange = (data: any) => {
    setVenueId(data.value);
    setValue("Id", data.value);
  };

  const {
    artistList: { isLoading, data: artistListData },
  }: {
    artistList: SeledtedArtistState;
  } = useSelector((state: RootState) => ({
    artistList: state.artistList,
  }));

  useEffect(() => {
    if (selectedCategory === "artist" && artistListData) {
      const newData: any = [];
      artistListData?.map((artist: any, index: number) => {
        newData.push({
          value: artist.objectId,
          label: artist.name,
        });
        return null;
      });
      setVenueOptions(newData);
    } else {
      getVenue();
    }
  }, [selectedCategory]);

  const onSubmit = (data: any) => {
    setIsSubmitLoading(true);
    if (data) {
      try {
        const res = createEmbedCodeAPI(data);
        showToast(SUCCESS_MESSAGES.embedCodeCreateSuccess, "success");
        handleClose(res);
        setIsSubmitLoading(false);
        setIsCreateEmbedcodeOpen(false);
      } catch (error) {
        console.log(error);
        setIsSubmitLoading(false);
      }
    }
  };

  return (
    <>
      <Modal
        className={`modal ${modalStyle.modal}`}
        show={isOpen}
        onHide={handleCancel}
      >
        <Modal.Header closeButton className={modalStyle.header}>
          <h1>Generate New Embed Code</h1>
        </Modal.Header>

        <Modal.Body>
          <div className="">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="venueSelectlabel">
                <label>CATEGORY</label>
              </div>
              <div className="venueSelect">
                <Select
                  className={`basic_multi_select selectField`}
                  classNamePrefix="Select a Venue"
                  placeholder="Select a Venue"
                  options={categoryOptions}
                  onChange={handleCategoryChange}
                  defaultValue={defaultOption}
                  instanceId="venue"
                />
                {formState &&
                  formState?.errors &&
                  formState?.errors["category"]?.message && (
                    <span className="text-danger">
                      {formState?.errors["category"]?.message}
                    </span>
                  )}
              </div>

              <div className="venueSelectlabel">
                <label>
                  {selectedCategory === "venue" ? "VENUE" : "ARTIST"}
                </label>
              </div>
              <div className="venueSelect">
                <Select
                  className={`basic_multi_select selectField`}
                  classNamePrefix="Select a Venue"
                  placeholder={
                    selectedCategory === "venue"
                      ? "Select a Venue"
                      : "Select a Artist"
                  }
                  options={venueOptions}
                  onInputChange={handleVenueChange}
                  onChange={handleChange}
                  instanceId="select_venue"
                />
                {formState &&
                  formState?.errors &&
                  formState?.errors["Id"]?.message && (
                    <span className="text-danger">
                      {formState?.errors["Id"]?.message}
                    </span>
                  )}
              </div>
              <div className="modal-footer shows-btn mt-5">
                <Button
                  type="ghost"
                  htmlType="button"
                  className="float-right"
                  data-bs-dismiss="modal"
                  onClick={handleCancel}
                >
                  Close
                </Button>
                <div>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="flaot-right"
                    loading={isSubmitLoading}
                    disabled={isSubmitLoading}
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default CreateEmbedcodeModal;
