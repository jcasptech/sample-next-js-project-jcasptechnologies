import Modal from "react-bootstrap/Modal";
import modalstyle from "../modal.module.scss";
import React, { useState, useEffect } from "react";
import ArtistVideo from "@components/artist/artistVideo";
import videoSongStyles from "./videoSongModal.module.scss";

export interface VideoModalProps {
  isVideoModalOpen: boolean;
  setIsVideoModalOpen: (data: any) => void;
  youtubeId?: string;
  setYoutubeId: (d: any) => void;
}
const VideoModal = (props: VideoModalProps) => {
  const { isVideoModalOpen, setIsVideoModalOpen, youtubeId, setYoutubeId } =
    props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [videoUrl, setVideoUrl] = useState("");

  useEffect(() => {
    if (youtubeId) {
      setIsModalOpen(true);
      setVideoUrl(`https://www.youtube.com/embed/${youtubeId}`);
    }
  }, [isVideoModalOpen]);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsVideoModalOpen(false);
    setYoutubeId("");
  };

  return (
    <>
      <Modal
        show={isModalOpen}
        onHide={handleCancel}
        className={videoSongStyles.main}
      >
        <Modal.Header closeButton className={modalstyle.header}>
          <h4 className="modal-title text-xs-center text-center modal-e text-raven">
            Video
          </h4>
        </Modal.Header>
        <Modal.Body className={`${modalstyle.modal_body} text-center`}>
          <ArtistVideo
            {...{
              videoUrl,
              className: `${modalstyle.videoContaint}`,
            }}
          />
        </Modal.Body>
      </Modal>
    </>
  );
};
export default VideoModal;
