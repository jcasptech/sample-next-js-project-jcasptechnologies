import { Button, DefaultLoader } from "@components/theme";
import { TextAreaField } from "@components/theme/form/formFieldsComponent";
import { GoogleCalendarIcon } from "@components/theme/icons/googleCalendarIcon";
import { IOSCalendarIcon } from "@components/theme/icons/iosCalendarIcon";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import {
  deleteShowAPI,
  getShowFullDetailAPI,
  updateShowAPI,
} from "@redux/services/calendar.api";
import { LoginUserState } from "@redux/slices/auth";
import { Col, Divider, Popconfirm, Row } from "antd";
import { DateTime } from "luxon";
import Link from "next/link";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import {
  LOGIN_USER_TYPE,
  SITE_URL,
  SUCCESS_MESSAGES,
} from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import {
  ShowUpdateInputs,
  ShowUpdateSchema,
} from "src/schemas/showUpdateSchema";
import ShareThisModal from "../ShareThisModal";
import CalendarShowsStyle from "./CalendarShowsModal.module.scss";
import { v4 } from "uuid";
import Image from "next/image";

export interface CalendarShowModalProps {
  isOpen: boolean;
  id: string | null;
  handleClose: (data: any) => void;
  setIsShowModalOpen: (data: any) => void;
}

const CalendarShowsModal = (props: CalendarShowModalProps) => {
  const { isOpen, id, setIsShowModalOpen, handleClose } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [showData, setShowData] = useState<any>();
  const [isFormLoading, setIsFormLoading] = useState(false);
  const [promoteOnJCasp, setPromoteOnJCasp] = useState(false);
  const [icsFileContent, setIcsFileContent] = useState<string | null>(null);

  const [url, setUrl] = useState("");
  const [isShareModalOpen, setIsShareModalOpen] = useState(false);

  const { register, handleSubmit, formState, control, reset } =
    useForm<ShowUpdateInputs>({
      resolver: yupResolver(ShowUpdateSchema),
    });

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const handleCancel = () => {
    setIsShowModalOpen(false);
  };

  const onSubmit = async (data: any) => {
    setIsFormLoading(true);
    try {
      if (id) {
        data["showId"] = id;
      }
      data["promoteOnJCasp"] = promoteOnJCasp ? "true" : "false";
      await updateShowAPI(data);
      showToast(SUCCESS_MESSAGES.updateShow, "success");
      handleClose(true);
      setIsShowModalOpen(false);
      setIsFormLoading(false);
    } catch (error) {
      setIsFormLoading(false);
    }
  };

  const handlepromoteOnJCasp = () => {
    if (loginUserState.data?.userType === LOGIN_USER_TYPE.ADMIN) {
    } else {
      setPromoteOnJCasp(!promoteOnJCasp);
    }
  };

  const handleDeleteShow = async (data: any) => {
    if (data) {
      setIsFormLoading(true);
      try {
        await deleteShowAPI({ showId: data });
        showToast(SUCCESS_MESSAGES.deleteShow, "success");
        handleClose(true);
        setIsShowModalOpen(false);
        setIsFormLoading(false);
      } catch (err: any) {
        setIsFormLoading(false);
      }
    }
  };

  const getFullDetail = async () => {
    if (id) {
      setIsLoading(true);
      try {
        const data = await getShowFullDetailAPI(id);
        setShowData(data);
        setPromoteOnJCasp(data?.promoteOnJCasp);
        setIsLoading(false);
      } catch (err: any) {
        setIsLoading(false);
        handleClose(false);
        setIsShowModalOpen(false);
      }
    }
  };

  useEffect(() => {
    if (showData) {
      const icsFile = `BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//jcasptechnologies.com//JCasp Calendar 1.0//EN\nCALSCALE:GREGORIAN\nMETHOD:PUBLISH\nBEGIN:VEVENT\nSUMMARY:${
        showData?.name || '""'
      }\nUID:${v4()}\nSTATUS:CONFIRMED\nTRANSP:TRANSPARENT\nDTSTART:${DateTime.fromISO(
        showData?.startDate?.iso
      )
        .setZone("utc")
        .toFormat("yyyyMMdd'T'HHmmss")}\nDTEND:${DateTime.fromISO(
        showData?.startDate?.iso
      )
        .setZone("utc")
        .plus({ hours: 2 })
        .toFormat("yyyyMMdd'T'HHmmss")}\nLOCATION:${
        showData?.venue?.address || ""
      }\nDESCRIPTION:${showData?.note || '""'}\nDTSTART;TZID="${
        showData?.timeZone || "America/Los_Angeles"
      }":${DateTime.fromISO(showData?.startDate?.iso)
        .setZone(showData?.timeZone || "America/Los_Angeles")
        .toFormat("yyyyMMdd'T'HHmmss")}\nDTEND;TZID="${
        showData?.timeZone || "America/Los_Angeles"
      }":${DateTime.fromISO(showData?.startDate?.iso)
        .setZone(showData?.timeZone || "America/Los_Angeles")
        .plus({ hours: 2 })
        .toFormat("yyyyMMdd'T'HHmmss")}\nEND:VEVENT\nEND:VCALENDAR`;
      setIcsFileContent(icsFile);
    }
  }, [showData]);

  useEffect(() => {
    if (id) {
      getFullDetail();
    }
  }, [id]);

  const handleShareModalOpen = (data: any) => {
    if (data) {
      setIsShareModalOpen(true);
      setUrl(data);
    }
  };

  return (
    <>
      {isShareModalOpen && (
        <ShareThisModal
          {...{
            isOpen: isShareModalOpen,
            url: url || "",
            setIsShareModalOpen,
          }}
        />
      )}

      <Modal
        show={isOpen}
        className={CalendarShowsStyle.modal}
        onHide={handleCancel}
        size="lg"
      >
        <h2>Show</h2>
        <Modal.Header closeButton className={`${CalendarShowsStyle.header}`}>
          <h3>
            {showData?.name}
            <p className="mb-1">
              {showData && showData?.startDate?.iso && (
                <>
                  {/* {DateTime.fromISO(showData?.startDate?.iso, {
                    zone: "utc",
                  }).toFormat("EEEE, MMMM d, y @ h:mm a")} */}
                  {DateTime.fromISO(showData?.startDate?.iso)
                    .setZone(showData?.timeZone || "America/Los_Angeles")
                    .toFormat("EEEE, MMMM d, y @ h:mm a")}
                </>
              )}
            </p>
          </h3>
        </Modal.Header>
        <Modal.Body className={`${CalendarShowsStyle.body}`}>
          {isLoading && <DefaultLoader />}
          {!isLoading && (
            <>
              <Row align={"middle"} className="pt-2 pb-2">
                <Col span={12}>
                  <Link
                    href={`https://www.google.com/calendar/event?action=TEMPLATE&text=${
                      showData?.name
                    }&dates=${DateTime.fromISO(showData?.startDate?.iso)
                      .setZone("utc")
                      .toFormat("yyyyMMdd'T'HHmmss'Z'")}/${DateTime.fromISO(
                      showData?.startDate?.iso
                    )
                      .plus({ hours: 2 })
                      .setZone("utc")
                      .toFormat("yyyyMMdd'T'HHmmss'Z'")}&details=${
                      showData?.note || ""
                    }&location=${
                      showData?.venue?.address || ""
                    }&trp=false&sprop=`}
                    target="_blank"
                    className="text-primary"
                  >
                    <GoogleCalendarIcon /> Sync with Google Calendar
                  </Link>
                </Col>
                <Col span={12}>
                  {icsFileContent && (
                    <Link
                      href={`data:text/calendar;base64,${btoa(
                        unescape(encodeURIComponent(icsFileContent))
                      )}`}
                      target="_blank"
                      className="text-primary"
                    >
                      <IOSCalendarIcon /> Download iCal File
                    </Link>
                  )}
                </Col>
              </Row>
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="show_details">
                  <div className="image_section">
                    <Image
                      src={
                        showData?.iconImage?.url ||
                        "/images/venue-placeholder.png"
                      }
                      alt="Venue Image"
                      width={125}
                      height={125}
                    />
                  </div>
                  <div className="description">
                    <div className="links">
                      <div>
                        <Link
                          target={"_blank"}
                          href={`/venue/${showData?.venue?.vanity}`}
                        >
                          {showData?.venue?.name}
                        </Link>
                      </div>
                      <div>
                        <Link
                          target={"_blank"}
                          href={`/artist/${showData?.artist?.vanity}`}
                        >
                          {capitalizeString(showData?.artistName)}
                        </Link>
                      </div>
                    </div>
                    <Divider className="custom_divider" />
                    {/* <Row className="pt-2">
                      <Col
                        md={{ span: 6 }}
                        lg={{ span: 6 }}
                        sm={{ span: 6 }}
                        xs={{ span: 12 }}
                        className="earnings"
                      >
                        {" "}
                        <label>Log Tips: $</label>
                      </Col>
                      <Col
                        md={{ span: 18 }}
                        lg={{ span: 18 }}
                        sm={{ span: 18 }}
                        xs={{ span: 24 }}
                        className="promoteOnJCasp"
                      >
                        <InputField
                          {...{
                            register,
                            formState,
                            className: "inputField",
                            id: "earnings",
                            placeholder: "Earnings",
                            type: "number",
                            defaultValue: showData?.earnings,
                            disabled:
                              loginUserState.data?.userType ===
                              LOGIN_USER_TYPE.ADMIN,
                          }}
                        />
                      </Col>
                    </Row> */}
                    <Row className="pt-2">
                      <Col className="earnings cursor-pointer">
                        {promoteOnJCasp ? (
                          <Image
                            src={"/images/general/checked-checkbox.png"}
                            onClick={handlepromoteOnJCasp}
                            alt="Checked"
                            width={24}
                            height={24}
                          />
                        ) : (
                          <Image
                            src={"/images/general/un-checked-checkbox.png"}
                            onClick={handlepromoteOnJCasp}
                            alt="Un Checked"
                            width={24}
                            height={24}
                          />
                        )}

                        <span
                          className={`${
                            loginUserState.data?.userType !==
                            LOGIN_USER_TYPE.ADMIN
                              ? "cursor-pointer"
                              : ""
                          } ms-2`}
                          onClick={handlepromoteOnJCasp}
                        >
                          This Event is Private do not promote on JCasp.
                        </span>
                      </Col>
                    </Row>
                  </div>
                </div>

                <Divider className="custom_divider" />
                <Row className="mt-4">
                  <Col>
                    <h3>Note</h3>
                  </Col>
                </Row>
                <Row className="">
                  <Col span={24}>
                    <TextAreaField
                      {...{
                        register,
                        formState,
                        className: "notes",
                        id: "note",
                        defaultValue: showData?.note,
                        disabled:
                          loginUserState.data?.userType ===
                          LOGIN_USER_TYPE.ADMIN,
                        placeholder:
                          "Enter some additional information about the show...",
                      }}
                    />
                  </Col>
                </Row>
                {loginUserState.data?.userType !== LOGIN_USER_TYPE.ADMIN && (
                  <div className="action_earning">
                    <Divider />
                    <div className="actions">
                      <div>
                        <Button
                          htmlType="submit"
                          type="primary"
                          disabled={isFormLoading || formState?.isSubmitting}
                          loading={isFormLoading || formState?.isSubmitting}
                        >
                          Save
                        </Button>
                      </div>
                      <div>
                        <Button
                          htmlType="button"
                          type="primary"
                          disabled={isFormLoading || formState?.isSubmitting}
                          loading={isFormLoading || formState?.isSubmitting}
                          onClick={() =>
                            handleShareModalOpen(
                              `${SITE_URL}/?showId=${showData?.objectId}`
                            )
                          }
                        >
                          Promote
                        </Button>
                      </div>
                      <div className="deleteImage">
                        <Popconfirm
                          title="Are you sure you want to delete?"
                          okText="Yes, Delete"
                          cancelText="No"
                          onConfirm={() => handleDeleteShow(showData?.objectId)}
                        >
                          <Button htmlType="button" type="default" className="">
                            Delete
                          </Button>
                        </Popconfirm>
                      </div>
                    </div>
                  </div>
                )}
              </form>
            </>
          )}

          <Divider />
          {isFormLoading && <DefaultLoader />}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default CalendarShowsModal;
