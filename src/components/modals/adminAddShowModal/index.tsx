import { yupResolver } from "@hookform/resolvers/yup";
import { getVenuesSearchAPI } from "@redux/services/general.api";
import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import Select from "react-select";
import { deepClone } from "src/libs/helpers";
import useList from "src/libs/useList";
import {
  AdminAddShowsFormValidationProps,
  AdminAddShowsFormValidationSchema,
} from "src/schemas/adminAddShowFormSchema";
import modalStyles from "./artistAddShow.module.scss";
import { Button } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { generalSearchArtistsAPI } from "@redux/services/general.api";
import { createAdminShowsAPI } from "@redux/services/shows.api";
import { DateTime } from "luxon";
import moment from "moment";
import DatePicker, { DateObject } from "react-multi-date-picker";
import DatePanel from "react-multi-date-picker/plugins/date_panel";
import TimePicker from "react-multi-date-picker/plugins/time_picker";
import {
  DATE_FORMAT,
  LOGIN_USER_TYPE,
  SUCCESS_MESSAGES,
} from "src/libs/constants";
import VenueSearchModal from "../venueSearchModal";
import { LoginUserState } from "@redux/slices/auth";
import { RootState } from "@redux/reducers";
import { useSelector } from "react-redux";
import Image from "next/image";

export interface AdminAddShowModalProps {
  isOpen: boolean;
  handleClose: (data: any) => void;
  setIsNewShowModalOpen: (data: any) => void;
}

const AdminAddShowModal = (props: AdminAddShowModalProps) => {
  const { isOpen, setIsNewShowModalOpen, handleClose } = props;

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [venueOptions, setVenueOptions] = useState<any>([]);
  const [venueId, setVenueId] = useState("");
  const [isShowLoading, setIsShowModalOpen] = useState(false);
  const [googlePlaceId, setGooglePlaceId] = useState("");
  const [isVenueModalOpen, setIsVenueModalOpen] = useState(false);

  const today = new Date();

  let dates = moment(today);
  const [dateValue, setdateValue] = useState([dates.format(DATE_FORMAT)]);

  let hn = 18;
  let mn = 0o0;

  const [values, setValues] = useState(
    new DateObject().set({
      hour: hn,
      minute: mn,
      format: "PM",
    })
  );
  const timess = values.format("hh:mm A");

  const [venueData, setVenueData] = useState<any>({
    place: "",
    place_id: "",
  });
  const [venueAddress, setVenueAddress] = useState({
    value: "",
    label: "",
  });

  const [showDetails, setShowDetails] = useState<any>([
    {
      date: dateValue || [],
      startTime: timess || "",
      artistId: "",
      artistName: "",
    },
  ]);

  const [artistData, setArtistData] = useState<any>([]);

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<AdminAddShowsFormValidationProps>({
      resolver: yupResolver(AdminAddShowsFormValidationSchema),
    });

  useEffect(() => {
    if (isOpen) {
      setIsModalOpen(isOpen);
    }
  }, [isOpen]);

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsNewShowModalOpen(false);
  };

  const handleChangeDate = (e: any, index: number) => {
    const formatDate = moment(e);
    const date = formatDate.format("YYYY-MM-DD");
    const updatedItems = [...showDetails];
    updatedItems[index] = { ...updatedItems[index], date: date };
    setShowDetails(updatedItems);
  };

  const handleChangeTime = (e: any, index: any) => {
    const formattedTime = moment(e).format("hh:mm A").toString();
    const updatedItems = [...showDetails];
    updatedItems[index] = { ...updatedItems[index], startTime: formattedTime };
    setShowDetails(updatedItems);
  };

  useEffect(() => {
    if (showDetails) {
      setValue(`showDetails` as any, undefined);
      showDetails.map((item: any, index: any) => {
        setValue(`showDetails[${index}].startTime` as any, item.startTime);
        setValue(`showDetails[${index}].date` as any, item.date);
        setValue(`showDetails[${index}].artistId` as any, item.artistId);
      });
    }
  }, [showDetails]);

  const { apiParam } = useList({
    queryParams: {
      search: "",
    },
  });

  const getVenue = async () => {
    try {
      const response = await getVenuesSearchAPI(apiParam);
      setVenueOptions(response);
    } catch (error) {
      console.log(error);
    }
  };

  let getAllArtistList = async () => {
    if (apiParam) {
      delete apiParam.take;
      delete apiParam.skip;
      try {
        let res = await generalSearchArtistsAPI(apiParam);
        setArtistData(res);
      } catch (error) {
        console.log(error, "error");
      }
    }
  };

  useEffect(() => {
    getVenue();
    getAllArtistList();
  }, [apiParam]);

  const handleVenueChange = (data: any) => {
    if (data) {
      apiParam.search = data;
      getVenue();
    }
  };

  const handleChange = (data: any) => {
    setVenueId(data.value);
    setValue("venueSelect", data.value);
  };

  const handleChangeArtist = (d: any, index: any) => {
    if (d) {
      apiParam.search = d;
      getAllArtistList();
    }
  };

  const addNewShow = () => {
    const tmpData = deepClone(showDetails);
    tmpData.push({
      date: dateValue || [],
      startTime: timess || "",
      artistId: "",
      artistName: "",
    });
    setShowDetails(tmpData);
  };

  const removeShow = (key: any) => {
    const tmpData = deepClone(showDetails);
    const tempData = tmpData.filter((item: any, index: any) => index != key);
    setShowDetails(tempData);
  };

  const handleArtistChange = (data: any, index: number) => {
    const updatedItems = [...showDetails];
    updatedItems[index] = {
      ...updatedItems[index],
      artistId: data.value,
      artistName: data.label,
    };
    setShowDetails(updatedItems);
  };

  const onSubmit = async (data: any) => {
    if (venueId) {
      data["venueId"] = venueId;
    }
    if (googlePlaceId) {
      data["googlePlaceId"] = googlePlaceId;
    }
    delete data.venueSelect;
    if (data) {
      setIsShowModalOpen(true);
      try {
        await createAdminShowsAPI(data);
        setIsShowModalOpen(false);
        setIsNewShowModalOpen(false);
        setIsShowModalOpen(false);
        handleClose(true);
        showToast(SUCCESS_MESSAGES.showAddedSuccess, "success");
      } catch (error) {
        setIsModalOpen(false);
        setIsShowModalOpen(false);
        setIsNewShowModalOpen(false);
      }
    }
  };

  useEffect(() => {
    if (venueData.place_id !== "") {
      setVenueAddress({
        value: venueData.place,
        label: venueData.place,
      });
      setValue("venueSelect", venueData.place_id);
      setVenueId("");
      setGooglePlaceId(venueData.place_id);
    }
  }, [venueData]);

  const disabledDatePast = (current: moment.Moment) => {
    const currentDate = DateTime.fromISO(current.toISOString())
      .set({
        hour: 0,
        minute: 0,
        second: 0,
      })
      .toUnixInteger();

    const currentWithTimezone = DateTime.fromISO(
      DateTime.utc()
        .set({
          hour: 0,
          minute: 0,
          second: 0,
        })
        .toISO({ includeOffset: false })
    ).toUnixInteger();

    return currentDate.valueOf() < currentWithTimezone.valueOf();
  };

  const handledateValues = (value: any, index: number) => {
    const datesArr: any = [];
    value.map((date: any, i: number) => {
      datesArr.push(date.format());
    });
    const updatedItems = [...showDetails];
    updatedItems[index] = {
      ...updatedItems[index],
      date: datesArr,
    };
    setdateValue(datesArr);
    setShowDetails(updatedItems);
  };

  const handleTimeValue = (value: any, index: number) => {
    const updatedItems = [...showDetails];
    updatedItems[index] = {
      ...updatedItems[index],
      startTime: value.format(),
    };
    setShowDetails(updatedItems);
  };

  return (
    <>
      {isVenueModalOpen && (
        <VenueSearchModal
          {...{
            isOpen: isVenueModalOpen,
            setIsVenueModalOpen,
            setVenueData,
          }}
        />
      )}

      <Modal
        show={isModalOpen}
        className={modalStyles.modal}
        onHide={handleCancel}
      >
        <Modal.Header closeButton className={modalStyles.header}>
          <h2 className="modal-title modal-e text-center" id="inputModalLabel">
            Add New Show
          </h2>
        </Modal.Header>
        <Modal.Body className={modalStyles.modal_body}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Row>
              <Col span={10}>
                <div className={`${modalStyles.venueSelectlabel} `}>
                  <label>JCasp Venue</label>
                </div>
              </Col>
              <Col span={14}>
                {loginUserState?.data.userType !==
                  LOGIN_USER_TYPE.VENUE_ADMIN && (
                  <div className={`venueSearch`}>
                    <a onClick={() => setIsVenueModalOpen(true)}>
                      Can&apos;t find? Create a Venue
                    </a>
                  </div>
                )}
              </Col>
            </Row>

            <div className={modalStyles.venueSelect}>
              {venueAddress.value !== "" && (
                <Select
                  className={`basic_multi_select selectField`}
                  classNamePrefix="Enter venue name to search"
                  placeholder="Enter venue name to search"
                  defaultValue={venueAddress}
                  value={venueAddress || ""}
                  options={venueOptions}
                  onInputChange={handleVenueChange}
                  onChange={handleChange}
                  isDisabled={venueAddress.value !== ""}
                  instanceId="venues"
                />
              )}

              {venueAddress.value === "" && (
                <>
                  <Select
                    className={`basic_multi_select selectField`}
                    classNamePrefix="Enter venue name to search"
                    placeholder="Enter venue name to search"
                    options={venueOptions}
                    onInputChange={handleVenueChange}
                    onChange={handleChange}
                    instanceId="venuename"
                  />

                  {formState &&
                    formState?.errors &&
                    formState?.errors["venueSelect"] &&
                    formState?.errors["venueSelect"].message && (
                      <span className="ant-typography ant-typography-danger text-danger block mt-1">
                        {formState.errors["venueSelect"].message}
                      </span>
                    )}
                </>
              )}
            </div>
            {showDetails?.map((form: any, index: any) => (
              <div key={index} className={index !== 0 ? "dividar mt-3" : ""}>
                <div id="dynamicRows" className="mt-3">
                  <div
                    className="row shows-m-bottom custom-rows  custom-rows"
                    id="row_1"
                  >
                    <div className="form-group col-md-4 col-sm-12">
                      <div className="row shows-venue-pad">
                        <div className="formB col-md-12 col-12">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label>Artist</label>
                            <div className="">
                              <Select
                                className={`basic_multi_select selectField`}
                                classNamePrefix="Select a Artist"
                                placeholder="Select a Artist"
                                onInputChange={(e) =>
                                  handleChangeArtist(e, index)
                                }
                                onChange={(d) => handleArtistChange(d, index)}
                                options={artistData}
                                instanceId="artist"
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]
                                  ?.artistId?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.artistId?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group col-md-4 col-sm-12">
                      <div className="row shows-venue-pad">
                        <div className="formB col-md-12 col-12">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label>Dates</label>
                            <div className="">
                              <DatePicker
                                multiple
                                placeholder="Select Date"
                                format={DATE_FORMAT}
                                value={dateValue}
                                minDate={new Date()}
                                onChange={(value) =>
                                  handledateValues(value, index)
                                }
                                plugins={[<DatePanel key={index} />]}
                              />

                              {/* <DatePicker
                                id={`showDetails[${index}].date`}
                                className={modalStyles.datePicker}
                                placeholder="Select Date"
                                onChange={(e) => handleChangeDate(e, index)}
                                value={form.date && moment(form.date)}
                                disabledDate={(currentDate) => {
                                  return disabledDatePast(currentDate);
                                }}
                              /> */}
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]?.date
                                  ?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.date?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group col-md-4 col-sm-12">
                      <div className="row shows-venue-pad align-center">
                        <div className="shows-venue formB col-md-8 col-8">
                          <div className={`${modalStyles.venueSelectlabel}`}>
                            <label htmlFor="time">Start Time</label>
                            <div className="timePiker">
                              <DatePicker
                                disableDayPicker
                                id={`showDetails[${index}].date`}
                                format="hh:mm A"
                                value={values}
                                placeholder="Select time"
                                onChange={(value) =>
                                  handleTimeValue(value, index)
                                }
                                plugins={[
                                  <TimePicker
                                    hideSeconds
                                    key={index}
                                    mStep={15}
                                    defaultValue={"06:00 AM"}
                                  />,
                                ]}
                              />
                              {formState &&
                                formState?.errors &&
                                formState?.errors["showDetails"] &&
                                formState?.errors["showDetails"][index]
                                  ?.startTime?.message && (
                                  <span className="text-danger">
                                    {
                                      formState?.errors["showDetails"][index]
                                        ?.startTime?.message
                                    }
                                  </span>
                                )}
                            </div>
                          </div>
                        </div>
                        <div className="icons col-4 col-md-4 mt-4">
                          {index === 0 && (
                            <div className="addIcon" onClick={addNewShow}>
                              <Image
                                src="images/general/addIcon.svg"
                                alt="Add new show"
                                width={40}
                                height={40}
                              />
                              {/* <img src="images/general/addIcon.svg" /> */}
                            </div>
                          )}

                          {index !== 0 && (
                            <div
                              className="addIcon"
                              onClick={() => removeShow(index)}
                            >
                              <Image
                                src="images/general/removeIcon.svg"
                                alt="Remove show"
                                width={40}
                                height={40}
                              />
                              {/* <img src="images/general/removeIcon.svg" /> */}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="DatesContainer">
                  {showDetails[index]?.date &&
                    showDetails[index]?.date?.length > 0 &&
                    showDetails[index]?.date?.map(
                      (item: any, index: number) => (
                        <span key={index} className="Multidates">
                          {item}{" "}
                        </span>
                      )
                    )}
                </div>
              </div>
            ))}

            <div className="modal-footer shows-btn mt-5">
              <Button
                htmlType="button"
                type="ghost"
                disabled={isShowLoading}
                onClick={handleCancel}
              >
                Close
              </Button>
              <div className={modalStyles.request_btn}>
                <Button
                  htmlType="submit"
                  type="primary"
                  loading={isShowLoading}
                  disabled={isShowLoading}
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default AdminAddShowModal;
