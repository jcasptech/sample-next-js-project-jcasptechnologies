import { RootState } from "@redux/reducers";
import { LoginUserState } from "@redux/slices/auth";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Modal from "react-bootstrap/Modal";
import modalstyle from "./artistContactModal.module.scss";
import Link from "next/link";
import { PhoneIcon } from "@components/theme/icons/phoneIcon";
import { EmailIcon } from "@components/theme/icons/emailIcon";

export interface ArtistContactModalProps {
  artistData: any;
  isOpen: boolean;
  setIsContactModalOpen: (data: any) => void;
}

const ArtistContactModal = (props: ArtistContactModalProps) => {
  const { isOpen, setIsContactModalOpen, artistData } = props;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useDispatch();
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsContactModalOpen(false);
  };

  return (
    <>
      <Modal className={modalstyle.modal} show={isOpen} onHide={handleCancel}>
        <Modal.Body>
          <div className="contact_details">
            <div>
              <p>Contact the artist directly using the information below </p>
            </div>
            <div className="borberBottom"></div>
            <div>
              <h3>Artist One</h3>
            </div>
            <div className="emailPhone">
              <div>
                <Link href="mailto:contact@jcasptechnologies.com">
                  <span>
                    <EmailIcon />
                    {"  "}
                  </span>{" "}
                  {artistData?.email}
                </Link>
              </div>
              <div>
                <Link href={`tel:+${artistData?.phone}`}>
                  <span>
                    {" "}
                    <PhoneIcon />
                    {"  "}:
                  </span>{" "}
                  {artistData?.phone}
                </Link>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default ArtistContactModal;
