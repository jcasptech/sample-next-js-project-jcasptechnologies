import CopyLinkIcon from "@components/theme/icons/copyLinkIcon";
import { showToast } from "@components/ToastContainer";
import Clipboard from "clipboard";
import Modal from "react-bootstrap/Modal";
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookMessengerIcon,
  FacebookMessengerShareButton,
  FacebookShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "react-share";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import shareModalstyle from "./shareThisModalstyle.module.scss";

export interface ShareThisModalProps {
  isOpen: boolean;
  setIsShareModalOpen: (data: any) => void;
  url?: string | undefined;
}
const ShareThisModal = (props: ShareThisModalProps) => {
  const { isOpen, setIsShareModalOpen, url } = props;

  const handleCancel = () => {
    setIsShareModalOpen(false);
  };

  return (
    <>
      <Modal
        show={isOpen}
        className={shareModalstyle.modal}
        onHide={handleCancel}
      >
        <Modal.Header closeButton className={shareModalstyle.header}>
          <h2> Share </h2>
        </Modal.Header>
        <Modal.Body className={shareModalstyle.modal_body}>
          <FacebookShareButton url={`${url}`}>
            <FacebookIcon />
          </FacebookShareButton>

          <EmailShareButton url={`${url}`}>
            <EmailIcon />
          </EmailShareButton>

          <TwitterShareButton url={`${url}`}>
            <TwitterIcon />
          </TwitterShareButton>

          <WhatsappShareButton url={`${url}`}>
            <WhatsappIcon />
          </WhatsappShareButton>

          <TelegramShareButton url={`${url}`}>
            <TelegramIcon />
          </TelegramShareButton>

          <FacebookMessengerShareButton appId="ravisen18" url={`${url}`}>
            <FacebookMessengerIcon />
          </FacebookMessengerShareButton>

          <LinkedinShareButton url={`${url}`}>
            <LinkedinIcon />
          </LinkedinShareButton>

          <div
            className="copyIcon"
            onClick={(e) => {
              e.preventDefault();
              Clipboard.copy(`${url}`);
              showToast(
                `${SUCCESS_MESSAGES.copyToClipboard} ${url}`,
                "success"
              );
            }}
          >
            <CopyLinkIcon />
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default ShareThisModal;
