import { Button, DefaultSkeleton } from "@components/theme";
import {
  InputField,
  InputGoogleAutoCompleteField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { postFileAPI } from "@redux/services/artist.api";
import { createClaimedArtistAPI } from "@redux/services/claim-unClaimed.api";
import { default as NextImage } from "next/image";
import { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ClaimArtistFormValidateSchema,
  ClaimArtistInputs,
} from "src/schemas/claimArtistFormValidationSchema";
import claimArtistStyle from "./claimArtist.module.scss";

export interface CreateClaimArtistModalProps {
  isOpen: boolean;
  setIsclaimArtistModalOpen: (data: any) => void;
  handleClose: (data: any) => void;
}

const CreateClaimArtistModal = (props: CreateClaimArtistModalProps) => {
  const { isOpen, setIsclaimArtistModalOpen, handleClose } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [imageIsLoading, setImageIsLoading] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const [isProfileImgUpdated, setIsProfileImgUpdated] = useState("false");
  const {
    register,
    handleSubmit,
    formState,
    control,
    setValue,
    setFocus,
    reset,
  } = useForm<ClaimArtistInputs>({
    resolver: yupResolver(ClaimArtistFormValidateSchema),
  });

  const handleCancel = () => {
    setIsclaimArtistModalOpen(false);
  };

  const onSubmit = async (data: any) => {
    data["isProfileImgUpdated"] = isProfileImgUpdated;
    if (data) {
      setIsLoading(true);
      try {
        const res = await createClaimedArtistAPI(data);
        showToast(SUCCESS_MESSAGES.claimArtistCreatedSuccess, "success");
        reset();
        handleClose(res);
        setIsclaimArtistModalOpen(false);
        setIsLoading(false);
      } catch (error: any) {
        setIsLoading(false);
      }
    }
  };

  const onFileSelected = (e: any) => {
    let image = e.target.files[0];
    const allowedTypes = ["image/jpeg", "image/png", "image/jpg"];
    let isValidDimensions = false;
    const reader = new FileReader();
    reader.onload = function (readerEvent: any) {
      const img: any = new Image();
      img.onload = async function () {
        if (this.width >= 750 && this.height >= 586) {
          setImageIsLoading(true);
          const formData = new FormData();
          formData.append("file", image);
          try {
            let response = await postFileAPI(formData);
            setSelectedImage(response?.url);
            setIsProfileImgUpdated("true");
            setImageIsLoading(false);
          } catch (error: any) {
            setImageIsLoading(false);
            showToast(error.message, "warning");
          }
        } else {
          setImageIsLoading(false);
          showToast(SUCCESS_MESSAGES.profileImage, "warning");
        }
      };
      img.src = readerEvent.target.result;
    };

    if (image && allowedTypes.includes(image.type)) {
      reader.readAsDataURL(image);
      setImageIsLoading(true);
    } else {
      showToast(SUCCESS_MESSAGES.inputFileType, "warning");
    }
  };

  const handleSelect = (e: any) => {
    e.preventDefault();
    let fileDialog = $('<input type="file">');
    fileDialog.click();
    fileDialog.on("change", onFileSelected);
  };

  return (
    <>
      <Modal
        show={isOpen}
        className={claimArtistStyle.modalStyle}
        onHide={handleCancel}
      >
        <Modal.Header closeButton className={claimArtistStyle.header}>
          <h1>Claim Artists</h1>
        </Modal.Header>
        <Modal.Body>
          <form
            className={`panel-form ${claimArtistStyle.main_form}`}
            onSubmit={handleSubmit(onSubmit)}
            method="POST"
          >
            {/* <fieldset> */}
            <div className="">
              <div className="col align-center field-row">
                <div className="col-12">
                  <label className={claimArtistStyle.label}>
                    Profile Image <small>(MINIMUM DIMENSIONS: 750 X 586)</small>
                  </label>
                </div>
                <div className="col-12 profile_pic mt-2">
                  {imageIsLoading && <DefaultSkeleton />}
                  <div className="profile-pic-edit">
                    <NextImage
                      src={selectedImage || "/images/artist-placeholder.png"}
                      alt="profile image"
                      height={128}
                      width={128}
                    />
                    <span className="edit-pp">
                      <a
                        href="#"
                        onClick={handleSelect}
                        title="Upload profile photo"
                        id="btnImportData"
                      >
                        <NextImage
                          src="/images/edit-ic.svg"
                          alt="Edit"
                          height={128}
                          width={38}
                        />
                      </a>
                    </span>
                  </div>
                </div>
              </div>

              <div className="col align-center field-row">
                <div className="col-12 col-sm-4">
                  <label className={claimArtistStyle.label}>Artist Name</label>
                </div>
                <div className="col-12 ">
                  <div className="">
                    <InputField
                      {...{
                        register,
                        formState,
                        id: "artistName",
                        className: `inputField`,
                        placeholder: "Enter full Artist Name",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="col align-center field-row">
                <div className="col-12 col-sm-4">
                  <label className={claimArtistStyle.label}>Youtube URL</label>
                </div>
                <div className="col-12">
                  <div className="">
                    <InputField
                      {...{
                        register,
                        formState,
                        id: "youtubeUrl",
                        className: `inputField`,
                        placeholder: "Enter Youtube URL",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="row align-center field-row">
                <div className="col-12 col-md-4 col-lg-4">
                  <div className="col-12">
                    <label className={claimArtistStyle.label}>City</label>
                  </div>
                  <div className="col-12">
                    <div className="">
                      <InputGoogleAutoCompleteField
                        {...{
                          register,
                          formState,
                          id: "city",
                          className: "inputField",
                          placeholder: "City",
                          searchType: "(cities)",
                          googleAutoCompleteConfig: {
                            setValue,
                            autoCompleteId: "city",
                            autoPopulateFields: {
                              province: "state",
                              country: "country",
                              city: "city",
                            },
                          },
                        }}
                      />
                      <div id="mapMoveHere"></div>
                      {/* <SelectField
                        {...{
                          register,
                          formState,
                          name: "country",
                          className: "inputField",
                          id: "country",
                          placeholder: "Enter Country Name",
                          onSelectChange: handleCountrySelect,
                          control,
                        }}
                        options={countries}
                      /> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 col-lg-4">
                  <div className="col-12">
                    <label className={claimArtistStyle.label}>State</label>
                  </div>
                  <div className="col-12">
                    <div className="">
                      <InputField
                        {...{
                          register,
                          formState,
                          className: "inputField",
                          id: "state",
                          placeholder: " State",
                        }}
                      />
                      {/* <SelectField
                        {...{
                          register,
                          formState,
                          name: "state",
                          className: "inputField",
                          id: "state",
                          placeholder: "Enter State Name",
                          control,
                        }}
                        options={states}
                      /> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 col-lg-4">
                  <div className="col-12">
                    <label className={claimArtistStyle.label}>Country</label>
                  </div>
                  <div className="col-12">
                    <div className="">
                      <InputField
                        {...{
                          register,
                          formState,
                          name: "country",
                          className: "inputField",
                          id: "country",
                          placeholder: "Country",
                        }}
                      />
                      {/* <InputField
                        {...{
                          register,
                          formState,
                          id: "city",
                          className: `inputField`,
                          placeholder: "Enter City Name",
                        }}
                      /> */}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col align-center field-row">
                <div className="col-12 col-sm-4">
                  <label className={claimArtistStyle.label}>Artist Bio</label>
                </div>
                <div className="col-12 ">
                  <TextAreaField
                    {...{
                      register,
                      formState,
                      id: "artistBio",
                      className: `${claimArtistStyle.textAreaField}`,
                      placeholder:
                        "Let fans, venues and fellow musicians know everything they should know!",
                    }}
                  />
                </div>
              </div>

              <div className="row align-center field-row">
                <div className="button">
                  <Button
                    htmlType="submit"
                    type="primary"
                    className="float-right"
                    loading={isLoading}
                    disabled={isLoading}
                  >
                    Create
                  </Button>

                  <Button
                    htmlType="button"
                    type="ghost"
                    className="float-right me-2"
                    onClick={handleCancel}
                  >
                    Cancel
                  </Button>
                </div>
              </div>
            </div>
            {/* </fieldset> */}
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default CreateClaimArtistModal;
