import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect, useRef, useState } from "react";
import { SongObject } from "@redux/slices/artists";
import { PauseIcon } from "@components/theme/icons/pause";
import { PlayIcon } from "@components/theme/icons/playIcon";
import { AudioTrackIcon } from "@components/theme/icons/audioTrackIcon";

export interface SongProps {
  song: SongObject;
  theme: "dark" | "light";
  songPlayed: string;
  setSongPlayed: (d: any) => void;
}

const Song = (props: SongProps) => {
  const { song, theme, songPlayed, setSongPlayed } = props;

  const audioRef = useRef<any>(null);
  const [isPlay, setIsPlay] = useState(false);
  const [currentTime, setCurrentTime] = useState(0);
  const [audioProgress, setAudioProgress] = useState({
    left: 100,
    current: 0,
  });
  const [duration, setDuration] = useState(0);

  const formatTime = (time: number) => {
    const minutes = Math.floor(time / 60);
    const seconds = Math.floor(time % 60);
    return `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  };

  useEffect(() => {
    if (song.objectId === songPlayed) {
      if (isPlay) {
        audioRef?.current?.pause();
        setIsPlay(false);
      } else {
        audioRef.current.play();
        setIsPlay(true);
      }
    } else {
      audioRef?.current?.pause();
      setIsPlay(false);
    }
  }, [songPlayed]);

  const handleTimeUpdate = () => {
    setCurrentTime(parseInt(audioRef.current.currentTime));
  };

  const handleLoadedMetadata = (data: any) => {
    if (data?.target?.duration) {
      setDuration(data?.target?.duration);
    }
  };

  const handleEnd = () => {
    setIsPlay(false);
    setAudioProgress({
      current: 0,
      left: 100,
    });
  };

  useEffect(() => {
    if (currentTime) {
      const current = (100 * currentTime) / duration;
      setAudioProgress({
        current: current,
        left: 100 - current,
      });
    }
  }, [currentTime]);

  useEffect(() => {
    if (audioRef?.current?.duration) {
      setDuration(audioRef?.current?.duration);
    }
  }, [audioRef.current]);

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <div className="col-12 col-sm-12 col-lg-6">
      <div className="audioFile">
        <span className="play float-left">
          <a
            onClick={(e) => {
              e.preventDefault();
              isPlay ? setSongPlayed(null) : setSongPlayed(song.objectId);
            }}
          >
            {isPlay ? <PauseIcon /> : <PlayIcon />}
          </a>
        </span>
        <div className="name float-left">
          {song?.name}
          <div className="progress-bar"></div>
          <p className="time float-left">{formatTime(duration)} sec</p>

          {isPlay && (
            <div className="d-flex w-100">
              <div
                className={`mt-2 ${
                  theme === "light" ? "audio-track" : "audio-track_dark"
                }`}
                style={
                  theme === "light"
                    ? {
                        backgroundImage: `linear-gradient(to left, transparent ${audioProgress.left}%, #FFD3C0 ${audioProgress.left}%)`,
                      }
                    : {
                        backgroundImage: `linear-gradient(to left, transparent ${audioProgress.left}%,  #FF6C2C ${audioProgress.left}%)`,
                      }
                }
              >
                <AudioTrackIcon
                  color={theme}
                  progress={audioProgress.current}
                />
                <span className="ms-3">
                  {formatTime(duration - currentTime)}
                </span>
              </div>
            </div>
          )}
          <audio
            ref={audioRef}
            src={song?.audioFile?.url}
            onTimeUpdate={handleTimeUpdate}
            onLoadedMetadata={handleLoadedMetadata}
            onEnded={handleEnd}
          />
        </div>
      </div>
    </div>
  );
};
export default Song;
