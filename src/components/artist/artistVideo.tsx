import { ArtistObject } from "@redux/slices/artists";
import { getVideoUrl } from "src/libs/helpers";

export interface ArtistVideoProp {
  artistDetail?: ArtistObject;
  videoUrl: string;
  youtubeId?: string;
  className?: string;
}

const ArtistVideo = (props: ArtistVideoProp) => {
  const { className, videoUrl } = props;

  return (
    <>
      <div className="col-12 col-sm-6">
        <iframe
          width="465px"
          height="300px"
          className={className}
          src={getVideoUrl(videoUrl)}
          title="YouTube video player"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowFullScreen
        ></iframe>
      </div>
    </>
  );
};

export default ArtistVideo;
