import { DefaultLoader } from "@components/theme";
import { RootState } from "@redux/reducers";
import { fetchTags, TagsState } from "@redux/slices/tags";
import { useReCaptcha } from "next-recaptcha-v3";
import Image from "next/image";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Slider from "react-slick";
import useList from "src/libs/useList";
import artistStyles from "./artist.module.scss";

export interface TagsProps {
  selectedTags: string[];
  setSelectedTags: (d: any) => void;
}

const Tags = (props: TagsProps) => {
  const { selectedTags, setSelectedTags } = props;
  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();

  const settings = {
    slidesToShow: 14,
    slidesToScroll: 10,
    autoplay: false,
    autoplaySpeed: 2500,
    infinite: false,
    mobileFirst: true,
    arrows: true,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 12,
          slidesToScroll: 8,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 10,
          slidesToScroll: 8,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 8,
          slidesToScroll: 6,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 300,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 280,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
    ],
  };

  const {
    tags: { isLoading, data: tagsData },
  }: {
    tags: TagsState;
  } = useSelector((state: RootState) => ({
    tags: state.tags,
  }));

  const { apiParam } = useList({
    queryParams: {},
  });

  const getTags = async () => {
    const recaptchaResponse = await executeRecaptcha("tag_list");
    dispatch(fetchTags(apiParam, recaptchaResponse));
  };

  useEffect(() => {
    if (loaded) {
      getTags();
    }
  }, [loaded]);

  return (
    <>
      <div className="col-12 ac_slideHold">
        <div className={`artist-cats slider ${artistStyles.tags_slider_arrow}`}>
          {isLoading && <DefaultLoader />}
          {!isLoading && (
            <Slider {...settings}>
              {tagsData &&
                tagsData?.length > 0 &&
                tagsData.map((tag, index) => (
                  <div
                    key={index}
                    className={`slide col-sm-1 col-2 ${
                      selectedTags.includes(tag.tag) ? "active" : ""
                    }`}
                    onClick={() => {
                      setSelectedTags((d: string[]) => {
                        if (d.includes(tag.tag)) {
                          delete d[d.indexOf(tag.tag)];
                        } else {
                          d.push(tag.tag);
                        }

                        return d.filter(function (el) {
                          return el != null;
                        });
                      });
                    }}
                  >
                    <div className="icon">
                      <Image
                        className="m-auto"
                        src={tag.sourceImage?.url || "/images/aall.png"}
                        alt={tag.label}
                        width={40}
                        height={40}
                      />
                    </div>
                    <div className="title">{tag.label}</div>
                  </div>
                ))}
            </Slider>
          )}
        </div>
      </div>
    </>
  );
};

export default Tags;
