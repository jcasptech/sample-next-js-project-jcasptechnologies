import ShareThisModal from "@components/modals/ShareThisModal";
import { showsObject } from "@redux/slices/artistShow";
import AOS from "aos";
import "aos/dist/aos.css";
import moment from "moment";
import Image from "next/image";
import { useEffect, useState } from "react";
import { SITE_URL } from "src/libs/constants";

export interface ArtistShowsProp {
  show: showsObject;
  theme: "dark" | "light";
}

const ArtistShows = (props: ArtistShowsProp) => {
  const { show, theme } = props;

  const [url, setUrl] = useState("");
  const [isShareModalOpen, setIsShareModalOpen] = useState(false);

  useEffect(() => {
    AOS.init();
  }, []);

  const handleShareModalOpen = (data: any) => {
    if (data) {
      setIsShareModalOpen(true);
      setUrl(data);
    }
  };

  return (
    <>
      {isShareModalOpen && (
        <ShareThisModal
          {...{
            isOpen: isShareModalOpen,
            url: url || "",
            setIsShareModalOpen,
          }}
        />
      )}
      <div
        className={`show_indi_card col-12  ${
          theme == "dark" ? "dark" : "artist-white"
        }`}
      >
        <div className="row vcenter m0 justify-content-start">
          <div className="col-sm-3 col-4 show_thumb p-0">
            <Image
              src={show?.iconImage?.url || "/images/ethumb.png"}
              alt="venue title"
              width={100}
              height={100}
              quality={100}
            />
          </div>
          <div className="col-sm-9 col-8 show_meta">
            <div className="title text-ellipsis">{show?.name}</div>
            <div className="calc mt sfonts">
              <Image
                src="/images/about-us/jcal-w.png"
                alt="date"
                width={20}
                height={22}
                quality={100}
              />
              &nbsp;&nbsp;
              {moment(show?.startDate?.iso).format("MMM D, Y")}
            </div>
            <div className="clock mt sfonts">
              <Image
                src="/images/about-us/wclock-white.png"
                alt="time"
                width={22}
                height={22}
                quality={100}
              />
              &nbsp;&nbsp;{" "}
              {moment(show?.startDate?.iso).utc().format("hh:mm A")}
            </div>

            <div className="sShow mt float-left w-100">
              <a
                onClick={() =>
                  handleShareModalOpen(`${SITE_URL}/?showId=${show.objectId}`)
                }
              >
                Share Show
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ArtistShows;
