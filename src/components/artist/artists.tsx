import { LoginModal } from "@components/modals/loginModal";
import { Button, DefaultSkeleton } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { BellIcon } from "@components/theme/icons/bellIcon";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import {
  addArtistSubscribeAPI,
  artistUnsubscribeAPI,
} from "@redux/services/artist.api";
import { whoAmI } from "@redux/services/auth.api";
import {
  ArtistObject,
  ArtistState,
  fetchArtist,
  loadMoreArtist,
} from "@redux/slices/artists";
import { LoginUserState, loginUserSuccess } from "@redux/slices/auth";
import AOS from "aos";
import { useReCaptcha } from "next-recaptcha-v3";
import Image from "next/image";
import Router, { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import useList from "src/libs/useList";
import artistStyles from "./artist.module.scss";

export interface ArtistsProps {
  selectedTags: string[];
}

const Artists = (props: ArtistsProps) => {
  const { selectedTags } = props;
  const dispatch = useDispatch();
  const router = useRouter();

  const { executeRecaptcha, loaded } = useReCaptcha();

  const [isActionLoading, setIsActionLoading] = useState(false);
  const [follows, setFollows] = useState<any[]>([]);
  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [userAction, setUserAction] = useState<"artistSubscribe" | null>(null);
  const [tmpArtistId, setTmpArtistId] = useState<any>(null);

  const {
    artist: { isLoading, data: artistData },
  }: {
    artist: ArtistState;
  } = useSelector((state: RootState) => ({
    artist: state.artist,
  }));

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const { apiParam } = useList({
    queryParams: {
      take: 16,
      skip: 0,
      include: ["metadata"],
    },
  });

  const loadMore = async () => {
    apiParam.skip = (apiParam.skip || 0) + 16;
    const recaptchaResponse = await executeRecaptcha("artists_list");
    dispatch(loadMoreArtist(apiParam, selectedTags, recaptchaResponse));
  };
  const handleArtist = (data: string) => {
    if (data) {
      Router.push(`/artist/${data}`);
    }
  };

  const getArtist = async () => {
    const recaptchaResponse = await executeRecaptcha("artists_list");
    dispatch(fetchArtist(apiParam, selectedTags, recaptchaResponse));
  };

  const handleArtistSubscribe = async (artistId: string) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          await addArtistSubscribeAPI(artistId);
          showToast(SUCCESS_MESSAGES.subscribeWebsite, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      setIsLoginModalOpen(true);
      setUserAction("artistSubscribe");
      setTmpArtistId(artistId);
      // showToast(SUCCESS_MESSAGES.requiredLoginSubscribe, "warning");
    }
  };

  const handleArtistUnsubscribe = async (artistId: string) => {
    if (loginUserState?.isLogin) {
      const userId = loginUserState?.data?.user?.objectId;
      if (userId) {
        setIsActionLoading(true);
        try {
          await artistUnsubscribeAPI(artistId);
          showToast(SUCCESS_MESSAGES.unsubscribeWebsite, "success");
          setIsActionLoading(false);
          const auth = await whoAmI();
          dispatch(loginUserSuccess(auth));
        } catch (error: any) {
          setIsActionLoading(false);
        }
      }
    } else {
      showToast(SUCCESS_MESSAGES.requiredLoginSubscribe, "warning");
    }
  };

  useEffect(() => {
    if (loaded) {
      getArtist();
    }
  }, [apiParam, selectedTags, loaded]);

  useEffect(() => {
    if (loginUserState?.data?.user?.follows) {
      setFollows(loginUserState?.data?.user?.follows);
    }
  }, [loginUserState?.data?.user]);

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      {artistData &&
        artistData?.list &&
        artistData?.list.length > 0 &&
        artistData.list.map((artist: ArtistObject, index: number) => (
          <div className="col-12 col-sm-4 col-md-3 acHold" key={index}>
            <div className="ArtCard float-left w-100">
              <div className={artistStyles.thumb}>
                <Image
                  className="cursor-pointer"
                  src={artist.posterImage?.url || "/images/artist-img.png"}
                  alt={artist.name}
                  onClick={(e) => handleArtist(artist.vanity)}
                  width={425}
                  height={260}
                />

                <div className={`${artistStyles.thumb__catgrs}`}>
                  {artist.tags &&
                    artist.tags.map((tag, index) => (
                      <span
                        key={index}
                        className="cursor-pointer"
                        onClick={(e) => {
                          e.preventDefault();
                          router.push(`/browse?tag=${tag}`);
                        }}
                      >
                        {capitalizeString(tag)}
                      </span>
                    ))}
                </div>
                <div className={`${artistStyles.thumb__favrt}`}>
                  {artist?.objectId && follows.indexOf(artist.objectId) > -1 ? (
                    <span
                      onClick={(e) => {
                        e.preventDefault();
                        handleArtistUnsubscribe(artist?.objectId);
                      }}
                      className="unfollow"
                    >
                      <BellIcon />
                    </span>
                  ) : (
                    <span
                      onClick={(e) => {
                        e.preventDefault();
                        handleArtistSubscribe(artist?.objectId);
                      }}
                    >
                      <BellIcon />
                    </span>
                  )}
                </div>
              </div>
              <div
                className={`title w-100 float-left cursor-pointer ${artistStyles.title}`}
                onClick={(e) => handleArtist(artist.vanity)}
              >
                {capitalizeString(artist.name)}
              </div>
              {/* <!-- note: provision is provided upto 2 lines in the title --> */}
              {/* <div className="reviews float-left w-100 vcenter">
                <Image
                  src="/images/rate.png"
                  alt="rate"
                  height={15}
                  width={15}
                />
                &nbsp;
                <span className={`${artistStyles.text_primary}`}>
                  {artist.rating} &nbsp;
                </span>{" "}
                ( {getFormatReview(artist.ratingCount)} Reviews )
              </div> */}
              <div
                className="abstract w-100 float-left two-line-and-text-ellipsis cursor-pointer"
                onClick={(e) => handleArtist(artist.vanity)}
              >
                {artist.metadata?.about &&
                  `${
                    artist.metadata.about.length >= 190
                      ? artist.metadata.about.substring(0, 190) + "..."
                      : artist.metadata.about
                  }`}
              </div>
            </div>
          </div>
        ))}

      {artistData && artistData?.list && artistData?.list.length <= 0 && (
        <EmptyMessage description="No artists found." />
      )}

      {(isActionLoading || isLoading) && <DefaultSkeleton />}

      {artistData?.hasMany && artistData?.hasMany === true && (
        <div className="col-12 load_more">
          <Button
            type="ghost"
            htmlType="button"
            onClick={(e) => {
              e.preventDefault();
              loadMore();
            }}
            loading={isActionLoading || isLoading}
            disabled={isActionLoading || isLoading}
          >
            Load more Artists
          </Button>
        </div>
      )}

      {isLoginModalOpen && userAction && (
        <LoginModal
          {...{
            actionFrom: userAction,
            handleOk: () => {
              setIsLoginModalOpen(false);
              setUserAction(null);
              if (userAction === "artistSubscribe") {
                handleArtistSubscribe(tmpArtistId);
              }
            },
            isOpen: isLoginModalOpen,
            handleCancel: () => {
              setIsLoginModalOpen(false);
            },
          }}
        />
      )}
    </>
  );
};

export default Artists;
