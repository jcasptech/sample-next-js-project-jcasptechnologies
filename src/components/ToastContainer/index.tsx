import React from "react";
import { ToastContainer as ToastifyContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ToastContainer = () => {
  return (
    <ToastifyContainer
      position="top-right"
      theme="dark"
      hideProgressBar={false}
    />
  );
};

const showToast = (
  message: string,
  type?: "success" | "warning" | "error" | "info" | "default"
) => {
  const options = {
    autoClose: 3000,
    hideProgressBar: false,
    type: type,
  };

  toast.dismiss();
  toast(message, options);
};

export { ToastContainer, showToast };
