import ShareThisModal from "@components/modals/ShareThisModal";
import { CalendarIcon } from "@components/theme/icons/calendarIcon";
import { CalendarClock } from "@components/theme/icons/calenderClockIcon";
import { ClockIcon } from "@components/theme/icons/clockIcon";
import { DeleteIcon } from "@components/theme/icons/deleteIcon";
import { VenueShareIcon } from "@components/theme/icons/venueShareIcon";
import { UserShowsObject } from "@redux/slices/userShows";
import { Popconfirm } from "antd";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { SITE_URL } from "src/libs/constants";
import { capitalizeString } from "src/libs/helpers";
import UserShowsStyles from "./userShows.module.scss";

export interface UserShowsProps {
  shows: UserShowsObject;
  handleDelete: (d: any) => void;
}

const UserShows = (props: UserShowsProps) => {
  const { shows, handleDelete } = props;

  const [url, setUrl] = useState("");
  const [isShareModalOpen, setIsShareModalOpen] = useState(false);

  const handleShareModalOpen = (data: any) => {
    if (data) {
      setIsShareModalOpen(true);
      setUrl(data);
    }
  };

  return (
    <>
      {isShareModalOpen && (
        <ShareThisModal
          {...{
            isOpen: isShareModalOpen,
            url: url || "",
            setIsShareModalOpen,
          }}
        />
      )}
      <div className="col-sm-12 col-md-6 col-lg-4">
        <div
          className={`${UserShowsStyles.show_indi_cards} show_indi_card shows-card aos-init aos-animate`}
          data-aos="fade-up"
          data-aos-delay="100"
          data-aos-duration="1200"
        >
          <div className="row m0">
            <div
              className={`${UserShowsStyles.image} col-lg-4 col-md-4 col-xs-12  show_thumb `}
            >
              <Image
                src={shows.iconImage?.url || "/images/ethumb.png"}
                alt={shows?.venueName}
                width={150}
                height={150}
              />
            </div>
            <div
              className={`col-lg-8 col-md-8 col-xs-12 show_meta ${UserShowsStyles.show_meta}`}
            >
              <div className={UserShowsStyles.topHeader}>
                <div>
                  {/* <img src="/images/calic.png" alt="c" /> */}
                  <CalendarIcon />
                  &nbsp; {moment(shows.startDate?.iso).format("D MMM, Y")}
                </div>
                <div>
                  {/* <img src="/images/home/clock.png" alt="c" /> */}
                  <ClockIcon />{" "}
                  {/* {DateTime.fromISO(shows.startDate?.iso, {
                    zone: "utc",
                  }).toFormat("hh:mm A")} */}
                  {moment(shows.startDate?.iso).utc().format("hh:mm A")}
                </div>
              </div>
              <div className="row">
                <div
                  className={` col-12 col-md-12 col-lg-12 col-xl-8 ${UserShowsStyles.venue_name} cursor-pointer`}
                >
                  <Link href={`/venue/${shows?.venue?.vanity}`} target="_blank">
                    {" "}
                    {capitalizeString(shows?.venueName)}{" "}
                  </Link>
                </div>
                <div className={`${UserShowsStyles.format_Name}`}>
                  <div className={UserShowsStyles.artist}>Format</div>
                  <div className={UserShowsStyles.name}>Band</div>
                </div>
              </div>
              <div className="row">
                <div
                  className={`col-12 ${UserShowsStyles.artist_Name} cursor-pointer`}
                >
                  <Link
                    href={`/artist/${shows?.artist?.vanity}`}
                    target="_blank"
                    className={`col-12 ${UserShowsStyles.artist_Name} cursor-pointer`}
                  >
                    <div className={UserShowsStyles.artist}>Artist</div>
                    <div className={UserShowsStyles.name}>
                      {shows?.artistName}
                    </div>
                  </Link>
                </div>
              </div>

              <div className="row">
                <div className="col-12 foots">
                  <div className={UserShowsStyles.sshare}>
                    <a
                      onClick={() =>
                        handleShareModalOpen(
                          `${SITE_URL}/?showId=${shows.objectId}`
                        )
                      }
                    >
                      <VenueShareIcon
                        {...{
                          className: UserShowsStyles.shareIcon,
                        }}
                      />
                    </a>
                  </div>

                  <div>
                    <Popconfirm
                      title="Are you sure you want to delete?"
                      okText="Yes, Delete"
                      cancelText="No"
                      onConfirm={(d) => handleDelete(shows.objectId)}
                    >
                      <div
                        className={`${UserShowsStyles.deleteIcon} cursor-pointer`}
                      >
                        <DeleteIcon />
                      </div>
                    </Popconfirm>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default UserShows;
