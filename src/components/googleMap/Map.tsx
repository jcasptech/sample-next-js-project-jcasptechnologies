import { DefaultLoader } from "@components/theme";
import { GoogleMap, InfoWindow, Marker } from "@react-google-maps/api";
import { RootState } from "@redux/reducers";
import { GeneralState } from "@redux/slices/general";
import { ShowsDataResponse, ShowsObject } from "@redux/slices/shows";
import type { NextPage } from "next";
import dynamic from "next/dynamic";
import { SetStateAction, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { GOOGLE_API_KEY } from "src/libs/constants";
import { loadScript } from "src/libs/helpers";
// import MarkerDetails from "./markerDetails";

const MarkerDetails = dynamic(() => import("./markerDetails"));

export interface MapProps {
  shows: ShowsDataResponse;
  clickedShow: string | null;
  setSelected: (d: any) => void;
  selected: any;
}
const Map: NextPage<MapProps> = (props: MapProps) => {
  const { shows, clickedShow, setSelected, selected } = props;

  const [centerLocation, setCenterLocation] = useState({
    lat: 32.883,
    lng: -117.156,
  });
  const [isLoading, setIsLoading] = useState(true);
  const [showsList, setShowsList] = useState<ShowsObject[]>([]);

  const [markerPositionList, setMarkerPositionList] = useState<any[]>([]);

  const {
    general: { currentLocation },
  }: {
    general: GeneralState;
  } = useSelector((state: RootState) => ({
    general: state.general,
  }));

  const onMarkerClick = (marker: any) => {
    setSelected(marker);
  };

  useEffect(() => {
    if (
      currentLocation?.Latitude !== undefined &&
      currentLocation?.Longitude !== undefined
    ) {
      setCenterLocation({
        lat: currentLocation.Latitude,
        lng: currentLocation.Longitude,
      });
    }
  }, [currentLocation]);

  useEffect(() => {
    if (shows?.list && shows?.list?.length > 0) {
      setShowsList(shows.list);
    }
  }, [shows]);

  useEffect(() => {
    if (showsList.length > 0) {
      const tmpmarkerPositionList: SetStateAction<any[]> = [];
      showsList.forEach((show) => {
        if (show?.location?.latitude && show?.location?.longitude) {
          tmpmarkerPositionList.push({
            position: {
              lat: show?.location?.latitude,
              lng: show?.location?.longitude,
            },
            venue: show.venue,
            showId: show.objectId,
            show: show,
          });
        }
      });
      setMarkerPositionList(tmpmarkerPositionList);
    }
  }, [showsList]);

  useEffect(() => {
    if (clickedShow) {
      markerPositionList.map((show) => {
        if (show?.showId === clickedShow) {
          setSelected(show);
        }
      });
    }
  }, [clickedShow]);

  const handleScriptLoad = () => {
    setIsLoading(false);
  };

  useEffect(() => {
    loadScript(
      `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=places`,
      () => handleScriptLoad()
    );
  }, []);

  return (
    <>
      {isLoading && <DefaultLoader />}

      {!isLoading && (
        <GoogleMap
          options={{
            clickableIcons: true,
            fullscreenControl: true,
            zoomControl: true,
            scaleControl: true,
            streetViewControl: false,
            mapTypeControl: false,
            // styles: [
            //   {
            //     featureType: "all",
            //     stylers: [{ saturation: -80 }, { lightness: 30 }],
            //   },
            // ],
            zoom: 10,
            minZoom: 3,
            mapTypeId: "roadmap",
          }}
          zoom={4}
          center={centerLocation}
          mapTypeId={"roadmap"}
          mapContainerStyle={{
            width: "100%",
            height: "100%",
            borderRadius: "15px",
          }}
          onLoad={() => console.log("Map Component Loaded...")}
        >
          {markerPositionList.map((mark: any, index: any) => (
            <Marker
              key={index}
              position={mark.position}
              icon="/images/general/map-marker.png"
              onClick={() => onMarkerClick(mark)}
            />
          ))}

          {selected && (
            <InfoWindow
              position={selected?.position}
              onCloseClick={() => setSelected(null)}
            >
              <div>
                <MarkerDetails
                  {...{
                    shows,
                    selected,
                  }}
                />
              </div>
            </InfoWindow>
          )}
        </GoogleMap>
      )}
    </>
  );
};

export default Map;
