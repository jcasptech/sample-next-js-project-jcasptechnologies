import ShareThisModal from "@components/modals/ShareThisModal";
import { LocationIcon } from "@components/theme/icons/locationIcon";
import { PhoneIcon } from "@components/theme/icons/phoneIcon";
import { WebsiteColorIcon } from "@components/theme/icons/websiteColorIcon";
import { RootState } from "@redux/reducers";
import { ShowsDataResponse } from "@redux/slices/shows";
import { fetchVenue, VenueState } from "@redux/slices/venueDetails";
import { useReCaptcha } from "next-recaptcha-v3";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SITE_URL } from "src/libs/constants";
import { google_calendar_url } from "src/libs/helpers";
import useList from "src/libs/useList";
import markerDetailStyles from "./markerDetails.module.scss";
import UpcomingEvent from "./upComingEvent";

export interface MarkerDetailsProps {
  shows: ShowsDataResponse;
  selected: any;
}

const MarkerDetails = (props: MarkerDetailsProps) => {
  const { shows, selected } = props;
  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();

  const [url, setUrl] = useState("");
  const [isShareModalOpen, setIsShareModalOpen] = useState(false);

  const {
    venueDetails: { isLoading, data: venueData },
  }: {
    venueDetails: VenueState;
  } = useSelector((state: RootState) => ({
    venueDetails: state.venueDetails,
  }));

  const { apiParam } = useList({
    queryParams: {
      id: selected?.venue.objectId,
    },
  });

  const getVenueDetails = async () => {
    const recaptchaResponse = await executeRecaptcha("venue_details");
    dispatch(fetchVenue(apiParam, selected?.venue.objectId, recaptchaResponse));
  };

  useEffect(() => {
    if (selected && loaded) {
      getVenueDetails();
    }
  }, [apiParam, selected, loaded]);

  const handleShareModalOpen = (data: any) => {
    if (data) {
      setIsShareModalOpen(true);
      setUrl(data);
    }
  };

  return (
    <>
      {isShareModalOpen && (
        <ShareThisModal
          {...{
            isOpen: isShareModalOpen,
            url: url || "",
            setIsShareModalOpen,
          }}
        />
      )}

      <div className={markerDetailStyles.map_info} id="map_info">
        <div
          className={`${markerDetailStyles.event_info} event_info`}
          id="event_info"
        >
          <div className="row m0 head vcenter">
            <div
              className={`col-12 col-sm-5  text-ellipsis ${markerDetailStyles.event_info_title}`}
            >
              <h4 className="m-0">{venueData?.venue?.name}</h4>
            </div>
            <div
              className={`col-12 col-sm-7 ${markerDetailStyles.event_info_meta} vcenter`}
            >
              <div className={markerDetailStyles.event_info_reminder}>
                <span>
                  <a
                    href={google_calendar_url(selected?.venue?.show)}
                    target="_blank"
                  >
                    <Image
                      className="m-1"
                      src="/images/general/reminder.png"
                      alt="Set a reminder"
                      width={19}
                      height={20}
                    />
                    &nbsp;Set a reminder
                  </a>
                </span>
              </div>
              <div className={markerDetailStyles.event_info_share}>
                {/* <ul className="evmeta"> */}
                <div>
                  {/* <li> */}
                  <a
                    href={`https://www.google.com/maps/place/${venueData?.venue?.address}`}
                    target="_blank"
                  >
                    <Image
                      src="/images/general/map-location.png"
                      alt="map-location.png"
                      height={25}
                      width={23}
                    />
                  </a>
                  {/* </li> */}
                  {/* <li> */}
                  <a
                    onClick={() =>
                      handleShareModalOpen(
                        `${SITE_URL}/?venueId=${venueData?.venue?.objectId}`
                      )
                    }
                    className="m-2"
                  >
                    <Image
                      src="/images/general/share-icon.png"
                      alt="share"
                      width={21}
                      height={21}
                    />
                  </a>
                  {/* </li> */}
                </div>
                {/* </ul> */}
              </div>
            </div>
          </div>
          <div className={`row ${markerDetailStyles.event_info_address}`}>
            <div className="col-12 col-sm-6">
              <div className={"row"}>
                <a
                  className={markerDetailStyles.event_info_address_location}
                  href={`https://www.google.com/maps/place/${venueData?.venue?.address}`}
                  target="_blank"
                >
                  <span className="col-2">
                    {/* <img src="/images/pin.png" alt="P" /> */}
                    <LocationIcon />
                  </span>
                  <span className="col-10">
                    <p>&nbsp;{venueData?.venue?.address}</p>
                  </span>
                </a>
              </div>
            </div>

            {venueData?.venue?.phone && (
              <div className="col-12 col-sm-6 Foljoss">
                <div className="row">
                  <a
                    href={`tel:${venueData?.venue?.phone}`}
                    className={markerDetailStyles.event_info_address_location}
                  >
                    <span className="col-2">
                      {/* <img src="/images/tel.png" alt="T" /> */}
                      <PhoneIcon />
                    </span>
                    <span className="col-10">
                      <p>&nbsp;{venueData?.venue?.phone}</p>
                    </span>
                  </a>
                </div>
              </div>
            )}

            {venueData?.venue?.site && (
              <div className="col-12 col-sm-6">
                <div className="row">
                  <div className="col-2">
                    {/* <img src="/images/web.png" alt="W" /> */}
                    <WebsiteColorIcon />
                  </div>
                  <div className="col-10">
                    <span>
                      <a href={venueData?.venue?.site} target="_blank">
                        {venueData?.venue?.site}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            )}
          </div>
          <div
            className={`row m0 ${markerDetailStyles.event_info_head} vcenter`}
          >
            <div className="col-12 title">
              <h4>Upcoming Events</h4>
            </div>
          </div>
          <div className="row m0 up_events">
            <UpcomingEvent
              {...{
                venueId: selected.venue.objectId,
              }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default MarkerDetails;
