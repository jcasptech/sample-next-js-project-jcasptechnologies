import ShareThisModal from "@components/modals/ShareThisModal";
import VideoModal from "@components/modals/videoSongModal";
import { Button } from "@components/theme";
import { CalendarIcon } from "@components/theme/icons/calendarIcon";
import { ClockIcon } from "@components/theme/icons/clockIcon";
import { VenueShareIcon } from "@components/theme/icons/venueShareIcon";
import { WhitePlayIcon } from "@components/theme/icons/whitePlayIcon";
import { RootState } from "@redux/reducers";
import {
  fetchUpcomingEvent,
  loadMoreUpcomingEvent,
  upcomingEventObject,
  UpcomingEventState,
} from "@redux/slices/shows/upcomingEvent";
import moment from "moment";
import { useReCaptcha } from "next-recaptcha-v3";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SITE_URL } from "src/libs/constants";
import useList from "src/libs/useList";
import MarkerDetailsStyle from "./markerDetails.module.scss";

export interface UpcomingEventProps {
  venueId: string;
}
const UpcomingEvent = (props: UpcomingEventProps) => {
  const { venueId } = props;
  const router = useRouter();
  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();

  const [youtubeId, setYoutubeId] = useState("");
  const [isVideoModalOpen, setIsVideoModalOpen] = useState(false);
  const [url, setUrl] = useState("");
  const [isShareModalOpen, setIsShareModalOpen] = useState(false);

  const {
    upcomingEvent: { isLoading, data: upcomingEventData },
  }: {
    upcomingEvent: UpcomingEventState;
  } = useSelector((state: RootState) => ({
    upcomingEvent: state.upcomingEvent,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 4,
      include: ["artist"],
    },
  });

  const getVenueEvents = async () => {
    const recaptchaResponse = await executeRecaptcha("venue_events_list");
    dispatch(fetchUpcomingEvent(apiParam, venueId, recaptchaResponse));
  };

  const loadMore = async () => {
    apiParam.skip = (apiParam.skip || 0) + 4;
    const recaptchaResponse = await executeRecaptcha("venue_events_list");
    dispatch(loadMoreUpcomingEvent(apiParam, venueId, recaptchaResponse));
  };

  useEffect(() => {
    if (loaded) {
      getVenueEvents();
    }
  }, [apiParam, loaded, venueId]);

  const handleVideoModal = (youtubeId: any) => {
    if (youtubeId) {
      setYoutubeId(`https://www.youtube.com/watch?v=${youtubeId}`);
      setIsVideoModalOpen(true);
    }
  };
  const handleArtist = (data: string) => {
    if (data) {
      router.push(`/artist/${data}`);
    }
  };

  const handleShareModalOpen = (data: any) => {
    if (data) {
      setIsShareModalOpen(true);
      setUrl(data);
    }
  };

  return (
    <>
      {isShareModalOpen && (
        <ShareThisModal
          {...{
            isOpen: isShareModalOpen,
            url: url || "",
            setIsShareModalOpen,
          }}
        />
      )}

      {upcomingEventData?.list &&
        upcomingEventData?.list?.length > 0 &&
        upcomingEventData?.list.map(
          (item: upcomingEventObject, index: number) => (
            <div
              className="col-12 col-sm-12 col-md-6 up_schedule mp-0"
              key={index}
            >
              <div className="show_indi_card">
                <div className={`${MarkerDetailsStyle.venusCardIcon}`}>
                  <div
                    className={`calc mt sfonts ${MarkerDetailsStyle.venusCardIcon__clIcon}`}
                  >
                    <CalendarIcon />
                    &nbsp;
                    {moment(item.startDate?.iso).format("MMM D, Y")}
                  </div>
                  <div
                    className={`clock-2 mt sfonts ${MarkerDetailsStyle.venusCardIcon__clockPosition}`}
                  >
                    <ClockIcon />
                    &nbsp;{moment(item?.startDate?.iso).utc().format("hh:mm A")}
                  </div>
                  <div
                    className={`${MarkerDetailsStyle.venusCardIcon__sshare} sshare-2 mt sfonts`}
                  >
                    <a
                      className="cursor-pointer"
                      onClick={() =>
                        handleShareModalOpen(
                          `${SITE_URL}/artist/${item?.artist?.vanity}`
                        )
                      }
                    >
                      <VenueShareIcon
                        {...{
                          className: MarkerDetailsStyle.shareIcon,
                        }}
                      />
                    </a>
                  </div>
                </div>
                <div className={`row m-2 ${MarkerDetailsStyle.CardImage}`}>
                  <div
                    className="col-sm-4 col-3 show_thumb p-0 cursor-pointer"
                    onClick={() => handleVideoModal(item?.artist?.youtubeId)}
                  >
                    <Image
                      src={item?.artistIconImage?.url}
                      alt="venue title"
                      width={84}
                      height={84}
                    />
                    <span className={`vw ${MarkerDetailsStyle.PlayIcon}`}>
                      <WhitePlayIcon
                        {...{ className: `${MarkerDetailsStyle.WhiteIcon}` }}
                      />
                    </span>
                  </div>
                  <div className="col-sm-8 col-9 show_meta">
                    <div
                      className="title title-2 text-ellipsis cursor-pointer"
                      onClick={(e) => handleArtist(item?.artist?.vanity)}
                    >
                      {item?.artistName}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        )}

      {upcomingEventData?.hasMany && (
        <div className="col-12 load_more">
          <Button
            type="ghost"
            htmlType="button"
            loading={isLoading}
            disabled={isLoading}
            onClick={(e) => {
              e.preventDefault();
              loadMore();
            }}
          >
            Load more Events
          </Button>
        </div>
      )}

      {isVideoModalOpen && (
        <VideoModal
          {...{
            setIsVideoModalOpen,
            isVideoModalOpen,
            youtubeId,
            setYoutubeId,
          }}
        />
      )}
    </>
  );
};
export default UpcomingEvent;
