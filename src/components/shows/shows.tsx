import { Button } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { ShowsDataResponse } from "@redux/slices/shows";
import AOS from "aos";
import "aos/dist/aos.css";
import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import ShowsStyles from "./shows.module.scss";

const Show = dynamic(() => import("./show"));

export interface ShowsProp {
  shows: ShowsDataResponse;
  handleFilter: (d: any) => void;
  isLoading?: boolean;
  setClickedShow: (d: any) => void;
  showType?: string;
}

const Shows = (props: ShowsProp) => {
  const { shows, handleFilter, isLoading, setClickedShow, showType } = props;
  const [isMore, setMore] = useState(false);

  useEffect(() => {
    if (shows && shows?.hasMany) {
      setMore(shows.hasMany);
    } else {
      setMore(false);
    }
  }, [shows]);

  useEffect(() => {
    setMore(false);
  }, []);

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <div>
        {/* <!-- Tab Nav --> */}
        <div className={ShowsStyles.show_filter}>
          <div>
            <input
              type="text"
              placeholder="Search for Artist / Venue"
              className="search_art"
              data-aos="fade-up"
              data-aos-delay="100"
              data-aos-duration="1200"
              onKeyUp={(e: any) => {
                handleFilter({
                  search: e.target?.value || "",
                });
              }}
            />
          </div>
          <div>
            <div className="nav-wrapper position-relative mb-2">
              <ul className="nav nav-pills nav-fill flex-row flex-md-row">
                <li className="nav-item">
                  <a
                    className={`nav-link mb-sm-3 mb-md-0 ${
                      showType === "TODAY" ? "active disabled" : ""
                    }`}
                    id=""
                    data-bs-toggle="tab"
                    onClick={(e) => {
                      e.preventDefault();
                      handleFilter({
                        showType: "TODAY",
                        text: "No More Today Shows",
                      });
                    }}
                    role="tab"
                    aria-controls="tabs-text-1"
                    aria-selected="true"
                  >
                    Today&apos;s Shows
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={`nav-link mb-sm-3 mb-md-0 ${
                      showType === "UPCOMING" ? "active disabled" : ""
                    }`}
                    id=""
                    data-bs-toggle="tab"
                    role="tab"
                    onClick={(e) => {
                      e.preventDefault();
                      handleFilter({
                        showType: "UPCOMING",
                        text: "No More Upcoming Shows",
                      });
                    }}
                    aria-controls="tabs-text-2"
                    aria-selected="false"
                  >
                    Upcoming Shows
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div className="row">
          <div className={`col-12 ${ShowsStyles.show_count}`}>
            <span>{shows?.total || 0}</span> Shows Found
          </div>
        </div>
        {/* <!-- End of Tab Nav --> */}

        {/* <!-- Tab Content --> */}
        <div className={ShowsStyles.shows_card}>
          <div className={`${ShowsStyles.shows_card__card} border-0`}>
            <div className="card-body p-0">
              <div className="tab-content" id="tabcontent1">
                <div
                  className="tab-pane fade show active"
                  id="tabs-text-1"
                  role="tabpanel"
                  aria-labelledby="tabs-text-1-tab"
                >
                  <div className="row p-0">
                    {shows && shows?.list && shows?.list?.length > 0 && (
                      <>
                        {shows?.list.map((show, index) => (
                          <Show
                            key={index}
                            {...{
                              show,
                              setClickedShow,
                              showType,
                            }}
                          />
                        ))}
                      </>
                    )}
                  </div>

                  {shows?.list && shows?.list?.length <= 0 && (
                    <EmptyMessage description="No shows found." />
                  )}

                  {isMore && (
                    <div className="col-12 load_more">
                      <Button
                        htmlType="button"
                        type="ghost"
                        onClick={(e) => {
                          e.preventDefault();
                          handleFilter({
                            loadMore: true,
                          });
                        }}
                        loading={isLoading}
                        disabled={isLoading}
                      >
                        Load more Shows
                      </Button>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <!-- End of Tab Content --> */}
      </div>
    </>
  );
};

export default Shows;
