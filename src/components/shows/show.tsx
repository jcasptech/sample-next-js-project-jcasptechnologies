import { ShowsObject } from "@redux/slices/shows";
import AOS from "aos";
import "aos/dist/aos.css";
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { capitalizeString } from "src/libs/helpers";
import ShowsStyles from "./shows.module.scss";

export interface ShowProp {
  show: ShowsObject;
  setClickedShow: (d: any) => void;
  showType?: string;
}

const Show = (props: ShowProp) => {
  const { show, setClickedShow, showType } = props;
  const router = useRouter();
  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <div className="col-12 col-sm-12 col-lg-6">
      <div className={ShowsStyles.show_indi_card}>
        <div className="show">
          <div
            className={`${ShowsStyles.show_indi_card__show_thumb} p-0 image cursor-pointer`}
            onClick={() => setClickedShow(show.objectId)}
          >
            <Image
              src={show.iconImage?.url || "/images/location-placeholder.png"}
              alt={show.venueName}
              width={127}
              height={127}
            />
            {/* <span>
              <img src="/images/wi.png" alt="w" className="vw" />
              &nbsp;{getFormatReview(show.venue?.viewCount || 0)} Views
            </span> */}
          </div>
          <div
            className={`${ShowsStyles.show_indi_card__show_meta} description`}
          >
            <div
              className={`${ShowsStyles.date_time} cursor-pointer`}
              onClick={(e) => {
                e.preventDefault();
                router.push(`/venue/${show.venue.vanity}`);
              }}
            >
              <div>
                <Image
                  src="/images/calic.png"
                  alt="Date"
                  width={18}
                  height={20}
                />
                &nbsp;
                {showType === "TODAY"
                  ? "Today"
                  : moment(show.startDate?.iso).format("MMMM D, Y")}
              </div>
              <div>
                <Image
                  src="/images/home/clock.png"
                  alt="Time"
                  width={19}
                  height={18}
                />{" "}
                {moment(show.startDate?.iso).utc().format("hh:mm A")}
              </div>
            </div>

            <div
              className={`${ShowsStyles.title} cursor-pointer`}
              onClick={(e) => {
                e.preventDefault();
                router.push(`/venue/${show.venue.vanity}`);
              }}
            >
              {capitalizeString(show.venueName)}
            </div>

            <div
              className={`${ShowsStyles.footer} cursor-pointer`}
              onClick={(e) => {
                e.preventDefault();
                router.push(`/artist/${show.artist.vanity}`);
              }}
            >
              <span className={`${ShowsStyles.calc} ${ShowsStyles.mt}`}>
                Artist
              </span>
              <a
                className={`${ShowsStyles.clock} ${ShowsStyles.mt} ${ShowsStyles.artname} cursor-pointer`}
              >
                {capitalizeString(show.artistName)}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Show;
