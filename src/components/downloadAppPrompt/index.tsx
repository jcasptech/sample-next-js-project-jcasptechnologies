import { Button } from "@components/theme";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { downloadAPPSMSAPI } from "@redux/services/general.api";
import { useReCaptcha } from "next-recaptcha-v3";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import { SMSFormValidateSchema, SMSInputs } from "src/schemas/SMSSchema";
import style from "./index.module.scss";

const DownloadAppPrompt = () => {
  const [visible, setVisible] = useState(false);
  const [isAlready, setIsAlready] = useState(
    typeof window !== "undefined" &&
      localStorage.getItem("isAlreadyDownloaded") === "true"
      ? true
      : false
  );
  const [isMobile, setIsMobile] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const { executeRecaptcha, loaded } = useReCaptcha();

  const { register, handleSubmit, formState, reset } = useForm<SMSInputs>({
    resolver: yupResolver(SMSFormValidateSchema),
  });

  const handleCancel = () => {
    setVisible(false);
    if (typeof window !== "undefined") {
      localStorage.setItem("isAlreadyDownloaded", "true");
    }
  };

  const onSubmit = async (data: SMSInputs) => {
    setIsLoading(true);
    try {
      const recaptchaResponse = await executeRecaptcha("download_APP_SMS");
      const res = await downloadAPPSMSAPI(data, recaptchaResponse);
      if (res) {
        showToast(SUCCESS_MESSAGES.downloadAPPSuccess, "success");
        reset();
        setVisible(false);
        setIsLoading(false);
        if (typeof window !== "undefined") {
          localStorage.setItem("isAlreadyDownloaded", "true");
        }
      }
    } catch (error: any) {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (isMobile && !isAlready) {
      setVisible(true);
    } else {
      setVisible(false);
    }
  }, [isMobile]);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768); // Adjust the breakpoint as needed
    };

    // Initial check
    handleResize();

    // Attach the event listener
    window.addEventListener("resize", handleResize);

    // Clean up the event listener on unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <>
      <Modal
        show={visible}
        className={style.modalStyle}
        onHide={handleCancel}
        centered
      >
        <Modal.Header closeButton className={style.header}>
          <h1>JCasp Music</h1>
        </Modal.Header>
        <Modal.Body>
          <Image
            className={style.image}
            src={"/images/general/JCasp-site-launch.jpg"}
            alt="JCasp Music"
            width={768}
            height={500}
          />

          <form
            className={`panel-form ${style.form}`}
            onSubmit={handleSubmit(onSubmit)}
            method="POST"
          >
            <fieldset>
              <div className="">
                <div className="row align-center field-row">
                  <div className="col-12">
                    <label className={style.label}>
                      Enter Your Phone Number To Receive Download Link Via SMS
                    </label>
                  </div>
                  <div className="col-12 ">
                    <div className="">
                      <InputField
                        {...{
                          register,
                          formState,
                          id: "phone",
                          placeholder: "111-222-3333",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="row align-center field-row">
                  <div className="col-12 vcenter">
                    <Button
                      htmlType="submit"
                      type="primary"
                      loading={isLoading || formState?.isSubmitting}
                      disabled={isLoading || formState?.isSubmitting}
                    >
                      Send SMS
                    </Button>
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
          <div className="row">
            <div className="col-6 text-center">
              <Link
                href="https://play.google.com/store/apps/details?id=com.JCasp.fender"
                className="hvr-float-shadow"
                target="_blank"
              >
                <Image
                  src="/images/home/google-play.webp"
                  alt="Download from playstore"
                  width={170}
                  height={51}
                  className={style.image}
                />
              </Link>
            </div>
            <div className="col-6 text-center">
              <Link
                href="https://apps.apple.com/us/app/JCasp-local-shows/id932936250"
                className="hvr-float-shadow"
                target="_blank"
              >
                <Image
                  src="/images/home/app_store.webp"
                  alt="Download from appstore"
                  width={170}
                  height={51}
                  className={style.image}
                />
              </Link>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default DownloadAppPrompt;
