import Image from "next/image";
import Link from "next/link";
import ApplicationDetalisStyles from "./applicationDetails.module.scss";

export interface DownloadSectionProps {}

const ApplicationDetalis = (props: DownloadSectionProps) => {
  return (
    <>
      <div className="container-fluid">
        <div className={`row m0 ${ApplicationDetalisStyles.vcenter1} swap_row`}>
          <div
            className={`col-lg-6 col-md-6 col-sm-12 ${ApplicationDetalisStyles.app_snap}`}
          >
            <Image
              src="/images/home/mobile-group.webp"
              alt="download app"
              width={920}
              height={800}
            />
          </div>

          <div
            className={`col-lg-6 col-md-6 col-sm-12 ${ApplicationDetalisStyles.app_text}`}
          >
            <div className="">
              <h2>Your Pocket Guide to Live Music</h2>
              <p className="aos-init aos-animate">
                Unlock the full potential of your local music scene with the
                JCasp mobile app. Browse upcoming shows tailored to your
                location, discover venues that match your vibe, and reach out to
                artists with just a tap. JCasp is your all-in-one platform for
                exploring live music and connecting with the artists in your
                area. Available for download on iOS and Android.
              </p>
              <div
                className={`float-left w-100 ${ApplicationDetalisStyles.app_text_app_dnw_btn}`}
              >
                <Link
                  href="https://play.google.com/store/apps/details?id=com.JCasp.fender"
                  target="_blank"
                  className="hvr-float-shadow"
                >
                  <Image
                    src="/images/home/google-play.webp"
                    alt="Download from playstore"
                    width={170}
                    height={51}
                  />
                </Link>
                <Link
                  href="https://apps.apple.com/us/app/JCasp-local-shows/id932936250"
                  target="_blank"
                  className="hvr-float-shadow"
                >
                  <Image
                    src="/images/home/app_store.webp"
                    alt="Download from appstore"
                    width={170}
                    height={51}
                  />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ApplicationDetalis;
