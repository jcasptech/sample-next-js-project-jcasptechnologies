import { use, useEffect, useState } from "react";
import {
  EmailShareButton,
  FacebookShareButton,
  HatenaShareButton,
  InstapaperShareButton,
  LineShareButton,
  LinkedinShareButton,
  LivejournalShareButton,
  MailruShareButton,
  OKShareButton,
  PinterestShareButton,
  PocketShareButton,
  RedditShareButton,
  TelegramShareButton,
  TumblrShareButton,
  TwitterShareButton,
  ViberShareButton,
  VKShareButton,
  WhatsappShareButton,
  WorkplaceShareButton,
  FacebookIcon,
  TwitterIcon,
} from "react-share";

import Link from "next/link";

export interface SocialShareProps {
  shareUrl?: string | undefined;
  hashtags?: string | undefined;
}

const SocialShare = (props: SocialShareProps) => {
  const { shareUrl, hashtags } = props;

  const [urls, setUrl] = useState<any>("");

  useEffect(() => {
    const url = window.location.href;
    setUrl("https://jcaspdev.jcasptechnologies.com/");
  }, []);

  return (
    <>
      <div>
        <Link
          href={`https://www.facebook.com/sharer/sharer.php?u=${urls}`}
          target="_blank"
        >
          <FacebookIcon />
        </Link>
      </div>

      <div>
        <Link
          href={`https://www.twitter.com/intent/tweet?url=${urls}`}
          target="_blank"
        >
          <TwitterIcon />
        </Link>
      </div>
    </>
  );
};
export default SocialShare;
