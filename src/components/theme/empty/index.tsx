import React from "react";
import { Empty, EmptyProps } from "antd";

type IEmpty = EmptyProps;
export const EmptyMessage: React.FC<IEmpty> = ({ ...rest }) => {
  return <Empty image={`/images/general/no-data.svg`} {...rest} />;
};
