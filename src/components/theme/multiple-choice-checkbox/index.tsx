import style from "./index.module.scss";

export interface MultipleChoiceCheckboxProps {
  label: string;
  handleOnChange: (data: boolean) => void;
  value?: boolean;
  isBeforeLabel?: boolean;
}

const MultipleChoiceCheckbox = (props: MultipleChoiceCheckboxProps) => {
  const { label, handleOnChange, value = false, isBeforeLabel = true } = props;

  return (
    <label
      onClick={() => {
        handleOnChange(!value);
      }}
      className={`${style.checkbox} ${value ? style.checked : ""}`}
    >
      {isBeforeLabel && (
        <span>
          {!value && (
            <svg
              width="26"
              height="26"
              viewBox="0 0 26 26"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="13" cy="13" r="12.5" stroke="#FF6C2C" />
            </svg>
          )}

          {value && (
            <svg
              width="26"
              height="26"
              viewBox="0 0 26 26"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="13" cy="13" r="13" fill="white" />
              <path
                d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                fill="#FF6C2C"
              />
            </svg>
          )}
        </span>
      )}
      {label}
      {!isBeforeLabel && (
        <span>
          {!value && (
            <svg
              width="26"
              height="26"
              viewBox="0 0 26 26"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="13" cy="13" r="12.5" stroke="#FF6C2C" />
            </svg>
          )}

          {value && (
            <svg
              width="26"
              height="26"
              viewBox="0 0 26 26"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="13" cy="13" r="13" fill="white" />
              <path
                d="M7 13.0909L8.63636 11.4545L11.9091 14.7273L17.6364 9L19.2727 10.6364L11.9091 18L7 13.0909Z"
                fill="#FF6C2C"
              />
            </svg>
          )}
        </span>
      )}
    </label>
  );
};

export default MultipleChoiceCheckbox;
