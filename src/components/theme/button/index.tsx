import React from "react";
import { Button as BaseButton } from "antd";
import { ButtonProps } from "antd/lib/button";
import buttonStyles from "./index.module.scss";

export type IButton = ButtonProps;

export const Button: React.FC<IButton> = ({ ...rest }) => {
  return (
    <BaseButton
      {...rest}
      className={`${rest?.className} ${buttonStyles.button}`}
      button-type={rest?.type}
    />
  );
};
