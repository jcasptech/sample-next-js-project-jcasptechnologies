import Image from "next/image";
import { useEffect, useState } from "react";
import { deepClone } from "src/libs/helpers";
import DoropdownStyles from "./dropdown.module.scss";

export interface FilterDropdownOption {
  value: string;
  label: string;
  selected: boolean;
}

export interface FilterDropdownTypes {
  options: FilterDropdownOption[];
  label: string;
}

export interface FilterDropdownProps {
  options: FilterDropdownTypes[];
  onChange: (d: FilterDropdownTypes[]) => void;
  placeholder?: string;
  isShowCheckBox?: boolean;
}

const FilterDropdown = (props: FilterDropdownProps) => {
  const { options, onChange, placeholder, isShowCheckBox } = props;
  const [isVisible, setIsVisible] = useState(false);

  const handleSelect = (optionKey: number, subOptionKey: number) => {
    const tmpOptions = deepClone(options);

    if (tmpOptions[optionKey]?.options[subOptionKey]) {
      tmpOptions[optionKey].options[subOptionKey].selected =
        !tmpOptions[optionKey].options[subOptionKey].selected;
    }

    onChange(tmpOptions);
    setIsVisible(!isVisible);
  };

  const handleVisible = () => {
    setIsVisible(false);
  };

  const handleBlur = () => {
    const timeout = setTimeout(handleVisible, 200);
  };

  return (
    <div
      className={`${DoropdownStyles.fancy_dropdown_wrapper} filter`}
      onBlur={handleBlur}
    >
      <div
        className={`fancy-dropdown-sleeve ${isVisible ? "show" : ""}`}
        onClick={() => setIsVisible(!isVisible)}
      >
        <input
          type="text"
          readOnly
          className="fancy-dropdown"
          onClick={() => setIsVisible(!isVisible)}
          placeholder={placeholder}
        />
      </div>
      <div className={`fancy-dropdown-options ${isVisible ? "show" : ""}`}>
        <div className="options">
          {options?.map((option, index) => (
            <div className="op" key={index}>
              <div className={`heading`}>{option.label}</div>
              <div className="suboption">
                {option?.options?.map((subop, subindex) => (
                  <div
                    className="filter_option cursor-pointer"
                    key={`sub${subindex}`}
                    onClick={() => {
                      handleSelect(index, subindex);
                    }}
                  >
                    <Image
                      src={`/images/general/${
                        subop.selected
                          ? "checked-checkbox.png"
                          : "un-checked-checkbox.png"
                      }`}
                      alt="checked/unchecked"
                      width={24}
                      height={24}
                    />
                    <span
                      className={`after_option ${
                        subop.selected ? subop.label : ""
                      }`}
                    >
                      {subop.label}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default FilterDropdown;
