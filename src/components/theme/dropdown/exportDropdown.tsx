import { useEffect, useState } from "react";
import DoropdownStyles from "./dropdown.module.scss";

export interface CustomDropdownOption {
  value: string;
  label: string;
}
export interface ExportCustomDropdownProps {
  options: CustomDropdownOption[];
  defaultValue?: string;
  onChange?: (d: any) => void;
  placeholder?: string;
  isShowCheckBox?: boolean;
}

const ExportCustomDropdown = (props: ExportCustomDropdownProps) => {
  const { options, defaultValue, onChange, placeholder, isShowCheckBox } =
    props;
  const [isVisible, setIsVisible] = useState(false);
  const [selectedValue, setSelectedValue] = useState(defaultValue);
  const [selectedLabel, setSelectedLabel] = useState("");

  const handleSelect = (option: CustomDropdownOption) => {
    setSelectedValue(option.value);
    setIsVisible(!isVisible);

    if (onChange) {
      onChange(option.value);
      setSelectedLabel(option.label);
    }
  };

  const handleVisible = () => {
    setIsVisible(false);
  };

  const handleBlur = () => {
    const timeout = setTimeout(handleVisible, 200);
  };

  useEffect(() => {
    if (defaultValue) {
      options.map((op) => {
        if (op.value === defaultValue) {
          setSelectedLabel(op.label);
        }
      });
    }
  }, [defaultValue]);

  return (
    <div className={DoropdownStyles.fancy_dropdown_wrapper} onBlur={handleBlur}>
      <div className={`fancy-dropdown-sleeve ${isVisible ? "show" : ""}`}>
        <input
          type="text"
          value={selectedLabel}
          readOnly
          className="fancy-dropdown"
          onClick={() => setIsVisible(!isVisible)}
          placeholder={placeholder}
        />
      </div>
      <div
        className={`fancy-dropdown-exportOptions ${isVisible ? "show" : ""}`}
      >
        <ul className="options-list">
          {options?.map((option, index) => (
            <li
              key={index}
              className="option-item"
              onClick={() => handleSelect(option)}
            >
              {option.label}
              {/* {isShowCheckBox && (
                <img
                  src={
                    option.value === selectedValue
                      ? "/images/general/checked.png"
                      : "/images/general/un-checked.png"
                  }
                />
              )} */}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default ExportCustomDropdown;
