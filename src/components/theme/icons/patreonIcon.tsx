export interface BellIconProps {
  theme?: "light" | "dark";
}
export const PatreonIcon = (props: BellIconProps) => {
  const { theme = "light" } = props;
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 15 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M3 0H0V15H3V0Z" fill={theme === "dark" ? "#000" : "#FFF"} />
      <path
        d="M9.5 0C6.46243 0 4 2.46243 4 5.5C4 8.53757 6.46243 11 9.5 11C12.5376 11 15 8.53757 15 5.5C15 2.46243 12.5376 0 9.5 0Z"
        fill={theme === "dark" ? "#000" : "#FFF"}
      />
    </svg>
  );
};
