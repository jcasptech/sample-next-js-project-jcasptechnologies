import React from "react";

export const FemaleIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={16}
      height={16}
      viewBox="0 0 20 20"
      {...props}
    >
      <g id="Group_3051" data-name="Group 3051">
        <path
          id="Path_7778"
          data-name="Path 7778"
          d="M97.339,11.336a6.653,6.653,0,1,0-5.48,1.9v2.224H90.3a.781.781,0,1,0,0,1.563h1.563v2.2a.782.782,0,0,0,1.563,0v-2.2h1.563a.781.781,0,1,0,0-1.563H93.422V13.232A6.613,6.613,0,0,0,97.339,11.336ZM89.048,10.23a5.08,5.08,0,1,1,7.186,0A5.084,5.084,0,0,1,89.048,10.23Z"
          transform="translate(-85.999 0)"
          fill="currentColor"
        />
      </g>
    </svg>
  );
};
