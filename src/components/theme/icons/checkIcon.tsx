import React from "react";

export const CheckIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={16}
      height={16}
      viewBox="0 0 20 20"
      {...props}
    >
      <g
        id="Group_1642"
        data-name="Group 1642"
        transform="translate(-318.893 -211.393)"
      >
        <circle
          id="Oval_Copy"
          data-name="Oval Copy"
          cx={7}
          cy={7}
          r={7}
          transform="translate(318.893 211.393)"
          fill="#ed1846"
        />
        <g
          id="interface_1_"
          data-name="interface (1)"
          transform="translate(322.75 216.095)"
        >
          <g id="Group_31" data-name="Group 31" transform="translate(0 0)">
            <path
              id="Path_36"
              data-name="Path 36"
              d="M6.194,68.09a.314.314,0,0,0-.445,0L1.984,71.855.536,70.408a.314.314,0,0,0-.445.444l1.67,1.67a.314.314,0,0,0,.445,0l3.988-3.988A.314.314,0,0,0,6.194,68.09Z"
              transform="translate(0 -67.997)"
              fill="#fff"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};
