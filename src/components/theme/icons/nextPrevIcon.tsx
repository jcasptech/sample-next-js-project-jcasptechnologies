import * as React from "react";
export interface PrevSlickIconProps {
  theme?: "dark" | "light";
  className?: string;
}
export const PrevSlickIcon = (props: PrevSlickIconProps) => {
  const { theme, className } = props;
  const [color, setColor] = React.useState("white");
  React.useEffect(() => {
    if (theme === "light") {
      setColor("black");
    }
  }, [theme]);
  return (
    <svg
      className={className}
      width="65"
      height="35"
      viewBox="0 0 65 35"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect x="0.5" y="0.5" width="64" height="34" rx="17" stroke="#BABABA" />
      <path
        d="M10.505 17.505C10.2317 17.7784 10.2317 18.2216 10.505 18.495L14.9598 22.9497C15.2332 23.2231 15.6764 23.2231 15.9497 22.9497C16.2231 22.6764 16.2231 22.2332 15.9497 21.9598L11.99 18L15.9497 14.0402C16.2231 13.7668 16.2231 13.3236 15.9497 13.0503C15.6764 12.7769 15.2332 12.7769 14.9598 13.0503L10.505 17.505ZM30 17.3L11 17.3V18.7L30 18.7V17.3Z"
        fill={color}
      />
    </svg>
  );
};
