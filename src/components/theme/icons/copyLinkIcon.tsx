const CopyLinkIcon = () => {
  return (
    <svg
      width="44px"
      height="44px"
      viewBox="0 0 48 48"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g id="SVGRepo_bgCarrier" strokeWidth="0" />

      <g
        id="SVGRepo_tracerCarrier"
        strokeLinecap="round"
        strokeLinejoin="round"
      />

      <g id="SVGRepo_iconCarrier">
        {" "}
        <g id="Base/copy-link">
          {" "}
          <path d="M0 0H48V48H0V0Z" fill="white" fill-opacity="0.01" />{" "}
          <g id="ç¼–ç»„ 2">
            {" "}
            <g id="ç¼–ç»„">
              {" "}
              <rect
                id="çŸ©å½¢"
                width="48"
                height="48"
                fill="white"
                fill-opacity="0.01"
              />{" "}
              <path
                id="å½¢çŠ¶"
                d="M12 9.92704V7C12 5.34315 13.3431 4 15 4H41C42.6569 4 44 5.34315 44 7V33C44 34.6569 42.6569 36 41 36H38.0174"
                stroke="#ff6c2c"
                strokeWidth="4"
              />{" "}
              <rect
                id="Rectangle Copy"
                x="4"
                y="10"
                width="34"
                height="34"
                rx="3"
                fill="#ff6c2c"
                stroke="#ff6c2c"
                strokeWidth="4"
                strokeLinejoin="round"
              />{" "}
            </g>{" "}
            <g id="ç¼–ç»„_2">
              {" "}
              <g id="Group">
                {" "}
                <path
                  id="Oval"
                  d="M18.4396 23.1098L23.7321 17.6003C25.1838 16.1486 27.5693 16.1806 29.0604 17.6717C30.5515 19.1628 30.5835 21.5483 29.1319 23L27.2218 25.0228"
                  stroke="white"
                  strokeWidth="4"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />{" "}
                <path
                  id="Oval Copy 2"
                  d="M13.4661 28.7469C12.9558 29.2573 11.9006 30.2762 11.9006 30.2762C10.4489 31.7279 10.4095 34.3152 11.9006 35.8063C13.3917 37.2974 15.7772 37.3294 17.2289 35.8777L22.3931 31.1894"
                  stroke="white"
                  strokeWidth="4"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />{" "}
                <path
                  id="Oval Copy"
                  d="M18.6631 28.3283C17.9705 27.6357 17.5927 26.7501 17.5321 25.8547C17.4624 24.8225 17.8143 23.7774 18.5916 23"
                  stroke="white"
                  strokeWidth="4"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />{" "}
                <path
                  id="Oval Copy 3"
                  d="M22.3218 25.8611C23.8129 27.3522 23.8449 29.7377 22.3932 31.1894"
                  stroke="white"
                  strokeWidth="4"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />{" "}
              </g>{" "}
            </g>{" "}
          </g>{" "}
        </g>{" "}
      </g>
    </svg>
  );
};

export default CopyLinkIcon;
