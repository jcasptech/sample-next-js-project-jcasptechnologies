import * as React from "react";

export const VideoIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      aria-hidden="true"
      data-prefix="fab"
      data-icon="github"
      className="github-icon_svg__svg-inline--fa github-icon_svg__fa-github github-icon_svg__fa-w-16"
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      {...props}
    >
      <path
        id="Icon_Video"
        data-name="Icon Video"
        d="M1.2,10.076A1.161,1.161,0,0,1,0,8.957V1.119A1.161,1.161,0,0,1,1.2,0H11.4a1.162,1.162,0,0,1,1.2,1.119V8.957a1.162,1.162,0,0,1-1.2,1.119ZM16.994,8.809,13.8,6.158V5.038H18V8.4a.581.581,0,0,1-.6.56A.621.621,0,0,1,16.994,8.809ZM13.8,5.038V3.918l3.181-2.64a.62.62,0,0,1,.419-.16.581.581,0,0,1,.6.56V5.038Z"
        fill="#ed1846"
      />
    </svg>
  );
};
