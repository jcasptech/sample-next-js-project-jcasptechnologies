import * as React from "react";

export interface NextSlickIconProps {
  theme?: "dark" | "light";
  className: string;
}
export const NextSlickIcon = (props: NextSlickIconProps) => {
  const { theme, className } = props;
  const [color, setColor] = React.useState("white");
  React.useEffect(() => {
    if (theme === "light") {
      setColor("black");
    }
  }, [theme]);
  return (
    <svg
      width="65"
      className={className}
      height="35"
      viewBox="0 0 65 35"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="-0.5"
        y="0.5"
        width="64"
        height="34"
        rx="17"
        transform="matrix(-1 0 0 1 64 0)"
        stroke="#BABABA"
      />
      <path
        d="M54.495 17.505C54.7683 17.7784 54.7683 18.2216 54.495 18.495L50.0402 22.9497C49.7668 23.2231 49.3236 23.2231 49.0503 22.9497C48.7769 22.6764 48.7769 22.2332 49.0503 21.9598L53.01 18L49.0503 14.0402C48.7769 13.7668 48.7769 13.3236 49.0503 13.0503C49.3236 12.7769 49.7668 12.7769 50.0402 13.0503L54.495 17.505ZM35 17.3L54 17.3V18.7L35 18.7V17.3Z"
        fill={color}
      />
    </svg>
  );
};
