export interface ButtonFillProps {
  color?: "light" | "dark";
  text?: string;
  fillButton?: string;
  className?: string;
}
export const ButtonFill = (props: ButtonFillProps) => {
  const { color, text, fillButton, className } = props;
  return (
    <>
      <svg
        width="100%"
        height="100%"
        viewBox="0 0 100% 100%"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect
          x="1"
          y="1"
          width="98%"
          height="96%"
          rx="13"
          fill="url(#paint0_linear_765_60140)"
        />
        <rect
          x="1"
          y="1"
          width="98%"
          height="96%"
          rx="13"
          stroke="url(#paint1_linear_765_60140)"
          strokeWidth="2"
        />
        <text x="35" y="35" font-family="Verdana" font-size="100" fill="black">
          {text || "button"}
        </text>
        <defs>
          <linearGradient
            id="paint0_linear_765_60140"
            x1="72"
            y1="0"
            x2="72"
            y2="56"
            gradientUnits="userSpaceOnUse"
          >
            <stop stop-color="#1D53E1" />
            <stop offset="1" stop-color="#769BFF" />
          </linearGradient>
          <linearGradient
            id="paint1_linear_765_60140"
            x1="72"
            y1="0"
            x2="72"
            y2="56"
            gradientUnits="userSpaceOnUse"
          >
            <stop stop-color="#92B0FF" />
            <stop offset="1" stop-color="#1340B8" />
          </linearGradient>
        </defs>
      </svg>
    </>
  );
};
