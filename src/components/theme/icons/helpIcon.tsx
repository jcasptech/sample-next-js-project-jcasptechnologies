import React from "react";

export interface HelpIconProps {
  className?: string;
}
export const HelpIcon = (props: HelpIconProps) => {
  const { className } = props;
  return (
    <svg
      width="18"
      height="16"
      className={className}
      viewBox="0 0 18 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_575_5892)">
        <path
          d="M0.00209256 7.998C-0.0174302 3.59703 3.8295 0.00535072 8.53685 5.95225e-06C13.2142 -0.00533881 17.0807 3.58946 17.0688 8.01983C17.0564 12.4248 13.2204 16.0125 8.51304 16C3.77664 15.9875 -0.0245726 12.3633 0.00209256 7.998ZM8.53828 0.965181C4.40661 0.998586 1.04584 4.05267 1.03489 7.98776C1.02394 11.8689 4.32471 15.0219 8.52828 15.0286C12.659 15.0348 16.0183 11.9527 16.0312 8.01404C16.0417 4.1373 12.7447 1.00928 8.53638 0.965181H8.53828Z"
          fill="url(#paint0_linear_575_5892)"
        />
        <path
          d="M8.53714 10.1747H8.28716C8.01384 10.1747 8.00717 10.1747 8.02289 9.91062C8.06574 9.20333 8.37334 8.60472 8.87522 8.08227C9.19663 7.74688 9.5628 7.45158 9.86088 7.09571C10.5099 6.32117 10.4466 5.23351 9.70374 4.61574C9.18901 4.18772 8.58476 4.08127 7.94194 4.29417C7.29912 4.50706 6.91723 4.93955 6.792 5.56622C6.73058 5.87221 6.73629 5.87354 6.4006 5.8731C6.22203 5.8731 6.04395 5.8731 5.86586 5.8731C5.76063 5.8731 5.71825 5.83568 5.72301 5.7328C5.76492 4.50306 6.83057 3.40248 8.13288 3.24793C9.68041 3.06353 11.0446 3.98951 11.3018 5.43349C11.4608 6.3274 11.2065 7.13491 10.5661 7.82082C10.2385 8.17268 9.8785 8.49782 9.54804 8.84746C9.26657 9.13954 9.09879 9.51197 9.07187 9.90439C9.05187 10.1743 9.04759 10.1716 8.75427 10.1743L8.53714 10.1747Z"
          fill="url(#paint1_linear_575_5892)"
        />
        <path
          d="M8.52766 12.2481C8.38481 12.2481 8.24196 12.2481 8.09912 12.2481C7.9996 12.2481 7.94293 12.2164 7.94436 12.1144C7.9477 11.8753 7.94436 11.6365 7.94436 11.3978C7.94436 11.3043 7.99531 11.2695 8.08721 11.2695C8.38339 11.2695 8.67972 11.2695 8.97621 11.2695C9.07144 11.2695 9.11335 11.3141 9.11287 11.4032C9.11065 11.6425 9.11065 11.8814 9.11287 12.1198C9.11287 12.2133 9.06192 12.2485 8.97002 12.2481H8.52766Z"
          fill="url(#paint2_linear_575_5892)"
        />
      </g>
      <defs>
        <linearGradient
          id="paint0_linear_575_5892"
          x1="8.53537"
          y1="0"
          x2="8.53537"
          y2="16"
          gradientUnits="userSpaceOnUse"
        >
          <stop stop-color="#8CC6E7" />
          <stop offset="1" stop-color="#39A6E3" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_575_5892"
          x1="8.53525"
          y1="3.22461"
          x2="8.53525"
          y2="10.1747"
          gradientUnits="userSpaceOnUse"
        >
          <stop stop-color="#8CC6E7" />
          <stop offset="1" stop-color="#39A6E3" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_575_5892"
          x1="8.52861"
          y1="11.2695"
          x2="8.52861"
          y2="12.2481"
          gradientUnits="userSpaceOnUse"
        >
          <stop stop-color="#8CC6E7" />
          <stop offset="1" stop-color="#39A6E3" />
        </linearGradient>
        <clipPath id="clip0_575_5892">
          <rect width="17.0667" height="16" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};
