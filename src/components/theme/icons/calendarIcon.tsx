import React from "react";

export const CalendarIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="19.8"
      height="20.2"
      viewBox="0 0 22 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1.10229 9.14453H20.7086"
        stroke="#858585"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.7861 13.4414H15.7955"
        stroke="#858585"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.9053 13.4414H10.9147"
        stroke="#858585"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.01367 13.4414H6.02306"
        stroke="#858585"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.7861 17.7148H15.7955"
        stroke="#858585"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.9053 17.7148H10.9147"
        stroke="#858585"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.01367 17.7148H6.02306"
        stroke="#858585"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.3484 1V4.62008"
        stroke="#858585"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.46289 1V4.62008"
        stroke="#858585"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.562 2.73633H6.24804C3.01769 2.73633 1 4.53585 1 7.84363V17.7982C1 21.158 3.01769 22.9991 6.24804 22.9991H15.5518C18.7924 22.9991 20.7999 21.1892 20.7999 17.8814V7.84363C20.8101 4.53585 18.8026 2.73633 15.562 2.73633Z"
        stroke="#858585"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
