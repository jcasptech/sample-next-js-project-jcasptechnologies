import * as React from "react";

export const EditIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      aria-hidden="true"
      data-prefix="fab"
      data-icon="github"
      className="github-icon_svg__svg-inline--fa github-icon_svg__fa-github github-icon_svg__fa-w-16"
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      {...props}
    >
      <g id="noun_edit_1071793" transform="translate(-10.448 -6)">
        <path
          id="Path_30989"
          className="st0"
          d="M12.3,14.6l-1.9,0.3l0.3-1.9l5.7-5.7l1.6,1.6L12.3,14.6z M18.4,8.5l-1.6-1.6L17.7,6l1.6,1.6 L18.4,8.5z"
          fill="currentcolor"
        />
      </g>
    </svg>
  );
};
