import * as React from "react";

export interface ChatIconProps {
  color?: "dark" | "light";
}
export const ChatIcon = (props: ChatIconProps) => {
  const { color = "dark" } = props;
  return (
    <svg
      width="18"
      height="18"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.04719 0.66706C6.12222 0.655309 3.40448 2.17293 1.88359 4.66735C0.362699 7.16177 0.260603 10.269 1.61448 12.8576L1.78141 13.183C1.91825 13.439 1.94687 13.7391 1.86091 14.0163C1.62269 14.649 1.42358 15.2958 1.2647 15.9528C1.2647 16.2862 1.3601 16.4766 1.71782 16.4687C2.35142 16.3288 2.9754 16.1485 3.58593 15.929C3.84889 15.8566 4.12848 15.8733 4.38088 15.9766C4.61141 16.0877 5.08042 16.3735 5.09632 16.3735C8.15945 17.9841 11.9005 17.5397 14.4996 15.2566C17.0988 12.9734 18.0164 9.32538 16.8062 6.08709C15.5959 2.8488 12.5091 0.692502 9.04719 0.66706Z"
        stroke={color === "dark" ? "white" : "black"}
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M5.07357 9.83268C5.53381 9.83268 5.9069 9.45959 5.9069 8.99935C5.9069 8.53911 5.53381 8.16602 5.07357 8.16602C4.61333 8.16602 4.24023 8.53911 4.24023 8.99935C4.24023 9.45959 4.61333 9.83268 5.07357 9.83268Z"
        fill={color === "dark" ? "white" : "black"}
        stroke={color === "dark" ? "white" : "black"}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.0472 9.83268C9.50744 9.83268 9.88053 9.45959 9.88053 8.99935C9.88053 8.53911 9.50744 8.16602 9.0472 8.16602C8.58696 8.16602 8.21387 8.53911 8.21387 8.99935C8.21387 9.45959 8.58696 9.83268 9.0472 9.83268Z"
        fill={color === "dark" ? "white" : "black"}
        stroke={color === "dark" ? "white" : "black"}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M13.0213 9.83268C13.4816 9.83268 13.8547 9.45959 13.8547 8.99935C13.8547 8.53911 13.4816 8.16602 13.0213 8.16602C12.5611 8.16602 12.188 8.53911 12.188 8.99935C12.188 9.45959 12.5611 9.83268 13.0213 9.83268Z"
        fill={color === "dark" ? "white" : "black"}
        stroke={color === "dark" ? "white" : "black"}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
