import React from "react";

export const SettingIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={30}
      height={35}
      viewBox="0 0 20 20"
      {...props}
    >
      <g
        id="_006-easy-installation"
        data-name="006-easy-installation"
        transform="translate(0)"
      >
        <path
          id="Path_8347"
          data-name="Path 8347"
          d="M28.328,21.06V17.268H26.811a7.831,7.831,0,0,0-.9-2.171l1.073-1.073L24.3,11.343,23.23,12.417a7.829,7.829,0,0,0-2.171-.9V10H17.268v1.517a7.83,7.83,0,0,0-2.171.9l-1.073-1.073-2.681,2.681L12.417,15.1a7.829,7.829,0,0,0-.9,2.171H10V21.06h1.517a7.829,7.829,0,0,0,.9,2.171L11.343,24.3l2.681,2.681L15.1,25.911a7.829,7.829,0,0,0,2.171.9v1.517H21.06V26.811a7.831,7.831,0,0,0,2.171-.9L24.3,26.984,26.984,24.3,25.911,23.23a7.831,7.831,0,0,0,.9-2.171Z"
          transform="translate(-9.627 -9.627)"
          fill="#ff8086"
        />
        <circle
          id="Ellipse_692"
          data-name="Ellipse 692"
          cx={5.489}
          cy={5.489}
          r={5.489}
          transform="translate(4.048 4.048)"
          fill="#fff"
        />
        <circle
          id="Ellipse_693"
          data-name="Ellipse 693"
          cx={3.851}
          cy={3.851}
          r={3.851}
          transform="translate(5.686 5.686)"
          fill="#e5646e"
        />
        <g
          id="Group_12353"
          data-name="Group 12353"
          transform="translate(7.411 7.994)"
        >
          <path
            id="Path_8348"
            data-name="Path 8348"
            d="M200.705,217.9a.743.743,0,0,1-.527-.218l-1.234-1.234L200,215.4l.708.708,1.511-1.511,1.054,1.054-2.038,2.038A.743.743,0,0,1,200.705,217.9Z"
            transform="translate(-198.944 -214.593)"
            fill="#fff"
          />
        </g>
        <g id="Group_12354" data-name="Group 12354" transform="translate(0 0)">
          <path
            id="Path_8349"
            data-name="Path 8349"
            d="M106.3,105.513a.373.373,0,0,0-.225.71,5.161,5.161,0,0,1,2.562,1.828,5.116,5.116,0,1,1-8.214,0,5.161,5.161,0,0,1,2.562-1.828.373.373,0,0,0-.225-.71,5.859,5.859,0,1,0,3.539,0Z"
            transform="translate(-94.99 -101.566)"
          />
          <path
            id="Path_8350"
            data-name="Path 8350"
            d="M18.7,7.268H17.47a8.2,8.2,0,0,0-.72-1.737l.871-.871a.373.373,0,0,0,0-.527L14.939,1.453a.373.373,0,0,0-.527,0l-.871.871A8.2,8.2,0,0,0,11.8,1.6V.373A.373.373,0,0,0,11.432,0H7.641a.373.373,0,0,0-.373.373V1.6a8.193,8.193,0,0,0-1.737.72l-.871-.871a.373.373,0,0,0-.527,0L1.453,4.134a.373.373,0,0,0,0,.527l.871.871A8.2,8.2,0,0,0,1.6,7.268H.372A.373.373,0,0,0,0,7.641v3.792a.373.373,0,0,0,.373.373H1.6a8.194,8.194,0,0,0,.72,1.737l-.871.871a.373.373,0,0,0,0,.527L4.134,17.62a.373.373,0,0,0,.527,0l.871-.871a8.2,8.2,0,0,0,1.737.72V18.7a.373.373,0,0,0,.373.373h3.792A.373.373,0,0,0,11.8,18.7V17.47a8.194,8.194,0,0,0,1.737-.72l.871.871a.373.373,0,0,0,.527,0l2.681-2.681a.373.373,0,0,0,0-.527l-.871-.871a8.2,8.2,0,0,0,.72-1.737H18.7a.373.373,0,0,0,.373-.373V7.641a.373.373,0,0,0-.373-.373Zm-.373,3.792H17.183a.373.373,0,0,0-.362.283,7.455,7.455,0,0,1-.857,2.067.373.373,0,0,0,.055.456l.81.81L14.676,16.83l-.81-.81a.373.373,0,0,0-.456-.055,7.453,7.453,0,0,1-2.067.857.373.373,0,0,0-.283.362v1.145H8.013V17.183a.373.373,0,0,0-.283-.362,7.453,7.453,0,0,1-2.067-.857.373.373,0,0,0-.456.055l-.81.81L2.243,14.676l.81-.81a.373.373,0,0,0,.055-.456,7.453,7.453,0,0,1-.857-2.067.373.373,0,0,0-.362-.283H.745V8.013H1.89a.373.373,0,0,0,.362-.283,7.454,7.454,0,0,1,.857-2.067.373.373,0,0,0-.055-.456l-.81-.81L4.4,2.243l.81.81a.373.373,0,0,0,.456.055A7.453,7.453,0,0,1,7.73,2.251a.373.373,0,0,0,.283-.362V.745H11.06V1.89a.373.373,0,0,0,.283.362,7.452,7.452,0,0,1,2.067.857.373.373,0,0,0,.456-.055l.81-.81L16.83,4.4l-.81.81a.373.373,0,0,0-.055.456,7.454,7.454,0,0,1,.857,2.067.373.373,0,0,0,.362.283h1.145Z"
            transform="translate(0 0)"
          />
          <path
            id="Path_8351"
            data-name="Path 8351"
            d="M142.636,146.858a4.223,4.223,0,1,0,4.223-4.223A4.228,4.228,0,0,0,142.636,146.858Zm7.7,0a3.478,3.478,0,1,1-3.478-3.478A3.482,3.482,0,0,1,150.337,146.858Z"
            transform="translate(-137.323 -137.322)"
          />
          <path
            id="Path_8352"
            data-name="Path 8352"
            d="M190.634,205.5a.372.372,0,0,0-.527,0l-1.054,1.054a.373.373,0,0,0,0,.527l1.234,1.234a1.117,1.117,0,0,0,1.58,0l2.038-2.038a.373.373,0,0,0,0-.527l-1.054-1.054a.373.373,0,0,0-.527,0l-1.248,1.248Zm1.955-.013.527.527-1.774,1.774a.373.373,0,0,1-.527,0l-.971-.971.527-.527.444.444a.373.373,0,0,0,.527,0Z"
            transform="translate(-181.906 -196.971)"
          />
          <circle
            id="Ellipse_694"
            data-name="Ellipse 694"
            cx={0.373}
            cy={0.373}
            r={0.373}
            transform="translate(9.164 3.675)"
          />
        </g>
      </g>
    </svg>
  );
};
