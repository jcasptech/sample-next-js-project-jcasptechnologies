import * as React from "react";

export const ExterLinkIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={26}
      height={10}
      viewBox="0"
      {...props}
    >
      <path
        id="Path_8313"
        data-name="Path 8313"
        d="M8.839,10.839V3.894l-.631.631v5.682H.631V2.631H6.313L6.945,2H0v8.839Z"
        transform="translate(0 -0.737)"
        fill="#ed1846"
      />
      <path
        id="Path_8314"
        data-name="Path 8314"
        d="M12.313,0H9.157l1.136,1.136L6,5.43l.884.884L11.177,2.02l1.136,1.136Z"
        transform="translate(-2.212)"
        fill="#ed1846"
      />
    </svg>
  );
};
