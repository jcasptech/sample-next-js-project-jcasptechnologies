import React from "react";

export const AssignPhysicianIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={26}
      height={26}
      viewBox="0 0 26 26"
      {...props}
    >
      <g id="noun-verified-user-1144941" transform="translate(-88.48 -4.48)">
        <path
          id="Path_31080"
          data-name="Path 31080"
          d="M269.019,398.16,266.49,400.1l-.49,2.194,3.381-2.555Z"
          transform="translate(-169.339 -375.538)"
          fill="currentColor"
        />
        <path
          id="Path_31081"
          data-name="Path 31081"
          d="M279.71,449.68l-3.071,2.348,1.548,1.471a.129.129,0,0,0,.155,0l1.781-1.91a.155.155,0,0,0,.026-.1Z"
          transform="translate(-179.488 -424.683)"
          fill="currentColor"
        />
        <path
          id="Path_31082"
          data-name="Path 31082"
          d="M277.857,297.245l-.645,2.865,2.09-1.6-.284-1.265.877-1.084a.194.194,0,0,0,.026-.129l-.568-1.393c-.026-.052-.052-.077-.1-.077h-1.677c-.052,0-.077.026-.1.077l-.49,1.419a.114.114,0,0,0,.026.129Z"
          transform="translate(-179.803 -276.713)"
          fill="currentColor"
        />
        <path
          id="Path_31083"
          data-name="Path 31083"
          d="M184.119,4.48a6.039,6.039,0,1,0,6.039,6.039A6.047,6.047,0,0,0,184.119,4.48Zm4.568,6.039a4.568,4.568,0,1,1-9.135,0,3.977,3.977,0,0,1,.155-1.11,6.279,6.279,0,0,0,2.606.645,4.292,4.292,0,0,0,2.478-.774c.077-.052,2.09-1.5,3.69-.052a4.353,4.353,0,0,1,.206,1.29Z"
          transform="translate(-85.471)"
          fill="currentColor"
        />
        <path
          id="Path_31084"
          data-name="Path 31084"
          d="M96.119,305.7,93.513,295.12h-.206a4.818,4.818,0,0,0-4.826,4.826l.052,1.806a13.865,13.865,0,0,0,7.587,3.948Z"
          transform="translate(0 -277.246)"
          fill="currentColor"
        />
        <path
          id="Path_31085"
          data-name="Path 31085"
          d="M363.921,302.5a5.011,5.011,0,0,1,5.006-5.006h.052a4.824,4.824,0,0,0-4.155-2.374h-.155L362.321,305.6a15.687,15.687,0,0,0,2.219-.645,5.155,5.155,0,0,1-.619-2.452Z"
          transform="translate(-261.221 -277.246)"
          fill="currentColor"
        />
        <path
          id="Path_31086"
          data-name="Path 31086"
          d="M418.889,362.88a4.49,4.49,0,1,0,4.49,4.49A4.494,4.494,0,0,0,418.889,362.88Zm-.49,6.245L416.67,367.4l.774-.774.929.929,1.961-1.961.774.774Z"
          transform="translate(-310.899 -341.883)"
          fill="currentColor"
        />
      </g>
    </svg>
  );
};
