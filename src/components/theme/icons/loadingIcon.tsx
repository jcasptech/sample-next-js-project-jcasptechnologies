import React from "react";

export const LoadingIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={16}
      height={16}
      viewBox="0 0 20 20"
      {...props}
    >
      <g id="loading-process" transform="translate(0 -0.001)">
        <path
          id="Path_7959"
          data-name="Path 7959"
          d="M11.95,2.051a7,7,0,1,0-9.9,9.9,7,7,0,1,0,9.9-9.9Zm-.45.45A6.322,6.322,0,0,1,13.364,7H11.921A4.927,4.927,0,0,0,7,2.08c-.161,0-.32.008-.477.023V.655Q6.76.637,7,.637A6.322,6.322,0,0,1,11.5,2.5ZM7,11.286A4.285,4.285,0,1,1,11.285,7,4.29,4.29,0,0,1,7,11.286Z"
          transform="translate(0 0)"
          fill="#ed1846"
        />
      </g>
    </svg>
  );
};
