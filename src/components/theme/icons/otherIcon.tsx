import React from "react";

export const OtherIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={20}
      height={20}
      viewBox="0 0 20 20"
      {...props}
    >
      <g
        id="Artboard_1"
        data-name="Artboard – 1"
        clipPath="url(#clip-Artboard_1)"
      >
        <path
          id="other"
          d="M11.248.09H8.55a.54.54,0,0,0,0,1.079h1.4L8.3,2.815a4.586,4.586,0,1,0-3.527,8.032V12.23H3.155a.54.54,0,0,0,0,1.079H4.773v1.349a.54.54,0,0,0,1.079,0V13.309H7.471a.54.54,0,0,0,0-1.079H5.852V10.847A4.579,4.579,0,0,0,9.029,3.611l1.679-1.679v1.4a.54.54,0,0,0,1.079,0V.63a.54.54,0,0,0-.54-.54ZM5.313,9.8a3.507,3.507,0,1,1,2.48-1.027A3.507,3.507,0,0,1,5.313,9.8Z"
          transform="translate(0 0)"
          fill="currentColor"
        />
      </g>
    </svg>
  );
};
