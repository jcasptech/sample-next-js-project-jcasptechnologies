import * as React from "react";

export const RescheduleIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      aria-hidden="true"
      data-prefix="fab"
      data-icon="github"
      className="github-icon_svg__svg-inline--fa github-icon_svg__fa-github github-icon_svg__fa-w-16"
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      {...props}
    >
      <g
        id="Group_3351"
        data-name="Group 3351"
        transform="translate(-76.292 -10.541)"
      >
        <g
          id="Group_3348"
          data-name="Group 3348"
          transform="translate(81.365 10.541)"
        >
          <g id="Group_3347" data-name="Group 3347">
            <path
              id="Path_7939"
              data-name="Path 7939"
              d="M191.935,0V1.448h1.448v.724a4.854,4.854,0,0,1,1.448,0V1.448h1.448V0Z"
              transform="translate(-191.935)"
              fill="#ed1846"
            />
          </g>
        </g>
        <g
          id="Group_3350"
          data-name="Group 3350"
          transform="translate(76.292 12.207)"
        >
          <g id="Group_3349" data-name="Group 3349">
            <path
              id="Path_7940"
              data-name="Path 7940"
              d="M54.6,52.18l.507-.507.579.579L56.7,51.239l-2.172-2.172-1.014,1.014.579.579-.652.652a7.166,7.166,0,1,0,1.158.869ZM49.677,62.533a5.068,5.068,0,0,1,0-10.135v5.068h5.068A5.037,5.037,0,0,1,49.677,62.533Z"
              transform="translate(-42.432 -49.067)"
              fill="#ed1846"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};
