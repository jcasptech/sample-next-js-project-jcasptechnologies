import * as React from "react";

export const FilterIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      aria-hidden="true"
      data-prefix="fab"
      data-icon="github"
      className="github-icon_svg__svg-inline--fa github-icon_svg__fa-github github-icon_svg__fa-w-16"
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      {...props}
    >
      <path
        id="Path_7523"
        data-name="Path 7523"
        d="M32.809,0H31.655V13.933H30.5V17.4h1.155v2.309h1.155V17.4h1.155V13.933H32.809Z"
        transform="translate(-30.5)"
        fill="#ed1846"
      />
      <path
        id="Path_7524"
        data-name="Path 7524"
        d="M393.809,13.933V0h-1.155V13.933H391.5V17.4h1.155v2.309h1.155V17.4h1.155V13.933Z"
        transform="translate(-377.606)"
        fill="#ed1846"
      />
      <path
        id="Path_7525"
        data-name="Path 7525"
        d="M273.809,0h-1.155V8.544H271.5v3.464h1.155v7.7h1.155v-7.7h1.155V8.544h-1.155Z"
        transform="translate(-262.224)"
        fill="#ed1846"
      />
      <path
        id="Path_7526"
        data-name="Path 7526"
        d="M152.809,0h-1.155V2.065H150.5V5.529h1.155V19.706h1.155V5.529h1.155V2.065h-1.155Z"
        transform="translate(-145.881)"
        fill="#ed1846"
      />
    </svg>
  );
};
