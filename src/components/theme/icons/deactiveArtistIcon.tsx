import React from "react";

export interface DeactiveArtistIconProps {}

export const DeactiveArtistIcon = (props: DeactiveArtistIconProps) => {
  return (
    <svg
      fill="#FFF"
      width="24px"
      height="24px"
      viewBox="0 0 32 32"
      id="icon"
      xmlns="http://www.w3.org/2000/svg"
      stroke="#FFF"
    >
      <g id="SVGRepo_bgCarrier" strokeWidth="0" />

      <g
        id="SVGRepo_tracerCarrier"
        strokeLinecap="round"
        strokeLinejoin="round"
      />

      <g id="SVGRepo_iconCarrier">
        <rect x="10" y="14" width="12" height="4" />

        <path d="M26,4H6A2,2,0,0,0,4,6V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V6A2,2,0,0,0,26,4ZM6,26V6H26V26Z" />

        <rect id="_Transparent_Rectangle_" fill="none" width="32" height="32" />
      </g>
    </svg>
  );
};
