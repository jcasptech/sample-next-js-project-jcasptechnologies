import React from "react";

export const DeleteIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="38"
      height="38"
      viewBox="0 0 38 38"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <circle cx="19" cy="19" r="19" fill="white" />
      <path
        d="M11.6667 26.7828C11.6667 28.0024 12.6666 29 13.8889 29H22.7778C24.0001 29 25 28.0024 25 26.7828V14H11.6667V26.7828ZM26.6667 10.6667H22.5L21.1049 9H15.5618L14.1667 10.6667H10V12.3333H26.6667V10.6667Z"
        fill="#EE4848"
      />
    </svg>
  );
};
