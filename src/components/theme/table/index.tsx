import { Button, Empty, Table, TableProps } from "antd";
import { FilterValue } from "antd/lib/table/interface";
import React from "react";
import { DEFAULT_TABLE_LIMIT } from "src/libs/constants";
import { EmptyMessage } from "../empty";

export interface TableComponentProps extends TableProps<any> {
  onTableChange?: (d: any) => void;
  className?: string;
  showHeader?: boolean;
}

const TableComponent = (props: TableComponentProps) => {
  const {
    rowKey = "",
    columns = [],
    dataSource = [],
    pagination = {},
    loading,
    onTableChange,
    className,
    showHeader = true,
  } = props;

  const onChange = (
    changePagination: any,
    changeFilters: Record<string, FilterValue | null>,
    changeSorter: any | any[]
  ) => {
    if (typeof onTableChange === "function") {
      let lkPagination = {};
      const lkFilters = {};
      let lkSorter = {};
      if (
        changePagination &&
        changePagination?.current &&
        changePagination?.defaultPageSize
      ) {
        // FIND LIMIT
        const limit = changePagination?.defaultPageSize || DEFAULT_TABLE_LIMIT;

        lkPagination = {
          skip: changePagination?.current * limit - limit,
        };
      }
      if (changeSorter?.field && changeSorter?.order) {
        lkSorter = {
          orderBy: `${changeSorter?.field}|${
            changeSorter?.order === "ascend" ? "asc" : "desc"
          }`,
        };
      }
      onTableChange({
        pagination: lkPagination,
        filters: lkFilters,
        sorter: lkSorter,
      });
    }
  };

  return (
    <Table
      rowKey={rowKey}
      columns={columns}
      dataSource={dataSource}
      onChange={onChange}
      className={`lkTable ${className}`}
      loading={loading}
      pagination={pagination}
      showHeader={showHeader}
      locale={{ emptyText: <EmptyMessage /> }}
    />
  );
};

export default TableComponent;
