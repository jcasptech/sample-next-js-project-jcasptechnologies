import Image from "next/image";
interface SuccessMessageProps {
  className?: String;
}

export const Images = (props: SuccessMessageProps) => {
  const { className }: any = props;
  return (
    <>
      <Image
        src="/images/success.png"
        alt="artist name"
        className={className}
      />
    </>
  );
};
