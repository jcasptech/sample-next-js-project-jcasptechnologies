/* eslint-disable react-hooks/exhaustive-deps */
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import GoogleAutoCompleteComponent from "@components/googleAutoComplete/googleAutoCompleteComponent";
import { Input, Select } from "antd";
import dynamic from "next/dynamic";
import { useEffect, useRef, useState } from "react";
import { Controller } from "react-hook-form";
import "react-quill/dist/quill.snow.css";
import formComponentStyles from "./formComponent.module.scss";
const ReactQuill = dynamic(import("react-quill"), { ssr: false });
import { DatePicker } from "antd";
import { TimePicker } from "antd";
// import "antd/dist/antd.css";
// import DatePicker from "react-multi-date-picker";
// import TimePicker from "react-multi-date-picker/plugins/time_picker";
import { format } from "path/posix";
import moment from "moment";
import Image from "next/image";

export const FormGroup: any = ({ children, className = "" }: any) => {
  return <div className={`${className}`}>{children}</div>;
};

interface InputRadioFieldProps {
  register: any;
  formState: any;
  id: string;
  name: string;
  label: string;
  value: string;
  className?: string;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
}

interface InputFieldProps {
  register: any;
  formState: any;
  id: string;
  type?: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  autoCapitalize?: string;
  autoComplete?: boolean;
  minLength?: number;
  maxLength?: number;
  min?: number;
  max?: number;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  value?: string;
  onChange?: (d: any) => void;
  autoFocus?: boolean;
}

interface InputGoogleAutoCompleteFieldProps {
  register: any;
  formState: any;
  id: string;
  type?: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  autoCapitalize?: string;
  autoComplete?: boolean;
  minLength?: number;
  maxLength?: number;
  min?: number;
  max?: number;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  value?: string;
  onChange?: (d: any) => void;
  onSelectChange?: (d: any) => void;
  onInputChange?: (d: any) => void;
  googleAutoCompleteConfig?: any;
  searchType?: string;
}

interface InputAfterFieldProps {
  register: any;
  formState: any;
  id: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  value?: string;
  addonAfter?: string;
  addonBefore?: string;
  control: any;
  onChange?: (d: any) => void;
}

interface InputDateFieldProps {
  register: any;
  formState: any;
  id: string;
  type?: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  showError?: boolean;
  control?: any;
  dateFormat?: string;
  name?: string;
  disabled?: boolean;
  isDisabledDate?: boolean;
  setValue?: any;
  disabledPast?: boolean;
  onChange?: (d: any) => void;
  allowClear?: boolean;
  isDefaultOpen?: boolean;
  timezone?: string;
}
interface TextAreaFieldProps {
  register: any;
  formState: any;
  id: string;
  type?: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  autoCapitalize?: string;
  autoComplete?: boolean;
  minLength?: number;
  maxLength?: number;
  min?: number;
  max?: number;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  onChange?: (d: any) => void;
}
interface TimePickerInputFieldProps {
  register: any;
  formState: any;
  id: string;
  type?: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  autoCapitalize?: string;
  autoComplete?: boolean;
  minLength?: number;
  maxLength?: number;
  min?: number;
  max?: number;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
}

interface TextAreaFieldProps {
  register: any;
  formState: any;
  id: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  autoCapitalize?: string;
  autoComplete?: boolean;
  minLength?: number;
  maxLength?: number;
  min?: number;
  max?: number;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  onChange?: (d: any) => void;
  autoFocus?: boolean;
  rows?: number;
}

interface SelectFieldProps {
  register: any;
  formState: any;
  control: any;
  id: string;
  name: string;
  label?: string;
  defaultValue?: any;
  placeholder?: string;
  className?: string;
  onSelect?: (relationship: any) => void;
  autoCapitalize?: string;
  showError?: boolean;
  options?: any;
  isClearable?: boolean;
  isMulti?: boolean;
  autoComplete?: boolean;
  setValue?: any;
  disabled?: boolean;
  value?: any;
  onSelectChange?: (d: any) => void;
  onInputChange?: (d: any) => void;
}

interface TextAreaHTMLFieldProps {
  name?: string;
  register: any;
  formState: any;
  id: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  showError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  control?: any;
  onChange?: (d: any) => void;
  value?: any;
}

interface FileDropFieldProps {
  register: any;
  formState: any;
  id: string;
  className?: string;
  showError?: boolean;
  control?: any;
  name?: string;
  disabled?: boolean;
  setValue?: any;
  onChange?: (d: any) => void;
  description?: any;
}
export interface InputDatePickerProps {
  register: any;
  formState: any;
  format?: string;
  id: string;
  type?: string;
  mode?: string;
  label?: string;
  placeholder?: string;
  defaultValue?: string;
  className?: string;
  showError?: boolean;
  value?: any;
  control?: any;
  dateFormat?: string;
  name?: string;
  disabled?: boolean;
  isDisabledDate?: boolean;
  setValue?: any;
  disabledPast?: boolean;
  onChange?: (d: any, e: any) => void;
  allowClear?: boolean;
  isDefaultOpen?: boolean;
  timezone?: string;
}

export const InputRadioField = (props: InputRadioFieldProps) => {
  const {
    register,
    formState,
    id,
    name,
    label,
    value,
    className = "",
    showError = true,
    disabled,
    readOnly,
  } = props;

  const [error, setError] = useState(null);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[name] &&
      formState?.errors[name].message
    ) {
      setError(formState?.errors[name].message);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      <div className="flex items-center mr-2">
        <input
          type="radio"
          {...register(name)}
          {...{
            id,
            className: `${className} ${error ? "ant-input-status-error" : ""}`,
            value,
            disabled,
            readOnly,
          }}
        />

        <label htmlFor={id} className="cursor-pointer">
          <span>{label}</span>
        </label>
      </div>
      {showError && error && (
        <span className="ant-typography ant-typography-danger block">
          {error}
        </span>
      )}
    </>
  );
};

export const InputField = (props: InputFieldProps) => {
  const {
    register,
    formState,
    id,
    type = "text",
    label,
    placeholder,
    defaultValue,
    className = "ant-input input-border",
    autoCapitalize,
    autoComplete,
    minLength,
    maxLength,
    min,
    max,
    showError = true,
    disabled,
    readOnly,
    value,
    onChange = null,
    autoFocus = false,
  } = props;
  const [error, setError] = useState(null);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="w-100">
        <input
          {...register(id)}
          {...{
            id,
            type,
            className: `${className} ${error ? "ant-input-status-error" : ""}`,
            defaultValue,
            autoCapitalize,
            autoComplete,
            placeholder,
            minLength,
            maxLength,
            min,
            max,
            disabled,
            readOnly,
            value,
            autoFocus,
          }}
          onChange={(e: any) => {
            if (onChange) {
              onChange(e);
            }
          }}
          onKeyUp={(e: any) => {
            if (onChange) {
              onChange(e);
            }
          }}
        />
        {showError && error && <span className="text-danger">{error}</span>}
      </div>
    </>
  );
};

export const InputGoogleAutoCompleteField = (
  props: InputGoogleAutoCompleteFieldProps
) => {
  const {
    register,
    formState,
    id,
    type = "text",
    label,
    placeholder,
    defaultValue,
    className = "ant-input input-border",
    autoCapitalize,
    autoComplete,
    minLength,
    maxLength,
    min,
    max,
    showError = true,
    disabled,
    readOnly,
    value,
    onChange = null,
    onSelectChange,
    onInputChange,
    googleAutoCompleteConfig = {
      autoCompleteId: "googleAutoComplete",
      autoPopulateFields: {
        addressLine1: "addressLine1",
        addressLine2: "addressLine2",
        city: "addressCity",
        province: "addressState",
        zip: "addressCode",
        country: "artistLocaleCountry",
        googlePlaceId: "googlePlaceId",
      },
      setValue: () => {},
    },
    searchType = "address",
  } = props;
  const [error, setError] = useState(null);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="">
        <GoogleAutoCompleteComponent
          autoCompleteId={googleAutoCompleteConfig.autoCompleteId}
          autoPopulateFields={googleAutoCompleteConfig.autoPopulateFields}
          setValue={googleAutoCompleteConfig.setValue}
          searchType={searchType}
        >
          <input
            {...register(id)}
            {...{
              id,
              type,
              className: `${className} ${
                error ? "ant-input-status-error" : ""
              }`,
              defaultValue,
              autoCapitalize,
              autoComplete,
              placeholder,
              minLength,
              maxLength,
              min,
              max,
              disabled,
              readOnly,
              value,
            }}
            onChange={(e: any) => {
              if (onChange) {
                onChange(e);
              }
            }}
            onInputChange={onInputChange}
          />
        </GoogleAutoCompleteComponent>
        {showError && error && (
          <span className="ant-typography ant-typography-danger block">
            {error}
          </span>
        )}
      </div>
    </>
  );
};

export const InputPasswordField = (props: InputFieldProps) => {
  const [fontColor1, setFontColor1] = useState("black");
  const [fontColor2, setFontColor2] = useState("black");
  const [fontColor3, setFontColor3] = useState("black");
  const {
    register,
    formState,
    id,
    label,
    placeholder,
    defaultValue,
    className = "",
    autoCapitalize,
    autoComplete,
    minLength,
    maxLength,
    min,
    max,
    showError = true,
    disabled,
    readOnly,
  } = props;
  const [error, setError] = useState(null);

  const [visible, setVisible] = useState(false);
  const [show, setShow] = useState(false);

  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="JCasp-input-group">
        <input
          {...register(id)}
          {...{
            id,
            type: visible ? "text" : "password",
            className: `${className} ${error ? "ant-input-status-error" : ""}`,
            defaultValue,
            autoCapitalize,
            autoComplete,
            placeholder,
            minLength,
            maxLength,
            min,
            max,
            disabled,
            readOnly,
          }}
        />
        <span className="JCasp-input-suffix">
          <span
            role="img"
            aria-label="eye-invisible"
            tabIndex={-1}
            className="anticon anticon-eye-invisible ant-input-password-icon"
            onClick={() => setVisible(!visible)}
          >
            {visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />}
          </span>
        </span>
      </div>

      {showError && error && <span className="text-danger">{error}</span>}
    </>
  );
};

export const InputPasswordFieldWithMessage = (props: InputFieldProps) => {
  const {
    register,
    formState,
    id,
    label,
    placeholder,
    defaultValue,
    className = "",
    autoCapitalize,
    autoComplete,
    minLength,
    maxLength,
    min,
    max,
    showError = true,
    disabled,
    readOnly,
  } = props;
  const [error, setError] = useState(null);

  const [showMyError, setShowMyError] = useState(false);
  const [myError, setMyError] = useState({
    long: false,
    uppercase: false,
    special: false,
  });
  const [visible, setVisible] = useState(false);
  const [show, setShow] = useState(false);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }

    return () => {
      setError(null);
    };
  }, [formState]);

  const handleMessage = (e: any) => {
    let value = e.target.value;
    if (value) {
      let tmp = {
        long: true,
        uppercase: true,
        special: true,
      };
      if (value?.length < 8) {
        tmp.long = true;
      } else {
        tmp.long = false;
      }

      if (!/^(?=.*[A-Z])/.test(value)) {
        tmp.uppercase = true;
      } else {
        tmp.uppercase = false;
      }

      if (!/^(?=.*[!@#$%^&*])/.test(value)) {
        tmp.special = true;
      } else {
        tmp.special = false;
      }

      setMyError(tmp);

      setShowMyError(true);
    } else {
    }
  };

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="JCasp-input-group">
        <input
          {...register(id)}
          {...{
            id,
            type: visible ? "text" : "password",
            className: `${className} ${error ? "ant-input-status-error" : ""}`,
            defaultValue,
            autoCapitalize,
            autoComplete,
            placeholder,
            minLength,
            maxLength,
            min,
            max,
            disabled,
            readOnly,
            onChange: handleMessage,
          }}
        />
        <span className="JCasp-input-suffix">
          <span
            role="img"
            aria-label="eye-invisible"
            tabIndex={-1}
            className="anticon anticon-eye-invisible ant-input-password-icon"
            onClick={() => setVisible(!visible)}
          >
            {visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />}
          </span>
        </span>
      </div>

      {formState.isSubmitted && (
        <div>
          <p className={formComponentStyles.password}>Passwords Must:</p>
          <ul>
            <li
              style={{ color: myError?.long ? "rgba(220, 53, 69)" : "green" }}
            >
              Be at least 8 characters long
            </li>
            <li
              style={{
                color: myError?.uppercase ? "rgba(220, 53, 69)" : "green",
              }}
            >
              Contain at least one uppercase letter
            </li>
            <li
              style={{
                color: myError?.special ? "rgba(220, 53, 69)" : "green",
              }}
            >
              {" "}
              Contain at least one special character
            </li>
          </ul>
        </div>
      )}

      {showError && error && !showMyError && (
        <span className="ant-typography ant-typography-danger text-danger block">
          Password is a required field
        </span>
      )}
    </>
  );
};

export const SignInPasswordField = (props: InputFieldProps) => {
  const {
    register,
    formState,
    id,
    label,
    placeholder,
    defaultValue,
    className = "",
    autoCapitalize,
    autoComplete,
    minLength,
    maxLength,
    min,
    max,
    showError = true,
    disabled,
    readOnly,
  } = props;
  const [error, setError] = useState(null);

  const [visible, setVisible] = useState(false);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="JCasp-input-group">
        <input
          {...register(id)}
          {...{
            id,
            type: visible ? "text" : "password",
            className: `${className} ${error ? "ant-input-status-error" : ""}`,
            defaultValue,
            autoCapitalize,
            autoComplete,
            placeholder,
            minLength,
            maxLength,
            min,
            max,
            disabled,
            readOnly,
          }}
        />
        <span className="JCasp-input-suffix">
          <span
            role="img"
            aria-label="eye-invisible"
            tabIndex={-1}
            className="anticon anticon-eye-invisible ant-input-password-icon"
            onClick={() => setVisible(!visible)}
          >
            {visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />}
          </span>
        </span>
      </div>
      {showError && error && (
        <span className="ant-typography ant-typography-danger text-danger block">
          {error}
        </span>
      )}
    </>
  );
};
export const TextAreaField = (props: TextAreaFieldProps) => {
  const {
    register,
    formState,
    id,
    label,
    placeholder,
    defaultValue,
    className = "ant-input",
    autoCapitalize,
    autoComplete,
    minLength,
    maxLength,
    min,
    max,
    showError = true,
    disabled,
    readOnly,
    rows = 5,
    onChange = null,
    autoFocus = false,
  } = props;
  const [error, setError] = useState(null);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="">
        <textarea
          {...register(id)}
          {...{
            id,
            className: `${className} ${error ? "ant-input-status-error" : ""}`,
            defaultValue,
            autoCapitalize,
            autoComplete,
            placeholder,
            minLength,
            maxLength,
            min,
            max,
            disabled,
            readOnly,
            autoFocus,
            rows,
          }}
          onChange={onChange}
        />
        {showError && error && (
          <span className="ant-typography ant-typography-danger text-danger block">
            {error}
          </span>
        )}
      </div>
    </>
  );
};

export const SelectField = (props: SelectFieldProps) => {
  const {
    formState,
    id,
    control,
    name,
    register,
    label,
    placeholder,
    defaultValue,
    showError = true,
    options,
    disabled,
    isClearable = true,
    className = "react-select",
    isMulti = true,
    onSelectChange,
    onInputChange,
  } = props;
  const [error, setError] = useState(null);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id]?.message
    ) {
      setError(formState?.errors[id]?.message);
    } else {
      setError(null);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="">
        <Controller
          control={control}
          name={name}
          render={({ field }) => (
            <Select
              {...register(id)}
              className={`${className}`}
              defaultValue={defaultValue}
              placeholder={placeholder}
              isClearable={isClearable}
              isMulti={isMulti}
              options={options}
              disabled={disabled}
              onInputChange={onInputChange}
              onChange={(value) => {
                field.onChange(value);
                if (onSelectChange) {
                  onSelectChange(value);
                }
              }}
              value={field.value}
              ref={field.ref}
              classNamePrefix="ant-react-select"
              instanceId={id}
            />
          )}
        />
        {showError && error && (
          <span className="ant-typography ant-typography-danger block">
            {error}
          </span>
        )}
      </div>
    </>
  );
};

export const InputAfterField = (props: InputAfterFieldProps) => {
  const {
    register,
    formState,
    id,
    label,
    placeholder,
    defaultValue,
    className = "ant-input input-border",
    showError = true,
    disabled,
    readOnly = false,
    value,
    addonAfter,
    addonBefore,
    control,
    onChange = null,
  } = props;
  const [error, setError] = useState(null);
  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="">
        <Controller
          control={control}
          name={id}
          render={({ field }) => (
            <Input
              {...register(id)}
              {...{
                id,
                className: `${className} ${
                  error ? "ant-input-status-error" : ""
                }`,
                defaultValue,
                placeholder,
                disabled,
                readOnly,
                value,
                addonAfter,
                addonBefore,
              }}
              onChange={onChange}
            />
          )}
        />
        {showError && error && (
          <span className="ant-typography ant-typography-danger block">
            {error}
          </span>
        )}
      </div>
    </>
  );
};

export const TextAreaHTMLField = (props: TextAreaHTMLFieldProps) => {
  const {
    register,
    formState,
    id,
    label,
    placeholder,
    defaultValue,
    className = "ant-input",
    showError = true,
    disabled,
    readOnly,
    control,
    onChange,
  } = props;

  const [error, setError] = useState(null);

  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    } else {
      setError(null);
    }
    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="">
        <Controller
          control={control}
          name={id}
          render={({ field }) => (
            <>
              <ReactQuill
                {...register(id)}
                name={id}
                defaultValue={defaultValue ? defaultValue : ""}
                disabled={disabled}
                className={`${className} ant-input input-border `}
                placeholder={placeholder}
                readOnly={readOnly}
                onChange={(value) => {
                  field.value = value;
                  if (onChange) {
                    onChange(value);
                  }
                }}
                onBlur={(value) => {
                  field.value = value;
                }}
                modules={{
                  toolbar: [
                    ["bold", "italic", "underline", "strike", "blockquote"],
                    [
                      { list: "ordered" },
                      { list: "bullet" },
                      { indent: "-1" },
                      { indent: "+1" },
                    ],
                    ["link"],
                    ["clean"],
                  ],
                }}
                theme="snow"
              />
            </>
          )}
        />

        {showError && error && (
          <span className="ant-typography ant-typography-danger block">
            {error}
          </span>
        )}
      </div>
    </>
  );
};

export const FileDropField = (props: FileDropFieldProps) => {
  const {
    register,
    formState,
    id,
    className = "",
    showError = true,
    disabled,
    control,
    onChange,
    description,
  } = props;
  const [error, setError] = useState(null);
  const fileInputRef: any = useRef(null);

  const [isDragging, setIsDragging] = useState(false);
  const [selectedFile, setSelectedFile] = useState("");

  const handleDragEnter = (event: any) => {
    event.preventDefault();
    setIsDragging(true);
  };

  const handleDragLeave = () => {
    setIsDragging(false);
  };

  const handleDragOver = (event: any) => {
    event.preventDefault();
  };

  const handleDrop = (event: any) => {
    event.preventDefault();
    setIsDragging(false);

    const files = event.dataTransfer.files;
    // Handle dropped files here
    if (onChange) {
      onChange({
        target: {
          files,
        },
      });
    }
  };

  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    }

    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      <div
        className={`input-file-dropdown`}
        onDragEnter={handleDragEnter}
        onDragLeave={handleDragLeave}
        onDragOver={handleDragOver}
        onDrop={handleDrop}
      >
        <input
          {...register(id)}
          {...{
            id,
            className: `${className}`,
            disabled,
            type: "file",
            ref: fileInputRef,
            onChange: onChange ? onChange : null,
          }}
        />

        <div
          className={`fileDropdown ${isDragging ? "dragging" : ""}`}
          onClick={(e) => {
            e.preventDefault();
            if (fileInputRef.current) {
              fileInputRef.current.click();
            }
          }}
        >
          {description && description}
          {!description && (
            <>
              <Image
                src="/images/dropAudio.png"
                className="mb-3"
                alt="audio"
                height={60}
                width={60}
              />
              <div>
                Drop your File(s) here or <span>Browse </span>
              </div>
              <div className="mute">Supports : JPG, JPEG,PNG </div>
            </>
          )}
        </div>

        {showError && error && (
          <span className="ant-typography ant-typography-danger block">
            {error}
          </span>
        )}
      </div>
    </>
  );
};

export const InputDateField = (props: InputDateFieldProps) => {
  const {
    register,
    formState,
    id,
    label,
    showError = true,
    control,
    dateFormat,
    placeholder,
    name,
    className,
    disabled,
    isDisabledDate,
    setValue,
    defaultValue = null,
    disabledPast = false,
    allowClear = false,
    onChange,
    isDefaultOpen = false,
  } = props;

  const [error, setError] = useState(null);
  const [date, setDate] = useState(defaultValue);
  const [isOpen, setIsOpen] = useState(isDefaultOpen);

  useEffect(() => {
    setDate(defaultValue);
  }, [defaultValue]);

  useEffect(() => {
    if (
      formState &&
      formState?.errors &&
      formState?.errors[id] &&
      formState?.errors[id].message
    ) {
      setError(formState?.errors[id].message);
    } else {
      setError(null);
    }

    return () => {
      setError(null);
    };
  }, [formState]);

  return (
    <>
      {label && (
        <div className="block pb-2">
          <label htmlFor={id}>{label}</label>
        </div>
      )}
      <div className="">
        <Controller
          control={control}
          name={id}
          render={({ field }) => (
            <>
              {isDefaultOpen ? (
                <DatePicker
                  {...register(id)}
                  onChange={(value, stringValue) => {
                    field.onChange(stringValue);
                    setDate(stringValue);
                    // if (setValue) {
                    //   setValue(`${id}`, stringValue);
                    // }
                    if (onChange) {
                      onChange(stringValue);
                    }
                  }}
                  id={id}
                  format={dateFormat}
                  value={date && moment(date).isValid() ? moment(date) : null}
                  ref={field.ref}
                  placeholder={placeholder}
                  defaultValue={defaultValue}
                  disabled={disabled}
                  className={`${className} ant-input input-border `}
                  open={isOpen}
                  onOpenChange={(open: boolean) => {
                    if (isDefaultOpen) {
                      setIsOpen(open);
                    }
                  }}
                />
              ) : (
                <DatePicker
                  {...register(id)}
                  onChange={(value, stringValue) => {
                    field.onChange(stringValue);
                    setDate(stringValue);
                    // if (setValue) {
                    //   setValue(`${id}`, stringValue);
                    // }
                    if (onChange) {
                      onChange(stringValue);
                    }
                  }}
                  format={dateFormat}
                  value={date && moment(date).isValid() ? moment(date) : null}
                  ref={field.ref}
                  placeholder={placeholder}
                  defaultValue={defaultValue}
                  disabled={disabled}
                  className={`${className} ant-input input-border `}
                />
              )}
            </>
          )}
        />
        {showError && error && (
          <span className="ant-typography ant-typography-danger block">
            {error}
          </span>
        )}
      </div>
    </>
  );
};
