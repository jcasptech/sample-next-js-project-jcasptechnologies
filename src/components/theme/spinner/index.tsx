import React from "react";
import { Spinner as Spin } from "react-bootstrap";

const Spinner = ({ className }: any) => {
  return (
    <Spin
      as="span"
      animation="border"
      size="sm"
      role="status"
      aria-hidden="true"
      className={className}
    />
  );
};

export default Spinner;
