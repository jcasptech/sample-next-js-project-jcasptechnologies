import style from "./index.module.scss";

export interface CustomCheckboxProps {
  label: string;
  handleOnChange: (data: boolean) => void;
  value?: boolean;
  isBeforeLabel?: boolean;
}

const CustomCheckbox = (props: CustomCheckboxProps) => {
  const { label, handleOnChange, value = false, isBeforeLabel = false } = props;

  return (
    <div className={style.checkbox}>
      <label
        onClick={() => {
          handleOnChange(!value);
        }}
      >
        {isBeforeLabel && (
          <span>
            {value && (
              <svg
                width="12"
                height="10"
                viewBox="0 0 12 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.132812 4.48958L1.69115 2.93125L4.80781 6.04792L10.262 0.59375L11.8203 2.15208L4.80781 9.16458L0.132812 4.48958Z"
                  fill="white"
                />
              </svg>
            )}
          </span>
        )}
        {label}
        {!isBeforeLabel && (
          <span>
            {value && (
              <svg
                width="12"
                height="10"
                viewBox="0 0 12 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.132812 4.48958L1.69115 2.93125L4.80781 6.04792L10.262 0.59375L11.8203 2.15208L4.80781 9.16458L0.132812 4.48958Z"
                  fill="white"
                />
              </svg>
            )}
          </span>
        )}
      </label>
    </div>
  );
};

export default CustomCheckbox;
