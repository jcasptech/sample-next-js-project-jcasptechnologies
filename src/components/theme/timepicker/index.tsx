import { DateTime } from "luxon";
import React, { useEffect, useState } from "react";
import DatePicker, { DateObject } from "react-multi-date-picker";
import TimePicker from "react-multi-date-picker/plugins/time_picker";

interface CustomTimePickerProps {
  handleOnChange: (data: any) => void;
  value: string;
}

const CustomTimePicker = (props: CustomTimePickerProps) => {
  const { value, handleOnChange } = props;
  const [selectedTime, setSelectedTime] = useState<any>(null);

  useEffect(() => {
    if (value) {
      const time = DateTime.fromFormat(value, "HH:mm:ss");
      console.log(time.toFormat("hh:mm a"));
      setSelectedTime(
        new DateObject().set({
          hour: Number(time.toFormat("HH")) || 18,
          minute: Number(time.toFormat("mm")) || 0,
        })
      );
    }
  }, [value]);

  return (
    <DatePicker
      disableDayPicker
      format="hh:mm A"
      value={selectedTime}
      placeholder="Select time"
      onChange={(d) => {
        if (d) {
          handleOnChange(
            DateTime.fromFormat(d.toString(), "hh:mm a").toFormat("HH:mm:ss")
          );
        }
      }}
      plugins={[
        <TimePicker hideSeconds mStep={15} defaultValue={"06:00 PM"} key="1" />,
      ]}
    />
  );
};

export default CustomTimePicker;
