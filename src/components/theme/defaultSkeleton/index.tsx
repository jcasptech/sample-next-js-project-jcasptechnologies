import Image from "next/image";
import defaultSkeletonStyles from "./defaultSkeleton.module.scss";

interface DefaultSkeletonProps {
  isContent?: boolean;
}
export const DefaultSkeleton = (props: DefaultSkeletonProps) => {
  const { isContent = false } = props;
  return (
    <div
      className={`${defaultSkeletonStyles.wrapper} ${
        isContent ? defaultSkeletonStyles.layoutLoader : ""
      } `}
    >
      <div className={defaultSkeletonStyles.backdrop}></div>
      <div className={defaultSkeletonStyles.preLoader}>
        <Image
          src="/images/loading.gif"
          alt="Loading..."
          className="image"
          width={100}
          height={100}
        />
      </div>
    </div>
  );
};

export const DefaultLoader = () => {
  return (
    <div className="row justify-content-center">
      <div className={defaultSkeletonStyles.defaultLoader}>
        <Image
          src="/images/loading.gif"
          alt="Loading..."
          className="image"
          style={{ maxWidth: "100%" }}
          width={100}
          height={100}
        />
      </div>
    </div>
  );
};
