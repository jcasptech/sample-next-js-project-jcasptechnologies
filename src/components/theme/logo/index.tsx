import Image from "next/image";

/* eslint-disable @next/next/no-img-element, jsx-a11y/alt-text */
export interface LogoProps {
  className?: string;
}

export const Logo = (props: LogoProps) => {
  const { className, collapsed }: any = props;
  return (
    <>
      {!collapsed ? (
        <Image
          src="/images/Logo.png"
          alt="JCasp"
          className={className}
          width={71}
          height={60}
          quality={100}
        />
      ) : (
        <Image
          src="/images/collapsed-logo.svg"
          alt="JCasp"
          className={`${className}`}
          width={71}
          height={60}
          quality={100}
        />
      )}
    </>
  );
};
