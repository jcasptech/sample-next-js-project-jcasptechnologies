import { BellIcon } from "@components/theme/icons/bellIcon";
import { LocationColorIcon } from "@components/theme/icons/locationColorIcon";
import { PhoneColorIcon } from "@components/theme/icons/phoneColorIcon";
import { WebsiteColorIcon } from "@components/theme/icons/websiteColorIcon";
import { VenueObject } from "@redux/slices/venues";
import AOS from "aos";
import "aos/dist/aos.css";
import Image from "next/image";
import { useEffect } from "react";
import { capitalizeString } from "src/libs/helpers";
import VenueStyles from "./venue.module.scss";

export interface VenueProps {
  venue: VenueObject;
  venueFollows: any[];
  handleVenue: (d: any) => void;
  handleVenueSubscribeActions: (
    id: string,
    type: "subscribe" | "unsubscribe"
  ) => void;
}

const Venue = (props: VenueProps) => {
  const { venue, venueFollows, handleVenue, handleVenueSubscribeActions } =
    props;

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <div
      className="col-12 col-sm-4 col-md-3 acHold"
      // data-aos="fade-down"
      // data-aos-delay="100"
      // data-aos-duration="1200"
    >
      <div className="ArtCard float-left w-100">
        <div className={VenueStyles.thumb}>
          <Image
            src={
              venue?.posterImage?.url || "/images/general/venue-placeholder.png"
            }
            onClick={(e) => handleVenue(venue.vanity)}
            className="cursor-pointer"
            alt={venue.name}
            width={450}
            height={260}
          />
          <div className={`${VenueStyles.thumb__favrt}`}>
            {venue?.objectId && venueFollows.indexOf(venue.objectId) > -1 ? (
              <span
                onClick={(e) => {
                  e.preventDefault();
                  handleVenueSubscribeActions(venue.objectId, "unsubscribe");
                }}
                className="unfollow"
              >
                <BellIcon />
              </span>
            ) : (
              <span
                onClick={(e) => {
                  e.preventDefault();
                  handleVenueSubscribeActions(venue.objectId, "subscribe");
                }}
              >
                <BellIcon />
              </span>
            )}
          </div>
        </div>
        <div
          className="title w-100 float-left capitalize cursor-pointer"
          onClick={(e) => handleVenue(venue.vanity)}
        >
          {capitalizeString(venue?.name)}
        </div>

        <div className="float-left w-100 d-flex justify-content-start align-items-center min-14">
          <LocationColorIcon /> &nbsp;{" "}
          <a
            href={
              venue?.address
                ? `https://www.google.com/maps/search/${encodeURI(
                    venue.address
                  )}`
                : "javascript:void(0)"
            }
            target="_blank"
            className="custom-text-muted venue-detail-link"
          >
            {venue?.address}
            {venue.state ? `, ${venue.state}` : ""}
          </a>
        </div>

        <div className="float-left w-100 d-flex justify-content-start align-items-center min-14">
          <WebsiteColorIcon /> &nbsp;{" "}
          <a
            href={venue?.website ? venue.website : "javascript:void(0)"}
            target="_blank"
            className="custom-text-muted venue-detail-link"
          >
            {venue?.website ? venue.website : null}
          </a>
        </div>

        <div className="float-left w-100 d-flex justify-content-start align-items-center min-14">
          <PhoneColorIcon /> &nbsp;{" "}
          <a
            href={venue?.phone ? `tel:${venue.phone}` : "javascript:void(0)"}
            className="custom-text-muted venue-detail-link"
          >
            {venue?.phone ? venue.phone : null}
          </a>
        </div>

        <div className="abstract w-100 float-left two-line-and-text-ellipsis">
          {venue?.about ? venue.about : ""}
        </div>
      </div>
    </div>
  );
};

export default Venue;
