import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { postConfirmPostAPI } from "@redux/services/active-posting.api";
import { bidActionsAPI, bidPayload } from "@redux/services/bids.api";
import { LoginUserState } from "@redux/slices/auth";
import {
  bidObject,
  BidsState,
  fetchBids,
  loadMoreBids,
} from "@redux/slices/bids";
import { Col, Row } from "antd";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import Swal from "sweetalert2";
import Bid from "./bid";
import bidsStyle from "./index.module.scss";

export interface BidsProps {
  postId: string;
  onConfirmed: (d: any) => void;
}

const Bids = (props: BidsProps) => {
  const { postId, onConfirmed } = props;

  const [isEditable, setIsEditable] = useState(false);
  const [isFormLoading, setIsFormLoading] = useState(false);

  const dispatch = useDispatch();
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const {
    bids: { isLoading, data: bidsData },
  }: {
    bids: BidsState;
  } = useSelector((state: RootState) => ({
    bids: state.bids,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 4,
      skip: 0,
    },
  });

  const handleLoadMore = (d: any) => {
    if (postId) {
      apiParam.skip = (apiParam.skip || 0) + 4;
      dispatch(loadMoreBids(postId, apiParam));
    }
  };

  const handleProConfirm = async (bid: bidObject) => {
    Swal.fire({
      title: `Are you sure you want to PRO Confirm ${name}?`,
      text: "PRO confirming means that when you select an artist, an event will be created with all the parameters of this posting, but the posting and event will not be associated with one another. The posting will remain up and unchanged.",
      showCancelButton: true,
      confirmButtonColor: "#0b4121",
      cancelButtonColor: "#454a54",
      confirmButtonText: "Yes, PRO Confirm",
    }).then((result) => {
      if (result.isConfirmed) {
        const postData = async () => {
          try {
            setIsFormLoading(true);
            await postConfirmPostAPI(bid.objectId);
            showToast(SUCCESS_MESSAGES.postingConfirmedSuccess, "success");
            onConfirmed(true);
            setIsFormLoading(false);
          } catch (error) {
            console.log(error);
            setIsFormLoading(false);
          }
        };
        postData();
      }
    });
  };

  const handleHideApplication = async (bid: bidObject) => {
    const data: bidPayload = {
      bidId: bid.objectId,
      isHide: true,
    };

    try {
      await bidActionsAPI(data);
      showToast(SUCCESS_MESSAGES.hideBidSuccess, "success");
      apiParam.skip = 0;
      dispatch(fetchBids(postId, apiParam));
    } catch (err) {
      console.log(err);
    }
  };

  const handleRecommendedArtist = async (
    bid: bidObject,
    isRecommended: boolean
  ) => {
    const data: bidPayload = {
      bidId: bid.objectId,
      isRecommended: isRecommended,
      isRecommendedChanged: true,
    };

    try {
      await bidActionsAPI(data);
      showToast(SUCCESS_MESSAGES.recommendedArtistSuccess, "success");
      apiParam.skip = 0;
      dispatch(fetchBids(postId, apiParam));
    } catch (err) {
      console.log(err);
    }
  };

  const handleReview = async (bid: bidObject) => {
    const data: bidPayload = {
      bidId: bid.objectId,
      isReviewed: true,
    };

    try {
      await bidActionsAPI(data);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (loginUserState?.data?.userType === LOGIN_USER_TYPE.ADMIN) {
      setIsEditable(true);
    }
  }, [loginUserState]);

  useEffect(() => {
    if (postId) {
      dispatch(fetchBids(postId, apiParam));
    }
  }, [postId]);

  return (
    <>
      {isLoading && <DefaultLoader />}
      <div className={bidsStyle.bids_box}>
        {bidsData &&
          bidsData?.list?.length > 0 &&
          bidsData?.list?.map((bid: bidObject, index: number) => (
            <Bid
              key={`bid-${index}`}
              {...{
                bid,
                handleProConfirm,
                handleHideApplication,
                handleRecommendedArtist,
                handleReview,
                isEditable,
                isFormLoading,
              }}
            />
          ))}

        {bidsData?.list && bidsData?.list?.length <= 0 && (
          <EmptyMessage description="No bids found." />
        )}

        {bidsData.hasMany && (
          <Button
            type="ghost"
            htmlType="button"
            loading={isLoading}
            disabled={isLoading}
            onClick={(e) => {
              e.preventDefault();
              handleLoadMore({
                loadMore: true,
              });
            }}
          >
            Load more
          </Button>
        )}
      </div>

      {/* {!isLoading &&
            bidsData &&
            bidsData?.list?.map((bids: any, index: number) => (
              <div
                className={`col-12 message-hold ${activePostingStyle.contain}`}
                key={index}
              >
                <div
                  className="show_indi_card bidsCard"
                  data-aos=""
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <div className="row m0 vcenter ">
                    <div className="col-sm-4 col-md-5 col-lg-5 col-xl-3 col-4 show_thumb p-0 imageArtist">
                      <img
                        src={
                          bids.artistIconImage?.url ||
                          "/images/artist-placeholder.png"
                        }
                        alt="venue title"
                      />
                    </div>
                    <div className="col-sm-8 col-md-7 col-lg-7 col-xl-9 col-8 show_meta">
                      <div className="row message-btns">
                        <div className="row title title-m text-ellipsis brderName mb-0">
                          <Link
                            className="col-12 text-ellipsis cursor-pointer text-primary"
                            target="_blank"
                            href={`/artist/${bids.artist?.vanity}`}
                          >
                            {" "}
                            {bids.artistName || ""}
                          </Link>
                        </div>
                        <div className="message bidsNote col-12 ">
                          {bids.note || ""}
                        </div>
                      </div>
                    </div>
                  </div>

                  
                  <div className="emailPhone">
                    <div className="header">
                      <div>Email </div>
                      <div>
                        <Link href="mailto:contact@jcasptechnologies.com">
                          {bids?.artist?.email}
                        </Link>
                      </div>
                    </div>
                    <div className="footer">
                      <div>Phone </div>
                      <div>
                        <Link href={`tel:+${bids?.artist?.phone}`}>
                          {bids?.artist?.phone}
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className={activePostingStyle.button}>
                    {loginUserState.data?.userType ===
                      LOGIN_USER_TYPE.ADMIN && (
                      <div className="confirm">
                        <Button
                          type="primary"
                          htmlType="button"
                          onClick={() =>
                            handleProConfirm(bids?.objectId, bids.artistName)
                          }
                        >
                          PRO Confirm
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ))}
          {bidsData?.list && bidsData?.list?.length <= 0 && <Empty />}

          {isLoading && <DefaultLoader />}
          {bidsData.hasMany && (
            <div className="col-12 load_more">
              <Button
                type="ghost"
                htmlType="button"
                loading={isLoading}
                disabled={isLoading}
                onClick={(e) => {
                  e.preventDefault();
                  handleLoadMore({
                    loadMore: true,
                  });
                }}
              >
                Load more bids
              </Button>
            </div>
          )} */}
    </>
  );
};

export default Bids;
