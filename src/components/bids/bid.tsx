import { Button } from "@components/theme";
import CustomCheckbox from "@components/theme/checkbox";
import { bidObject } from "@redux/slices/bids";
import { Divider, Popconfirm } from "antd";
import Image from "next/image";
import Link from "next/link";
import bidStyle from "./index.module.scss";

export interface BidProps {
  bid: bidObject;
  handleProConfirm: (bid: bidObject) => void;
  handleReview: (bid: bidObject) => void;
  handleHideApplication: (bid: bidObject) => void;
  handleRecommendedArtist: (bid: bidObject, isRecommended: boolean) => void;
  isEditable: boolean;
  isFormLoading: boolean;
}

const Bid = (props: BidProps) => {
  const {
    bid,
    handleProConfirm,
    handleHideApplication,
    handleReview,
    handleRecommendedArtist,
    isEditable,
    isFormLoading,
  } = props;

  return (
    <div className={bidStyle.bid}>
      <div className="bid__desc">
        <Image
          src={bid.artistIconImage?.url || "/images/artist-placeholder.png"}
          alt={bid.artistName}
          width={115}
          height={115}
        />

        <div className="bid__information">
          <div className="bid__information__name">
            <Link
              className="text-ellipsis cursor-pointer text-primary"
              target="_blank"
              href={`/artist/${bid.artist?.vanity}`}
              onClick={(e) => {
                handleReview(bid);
              }}
            >
              {bid.artistName || ""}
            </Link>

            <CustomCheckbox
              {...{
                handleOnChange: (d: boolean) => {
                  handleRecommendedArtist(bid, d);
                },
                label: "Recommended Artist",
                isBeforeLabel: false,
                value: bid.sortPriority > 100 ? true : false,
              }}
            />
          </div>
          <p>{bid.note || ""}</p>
        </div>
      </div>
      <div className="bid__info">
        <div className="email">
          <label>Email: </label>
          {bid.artist?.email ? (
            <Link href={`mailto:${bid.artist.email}`}>{bid.artist.email}</Link>
          ) : (
            "-"
          )}
        </div>
        <div className="phone">
          <label>Phone: </label>
          {bid.artist?.phone ? (
            <Link href={`tel:${bid.artist.phone}`}>{bid.artist.phone}</Link>
          ) : (
            "-"
          )}
        </div>
      </div>
      {isEditable && (
        <div className="bid__actions">
          <Button
            type="primary"
            htmlType="button"
            onClick={() => handleProConfirm(bid)}
            loading={isFormLoading}
            disabled={isFormLoading}
          >
            PRO Confirm
          </Button>

          <Popconfirm
            title={`Are you sure you want to hide?`}
            okText="Yes, Hide"
            cancelText="Cancel"
            onConfirm={() => {
              handleHideApplication(bid);
            }}
          >
            <Button type="ghost" htmlType="button">
              Hide Application
            </Button>
          </Popconfirm>
        </div>
      )}
      <Divider className="mb-0 mt-0" />
    </div>
  );
};

export default Bid;
