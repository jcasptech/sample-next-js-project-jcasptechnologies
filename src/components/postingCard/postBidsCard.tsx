import { Button, DefaultLoader } from "@components/theme";
import { EmptyMessage } from "@components/theme/empty";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { postConfirmPostAPI } from "@redux/services/active-posting.api";
import {
  ActiveBidsState,
  fetchActivePostingBids,
  loadMoreActivePostingBids,
} from "@redux/slices/activePostingBids";
import { LoginUserState } from "@redux/slices/auth";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { LOGIN_USER_TYPE, SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import Swal from "sweetalert2";
import activePostingStyle from "./activePostingbids.module.scss";

export interface PostbidsCardProps {
  selectEventId: string;
  setDetailsOpen: (d: any) => void;
  setSelectEventId: (d: any) => void;
}
const PostBidsCard = (props: PostbidsCardProps) => {
  const { selectEventId, setDetailsOpen, setSelectEventId } = props;
  const dispatch = useDispatch();
  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const {
    bids: { isLoading, data: bidsData },
  }: {
    bids: ActiveBidsState;
  } = useSelector((state: RootState) => ({
    bids: state.activePostingBids,
  }));

  const { apiParam } = useList({
    queryParams: {
      take: 4,
      skip: 0,
    },
  });

  const handleLoadMore = (d: any) => {
    if (selectEventId) {
      apiParam.skip = (apiParam.skip || 0) + 4;
      dispatch(loadMoreActivePostingBids(selectEventId, apiParam));
    }
  };

  useEffect(() => {
    if (selectEventId) {
      dispatch(fetchActivePostingBids(selectEventId, apiParam));
    }
  }, [selectEventId]);

  const handleLoade = () => {
    if (selectEventId) {
      dispatch(fetchActivePostingBids(selectEventId, apiParam));
    }
  };

  const handleProConfirm = async (data: any, name: string) => {
    Swal.fire({
      title: `Are you sure you want to PRO Confirm ${name}?`,
      text: "PRO confirming means that when you select an artist, an event will be created with all the parameters of this posting, but the posting and event will not be associated with one another. The posting will remain up and unchanged.",
      showCancelButton: true,
      confirmButtonColor: "#0b4121",
      cancelButtonColor: "#454a54",
      confirmButtonText: "Yes, PRO Confirm",
    }).then((result) => {
      if (result.isConfirmed) {
        const postData = async () => {
          try {
            await postConfirmPostAPI(data);
            showToast(SUCCESS_MESSAGES.postingConfirmedSuccess, "success");
            handleLoade();
            setDetailsOpen("");
            setSelectEventId("");
          } catch (error) {
            console.log(error);
          }
        };
        postData();
      }
    });
  };

  return (
    <>
      <div className="col-12 postHold phDet">
        <div className="float-left w-100 pList">
          <div className="vwbid vwdetails w-100 mb-4">
            <a href="#" onClick={() => setDetailsOpen("details")}>
              &#8592; Back to Gig details
            </a>
          </div>

          {/* {isLoading && <DefaultLoader />} */}

          {!isLoading &&
            bidsData &&
            bidsData?.list?.map((bids: any, index: number) => (
              <div
                className={`col-12 message-hold ${activePostingStyle.contain}`}
                key={index}
              >
                <div
                  className="show_indi_card bidsCard"
                  data-aos=""
                  data-aos-delay="100"
                  data-aos-duration="1200"
                >
                  <div className="row m0 vcenter ">
                    <div className="col-sm-4 col-md-5 col-lg-5 col-xl-3 col-4 show_thumb p-0 imageArtist">
                      <Image
                        src={
                          bids.artistIconImage?.url ||
                          "/images/artist-placeholder.png"
                        }
                        alt="Artist title"
                        width={180}
                        height={180}
                        className="h-auto"
                      />
                    </div>
                    <div className="col-sm-8 col-md-7 col-lg-7 col-xl-9 col-8 show_meta">
                      <div className="row message-btns">
                        <div className="row title title-m text-ellipsis brderName mb-0">
                          <Link
                            className="col-12 text-ellipsis cursor-pointer text-primary"
                            target="_blank"
                            href={`/artist/${bids.artist?.vanity}`}
                          >
                            {" "}
                            {bids.artistName || ""}
                          </Link>
                        </div>
                        <div className="message bidsNote col-12 ">
                          {bids.note || ""}
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* <Row justify="space-between" className="emailPhone" align="middle">
                    <Col span={12} className={'header'}>
                      <div>Email </div>
                      <div>
                        <Link href="mailto:contact@jcasptechnologies.com">
                          {bids?.artist?.email}
                        </Link>
                      </div>
                    </Col>
                    <Col span={12} className={'footer'}>
                      <div>Phone </div>
                      <div>
                        <Link href={`tel:+${bids?.artist?.phone}`}>
                          {bids?.artist?.phone}
                        </Link>
                      </div>
                    </Col>
                  </Row> */}
                  <div className="emailPhone">
                    <div className="header">
                      <div>Email </div>
                      <div>
                        <Link href="mailto:contact@jcasptechnologies.com">
                          {bids?.artist?.email}
                        </Link>
                      </div>
                    </div>
                    <div className="footer">
                      <div>Phone </div>
                      <div>
                        <Link href={`tel:+${bids?.artist?.phone}`}>
                          {bids?.artist?.phone}
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className={activePostingStyle.button}>
                    {loginUserState.data?.userType ===
                      LOGIN_USER_TYPE.ADMIN && (
                      <div className="confirm">
                        <Button
                          type="primary"
                          htmlType="button"
                          onClick={() =>
                            handleProConfirm(bids?.objectId, bids.artistName)
                          }
                        >
                          PRO Confirm
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ))}
          {bidsData?.list && bidsData?.list?.length <= 0 && (
            <EmptyMessage description="No bids found." />
          )}

          {isLoading && <DefaultLoader />}
          {bidsData.hasMany && (
            <div className="col-12 load_more">
              <Button
                type="ghost"
                htmlType="button"
                loading={isLoading}
                disabled={isLoading}
                onClick={(e) => {
                  e.preventDefault();
                  handleLoadMore({
                    loadMore: true,
                  });
                }}
              >
                Load more bids
              </Button>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default PostBidsCard;
