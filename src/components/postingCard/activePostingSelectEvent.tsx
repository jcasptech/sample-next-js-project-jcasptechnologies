import { useEffect, useState } from "react";
import { DefaultLoader } from "@components/theme";
import {
  getPostingFullDetailAPI,
  submitGigAPI,
} from "@redux/services/calendar.api";
import moment from "moment";
import Image from "next/image";

export interface ActivePostingSelectEventProps {
  selectEventId: string;
  setDetailsOpen: (d: any) => void;
}
const ActivePostingSelectEvent = (props: ActivePostingSelectEventProps) => {
  const { selectEventId, setDetailsOpen } = props;
  const [postData, setPostData] = useState<any>();
  const [isfullLoading, setIsFullLoading] = useState(false);

  useEffect(() => {
    const getFullDetail = async () => {
      setIsFullLoading(true);
      if (selectEventId) {
        try {
          const data = await getPostingFullDetailAPI(selectEventId);
          setPostData(data);
          setIsFullLoading(false);
        } catch (err: any) {
          console.log(err);
          setIsFullLoading(false);
        }
      }
    };
    getFullDetail();

    setTimeout(() => {
      const divElement = document.getElementById("postingDetails");
      if (window.innerWidth < 1024) {
        if (divElement) {
          divElement.scrollIntoView({ behavior: "smooth" });
        }
      }
    }, 1000);
  }, [selectEventId]);

  const handleAttedence = (data: any) => {
    let people = "<50";

    if (data === "min") {
      people = "<50";
    } else if (data === "small") {
      people = "50-100";
    } else if (data === "medium") {
      people = "100-200";
    } else if (data === "max") {
      people = ">200";
    } else {
      people = "<50";
    }
    return people;
  };

  return (
    <>
      {isfullLoading && <DefaultLoader />}
      {!isfullLoading && (
        <div className="col-12 postHold phDet">
          <div className="float-left w-100 pList">
            <div className="pbid">
              <Image src="/images/rbidc.svg" alt="B" width={19} height={17} />
              &nbsp;{postData?.bidCount || 0} Bids
            </div>
            <div className="vwbid">
              {postData?.bidCount > 0 && (
                <a href="#" onClick={() => setDetailsOpen("bids")}>
                  View Bids &#8594;
                </a>
              )}
            </div>
            <div className="title">{postData?.title || "Title"}</div>
            <div className="desc notePostEvent">{postData?.note || ""}</div>
            <div className="row metaH">
              <div className="col-12 evedt">
                <span className="float-left w-100 tt">Location:</span>
                <span className="float-left w-100 val">
                  {postData?.venue?.address || ""}
                </span>
              </div>
              <div className="col-12 evedt">
                <span className="float-left w-100 tt">Date & Time:</span>
                <span className="float-left w-100 val">
                  {moment(postData?.startDate?.iso)
                    .utc()
                    .format("ddd, MMM DD, YYYY")}{" "}
                  from {moment(postData?.startDate?.iso).utc().format("h:mm A")}{" "}
                  to {moment(postData?.endDate?.iso).utc().format("h:mm A")}{" "}
                  {moment(postData?.startDate?.iso).format("z")}
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">Budget:</span>
                <span className="float-left w-100 val">
                  ${postData?.totalCost || ""}
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">Type of event:</span>
                <span className="float-left w-100 val capitalize">
                  {postData?.variety || ""}
                </span>
              </div>
              <div className="col-12 col-sm-12 evedt">
                <span className="float-left w-100 tt">
                  Type of artist/music wanted:
                </span>
                <span className="float-left w-100 val capitalize">
                  {postData?.genres?.join(", ")}
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">
                  Expected attendance:
                </span>
                <span className="float-left w-100 val">
                  {handleAttedence(postData?.attendanceCategory)} people
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">Indoor/Outdoor:</span>
                <span className="float-left w-100 val capitalize">
                  {postData?.playSetting || ""}
                </span>
              </div>
              <div className="col-12 evedt">
                <span className="float-left w-100 tt">
                  Should the artist bring their own PA system?:
                </span>
                <span className="float-left w-100 val">
                  {postData?.equipmentProvided ? "No" : "Yes"}
                </span>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ActivePostingSelectEvent;
