import VenueSearchModal from "@components/modals/venueSearchModal";
import { Button } from "@components/theme";
import CustomCheckbox from "@components/theme/checkbox";
import { InputField } from "@components/theme/form/formFieldsComponent";
import MultipleChoiceCheckbox from "@components/theme/multiple-choice-checkbox";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { getVenuesSearchAPI } from "@redux/services/general.api";
import { AdminVenuesState } from "@redux/slices/adminVenues";
import { LoginUserState } from "@redux/slices/auth";
import { CreatePostingsPayload } from "@redux/slices/postings";
import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { LOGIN_USER_TYPE } from "src/libs/constants";
import { deepClone } from "src/libs/helpers";
import useList from "src/libs/useList";
import {
  CreatePostingsFormFirstSchema,
  CreatePostingsFormInputs,
} from "src/schemas/createPostingFormSchema";
import createPostingStyles from "./createPosting.module.scss";

export interface GigDetailsProps {
  postingData: CreatePostingsPayload;
  handleNext: (d: any) => void;
  setPostingData: (d: any) => void;
}

const GigDetails = (props: GigDetailsProps) => {
  const { postingData, handleNext, setPostingData } = props;

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<CreatePostingsFormInputs>({
      resolver: yupResolver(CreatePostingsFormFirstSchema),
    });

  const [variety, setVariety] = useState("");
  const [promotion, setpromotion] = useState(false);
  const [placeData, setPlaceData] = useState("");
  const [placeName, setplaceName] = useState("");
  const [isVenueModalOpen, setIsVenueModalOpen] = useState(false);
  const [selectedOption, setSelectedOption]: any = useState(null);

  const [venueOptions, setVenueOptions] = useState<any>([]);

  const loginUserState: LoginUserState = useSelector(
    (state: RootState) => state.loginUser
  );

  const [venueId, setVenueId] = useState("");
  const dispatch = useDispatch();

  const {
    adminVenues: { isLoading, data: venuesData },
  }: {
    adminVenues: AdminVenuesState;
  } = useSelector((state: RootState) => ({
    adminVenues: state.adminVenues,
  }));

  const [venueData, setVenueData] = useState<any>({
    place: "",
    place_id: "",
  });

  useEffect(() => {
    if (postingData) {
      setValue("title", postingData?.title || "");
      setValue("offerAmount", postingData?.offerAmount || "");
      setValue("googlePlaceId", postingData.googlePlaceId || "");
      setValue("venueId", postingData.venueId || "");
      setValue("variety", postingData?.variety || "");
      setVariety(postingData?.variety || "");
      setpromotion(postingData?.promotion || false);
      setPlaceData(postingData?.googlePlaceId || "");
      setplaceName(postingData?.placeName || "");
    }
  }, [postingData]);

  const onSubmit = (data: any) => {
    data["promotion"] = promotion;
    data["placeName"] = placeName;
    if (data) {
      handleNext(data);
    }
  };

  const handleSelect = (place: any) => {
    setPlaceData(place.place_id);
    setValue("googlePlaceId", place.place_id);
    setplaceName(place?.formatted_address);
  };

  useEffect(() => {
    if (variety) {
      setValue("variety", variety);
    }
  }, [variety]);

  const { apiParam } = useList({
    queryParams: {
      search: "",
    },
  });

  const getVenue = async () => {
    try {
      let response = await getVenuesSearchAPI(apiParam);
      setVenueOptions(response);
    } catch (error) {
      console.log(error);
    }
  };

  // useEffect(() => {
  //   if (loginUserState?.data?.user?.role === LOGIN_USER_TYPE.VENUE_ADMIN) {
  //     if (venuesData && venuesData?.list?.length) {
  //       const newData: any = [];
  //       venuesData?.list?.map((venue: any, index: number) => {
  //         newData.push({
  //           value: venue.objectId,
  //           label: venue.name,
  //         });
  //         setVenueOptions(newData);
  //       });
  //     }
  //   }
  // }, [venuesData]);

  useEffect(() => {
    getVenue();
  }, [apiParam]);

  const handleVenueChange = (data: any) => {
    if (data) {
      apiParam.search = data;
      getVenue();
    } else {
      apiParam.search = "";
    }
  };

  const handleChange = (data: any) => {
    if (data) {
      setVenueId(data.value);
      setValue("venueId", data.value);
      setplaceName(data.label);
      setSelectedOption(data);
    }
  };

  const handlePromotion = () => {
    setpromotion(!promotion);
  };

  useEffect(() => {
    if (venueData.place_id !== "") {
      const tmp = deepClone(venueOptions);
      const op = {
        value: venueData.place_id,
        label: venueData.place,
      };

      tmp.push(op);
      setVenueOptions(tmp);
      setVenueId(venueData.place_id);
      setValue("venueId", venueData.place_id);
      setplaceName(venueData.place);

      setSelectedOption(op);
    }
  }, [venueData]);

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="panel-form">
        <div className="stepper-form">
          <div className="row stepper-form-element align-center m-0 p-0 mb-3">
            <div className="col-12 col-sm-3 ">
              <label>Title</label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "title",
                  placeholder: "Event Title",
                }}
              />
            </div>
          </div>
          <div
            className={`row stepper-form-element align-center m-0 p-0 mb-3 ${createPostingStyles.variety}`}
          >
            <div className="col-12 col-sm-3">
              <label>Variety</label>
            </div>
            <div className={`col-12 col-sm-9`}>
              <Row gutter={[20, 20]}>
                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: (d) => {
                        if (d) {
                          setVariety("corporate");
                        }
                      },
                      label: "Corporate",
                      isBeforeLabel: true,
                      value: variety === "corporate",
                    }}
                  />
                </Col>
                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: (d) => {
                        if (d) {
                          setVariety("private");
                        }
                      },
                      label: "Private",
                      isBeforeLabel: true,
                      value: variety === "private",
                    }}
                  />
                </Col>
                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: (d) => {
                        if (d) {
                          setVariety("wedding");
                        }
                      },
                      label: "Wedding",
                      isBeforeLabel: true,
                      value: variety === "wedding",
                    }}
                  />
                </Col>
                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: (d) => {
                        if (d) {
                          setVariety("restaurant");
                        }
                      },
                      label: "Restaurant",
                      isBeforeLabel: true,
                      value: variety === "restaurant",
                    }}
                  />
                </Col>
                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: (d) => {
                        if (d) {
                          setVariety("music venue");
                        }
                      },
                      label: "Music Venue",
                      isBeforeLabel: true,
                      value: variety === "music venue",
                    }}
                  />
                </Col>
                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: (d) => {
                        if (d) {
                          setVariety("other");
                        }
                      },
                      label: "Other",
                      isBeforeLabel: true,
                      value: variety === "other",
                    }}
                  />
                </Col>
              </Row>
              {/* <div className="row m-0 p-0">
                <div className="col-12 col-sm-6 col-lg-4 Tickchk p-0">
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${variety === "corporate" ? "checked" : ""}`}
                    onClick={() => setVariety("corporate")}
                  >
                    <div className="checkbox">
                      <input
                        type="checkbox"
                        name="checkboxes"
                        value="1"
                        id="corporateBox"
                        className=""
                      />
                      Corporate
                    </div>
                  </div>
                </div>
                <div className="col-12 col-sm-6 col-lg-4 Tickchk p-0">
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${variety === "private" ? "checked" : ""}`}
                    onClick={() => setVariety("private")}
                  >
                    <div className="checkbox">
                      <input
                        type="checkbox"
                        name="checkboxes"
                        value="1"
                        id="privateBox"
                        className=""
                      />
                      Private
                    </div>
                  </div>
                </div>
                <div className="col-12 col-sm-6 col-lg-4 Tickchk p-0">
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${variety === "wedding" ? "checked" : ""}`}
                    onClick={() => setVariety("wedding")}
                  >
                    <div className="checkbox">
                      <input
                        type="checkbox"
                        name="checkboxes"
                        value="1"
                        id="weddingbox"
                        className=""
                      />
                      Wedding
                    </div>
                  </div>
                </div>
                <div className="col-12 col-sm-6 col-lg-4 Tickchk p-0">
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${variety === "restaurant" ? "checked" : ""}`}
                    onClick={() => setVariety("restaurant")}
                  >
                    <div className="checkbox">
                      <input
                        type="checkbox"
                        name="checkboxes"
                        value="1"
                        id="restaurantbox"
                        className=""
                      />
                      Restaurant
                    </div>
                  </div>
                </div>
                <div className="col-12 col-sm-6 col-lg-4 Tickchk p-0">
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${variety === "music venue" ? "checked" : ""}`}
                    onClick={() => setVariety("music venue")}
                  >
                    <div className="checkbox">
                      <input
                        type="checkbox"
                        name="checkboxes"
                        value="1"
                        id="music"
                        className=""
                      />
                      Music Venue
                    </div>
                  </div>
                </div>
                <div className="col-12 col-sm-6 col-lg-4 Tickchk p-0">
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${variety === "other" ? "checked" : ""}`}
                    onClick={() => setVariety("other")}
                  >
                    <div className="checkbox">
                      <input
                        type="checkbox"
                        name="checkboxes"
                        value="1"
                        id="otherbox"
                        className=""
                      />
                      Other
                    </div>
                  </div>
                </div>
              </div> */}
              {formState &&
                formState?.errors &&
                formState?.errors["variety"]?.message && (
                  <span className="text-danger">
                    {formState?.errors["variety"]?.message}
                  </span>
                )}
            </div>
          </div>
          <div className="row stepper-form-element align-center m-0 p-0">
            <div className="col-md-3 col-sm-12 ">
              <label>Location</label>
            </div>
            <div className={`col-md-9 col-sm-12 mt-1 `}>
              {loginUserState?.data?.userType !==
                LOGIN_USER_TYPE.VENUE_ADMIN && (
                <div className={createPostingStyles.venueSearch}>
                  <span
                    title="Create a new Venue"
                    onClick={() => setIsVenueModalOpen(true)}
                  >
                    Can&apos;t find? Create a Venue
                  </span>
                </div>
              )}
              <Select
                className={`basic_multi_select selectField`}
                classNamePrefix="Select a Venue"
                placeholder="Select a Venue"
                isClearable={false}
                options={venueOptions}
                onInputChange={handleVenueChange}
                onChange={handleChange}
                defaultInputValue={postingData?.placeName}
                instanceId="venue"
                value={selectedOption}
              />

              {formState &&
                formState?.errors &&
                formState?.errors["venueId"]?.message && (
                  <span className="text-danger">
                    {formState?.errors["venueId"]?.message}
                  </span>
                )}
            </div>
          </div>
          <div className="row stepper-form-element align-center m-0 p-0 mt-3">
            <div className="col-md-3 col-sm-12 ">
              <label>Offer Amount</label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  type: "number",
                  className: "inputField",
                  id: "offerAmount",
                  placeholder: "$",
                  name: "offerAmount",
                }}
              />
            </div>
          </div>
          <div className="row stepper-form-element align-center m-0 p-0 mt-3">
            <div className="col-md-3 col-sm-12 ">
              <label>Promotion</label>
            </div>
            <div
              className={`col-md-9 col-sm-12 mt-1  ${createPostingStyles.promotionChkMain}`}
            >
              <CustomCheckbox
                {...{
                  handleOnChange: handlePromotion,
                  label:
                    "I want this event to be promoted publicly on JCasp.",
                  isBeforeLabel: true,
                  value: promotion,
                }}
              />
              {/* <div className="m-2 cursor-pointer" onClick={handlePromotion}>
                {promotion ? (
                  <img src="images/general/checked-checkbox.png" alt="" />
                ) : (
                  <img src="images/general/un-checked-checkbox.png" alt="" />
                )}
                <span className="ms-2">
                  I want this event to be promoted publicly on JCasp.
                </span>
              </div> */}
              {/* <span>I want this event to be promoted publicly on JCasp.</span> */}
            </div>
          </div>
        </div>
        <Button htmlType="submit" className="mt-4 float-right" type="primary">
          Next Step
        </Button>
      </form>

      {isVenueModalOpen && (
        <VenueSearchModal
          {...{
            isOpen: isVenueModalOpen,
            setIsVenueModalOpen,
            setVenueData,
          }}
        />
      )}
    </>
  );
};

export default GigDetails;
