import { yupResolver } from "@hookform/resolvers/yup";
import { CreatePostingsPayload } from "@redux/slices/postings";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Select from "react-select";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import {
  CreatePostingsMusicFormatInputs,
  CreatePostingsMusicFormatSchema,
} from "src/schemas/createPostingGigDetails";
import createPostingStyles from "./createPosting.module.scss";
// import { DatePicker, TimePicker } from "antd";
import { Button } from "@components/theme";
import { showToast } from "@components/ToastContainer";
import { RootState } from "@redux/reducers";
import { TagsState } from "@redux/slices/tags";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import moment from "moment";
import DatePicker, { DateObject } from "react-multi-date-picker";
import TimePicker from "react-multi-date-picker/plugins/time_picker";
import { useSelector } from "react-redux";

export interface MusicDetailsProps {
  postingData: CreatePostingsPayload;
  handleNext: (d: any) => void;
  handlePrev: (d: any) => void;
  setPostingData: (d: any) => void;
}

const MusicDetails = (props: MusicDetailsProps) => {
  const { postingData, setPostingData, handleNext, handlePrev } = props;

  const { handleSubmit, formState, setValue } =
    useForm<CreatePostingsMusicFormatInputs>({
      resolver: yupResolver(CreatePostingsMusicFormatSchema),
    });

  const [musicStyle, setMusicStyle] = useState<any>([]);
  const [artistElement, setArtistElement] = useState<any>([]);
  const [artistElementInput, setArtistElementInput] = useState("");

  const [date, setDate] = useState<any>(null);

  const today = new Date();
  dayjs.extend(customParseFormat);

  const {
    tags: { isLoading, data: tagsData },
  }: {
    tags: TagsState;
  } = useSelector((state: RootState) => ({
    tags: state.tags,
  }));

  const options = tagsData?.map((tag) => ({
    value: tag.label,
    label: tag.label,
  }));

  const defaultTags: any = postingData?.musicStyles?.map(
    (tag: any, i: any) => ({
      value: tag.value,
      label: tag.value,
    })
  );

  dayjs.extend(customParseFormat);

  const [startValues, setStartValues] = useState<any | string>(
    new DateObject().set({
      hour: 18,
      minute: 0o0,
      format: "PM",
    }) || ""
  );

  const [endValues, setEndValues] = useState<any | string>(
    new DateObject().set({
      hour: 20,
      minute: 0o0,
      format: "PM",
    })
  );

  let dates = moment(today);
  let tempDate = dates.format("DD/MM/YYYY");
  const [dateValue, setdateValue] = useState(dates.format("YYYY-MM-DD"));

  useEffect(() => {
    if (postingData) {
      if (postingData?.end) {
        const ampm = postingData?.end.split(" ").pop();
        let tem = 0;
        if (ampm === "PM") {
          tem = 12;
        }
        setEndValues(
          new DateObject().set({
            hour: Number(postingData?.end.split(":").shift()) + tem,
            minute: Number(
              postingData?.end.split(" ").shift().split(":").pop()
            ),
          })
        );
      }
      if (postingData?.start) {
        const ampm = postingData?.end.split(" ").pop();
        let tem = 0;
        if (ampm === "PM") {
          tem = 12;
        }
        setStartValues(
          new DateObject().set({
            hour: Number(postingData?.start.split(":").shift()) + tem,
            minute: Number(
              postingData?.start.split(" ").shift().split(":").pop()
            ),
          })
        );
      }
      setMusicStyle(postingData?.musicStyles || "");
      handleArtistElement(postingData?.musicStyles);
      setdateValue(postingData?.dates || "");
      setValue("dates", postingData?.dates || "");
      setValue("start", postingData?.start || "");
      setValue("end", postingData?.end || "");
      setValue("musicStyles", postingData?.musicStyles);
    }
  }, [postingData]);

  const handleArtistElement = (data: any) => {
    setArtistElement(data);
  };

  const handleNewElements = (event: any) => {
    if (event.key === "Enter") {
      if (artistElementInput) {
        let isAlready = false;
        artistElement.forEach((element: any) => {
          if (
            element.value.toLowerCase() === artistElementInput.toLowerCase()
          ) {
            isAlready = true;
          }
        });
        if (!isAlready) {
          const newOption = {
            value: artistElementInput.toLowerCase(),
            label: artistElementInput,
          };
          handleArtistElement([...artistElement, newOption]);
          setArtistElementInput("");
          event.preventDefault();
        } else {
          showToast(SUCCESS_MESSAGES.alreadyArtistElements, "warning");
        }
      }
    }
  };

  const onSubmit = (data: any) => {
    if (data) {
      handleNext(data);
    }
  };

  useEffect(() => {
    if (startValues) {
      setValue("start", startValues.format());
    }
  }, [startValues]);

  useEffect(() => {
    if (endValues) {
      setValue("end", endValues.format());
    }
  }, [endValues]);

  useEffect(() => {
    setValue("musicStyles", artistElement);
  }, [artistElement]);

  const handledateValues = (value: any) => {
    setValue("dates", value.format());
  };

  const handleStartTimeValue = (value: any) => {
    setStartValues(value);
    setValue("start", value.format());
  };

  const handleEndTimeValue = (value: any) => {
    setEndValues(value);
    setValue("end", value.format());
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="panel-form">
        <div className="stepper-form">
          <div
            className={`row stepper-form-element align-center m-0 p-0 mt-4 ${createPostingStyles.makecol}`}
          >
            <div className="col-md-3 col-sm-12 mb-3">
              <label>Artist Elements</label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <Select
                defaultValue={defaultTags || ""}
                isMulti
                className={`basic_multi_select selectField`}
                classNamePrefix="select"
                options={options}
                onChange={handleArtistElement}
                onKeyDown={handleNewElements}
                inputValue={artistElementInput}
                onInputChange={(val) => setArtistElementInput(val)}
                instanceId="artistElement"
              />

              {formState &&
                formState?.errors &&
                formState?.errors["musicStyles"]?.message && (
                  <span className="text-danger">
                    {formState?.errors["musicStyles"]?.message}
                  </span>
                )}
            </div>
          </div>
          <div
            className={`row  m-0 p-0 stepper-form-element mt-3 align-center ${createPostingStyles.makecol}`}
          >
            <div className="col-md-3 col-sm-12 mb-3">
              <label>Date & Time</label>
            </div>
            <div className="col-12 col-sm-3 w75 citySContry mt-1">
              <DatePicker
                placeholder="Select Date"
                format="YYYY-MM-DD"
                value={dateValue}
                minDate={new Date()}
                onChange={(value) => handledateValues(value)}
                // plugins={[<DatePanel />]}
              />
              {/* <DatePicker
                id="dates"
                className={createPostingStyles.datePicker}
                placeholder="Select Date"
                value={date && moment(date).isValid() ? moment(date) : null}
                onChange={(e) => handleChangeDate(e)}
                disabledDate={(currentDate) => {
                  return disabledDatePast(currentDate);
                }}
              /> */}

              {formState &&
                formState?.errors &&
                formState?.errors["dates"] &&
                formState?.errors["dates"]?.message && (
                  <span className="text-danger dateTime">
                    {formState?.errors["dates"]?.message}
                  </span>
                )}
            </div>

            <div className="col-12 col-sm-3 w75 timePiker citySContry mt-1">
              <DatePicker
                disableDayPicker
                id="start"
                format="hh:mm A"
                value={startValues}
                placeholder="Start time"
                onChange={(value) => handleStartTimeValue(value)}
                plugins={[<TimePicker key={"1"} hideSeconds mStep={15} />]}
              />
              {formState &&
                formState?.errors &&
                formState?.errors["start"] &&
                formState?.errors["start"]?.message && (
                  <span className="text-danger startTime">
                    {formState?.errors["start"]?.message}
                  </span>
                )}
            </div>

            <div className="col-12 col-sm-3 w75 timePiker d-flex citySContry mt-1">
              <DatePicker
                disableDayPicker
                id="end"
                format="hh:mm A"
                value={endValues}
                placeholder="End time"
                onChange={(value) => handleEndTimeValue(value)}
                plugins={[<TimePicker key={"1"} hideSeconds mStep={15} />]}
              />
              {formState &&
                formState?.errors &&
                formState?.errors["end"] &&
                formState?.errors["end"]?.message && (
                  <span className="text-danger startTime">
                    {formState?.errors["end"]?.message}
                  </span>
                )}
            </div>
          </div>
        </div>
        <Button
          htmlType="button"
          onClick={(d) => handlePrev(d)}
          className="mt-4"
          type="ghost"
        >
          &#8592; Previous Step
        </Button>
        <Button htmlType="submit" type="primary" className="float-right mt-4">
          Next Step
        </Button>
      </form>
    </>
  );
};

export default MusicDetails;
