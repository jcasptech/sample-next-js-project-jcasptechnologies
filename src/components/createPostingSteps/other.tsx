import { Button } from "@components/theme";
import CustomCheckbox from "@components/theme/checkbox";
import { TextAreaField } from "@components/theme/form/formFieldsComponent";
import MultipleChoiceCheckbox from "@components/theme/multiple-choice-checkbox";
import { yupResolver } from "@hookform/resolvers/yup";
import { CreatePostingsPayload } from "@redux/slices/postings";
import { Col, Row } from "antd";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import {
  CreatePostingsOtherDetailsInputs,
  CreatePostingsOtherDetailsSchema,
} from "src/schemas/createPostingOtherDetails";
import createPostingStyles from "./createPosting.module.scss";

export interface OtherDetailsProps {
  postingData: CreatePostingsPayload;
  handleNext: (d: any) => void;
  handlePrev: (d: any) => void;
  setPostingData: (d: any) => void;
  createPosting: (d: any) => void;
}

const OtherDetails = (props: OtherDetailsProps) => {
  const { postingData, handleNext, handlePrev, createPosting } = props;

  const [playSettings, setPlaySetting] = useState("");
  const [attandance, setAttandance] = useState("");
  const [payment, setPayment] = useState(false);
  const [isPaymentChecked, setIsPaymentchecked] = useState(false);
  const [isSoundEquipment, setIsSoundEquipment] = useState(
    postingData?.equipmentProvided
  );

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<CreatePostingsOtherDetailsInputs>({
      resolver: yupResolver(CreatePostingsOtherDetailsSchema),
    });

  useEffect(() => {
    if (postingData) {
      setPlaySetting(postingData?.playSettings || "");
      setAttandance(postingData?.expectedAttandance || "");
      setValue("playSettings", postingData?.playSettings || "");
      setValue("expectedAttandance", postingData?.expectedAttandance || "");
      setValue("note", postingData?.note || "");
    }
  }, [postingData]);

  const onSubmit = (data: any) => {
    if (payment) {
      setIsPaymentchecked(false);
      if (data) {
        data.equipmentProvided = isSoundEquipment;
        handleNext(data);
        createPosting(data);
      }
    } else {
      setIsPaymentchecked(true);
    }
  };

  useEffect(() => {
    setValue("playSettings", playSettings);
  }, [playSettings]);

  useEffect(() => {
    setValue("expectedAttandance", attandance);
  }, [attandance]);

  const handlePayment = () => {
    setPayment(!payment);
  };
  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="panel-form">
        <div className="stepper-form">
          <div className="row stepper-form-element align-center m-0 p-0 mb-3">
            <div className="col-12 col-sm-3">
              <label>Play Sitting</label>
            </div>
            <div className="col-12 col-sm-9">
              <div className="row">
                <div
                  className={`col-12 col-sm-6 col-lg-4 Tickchk iconChks ${createPostingStyles.iconChks}`}
                >
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${createPostingStyles.checkbox1} ${
                      playSettings === "indoors" ? "checked" : ""
                    }`}
                    onClick={() => {
                      setPlaySetting("indoors");
                    }}
                  >
                    <div
                      className={`checkbox find ${createPostingStyles.checkbox2}`}
                    >
                      <input
                        type="checkbox"
                        name="Soloist"
                        value="1"
                        id="Soloist"
                        className=""
                      />
                      <Image
                        className={createPostingStyles.image}
                        src="/images/createPost/indoor.svg"
                        alt="soloist.svg"
                        width={84}
                        height={84}
                      />
                      Indoors
                    </div>
                  </div>
                </div>
                <div
                  className={`col-12 col-sm-6 col-lg-4 Tickchk iconChks ${createPostingStyles.iconChks} `}
                >
                  <div
                    className={`input-group checkMe ${
                      createPostingStyles.checkbox
                    } ${createPostingStyles.checkbox1} ${
                      playSettings === "outdoors" ? "checked" : ""
                    }`}
                    onClick={() => {
                      setPlaySetting("outdoors");
                    }}
                  >
                    <div
                      className={`checkbox ${createPostingStyles.checkbox2}`}
                    >
                      <input
                        type="checkbox"
                        name="DJ"
                        value="1"
                        id="DJ"
                        className=""
                      />
                      <Image
                        className={createPostingStyles.image}
                        src="/images/createPost/outdoor.svg"
                        alt="dj.svg"
                        width={84}
                        height={84}
                      />
                      {/* <img
                        className={createPostingStyles.image}
                        src="/images/createPost/outdoor.svg"
                        alt="dj.svg"
                      /> */}
                      Outdoors
                    </div>
                  </div>
                </div>
              </div>
              {formState &&
                formState?.errors &&
                formState?.errors["playSettings"]?.message && (
                  <span className="text-danger">
                    {formState?.errors["playSettings"]?.message}
                  </span>
                )}
            </div>
          </div>

          <div className="row stepper-form-element align-center m-0 p-0 mb-3">
            <div className="col-12 col-sm-3">
              <label>Expected Attandance</label>
            </div>
            <div className="col-12 col-sm-9">
              <Row gutter={[20, 20]}>
                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: () => {
                        setAttandance("<50");
                      },
                      label: "<50",
                      isBeforeLabel: true,
                      value: attandance === "<50",
                    }}
                  />
                </Col>

                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: () => {
                        setAttandance("50-100");
                      },
                      label: "50-100",
                      isBeforeLabel: true,
                      value: attandance === "50-100",
                    }}
                  />
                </Col>

                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: () => {
                        setAttandance("100-200");
                      },
                      label: "100-200",
                      isBeforeLabel: true,
                      value: attandance === "100-200",
                    }}
                  />
                </Col>

                <Col lg={8} md={12} sm={12} xs={24}>
                  <MultipleChoiceCheckbox
                    {...{
                      handleOnChange: () => {
                        setAttandance("+200");
                      },
                      label: "+200",
                      isBeforeLabel: true,
                      value: attandance === "+200",
                    }}
                  />
                </Col>
              </Row>

              {formState &&
                formState?.errors &&
                formState?.errors["expectedAttandance"]?.message && (
                  <span className="text-danger">
                    {formState?.errors["expectedAttandance"]?.message}
                  </span>
                )}
            </div>
          </div>
          <div className="row stepper-form-element align-center m-0 p-0 mb-3">
            <div className="col-md-3 col-sm-12 ">
              <label>Sound Equipment</label>
            </div>
            <div
              className={`col-md-9 col-sm-12 mt-1 ${createPostingStyles.promotionChkMain}`}
            >
              <div
                className={`m-2 cursor-pointer ${createPostingStyles.termsChk}`}
              >
                <CustomCheckbox
                  {...{
                    handleOnChange: () => {
                      setIsSoundEquipment(!isSoundEquipment);
                    },
                    label: "The artist must provide their own sound equipment",
                    isBeforeLabel: true,
                    value: isSoundEquipment,
                  }}
                />

                {/* {isSoundEquipment ? (
                  <img src="images/general/checked-checkbox.png" alt="" />
                ) : (
                  <img src="images/general/un-checked-checkbox.png" alt="" />
                )}
                <span className={`ms-2 d-block ${createPostingStyles.terms}`}>
                  The artist must provide their own sound equipment
                </span> */}
              </div>
            </div>
          </div>
          <div className="row stepper-form-element align-center m-0 p-0 mb-3">
            <div className="col-12 col-sm-3 ">
              <label>Extra Note</label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <TextAreaField
                {...{
                  register,
                  formState,
                  className: `${createPostingStyles.textAreaField}`,
                  id: "note",
                  placeholder: "Provide Details of your event here...",
                  name: "note",
                }}
              />
            </div>
          </div>
          <div className="row stepper-form-element align-center m-0 p-0 mb-3">
            <div className="col-md-3 col-sm-12 ">
              <label>Payment</label>
            </div>
            <div
              className={`col-md-9 col-sm-12 mt-1 ${createPostingStyles.promotionChkMain}`}
            >
              <div
                className={`m-2 cursor-pointer ${createPostingStyles.termsChk}`}
              >
                <CustomCheckbox
                  {...{
                    handleOnChange: handlePayment,
                    label:
                      "I understand that the event payment will not be facilitated by JCasp",
                    isBeforeLabel: true,
                    value: payment,
                  }}
                />

                {/* {payment ? (
                  <img src="images/general/checked-checkbox.png" alt="" />
                ) : (
                  <img src="images/general/un-checked-checkbox.png" alt="" />
                )}
                <span className={`ms-2 d-block ${createPostingStyles.terms}`}>
                  I understand that the event payment will not be facilitated by
                  JCasp
                </span> */}
              </div>
            </div>
            <div className="col-lg-3 col-12"></div>
            <div className="col-md-9 col-sm-12 mt-1 ">
              {isPaymentChecked ? (
                <span className="text-danger"> Please agree. </span>
              ) : (
                " "
              )}
            </div>
          </div>
        </div>
        <Button
          htmlType="button"
          onClick={(d) => handlePrev(d)}
          className="mt-4"
          type="ghost"
        >
          &#8592; Previous Step
        </Button>
        <Button htmlType="submit" className="float-right mt-4" type="primary">
          Submit Posting
        </Button>
      </form>
    </>
  );
};

export default OtherDetails;
