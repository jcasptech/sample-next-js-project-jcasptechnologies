import { ActivePostingObject } from "@redux/slices/activePosting";
import moment from "moment";

export interface ConfirmPostingCardProps {
  postData: ActivePostingObject;
  setSelectEventId: (d: any) => void;
}

const ConfirmPostingCard = (props: ConfirmPostingCardProps) => {
  const { postData, setSelectEventId } = props;

  return (
    <>
      <div
        className="col-12 col-sm-6 postHold cursor-pointer"
        onClick={() => setSelectEventId(postData?.objectId)}
      >
        <div className="float-left w-100 pList">
          <div className="pbid">
            {/* <svg
              xmlns="http://www.w3.org/2000/svg"
              width="19"
              height="17"
              viewBox="0 0 19 17"
              fill="none"
            >
              <path
                d="M0.333984 14.2548C0.410166 13.8326 0.475062 13.4072 0.610496 12.9963C1.13107 11.4305 2.12896 10.2461 3.51575 9.37408C4.08018 9.02017 4.69193 8.7476 5.33283 8.56446C5.3775 8.55132 5.42171 8.53397 5.49836 8.5077C4.11815 7.58456 3.40665 6.32931 3.54162 4.6777C3.63002 3.5932 4.12238 2.68601 4.9618 1.98662C6.61334 0.609412 9.06997 0.764206 10.4897 2.33092C11.3361 3.26907 11.7076 4.37233 11.5242 5.62336C11.3413 6.86359 10.6745 7.81112 9.61359 8.47956C9.65732 8.55555 9.72316 8.55414 9.77724 8.57056C11.3437 9.04432 12.6303 9.92008 13.5915 11.2443C14.3115 12.2271 14.7209 13.402 14.7671 14.6183C14.7845 15.0128 14.7798 15.4083 14.7719 15.8028C14.7648 16.1311 14.5057 16.3267 14.2188 16.2338C14.0415 16.1761 13.9367 16.0176 13.931 15.7821C13.9263 15.5007 13.931 15.2192 13.931 14.9378C13.9188 13.1623 13.1965 11.7232 11.8417 10.5927C10.8297 9.7484 9.6545 9.26948 8.34577 9.12829C6.58137 8.93831 4.95474 9.32201 3.52046 10.3788C2.17834 11.3681 1.36949 12.6923 1.18703 14.3622C1.14001 14.7708 1.17199 15.1869 1.16493 15.5996C1.16352 15.6952 1.15692 15.7905 1.14518 15.8853C1.14007 15.9698 1.10724 16.0503 1.05175 16.1144C0.996262 16.1785 0.921191 16.2226 0.838101 16.2399C0.65423 16.2868 0.508921 16.2249 0.39653 16.0781C0.37866 16.0546 0.356557 16.034 0.336806 16.012L0.333984 14.2548ZM7.54727 8.22485C7.96492 8.22435 8.37839 8.14181 8.76406 7.98193C9.14973 7.82205 9.50005 7.58796 9.79502 7.29303C10.09 6.9981 10.3238 6.64811 10.4832 6.26303C10.6426 5.87796 10.7244 5.46534 10.7239 5.04874C10.7234 4.63214 10.6406 4.21972 10.4803 3.83502C10.3201 3.45032 10.0854 3.10088 9.78971 2.80664C9.49403 2.51241 9.14315 2.27915 8.75711 2.12018C8.37106 1.96121 7.9574 1.87964 7.53975 1.88014C5.78945 1.88014 4.36692 3.30659 4.36833 5.06047C4.36595 5.47723 4.4466 5.89032 4.6056 6.27575C4.76459 6.66117 4.99877 7.01124 5.29452 7.30564C5.59028 7.60004 5.94172 7.8329 6.32844 7.9907C6.71516 8.14849 7.12946 8.22809 7.54727 8.22485Z"
                fill="black"
                stroke="black"
                strokeWidth="0.190126"
              />
              <path
                d="M16.3657 5.7006C16.3915 6.77947 15.997 7.66649 15.1632 8.35885C15.1101 8.40294 15.009 8.42686 15.0221 8.50285C15.0353 8.57884 15.1364 8.5765 15.2008 8.60089C16.7245 9.17926 17.8305 10.1934 18.4456 11.7127C18.6688 12.2632 18.7807 12.8522 18.7748 13.446C18.7724 13.7091 18.7785 13.9727 18.7724 14.2354C18.7673 14.4728 18.6069 14.6421 18.3892 14.6576C18.1714 14.6731 17.9909 14.5305 17.9537 14.2931C17.9039 13.9727 17.941 13.6481 17.9288 13.3259C17.8893 12.2756 17.4943 11.3717 16.7767 10.6137C15.8465 9.63145 14.6901 9.1361 13.3386 9.08779C13.2488 9.08445 13.1594 9.0733 13.0715 9.05448C12.9794 9.03627 12.8964 8.98696 12.8364 8.91486C12.7764 8.84276 12.7432 8.75226 12.7423 8.65858C12.7399 8.56175 12.7726 8.46731 12.8345 8.39265C12.8964 8.318 12.9832 8.26818 13.079 8.25237C13.2615 8.21953 13.4491 8.21484 13.6306 8.17966C14.6708 7.97655 15.4388 7.09281 15.523 6.03974C15.6353 4.63251 14.51 3.52034 13.2657 3.48985C13.2059 3.48645 13.1466 3.47766 13.0884 3.46358C12.8585 3.41339 12.7212 3.22904 12.7451 3.01139C12.7682 2.80406 12.9384 2.65348 13.1684 2.64879C13.6988 2.64066 14.222 2.77203 14.6854 3.02968C15.6795 3.56537 16.3332 4.60437 16.3657 5.7006Z"
                fill="black"
                stroke="black"
                strokeWidth="0.190126"
              />
            </svg> */}
            &nbsp;{postData?.selectedArtist?.name || "Artist Name"}
          </div>
          <div className="title text-ellipsis">{postData?.title || ""}</div>
          <div className="desc texts-ellipsis">{postData.note || ""}</div>
          <div className="meta ">
            <span className="text-ellipsis">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="33"
                height="33"
                viewBox="0 0 33 33"
                fill="none"
              >
                <rect
                  x="0.012207"
                  width="32.3214"
                  height="32.3214"
                  rx="9.5063"
                  fill="#F5F5F5"
                />
                <rect
                  x="0.012207"
                  width="32.3214"
                  height="32.3214"
                  rx="9.5063"
                  fill="#F5F5F5"
                />
                <path
                  d="M16.3199 25.6705C15.9191 25.5576 15.6231 25.2859 15.324 25.0165C14.2895 24.0874 13.2383 23.1765 12.2781 22.168C11.2603 21.0996 10.3406 19.9536 9.6433 18.6455C9.07751 17.5845 8.68173 16.4682 8.59022 15.2544C8.28503 11.1987 11.1381 7.50335 15.1623 6.7968C17.8566 6.32391 20.2387 7.04579 22.2231 8.94013C23.5889 10.2436 24.3688 11.8597 24.6085 13.7396C24.8031 15.2651 24.5086 16.6991 23.8694 18.0792C23.2261 19.4686 22.3179 20.6736 21.2959 21.8001C20.1597 23.0543 18.8711 24.1445 17.6197 25.2752C17.4131 25.4525 17.173 25.5865 16.9136 25.6691L16.3199 25.6705ZM9.72366 14.7151C9.71809 15.7101 9.97776 16.6443 10.3851 17.5418C11.0058 18.9089 11.9125 20.0776 12.9252 21.1702C14.0061 22.3371 15.2148 23.3688 16.3989 24.426C16.5638 24.5733 16.6702 24.5724 16.8365 24.4233C18.0057 23.3771 19.1995 22.357 20.274 21.2078C21.0855 20.3382 21.8264 19.4138 22.4196 18.3788C23.2488 16.9314 23.698 15.4031 23.436 13.7145C23.1624 11.9517 22.3439 10.4954 20.9675 9.37213C19.6204 8.27121 18.0596 7.76209 16.3111 7.83223C15.5011 7.86288 14.7032 8.0396 13.956 8.35389C12.702 8.87885 11.6312 9.76282 10.8782 10.8947C10.1253 12.0265 9.72374 13.3557 9.72413 14.7151H9.72366Z"
                  fill="url(#paint0_radial_707_1974)"
                />
                <path
                  d="M16.6006 17.9585C14.717 17.9487 13.0558 16.4539 13.0591 14.3942C13.0623 12.282 14.8094 10.8354 16.6318 10.8438C18.4467 10.8517 20.1789 12.3019 20.1738 14.4072C20.1682 16.4808 18.4848 17.9659 16.6006 17.9585ZM16.6081 16.7972C17.2427 16.802 17.8532 16.5546 18.3055 16.1094C18.7578 15.6642 19.0147 15.0576 19.0199 14.423C19.0334 13.1906 18.0755 11.9986 16.6155 11.9991C15.1555 11.9995 14.2134 13.1766 14.2139 14.3877C14.2111 14.7035 14.2711 15.0168 14.3902 15.3093C14.5094 15.6018 14.6854 15.8678 14.9081 16.0918C15.1308 16.3159 15.3957 16.4935 15.6875 16.6144C15.9793 16.7353 16.2922 16.7971 16.6081 16.7962V16.7972Z"
                  fill="url(#paint1_radial_707_1974)"
                />
                <defs>
                  <radialGradient
                    id="paint0_radial_707_1974"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(13.249 10.1774) rotate(77.7326) scale(15.8551 13.5949)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint1_radial_707_1974"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(15.1277 12.1613) rotate(75.5978) scale(5.9853 5.95491)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                </defs>
              </svg>
              &nbsp;{postData?.address || ""}
            </span>
            <span className="cal-o">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="33"
                height="33"
                viewBox="0 0 33 33"
                fill="none"
              >
                <rect
                  x="0.0273438"
                  width="32.3214"
                  height="32.3214"
                  rx="9.5063"
                  fill="#F5F5F5"
                />
                <path
                  d="M7.71973 13.6953H24.6644"
                  stroke="url(#paint0_radial_707_1982)"
                  strokeWidth="1.14076"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M20.4102 17.4141H20.4195"
                  stroke="url(#paint1_radial_707_1982)"
                  strokeWidth="1.90126"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M16.1924 17.4141H16.2018"
                  stroke="url(#paint2_radial_707_1982)"
                  strokeWidth="1.90126"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M11.9648 17.4141H11.9742"
                  stroke="url(#paint3_radial_707_1982)"
                  strokeWidth="1.90126"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M20.4102 21.1016H20.4195"
                  stroke="url(#paint4_radial_707_1982)"
                  strokeWidth="1.90126"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M16.1924 21.1016H16.2018"
                  stroke="url(#paint5_radial_707_1982)"
                  strokeWidth="1.90126"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M11.9648 21.1016H11.9742"
                  stroke="url(#paint6_radial_707_1982)"
                  strokeWidth="1.90126"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M20.0332 6.65625V9.78489"
                  stroke="url(#paint7_radial_707_1982)"
                  strokeWidth="1.14076"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M12.3535 6.65625V9.78489"
                  stroke="url(#paint8_radial_707_1982)"
                  strokeWidth="1.14076"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M20.217 8.15625H12.1674C9.37561 8.15625 7.63184 9.71147 7.63184 12.5702V21.1734C7.63184 24.0771 9.37561 25.6682 12.1674 25.6682H20.2082C23.0088 25.6682 24.7438 24.104 24.7438 21.2453V12.5702C24.7526 9.71147 23.0176 8.15625 20.217 8.15625Z"
                  stroke="url(#paint9_radial_707_1982)"
                  strokeWidth="1.14076"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <defs>
                  <radialGradient
                    id="paint0_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(12.6465 13.8805) rotate(12.9426) scale(3.63798 3.27957)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint1_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(20.4129 17.5992) rotate(89.8618) scale(0.814817 0.00811523)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint2_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(16.1951 17.5992) rotate(89.8618) scale(0.814817 0.00811523)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint3_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(11.9676 17.5992) rotate(89.8618) scale(0.814817 0.00811523)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint4_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(20.4129 21.2867) rotate(89.8618) scale(0.814817 0.00811523)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint5_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(16.1951 21.2867) rotate(89.8618) scale(0.814817 0.00811523)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint6_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(11.9676 21.2867) rotate(89.8618) scale(0.814817 0.00811523)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint7_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(20.324 7.23563) rotate(85.3077) scale(2.55783 0.861247)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint8_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(12.6443 7.23563) rotate(85.3077) scale(2.55783 0.861247)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint9_radial_707_1982"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(12.6073 11.3992) rotate(75.9134) scale(14.7114 14.3425)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                </defs>
              </svg>
              &nbsp;{moment(postData?.startDate?.iso).format("MMM Do YYYY")}
            </span>
            <span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="33"
                height="33"
                viewBox="0 0 33 33"
                fill="none"
              >
                <rect
                  x="0.0898438"
                  width="32.3214"
                  height="32.3214"
                  rx="9.5063"
                  fill="#F5F5F5"
                />
                <path
                  d="M15.7306 25.6689H15.025C14.4977 25.5545 14.0889 25.2567 13.7131 24.8783C11.6596 22.8126 9.59987 20.7533 7.53398 18.7003C7.15596 18.3245 6.85777 17.9156 6.74414 17.388V16.6453C6.84979 16.2106 7.07278 15.8133 7.38879 15.4967C10.2605 12.6305 13.1293 9.76226 15.9953 6.89205C16.0881 6.79202 16.2001 6.71181 16.3247 6.65625H24.0857C24.3197 6.73312 24.5569 6.79773 24.7712 6.92844C25.303 7.25299 25.6082 7.73536 25.7567 8.32728V16.0883C25.6996 16.2368 25.5919 16.3482 25.482 16.4567C22.6618 19.2766 19.8423 22.0977 17.0236 24.9199C16.6552 25.2893 16.2471 25.5667 15.7306 25.6689ZM20.2795 7.78475C19.2215 7.78475 18.1628 7.79181 17.1056 7.77918C16.8516 7.77621 16.6712 7.84714 16.4892 8.03058C14.663 9.8702 12.831 11.7046 10.9934 13.5338C10.0702 14.457 9.14696 15.3799 8.22356 16.3025C8.08096 16.4448 7.95991 16.597 7.90978 16.7968C7.81546 17.173 7.94691 17.4741 8.21056 17.7367C9.23521 18.7598 10.259 19.7835 11.2819 20.8076C12.4019 21.9276 13.5217 23.0477 14.6414 24.1679C14.7922 24.319 14.9522 24.452 15.1661 24.5021C15.5482 24.5901 15.849 24.4531 16.1182 24.1835C18.903 21.3935 21.6902 18.6066 24.4797 15.8228C24.5306 15.7745 24.5705 15.7158 24.5966 15.6507C24.6227 15.5856 24.6344 15.5156 24.6308 15.4455C24.6271 13.2491 24.6261 11.0527 24.6279 8.85607C24.6279 8.15758 24.2565 7.78549 23.5647 7.78512C22.4698 7.78413 21.3747 7.78401 20.2795 7.78475Z"
                  fill="url(#paint0_radial_707_1966)"
                />
                <path
                  d="M20.8453 9.79694C21.0778 9.79501 21.3083 9.83931 21.5235 9.92726C21.7387 10.0152 21.9343 10.145 22.0988 10.3092C22.2634 10.4734 22.3937 10.6687 22.4822 10.8837C22.5706 11.0987 22.6154 11.3291 22.614 11.5615C22.614 12.54 21.8194 13.3414 20.8491 13.3403C19.8788 13.3391 19.0763 12.5363 19.0703 11.5656C19.0644 10.5949 19.8684 9.79731 20.8453 9.79694ZM20.8453 12.1735C20.9257 12.1743 21.0054 12.1589 21.0796 12.1282C21.1539 12.0975 21.2212 12.0522 21.2776 11.995C21.334 11.9377 21.3783 11.8697 21.4078 11.795C21.4373 11.7203 21.4515 11.6404 21.4495 11.5601C21.4515 11.481 21.4375 11.4023 21.4085 11.3287C21.3795 11.2551 21.3359 11.1881 21.2805 11.1316C21.2251 11.0751 21.159 11.0303 21.0859 10.9999C21.0129 10.9694 20.9345 10.954 20.8554 10.9544C20.7753 10.9536 20.6959 10.9687 20.6216 10.9986C20.5473 11.0286 20.4797 11.0728 20.4225 11.1289C20.3654 11.185 20.3198 11.2518 20.2885 11.3255C20.2572 11.3992 20.2406 11.4783 20.2399 11.5584C20.2391 11.6385 20.2541 11.7179 20.2841 11.7922C20.314 11.8664 20.3583 11.9341 20.4144 11.9912C20.4705 12.0484 20.5373 12.0939 20.611 12.1253C20.6847 12.1566 20.7638 12.1731 20.8439 12.1739L20.8453 12.1735Z"
                  fill="url(#paint1_radial_707_1966)"
                />
                <defs>
                  <radialGradient
                    id="paint0_radial_707_1966"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(12.2722 10.1771) rotate(75.5977) scale(15.9944 15.9133)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                  <radialGradient
                    id="paint1_radial_707_1966"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientUnits="userSpaceOnUse"
                    gradientTransform="translate(20.1007 10.4531) rotate(75.5962) scale(2.9809 2.96606)"
                  >
                    <stop stop-color="#1D1D1D" />
                    <stop offset="1" />
                  </radialGradient>
                </defs>
              </svg>
              &nbsp;${postData?.totalCost || 0}
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default ConfirmPostingCard;
