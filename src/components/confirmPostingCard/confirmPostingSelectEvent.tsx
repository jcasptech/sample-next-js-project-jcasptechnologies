import { useEffect, useState } from "react";
import { DefaultLoader } from "@components/theme";
import {
  getPostingFullDetailAPI,
  submitGigAPI,
} from "@redux/services/calendar.api";
import moment from "moment";

export interface ConfirmPostingSelectEventProps {
  selectEventId: string;
}
const ConfirmPostingSelectEvent = (props: ConfirmPostingSelectEventProps) => {
  const { selectEventId } = props;
  const [postData, setPostData] = useState<any>();
  const [isfullLoading, setIsFullLoading] = useState(false);

  useEffect(() => {
    const getFullDetail = async () => {
      setIsFullLoading(true);
      if (selectEventId) {
        try {
          const data = await getPostingFullDetailAPI(selectEventId);
          setPostData(data);
          setIsFullLoading(false);
        } catch (err: any) {
          console.log(err);
          setIsFullLoading(false);
        }
      }
    };
    getFullDetail();

    setTimeout(() => {
      const divElement = document.getElementById("postingDetails");
      if (window.innerWidth < 1024) {
        if (divElement) {
          divElement.scrollIntoView({ behavior: "smooth" });
        }
      }
    }, 1000);
  }, [selectEventId]);

  const handleAttedence = (data: any) => {
    let people = "<50";

    if (data === "min") {
      people = "<50";
    } else if (data === "small") {
      people = "50-100";
    } else if (data === "medium") {
      people = "100-200";
    } else if (data === "max") {
      people = ">200";
    }
    return people;
  };

  return (
    <>
      {isfullLoading && <DefaultLoader />}
      {!isfullLoading && (
        <div className="col-12 postHold phDet">
          <div className="float-left w-100 pList">
            <div className="pbid">
              {/* <img src="/images/rbidc.svg" alt="B" /> */}
              &nbsp;{postData?.selectedArtist?.name || "Artist Name"}
            </div>
            {/* <div className="vwbid">
              <a href="#" onClick={() => setIsBidsModalOpen(true)}>
                View Bids &#8594;
              </a>
            </div> */}
            <div className="title">{postData?.title || "Title"}</div>
            <div className="desc notePostEvent">{postData?.note || ""}</div>
            <div className="row metaH">
              <div className="col-12 evedt">
                <span className="float-left w-100 tt">Location:</span>
                <span className="float-left w-100 val">
                  {postData?.address || ""}
                </span>
              </div>
              <div className="col-12 evedt">
                <span className="float-left w-100 tt">Date & Time:</span>
                <span className="float-left w-100 val">
                  {moment(postData?.startDate?.iso)
                    .utc()
                    .format("ddd, MMM DD, YYYY")}{" "}
                  from {moment(postData?.startDate?.iso).utc().format("h:mm a")}{" "}
                  to {moment(postData?.endDate?.iso).utc().format("h:mm a")}{" "}
                  {moment(postData?.startDate?.iso).format("z")}
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">Budget:</span>
                <span className="float-left w-100 val">
                  ${postData?.totalCost || ""}
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">Type of event:</span>
                <span className="float-left w-100 val capitalize">
                  {postData?.variety || ""}
                </span>
              </div>
              <div className="col-12 col-sm-12 evedt">
                <span className="float-left w-100 tt">
                  Type of artist/music wanted:
                </span>
                <span className="float-left w-100 val capitalize">
                  {postData?.artistFormats?.join(", ")}
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">
                  Expected attendance:
                </span>
                <span className="float-left w-100 val">
                  {handleAttedence(postData?.attendanceCategory)} people
                </span>
              </div>
              <div className="col-12 col-sm-6 evedt">
                <span className="float-left w-100 tt">Indoor/Outdoor:</span>
                <span className="float-left w-100 val capitalize">
                  {postData?.playSetting || ""}
                </span>
              </div>
              <div className="col-12 evedt">
                <span className="float-left w-100 tt">
                  Should the artist bring their own PA system?:
                </span>
                <span className="float-left w-100 val">
                  {postData?.equipmentProvided ? "No" : "Yes"}
                </span>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ConfirmPostingSelectEvent;
