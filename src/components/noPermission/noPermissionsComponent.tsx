import Image from "next/image";
import React from "react";

export interface NoPermissionsComponentProps {
  message?: string;
}

const NoPermissionsComponent = (props: NoPermissionsComponentProps) => {
  let { message } = props;
  if (!message) {
    message = "You don't have permission to access requested page.";
  }
  return (
    <div className={`main-section admin-panel-section`}>
      <section className="section text-center h-100vh">
        <div>
          <h1 className="text-primary text-6xl">403</h1>
          <h3 className="text-primary text-xl">Access Denied</h3>
        </div>
        <div>
          <Image
            src="/images/lost.svg"
            width={250}
            height={250}
            className="m-auto mb-4"
            alt="access denied"
          />
        </div>
        <h4 className="text-grey-500 text-xl">{message}</h4>
        <p className="text-grey-500 text-lg">
          Please contact to your account administrator{" "}
        </p>
      </section>
    </div>
  );
};

export default NoPermissionsComponent;
