import { FullStarGrayIcon } from "@components/theme/icons/fullStarGrayIcon";
import { FullStartIcon } from "@components/theme/icons/fullStarIcon";
import { HalfStarIcon } from "@components/theme/icons/HalfStarIcon";
import { RootState } from "@redux/reducers";
import { ArtistObject } from "@redux/slices/artists";
import {
  ArtistRatingState,
  fetchAritstRating,
} from "@redux/slices/artists-rating";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import useList from "src/libs/useList";
import artistStyles from "./artistRating.module.scss";
import AOS from "aos";
import "aos/dist/aos.css";
import { QuoteIcon } from "@components/theme/icons/quoteIcon";
import { capitalizeString } from "src/libs/helpers";
import Image from "next/image";

export interface ArtistRatingProps {
  redirectPage: "/signup" | "/login";
}

const ArtistRating = (props: ArtistRatingProps) => {
  const { redirectPage } = props;
  const [selectedArtist, setSelectedArtist] = useState<any>(null);

  const dispatch = useDispatch();
  const {
    artistRating: { isLoading, data: artistRatingData },
  }: {
    artistRating: ArtistRatingState;
  } = useSelector((state: RootState) => ({
    artistRating: state.artistRating,
  }));

  const { apiParam } = useList({
    queryParams: {
      include: ["metadata"],
    },
  });

  useEffect(() => {
    dispatch(fetchAritstRating(apiParam));
  }, []);

  useEffect(() => {
    if (artistRatingData?.length > 0) {
      setSelectedArtist(artistRatingData[0]);
    }
  }, [artistRatingData]);

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <>
      <div className={`col-12 col-sm-6 ${artistStyles.testi_box}`}>
        <div className={`row m0 ${artistStyles.main_box}`}>
          <div
            className={`col-12 ${artistStyles.testi_box__maintesti}`}
            data-aos="fade-down"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <Image
              src={selectedArtist?.iconImage?.url || "/images/signtesti.png"}
              alt={selectedArtist?.name}
              className="testimonial_shape"
              width={390}
              height={392}
            />
            {/* <img
              src={selectedArtist?.iconImage?.url || "/images/signtesti.png"}
              alt={selectedArtist?.name}
              className="testimonial_shape"
            /> */}
          </div>
          <div
            className={`col-12 ${artistStyles.testi_box__maintesti_name}`}
            data-aos="fade-up"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <span>{capitalizeString(selectedArtist?.name)}</span>
          </div>
          <div
            className={`col-12 ${artistStyles.testi_box__maintesti_text}`}
            data-aos="fade-up"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <p className={artistStyles.about}>
              {selectedArtist?.metadata?.about &&
              selectedArtist?.metadata?.about?.length > 120
                ? selectedArtist?.metadata?.about.slice(0, 220) + "..."
                : selectedArtist?.metadata?.about}
            </p>
          </div>
          <div
            className={`col-12 ${artistStyles.testi_box__maintesti_reviews}`}
            data-aos="fade-down"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            <div className={artistStyles.overlapTh}>
              <div className={artistStyles.ovethums}>
                <ul>
                  {artistRatingData &&
                    artistRatingData?.length > 0 &&
                    artistRatingData.map(
                      (single: ArtistObject, index: number) => {
                        return (
                          <li
                            key={index}
                            className={`artist-small-icon ${artistStyles.smallIcons}`}
                            style={{
                              backgroundImage: `url(${
                                single?.iconImage?.url || "/images/t1.png"
                              })`,
                            }}
                            onClick={(e) => {
                              e.preventDefault();
                              setSelectedArtist(single);
                            }}
                          ></li>
                        );
                      }
                    )}
                </ul>
              </div>
              <div className={artistStyles.rating}>
                <ul>
                  {[1, 2, 3, 4, 5].map((star) => (
                    <li key={`star-${star}`}>
                      {star <= selectedArtist?.rating ? (
                        <FullStartIcon />
                      ) : star > selectedArtist?.rating &&
                        selectedArtist?.rating > star - 1 ? (
                        <HalfStarIcon />
                      ) : (
                        <FullStarGrayIcon />
                      )}
                    </li>
                  ))}
                </ul>
                <span className={artistStyles.ratingDetails}>
                  {selectedArtist?.rating || 0}
                </span>
                <p>From {selectedArtist?.ratingCount || 0} Artist Reviews</p>
              </div>
            </div>
          </div>
          <div
            className={`col-12 ${artistStyles.testi_box__maintesti_cta}`}
            data-aos="fade-up"
            data-aos-delay="100"
            data-aos-duration="1200"
          >
            {redirectPage && (
              <p>
                {redirectPage === "/login"
                  ? "Already have an account?"
                  : "Don't have an account?"}

                <Link
                  href={redirectPage}
                  className={`${artistStyles.artist_cta} hvr-float-shadow`}
                >
                  User {redirectPage === "/login" ? "Login" : "Signup"}
                </Link>
              </p>
            )}
          </div>
        </div>
      </div>
    </>
  );
};
export default ArtistRating;
