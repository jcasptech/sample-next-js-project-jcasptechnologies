import Spinner from "@components/theme/spinner";
import React, { useState, useEffect } from "react";
import { UpdateArtistPayload } from "@redux/slices/artists";
import Image from "next/image";
import { Button } from "@components/theme";

export interface ArtistProfileSteperSevenProps {
  profileData: UpdateArtistPayload;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
  handlePostData: (d: any) => void;
  isUpdateLoading: boolean;
}

const ArtistTheme = (props: ArtistProfileSteperSevenProps) => {
  const {
    profileData,
    handlePrev,
    handleNext,
    handlePostData,
    isUpdateLoading,
  } = props;

  const [updateTheme, setUpdateTheme] = useState<"dark" | "light">("dark");

  useEffect(() => {
    if (profileData?.profileTheme) {
      setUpdateTheme(profileData?.profileTheme);
    }
  }, [profileData]);

  const handleChangetheme = (e: any) => {
    e.preventDefault();
    if (updateTheme !== null) {
      handleNext({ profileTheme: updateTheme });
      handlePostData(updateTheme);
    }
  };

  return (
    <>
      <form className="panel-form">
        <div className="stepper-form">
          <div className="row align-center field-row">
            <div className="col-12 col-sm-6 profThem text-center">
              <Image
                src="/images/dark-mode.webp"
                alt="Dark Mode"
                width={415}
                height={297}
              />
              <div className="input-group chkbx">
                <div className="checkbox checkbox-info">
                  <input type="hidden" name="darkM" value="1" />
                  <input
                    type="checkbox"
                    id="darkM"
                    className="valid"
                    aria-required="true"
                    aria-invalid="false"
                    checked={updateTheme === "dark"}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setUpdateTheme("dark");
                      }
                    }}
                  />
                  <label htmlFor="darkM" onClick={() => setUpdateTheme("dark")}>
                    Dark Mode
                  </label>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-6 profThem text-center">
              <Image
                src="/images/light-mode.webp"
                alt="Light Mode"
                width={415}
                height={297}
              />
              <div className="input-group chkbx">
                <div className="checkbox checkbox-info">
                  <input type="hidden" name="lightM" value="1" />
                  <input
                    type="checkbox"
                    id="lightM"
                    className="valid"
                    aria-required="true"
                    aria-invalid="false"
                    checked={updateTheme === "light"}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setUpdateTheme("light");
                      }
                    }}
                  />
                  <label
                    htmlFor="lightM"
                    onClick={() => setUpdateTheme("light")}
                  >
                    Light Mode
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Button
          htmlType="button"
          type="ghost"
          onClick={(d) => handlePrev(d)}
          className={`mt-5`}
          disabled={isUpdateLoading}
        >
          &#8592; Previous Step
        </Button>
        <Button
          htmlType="button"
          type="primary"
          onClick={handleChangetheme}
          className={`float-right mt-5`}
          disabled={isUpdateLoading}
          loading={isUpdateLoading}
        >
          Finish
        </Button>
      </form>
    </>
  );
};
export default ArtistTheme;
