import { useEffect } from "react";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  ArtistProfileFormVideoSchema,
  ArtistProfileVideoInputs,
} from "src/schemas/artistProfileFormVideoSchema";
import { UpdateArtistPayload } from "@redux/slices/artists";
import styles from "./artistProfileSteps.module.scss";
import { getVideoUrl } from "src/libs/helpers";
import Image from "next/image";
import { Button } from "@components/theme";

export interface ArtistVideoLinksProps {
  profileData: UpdateArtistPayload;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
  handleRemoveVideoForm: (d: any) => void;
  handleAddVideolinks: (d: any) => void;
  handleInputChange: (index: number, event: any) => void;
}

const ArtistVideoLinks = (props: ArtistVideoLinksProps) => {
  const {
    profileData,
    handleNext,
    handlePrev,
    handleAddVideolinks,
    handleRemoveVideoForm,
    handleInputChange,
  } = props;

  const { register, handleSubmit, formState, setFocus, setValue, reset } =
    useForm<ArtistProfileVideoInputs>({
      resolver: yupResolver(ArtistProfileFormVideoSchema),
    });

  useEffect(() => {
    if (
      profileData?.youtubeVideoLinks &&
      profileData?.youtubeVideoLinks.length > 0
    ) {
      setValue(`videos` as any, undefined);
      profileData.youtubeVideoLinks.map((links: any, index: any) => {
        setValue(`videos[${index}].youtubeVideoLinks` as any, links.link || "");
      });
    }
  }, [profileData]);

  const onSubmit = (data: any) => {
    const getUrl = data?.videos?.map((video: any) => {
      return { link: video?.youtubeVideoLinks };
    });
    if (getUrl) {
      handleNext({ youtubeVideoLinks: getUrl });
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="panel-form">
        <div className="stepper-form">
          {profileData?.youtubeVideoLinks?.map((list: any, index) => (
            <span key={`ArtistVideoLinks_${index}`}>
              <div
                className={`row stepper-form-element ${
                  index !== 0 ? "song_multi_add" : ""
                } align-center mt-3 `}
              >
                <div className="col-md-3 col-sm-12">
                  <label>Youtube/Vimeo Video Links</label>
                </div>
                <div className="col-md-9 col-sm-12">
                  <InputField
                    {...{
                      register,
                      formState,
                      className: "inputField",
                      defaultValue: list.link || "",
                      name: `videos[${index}].youtubeVideoLinks`,
                      id: `videos[${index}].youtubeVideoLinks`,
                      placeholder: "Enter youtube video links",
                    }}
                    onChange={(event) => handleInputChange(index, event)}
                  />
                  {formState &&
                    formState?.errors &&
                    formState?.errors["videos"] &&
                    formState?.errors["videos"][index]?.youtubeVideoLinks
                      ?.message && (
                      <span className="text-danger">
                        {
                          formState?.errors["videos"][index]?.youtubeVideoLinks
                            ?.message
                        }
                      </span>
                    )}
                </div>
              </div>
              {profileData.youtubeVideoLinks[index].link ? (
                <div className={`col-12 ${styles.videoBox}`}>
                  <iframe
                    width="auto"
                    height="auto"
                    className="frameVideo"
                    src={`${getVideoUrl(
                      profileData.youtubeVideoLinks[index].link
                    )}`}
                    title="YouTube video player"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowFullScreen
                  ></iframe>
                </div>
              ) : (
                ""
              )}

              <div className="row align-center mt-2 field-row">
                <div className="col-12 col-sm-3">&nbsp;</div>

                <div className="col-12 col-sm-9 clone_acts">
                  {index === 0 && (
                    <a
                      title="Add"
                      onClick={(e) => {
                        e.preventDefault();
                        handleAddVideolinks(e);
                      }}
                      className="clone adcln"
                    >
                      <Image
                        src="/images/add_cl.svg"
                        alt="Add"
                        width={24}
                        height={24}
                        quality={100}
                      />
                      &nbsp;Add Video
                    </a>
                  )}
                  {index !== 0 && (
                    <a
                      title="Remove"
                      onClick={(e) => {
                        e.preventDefault();
                        handleRemoveVideoForm(index);
                      }}
                      className="delete dltcln"
                    >
                      <Image
                        src="/images/rem_cl.svg"
                        alt="Remove"
                        width={24}
                        height={24}
                        quality={100}
                      />
                      &nbsp;Remove Video
                    </a>
                  )}
                </div>
              </div>
            </span>
          ))}
        </div>
        <Button
          htmlType="button"
          type="ghost"
          onClick={(d) => handlePrev(d)}
          className="mt-4"
        >
          &#8592; Previous Step
        </Button>
        <Button htmlType="submit" type="primary" className="float-right mt-4">
          Next Step &#8594;
        </Button>
      </form>
    </>
  );
};
export default ArtistVideoLinks;
