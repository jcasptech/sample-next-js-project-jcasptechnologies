import { Button, DefaultSkeleton } from "@components/theme";
import { FileDropField } from "@components/theme/form/formFieldsComponent";
import { DeleteIcon } from "@components/theme/icons/deleteIcon";
import { showToast } from "@components/ToastContainer";
import { deleteFileAPI, postFileAPI } from "@redux/services/artist.api";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { Popconfirm } from "antd";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { acceptedCertificateImage, SUCCESS_MESSAGES } from "src/libs/constants";
import artistProfileStyle from "./artistProfileSteps.module.scss";

export interface PhotosProps {
  profileData: UpdateArtistPayload;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
}

const Photos = (props: PhotosProps) => {
  const { profileData, handlePrev, handleNext } = props;
  const [selectedImage, setSelectedImage] = useState<any>([]);
  const [isFileupLoad, setIsFileupLoad] = useState(false);

  const { register, formState } = useForm();

  useEffect(() => {
    if (profileData?.photos) {
      profileData?.photos.map((photo: any) => {
        setSelectedImage((prevState: any) => {
          return [
            ...prevState,
            {
              ["objectId"]: photo?.objectId || "",
              ["url"]: photo?.url || "",
              ["name"]: photo?.name || "",
            },
          ];
        });
      });
    }
  }, [profileData]);

  const onFileSelected = async (files: any[]) => {
    let image = files[0];
    const fileExtension = image?.name?.split(".")?.pop()?.toLowerCase();
    if (image && acceptedCertificateImage.includes(fileExtension)) {
      setIsFileupLoad(true);
      const formData = new FormData();
      formData.append("file", image);
      try {
        let response = await postFileAPI(formData);
        if (response) {
          setIsFileupLoad(false);
        }
        setSelectedImage((prevState: any) => {
          return [
            ...prevState,
            {
              ["objectId"]: "",
              ["url"]: response?.url,
              ["name"]: response?.file?.originalname,
            },
          ];
        });
      } catch (error) {
        setIsFileupLoad(false);
        console.log("not found");
      }
    } else {
      showToast(SUCCESS_MESSAGES.inputFileType, "warning");
    }
  };

  const handlePhotosSubmit = (e: any) => {
    handleNext({ photos: selectedImage });
  };

  const handleDeletefile = async (link: any, objectId: any, formId: any) => {
    if (link) {
      const updatedPhotos = selectedImage.filter(
        (image: any, index: any) => index !== formId
      );
      const data: any = { link: link };

      if (!link.includes("public/temp/uploads")) {
        data["type"] = "photoes";
        data["objectID"] = objectId;
      }

      try {
        await deleteFileAPI(data);
        setSelectedImage(updatedPhotos);
        showToast(SUCCESS_MESSAGES.fileDeletedSuccess, "success");
      } catch (error) {
        console.log(error, "error");
      }
    }
  };

  return (
    <>
      {isFileupLoad && <DefaultSkeleton />}
      <form className="panel-form">
        <div className="stepper-form">
          <div className={""}>
            <FileDropField
              {...{
                register,
                formState,
                name: `photos`,
                id: `photos`,
                type: "hidden",
                description: (
                  <>
                    <Image
                      src="/images/artistPhotosIcon.png"
                      className="mb-3"
                      alt="photo"
                      width={60}
                      height={56}
                    />
                    <div>
                      Drop your photo(s) here or <span> Browse </span>
                    </div>
                    <div className="mute">
                      Supports :{" "}
                      {acceptedCertificateImage.join(", ").toUpperCase()}
                    </div>
                  </>
                ),
                onChange: (e: any) => {
                  onFileSelected(e.target.files);
                },
              }}
            />
          </div>
          <div className={artistProfileStyle.listContainer}>
            {selectedImage &&
              selectedImage.length > 0 &&
              selectedImage.map((image: any, index: any) => (
                <div className="photos_List" key={index}>
                  <Image
                    src={image.url || image || ""}
                    className="m-0"
                    alt="image"
                    width={250}
                    height={180}
                  />
                  <Popconfirm
                    title="Are you sure you want to delete?"
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() =>
                      handleDeletefile(image?.url, image?.objectId, index)
                    }
                  >
                    <div
                      className={`${artistProfileStyle.deleteIcon} cursor-pointer`}
                    >
                      <DeleteIcon />
                    </div>
                  </Popconfirm>
                </div>
              ))}
          </div>
        </div>
        <Button
          htmlType="button"
          type="ghost"
          onClick={(d) => handlePrev(d)}
          className="mt-5"
        >
          &#8592; Previous Step
        </Button>
        <Button
          htmlType="button"
          type="primary"
          onClick={handlePhotosSubmit}
          className="float-right mt-5"
        >
          Next Step &#8594;
        </Button>
      </form>
    </>
  );
};
export default Photos;
