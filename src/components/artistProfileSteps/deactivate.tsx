import { showToast } from "@components/ToastContainer";
import {
  activateArtistAPI,
  deactivateArtistAPI,
  removeArtistAPI,
} from "@redux/services/artist.api";
import { whoAmI } from "@redux/services/auth.api";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { loginUserSuccess } from "@redux/slices/auth";
import { fetchSelectedArtist } from "@redux/slices/selectedArtist";
import { Popconfirm } from "antd";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import profileStyle from "./artistProfileSteps.module.scss";

export interface DeactivateArtistProps {
  profileData: UpdateArtistPayload;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
  setCurrentStep: (d: any) => void;
  selectedArtistData: any;
}

const DeactivateArtist = (props: DeactivateArtistProps) => {
  const { profileData, handlePrev, selectedArtistData } = props;

  const [artistId, setArtistId] = useState("");
  const dispatch = useDispatch();
  useEffect(() => {
    setArtistId(selectedArtistData.objectId);
  }, [selectedArtistData]);

  const handleDeactiveArtist = async () => {
    if (artistId) {
      try {
        await deactivateArtistAPI(artistId);
        showToast(SUCCESS_MESSAGES.ArtistDeactivatedSuccess, "success");
        dispatch(fetchSelectedArtist(artistId));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleActiveArtist = async () => {
    if (artistId) {
      try {
        await activateArtistAPI(artistId);
        showToast(SUCCESS_MESSAGES.ArtistActivatedSuccess, "success");
        dispatch(fetchSelectedArtist(artistId));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRemoveArtist = async () => {
    if (artistId) {
      try {
        await removeArtistAPI(artistId);
        showToast(SUCCESS_MESSAGES.ArtistRemovedSuccess, "success");
        const auth = await whoAmI();
        dispatch(loginUserSuccess(auth));
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <>
      <form className="panel-form">
        <div className="stepper-form">
          <div className="row align-center field-row">
            <div className={profileStyle.deactiveMainFiled}>
              <div className="de-active">
                {selectedArtistData?.approved && (
                  <Popconfirm
                    title={`Are you sure you want to Deactivate "${profileData?.artistName}"?`}
                    okText="Yes, Deactivate"
                    cancelText="Cancel"
                    onConfirm={handleDeactiveArtist}
                  >
                    <button type="button" className="">
                      Deactivate Artist
                    </button>
                  </Popconfirm>
                )}

                {!selectedArtistData?.approved && (
                  <Popconfirm
                    title={`Are you sure you want to Activate "${profileData?.artistName}"?`}
                    okText="Yes, Activate"
                    cancelText="Cancel"
                    onConfirm={handleActiveArtist}
                  >
                    <button type="button" className="">
                      Activate Artist
                    </button>
                  </Popconfirm>
                )}
              </div>
              <div className="de-active">
                <p>
                  This action will mark your profile {profileData?.artistName}{" "}
                  as unapproved and you will no longer appear publicly on
                  JCasp or be able to apply for gigs. If you change your mind
                  later, updating your profile information in any way that
                  satisfies the requirements will trigger re-approval for your
                  profile.
                </p>
              </div>
              <div className="Remove">
                <Popconfirm
                  title="Are you sure you want to Remove this Artist?"
                  okText="Yes, Remove"
                  cancelText="Cancel"
                  onConfirm={handleRemoveArtist}
                >
                  <button type="button" className="">
                    Remove Artist
                  </button>
                </Popconfirm>
              </div>
              <div className="de-active">
                <p className="">
                  This action will unapprove this artist profile and remove it
                  entirely from your dashboard.
                </p>
              </div>
            </div>
          </div>
        </div>
        <button
          type="button"
          onClick={(d) => handlePrev(d)}
          className="action-button previous previous_button mt-5"
        >
          &#8592; Previous Step
        </button>
      </form>
    </>
  );
};
export default DeactivateArtist;
