import { Button, DefaultSkeleton } from "@components/theme";
import {
  InputAfterField,
  InputField,
  InputGoogleAutoCompleteField,
  TextAreaField,
} from "@components/theme/form/formFieldsComponent";
import { ArtistHelpIcon } from "@components/theme/icons/artistHeplIcon";
import { EmailIcon } from "@components/theme/icons/emailIcon";
import { PhoneIcon } from "@components/theme/icons/phoneIcon";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState } from "@redux/reducers";
import { postFileAPI } from "@redux/services/artist.api";
import { checkValidAPI } from "@redux/services/general.api";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { fetchTags, TagsState } from "@redux/slices/tags";
import { useReCaptcha } from "next-recaptcha-v3";
import IMG from "next/image";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import { Tooltip } from "react-tooltip";
import { SUCCESS_MESSAGES } from "src/libs/constants";
import useList from "src/libs/useList";
import {
  ArtistProfileFormFirstSchema,
  ArtistProfileFormInputs,
} from "src/schemas/artistProfileFormSchema";
import profileStyles from "./artistProfileSteps.module.scss";

export interface ProfileProps {
  profileData: UpdateArtistPayload;
  handleNext: (d: any) => void;
  setIsProfileImgUpdated: (d: any) => void;
}

const Profile = (props: ProfileProps) => {
  const { profileData, handleNext, setIsProfileImgUpdated } = props;
  const [paymentPreferance, setPaymentPreferance] = useState<
    "paypal" | "venmo" | null
  >(null);
  const [isVenmoChecked, setIsVenmoChecked] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const [isPaypalChecked, setIsPaypalChecked] = useState(false);
  const [userProfileData, setUserProfileData] = useState<any>();
  const [artistElement, setArtistElement] = useState<any[]>([]);
  const [artistElementInput, setArtistElementInput] = useState("");
  const [imageIsLoading, setImageIsLoading] = useState(false);

  const [ischeckedEmail, setIsCheckedEmail] = useState(false);
  const [vanityTag, setVanityTag] = useState("");

  const dispatch = useDispatch();
  const { executeRecaptcha, loaded } = useReCaptcha();

  const { register, handleSubmit, formState, control, setValue, reset } =
    useForm<ArtistProfileFormInputs>({
      resolver: yupResolver(ArtistProfileFormFirstSchema),
    });

  useEffect(() => {
    if (profileData) {
      setValue("profileImage", profileData?.profileImage || "");
      setValue("artistName", profileData?.artistName || "");
      setValue("artistPhonenumber", profileData?.artistPhonenumber || "");
      setValue("artistEmailId", profileData?.artistEmailId || "");
      setValue("artistLocaleCity", profileData?.artistLocaleCity || "");
      if (profileData?.artistLocaleState) {
        setValue("artistLocaleState", profileData?.artistLocaleState || "");
      }
      if (profileData?.artistLocaleCountry) {
        setValue("artistLocaleCountry", profileData?.artistLocaleCountry || "");
      }
      setValue("youtubeId", profileData?.youtubeId || "");
      if (
        profileData?.paymentPreference &&
        profileData?.paymentPreference === "paypal"
      ) {
        setPaymentPreferance("paypal");
        setValue("paypalId", profileData?.paypalId || "");
      }
      if (
        profileData?.paymentPreference &&
        profileData?.paymentPreference === "venmo"
      ) {
        setPaymentPreferance("venmo");
        setValue("vemeoId", profileData?.vemeoId || "");
      }

      setValue("artistBio", profileData?.artistBio || "");
      setVanityTag(profileData?.JCaspVanityTag);
      setValue("JCaspVanityTag", `${profileData?.JCaspVanityTag || ""}`);
    }
  }, [profileData]);

  useEffect(() => {
    if (paymentPreferance) {
      if (paymentPreferance === "paypal") {
        setValue("vemeoId", "");
      } else {
        setValue("paypalId", "");
      }
      setValue("paymentPreference", paymentPreferance);
    }
  }, [paymentPreferance]);

  const handleArtistElement = (data: any) => {
    setArtistElement(data);
  };

  const handleNewElements = (event: any) => {
    if (event.key === "Enter") {
      if (artistElementInput) {
        let isAlready = false;
        artistElement.forEach((element) => {
          if (
            element.value.toLowerCase() === artistElementInput.toLowerCase()
          ) {
            isAlready = true;
          }
        });
        if (!isAlready) {
          const newOption = {
            value: artistElementInput.toLowerCase(),
            label: artistElementInput,
          };
          handleArtistElement([...artistElement, newOption]);
          setArtistElementInput("");
          event.preventDefault();
        } else {
          showToast(SUCCESS_MESSAGES.alreadyArtistElements, "warning");
        }
      }
    }
  };

  const {
    tags: { data: tagsData },
  }: {
    tags: TagsState;
  } = useSelector((state: RootState) => ({
    tags: state.tags,
  }));

  const options = tagsData?.map((tag) => ({
    value: tag.label,
    label: tag.label,
  }));

  const defaultTags: any = profileData?.artistElements?.map(
    (tag: any, i: any) => ({
      value: tag,
      label: tag,
    })
  );

  const tagData = artistElement.map((tag: any) => {
    return tag.label;
  });

  const onSubmit = (data: any) => {
    if (ischeckedEmail === false) {
      if (data) {
        setUserProfileData({
          ...data,
          profileImage: selectedImage,
          artistElements: tagData,
        });
      }
    }
  };

  useEffect(() => {
    if (userProfileData) {
      handleNext(userProfileData);
    }
  }, [userProfileData]);

  useEffect(() => {
    if (defaultTags) {
      setArtistElement(defaultTags);
    }
    if (profileData?.profileImage) {
      setSelectedImage(profileData?.profileImage);
    }
  }, [profileData]);

  const handleVanityChange = (event: any) => {
    const { value } = event.target;
    const sanitizedValue = value.toLowerCase().replace(/\s+/g, "-");
    setValue("JCaspVanityTag", sanitizedValue);
    setVanityTag(sanitizedValue);
  };

  const onFileSelected = async (e: any) => {
    let image = e.target.files[0];
    const allowedTypes = ["image/jpeg", "image/png", "image/jpg"];
    const reader = new FileReader();
    reader.onload = function (readerEvent: any) {
      const img: any = new Image();

      img.onload = async function () {
        // if (this.width >= 750 && this.height >= 586) {
        const formData = new FormData();
        formData.append("file", image);
        try {
          let response = await postFileAPI(formData);
          setSelectedImage(response?.url);
          setValue("profileImage", response?.url);
          setIsProfileImgUpdated("true");
          setImageIsLoading(false);
        } catch (error: any) {
          setImageIsLoading(false);
          showToast(error.message, "warning");
        }
      };
      img.src = readerEvent.target.result;
    };

    if (image && allowedTypes.includes(image.type)) {
      reader.readAsDataURL(image);
      setImageIsLoading(true);
    } else {
      showToast(SUCCESS_MESSAGES.inputFileType, "warning");
    }
  };

  const handleSelect = (e: any) => {
    e.preventDefault();
    let fileDialog = $('<input type="file">');
    fileDialog.click();
    fileDialog.on("change", onFileSelected);
  };

  const { apiParam } = useList({
    queryParams: {},
  });

  const getTags = async () => {
    const recaptchaResponse = await executeRecaptcha("tag_list");
    dispatch(fetchTags(apiParam, recaptchaResponse));
  };

  useEffect(() => {
    if (loaded) {
      getTags();
    }
  }, [loaded]);

  useEffect(() => {
    setValue("artistElements", artistElement);
  }, [artistElement]);

  const handleEmailChange = async (value: any, email: string) => {
    if (value === profileData?.artistEmailId) {
      return false;
    } else if (value) {
      apiParam.type = "email";
      apiParam.content = value;
      getValidate();
    }
  };

  const getValidate = async () => {
    if (apiParam)
      try {
        const res = await checkValidAPI(apiParam);
        if (res.available) {
          setIsCheckedEmail(true);
        } else {
          setIsCheckedEmail(false);
        }
      } catch (error) {
        console.log(error);
      }
  };

  return (
    <>
      {imageIsLoading && <DefaultSkeleton />}
      <form onSubmit={handleSubmit(onSubmit)} className="panel-form">
        <div className="stepper-form">
          <div className="row mt-4">
            <div className="col-md-3 col-sm-12">
              <label>Featured Image</label>
              <div className="muted">Supported Files : jpeg, jpg, png</div>
            </div>

            <div className="col-md-9 col-sm-12">
              <div className="profile-pic-edit">
                <IMG
                  src={selectedImage || "/images/artist-placeholder.png"}
                  alt="profile image"
                  width={180}
                  height={180}
                />
                <span className="edit-pp">
                  <a
                    title="Upload profile photo"
                    id="btnImportData"
                    onClick={(e) => {
                      e.preventDefault();
                      handleSelect(e);
                    }}
                  >
                    <IMG
                      src="/images/edit-ic.svg"
                      alt="edit"
                      width={36}
                      height={128}
                    />
                  </a>
                </span>
              </div>

              {formState &&
                formState?.errors &&
                formState?.errors["profileImage"] &&
                formState?.errors["profileImage"].message && (
                  <span className="text-danger">
                    {formState?.errors["profileImage"].message}
                  </span>
                )}
            </div>
          </div>

          <div className=" row stepper-form-element align-center mt-4">
            <div className="col-md-3 col-sm-12 ">
              <label
                data-tooltip-id="artistElement"
                data-tooltip-content="Ex: Soloist, Acoustic, vocals, rock, piano etc."
              >
                What best describes you?{" "}
                <ArtistHelpIcon
                  {...{
                    className: "text-danger",
                  }}
                />
                <Tooltip id="artistElement" place="bottom" />
              </label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <Select
                defaultValue={defaultTags || ""}
                isMulti
                className={`basic_multi_select selectField`}
                classNamePrefix="select"
                options={options}
                value={artistElement}
                onChange={handleArtistElement}
                onKeyDown={handleNewElements}
                inputValue={artistElementInput}
                onInputChange={(val) => setArtistElementInput(val)}
                instanceId="artistElement"
              />
              <p>
                {formState &&
                  formState?.errors &&
                  formState?.errors["artistElements"]?.message && (
                    <span className="text-danger">
                      {formState?.errors["artistElements"]?.message}
                    </span>
                  )}
              </p>
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label>Artist Name</label>
            </div>
            <div className="col-md-9 col-sm-12">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "artistName",
                  placeholder: "Enter Artist Name",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3 ">
            <div className="col-md-3 col-sm-12 ">
              <label>Artist Phone Number</label>
            </div>
            <div className="col-md-9 col-sm-12  mt-1 position-relative">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "artistPhonenumber",
                  placeholder: " Enter Artist Phone Number",
                }}
              />
              {
                <span className="phoneIcon">
                  {" "}
                  <PhoneIcon />{" "}
                </span>
              }
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12 ">
              <label>Artist Email</label>
            </div>
            <div className="col-md-9 col-sm-12  mt-1 position-relative">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "artistEmailId",
                  placeholder: " Enter Artist Email ID",
                }}
                onChange={(e: any) =>
                  handleEmailChange(e.target.value, profileData?.artistEmailId)
                }
              />
              {ischeckedEmail ? (
                <p className="text-danger">Email already exists</p>
              ) : (
                ""
              )}
              {
                <span className="phoneIcon">
                  {" "}
                  <EmailIcon />
                </span>
              }
            </div>
          </div>

          <div className="row  stepper-form-element mt-3 align-center">
            <div className="col-md-12 col-lg-3 col-12 mt-3">
              <label>Location</label>
              <br />
              <span className="muted labelDes">
                {" "}
                Please select the city in which you mostly perform
              </span>
            </div>
            <div
              className={`col-md-3 col-sm-12 mt-1 ${profileStyles.artistlocale}`}
            >
              <label>City</label>
            </div>
            <div
              className={`col-12 col-sm-3 w75 citySContry mt-1 ${profileStyles.cityStContry}`}
            >
              <InputGoogleAutoCompleteField
                {...{
                  register,
                  formState,
                  id: "artistLocaleCity",
                  className: "inputField",
                  placeholder: "City",
                  searchType: "(cities)",
                  googleAutoCompleteConfig: {
                    setValue,
                    autoCompleteId: "artistLocaleCity",
                    autoPopulateFields: {
                      province: "artistLocaleState",
                      country: "artistLocaleCountry",
                      city: "artistLocaleCity",
                    },
                  },
                }}
              />
              <div id="mapMoveHere"></div>
            </div>
            <div
              className={`col-md-3 col-sm-12 mt-3 ${profileStyles.artistlocale}`}
            >
              <label>State / Province</label>
            </div>
            <div
              className={`col-12 col-sm-3 w75 citySContry mt-1 ${profileStyles.stateStyle}`}
            >
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "artistLocaleState",
                  placeholder: " State",
                }}
              />
            </div>
            <div
              className={`col-md-3 col-sm-12  mt-3 ${profileStyles.artistlocale}`}
            >
              <label>Country</label>
            </div>
            <div className="col-12 col-sm-3 w75 citySContry mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  name: "artistLocaleCountry",
                  className: "inputField",
                  id: "artistLocaleCountry",
                  placeholder: "Country",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label
                data-tooltip-id="videoUrl-tooltip"
                data-tooltip-content="You will be able to add additional videos in the video section."
              >
                Featured Youtube Video URL{" "}
                <ArtistHelpIcon
                  {...{
                    className: "text-danger",
                  }}
                />
                <Tooltip id="videoUrl-tooltip" place="bottom" />
              </label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "youtubeId",
                  placeholder: "Enter Featured Youtube Video Url",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="mt-2">
              <label> Preferred Method of Payment </label>
            </div>
            <div className="col-12 col-sm-3">
              <div className="input-group">
                <div className="checkbox checkbox-info">
                  <input
                    type="checkbox"
                    id="PayPal"
                    className="valid"
                    aria-required="true"
                    aria-invalid="false"
                    checked={paymentPreferance === "paypal"}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setPaymentPreferance("paypal");
                      }
                    }}
                  />
                  <label
                    htmlFor="PayPal"
                    onClick={() => setPaymentPreferance("paypal")}
                  >
                    PayPal
                  </label>
                </div>
                <div />
              </div>
            </div>
            <div className="col-12 col-sm-9 citySContry mt-2">
              <InputField
                {...{
                  register,
                  formState,
                  className: `inputField`,
                  id: "paypalId",
                  placeholder: "purple.rain@gmail.com",
                  disabled: paymentPreferance !== "paypal",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-12 col-sm-3 ">
              <div className="input-group">
                <div className="checkbox checkbox-info">
                  <input
                    type="checkbox"
                    id="venmo"
                    className="valid"
                    aria-required="true"
                    aria-invalid="false"
                    checked={paymentPreferance === "venmo"}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setPaymentPreferance("venmo");
                      }
                    }}
                  />
                  <label
                    htmlFor="venmo"
                    onClick={() => setPaymentPreferance("venmo")}
                  >
                    Venmo
                  </label>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-9  citySContry mt-2">
              <InputField
                {...{
                  register,
                  formState,
                  className: `inputField`,
                  id: "vemeoId",
                  placeholder: "@CREAM",
                  disabled: paymentPreferance !== "venmo",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label>Artist Bio</label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <TextAreaField
                {...{
                  register,
                  formState,
                  className: `${profileStyles.textAreaField}`,
                  id: "artistBio",
                  placeholder: "Enter Artist Bio",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12 ">
              <label>JCasp Vanity Tag</label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputAfterField
                {...{
                  register,
                  formState,
                  className: `inputField ${profileStyles.gigVanity}`,
                  id: "JCaspVanityTag",
                  value: vanityTag || "",
                  placeholder: "Enter Your JCasp Vanity Tag",
                  control,
                  addonBefore: "https://jcaspdev.jcasptechnologies.com/",
                  onChange: handleVanityChange,
                }}
              />
            </div>
          </div>
        </div>
        <Button htmlType="submit" type="primary" className="float-right mt-4">
          Next Step &#8594;
        </Button>
      </form>
    </>
  );
};

export default Profile;
