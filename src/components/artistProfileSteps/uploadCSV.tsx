import { Button, DefaultSkeleton } from "@components/theme";
import { FileDropField } from "@components/theme/form/formFieldsComponent";
import { DeleteIcon } from "@components/theme/icons/deleteIcon";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  deleteSongsAPI,
  postFileAPI,
  postUploadCSVFileAPI,
} from "@redux/services/artist.api";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { fetchSelectedArtist } from "@redux/slices/selectedArtist";
import { Popconfirm } from "antd";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { acceptedCSVFiles, SUCCESS_MESSAGES } from "src/libs/constants";
import { downloadFile } from "src/libs/helpers";
import {
  UploadCSVFormInputs,
  UploadCSVFormSchema,
} from "src/schemas/uploadCSVfileSchema";
import profileStyle from "./artistProfileSteps.module.scss";

export interface UploadCSVProps {
  profileData: UpdateArtistPayload;
  setCurrentStep: (d: any) => void;
  selectedArtistData: any;
  handleNext: (d: any) => void;
}

const UploadCSV = (props: UploadCSVProps) => {
  const { profileData, setCurrentStep, selectedArtistData, handleNext } = props;

  const { register, handleSubmit, formState, setFocus, setValue, reset } =
    useForm<UploadCSVFormInputs>({
      resolver: yupResolver(UploadCSVFormSchema),
    });
  const dispatch = useDispatch();
  const [selectedCSv, setSelectedCSv] = useState<any>({
    csvFile: "",
    csvName: "",
  });
  const [isFileupLoad, setIsFileupLoad] = useState(false);
  const [artistId, setArtistId] = useState("");

  const [isSaveFile, setIsSaveFile] = useState(false);

  useEffect(() => {
    setArtistId(selectedArtistData.objectId);
  }, [selectedArtistData]);

  useEffect(() => {
    if (profileData) {
      setSelectedCSv({
        csvFile: profileData?.csvFile || "",
        csvName: profileData?.csvName || "",
      });
    }
  }, [profileData]);

  const handleSaveCSV = async (data: any) => {
    data["artistId"] = artistId;
    if (data && artistId) {
      setIsFileupLoad(true);
      try {
        const res = await postUploadCSVFileAPI(data);
        if (res?.message) {
          showToast(res.message, "success");
        }
        setIsSaveFile(false);
        setIsFileupLoad(false);
        dispatch(fetchSelectedArtist(artistId));
        reset();
      } catch (error) {
        console.log(error, "error");
        setIsFileupLoad(false);
      }
    }
  };

  const onCsvFileSelected = async (files: any) => {
    let file = files[0];
    const fileExtension = file?.name?.split(".")?.pop()?.toLowerCase();
    if (file && acceptedCSVFiles.includes(fileExtension)) {
      setIsFileupLoad(true);
      const formData = new FormData();
      formData.append("file", file);
      try {
        let response = await postFileAPI(formData);
        if (response) {
          setIsFileupLoad(false);
        }
        handleSaveCSV({ url: response?.url });
      } catch (error) {
        setIsFileupLoad(false);
        console.log("not found");
      }
    } else {
      showToast(SUCCESS_MESSAGES.fileValidError, "warning");
    }
  };

  const handleDeletefile = async (url: any, data: any) => {
    if (url && artistId) {
      try {
        await deleteSongsAPI(artistId);
        showToast(SUCCESS_MESSAGES.fileDeletedSuccess, "success");
        dispatch(fetchSelectedArtist(artistId));
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleNextStep = () => {
    if (selectedCSv) {
      handleNext(selectedCSv);
    } else {
      handleNext(selectedCSv);
    }
  };

  return (
    <>
      {isFileupLoad && <DefaultSkeleton />}
      <form className="panel-form">
        <div className="stepper-form">
          <div className="row align-center field-row">
            <div className="col-md-12 col-sm-12">
              <FileDropField
                {...{
                  register,
                  formState,
                  name: `csvUrl`,
                  id: `csvUrl`,
                  description: (
                    <>
                      <Image
                        src="/images/csv-file.png"
                        className="mb-3"
                        alt="csv file"
                        height={60}
                        width={60}
                      />
                      <div>
                        Drop your File here or <span>Browse </span>
                      </div>
                      <div className="mute">
                        Supports : {acceptedCSVFiles.join(", ").toUpperCase()}{" "}
                      </div>
                    </>
                  ),
                  onChange: (e: any) => {
                    onCsvFileSelected(e.target.files);
                  },
                }}
              />

              {selectedCSv && selectedCSv.csvFile && (
                <div className={profileStyle.uploaded}>
                  <div className={profileStyle.preview_file}>
                    <div className="preview-container">
                      <div className={profileStyle.photosList__ImageName}>
                        <div className={profileStyle.image}>
                          <Image
                            src={
                              selectedCSv?.csvFile?.includes("pdf")
                                ? "/images/general/pdf-icon.png"
                                : "/images/csv-fileformat.png"
                            }
                            className="m-0"
                            alt="file"
                            width={50}
                            height={50}
                          />
                        </div>
                        <div
                          className={`${profileStyle.name} text-ellipsis ms-2`}
                        >
                          <p className="ellipsesss">{selectedCSv.csvName}</p>
                        </div>
                      </div>
                      <Popconfirm
                        title="Are you sure you want to delete?"
                        okText="Yes, Delete"
                        cancelText="No"
                        onConfirm={() =>
                          handleDeletefile(selectedCSv?.csvFile, "delete")
                        }
                      >
                        <div
                          className={`${profileStyle.deleteIcon} cursor-pointer`}
                        >
                          <DeleteIcon />
                        </div>
                      </Popconfirm>
                    </div>
                  </div>

                  <div className="">
                    <Button
                      htmlType="button"
                      type="primary"
                      onClick={() =>
                        downloadFile(
                          `${selectedCSv?.csvFile}`,
                          `${selectedCSv.csvName}`
                        )
                      }
                    >
                      Download file
                    </Button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>

        <Button
          htmlType="button"
          type="ghost"
          onClick={() => setCurrentStep(1)}
          className="mt-4"
        >
          &#8592; Previous Step
        </Button>
        <Button
          htmlType="button"
          type="primary"
          onClick={() => handleNextStep()}
          className={`float-right mt-4 `}
        >
          Next Step &#8594;
        </Button>
      </form>
    </>
  );
};
export default UploadCSV;
