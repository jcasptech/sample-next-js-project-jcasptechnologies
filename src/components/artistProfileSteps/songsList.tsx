import { DefaultSkeleton } from "@components/theme";
import {
  FileDropField,
  InputField,
} from "@components/theme/form/formFieldsComponent";
import { showToast } from "@components/ToastContainer";
import { yupResolver } from "@hookform/resolvers/yup";
import { postFileAPI } from "@redux/services/artist.api";
import { UpdateArtistPayload } from "@redux/slices/artists";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { acceptedAudioFiles, SUCCESS_MESSAGES } from "src/libs/constants";
import {
  ArtistProfileFormSecondInputs,
  ArtistProfileFormSecondSchema,
} from "src/schemas/artistProfileFormSongsSchema";

export interface SongListProps {
  profileData: UpdateArtistPayload;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
  handleAddSongs: (d: any) => void;
  handleRemoveSongs: (d: any) => void;
}

const SongList = (props: SongListProps) => {
  const {
    profileData,
    handlePrev,
    handleNext,
    handleAddSongs,
    handleRemoveSongs,
  } = props;
  const [isFileupLoad, setIsFileupLoad] = useState(false);
  const { register, handleSubmit, formState, setValue } =
    useForm<ArtistProfileFormSecondInputs>({
      resolver: yupResolver(ArtistProfileFormSecondSchema),
    });

  useEffect(() => {
    if (profileData?.songs && profileData?.songs.length > 0) {
      setValue(`songs` as any, undefined);
      profileData?.songs.map((song, index) => {
        setValue(
          `songs[${index}].songSingerName` as any,
          song?.songSingerName || ""
        );
        setValue(`songs[${index}].songName` as any, song?.songName || "");
        setValue(`songs[${index}].url` as any, song?.url || "");
      });
    }
  }, [profileData]);

  const onFileSelected = async (files: any[], index: number) => {
    let audio = files[0];
    const fileExtension = audio?.name?.split(".")?.pop()?.toLowerCase();
    if (audio && acceptedAudioFiles.includes(fileExtension)) {
      setIsFileupLoad(true);
      const formData = new FormData();
      formData.append("file", audio);
      try {
        let response = await postFileAPI(formData);
        if (response) {
          setIsFileupLoad(false);
        }
        setValue(`songs[${index}].url` as any, response?.url || "");
      } catch (error) {
        console.log("not found");
      }
    } else {
      showToast(SUCCESS_MESSAGES.fileValidError, "warning");
    }
  };

  const onSubmit = (data: any) => {
    if (data && data.csvFile !== undefined) {
      handleNext(data);
    } else {
      handleNext({ songs: data.songs });
    }
  };

  return (
    <>
      {isFileupLoad && <DefaultSkeleton />}
      <form onSubmit={handleSubmit(onSubmit)} className="panel-form">
        <div className="stepper-form">
          {profileData.songs.map((list: any, index) => (
            <>
              <div
                className={`row stepper-form-element ${
                  index !== 0 ? "song_multi_add" : ""
                } align-center mt-3 `}
              >
                <div className="col-md-3 col-sm-12">
                  <label>Song Name</label>
                </div>
                <div className="col-md-9 col-sm-12">
                  <InputField
                    {...{
                      register,
                      formState,
                      className: "inputField",
                      defaultValue: list.name || list.songName || "",
                      name: `songs[${index}].songName`,
                      id: `songs[${index}].songName`,
                      placeholder: "Song Name",
                    }}
                  />
                  {formState &&
                    formState?.errors &&
                    formState?.errors["songs"] &&
                    formState?.errors["songs"][index]?.songName?.message && (
                      <span className="text-danger">
                        {formState?.errors["songs"][index]?.songName?.message}
                      </span>
                    )}
                </div>
              </div>
              <div className="row stepper-form-element align-center mt-3">
                <div className="col-md-3 col-sm-12">
                  <label>Song Singer Name</label>
                </div>
                <div className="col-md-9 col-sm-12">
                  <InputField
                    {...{
                      register,
                      formState,
                      className: "inputField",
                      defaultValue:
                        list.artistName || list.songSingerName || "",
                      name: `songs[${index}].songSingerName`,
                      id: `songs[${index}].songSingerName`,
                      placeholder: "Song Singer Name",
                    }}
                  />

                  {formState &&
                    formState?.errors &&
                    formState?.errors["songs"] &&
                    formState?.errors["songs"][index]?.songSingerName
                      ?.message && (
                      <span className="text-danger">
                        {
                          formState?.errors["songs"][index]?.songSingerName
                            ?.message
                        }
                      </span>
                    )}
                </div>
              </div>

              <div className="row stepper-form-element align-center mt-3">
                <div className="col-md-3 col-sm-12">
                  <label>Song File</label>
                </div>
                <div className="col-md-9 col-sm-12">
                  <FileDropField
                    {...{
                      register,
                      formState,
                      defaultValue: list.url || "",
                      name: `songs[${index}].url`,
                      id: `songs[${index}].url`,
                      type: "hidden",
                      description: (
                        <>
                          <Image
                            src="/images/dropAudio.png"
                            className="mb-3"
                            alt="audio"
                            height={60}
                            width={60}
                          />
                          {/* <img src="/images/dropAudio.png" className="mb-3" /> */}
                          <div>
                            Drop your File here or <span>Browse </span>
                          </div>
                          <div className="mute">
                            Supports :{" "}
                            {acceptedAudioFiles.join(", ").toUpperCase()}{" "}
                            {list?.url && (
                              <Image
                                src="/images/mp3FileIcon.svg"
                                alt="mp 3"
                                height={30}
                                width={30}
                              />
                            )}
                          </div>
                        </>
                      ),
                      onChange: (e: any) => {
                        onFileSelected(e.target.files, index);
                      },
                    }}
                  />
                  {formState &&
                    formState?.errors &&
                    formState?.errors["songs"] &&
                    formState?.errors["songs"][index]?.url?.message && (
                      <span className="text-danger">
                        {formState?.errors["songs"][index]?.url?.message}
                      </span>
                    )}
                </div>
              </div>

              <div className="row align-center mt-2 field-row">
                <div className="col-12 col-sm-3">&nbsp;</div>

                <div className="col-12 col-sm-9 clone_acts">
                  {index === 0 && (
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        handleAddSongs(e);
                      }}
                      className="clone adcln"
                    >
                      <Image
                        src="/images/general/add_song.svg"
                        alt="Add Song"
                        height={24}
                        width={24}
                      />
                      {/* <img src="/images/add_cl.svg" /> */}
                      &nbsp;Add Song
                    </a>
                  )}
                  {index !== 0 && (
                    <a
                      onClick={(e) => {
                        e.preventDefault();
                        handleRemoveSongs(index);
                      }}
                      className="delete dltcln"
                    >
                      <Image
                        src="/images/general/rem_song.svg"
                        alt="Remove Song"
                        height={24}
                        width={24}
                      />
                      {/* <img src="/images/rem_cl.svg" /> */}
                      &nbsp;Remove Song
                    </a>
                  )}
                </div>
              </div>
            </>
          ))}
        </div>

        <button
          type="button"
          onClick={handlePrev}
          className="action-button previous previous_button mt-4"
        >
          &#8592; Previous Step
        </button>
        <button type="submit" className="next action-button link-button mt-4">
          Next Step
        </button>
      </form>
    </>
  );
};
export default SongList;
