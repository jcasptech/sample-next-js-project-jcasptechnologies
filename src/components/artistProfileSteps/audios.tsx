import { DefaultSkeleton } from "@components/theme";
import { FileDropField } from "@components/theme/form/formFieldsComponent";
import { DeleteIcon } from "@components/theme/icons/deleteIcon";
import { showToast } from "@components/ToastContainer";
import { deleteFileAPI, postFileAPI } from "@redux/services/artist.api";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { Popconfirm } from "antd";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { acceptedAudioFiles, SUCCESS_MESSAGES } from "src/libs/constants";
import artistProfileStyle from "./artistProfileSteps.module.scss";

export interface ArtistAudiosProps {
  profileData: UpdateArtistPayload;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
}

const ArtistAudios = (props: ArtistAudiosProps) => {
  const { profileData, handlePrev, handleNext } = props;
  const [selectedAudio, setSelectedAudio] = useState<any>([]);
  const [isError, setIsError] = useState(false);
  const [isFileupLoad, setIsFileupLoad] = useState(false);

  const { register, formState, control, setValue, reset } = useForm();

  useEffect(() => {
    if (profileData?.audioFiles) {
      profileData?.audioFiles.map((audio: any) => {
        setSelectedAudio((prevState: any) => {
          return [
            ...prevState,
            {
              ["objectID"]: audio?.objectId || "",
              ["url"]: audio?.url || "",
              ["name"]: audio?.name || "",
            },
          ];
        });
      });
    }
  }, [profileData]);

  const onFileSelected = async (files: any[]) => {
    let audio = files[0];
    const fileExtension = audio?.name?.split(".")?.pop()?.toLowerCase();
    if (audio && acceptedAudioFiles.includes(fileExtension)) {
      setIsFileupLoad(true);
      const formData = new FormData();
      formData.append("file", audio);
      try {
        let response = await postFileAPI(formData);
        if (response) {
          setIsFileupLoad(false);
        }
        setSelectedAudio((prevState: any) => {
          return [
            ...prevState,
            {
              ["objectID"]: "",
              ["url"]: response?.url,
              ["name"]: response?.file?.originalname,
            },
          ];
        });
      } catch (error) {
        setIsFileupLoad(false);
        console.log("not found");
      }
    } else {
      showToast(SUCCESS_MESSAGES.fileValidError, "warning");
    }
  };

  const handleDeletefile = async (link: any, objectId: any, formId: any) => {
    if (link) {
      const data: any = { link: link };

      if (!link.includes("public/temp/uploads")) {
        data["type"] = "songs";
        data["objectID"] = objectId;
      }

      try {
        await deleteFileAPI(data);
        const updatedAudio = selectedAudio.filter(
          (audio: any, index: any) => index !== formId
        );
        setSelectedAudio(updatedAudio);
        showToast(SUCCESS_MESSAGES.fileDeletedSuccess, "success");
      } catch (error) {
        console.log(error, "error");
      }
    }
  };

  const handleAudio = () => {
    handleNext({ audioFiles: selectedAudio });
  };

  return (
    <>
      {isFileupLoad && <DefaultSkeleton />}
      <form className="panel-form">
        <div className="stepper-form">
          <div className={""}>
            <FileDropField
              {...{
                register,
                formState,
                name: `audio`,
                id: `audio`,
                type: "hidden",
                description: (
                  <>
                    <Image
                      src="/images/dropAudio.png"
                      className="mb-3"
                      alt="audio"
                      height={60}
                      width={60}
                    />
                    <div>
                      Drop your File here or <span>Browse </span>
                    </div>
                    <div className="mute">
                      Supports : {acceptedAudioFiles.join(", ").toUpperCase()}
                    </div>
                  </>
                ),
                onChange: (e: any) => {
                  onFileSelected(e.target.files);
                },
              }}
            />
          </div>
          {isError && <span className="text-danger">Please select file</span>}
          <div className={artistProfileStyle.listContainer}>
            {selectedAudio &&
              selectedAudio.length > 0 &&
              selectedAudio.map((audio: any, index: number) => (
                <div
                  className={artistProfileStyle.photosList}
                  key={`ArtistAudios_${index}`}
                >
                  <div className={artistProfileStyle.photosList__ImageName}>
                    <div className={artistProfileStyle.image}>
                      <Image
                        src="/images/audioIcon.png"
                        className="m-0"
                        alt="audio"
                        width={46}
                        height={46}
                        quality={100}
                      />
                    </div>
                    <div className={`${artistProfileStyle.name} ms-2`}>
                      <p>{audio?.name}</p>
                    </div>
                  </div>
                  <Popconfirm
                    title="Are you sure you want to delete?"
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() =>
                      handleDeletefile(audio.url, audio?.objectID, index)
                    }
                  >
                    <div
                      className={`${artistProfileStyle.deleteIcons} cursor-pointer`}
                    >
                      <DeleteIcon />
                    </div>
                  </Popconfirm>
                </div>
              ))}
          </div>
        </div>
        <button
          type="button"
          onClick={(d) => handlePrev(d)}
          className="action-button previous previous_button mt-5"
        >
          &#8592; Previous Step
        </button>
        <button
          type="button"
          onClick={handleAudio}
          className="next action-button link-button mt-5"
        >
          Next Step &#8594;
        </button>
      </form>
    </>
  );
};
export default ArtistAudios;
