import { Button } from "@components/theme";
import { InputField } from "@components/theme/form/formFieldsComponent";
import { ArtistHelpIcon } from "@components/theme/icons/artistHeplIcon";
import { FacebookIcon } from "@components/theme/icons/facebookIcon";
import { InstagramIcon } from "@components/theme/icons/instagramIcon";
import { PatreonIcon } from "@components/theme/icons/patreonIcon";
import { SpotifyIcon } from "@components/theme/icons/spotifyIcon";
import { VimeoIcon } from "@components/theme/icons/vimeoIcon";
import { WebsiteIcon } from "@components/theme/icons/websiteIcon";
import { YoutubeIcon } from "@components/theme/icons/youtubeIcon";
import { yupResolver } from "@hookform/resolvers/yup";
import { UpdateArtistPayload } from "@redux/slices/artists";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { Tooltip } from "react-tooltip";
import { ArtistProfileFormLinksSchema } from "src/schemas/artistProfileFormLinksSchema";
import { ArtistProfileFormInputs } from "src/schemas/artistProfileFormSchema";

export interface ArtistLinksProps {
  profileData: UpdateArtistPayload;
  handlePrev: (d: any) => void;
  handleNext: (d: any) => void;
}

const ArtistLinks = (props: ArtistLinksProps) => {
  const { profileData, handlePrev, handleNext } = props;
  const { register, handleSubmit, formState, setValue } =
    useForm<ArtistProfileFormInputs>({
      resolver: yupResolver(ArtistProfileFormLinksSchema),
    });

  useEffect(() => {
    if (profileData) {
      setValue("patreonLink", profileData?.patreonLink || "");
      setValue("facebookPage", profileData?.facebookPage || "");
      setValue("instagramUsername", profileData?.instagramUsername || "");
      setValue("website", profileData?.website || "");
      setValue("liveStreamUrl", profileData?.liveStreamUrl || "");
      setValue("vemeoLink", profileData?.vemeoLink || "");
      setValue("spotifyLink", profileData?.spotifyLink || "");
      setValue("youtubeChannel", profileData?.youtubeChannel || "");
    }
  }, [profileData]);

  const onSubmit = (data: any) => {
    if (data) {
      handleNext(data);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="panel-form">
        <div className="stepper-form">
          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label
                data-tooltip-id="spotify-link"
                data-tooltip-content="Youtube Link"
              >
                <YoutubeIcon theme="dark" />
                &nbsp; Youtube Channel
                <span className="ms-1">
                  {" "}
                  <ArtistHelpIcon />{" "}
                </span>
                <Tooltip id="spotify-link" place="bottom" />
              </label>
            </div>
            <div className="col-md-9 col-sm-12">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "youtubeChannel",
                  placeholder: "Enter Youtube Link",
                }}
              />
            </div>
          </div>
          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label
                data-tooltip-id="website-link"
                data-tooltip-content="Website link"
              >
                <WebsiteIcon theme="dark" />
                &nbsp; Website
                <span className="ms-1">
                  {" "}
                  <ArtistHelpIcon />{" "}
                </span>
                <Tooltip id="website-link" place="bottom" />
              </label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "website",
                  placeholder: "Enter Website Address",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label
                data-tooltip-id="patreon-link"
                data-tooltip-content="Enter Patreon Link"
              >
                <PatreonIcon theme="dark" />
                &nbsp; Patreon
                <span className="ms-1">
                  <ArtistHelpIcon />{" "}
                </span>
                <Tooltip id="patreon-link" place="bottom" />
              </label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "patreonLink",
                  placeholder: "Enter Patreon Link",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label
                data-tooltip-id="instagram-username"
                data-tooltip-content="Instagram Username"
              >
                <InstagramIcon theme="dark" />
                &nbsp; Instagram
                <span className="ms-1">
                  {" "}
                  <ArtistHelpIcon />{" "}
                </span>
                <Tooltip id="instagram-username" place="bottom" />
              </label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "instagramUsername",
                  placeholder: "Enter Instagram Username",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12 ">
              <label
                data-tooltip-id="facebook-page"
                data-tooltip-content="Facebook page link"
              >
                <FacebookIcon theme="dark" />
                &nbsp; Facebook
                <span className="ms-1">
                  {" "}
                  <ArtistHelpIcon />{" "}
                </span>
                <Tooltip id="facebook-page" place="bottom" />
              </label>
            </div>
            <div className="col-md-9 col-sm-12  mt-1 position-relative">
              <InputField
                {...{
                  register,
                  formState,
                  id: "facebookPage",
                  className: "inputField",
                  placeholder: "Enter Facebook page Link",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label>
                <SpotifyIcon theme="dark" />
                &nbsp; Spotify
              </label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "spotifyLink",
                  placeholder: "Enter Spotify Link",
                }}
              />
            </div>
          </div>

          <div className="row stepper-form-element align-center mt-3">
            <div className="col-md-3 col-sm-12">
              <label>
                <VimeoIcon theme="dark" />
                &nbsp; Vimeo
              </label>
            </div>
            <div className="col-md-9 col-sm-12 mt-1">
              <InputField
                {...{
                  register,
                  formState,
                  className: "inputField",
                  id: "vemeoLink",
                  placeholder: "Enter Vemeo Link",
                }}
              />
            </div>
          </div>
        </div>
        <Button
          htmlType="button"
          type="ghost"
          onClick={(d) => handlePrev(d)}
          className="mt-4"
        >
          &#8592; Previous Step
        </Button>
        <Button htmlType="submit" type="primary" className="float-right mt-4">
          Next Step &#8594;
        </Button>
      </form>
    </>
  );
};
export default ArtistLinks;
