import { PrevSlickIcon } from "@components/theme/icons/nextPrevIcon";
import { NextSlickIcon } from "@components/theme/icons/nextSlickIcon";
import { useRef } from "react";
import Slider from "react-slick";
import latestReviewStyle from "./latestReview.module.scss";
export interface LetestReviewsProps {
  settings: any;
  letestFiveReviewData: any;
}
const LetestReviews = (props: LetestReviewsProps) => {
  const { settings, letestFiveReviewData } = props;

  const sliderRef = useRef<any>(null);

  return (
    <div
      className={`"venue_slider position-relative pb-0 ${latestReviewStyle.venue_slider_2}`}
    >
      <div className="slider demo">
        <Slider ref={sliderRef} {...settings}>
          {/* <div className="">
            <div className={`${latestReviewStyle.testi_box} testi-box2 `}>
              <div className="card-gradient-j">
                <div className="row cards-j">
                  {letestFiveReviewData &&
                    letestFiveReviewData?.length > 0 &&
                    letestFiveReviewData
                      .slice(0, 4)
                      .map((review: any, index: number) => (
                        <div className="card card-bg text-white" key={index}>
                          <div className={`${latestReviewStyle.card_j}`}>
                            <h4>
                              {review?.user?.name || "Lucas Oliver"}{" "}
                              <img src="/images/about-us/blue-tic.png" alt="" />
                            </h4>
                            <div className="rating">
                              <ul>
                                {[1, 2, 3, 4, 5].map((star) => (
                                  <li key={`star-${star}`}>
                                    {star <= review?.rating ? (
                                      <FullStartIcon />
                                    ) : star > review?.rating &&
                                      review?.rating > star - 1 ? (
                                      <HalfStarIcon />
                                    ) : (
                                      <FullStarGrayIcon />
                                    )}
                                  </li>
                                ))}
                              </ul>
                            </div>
                            <br />
                            <p className="card-text">
                              {review?.comment || "Artist Comment"}
                            </p>
                          </div>
                        </div>
                      ))}
                </div>
              </div>
              <div
                className="col-12 maintesti_name maintesti_name2"
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                <h3>JCasp Light User Reviews</h3>
              </div>
              <div
                className={`col-12 ${latestReviewStyle.maintesti_text} maintesti_text2`}
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="1200"
              >
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                  vulputate libero et velit interdum, ac aliquet odio mattis.
                </p>
              </div>
            </div>
          </div> */}

          {/* <div className="">
            <div className={`row img-2-2 ${latestReviewStyle.img_2_2} pt-3`}>
              <img src="/images/slider-2-2.png" alt="img" />
            </div>

            <div className={`row ${latestReviewStyle.img_1_2} pt-3`}>
              <img src="/images/slider1-2.png" alt="img" />
            </div>
          </div>
          <div className="">
            <div className={`row ${latestReviewStyle.img_2} pt-3`}>
              <img src="/images/slider-2.png" alt="img" />
            </div>
            <div className={`row ${latestReviewStyle.img_1} pt-3`}>
              <img src="/images/slider1.png" alt="img" />
            </div>
          </div>

          <div className="">
            <div className={latestReviewStyle.logoContainer}>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/bistro.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}`}>
                <img
                  className=""
                  src="/images/JCasp-light/black_angus.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img src="/images/JCasp-light/bridgessantafe.png" alt="img" />
              </div>

              <div className={`${latestReviewStyle.logoContainer__logo}`}>
                <img
                  className=""
                  src="/images/JCasp-light/cohn.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}`}>
                <img src="/images/JCasp-light/coto-de-caza.png" alt="img" />
              </div>

              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/crosbysantafe.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/crossingscarlsbad.png"
                  alt="img"
                />
              </div>

              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/draftrepublix.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/elniguel.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/griffinclub.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/hilton.png"
                  alt="img"
                />
              </div>

              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/jcresorts.png"
                  alt="img"
                />
              </div>

              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/LogoCropped.jpg"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/northcity.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/PALOMA-identity.png"
                  alt="img"
                />
              </div>
              <div className={`${latestReviewStyle.logoContainer__logo}  `}>
                <img
                  className=""
                  src="/images/JCasp-light/hilton.png"
                  alt="img"
                />
              </div>
            </div>
          </div> */}
        </Slider>
        <div className={`${latestReviewStyle.slide_Icon}`}>
          <a onClick={() => sliderRef?.current?.slickPrev()}>
            <PrevSlickIcon
              {...{
                className: "me-2",
              }}
            />
          </a>
          <a onClick={() => sliderRef?.current?.slickNext()}>
            <NextSlickIcon
              {...{
                className: "ms-2",
              }}
            />
          </a>
        </div>
      </div>
    </div>
  );
};
export default LetestReviews;
