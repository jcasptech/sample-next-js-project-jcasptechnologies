/** @type {import('next').NextConfig} */
const withPlugins = require("next-compose-plugins");
const withLess = require("next-with-less");
const withPurgeCSS = require("next-purgecss");
const { withSentryConfig } = require("@sentry/nextjs");

const path = require("path");
const runtimeCaching = require("next-pwa/cache");
const generateRobotsTxt = require("./scripts/generate-robots-txt");

let nextConfig = {
  reactStrictMode: false,
  webpack5: true,
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  env: {
    NEXT_ENV: process.env.NEXT_ENV,
    NEXT_ENDPOINT: process.env.NEXT_ENDPOINT,
    NEXT_API_ENDPOINT: process.env.NEXT_API_ENDPOINT,
  },
  poweredByHeader: false,
  future: {},
  webpack(config, { isServer }) {
    if (isServer) {
      generateRobotsTxt();
    }
    return config;
  },
  images: {
    domains: ["localhost", "files.jcasptechnologies.com", "jcaspdev.jcasptechnologies.com"],
    minimumCacheTTL: 31536000,
    formats: ["image/avif", "image/webp"],
  },
  // async redirects() {
  //   return [
  //     {
  //       source: "/",
  //       destination: "/",
  //       permanent: false,
  //     },
  //   ];
  // },
  async headers() {
    return [
      {
        source: "/_next/static/(.*)",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000, immutable", // Max age for one year
          },
        ],
      },
      {
        source: "/images/(.*)",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000", // Max age for one week
          },
        ],
      },
    ];
  },
};

if (process.env.NEXT_ENV !== "local") {
  nextConfig.pwa = {
    dest: "public",
    runtimeCaching,
  };
  const withPWA = require("next-pwa");
  nextConfig = withPWA(nextConfig);
}

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: false,
});

const SentryWebpackPluginOptions = {
  // Your Sentry configuration options here
  dsn: process.env.SENTRY_DSN,
};

const plugins = [
  [
    withLess,
    {
      lessLoaderOptions: {
        lessOptions: {
          modifyVars: {
            "primary-color": "#ED1846",
            "link-color": "#ED1846",
            "success-color": "#6DB324",
            "warning-color": "#F26B1D",
            "error-color": "#FD6060",
            "font-size-base": "14px",
            "heading-color": "#000000",
            "text-color": "#000000",
            "text-color-secondary": "#12263F",
            "disabled-color": "#ef5674",
            "border-radius-base": "2px",
            "border-color-base": "#F1F1F1",
          },
        },
      },
    },
  ],
  [withBundleAnalyzer],
  // [withSentryConfig, SentryWebpackPluginOptions],
];

module.exports = withPlugins(plugins, nextConfig);
