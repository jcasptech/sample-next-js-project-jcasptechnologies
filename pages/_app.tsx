/* eslint-disable @next/next/no-sync-scripts, @next/next/no-css-tags, react-hooks/exhaustive-deps */
import { DefaultSkeleton } from "@components/theme/defaultSkeleton";
import {
  loginReset,
  loginUserFail,
  loginUserReset,
  loginUserSuccess,
} from "@redux/actions";
import { getLoginUser } from "@redux/services/auth.api";
import { wrapper } from "@redux/store";
import "bootstrap/dist/css/bootstrap.min.css";
import "@styles/global.scss";
import App, { AppProps } from "next/app";
import { AppContextType } from "next/dist/shared/lib/utils";
import Head from "next/head";
import router, { Router, useRouter } from "next/router";
import { useEffect, useState } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import ErrorComponent from "src/containers/error/errorComponent";
import { LayoutContextProvider } from "src/contexts/layoutContext";
import { API, nextRedirect } from "src/libs/helpers";
import Cookies from "universal-cookie";
import { definePermission, PermissionProvider } from "react-redux-permission";
import { useDispatch, useStore } from "react-redux";
import "@fortawesome/fontawesome-svg-core/styles.css";
import "hover.css";
import { setGeneralTheme, setLocation } from "@redux/slices/general";
import {
  GOOGLE_API_KEY,
  GOOGLE_RECAPTCHA_SITE_KEY,
  PERMISSIONS,
} from "src/libs/constants";
import { ReCaptchaProvider } from "next-recaptcha-v3";
import "antd/dist/antd.css";
import "react-tooltip/dist/react-tooltip.css";
import { ToastContainer } from "@components/ToastContainer";
import "@styles/panel-menu.scss";
import "@styles/steppers.scss";
import "@styles/navigation.scss";

const Noop = ({ children }: { children: JSX.Element }) => children;

const MyApp = ({
  Component,
  Component: { Layout = Noop, ...restLayoutProps },
  pageProps,
}: AppProps | any): JSX.Element => {
  const queryClient = new QueryClient();

  const { statusCode = 200 } = pageProps || {};

  const [isLoading, setIsLoading] = useState(false);

  const store = useStore();
  // const dispatch = useDispatch();

  Router.events.on("routeChangeStart", (/*url*/) => {
    setIsLoading(true);
  });

  Router.events.on("routeChangeComplete", () => {
    setIsLoading(false);
  });

  Router.events.on("routeChangeError", () => {
    setIsLoading(false);
  });

  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");

    // if (navigator.geolocation) {
    //   navigator.geolocation.getCurrentPosition(
    //     (position) => {
    //       const { latitude, longitude } = position.coords;
    //       const geocodingApiUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${GOOGLE_API_KEY}`;
    //       fetch(geocodingApiUrl)
    //         .then((response) => response.json())
    //         .then((data) => {
    //           if (data.results.length > 0) {
    //             const city = data.results[0].address_components.find(
    //               (component: any) => component.types.includes("locality")
    //             ).long_name;
    //             // Dispatch the city to your state or perform any other actions
    //             dispatch(
    //               setLocation({
    //                 Latitude: latitude,
    //                 Longitude: longitude,
    //                 City: city,
    //               })
    //             );
    //           }
    //         });
    //     },
    //     (error) => {
    //       console.log(error);
    //     }
    //   );
    // } else {
    //   console.log("Geolocation is not supported by this browser.");
    // }
  }, []);

  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
        <title>Local Shows on JCasp</title>
        <meta name="description" content="JCasp" />
        <meta name="keywords" content="JCasp" />
        <link href="/favicon.ico" rel="icon" />
        {process.env.NEXT_ENV !== "production" && (
          <meta name="robots" content="noindex,nofollow" />
        )}
      </Head>
      <QueryClientProvider client={queryClient}>
        <PermissionProvider store={store} reducerKey="permissions">
          <ReCaptchaProvider reCaptchaKey={GOOGLE_RECAPTCHA_SITE_KEY}>
            <LayoutContextProvider>
              {statusCode === 200 && (
                <>
                  <Layout {...restLayoutProps}>
                    {isLoading && (
                      <>
                        <div>
                          <DefaultSkeleton isContent={true} />
                        </div>
                      </>
                    )}
                    {!isLoading && <Component {...pageProps} />}
                  </Layout>
                </>
              )}
              {!isLoading && statusCode !== 200 && (
                <ErrorComponent {...pageProps} />
              )}
            </LayoutContextProvider>
          </ReCaptchaProvider>
        </PermissionProvider>
      </QueryClientProvider>

      <ToastContainer />
    </>
  );
};

MyApp.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (appContext: AppContextType | any) => {
    const ctx = appContext.ctx;
    let appProps: any = {};

    if (ctx?.pathname !== "/artist/[vanity]") {
      store.dispatch(setGeneralTheme(""));
    }

    if (appContext.ctx.req) {
      const cookies = new Cookies(ctx.req.headers.cookie);
      const authCookies: any = cookies.get("authApp");
      // Server Side Axios Setup.
      const axiosInstance = API(true);
      axiosInstance.interceptors.request.use(
        (c: any) => {
          if (authCookies?.accessToken) {
            c.headers["authorization"] = `Bearer ${authCookies?.accessToken}`;
          }
          return c;
        },
        (error: any) => {
          // Do something with request error
          return Promise.reject(error);
        }
      );

      // This Line will trigger component level getInitialProps
      appProps = await App.getInitialProps(appContext);
      const statusCode = appProps?.pageProps?.statusCode || 200;

      if (statusCode === 404) {
        if (ctx.pathname === "/404") {
          nextRedirect({ ctx, location: "/404" });
        }
      }

      if (statusCode < 400) {
        if (authCookies) {
          if (authCookies?.accessToken) {
            if (ctx.pathname === "/login") {
              nextRedirect({ ctx, location: "/" });
            }
          }
        } else {
          const redirectUrls = [
            "/",
            "/404",
            "/login",
            "/signup",
            "/forgot-password",
            "/[token]/reset-password",
            "/verify-email",
            "/email-verified",
            "/email-invalid",
            "/video-test",
            "/pay-test",
            "/terms-of-use",
            "/privacy-policy",
            "/artist-signup",
            "/browse",
            "/artist/[vanity]",
            "/venue/[vanity]",
            "/JCasp-light",
            "/pricing",
            "/contact-us",
            "/payments/success",
            "/payments/cancel",
            "/landing",
            "/about-us",
            "/how-it-works",
            "/faq",
            "/chat-gpt-plugin-terms-of-use",
            "/services",
          ];
          if (redirectUrls.indexOf(ctx.pathname) === -1) {
            nextRedirect({ ctx, location: "/" });
          }
        }
      }
      // Fill data of loginUser in redux from Serverside.
      if (authCookies?.accessToken) {
        try {
          const loginUser = await getLoginUser({
            Authorization: `Bearer ${authCookies?.accessToken}`,
          });

          store.dispatch(
            definePermission(
              PERMISSIONS["COMMON"].concat(PERMISSIONS[authCookies?.userType])
            )
          );
          store.dispatch(loginUserSuccess(loginUser));
        } catch (e: any) {
          store.dispatch(loginUserFail(e.message));
          store.dispatch(loginReset());
          store.dispatch(loginUserReset());
        }
      }
    } else {
      appProps = await App.getInitialProps(appContext);

      const statusCode = appProps?.pageProps?.statusCode || 200;

      if (statusCode === 404) {
        if (ctx.pathname === "/404") {
          nextRedirect({ ctx, location: "/404" });
        }
      }

      const cookies = new Cookies();
      const authCookies: any = cookies.get("authApp");
      if (authCookies) {
        if (authCookies?.accessToken) {
          if (ctx.pathname === "/login") {
            nextRedirect({ ctx, location: "/" });
          }
        }
      } else {
        const redirectUrls = [
          "/",
          "/404",
          "/login",
          "/signup",
          "/forgot-password",
          "/[token]/reset-password",
          "/verify-email",
          "/email-verified",
          "/email-invalid",
          "/video-test",
          "/pay-test",
          "/terms-of-use",
          "/privacy-policy",
          "/artist-signup",
          "/browse",
          "/artist/[vanity]",
          "/venue/[vanity]",
          "/JCasp-light",
          "/pricing",
          "/contact-us",
          "/payments/success",
          "/payments/cancel",
          "/landing",
          "/about-us",
          "/how-it-works",
          "/faq",
          "/terms",
          "/chat-gpt-plugin-terms-of-use",
          "/services",
        ];
        if (redirectUrls.indexOf(ctx.pathname) === -1) {
          nextRedirect({ ctx, location: "/" });
        }
      }
    }

    return { ...appProps };
  }
);

export default wrapper.withRedux(MyApp);
